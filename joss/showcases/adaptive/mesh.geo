//+
Point(1) = {0, 0, 0, 0.05};
//+
Point(2) = {2, 0, 0, 0.05};
//+
Point(3) = {0, 2, 0, 0.05};
//+
Point(5) = {2, 1.5, 0, 0.05};
//+
Point(6) = {0.6, 1, 0, 0.05};
//+
Point(7) = {1.5, 1.6, 0, 0.05};
//+
Spline(3) = {3, 6, 7, 5};
//+
Line(4) = {1, 3};
//+
Line(5) = {1, 2};
//+
Line(6) = {2, 5};
//+
Dilate {{0, 0, 0}, {1.5, 1, 1}} {
  Curve{4}; Point{1}; Curve{5}; Point{2}; Curve{6}; Point{5}; Curve{3}; Point{7}; Point{6}; Point{3}; 
}
//+
Curve Loop(1) = {4, 3, -6, -5};
//+
Plane Surface(1) = {1};
//+
Physical Curve(1) = {5};
//+
Physical Curve(2) = {4};
//+
Physical Curve(3) = {3};
//+
Physical Curve(4) = {6};
//+
Physical Surface(5) = {1};
