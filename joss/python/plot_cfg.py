import matplotlib as mpl
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import make_axes_locatable

AESTHETICS_SETTINGS = {
    "figure.dpi": 300,
    # Font size
    "font.size": 10.0,
    # Relative font sizes (should all be the same as font.size)
    "figure.titlesize": "medium",
    "axes.titlesize": "medium",
    "axes.labelsize": "medium",
    "xtick.labelsize": "medium",
    "ytick.labelsize": "medium",
    "legend.fontsize": "medium",
    "legend.title_fontsize": "medium",
    # Line width
    "lines.linewidth": 1.125,  # = lines.linewidth * 3 / 4
}
mpl.rcParams.update(AESTHETICS_SETTINGS)

# Regular figure settings
WIDTH_DEFAULT = 6.4
HEIGHT_DEFAULT = 4.8


def append_colorbar(
    axes, data, location="right", orientation="vertical", size="3%", pad=0.1
):
    """Append a colorbar to the axes object given the colorbar data."""
    divider = make_axes_locatable(axes)
    cax = divider.append_axes(location, size=size, pad=pad)
    return plt.colorbar(data, cax=cax, orientation=orientation)
    # return plt.colorbar(data, ax=axes, pad=0.1)
