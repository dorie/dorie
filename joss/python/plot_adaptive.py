from plot_cfg import *
from dorie.utilities.vtktools.vtkreader import PVDReader

import matplotlib.pyplot as plt
from matplotlib.collections import PolyCollection

import numpy as np

# Fix the seed to re-create the exact same image later on
np.random.seed(1)

# Plot settings
FILEPATH = "/home/lriedel/dune/simulations/joss/adaptive/output_richards.pvd"
FILES = [0, 23, 45]
DATASETS = ["K_0", "theta_w", "theta_w"]
CMAPS = [plt.cm.RdBu, plt.cm.Blues, plt.cm.Blues]
PLOT_FLUX = [False, True, True]
QUIVER_SCALE = [None, 4e-5, 9e-6]
Q_KEY_FLUX = [None, 5e-6, 1e-6]

print("Loading VTK files...")
richards = PVDReader(FILEPATH)

# Create the figure
fig, axes = plt.subplots(
    2,
    1,
    sharex=True,
    constrained_layout=True,
    figsize=(WIDTH_DEFAULT, 1.4 * HEIGHT_DEFAULT),
)

for ax, file_number, dataset_name, cmap, plot_flux, q_scale, q_flux in zip(
    axes, FILES, DATASETS, CMAPS, PLOT_FLUX, QUIVER_SCALE, Q_KEY_FLUX
):
    vtk_file = richards[file_number]
    print("Plotting file {}".format(vtk_file.filepath))
    dataset = vtk_file._dataset

    vertices = []  # List of list of cell vertices
    values = []  # Target quantity values at cell centers
    U = []  #  X-component of flux
    V = []  # Y-component of flux

    print("Iterating over cells...")
    cell_it = dataset.NewCellIterator()
    while not cell_it.IsDoneWithTraversal():

        # Fetch cell vertices
        points = cell_it.GetPoints()
        point_coords = []
        for index in range(points.GetNumberOfPoints()):
            point = points.GetPoint(index)
            point_coords.append(point[:2])
            flux = vtk_file["flux_RT0"].evaluate(point)
            U.append(flux[0])
            V.append(flux[1])
        vertices.append(point_coords)

        # Compute value at cell center
        center = np.mean(point_coords, axis=0)
        values.append(vtk_file[dataset_name].evaluate((center[0], center[1], 0.0)))

        # Advance iterator
        cell_it.GoToNextCell()

    # Draw polygons from vertices
    print("Plotting polygon collection...")
    poly = PolyCollection(vertices, cmap=cmap, edgecolors="grey", linewidths=0.3)

    # Logarithmic scaling for conductivity
    values = np.asarray(values)
    if dataset_name == "K_0":
        values = np.log10(values)

    # Set cell center values as color array
    poly.set_array(values)
    ax.add_collection(poly)

    # Axes aesthetics
    ax.set_xlim(left=0.0, right=3.0)
    ax.set_ylim(bottom=0.0, top=2.0)
    ax.set_aspect("equal")
    ax.get_yaxis().get_major_locator().set_params(nbins=6)
    ax.set_ylabel("Height [m]")

    # Add colorbar
    cb1 = append_colorbar(ax, poly)
    if dataset_name == "K_0":
        cb1.set_label(
            r"Saturated Conductivity $[\mathrm{m}\,\mathrm{s}^{-1}]$"
        )
        cb1.ax.get_yaxis().get_major_locator().set_params(nbins=5)
        cb1.update_ticks()
    else:
        cb1.set_label(r"Volumetric Water Content $[\mathrm{-}]$")

    if plot_flux:
        print("Plotting flux...")
        # Flatten list of vertices
        vertices = np.reshape(np.asarray(vertices), (-1, 2))
        U, V = np.asarray(U), np.asarray(V)

        # Select every 25th position with relative magnitude > 0.05
        # because lower magnitudes are just plotted as dots
        magnitude = np.sqrt(np.asarray(U) ** 2 + np.asarray(V) ** 2)
        ix_clamped = np.where(magnitude / np.max(magnitude) > 0.05)[0]
        ix = np.random.choice(ix_clamped, size=len(ix_clamped) // 25)

        # Create quiver plot
        q_key = ax.quiver(
            vertices[:, 0][ix],
            vertices[:, 1][ix],
            U[ix],
            V[ix],
            magnitude[ix] / np.max(magnitude[ix]),  # Color
            units="xy",
            angles="xy",
            scale=q_scale,
            scale_units="inches",
            pivot="middle",
            cmap=plt.cm.inferno,
        )

        # Compute size and color for quiver key
        q_key_scale = q_flux / np.max(magnitude[ix])
        q_key_length = q_key_scale * np.max(magnitude[ix])
        q_key_color = plt.cm.inferno(q_key_scale)
        ax.quiverkey(
            q_key,
            U=q_key_length,
            color=q_key_color,
            coordinates="axes",
            X=0.1,
            Y=0.95,
            labelpos="E",
            label=r"${:1d}\cdot 10^{{-6}}\,\mathrm{{m}}\,\mathrm{{s}}^{{-1}}$".format(
                int(q_flux * 1e6)
            ),
        )

        # Add time label
        ax.text(
            0.98,
            0.95,
            r"$t = {:.0f}\,\mathrm{{h}}$".format(vtk_file.time // 3600),
            horizontalalignment="right",
            verticalalignment="center",
            transform=ax.transAxes,
        )

# X-label only for last axes
ax.set_xlabel(r"Width [m]")

# Save figure
fig.savefig("../adaptive.pdf")
fig.savefig("../adaptive.png")
