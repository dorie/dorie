from plot_cfg import *
from dorie.utilities.vtktools.vtkreader import PVDReader

import matplotlib.pyplot as plt
from matplotlib.patches import FancyBboxPatch

import numpy as np

# Fix the seed to re-create the exact same image later on
np.random.seed(3)


def eval_dataset(dataset, return_points=False):
    """Evaluate a dataset at regular intervals and return the values. Optionally return
    the evaluation points as well."""
    xpoints = 300
    ypoints = 200
    xmin, xmax = dataset.bounds[0][0], dataset.bounds[0][1]
    ymin, ymax = dataset.bounds[1][0], dataset.bounds[1][1]

    offset_x = (xmax - xmin) / (2 * xpoints)
    offset_y = (ymax - ymin) / (2 * ypoints)
    x_values = np.linspace(xmin + offset_x, xmax - offset_x, xpoints)
    y_values = np.linspace(ymin + offset_y, ymax - offset_y, ypoints)
    z = 0.0

    data = np.squeeze(np.empty((ypoints, xpoints, dataset.number_of_components)))
    for kx in range(xpoints):
        for ky in range(ypoints):
            data[ky, kx] = dataset.evaluate([x_values[kx], y_values[ky], z])

    if return_points:
        return x_values, y_values, data
    else:
        return data


# Retrieve the data
path_richards = "/home/lriedel/dune/simulations/joss/solute/output_richards.pvd"
path_transport = "/home/lriedel/dune/simulations/joss/solute/output_transport.pvd"

print("Loading VTK data...")
richards = PVDReader(path_richards)
transport = PVDReader(path_transport)

print("Evaluating VTK data...")
data_wc = eval_dataset(richards[-1]["theta_w"])
x, y, data_flux = eval_dataset(richards[-1]["flux_RT0"], return_points=True)
# data_c_w = eval_dataset(transport[-1]["solute"])
data_c_t = eval_dataset(transport[-1]["total_solute"])
print(x.shape, y.shape)

print("Computing flux streamlines...")
u = data_flux[..., 0]
v = data_flux[..., 1]
magnitude = np.sqrt(u ** 2 + v ** 2).flatten()

print("Plotting data...")
fig, axes = plt.subplots(
    2,
    1,
    sharex=True,
    constrained_layout=True,
    figsize=(WIDTH_DEFAULT, 1.4 * HEIGHT_DEFAULT),
)

# Plot the water content
im0 = axes[0].imshow(
    np.flipud(data_wc),
    cmap=plt.cm.Blues,
    interpolation="none",
    extent=[0, 3, 0, 2],
    aspect="equal",
)

# Create a grid
xx, yy = np.meshgrid(x, y, indexing="xy")
xx, yy = xx.flatten(), yy.flatten()

# Drop values with relative magnitudes > 0.4 (outliers) and < 0.03 (just dots)
magnitude_normed = magnitude / np.max(magnitude)
ix_clamped = np.where((magnitude_normed > 0.03) & (magnitude_normed < 0.4))[0]

# Select every 25th value
ix = np.random.choice(ix_clamped, size=len(ix_clamped) // 25)
magnitude_normed = magnitude_normed[ix]

# Create quiverplot
q_key = axes[0].quiver(
    xx[ix],
    yy[ix],
    u.flat[ix],
    v.flat[ix],
    magnitude_normed,  # Color
    units="xy",
    angles="xy",
    pivot="middle",
    scale=1e-6,
    scale_units="inches",
    cmap=plt.cm.inferno,
)

# Add a box behind the quiver key
q_key_box = FancyBboxPatch(
    (0.03, 0.94),
    0.24,
    0.03,
    transform=axes[0].transAxes,
    boxstyle="Round, pad=0.02",
    facecolor="white",
    edgecolor="k",
    alpha=0.7,
)
axes[0].add_patch(q_key_box)

# Compute quiver key scale and color
Q_FLUX = 1e-7
q_key_scale = Q_FLUX / np.max(magnitude[ix])
q_key_length = q_key_scale * np.max(magnitude[ix])
q_key_color = plt.cm.inferno(q_key_scale)
axes[0].quiverkey(
    q_key,
    U=q_key_length,
    color=q_key_color,
    coordinates="axes",
    X=0.05,
    Y=0.95,
    labelpos="E",
    label=r"${:1d}\cdot 10^{{-7}}\,\mathrm{{m}}\,\mathrm{{s}}^{{-1}}$".format(
        int(Q_FLUX * 1e7)
    ),
)

# Create colorbar
cb0 = append_colorbar(axes[0], im0)
cb0.set_label(r"Volumetric Water Content $[\mathrm{-}]$")

# Plot total solute concentration
im1 = axes[1].imshow(
    np.flipud(data_c_t),
    cmap=plt.cm.viridis,
    interpolation="none",
    extent=[0, 3, 0, 2],
    aspect="equal",
)
cb1 = append_colorbar(axes[1], im1)
cb1.set_label(r"Total Solute Concentration $[\mathrm{kg}\,\mathrm{m}^{-3}]$")

# Plot solute concentration in water phase
# im2 = axes[2].imshow(
#     np.flipud(data_c_w),
#     cmap=plt.cm.viridis,
#     interpolation="none",
#     extent=[0, 3, 0, 2],
#     aspect="equal",
# )
# cb2 = append_colorbar(axes[2], im2)
# cb2.set_label(
#     r"Water Phase Solute Concentration $C_w\,/\,\mathrm{kg}\,\mathrm{m}^{-3}$"
# )

# Axes aesthetics
for ax in axes:
    ax.get_yaxis().get_major_locator().set_params(nbins=6)
    ax.set_ylabel(r"Height [m]")
axes[1].set_xlabel(r"Width [m]")

# Time label
for ax in axes[1:]:
    ax.text(
        0.02,
        0.95,
        r"$t = 58\,\mathrm{{d}}$",
        horizontalalignment="left",
        verticalalignment="center",
        transform=ax.transAxes,
        color="white",
    )

# Save figure
fig.savefig("../solute.pdf")
fig.savefig("../solute.png")
