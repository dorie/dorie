import numpy as np
from dorie.parfield.converter import BaseConverter
from dorie.utilities.text_to_bool import text_to_bool

class ExponentialConverter(BaseConverter):
    """Convert the random field by applying the natural exponential function.

    This converter takes values from a floating-point valued random field
    and applies the natural exponential function to it. If the original
    values follow a normal distribution, the resulting values will follow a
    log-normal distribution.
    """

    def __init__(self, param, input_file):
        super(ExponentialConverter, self).__init__(param, input_file)

    def _create_dataset(self):
        """Create the dataset which will be written into the target file.

        Apply ``exp`` to all values.

        Optionally, the field can be adjusted by the random field variance.
        If the resulting field is applied as Miller scaling field, and the
        original field was normally distributed, the resulting field will have
        the same macroscopic properties as an unscaled field, in the sense
        that average water flux is the same (Roth 1995).
        """
        if self._random_field is None:
            raise RuntimeError("Error reading the random field!")

        scale = text_to_bool(self._read_parameter("converter.exponential",
                                                  "varianceScaling"))
        if scale:
            variance = float(self._read_parameter("stochastic", "variance"))
        else:
            variance = 0.0

        self._dataset = np.exp(self._random_field - variance)
