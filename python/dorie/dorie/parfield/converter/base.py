import h5py
import os
import sys
import numpy as np

from dune.testtools.parser import parse_ini_file

from dorie.utilities.text_to_bool import text_to_bool

class BaseConverter(object):
    """
    Base class for all parameter field generator modules. Implements the writing
    of the parameter field to an H5 file, and supplies some utilities.

    Derived classes only need to implement the :meth:`_create_data` method, and call
    the base class constructor.

    """
    _cp = None #: RawConfigParser instance
    _random_field = None # The random field to be read in
    _dataset = None # The field to be written into the target file

    def __init__(self, param, input_file=None):
        self._cp = parse_ini_file(param)

        outfile = self._read_parameter('general', 'outputFile')
        dataset = self._read_parameter('general', 'dataset')
        self._read_random_field(input_file)
        self._create_dataset()
        self.write(outfile, dataset)
    
    def _read_parameter(self, section, key):
        """
        Reads a parameter from the configuration file.

        :Returns:
            The respective value as string.
        """
        try:
            return self._cp[section][key]
        except KeyError:
            raise RuntimeError("Missing option {}.{} in parameter file".format(section,key))
    
    def _read_random_field(self, input_file):
        """Read the random field that was created by the random field generator.
        """
        # read HDF5 file
        with h5py.File(input_file, 'r') as f:
            self._random_field = np.array(f.get("/stochastic"),
                                          dtype=np.float64)

    def _create_dataset(self):
        """Create the dataset which will be written into the target file.

        This default version creates a copy of the input random field.
        """
        if self._random_field is None:
            raise RuntimeError("Error copying the random field!")

        self._dataset = self._random_field

    def write(self, outfile, dataset):
        """Write the stored dataset into the output file.

        Use maximum compression.

        Args:
            outfile: The H5 output file to write the dataset into.
                This file may already exist!
            dataset: The name (internal full path) of the dataset to write.
        """
        if self._dataset is None:
            raise RuntimeError("Field to write was not defined!")

        with h5py.File(outfile, 'a') as f:
            if dataset in f:
                raise RuntimeError("Dataset {} already exists in file {}!"
                                   "".format(dataset, outfile))

            f.create_dataset(dataset,
                             data=self._dataset,
                             compression="gzip",
                             compression_opts=9)
