import numpy as np
from dorie.parfield.converter import BaseConverter

class BinaryConverter(BaseConverter):
    """Convert the random field into a binary field.

    This converter uses the random field consisting of floats
    to create a binary field, consisting of only two integer values.
    These values are specified in the config file.
    """

    def __init__(self, param, input_file):
        super(BinaryConverter, self).__init__(param, input_file)
    
    def _create_dataset(self):
        """Create the dataset which will be written into the target file.

        Convert the random field into the binary field.
        Use the primary index for a field value f <= 0, and the secondary
        index for a field value f > 0.
        """
        if self._random_field is None:
            raise RuntimeError("Error reading the random field!")
        
        indices = self._read_parameter('converter.binary', 'indices').split()
        if len(indices) != 2:
            raise RuntimeError("Specify exactly two indices for the binary"
                "converter! Found the following: '{}'".format(indices))
        
        index_a, index_b = int(indices[0]), int(indices[1])

        self._dataset = np.where(self._random_field > 0,
                                 index_b,
                                 index_a)
