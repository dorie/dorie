import argparse
import multiprocessing

# import all commands
from .cmds import *

def create_cl_parser():
    """Create the command line parser for the DORiE CLI.

    Returns:
        ArgumentParser object for parsing the command line.
    """
    parser = argparse.ArgumentParser(
        description="DORiE Command Line Interface\n"
                    "For more information check out our online documentation: "
                    "https://hermes.iup.uni-heidelberg.de/dorie_doc/master/html/",
        epilog="DORiE  Copyright (C) 2019  DORiE Developers\n"
               "This program comes with ABSOLUTELY NO WARRANTY.\n"
               "This is free software, and you are welcome to redistribute it\n"
               "under certain conditions; refer to the files LICENSE and\n"
               "COPYING.md in the source code repository for details.",
        formatter_class=argparse.RawDescriptionHelpFormatter
    )

    subparsers = parser.add_subparsers(title="Commands", dest="command")
    subparsers.required = True

    parser_create = subparsers.add_parser('create',
                                            description="Copy example configuration to current folder.",
                                            help="Copy example configuration to current folder.")
    parser_create.add_argument('-f','--force',
                               action="store_true",
                               help="Override existing files.")
    parser_create.set_defaults(func=create)

    parser_pfg = subparsers.add_parser('pfg',
        help="Start the random field generator.",
        description="This starts a lightweight wrapper around the "
             "dune-randomfield generator, along with a field converter."
        )
    parser_pfg.add_argument('config',
                            help="Configuration file for the parameter field generator. "
                                 "A default version can be created with 'dorie create'.")
    parser_pfg.add_argument('-p','--parallel',
                            metavar='N',
                            nargs='?',
                            default=1,
                            const=multiprocessing.cpu_count(),
                            type=int,
                            required=False,
                            help="Run in parallel on N processes. "
                                    "If N is not specified, run on all available CPU threads.")
    parser_pfg.add_argument('-m','--mpi-flags',
                            action="append",
                            required=False,
                            help="Additional flags that are passed to mpirun when run in parallel. "
                                    "May be specified multiple times.")
    parser_pfg.set_defaults(func=pfg)

    parser_run = subparsers.add_parser('run',
        help="Run the DORiE main routine.",
        description="Run the DORiE main routine.")
    parser_run.add_argument('config',
                            help="DORiE configuration file. "
                                 "A default version can be created with 'dorie create'.")
    parser_run.add_argument('-p','--parallel', metavar='N',
                            nargs='?',
                            default=1,
                            const=multiprocessing.cpu_count(),
                            type=int,
                            required=False,
                            help="Run in parallel on N processes. "
                                 "If N is not specified, run on all available CPU threads.")
    parser_run.add_argument('-m','--mpi-flags',
                            action="append",
                            required=False,
                            help="Additional flags that are passed to mpirun when run in parallel. "
                                    "May be specified multiple times.")
    parser_run.set_defaults(func=run)

    parser_plot = subparsers.add_parser('plot', help="Plot a preview of a VTK file created by DORiE.",
                                        description="Plot a preview of a VTK file created by DORiE.")
    parser_plot.add_argument('vtk',
                             help="Input VTK file.")
    parser_plot.add_argument('-v','--var',
                             nargs='*',
                             help="Plot only given variables. If not specified, all variables are plotted.")
    parser_plot.set_defaults(func=plot)

    return parser
