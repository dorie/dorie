def load(package,location):
    import os
    
    target_dir = os.listdir(os.path.dirname(os.path.realpath(location)))
    
    # import all .py scripts in the package folder
    for module in target_dir:
        if module.startswith("_") or not module.endswith(".py"):
            continue
        __import__(package+"."+module[:-3], locals(), globals())
