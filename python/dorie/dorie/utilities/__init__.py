from dorie.utilities._load_all_in_folder import load

"""
Import script to allow for statements of the form
    from dorie.utilities import grid
"""

load("dorie.utilities",__file__)
