def text_to_bool(string):
    string = str(string)
    if string.lower() in ["true","on","yes","1"]:
        return True
    elif string.lower() in ["false","off","no","0"]:
        return False
    else:
        raise ValueError("Cannot convert value to bool: {}".format(string))
