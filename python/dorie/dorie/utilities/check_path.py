import os

def check_path(filepath):
    """
    Convenience function that checks if a path exists and creates it if needed

    :param filepath: Path to a file or directory that is to be created

    """
    path = os.path.dirname(filepath)
    if path != '':
        if not os.path.exists(path):
            try:
                os.makedirs(path)
            except OSError:
                if os.path.exists(path):
                    pass
                else:
                    raise
