import os
import warnings
import xml.etree.ElementTree as ET
from collections.abc import Mapping, Sequence

from vtk import vtkXMLGenericDataObjectReader, \
                vtkCellTreeLocator, \
                vtkDataSet, \
                vtkGenericCell, \
                reference
import numpy as np

class VTKDataArray:
    """Wrapper for evaluating a VTK data array

    Attributes:
        bounds (:obj:`np.array`, shape ``(3, 2)``): Outer bounds of the grid
                                                    this data array lives on.
    """

    def __init__(self, dataset, locator, data_array, data_type):
        """Initialize this wrapper from a dataset, a cell locator,
        and the underlying VTK data array.

        Args:
            dataset: VTK dataset object the data array is stored in.
            locator: Locator for finding cells based on global coordinates.
            data_array: VTK data array containing the data to evaluate.
            data_type (str): Type of data stored in the array (``point`` or 
                             ``cell``).

        Raises:
            ValueError: Unsupported data type.
        """

        self._locator = locator

        self._data_array = data_array
        if data_type == "point":
            self._eval = self.__eval_vertex__
        elif data_type == "cell":
            self._eval = self.__eval_cell__
        else:
            raise ValueError("Unsupported VTK array data type: {}".format(
                data_type
            ))

        # store physical dataset bounds
        self.bounds = np.reshape(dataset.GetBounds(), (3, 2))

        # cache for cell and ID
        self._cell = vtkGenericCell()
        self._cell_id = -1 # invalid cell

    @property
    def number_of_components (self):
        """Number of components of the evaluated quantity."""
        return self._data_array.GetNumberOfComponents()

    def evaluate (self, position):
        """Evaluate the selected data array at a certain position.

        Args:
            position (list): 3D coordinate probing the data array.

        Returns:
            np.array: The value of the data array at the given position.

            The output is squeezed: Scalar data arrays return scalar values,
            vector arrays return numpy arrays with shape ``(3)``.

        Raises:
            ValueError: ``position`` argument not a 3D list or array.
            ValueError: Position outside grid.
            RuntimeError: Numerical error in cell locator algorithm.
        """
        if len(position) != 3:
            raise ValueError("Position must be a 3D lest/array")

        # unused stubs
        zeros = [0, 0, 0]
        zero_ref = reference(0)

        # interpolation weights
        weights = np.zeros(10)

        # check if we cached the correct cell
        on_cell = self._cell.EvaluatePosition(position,
                                              zeros,
                                              zero_ref,
                                              zeros,
                                              zero_ref,
                                              weights)

        if on_cell == 0 or self._cell_id < 0:
            # locate correct cell
            self._cell_id = self._locator.FindCell(position,
                                                   0,
                                                   self._cell,
                                                   zeros,
                                                   weights)

            if self._cell_id < 0:
                raise ValueError("Position outside domain: {}".format(position))

        elif on_cell < 0:
            raise RuntimeError("Error while evaluating if position {} is on "
                               "cell {}".format(position, self._cell_id))

        # evaluate the data
        data = self._eval(weights)
        return np.squeeze(data)

    def __eval_cell__ (self, weights):
        """Evaluation function for a cell-type data array."""
        return np.asarray(self._data_array.GetTuple(self._cell_id))

    def __eval_vertex__ (self, weights):
        """Evaluation function for a vertex-type data array."""
        data = np.zeros(self.number_of_components)
        for point in range(self._cell.GetNumberOfPoints()):
            point_id = self._cell.GetPointId(point)
            tup = np.asarray(self._data_array.GetTuple(point_id))
            data += weights[point] * tup

        return data

class VTKReader (Mapping):
    """A generic VTK reader for any type of VTK XML files.

    It maps the data array names to data array evaluators, see
    :py:class:`VTKDataArray`. Use it like any Python dict.

    Warning:
        VTK files separately store vertex (point) and cell data and associate
        the data arrays by their names. If the loaded file contains one array
        of each type with the same name, the *vertex (point)* array takes
        precedence. **The cell array cannot be accessed!** The key sequence
        will contain the name twice, but the associated value will be the
        vertex data set in both cases.

    Args:
        file_name (str): Name of the VTK file to open. Any VTK file in XML
                         format is supported.
        time (float, optional): Time associated with the VTK file. Defaults to ``None``.

    Attributes:
        filepath (os.path): The absolute path to the VTK file. Used for
                            comparing two instances of ``VTKReader``.
        time (float): The time associated with the VTK file, if any.
        _reader: Generic VTK XML data file reader.
        _dataset: The complete data set loaded by the reader.
        _locator: Fast locator of grid cells based on global coordinates.
        _point_data: Pointer to the VTK point data arrays.
        _cell_data: Pointer to the VTK cell data arrays.
    """
    time = None
    filepath = None

    def __init__(self, file_name, time=None):
        self.filepath = os.path.abspath(file_name)
        if not os.path.isfile(self.filepath):
            raise FileNotFoundError("File not found: {}".format(self.filepath))

        # set up the reader
        self._reader = vtkXMLGenericDataObjectReader()
        self._reader.SetFileName(self.filepath)
        self._reader.Update()
        self._dataset = vtkDataSet.SafeDownCast(self._reader.GetOutput())

        # build the locator
        self._locator = vtkCellTreeLocator()
        self._locator.SetDataSet(self._dataset)
        self._locator.BuildLocator()

        # ID of the current array
        self._point_data = self._dataset.GetPointData()
        self._cell_data = self._dataset.GetCellData()

        # Set time
        self.time = time

    @property
    def bounds(self):
        """:obj:`np.array`, shape ``(3, 2)``: The rectangular bounds of the
        dataset grid in any spatial direction. This value is shared among all
        :py:class:`VTKDataArray` objects retrieved from this instance.
        """
        return np.reshape(self._dataset.GetBounds(), (3, 2))

    def __eq__(self, other):
        """Compare to another VTKReader instance based on the VTK file path.

        Note:
            This does not check the stored times, meaning that two instances
            might compare equal in terms of the file path but may store
            different times.
        """
        return self.filepath == other.filepath

    def __len__(self):
        return self._point_data.GetNumberOfArrays() \
               + self._cell_data.GetNumberOfArrays()

    def __getitem__(self, key):
        if self._point_data.HasArray(key) == 1:
            return VTKDataArray(self._dataset,
                                self._locator,
                                self._point_data.GetArray(key),
                                "point")
        elif self._cell_data.HasArray(key) == 1:
            return VTKDataArray(self._dataset,
                                self._locator,
                                self._cell_data.GetArray(key),
                                "cell")
        else:
            raise KeyError("Data array not found: {}".format(key))

    def __iter__(self):
        for i in range(len(self)):
            yield self._get_array_name(i)

    def _get_array_name (self, array_id):
        """Return the name of the array at the given ID.

        The VTK data set stores separate IDs for cell and point data arrays.
        This method subtracts the number of point data arrays from the given
        array ID if it is larger than the number of point data arrays, to
        properly access cell data arrays.

        Parameters:
            array_id (int): Internal ID of the data array.

        Returns:
            str: The name of the array associated with the given ID.
        """
        if array_id >= len(self):
            raise ValueError("Array id '{}' out of bounds of dataset (size "
                             "is: {})".format(array_id, len(self)))

        num_point_arrays = self._point_data.GetNumberOfArrays()

        if array_id < num_point_arrays:
            data = self._point_data

        # start counting from zero again for cell arrays
        else:
            array_id = array_id - num_point_arrays
            data = self._cell_data

        return data.GetArrayName(array_id)


class PVDReader(Sequence):
    """This class reads a PVD file and stores the referenced VTK file paths and their
    respective time stamps. Use this object like a list of :py:class:`VTKReader`.

    The VTK files are guaranteed to be ordered with respect to time, regardless
    of their actual order in the PVD file.

    Note:
        To avoid excessive RAM loads, this class initializes :py:class:`VTKReader`
        instances *lazily*. They are only instantiated when they are actually requested
        by the user through an access operator. Users will have to keep/store
        :py:class:`VTKReader` instances if they want to switch between several files
        often.

    Args:
        file_path (str): Path to the PVD file to open.
        throw_on_missing (bool, optional): Throw immediately if a referenced file does
            not exist. Defaults to ``False``, in which case a warning is issued. If the
            referenced file does not exist, retrieving the respective
            :py:class:`VTKReader` will throw inevitably.

    Raises:
        FileNotFoundError: PVD file at ``file_path`` does not exist.
        IOError: PVD file has unknown XML structure.
        FileNotFoundError: VTK file referenced by the PVD file does not exist (only if
            ``throw_on_missing`` is ``True``).

    Attributes:
        _datasets (list): Tuples of time stamp and VTK file path.
    """

    def __init__(self, file_path, throw_on_missing=False):
        # Open the PVD file
        file_path = os.path.abspath(file_path)
        tree = ET.parse(file_path)
        root = tree.getroot()
        dirname = os.path.dirname(file_path)

        # Check structure
        if not root.get("type") == "Collection" \
                or root.get("version") != "0.1":
            raise IOError("Input PVD file must have type 'Collection' and "
                          "version 0.1")
        collection = root.find("Collection")

        # Load the files
        self._datasets = []
        for vtk_file in collection.findall("DataSet"):
            # PVD file contains local file paths
            vtk_file_path = os.path.join(dirname, vtk_file.get("file"))

            # Check if file exists
            if not os.path.isfile(vtk_file_path):
                vtk_file_abspath = os.path.abspath(vtk_file_path)
                msg = "Referenced VTK file does not exist: {}".format(vtk_file_abspath)
                if throw_on_missing:
                    raise FileNotFoundError(msg)
                else:
                    warnings.warn(msg, UserWarning)

            # Extract time
            time = float(vtk_file.get("timestep"))

            # Store the data
            self._datasets.append((time, vtk_file_path))

        # Sort the elements according to the time
        self._datasets.sort(key=lambda x: x[0])

    def __getitem__(self, index):
        """Create a :py:class:`VTKReader` instance for the respective VTK file and
        return it.
        """
        time, vtk_file_path = self._datasets[index]
        return VTKReader(vtk_file_path, time)

    def __len__(self):
        return len(self._datasets)

    @property
    def times(self):
        """:obj:`iterable` of :obj:`float`: The times associated with the
        stored VTK files.
        """
        for dataset in self._datasets:
            yield dataset[0]
