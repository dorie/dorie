import os

def grab_all(location):
    """
    Reads the file names of all files ending in .vtr and .vtu in a folder and
    returns them in a sorted list.

    :param location: folder to be searched

    """
    vtks = []
    target_dir = "{}/{}".format(os.path.dirname(os.path.realpath(location)),location)
    if not os.path.isdir(target_dir):
        raise RuntimeError("VTK directory {0} does not exist".format(target_dir))
    for f in os.listdir(target_dir):
        if f.endswith(".vtr") or f.endswith(".vtu"):
            vtks.append("{}/{}".format(target_dir,f))
    if not vtks:
        raise RuntimeError("No .vtr or .vtu files found in directory {0}".format(target_dir))
    return sorted(vtks)

def grab_first(location):
    """
    Returns the first .vtr or .vtu file found in a folder.

    :param location: folder to be searched

    """
    return grab_all(location)[0]

def grab_last(location):
    """
    Returns the last .vtr or .vtu file found in a folder.

    :param location: folder to be searched
    
    """
    return grab_all(location)[-1]
