import os
import xml.etree.ElementTree as ET
import warnings

from dorie.utilities.warnings import VTKWarning
from dorie.utilities.vtktools import vtkfile

def read(files):
    """
    Wrapper function that loops over the pvd files given and passes
    them along to the reader function :func:`_read_single_pvd`.

    :param files: Holds the file(s) to be read
    :type files: str or list of str

    """
    if isinstance(files,str):
        files = [files]

    values = []
    for f in files:
        if not isinstance(f,str):
            raise TypeError("First argument must either be string of list of strings")
        if not os.path.isfile(f):
            raise RuntimeError("File {0} not found".format(f))

        values.append(_read_single_pvd(f))
        
    if len(values) == 1:
        return values[0]
    else:
        return values


def _read_single_pvd(pvd):
    """
    Traverses the PVD XML tree root given in ``root``.

    Assumes the structure

    .. literal::

        <Collection> --> <DataSet timestep='...'>

    :param str pvd: Path to the PVD file

    .. note::

        This function only parses ASCII encoded PVD files.

    """
    error_base = "Error while reading PVD file {}:\n".format(pvd)
    
    try:
        xml_tree = ET.parse(pvd)
    except ET.ParseError:
        raise ET.ParseError(error_base + "Make sure the PVD "
                              "file is encoded in ASCII")
    xml_root = xml_tree.getroot()
    pvd_type = xml_root.attrib["type"]

    if pvd_type != "Collection":
        raise RuntimeError(error_base + "XML root attribute 'type' must "
                           "be 'Collection' (is: {})".format(root.attrib["type"]))
                    
    out = []       
    for collection in xml_root:
        for data in collection:
            if data.tag == "DataSet":
                if "file" in data.attrib:
                    f = data.attrib["file"]
                else:
                    raise RuntimeError(error_base + "Encountered DataSet without 'file' attribute")
                vtk = vtkfile.VTKFile(f)
                if "timestep" in data.attrib:
                    vtk.set_time(float(data.attrib["timestep"]))
                else:
                    raise RuntimeError(error_base + \
                            "Encountered DataSet without 'timestep' attribute")
                out.append(vtk)
            else:
                raise RuntimeError(error_base + "Unexpected tag: {0} (expected "
                                   "DataSet)".format(dataArray.tag))

    return out
