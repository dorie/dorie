from dorie.utilities._load_all_in_folder import load

"""
Import script to allow for statements of the form
    from dorie.utilities.vtktools import vtk_reader
"""

load("dorie.utilities.vtktools",__file__)
