import os
from scipy import interpolate, integrate
import numpy as np
import matplotlib as mpl
mpl.use("agg")
import matplotlib.pyplot as plt

from dorie.utilities.vtktools import pvd_reader
from dorie.testtools.utilities.decorators import evaluation_script
from dorie.testtools.utilities import muphi_reader
from dorie.utilities import grid
from dorie.utilities import fuzzy_compare_grid

@evaluation_script
def evaluate(iniinfo,runtime):
    pvd = iniinfo["richards.output.fileName"] + ".pvd"
    vtks = pvd_reader.read(pvd)
    vtk_file = vtks[-1]
    vtk_file.read(["head"])

    run_grid = vtk_file.grid
    run_cells = run_grid.cellCenters()
    run_head = run_grid.data()["head"]

    try:
        reference = iniinfo["_reference.path"] + "/" + iniinfo["richards.output.fileName"] + ".bin"
    except KeyError:
        raise ValueError("The muphi evaluator assumes the key _reference.path to be existent in the inifile")
    if os.path.isfile(reference):
        muphi_cells, muphi_head = muphi_reader.read(reference)
        muphi_grid = grid.RegularGrid(muphi_cells)
        muphi_grid.add_data("head",muphi_head)
    else:
        raise RuntimeError("Reference file {} does not exist".format(reference))

    if run_head.size != muphi_head.size:
        print("Encountered different number of cells")
        return False

    run_head = run_head.reshape(muphi_head.shape)
    diff = run_head - muphi_head
    plt.figure()
    im = plt.imshow(np.flipud(run_head))
    plt.colorbar(im)
    plt.savefig("dorie.png")
    plt.figure()
    im = plt.imshow(np.flipud(muphi_head))
    plt.colorbar(im)
    plt.savefig("muphi.png")
    plt.figure()
    im = plt.imshow(np.flipud(diff))
    plt.colorbar(im)
    plt.savefig("diff.png")

    l2 = np.sqrt(np.mean(np.square(diff))) # RMS error
    print("L2: {:.2e}".format(l2))
    meantol = float(iniinfo['_reference.meantol'])
    return bool(l2 < meantol)
