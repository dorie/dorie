import numpy as np

from dorie.utilities.vtktools import pvd_reader
from dorie.testtools.utilities.decorators import evaluation_script

@evaluation_script
def evaluate(iniinfo,runtime):
    """Verify that a certain scalar quantity in the output has a standard
       deviation below a given tolerance.
    """
    model = iniinfo["_model"]
    pvd = iniinfo["{}.output.fileName".format(model)] + ".pvd"
    vtks = pvd_reader.read(pvd)
    vtk_file = vtks[-1]

    # read data from the vtk file
    quantity_name = iniinfo["_quantity"]
    grid = vtk_file.read([quantity_name])
    cells = grid.cellCenters()
    y = cells[...,1].flatten()
    tri, connectivity = grid.triangulation()
    data = grid.data()
    iy = np.argsort(y)
    y = y[iy]

    # compute stddev
    average = lambda v: grid.integrate(v) / grid.integrate(1)
    stddev = lambda v: np.sqrt(grid.integrate((v - average(v))**2)
                               / grid.integrate(1))

    tol = float(iniinfo["_tol"])
    quantity = data[quantity_name].flatten()[iy]
    q_mean = average(quantity)
    q_std = stddev(quantity)

    print("{} – avrg: {}, stddev: {}".format(quantity_name, q_mean, q_std))
    return bool(q_std < tol)
