import os

from dorie.testtools.utilities.decorators import evaluation_script
from dorie.utilities.vtktools import pvd_reader
from dorie.utilities.vtktools import vtkfile
from dorie.utilities.text_to_bool import text_to_bool

from dorie.utilities import fuzzy_compare_grid


@evaluation_script
def evaluate(iniinfo,runtime):
    pvd = iniinfo["richards.output.fileName"] + ".pvd"
    vtks = pvd_reader.read(pvd)

    try:
        reference = iniinfo["_reference.path"] + "/" + iniinfo["richards.output.fileName"] + ".vtu"
    except KeyError:
        raise ValueError("The reference evaluator assumes the key _reference.path to be existent in the inifile")

    if not os.path.isfile(reference):
        try:
            reference_new = iniinfo["_reference.path"] + "/" + iniinfo["_reference.name"] + ".pvd"
        except KeyError:
            raise ValueError("Since there is not a single reference file found that matches the output filename, "
                "The reference evaluator assumes the keys _reference.path and _reference.name to be existent in the inifile")

        vtks_ref = pvd_reader.read(reference_new)
        reference = vtks_ref[-1].path

    vtkfile_run = vtkfile.VTKFile(vtks[-1].path)
    vtkfile_run.read()
    vtkfile_ref = vtkfile.VTKFile(reference)
    vtkfile_ref.read()

    print("Operating on files: {}, {}".format(vtks[-1].path,reference))

    # demand equal grids when not using adaptivity
    strict_compare = iniinfo["adaptivity.policy"] == "none"
    abstol = float(iniinfo["_abstol"]) if "_abstol" in iniinfo else None
    reltol = float(iniinfo["_reltol"]) if "_reltol" in iniinfo else None
    ret = fuzzy_compare_grid.compare(vtkfile_run.grid, vtkfile_ref.grid,
        abstol=abstol, reltol=reltol, strict=strict_compare)

    return ret
