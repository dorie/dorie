import os
import math
import numpy as np
from scipy import integrate
import matplotlib as mpl
mpl.use("agg")
import matplotlib.pyplot as plt

from dorie.utilities.vtktools import pvd_reader
from dorie.testtools.utilities.decorators import evaluation_script
from dorie.testtools.utilities.read_yml import read_yml, get_parameter_data
from dorie.testtools.utilities.read_h5 import read_h5_dataset
from dorie.testtools.utilities import richards_equation

@evaluation_script
def evaluate(iniinfo,runtime):
    pvd = iniinfo["richards.output.fileName"] + ".pvd"
    vtks = pvd_reader.read(pvd)
    vtk_file = vtks[-1]

    influx = float(iniinfo["_ode.flux"])
    initial_head = float(iniinfo["_ode.headLower"])
    extensions = list(map(float,iniinfo["grid.extensions"].split()))

    fe_order = int(iniinfo["richards.numerics.FEorder"])
    do_flux_rt = iniinfo["richards.fluxReconstruction.enable"]
    do_flux_grad = fe_order > 0
    is_stationary = (iniinfo["richards.initial.type"] == "stationary")
    if do_flux_rt:
        flux_rt_key = "flux_RT" + str(max(0,fe_order-1))

    # get parameter data
    param_data = read_yml(iniinfo["richards.parameters.file"])
    mapping_file = iniinfo["grid.mapping.file"]
    mapping_dataset = iniinfo["grid.mapping.volume"]

    # homogeneous setting
    if mapping_file in ["None", "none", ""]:
        medium_index = int(mapping_dataset)
        param = get_parameter_data(medium_index, volumes=param_data["volumes"])
        param_type = param["richards"]["type"]
        if param_type != "MvG":
            raise NotImplementedError("This testing function only supports "
                                      "Mualem van Genuchten parameterizations. "
                                      "Parameterization was: {}".format(
                                        param_type))

        parfun = lambda y: param["richards"]["parameters"]

    # 1D heterogeneity
    else:
        mapping = read_h5_dataset(mapping_file, mapping_dataset)

        # check mapping shape
        if np.any(np.array(mapping.shape[1:]) > 1):
            raise RuntimeError("ODE solutions only work for pseudo 1D cases, "
                               "with a single cell in horizontal directions.")

        def parfun(y):
            # cell index
            cell = math.floor(y * mapping.shape[0] / extensions[-1])
            index = mapping[cell, ...].flatten()
            param = get_parameter_data(index, volumes=param_data["volumes"])
            param_type = param["richards"]["type"]
            if param_type != "MvG":
                raise NotImplementedError("This testing function only supports "
                                      "Mualem van Genuchten parameterizations. "
                                      "Parameterization was: {}".format(
                                        param_type))
            return param["richards"]["parameters"]

    print("Operating on file: {}".format(vtk_file))

    variables = ["head", "flux", "K_0"]
    if do_flux_rt:
        variables.append(flux_rt_key)
    grid = vtk_file.read(variables)
    cells = grid.cellCenters()
    y = cells[...,1].flatten()
    tri, connectivity = grid.triangulation()
    data = grid.data()
    iy = np.argsort(y)
    iy_inv = np.arange(len(y))[iy]
    y = y[iy]

    k0_dorie = data["K_0"].flatten()[iy]
    k0_calc = np.array([parfun(yi)["k0"] for yi in y]).flatten()
    maxdiff = np.max(np.abs(k0_dorie-k0_calc))
    if maxdiff > 1e-10:
        print("Encountered unexpected parameter values. Maximum difference: {:.2e}".format(maxdiff))
        return False
    del k0_dorie
    del k0_calc

    r = integrate.ode(richards_equation.richards)
    r.set_integrator("dopri5", atol=1e-10, rtol=1e-10)
    r.set_initial_value(initial_head, 0)
    r.set_f_params(influx,parfun)

    r_y = []
    for t in y:
        if t - r.t > 1E-7: # ODE solver can't handle step sizes of ~ 0
            r.integrate(t)
        r_y.append(r.y[0])
    r_y = np.array(r_y)
    head = data["head"].flatten()[iy]
    res_head = r_y - head
    average = lambda v: grid.integrate(v) / grid.integrate(1)
    l2_head = np.sqrt(average(np.square(res_head[iy_inv])))
    print("head L2 error: {:.2e}".format(float(l2_head)))

    plt.figure()
    plt.plot(y,r_y,label="ODE solution")
    plt.plot(y,head,label="DORiE solution")
    plt.legend(loc="best")
    plt.savefig("{}/head_solutions.png".format(iniinfo["richards.output.outputPath"]))
    plt.close()

    plt.figure()
    plt.plot(y,res_head)
    plt.savefig("{}/head_residual.png".format(iniinfo["richards.output.outputPath"]))
    plt.close()

    # Check matric head
    head_tol = 1E-5 if not "_ode.head_abstol" in iniinfo else float(iniinfo["_ode.head_abstol"])    
    if is_stationary:
        head_tol = head_tol * float(iniinfo["_ode.head_eps_stat"])
    passed = bool(l2_head < head_tol)

    # Check flux from gradient
    if do_flux_grad:
        flux = data["flux"][iy,:]
        res_flux = flux - np.array([0.,influx,0.])[np.newaxis,:]
        l2_flux = [np.sqrt(average(np.square(r[iy_inv]))) for r in res_flux.T]
        print("flux L2 errors: {:.2e}, {:.2e}, {:.2e}".format(*l2_flux))

        plt.figure()
        plt.plot(y,res_flux)
        plt.savefig("{}/flux_residual.png".format(iniinfo["richards.output.outputPath"]))
        plt.close()

        flux_tol = abs(influx) * 1e-5 if not "_ode.flux_abstol" in iniinfo else float(iniinfo["_ode.flux_abstol"])
        if is_stationary:
            flux_tol = flux_tol * float(iniinfo["_ode.flux_eps_stat"])
        passed = passed and all([l < flux_tol for l in l2_flux])

    # Check reconstructed flux
    if do_flux_rt:
        flux_rt = data[flux_rt_key][iy,:]
        res_flux_rt = flux_rt - np.array([0.,influx,0.])[np.newaxis,:]
        l2_flux_rt = [np.sqrt(average(np.square(r[iy_inv]))) for r in res_flux_rt.T]
        print(flux_rt_key + " L2 errors: {:.2e}, {:.2e}, {:.2e}".format(*l2_flux_rt))
        
        plt.figure()
        plt.plot(y,res_flux_rt)
        plt.savefig("{}/flux_rt_residual.png".format(iniinfo["richards.output.outputPath"]))
        plt.close()

        flux_rt_tol = 1e-13 if not "_ode.flux_rt_abstol" in iniinfo else float(iniinfo["_ode.flux_rt_abstol"])
        if is_stationary:
            flux_rt_tol = flux_rt_tol * float(iniinfo["_ode.flux_rt_eps_stat"])

        passed = passed and all([l < flux_rt_tol for l in l2_flux_rt])

    return passed
