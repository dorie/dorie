import sys
import os
import subprocess
import inspect
import time
import importlib

def test_dorie(iniinfo,inifile,executable):
    """
    Parses the parameter file, calls DORiE, calls the evaluation routines and
    exits with 0 (pass) or 1 (fail).

    You can specify which command of the dorie wrapper to test with the `_test_command` key (e.g. `run`
    for `dorie run`). Options to the dorie wrapper can be specified with `_test_command_options`.
    The last argument given to the dorie wrapper is specified with `_test_target` (defaults to the ini file).
    """
    command = [executable]
    if not "_test_command" in iniinfo:
        raise ValueError("Key _test_command has to be given")
    test_command = iniinfo["_test_command"]
    try:
        evaluation = importlib.import_module("dorie.testtools.dorie_" + test_command)
    except ImportError:
        raise ValueError("Unknown test command " + test_command)
    command.append(test_command)
    if not "_test_target" in iniinfo:
        command.append(inifile)
    elif iniinfo["_test_target"] != "None":
        command.append(iniinfo["_test_target"])
    if "_test_command_options" in iniinfo and iniinfo["_test_command_options"] != "None":
        [command.append(k) for k in iniinfo["_test_command_options"].split()]
    print("Calling dorie with: " + " ".join(command))

    # Run DORiE and measure total execution time
    start = time.perf_counter()
    return_code = subprocess.call(command)
    end = time.perf_counter()
    runtime = end - start

    if return_code == 1 or not "_evaluation" in iniinfo:
        return return_code

    evaluation_tasks = [x.strip() for x in iniinfo["_evaluation"].split(",")]
    evaluation_scripts = inspect.getmembers(evaluation, inspect.ismodule)

    return_codes = []
    for task in evaluation_tasks:
        task_module = None
        for script_name, script_module in evaluation_scripts:
            if task == script_name and hasattr(script_module,"evaluate"):
                if script_module.evaluate._isEvaluator: # check if script is decorated with @evaluation_script
                    task_module = script_module
        if task_module is None:
            raise ImportError("Evaluation script for evaluation task {0} was not found"\
                            .format(task))
        print("Running {} evaluator".format(task))
        return_code = task_module.evaluate(iniinfo,runtime)
        if return_code == 0:
            print(" - passed\n")
        else:
            print(" - failed with code {}\n".format(return_code))
        return_codes.append(return_code)

    if any(r != 0 for r in return_codes):
        print("At least one evaluation task failed!")
        print("--- test failed ---")
        return 1
    else:
        print("--- test passed ---")
        return 0
