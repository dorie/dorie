def evaluation_script(func):
    def decorated_evaluator(*args,**kwargs):
        return_code = func(*args,**kwargs)
        if return_code is True:
            return_code = 0
        elif return_code is False:
            return_code = 1
        if isinstance(return_code,int):
            return_code = int(return_code)
        else:
            raise TypeError("An evaluation script must return an integer or bool"
                            "(got: {} {})".format(type(return_code),return_code))
        return return_code
    decorated_evaluator._isEvaluator = True
    return decorated_evaluator
