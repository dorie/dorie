import numpy as np
import struct


def read(filename):
    """
    Functions that reads and parses a binary file created by muPhi. Specification
    according to muPhi docs:

    - three 32-bit integers encoding number of grid cells per dimension

    - three single-precision floats encoding grid extensions in each dimension

    - array data as double precision floats

    Returns the array data and a meshgrid (assumes that the grid is regularly spaced).

    :param str filename: path to the muPhi binary (.bin) file
    :raises RuntimeError: if the input file is not readable
    :returns: grid cells, data array
    :rtype: (list of numpy arrays, numpy array)

    """
    try:
        f = open(filename,"rb")
    except IOError:
        raise RuntimeError("File {} cannot be read".format(filename))

    # muPhi binaries start with 3 integers and 3 floats encoding number of cells and grid extensions
    header_spec = "iiifff"
    skip_header = struct.calcsize(header_spec)
    header_raw = f.read(skip_header)
    header = struct.unpack(header_spec,header_raw)
    cells = [c for c in header[:3] if c > 1]
    extensions = [h for h in header[3:] if h != 0]

    # read array data with numpy (double-precision floats)
    data = np.fromfile(f, dtype=np.double).reshape(cells).T
    f.close()

    # assemble grid
    x = [np.arange(.5*e/float(c),e,e/float(c)) for e,c in zip(extensions,cells)]
    grid = np.dstack(np.meshgrid(*x))
    return grid, data
