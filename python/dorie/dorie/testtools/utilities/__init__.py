from dorie.utilities._load_all_in_folder import load

"""
Import script to allow for statements of the form
    from dorie.testtools.utilities import statistics
"""

load("dorie.testtools.utilities",__file__)
