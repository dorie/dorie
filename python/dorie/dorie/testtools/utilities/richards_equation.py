import numpy as np

def k(h,k0,tau,alpha,n,**kwargs):
	"""
	Returns the conductivity :math:`K` as given by the Mualem-van Genuchten parameterization:

	.. math::

		K = K_0 (1 + (\\alpha h)^n)^{1-1/n} (1 - (1 - \\frac{1}{(1+(\\alpha h)^n)^{1-1/n}})^2)

	:param float h: Matric head
	:param float K0: Saturated hydraulic conductivity
	:param float tau: :math:`\\tau`
	:param float alpha: :math:`\\alpha`
	:param float n: :math:`n`

	"""
	return k0*np.power(1+np.power(alpha*h,n),-tau*(1-1/n))*np.power(1-np.power(1-1/(1+np.power(alpha*h,n)),1-1/n),2)

def richards(y,h,jw,param):
	"""
	Returns :math:`du/dy` as given by the Richards equation in a 1-dimensional medium with constant inflow:

	.. math::

		\\frac{\\partial u}{\\partial y} = -1 - j_w/a

	:param float y: Current depth
	:param float h: Matric head
	:param float jw: Inflow at the top of the domain
	:param callable param: Function that accepts y and returns material parameters as a dict

	"""
	return -1-jw/k(h,**param(y))

def van_genuchten(h,alpha,n,theta_r,theta_s):
	"""
	Returns the water content :math:`\\Theta` as given by van Genuchten:

	.. math::

		\\Theta = (1 + (\\alpha h)^n)^{1/n - 1} (\\theta_s - \\theta_r) + \\theta_r

	:param float h: Matric head
	:param float alpha: :math:`\\alpha`
	:param float n: :math:`n`
	:param float theta_r: :math:`\\theta_r`
	:param float theta_s: :math:`\\theta_s`

	"""
	return np.power(1+np.power(alpha*h,n),1/n-1)*(theta_s-theta_r)+theta_r
