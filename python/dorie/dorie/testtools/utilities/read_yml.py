import os
import yaml

def read_yml (filepath):
    """Read a YAML file and return its entire contents as dict.

    :param filepath: Path to the YAML data file.
    
    :returns:
        A dict containing all data from the YAML file.
    """

    with open(filepath, 'r') as stream:
        data = yaml.load(stream, Loader=yaml.CLoader)

    return data

def get_parameter_data (index, *, filepath=None, volumes=None):
    """Parse a dict-like data tree and return the parameterization data with
    the correct index. Either ``filepath`` or ``volumes``have to be given.

    :param index: The mapping index of the target parameterization.
    :param filepath: Path to a YAML parameter file.
    :param volumes: A dictionary of parameterization data.
        Can be retrieved by opening a parameter file and accessing the entry
        ``volumes``.
    
    :returns:
        A dict containing the target parameterization data.
    """

    if filepath is not None:
        if volumes is not None:
            raise SyntaxError("Specify either \"volumes\" or \"filepath\", "
                               "not both!")
        
        volumes = read_yaml(filepath)["volumes"]

    for data in volumes.values():
        if data["index"] == index:
            return data
    
    raise RuntimeError("Index {} could not be found!".format(index))
