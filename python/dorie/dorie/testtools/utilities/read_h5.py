import os
import h5py
import numpy as np

def read_h5_dataset (filepath, dataset):
    """Read an H5 dataset from a given file.

    :param filepath: Path to the H5 file.
    :param dataset: Name (and internal path) of the dataset to read

    :returns:
        H5 dataset as numpy array.
    """
    with h5py.File(filepath, "r") as f:
        if not dataset in f:
            raise RuntimeError("Dataset {} not found in file {}".format(
                dataset, filepath
            ))
        
        dataset = f[dataset]
        return np.array(dataset)