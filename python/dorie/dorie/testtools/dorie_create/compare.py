import filecmp

from dorie.testtools.utilities.decorators import evaluation_script

@evaluation_script
def evaluate(iniinfo,runtime):
    fileset_1 = iniinfo["_compare.created_files"].split()
    fileset_2 = iniinfo["_compare.reference_files"].split()
    for f1,f2 in zip(fileset_1,fileset_2):
        if not filecmp.cmp(f1,f2):
            print("File contents of {} and {} do not match".format(f1,f2))
            return False
    return True
