from dorie.utilities._load_all_in_folder import load

"""
Import script to allow for statements of the form
    from dorie.testtools.dorie_run import ode
"""

load("dorie.testtools.dorie_create",__file__)
