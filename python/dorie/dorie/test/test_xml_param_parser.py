import pytest
import numpy as np

from collections import OrderedDict

from dorie.parscraper.readers import xml
from dorie.parscraper.parameter import Parameter

XML_FILE = "param_defaults.xml"
CATEGORY = "cat1"
PARAM_1 = "param1"
PARAM_2 = "param2"

# Fixtures ---------------------------------------------------------------
@pytest.fixture
def xml_param ():
    """Parse the XML file and return the parameter instances"""
    return xml.parse(XML_FILE)

@pytest.fixture
def param_1():
    """The resulting parameter instance of 'param1' to test against"""
    param = Parameter(None, CATEGORY, PARAM_1)
    param.definition = "Definition"
    param.suggestion = "0"
    param.values = "0, 1"
    param.comment = "comment"
    param.hidden = False
    param.versionadded = {"1.0": ""}
    param.versionchanged = {"1.1": "change", "1.2": "change2"}
    # NOTE: Newlines are removed
    param.deprecated = {"2.0": "deprecated deprecated"}

    return param

@pytest.fixture
def param_2():
    """The resulting parameter instance of 'param2' to test against"""
    param = Parameter(None, CATEGORY, PARAM_2)
    # NOTE: Internal newlines persist
    param.definition = "Definition\nDefinition"
    param.suggestion = "2"
    param.values = "2"
    param.comment = None
    param.hidden = True
    param.versionadded = {}
    param.versionchanged = {}
    param.deprecated = {}

    return param

# Tests ------------------------------------------------------------------

def test_parse(xml_param, param_1, param_2):
    """Check if the parser returns the correct instances"""
    assert isinstance(xml_param, OrderedDict)
    assert CATEGORY in xml_param

    print(xml_param[CATEGORY][0])
    print(param_1)
    assert param_1 == xml_param[CATEGORY][0]
    assert param_2 == xml_param[CATEGORY][1]
