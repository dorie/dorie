import pytest
import numpy as np

from ..utilities.vtktools.vtkreader import VTKReader, VTKDataArray, PVDReader

VTK_CELL = "vtkreader-cell.vtu"
VTK_VERTEX = "vtkreader-vertex.vtu"
PVD_FILE = "vtkreader.pvd"
PVD_FILE_WRONG = "wrong-link.pvd"
DEFAULT_ARRAY = "head"
ARRAYS = ["head", "flux", "K_0", "Theta", "theta_w"]

# Fixtures ---------------------------------------------------------------
@pytest.fixture
def vtk_cell ():
    """Create a VTKReader for the cell VTK test file"""
    return VTKReader(VTK_CELL)

@pytest.fixture
def vtk_vertex ():
    """Create a VTKReader for the vertex VTK test file"""
    return VTKReader(VTK_VERTEX)


@pytest.fixture
def pvd_reader():
    return PVDReader(PVD_FILE)

# Tests ------------------------------------------------------------------

def test_init_vtkreader ():
    """Test if initialization works"""
    reader = VTKReader(VTK_CELL)
    isinstance(reader[DEFAULT_ARRAY], VTKDataArray)

    reader = VTKReader(VTK_VERTEX, 1.0)
    isinstance(reader.get(DEFAULT_ARRAY), VTKDataArray)
    assert reader.time == 1.0

    with pytest.raises(FileNotFoundError):
        VTKReader("does-not-exist.vtu")

def test_dict_properties (vtk_cell, vtk_vertex):
    """Test if all included arrays are found"""
    assert len(vtk_cell) == len(ARRAYS)

    for array in ARRAYS:
        assert array in vtk_cell
        assert array in vtk_vertex.keys()

    for key in vtk_cell:
        assert key in ARRAYS

    for key, array in vtk_cell.items():
        assert key in ARRAYS

    assert not "other" in vtk_cell
    with pytest.raises(KeyError):
        array = vtk_vertex["other"]

def test_array_properties (vtk_cell):
    """Test the VTKArray properties"""
    array = vtk_cell[DEFAULT_ARRAY]
    assert array.number_of_components == 1
    assert np.allclose(array.bounds, [[0, 1],
                                      [0, 1],
                                      [0, 0]])
    assert np.allclose(vtk_cell.bounds, array.bounds)

    assert vtk_cell["flux"].number_of_components == 3

def test_evaluate (vtk_cell, vtk_vertex):
    """Test the evaluation function"""
    array_cell = vtk_cell[DEFAULT_ARRAY]
    array_vertex = vtk_vertex[DEFAULT_ARRAY]
    center = np.mean(array_cell.bounds, axis=1)

    # should return scalar
    with pytest.raises(TypeError):
        len(array_cell.evaluate(center))

    # check return of vector
    array_vertex = vtk_vertex["flux"]
    assert len(array_vertex.evaluate(center)) == 3

    # check values are equal
    for array in ["head", "K_0", "flux"]:
        array_cell = vtk_cell[array]
        array_vertex = vtk_vertex[array]
        assert np.allclose(array_cell.evaluate(center),
                           array_vertex.evaluate(center))

    # check evaluation errors
    # position has wrong shape
    with pytest.raises(ValueError):
        array_vertex.evaluate([0.0, 0.0])

    # position out of bounds
    pos = array_vertex.bounds[:, 1]
    pos = pos + [1.0, 0.0, 1.0]
    with pytest.raises(ValueError):
        array_vertex.evaluate(pos)


def test_pvd_reader(pvd_reader, vtk_cell, vtk_vertex):
    # Check sorted times via property
    times_real = [0.0, 1.0, 1.2]
    times_read = list(pvd_reader.times)
    assert times_read == times_real

    # Check sorted times via access of the VTKReaders
    for vtk, time in zip(pvd_reader, times_real):
        assert vtk.time == time

    # Check if the VTKReaders point to the same files
    assert pvd_reader[0] == vtk_cell
    assert pvd_reader[1] == vtk_cell
    assert pvd_reader[2] == vtk_vertex


def test_init_pvd_reader():
    with pytest.raises(FileNotFoundError):
        PVDReader("does-not-exist.pvd")

    with pytest.warns(UserWarning):
        reader = PVDReader(PVD_FILE_WRONG)

    assert len(reader) == 1
    assert len(list(reader.times)) == 1
    assert next(iter(reader.times)) == 100

    with pytest.raises(FileNotFoundError):
        reader[0]

    with pytest.raises(FileNotFoundError):
        reader = PVDReader(PVD_FILE_WRONG, throw_on_missing=True)
