from __future__ import unicode_literals

import datetime
import os
import re
import textwrap
from distutils.version import LooseVersion

try:
    str = unicode
except NameError:
    pass

from dorie.utilities.check_path import check_path
from dorie.parscraper.parameter import Parameter

def write(parameters,out,path_base,*args,**kwargs):
    """Writes the contents of the ``parameters`` input dict to a .rst file. The
    output is structured as

        Category: ``<category>``

        .. object:: <parameter>

           <description>

           .. deprecated: <ver>
              [description]

           .. versionchanged: <ver>
              [description]

           .. versionadded: <ver>
              [description]

           Possible values | <values>
           Default value   | <suggestion>
           Queried at      | <file path (if available)>

    The description obeys paragraph breaks in the description and properly
    formats list indentations.

    Args:
        parameters (dict): Keys: category, Values: iterables of
            :class:`dorie.parscraper.parameter.Parameter` instances.
        out (str): Path to the output file. Must be writable.
        path_base (str): Base path to the source files. Required for working
            links to the files.
    """

    check_path(out)
    with open(out,"wb") as output:
        rst_document = _format_rst(parameters, path_base)
        output.write(rst_document.encode('utf-8'))

def _format_rst(parameters, path_base):
    rst = u""
    for category in parameters:
        rst += _format_heading(category)
        for param in parameters[category]:
            rst += _format_parameter(param, path_base)
    return rst

def _format_parameter(parameter, path_base):
    tab = "   " # Three spaces to match with 'object' indentation

    # Headline
    out = u""
    out += ".. object:: {}\n\n".format(parameter.key)

    # Description
    # Double newlines are paragraph splits
    merged_paragraphs = _merge_lines(parameter.definition.strip().splitlines())
    for paragraph in merged_paragraphs:
        indent = "  " if paragraph.startswith('*') \
                      or paragraph.startswith('-') \
                 else ""
        for line in textwrap.wrap(paragraph, subsequent_indent=indent):
            out += tab + line + "\n"
        out += "\n"
    out += "\n"

    # Changes by version
    def write_version_attr(param, attr_name):
        data_dict = getattr(param, attr_name)
        versions_sorted = sorted(data_dict.keys(),
                                 key=LooseVersion,
                                 reverse=True)
        nonlocal out
        for version in versions_sorted:
            out += tab + ".. {0}:: {1}\n".format(attr_name, version)
            description = data_dict[version]
            if description:
                out += tab + tab + description + "\n"
            out += "\n"

    write_version_attr(parameter, "deprecated")
    write_version_attr(parameter, "versionchanged")
    write_version_attr(parameter, "versionadded")

    # Table
    out += tab + ".. list-table::\n"
    out += tab + tab + ":widths: auto\n\n"
    out += tab + tab + "* - *Possible values*\n"
    out += tab + tab + "  - {}\n".format(parameter.values)
    out += tab + tab + "* - *Default value*\n"
    out += tab + tab + "  - {}\n".format(parameter.suggestion)

    # State sources
    out += tab + tab + "* - *Queried at*\n"
    out += tab + tab + "  -"
    source_list = _sources(parameter, path_base)
    for line in source_list:
        out += " | " + line + "\n" + tab + tab + "   "
    if not source_list:
        # Do not delete last line (contains empty column)
        out += "\n\n"

    # remove final line
    out = out[:out.rfind('\n')]

    out += "\n\n\n"
    return out

def _format_heading(text):
    text = "Category: ``[{}]``".format(text)
    sep = "+"*len(text)
    return text + "\n" + sep + "\n\n"

def _sources(p,path_base):
    """
    Assembles the cell text for the sources of a parameter. Since the
    Parameter._sources attribute is a list of tuples, we need to parse this
    into a printable RST format. Prints a link to each source file, the
    corresponding line number, and concatenates them with newlines.

    :Returns:
        List of unicode strings, each containing one line of source file link
        without indentation or newline characters.

    :param p: Parameter object.
    """
    out = []
    for source_file, line_num, var_type in p._sources:
        full_path = os.path.join(path_base,source_file)

        # truncate long file names
        if len(source_file) > 40:
            link_text = "..." + source_file[-40:]
        else:
            link_text = source_file

        out.append(u"`{0}#L{1} <file://{2}>`_".format(link_text,
                                                      line_num,
                                                      full_path))
    return out

def _merge_lines(lines):
    """Merges lines which are not separated by an empty line and returns
    the resulting list.

    Args:
        lines (list): List of strings, each representing one line.

    Returns:
        list: List of strings, each representing one line.
    """
    result = [""]

    for line in lines:
        if line:
            result[-1] = " ".join((result[-1], line))
        else:
            result.append("")

    return [line.strip() for line in result if line]
