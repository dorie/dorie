from __future__ import unicode_literals

import datetime
import os
import re

from dorie.utilities.check_path import check_path
from dorie.parscraper.parameter import Parameter

def write(parameters,out,*args,**kwargs):
    """
    Writes the contents of the ``parameters`` input dict to a .ini file. The output is
    structured as

    ::

        [key]
        # value.comment
        value.key = value.suggestion

    to match the format of the DUNE input files. A missing comment attribute leads
    to omitting of the comment line; a missing suggestion to the value ``UNDEFINED``.

    :param dict parameters: dict with categories as keys and iterables of Parameter\
    instances as values.
    :param str out: Path to the output file. Must be writable.
    
    """
    check_path(out)
    with open(out,"w") as f:
        f.write("# Parameter file created by the DORiE parameter scraper\n")
        f.write("# {0:%Y-%m-%d, %H:%M}\n\n".format(datetime.datetime.today()))
        f.write("# Consult the \"Configuration File Guide\" in the User Manual"
                "\n# for more information on the settings in this file\n\n")
        for category in parameters:
            f.write("\n[{0}]\n".format(category))
            for p in parameters[category]:
                if not isinstance(p,Parameter):
                    raise TypeError("Input dict must contain a iterables of Parameter instances")
                if p.comment:
                    comment_oneline = p.comment.replace("\n"," ")
                    f.write("# {0}\n".format(comment_oneline))
                if p.suggestion:
                    f.write("{0} = {1}\n".format(p.key,p.suggestion))
                else:
                    f.write("{0} = NONE\n".format(p.key))
