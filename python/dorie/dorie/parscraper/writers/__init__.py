from dorie.utilities._load_all_in_folder import load

"""
Import script to allow for statements of the form
    from dorie.parscraper.writers import html
"""

load("dorie.parscraper.writers",__file__)
