from dorie.utilities._load_all_in_folder import load

"""
Import script to allow for statements of the form
    from dorie.parscraper.readers import xml
"""

load("dorie.parscraper.readers",__file__)
