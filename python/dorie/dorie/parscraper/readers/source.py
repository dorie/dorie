import re
import os

# COMPILE SOME REGEX PATTERNS ONCE FOR FASTER ACCESS

# match lines that are commented with //
comment_pattern = re.compile(r"^\s*//")

# match the inifile.get<type>("category.test.parameter",default) code lines
# group 1: type; group 2: "; group 3: category.test.parameter; group 4: default
get_pattern = re.compile(r'\w+\.*\.get\s*<\s*(.*?)\s*>\s*\(\s*([\'\"])(.+?)\2\s*(,\w+?)?\)')

# match lines that end in // %HIDDEN%
hide_pattern = re.compile(r'\/\/\s*\%\s*HIDDEN\s*\%\s*$')

def parse(filename,model=None):
    """
    Reads a source file and looks for patterns like

    .. code-block:: c

        inifile.get<type>("category.test.parameter",default)

    When found, the relevant information gets scraped and assembled in a dict.

    .. note::
	
	You can prevent a parameter to be matched by adding ``// %HIDDEN%`` to \
	the end of the corresponding line.

    :param str filename: Full path to the input file
    :return: list of dicts (one dict for each matching pattern in the source file), \
    each containing all found information

    """
    scraped_gets = []

    with open(filename) as f:
        parameter_done = True
        for linenum, line in enumerate(f):
            if not comment_pattern.match(line) and not hide_pattern.search(line):
                re_get_matches = get_pattern.findall(line)
                for re_get_match in re_get_matches:
                    name = re_get_match[2].split(".")
                    if model:
                        category = model + "." + ".".join(name[:-1])
                        filename = os.path.join(model, filename)
                    else:
                        category = ".".join(name[:-1])
                    key = name[-1]
                    scraped_gets.append(
                        {"category": category, "key": key, "type": re_get_match[0],
                        "file": filename, "line": linenum}
                     )

    return scraped_gets
