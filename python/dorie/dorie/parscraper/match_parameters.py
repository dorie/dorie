from __future__ import absolute_import

import os
from warnings import warn

from dorie.parscraper.warnings import ScraperWarning


def match(xml_parameters,source_parameters,base_path):
    """
    Expects the output of :mod:`dorie.parscraper.readers.xml` and 
    :func:`dorie.parscraper.readers.source` as input,
    and tries to match the parameters with each other. Records, *where* the parameters
    specified in the XML file are found in the code (file name, line number, and
    which type they are cast to), and supplements the Parameter instances with this
    information.

    Returns an OrderedDict with categories as keys and lists of Parameter instances
    as values. Parameters that are only found in the source files, but are not
    specified in XML, are not included in the output. Parameters that are only
    specified in the XML file but not in the code are returned, but a warning is
    issued.

    :param xml_parameters: Output of :func:`dorie.parscraper.readers.xml.parse`
    :param source_parameters: Output of :func:`dorie.parscraper.readers.source.parse`
    
    """
    for get in source_parameters:
        try:
            category_parameters = xml_parameters[get["category"]]
        except KeyError:
            warn("{0}.{1} queried in {2}:{3}, but not found in xml file"\
                    .format(get["category"],get["key"],get["file"],get["line"]),ScraperWarning)
            continue
        category_keys = [x.key for x in category_parameters]
        try:
            par_pos = category_keys.index(get["key"])
        except ValueError:
            warn("{0}.{1} queried in {2}:{3}, but not found in xml file"\
                    .format(get["category"],get["key"],get["file"],get["line"]),ScraperWarning)
            continue

        par = category_parameters[par_pos]
        par._code_found = True
        # we are only interested in paths relative to the parent source folder
        par._sources.append((get["file"], get["line"], get["type"]))

    for category in xml_parameters:
        for unmatched in [x for x in xml_parameters[category] if not x._code_found and not x.hidden]:
            warn("{0}.{1} specified in xml file but not found in sources"\
                        .format(unmatched.category,unmatched.key),ScraperWarning)

    return xml_parameters
