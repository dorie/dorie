import os
import sys
import warnings
import argparse
import traceback
from collections import OrderedDict
from yaml import dump, unsafe_load

from dorie.parscraper import readers, writers, match_parameters
from dorie.parscraper.warnings import OutputWarning
from dorie.parscraper.parameter import Parameter

def scrape(xml,
           source,
           out,
           overwrite=False,
           model=None,
           debug=False):
    # PARSE XML FILE
    if os.path.isfile(xml):
        xml_parameters = readers.xml.parse(xml,model)
    else:
        raise IOError("XML file {0} does not exist".format(xml))

    # CHECK SOURCE FOLDER
    if not source.endswith("/"):
        source += "/"

    if not os.path.isdir(source):
        raise IOError("Source folder {0} does not exist".format(source))

    # ITERATE OVER ALL .cc AND .hh FILES IN SOURCE FOLDER, AND CALL SOURCE SCRAPER
    source_parameters = []
    for subdir, dirs, files in os.walk(source):
        for f in files:
            if f.endswith(".cc") or f.endswith(".hh"):
                full_path = os.path.join(subdir, f)
                source_parameters.append(readers.source.parse(full_path,model))

    source_parameters = [item for sublist in source_parameters for item in sublist] # flatten list

    # MATCH XML AND SCRAPED PARAMETERS
    matched_parameters = match_parameters.match(xml_parameters,source_parameters,source)

    # WRITE YAML DATA FILE
    if not overwrite:
        # concatenate file contents and current data
        with open(out, 'r') as file:
            data = unsafe_load(file)
            data.update(matched_parameters)
            matched_parameters = data

    with open(out, 'w') as file:
        file.write(dump(matched_parameters, default_flow_style=False))
