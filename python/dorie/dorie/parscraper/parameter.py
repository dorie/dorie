class Parameter:
    """
    Implements a class for storing DORiE parameters.

    :param str category: The category of the parameter, indicated in the DUNE infiles \
    by square brackets.
    :param str key: The name of the parameter, at the left hand side of the equal sign \
    in DUNE inifiles.

    """

    def __init__(self,model,category,key):
        self.key = key #: Parameter name
        self.category = category #: Parameter category
        self.model = model #: Model category
        self.definition = None #: Long explanation of the parameter, shown in the documentation
        self.suggestion = None #: Suggested (default) value
        self.values = None #: Possible values
        self.comment = None #: Extra comment showing up in the generated ini files
        self.hidden = False #: Do not issue an error if parameter is not found
        self.versionadded = {} #: Versions and descriptions on feature added
        self.versionchanged = {} #: Versions and descriptions on feature changed
        self.deprecated = {} #: Versions and descriptions on feature deprecated

        #: List of all the positions in the code where the parameter was found,
        #: encoded in a tuple: (filename, lineno, C++ type)
        self._sources = []
        self._code_found = False #: Whether the parameter was found in the code at all

    def __str__(self):
        """
        Allows for nice output of a Parameter when printed.
        """
        string = ""
        if (self.model):
            string += "{0}.{1}.{2}\n".format(self.model,self.category,self.key)
        else:
            string += "{0}.{1}\n".format(self.category,self.key)
        string += "Definition: {0}\n".format(self.definition)
        string += "Suggested value: {0}\n".format(self.suggestion)
        string += "Allowed values: {0}\n".format(self.values)
        string += "Comment: {0}\n".format(self.comment)
        string += "Hidden: {}\n".format("yes" if self.hidden else "no")
        string += self.__write_version_dict(self.versionadded, "Version added")
        string += self.__write_version_dict(self.versionchanged, "Version changed")
        string += self.__write_version_dict(self.deprecated, "Deprecated")
        string += "Found at:\n"
        for f, l, t in self._sources:
            string += "\t{0}:{1} as {2}\n".format(f,l,t)
        return string

    def __write_version_dict(self, data, heading):
        if not data:
            return ""

        out = heading + ":\n"
        for ver, descr in data.items():
            out += "  v{}".format(ver)
            if descr:
                out += ": " + descr
            out += "\n"

        return out

    def __eq__(self, other):
        """Compare two parameters by all their attributes"""
        if isinstance(other, Parameter):
            return self.__dict__ == other.__dict__
        else:
            return False
