#!/usr/bin/env python3

from setuptools import setup, find_packages
    
setup(name='dorie',
    version='0.1',
    description='Python package providing the DORiE parameter scraper',
    author='Lukas Riedel',
    author_email='dorieteam@iup.uni-heidelberg.de',
    url='https://ts-gitlab.iup.uni-heidelberg.de/dorie/dorie',
    packages=find_packages(exclude=["dorie.test"]),
    test_suite='py.test',
    install_requires= ['cycler',
                    'configparser',
                    'h5py',
                    'matplotlib',
                    'numpy',
                    'Pillow',
                    'pygmsh',
                    'pyparsing',
                    'pytest',
                    'python-dateutil',
                    'pytz',
                    'pyyaml',
                    'scipy',
                    'six',
                    'vtk',
                    'wheel'
    ],
    scripts=[
        'wrapper/dorie',
        'wrapper/pf_from_file.py',
        'wrapper/plot_vtk.py',
        'wrapper/scrape_folder.py',
        'wrapper/test_dorie.py',
        'wrapper/write_config.py'
    ]
)
