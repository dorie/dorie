#!/usr/bin/env python3

from __future__ import absolute_import

import os
import warnings
import argparse
from collections import OrderedDict
from yaml import unsafe_load

from dorie.parscraper import writers
from dorie.parscraper.parameter import Parameter
from dorie.parscraper.warnings import OutputWarning

def write(data, args):
    # CALL OUTPUT SCRIPTS
    for out in args.output:
        file_suffix = out.split(".")[-1]
        if hasattr(writers, file_suffix):
            writer = getattr(writers, file_suffix)
            writer.write(data,
                         out,
                         args.source,
                         css=args.css)
        else:
            warnings.warn("Unknown output format: .{}. Skipping output".format(file_suffix), OutputWarning)

def read_data(args):
    with open(args.input, 'r') as file:
        return unsafe_load(file)

def parse_args():
    parser = argparse.ArgumentParser(
        description="Load the YAML file containing all scraped parameter "
                    "data and write this data into other formats.\n"
                    "Supported formats are INI, HTML, RST.")
    parser.add_argument("input", type=os.path.realpath,
                        help="YAML file containing all parameter data")
    parser.add_argument("output", type=os.path.realpath, nargs='+',
                        help="Output files to write data to.")
    parser.add_argument("-s", "--source", type=os.path.realpath, 
                        required=True,
                        help="Top level path to the source files. "
                             "Required for appropriate file links.")
    parser.add_argument("-c", "--css", required=False,
                        help="Path to a CSS file to include into HTML output")
    parser.add_argument("--debug", action="store_true", required=False,
                        help="Display warnings")
    return parser.parse_args()

if __name__ == "__main__":
    args = parse_args()
    data = read_data(args)
    write(data, args)
