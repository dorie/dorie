#!/usr/bin/env python3

from __future__ import absolute_import

import os
import warnings
import argparse

from dorie.utilities.vtktools import vtkfile
from dorie.utilities.vtktools import pvd_reader
from dorie.utilities import check_path

"""
Script invoking the vtk plotter on a file or glob pattern given via command line.
"""

if __name__ == "__main__":
    try: # catch all exceptions we we can output an error message
        with warnings.catch_warnings(record=True) as warn: # catch all warnings so we can count them
            # PARSE COMMAND LINE
            parser = argparse.ArgumentParser()
            parser.add_argument('-f','--file',help="Input PVD file, VTK file or glob pattern",required=True,nargs="*")
            parser.add_argument('--var',help='The variables to plot, plots all if not given',required=False,nargs="*")
            parser.add_argument('--Wno',help='Hide warnings',action='store_false',required=False)
            parser.add_argument('-o','--out',help='Path to output directory',required=False)
            args = vars(parser.parse_args())

            files = args["file"]
            variables = args["var"]
            if variables == []:
                variables = None

            out = "." if args["out"] is None else args["out"]
            check_path.check_path(out)

            # CALL WRAPPER
            for f in files:
                if f.endswith("pvd"):
                    print("Reading PVD file {}".format(f))
                    vtks = pvd_reader.read(f)
                else:
                    print("Reading VTK file {}".format(f))
                    vtks = [vtkfile.VTKFile(f)]

                for vtk in vtks:
                    print("  Plotting {}...".format(vtk))
                    vtk.read(variables=variables)
                    if variables is None:
                        variables = list(vtk.grid.data().keys())
                    figures = vtk.plot(variables=variables)
                    for v,fig in zip(variables,figures):
                        fig.savefig("{}/{}-{}.png".format(out,vtk.filename,v))

            # HANDLE WARNINGS
            debug = args["Wno"]

            if debug:
                for w in warn:
                    print("  {0}: {1}".format(w.category.__name__,w.message))
            print("-- VTK plotter exited with ({0}) warning(s) and ({1}) error(s)\n".format(len(warn),0))
    except:
        print("-- VTK plotter failed with ({0}) warning(s) and ({1}) error(s)\n".format(len(warn),1))
        raise
