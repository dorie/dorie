#!/usr/bin/env python3

from __future__ import absolute_import

import sys
import os
import argparse

from dune.testtools.parser import parse_ini_file
from dorie.testtools.wrapper import test_dorie

DORIE_WRAPPER = "dorie"


def get_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--ini', help='The inifile', required=True)
    args, _ = parser.parse_known_args()
    return args


if __name__ == "__main__":
    args = get_args()
    iniargument = args.ini
    iniinfo = parse_ini_file(args.ini)
    sys.exit(test_dorie.test_dorie(iniinfo, iniargument, DORIE_WRAPPER))
