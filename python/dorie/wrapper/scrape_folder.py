#!/usr/bin/env python3

from __future__ import absolute_import

import os
import warnings
import argparse

from dorie.parscraper.wrapper import scrape_folder

"""
Script invoking the parameter scraper on a folder given via command line.
"""

if __name__ == "__main__":
    try: # catch all exceptions so we can output an error message
        with warnings.catch_warnings(record=True) as warn: # catch all warnings so we can count them
            # PARSE COMMAND LINE
            parser = argparse.ArgumentParser()
            parser.add_argument('-x','--xml',help="The XML file holding parameter meta-information",required=True)
            parser.add_argument('-s','--source',help='The C++ source folder to scrape',required=True)
            parser.add_argument('-o','--out',help='YAML output file',required=True)
            parser.add_argument('--overwrite', action="store_true",
                                help="Overwrite existing output file. "
                                     "Append otherwise.")
            parser.add_argument('-m','--model',help='Prefix of the parsed model',required=False)
            parser.add_argument('--debug',help='Display warnings',action='store_true',required=False)
            args = vars(parser.parse_args())

            # CALL WRAPPER
            scrape_folder.scrape(**args)

            # HANDLE WARNINGS
            if not "debug" in args:
                debug = False
            else:
                debug = args["debug"]

            if debug:
                for w in warn:
                    print("  {0}: {1}".format(w.category.__name__,w.message))
            print("-- Parameter scraper exited with ({0}) warning(s) and ({1}) error(s)\n".format(len(warn),0))
    except:
        print("-- Parameter scraper failed with ({0}) warning(s) and ({1}) error(s):\n".format(len(warn),1))
        raise
