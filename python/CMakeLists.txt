# install Python package
add_subdirectory(dorie/wrapper)
add_subdirectory(dorie/dorie/cli)
dune_python_install_package(PATH dorie)

# add Python tests
dune_python_add_test(NAME python_tests
                     COMMAND ${CMAKE_BINARY_DIR}/run-in-dune-env
                             python -m pytest -v
                             ${CMAKE_CURRENT_SOURCE_DIR}/dorie/dorie/test/
                     WORKING_DIRECTORY
                        ${CMAKE_CURRENT_SOURCE_DIR}/dorie/dorie/test/
)
