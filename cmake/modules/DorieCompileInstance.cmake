# Default Dorie model targets
add_custom_target(richards)
add_custom_target(transport)
add_custom_target(dorie ALL)
add_dependencies(dorie richards transport)

# Maximum polynomial orders of Richards model for available targets
set(DORIE_MAX_RORDER_2 6)
set(DORIE_MAX_RORDER_3 6)

# Maximum polynomial orders of transport model for available targets
set(DORIE_MAX_TORDER_2 3)
set(DORIE_MAX_TORDER_3 3)

# Maximum polynomial orders of Richards model for default targets
set(DORIE_MAX_DEFAULT_RORDER_2 3)
set(DORIE_MAX_DEFAULT_RORDER_3 1)

# Maximum polynomial orders of transport model for default targets
set(DORIE_MAX_DEFAULT_TORDER_2 3)
set(DORIE_MAX_DEFAULT_TORDER_3 1)

#
# .. cmake_function:: dorie_compile_instance
#
# Adds an executable and library for the specified model.
#
# The parameters specify static settings for the model instance. If these
# settings comply to the limits of the default variables, the instance is
# added to the global "richards" or "transport" targets, depending on which
# MODEL type is built.
#
# In case of "transport", the appropriate "richards" library must be available.
# Otherwise, it is also defined by this function.
#
# A sanity check for the input variables is not performed by CMake, but by
# the C++ code during compile-time.
#
# This function takes the following arguments:
#
#   - MODEL: Name of the model. Accepts "richards" or "transport".
#   - DIMENSION: Spatial dimension.
#   - RORDER: Finite element polynomial order of the Richards module.
#   - TORDER: Finite element polynomial order of the Transport module.
#   - CREATED_LIB: Variable to store the created library target name in.
#
function(dorie_compile_instance)

    # parse the function arguments
    set(SINGLE_ARGS MODEL DIMENSION RORDER TORDER CREATED_LIB)
    cmake_parse_arguments(ARGS "" "${SINGLE_ARGS}" "" ${ARGN})

    if (ARGS_UNPARSED_ARGUMENTS)
        message(WARNING "Unparsed arguments when calling "
                        "'dorie_create_executable: "
                        "${ARGS_UNPARSED_ARGUMENTS}")
    endif ()

    # set dimension string
    set(DIM_STR "d${ARGS_DIMENSION}")

    # set option string
    set(OPTION_STR "r${ARGS_RORDER}")

    # issue warning if transport order is given for 'richards'
    if (ARGS_MODEL STREQUAL "richards")
        set (lib_src ${PROJECT_SOURCE_DIR}/dune/dorie/model/richards/impl/impl.cc)
        if (ARGS_TORDER)
            message(WARNING "Ignoring argument TORDER for MODEL "
                            "'richards'")
        endif ()
    # append transport order to option string
    elseif (ARGS_MODEL STREQUAL "transport")
        set (lib_src ${PROJECT_SOURCE_DIR}/dune/dorie/model/coupling/impl/impl.cc)
        string(APPEND OPTION_STR "_t${ARGS_TORDER}")
    # unknown model
    else ()
        message(SEND_ERROR "Unsupported model: ${ARGS_MODEL}. "
                           "Must be either 'richards' or 'transport'")
    endif ()

    # register the library
    set(lib_name "dorie_${ARGS_MODEL}_${DIM_STR}_${OPTION_STR}")
    if (NOT TARGET ${lib_name})
        add_library(${lib_name} EXCLUDE_FROM_ALL STATIC ${lib_src})

        # link to dependencies
        target_link_libraries(${lib_name}
            PUBLIC
                spdlog::spdlog
                muparser::muparser
                hdf5
                yaml-cpp
                ${DUNE_LIBS}
        )

        # Set the C++ standard as compile feature
        target_compile_features(${lib_name} PUBLIC cxx_std_17)

        # register the executable
        set(exe_name "${ARGS_MODEL}_${DIM_STR}_${OPTION_STR}")
        set(src_file ${CMAKE_SOURCE_DIR}/dune/dorie/${ARGS_MODEL}.cc)
        add_executable(${exe_name} EXCLUDE_FROM_ALL ${src_file})
        target_link_libraries(${exe_name} PUBLIC ${lib_name})

        # Coverage links if enabled
        if(COVERAGE_REPORT)
            target_compile_options(${exe_name} PUBLIC --coverage)
            target_link_libraries(${exe_name} PUBLIC --coverage)
        endif()

        # Handling of default targets
        if (ARGS_RORDER LESS_EQUAL DORIE_MAX_DEFAULT_RORDER_${ARGS_DIMENSION})
            if (ARGS_MODEL STREQUAL "richards"
                OR ((ARGS_TORDER LESS_EQUAL DORIE_MAX_DEFAULT_TORDER_${ARGS_DIMENSION})
                    AND (ARGS_RORDER EQUAL ARGS_TORDER)))
                # Make dependency of model meta-target
                add_dependencies(${ARGS_MODEL} ${exe_name})

                # Add installation instructions
                include(GNUInstallDirs)
                install(TARGETS ${exe_name}
                        RUNTIME DESTINATION ${CMAKE_INSTALL_BINDIR}
                        PERMISSIONS OWNER_READ OWNER_WRITE OWNER_EXECUTE
                                    GROUP_READ GROUP_EXECUTE
                                    WORLD_READ WORLD_EXECUTE)
            endif()
        endif()

        # set compile definitions
        target_compile_definitions(${lib_name}
            PUBLIC
                DORIE_DIM=${ARGS_DIMENSION}
                DORIE_RORDER=${ARGS_RORDER})

        if (ARGS_MODEL STREQUAL "transport")
            target_compile_definitions(${lib_name}
                PUBLIC DORIE_TORDER=${ARGS_TORDER})
        endif ()
    endif()

    # If we build a transport model, build the Richards library as well
    if (ARGS_MODEL STREQUAL "transport")
        dorie_compile_instance(MODEL "richards"
                               DIMENSION ${ARGS_DIMENSION}
                               RORDER ${ARGS_RORDER}
                               CREATED_LIB richards_lib
        )
        # ... and link to it!
        target_link_libraries(${exe_name} PUBLIC ${richards_lib})
    endif()

    # Report the library target name
    if (ARGS_CREATED_LIB)
        set(${ARGS_CREATED_LIB} ${lib_name} PARENT_SCOPE)
    endif ()

endfunction()
