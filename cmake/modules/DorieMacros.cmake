# --- DEPENDENCIES --- #
# These macros check for the following packages, yielding the respective
# targets
#
#   HDF5        ... hdf5
#   yaml-cpp    ... yaml-cpp
#   muparser    ... muparser::muparser
#
# All of these are required an will be autmatically linked to executables when
# using the DorieTesting CMake macros.

# Add CMake policy stack
cmake_policy(PUSH)

# Allow using `<Package>_ROOT` on CMake >3.12
if(POLICY CMP0074)
    cmake_policy(SET CMP0074 NEW)
endif()

# HDF5
set(HDF5_PREFER_PARALLEL TRUE)
find_package (HDF5 REQUIRED COMPONENTS C)
if(NOT HDF5_IS_PARALLEL)
    message(SEND_ERROR "Parallel HDF5 must be installed!")
endif()

# Define a target because the CMake module does not
add_library(hdf5 INTERFACE IMPORTED GLOBAL)

# TODO: Use `target_XXX` functions after raising CMake requirements
# Sanitize the definitions because we cannot use the proper CMake function...
string(REPLACE -D "" HDF5_DEFINITIONS "${HDF5_DEFINITIONS}")
# Set properties directly because of a bug in CMake 3.10
set_target_properties(hdf5 PROPERTIES
    INTERFACE_LINK_LIBRARIES "${HDF5_LIBRARIES}"
    INTERFACE_INCLUDE_DIRECTORIES "${HDF5_INCLUDE_DIRS}"
    INTERFACE_COMPILE_DEFINITIONS "${HDF5_DEFINITIONS};H5_HAVE_PARALLEL")

# yaml-cpp
find_package (yaml-cpp 0.5.2 REQUIRED)

# muparser
find_package (muparser REQUIRED)

# spdlog
find_package(spdlog 1.0 REQUIRED)

# Report the DUNE libs
message (STATUS "DUNE Libraries: ${DUNE_LIBS}")

# Remove CMake policy stack
cmake_policy(POP)

# --- CMAKE MODULES --- #
# Include the CMake modules used in the project

include(DorieCompileInstance)

# Check if testing is enabled
if (dune-testtools_FOUND)
    message(STATUS "Testing enabled: dune-testtools found.")
    set(DORIE_TESTING TRUE)
    # include the DORiE testing macros
    include(DorieTesting)
else()
    message(STATUS "Testing disabled: dune-testtools not found.")
endif()
