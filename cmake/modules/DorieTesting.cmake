# Add the unit test build target and test command
add_custom_target(build_unit_tests)
add_custom_target(unit_tests
    COMMAND ctest --output-on-failure --tests-regex ^ut.+$
)

# Add the system test build target and test command
add_custom_target(build_system_tests)
add_custom_target(system_tests
    COMMAND ctest
            --output-on-failure --exclude-regex "^\\(ut.+\\|python_tests\\|dorie_example.+\\)$"
)

# Add target for manual examples
add_custom_target(example_tests
    COMMAND ctest
            --output-on-failure --tests-regex ^dorie_example.+$
)

# create the mapping datafile as preparation for all tests
add_custom_target(prepare_testing
    COMMAND ${DUNE_PYTHON_VIRTUALENV_EXECUTABLE} ${PROJECT_SOURCE_DIR}/test/maps/create_param_maps.py ${PROJECT_SOURCE_DIR}/test/maps/cell_ids.h5
    COMMAND ${DUNE_PYTHON_VIRTUALENV_EXECUTABLE} ${PROJECT_SOURCE_DIR}/dune/dorie/test/create_scaling.py ${PROJECT_BINARY_DIR}/dune/dorie/test/scaling.h5
)
add_dependencies(system_tests prepare_testing)
add_dependencies(unit_tests prepare_testing)

# Create a fake library target to satisfy dune-testtools
add_library(dorie_test UNKNOWN IMPORTED)
add_library(dorie_example UNKNOWN IMPORTED)
set_target_properties(dorie_test dorie_example
    PROPERTIES IMPORTED_LOCATION ${PROJECT_BINARY_DIR}/activate)
#
# .. cmake_function:: add_coverage_links
#
#   This function adds the appropriate compiler and linker flags for creating
#   a coverage report from the resulting object files of the specified targets.
#   Signature:
#       `add_coverage_links(<target>...)`
#
function(add_coverage_links)
    foreach(target ${ARGV})
        target_compile_options(${target} PRIVATE --coverage)
        target_link_libraries(${target} --coverage)
    endforeach()
endfunction()

#
# .. cmake_function:: dorie_add_unit_test
#
#   .. cmake_param:: NAME
#       :single:
#       :required:
#
#       The name of the resulting test executable and target.
#
#   .. cmake_param:: TARGET
#       :single:
#
#       The target this test applies to. This is only required if no SOURCES
#       are specified.
#
#   .. cmake_param:: CUSTOM_MAIN
#       :option:
#
#       Write a custom `main()` function for the unit test executables instead
#       of generating a default one automatically.
#
#   This function serves as wrapper around the function `dune_add_test` which
#   registers test for existing targets or adds new test executables from the
#   given source files. This function additionally registers the tests as unit
#   tests within DORiE and adds flags for coverage reports. Notice that
#   `dune_add_test` requires more parameters than this function alone.
#
#   The registered executable is automatically linked to the libraries DORiE
#   DORiE depends on.
#
#   Use this function exactly like `dune_add_test`.
#
function(dorie_add_unit_test)
    set(SINGLE NAME TARGET)
    set(OPTION CUSTOM_MAIN)
    cmake_parse_arguments(UNIT_TEST "${OPTION}" "${SINGLE}" "" ${ARGN})

    # use name prefix for test
    if(NOT UNIT_TEST_NAME)
        message(SEND_ERROR "No unit test name specified!")
    endif()
    string(PREPEND UNIT_TEST_NAME ut-)

    # forward to dune function
    dune_add_test(NAME ${UNIT_TEST_NAME}
                  ${UNIT_TEST_UNPARSED_ARGUMENTS})

    # get (build) target name if no target was given
    if(NOT UNIT_TEST_TARGET)
        set(UNIT_TEST_TARGET ${UNIT_TEST_NAME})
    endif()

    # add to build target and employ compile options
    target_link_libraries(${UNIT_TEST_TARGET}
        muparser::muparser hdf5 yaml-cpp spdlog::spdlog)
    # add_coverage_links(${UNIT_TEST_TARGET})

    target_compile_definitions(${UNIT_TEST_TARGET}
        PUBLIC
            GTEST
    )

    if(COVERAGE_REPORT)
        add_coverage_links(${UNIT_TEST_TARGET})
    endif()

    if (UNIT_TEST_CUSTOM_MAIN)
        target_link_libraries(${UNIT_TEST_TARGET} gtest)
    else ()
        target_link_libraries(${UNIT_TEST_TARGET} gtest_main)
    endif()

    add_dependencies(build_unit_tests ${UNIT_TEST_TARGET})
endfunction()

#
# .. cmake_function:: dorie_add_metaini_test
#
#   .. cmake_param:: UNIT_TEST
#       :option:
#
#       Registers the created tests as unit tests, including coverage flags.
#       If not specified, the tests are registered as system tests.
#
#   .. cmake_param:: CUSTOM_MAIN
#       :option:
#
#       Write a custom `main()` function for the unit test executables instead
#       of generating a default one automatically. Only applies if UNIT_TEST
#       is enabled.
#
#   .. cmake_param:: TARGET
#       :single:
#
#       The existing target to apply these tests to. This is incompatible to
#       the option `UNIT_TEST` and the parameter `BASENAME`, because the base
#       name of the tests will automatically be set to the target name.
#
#   .. cmake_param:: METAINI
#       :single:
#       :required:
#
#       The meta-ini _input_ file for this test.
#
#   .. cmake_param:: SCRIPT
#       :single:
#
#       The Python script to call for this test. The script has to be installed
#       into the CMake `virtualenv`. Defaults to `dune_execute.py` for unit
#       tests and `test_dorie.py` for system tests.
#
#   .. cmake_param:: BASENAME
#       :single:
#
#       The basename for tests created from source files. This option is ignored
#       (by `dune_add_system_test`) if `TARGET` is specified.
#
#   .. cmake_param:: CREATED_TARGETS
#       :single:
#
#       The variable to hold the created CMake targets for post-processing.
#
#   This function serves as wrapper around the function `dune_add_system_test`
#   which registers test for existing targets or adds new test executables from
#   the given source files by expanding meta ini files.
#   This function can register the tests as unit or as system tests within
#   DORiE. Notice that `dune_add_system_test` requires more parameters
#   than this function alone.
#
#   Use this function like `dune_add_system_test`, considering the options
#   given above.
#
#   When registering a new executable (`TARGET` is not specified), this
#   executable will be linked to the libraries DORiE depends on.
#
function(dorie_add_metaini_test)
    set(OPTIONS UNIT_TEST CUSTOM_MAIN)
    set(SINGLE TARGET METAINI SCRIPT BASENAME CREATED_TARGETS)
    cmake_parse_arguments(SYSTEM_TEST "${OPTIONS}" "${SINGLE}" "" ${ARGN})

    if(NOT SYSTEM_TEST_METAINI)
        message(SEND_ERROR "No meta ini file given!")
    endif()

    if(SYSTEM_TEST_CUSTOM_MAIN AND NOT SYSTEM_TEST_UNIT_TEST)
        message(WARNING "Ignoring option CUSTOM_MAIN because option UNIT_TEST "
                        "was not enabled")
    endif()

    # configure meta ini file or just copy.
    get_filename_component(metaini-name ${SYSTEM_TEST_METAINI} NAME_WE)
    get_filename_component(metaini-extension ${SYSTEM_TEST_METAINI} EXT)
    if(metaini-extension STREQUAL ".mini.in")
        configure_file(${SYSTEM_TEST_METAINI} ${metaini-name}.mini)
        set(SYSTEM_TEST_METAINI "${metaini-name}.mini")
    else()
        configure_file(${SYSTEM_TEST_METAINI} ${SYSTEM_TEST_METAINI})
    endif()

    if(SYSTEM_TEST_TARGET AND SYSTEM_TEST_UNIT_TEST)
        message(SEND_ERROR "Specifying TARGET is incompatible to option UNIT_TEST!")
    endif()

    # Set at least the prefix as basename
    if(NOT SYSTEM_TEST_TARGET)
        if(SYSTEM_TEST_UNIT_TEST)
            string(PREPEND SYSTEM_TEST_BASENAME ut-)
        else()
            string(PREPEND SYSTEM_TEST_BASENAME st-)
        endif()
    endif()

    # default script for system tests
    if(NOT SYSTEM_TEST_SCRIPT)
        if(NOT SYSTEM_TEST_UNIT_TEST)
            set(SYSTEM_TEST_SCRIPT test_dorie.py)
        endif()
    endif()

    # forward to DUNE function
    dune_add_system_test(
        ${SYSTEM_TEST_UNPARSED_ARGUMENTS}
        TARGET ${SYSTEM_TEST_TARGET}
        BASENAME ${SYSTEM_TEST_BASENAME}
        INIFILE ${CMAKE_CURRENT_BINARY_DIR}/${SYSTEM_TEST_METAINI}
        SCRIPT ${SYSTEM_TEST_SCRIPT}
        CREATED_TARGETS created_targets
    )

    # report created targets to parent scope
    set(${SYSTEM_TEST_CREATED_TARGETS} ${created_targets} PARENT_SCOPE)

    # Set properties for new target
    if(NOT SYSTEM_TEST_TARGET)
        # Link to dependencies
        target_link_libraries(${created_targets}
            muparser::muparser hdf5 yaml-cpp spdlog::spdlog)

        # Add coverage flags if enabled
        if(COVERAGE_REPORT)
            add_coverage_links(${created_targets})
        endif()
    endif()

    # add dependencies and flags
    if(SYSTEM_TEST_UNIT_TEST)
        add_dependencies(build_unit_tests ${created_targets})

        target_compile_definitions(${created_targets}
            PUBLIC
                GTEST
        )

        if (SYSTEM_TEST_CUSTOM_MAIN)
            target_link_libraries(${created_targets} gtest)
        else ()
            target_link_libraries(${created_targets} gtest_main)
        endif()
    else()
        add_dependencies(build_system_tests ${created_targets})
    endif()
endfunction()
