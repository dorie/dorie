<!-- _Set the title to: "Release: X.Y.0" -->
<!-- Replace X.Y with the actual version numbers everywhere -->

We're rolling out version `X.Y.0`! :tada:

### 1 — In the Code

- [ ] [Create branch][new branch] `X.Y-release` from `master`.
- [ ] *(Insert MR here)*: Merge `X.Y-release` into `master`. Use the
   `pre-release` MR description template.

### 2 — On GitLab

- [ ] [Create branch][new branch] `X.Y-stable` from `master`
- [ ] [Create label][new label] ~"Pick into X.Y"
- [ ] [Create tag][new tag] `X.Y.0` from branch `X.Y-stable`
   - Message:

      ```
      Version X.Y.0 (YYYY-MM-DD)
      ```

   - Release Notes:
      ```
      # Version X.Y.0 (YYYY-MM-DD)

      -> Insert version changelog here! <-
      ```
      Shortcut to the most up-to-date [CHANGELOG.md][changelog]
- [ ] Update "Release" [project badge][badge] <!-- only if applicable -->

### 3 — In the Code

- [ ] *(Insert MR here)*: Merge `X.Y-release` into `master`. Use the
   `post-release` description template.

### 4 — On Docker Hub

- [ ] Update [description on DockerHub][DockerHub description]
- [ ] Push new `latest` tag to DockerHub <!-- only if applicable -->

### 5 — All done? :white_check_mark:

Close this issue!

/label ~Release

[new branch]: https://ts-gitlab.iup.uni-heidelberg.de/dorie/dorie/-/branches/new
[new tag]: https://ts-gitlab.iup.uni-heidelberg.de/dorie/dorie/-/tags/new
[changelog]: https://ts-gitlab.iup.uni-heidelberg.de/dorie/dorie/blob/master/CHANGELOG.md
[new label]: https://ts-gitlab.iup.uni-heidelberg.de/dorie/dorie/-/labels/new
[DockerHub description]: https://hub.docker.com/r/dorie/dorie
[badge]: https://ts-gitlab.iup.uni-heidelberg.de/dorie/dorie/edit
