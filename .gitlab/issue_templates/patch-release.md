<!-- _Set the title to: "Patch Release: X.Y.Z" -->
<!-- Replace X.Y.Z with the actual version numbers everywhere -->

We're releasing patch version `X.Y.Z` for branch `X.Y-stable`! :tada:

### 1 — List of MRs to be Included

List the MRs and the commit SHA of their respective merge commits here.
Placing them in chronological order here ensures fewer issues when
cherry-picking them!

The MRs are indicated by the ~"Pick into X.Y" label.

| MR | Merge Commit SHA |
| -- | ---------------- |
| !  |  ...  |

### 2 — On GitLab
Use the "patch-release" template for creating a new Merge Request.

- [ ] [Create a branch][new branch] `X.Y-patch` from `X.Y-stable`
- [ ] [Create a Merge Request][new mr] with source branch `X.Y-patch` and
   target branch `X.Y-stable`: !
- [ ] Merge this MR: !
- [ ] [Create tag][new tag] `X.Y.Z` from branch `X.Y-stable`
   - Message:

      ```
      Version X.Y.Z (YYYY-MM-DD)
      ```

   - Release Notes:
      ```
      # Version X.Y.Z (YYYY-MM-DD)

      -> Copy appropriate entries from MR changelog here <-
      ```
- [ ] Update "Release" [project badge][badge] <!-- only if applicable -->

### 3 — On Docker Hub
- [ ] Update [description on DockerHub][DockerHub description]
- [ ] Push new `latest` tag to DockerHub <!-- only if applicable -->

### 4 — All done? :white_check_mark:
Close this issue!

/label ~Release

[new branch]: https://ts-gitlab.iup.uni-heidelberg.de/dorie/dorie/-/branches/new
[new mr]: https://ts-gitlab.iup.uni-heidelberg.de/dorie/dorie/merge_requests/new
[new tag]: https://ts-gitlab.iup.uni-heidelberg.de/dorie/dorie/-/tags/new
[DockerHub description]: https://hub.docker.com/r/dorie/dorie
[badge]: https://ts-gitlab.iup.uni-heidelberg.de/dorie/dorie/edit
