<!-- _Set the title to: "Post Release: X.Y.0" -->
<!-- Replace X.Y with the actual version numbers everywhere -->

We're updating the repository after Release `X.Y.0` 🧹

### Release Issue
<!-- DO NOT use automatic Issue resolution here! -->
The release is coordinated in issue #

### Tasks

- [ ] Update version numbers in `VERSION` and `dune.module` to `X.Y+1-pre`
- [ ] Add new "Unreleased" section in `CHANGELOG.md`

### Can this MR be merged?

- [ ] Pipeline passing <!-- please check for new warnings -->
- [ ] Delete branch option set

### Related Issues
