<!-- _Set the title to: "Pre Release: X.Y.0" -->
<!-- Replace X.Y.0 with the actual version numbers everywhere -->

We're gearing up for Release `X.Y.0` 🎉

### Release Issue
<!-- DO NOT use automatic Issue resolution here! -->
The release is coordinated in issue #

### Tasks

- [ ] Update version numbers in `VERSION`, `CHANGELOG.md`, `dune.module` to
    `X.Y.0`.
- [ ] Update all `version="unreleased"` tags in the default config file XML
    sources to `version="X.Y.0"`.
- [ ] Update all  [version directives][sphinx-ver-dir] with version `unreleased`
    in the user docs to `X.Y.0`.
- [ ] Trigger re-build of Docker base image,
    see [`docker/README.md`][docker_readme]

### Can this MR be merged?

- [ ] Pipeline passing <!-- please check for new warnings -->
- [ ] Delete branch option **not set**

### Related Issues

[docker_readme]: https://ts-gitlab.iup.uni-heidelberg.de/dorie/dorie/blob/master/docker/README.md
[sphinx-ver-dir]: https://www.sphinx-doc.org/en/master/usage/restructuredtext/directives.html#directive-versionadded
