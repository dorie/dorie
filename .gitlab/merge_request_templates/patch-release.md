<!-- _Set the title to: "Resolve "Patch Release: X.Y.Z"" -->
<!-- Replace X.Y.Z with the actual version numbers everywhere -->

We're releasing patch version `X.Y.Z` for branch `X.Y-stable`! :tada:

### Release Issue
<!-- DO NOT use automatic Issue resolution here! -->
The MRs to be considered in this update are listed in #

### Tasks
- [ ] Cherry-pick the listed commits into `X.Y-patch`
- [ ] `CHANGELOG.md`: Move appropriate entries into new section
- [ ] Update version numbers in `VERSION`, `CHANGELOG.md`, `dune.module`
- [ ] Update `version="unreleased"` tags in the default config file XML
    sources <!-- only if applicable -->
- [ ] Update Sphinx version directives with version `unreleased` in the user
    docs. <!-- only if applicable -->

#### Help on Cherry-Picking
Cherry-picking merge commits requires specifying the "mainline" parent, which
should always be number 1. Append a line indicating a cherry-pick to the commit
message with the `-x` argument. The following should do the trick:
```bash
git cherry-pick -m 1 -x <commits>
```
Replace `<commits>` with the Commit SHAs listed above, separated by a single
whitespace each. Make sure they are in chronological order to reduce the
number of merge conflicts! Fix such conflicts as unintrusively as possible.

### Can this MR be merged?
- [ ] Pipeline passing <!-- please check for new warnings -->
- [ ] Delete branch option set <!-- unless there's a good reason -->

### Related Issues
