FROM ubuntu:focal
LABEL maintainer="lriedel@iup.uni-heidelberg.de"
# number of cores for parallel builds
ARG PROCNUM=1
# Compilers to be used
ARG CC=gcc
ARG CXX=g++

# Pass compilers as environment variables (will persist in other images)
ENV CC=$CC CXX=$CXX

# disable any prompts while installing packages
ENV DEBIAN_FRONTEND=noninteractive
RUN apt-get clean \
    && apt-get update \
    && apt-get install -y \
        clang \
        cmake \
        doxygen \
        gcc \
        g++ \
        gfortran \
        git \
        graphviz \
        libatlas-base-dev \
        libfftw3-dev \
        libfftw3-mpi-dev \
        libfreetype6-dev \
        libgl-dev \
        libgraphviz-dev \
        libhdf5-mpi-dev \
        libmetis-dev \
        libmuparser-dev \
        libopenmpi-dev \
        libpng-dev \
        libparmetis-dev \
        libspdlog-dev \
        libsuperlu-dev \
        libxft-dev \
        libyaml-cpp-dev \
        locales \
        python3-dev \
        python3-pip \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* \
    && localedef -i en_US -c -f UTF-8 -A /usr/share/locale/locale.alias en_US.UTF-8
ENV LANG en_US.utf8

WORKDIR /opt/dune

# Clone DUNE modules
COPY clone_dune ./
RUN bash clone_dune

# Configure and build DUNE modules
COPY build.opts ./
RUN MAKE_FLAGS="-j ${PROCNUM}" \
    ./dune-common/bin/dunecontrol --opts=build.opts all
