ARG BASE_IMG_UBUNTU_VERSION=focal
ARG BASE_IMG_VERSION=1
ARG DUNE_ENV_IMAGE=dorie/dune-env:${BASE_IMG_UBUNTU_VERSION}-v${BASE_IMG_VERSION}
FROM $DUNE_ENV_IMAGE

# start build image
FROM ${DUNE_ENV_IMAGE} as build-env

# maintainer info
LABEL maintainer="lriedel@iup.uni-heidelberg.de"

# number of cores for parallel builds
ARG PROCNUM=1

# copy the build context to this image
WORKDIR /opt/dune/dorie
COPY ./ ./

# Build and install DORiE
WORKDIR /opt/dune/
RUN MAKE_FLAGS="-j${PROCNUM}" \
    ./dune-common/bin/dunecontrol --opts=dorie/build.opts --only=dorie all \
    && ./dune-common/bin/dunecontrol --only=dorie make install

# Start a fresh image as production environment
FROM $DUNE_ENV_IMAGE as prod-env

# Copy binaries and Python venv
# NOTE: Python venv must remain in the same directory!
WORKDIR /opt/
COPY --from=build-env /opt/dune/dune-common/build-cmake/dune-env /opt/dune/dune-common/build-cmake/dune-env
COPY --from=build-env /opt/dune/dune-common/build-cmake/run-in-dune-env /opt/dune/dune-common/build-cmake
WORKDIR /opt/dorie
COPY --from=build-env /opt/dorie ./

# Tell CLI that we are using a DORiE installation
ENV DORIE_INSTALL_DIR /opt/dorie

# move to working directory
WORKDIR /mnt

# run bash in the virtualenv (this actually creates two bash instances...)
# TODO: create new user with restricted privileges
ENTRYPOINT [ "/opt/dune/dune-common/build-cmake/run-in-dune-env" ]
CMD ["/bin/bash" ]
