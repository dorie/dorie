ARG BASE_IMG_UBUNTU_VERSION=focal
ARG BASE_IMG_VERSION=1
ARG DUNE_ENV_IMAGE=dorie/dune-env:${BASE_IMG_UBUNTU_VERSION}-v${BASE_IMG_VERSION}
FROM $DUNE_ENV_IMAGE

LABEL maintainer="lriedel@iup.uni-heidelberg.de"

# number of cores for parallel builds
ARG PROCNUM=1

# Upgrade Ubuntu packages
# NOTE: "localedef" is required in case "locale" package is updated
RUN apt-get clean \
    && apt-get update \
    && apt-get upgrade -y \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* \
    && localedef -i en_US -c -f UTF-8 -A /usr/share/locale/locale.alias en_US.UTF-8
ENV LANG en_US.utf8

# Update and build DUNE modules
WORKDIR /opt/dune
COPY build.opts ./
RUN ./dune-common/bin/dunecontrol update
RUN MAKE_FLAGS="-j ${PROCNUM}" \
    ./dune-common/bin/dunecontrol --opts=build.opts all
