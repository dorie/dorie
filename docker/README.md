# DORiE Docker Images

DORiE uses [Docker](https://www.docker.com/) images for testing and deploying
applications. These images are publicly available from
[Docker Hub](https://hub.docker.com/). There are two repositories:

* [`dorie/dorie`](https://hub.docker.com/r/dorie/dorie/): The DORiE application.
* [`dorie/dune-env`](https://hub.docker.com/r/dorie/dune-env/): The DUNE
    environment image.

`dune-env` is used for compiling DORiE in a 'clean' Ubuntu environment where all
dependencies are already installed. The final DORiE application image is the
DUNE image with DORiE installed.

We differentiate between two base images, one where DUNE is compiled with GCC,
and one where it is compiled with Clang.

The tags of `dune-env` indicate the version of the base Ubuntu image followed
by the version number of the image configuration. This number is increased
whenever the image changes.

## GitLab CI configuration

Both images are updated by the GitLab CI/CD Pipeline defined in
[`.gitlab-ci.yml`](../gitlab-ci.yml).

The `setup` build stage is builds a new DUNE environment image "from scratch".
The `prep` stage updates all APT and DUNE packages in the DUNE environment image
and is run whenever a pipeline includes the deployment of a new application
image. Both stages are automatically triggered if the respective Dockerfiles
are changed. The `prep` stage is additionally executed for every push to
the `master` branch or a tag.

The `deploy` stage deploys DORiE application images. It compiles DORiE into the
DUNE environment image. This stage is only run if tags are pushed or commits
are pushed to `master`.
