# Copyright Notice

    DORiE – The DUNE Operated Richards Equation Solving Environment
    Copyright (C) 2019  DORiE Developers

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

A copy of the [GNU General Public License Version 3](https://www.gnu.org/licenses/gpl-3.0.en.html)
is distributed with the source code of this program.

## Copyright Holders

The copyright holders of DORiE are referred to as *DORiE Developers* in the
respective copyright notices.

DORiE has been developed by (in alphabetical order)

* Dion Häfner
* Ole Klein
* Santiago Ospina De Los Ríos
* Lukas Riedel
* Felix Riexinger
