# DORiE
(**D**UNE-**O**perated **Ri**chards equation solving **E**nvironment)

DORiE is a software package for solving the Richards equation coupled with the
passive transport equation. The core feature is a C++ PDE-solver powered by
[DUNE](https://dune-project.org/) and especially the
[DUNE-PDELab](https://dune-project.org/modules/dune-pdelab/) module.

Just getting started? Use the
[Cook Book](https://hermes.iup.uni-heidelberg.de/dorie_doc/master/html/cookbook/index.html)
in the User Manual to dive right in!

### Contents of this README

* [Overview](#overview)
* [Installation](#installation-instructions)
    * [Docker Image](#download-docker-image)
    * [Dependencies](#dependencies)
    * [Manual Installation](#manual-installation)
    * [Recommended Tools](#recommended-third-party-software)
* [Documentation](#documentation)
* [Usage](#usage)
* [Troubleshooting](#troubleshooting)

---

## Overview

DORiE offers a variety of solver and discretization solutions. The passive
transport module is optional. For both modules independently, users may choose
finite volume (FV) or discontinuous Galerkin (DG) discretizations. The latter
may be used on unstructured grids and can take advantage of adaptive local grid
refinement.

The C++ routines are accompanied by various tools for program setup,
program testing, and output analysis, which are mostly written in Python.

DORiE is developed and maintained by the
[DORiE Developers](mailto:dorieteam@iup.uni-heidelberg.de) of the
[TS-CCEES](http://ts.iup.uni-heidelberg.de/) research group at the
[Institute of Environmental Physics (IUP) Heidelberg](http://www.iup.uni-heidelberg.de/),
supervised by
[Kurt Roth](http://ts.iup.uni-heidelberg.de/people/prof-dr-kurt-roth/),
in collaboration with
[Ole Klein](https://conan.iwr.uni-heidelberg.de/people/oklein/) and the
[Scientific Computing Group](https://conan.iwr.uni-heidelberg.de/) of the
[Interdisciplinary Center for Scientific Computing (IWR) Heidelberg](https://typo.iwr.uni-heidelberg.de/home/).

DORiE is free software and licensed under the
[GNU General Public License Version 3](https://www.gnu.org/licenses/gpl-3.0.en.html).
For the copyright notice and the list of copyright holders,
see [`COPYING.md`](COPYING.md).

Contributions to the project are always welcome! Please notice our
[Contribution Guidelines](CONTRIBUTING.md).

### How to Cite DORiE

DORiE v2.0 was reviewed and published in the [Journal of Open Source Software (JOSS)](https://joss.theoj.org/).
Please cite at least the following publication if you use DORiE (or a modified version thereof) for your own work:

> Riedel, Lukas, Santiago Ospina Los De Ríos, Dion Häfner, and Ole Klein. 2020. “DORiE: A Discontinuous Galerkin Solver for Soil Water Flow and Passive Solute Transport Based on DUNE.” *Journal of Open Source Software* 5 (52): 2313. https://doi.org/10.21105/joss.02313.

The [`CITATION.cff`](CITATION.cff) file in this repository follows the [citation file format](https://citation-file-format.github.io/) and contains additional metadata to reference this software, its authors, and associated publications.

## Installation Instructions

DORiE is a [DUNE](https://dune-project.org/) module and requires several other
DUNE modules as well as third party software packages. Installation can be
handled manually on your local machine, but we recommend using a pre-compiled
image for the deployment software [Docker](https://www.docker.com/) to
inexperienced users instead. Docker can also be used to create a development
environment without installing the dependencies on the host machine.

### Download Docker Image

No installation is necessary if you download DORiE as Docker image from
[Docker Hub](https://hub.docker.com/r/dorie/dorie/).

If you want to use any stable version of DORiE, or the most recent unstable
version, you can download the appropriate images from Docker Hub. To do so,
execute

    docker pull dorie/dorie[:<tag>]

Omitting the tag information downloads the image with tag `latest` which
refers to the latest stable version. You can download any tag by specifying
`<tag>`. The list of
[available tags](https://hub.docker.com/r/dorie/dorie/tags) can be found on
Docker Hub and matches the release tags list of the Git repository. The latest
unstable version is tagged as `devel`.

You can then proceed directly to the the instructions on
[how to execute DORiE](#running-dorie). The commands listed there are appended
to the usual commands for running a Docker container. See the description on
Docker Hub for further details.

#### Use Docker for Development

An intermediate solution for creating a development environment without
installing all dependencies is using the
[DUNE environment Docker image](https://hub.docker.com/r/dorie/dune-env) of
DORiE. It is based on a Ubuntu image and contains all dependencies of DORiE.
Developers can clone the DORiE source code onto their host system, modify it
there and then mount the directory into the Docker image to compile the program.
To start a container from the image with the local `<dorie_dir>` mounted,
execute

    docker run -it -v <dorie_dir>:/opt/dune/ dorie/dune-env:<tag>

Inside the container, you can then execute the `dunecontrol` script as explained
in the installation instructions further below.

### Dependencies

Depending on your system configuration, there will be more packages necessary to
install DORiE on your machine. See the step-by-step manual for further details.
The specified versions are the _supported_ ones, where compatibility is ensured
by CI tests.

#### DUNE Packages

| Software | Version/Branch | Comments |
| ---------| -------------- | -------- |
| [dune-common](https://gitlab.dune-project.org/core/dune-common) | releases/2.6
| [dune-geometry](https://gitlab.dune-project.org/core/dune-geometry) | releases/2.6
| [dune-grid](https://gitlab.dune-project.org/core/dune-grid) | releases/2.6
| [dune-uggrid](https://gitlab.dune-project.org/staging/dune-uggrid) | releases/2.6
| [dune-istl](https://gitlab.dune-project.org/core/dune-istl) | releases/2.6
| [dune-localfunctions](https://gitlab.dune-project.org/core/dune-localfunctions) | releases/2.6
| [dune-functions](https://gitlab.dune-project.org/staging/dune-functions) | releases/2.6
| [dune-typetree](https://gitlab.dune-project.org/staging/dune-typetree) | releases/2.6
| [dune-pdelab](https://gitlab.dune-project.org/pdelab/dune-pdelab) | releases/2.6
| [dune-randomfield](https://gitlab.dune-project.org/oklein/dune-randomfield) | releases/2.6
| [dune-testtools](https://gitlab.dune-project.org/quality/dune-testtools) | releases/2.6 | *Optional:* For system tests

#### DUNE Requirements

| Software | Version/Branch | Comments |
| ---------| -------------- | -------- |
| CMake | 3.16 |
| GCC | 9.3 | Full C++17 support required
| *or* LLVM Clang | 10 | Full C++17 support required
| *or* AppleClang | 11 | Full C++17 support required
| git |
| pkg-config |
| FFTW3 | 3.3.8 | MPI support required
| Python | 3.8 |
| pip | 20 |
| MPI | | Tested with OpenMPI 4.0.3
| SuperLU | 5.2 |
| OpenGL | 1.3 | Or another GL implementation compatible with VTK 9

#### DORiE Requirements

| Software | Version/Branch | Comments |
| ---------| -------------- | -------- |
| [HDF5](https://www.hdfgroup.org/solutions/hdf5/) | 1.10 | MPI support required
| [yaml-cpp](https://github.com/jbeder/yaml-cpp) | >= 5.2.0 |
| [muparser](http://beltoforion.de/article.php?a=muparser) | master |
| [spdlog](https://github.com/gabime/spdlog) | >= 1.0 |
| [Google Test](https://github.com/google/googletest) | `HEAD` | Included as Git Submodule

#### Optional Packages

| Software | Version/Branch | Comments |
| -------- | -------------- | -------- |
| [doxygen](http://www.stack.nl/~dimitri/doxygen/) | 1.8.17 | Builds documentation
| [METIS](http://glaros.dtc.umn.edu/gkhome/views/metis) | 5 | For parallel runs
| [ParMETIS](http://glaros.dtc.umn.edu/gkhome/views/metis) | 4 | For parallel runs

### Manual Installation

Installing all packages manually can be quite an effort, but useful for
developers who want to have easy access to the source files or users who prefer
to run DORiE without the Docker overhead.

Whenever possible, dependencies should be installed using a package manager
like [APT](https://wiki.ubuntuusers.de/APT/) on Ubuntu or
[Homebrew](http://brew.sh/) on Mac. Manual installation on a Windows
environment is not supported!

DORiE is configured, built, and installed via the
[DUNE Buildsystem](https://dune-project.org/doc/installation/), using the
`dunecontrol` script to handle DUNE-internal dependencies.

#### Step-by-step Instructions
These instructions are suitable for a clean **Ubuntu** or **macOS** setup.
The main difference between the two systems is the package manager.
Debian-based systems have the APT manager already built in. On Mac, we
recommend installing [Homebrew](http://brew.sh/). If you prefer to use
[MacPorts](https://www.macports.org/), notice that packages will need to be
installed differently than indicated here.

Manual installations on macOS require installing HDF5 from source. This can
be tricky, but the following instructions should work on a clean system.

If you installed [Anaconda](https://conda.io/docs/user-guide/install/download.html)
on your machine, you don't need to install Python or Pip. Simply skip these
packages when using the package managers for installing the software. However,
notice the warnings when compiling DORiE below!

1. **macOS** users need to start by installing the Apple Command Line Tools by
    executing

        xcode-select --install

    Make sure you have no pending software updates for your respective version
    of macOS!

2. Install third party packages:

    **Ubuntu:**

        apt update
        apt install cmake doxygen gcc g++ gfortran \
                    git libatlas-base-dev libfftw3-dev libfftw3-mpi-dev \
                    libfreetype6-dev libgl-dev libhdf5-mpi-dev libmuparser-dev \
                    libopenmpi-dev libpng-dev libspdlog-dev libsuperlu-dev \
                    libyaml-cpp-dev libxft-dev python3-dev python3-pip

    **macOS:**

        brew update
        brew install cmake doxygen fftw gcc libpng open-mpi muparser \
                     pkg-config python3 spdlog superlu yaml-cpp

3. **macOS only:** Install HDF5 with MPI support from source.

    1. Download an archive of the
    [HDF5 source code](https://www.hdfgroup.org/downloads/hdf5/source-code/),
    and extract it.

    1. Enter the extracted folder. In there, create a `build` directory, and
    enter it:

            mkdir build && cd build

    1. Configure your build. If you followed the instructions above, the
    OpenMPI C compiler is reachable via the command `mpicc`. If not, you have
    to specify a full path to it. Use the option `prefix` to specify where
    you want the package to be installed. This should *not* be a
    system-reserved path like `/usr/local`, and *not* be located in a
    sub-directory of the source code. Execute the configuration script:

            ./../configure CC=mpicc --prefix=<hdf5-path> --enable-parallel

    1. Build and install the library:

            make && make install

4. The parallel linear solver of DORiE can make use of the ParMETIS package.
    If you want to run DORiE in parallel on multiple processes, additionally
    install METIS and ParMETIS:

    **Ubuntu:**

        apt install libmetis-dev libparmetis-dev

    **macOS:** _Support is dropped because ParMETIS is currently unavailable
    from Homebrew._

    **Parallel runs without these two packages are possible but not supported!**

5. Create a directory for all your DUNE modules, including DORiE. If you already
   cloned DORiE somewhere else, move the cloned folder into the DUNE module
   directory.

6. Clone the [DUNE modules](#dune-Packages) into the DUNE module directory. Use
   `git checkout` to switch to the correct branches. We provide a bash script
   which clones all required repositories and checks out the correct branches
   into the directory it is executed from. Move into the DUNE module folder and
   execute it:

        bash dorie/clone_dune

7. Make sure the DORiE repository is correctly set up. DORiE includes
   [Git Submodules](https://git-scm.com/book/en/v2/Git-Tools-Submodules),
   which are only downloaded if you add the `--recurse-submodules` option to the
   `git clone` command. To make sure the submodules are cloned and checked out,
   enter the DORiE repository and execute

        git submodule init && git submodule update

8. Enter the DUNE module directory, and call

        ./dune-common/bin/dunecontrol --opts=dorie/build.opts all

    to build all DUNE modules. Additionally, you can add `MAKE_FLAGS="-j X"`
    to the command in order to compile on `X` processes in parallel.

    The `build.opts` file in this repository contains required and useful CMake
    variable settings for a build of DORiE. If you installed software into paths
    not appended to your `PATH` variable, you will have to add more
    `CMAKE_FLAGS` in the options file to make sure that CMake finds all
    packages. If `<Package>_ROOT` variables suffice, you can simply prepend them
    before the `dunecontrol` command. See the
    [Dune Installation Docs](https://www.dune-project.org/doc/installation/) for
    further information.

    If you installed HDF5 from source (all **macOS** users) or use Anaconda,
    specify the path to your HDF5 installation by using the `HDF5_ROOT`
    environment variable. On Ubuntu, prepend the path to the APT package,

        HDF5_ROOT=/usr/

    and on macOS, prepend

        HDF5_ROOT=<hdf5-path>

    in the `dunecontrol` command above, replacing `<hdf5-path>` with the
    path chosen as installation prefix when configuring HDF5. Alternatively,
    `export` these paths in your shell before calling `dunecontrol`, or add
    them as CMake variables (with prefix `-D`) to the repository options file.

### Recommended Third-Party Software

The following software packages are cross-platform, so you should be able to
find a release that fits your operating system:

* [ParaView](http://www.paraview.org/): A powerful post-processing tool for VTK
    files. Offers both visualization and data analysis tools.
* [Gmsh](http://gmsh.info/): An open-source CAD that can be used to create the
    `.msh` files used by DORiE to define unstructured meshes.

## Documentation

The documentation of DORiE is twofold. The Sphinx documentation contains a
manual with guidelines and tutorials for users of the compiled software
package. The Doxygen documentation of the C++ source code is intended for
developers only and explains the inner workings of the software.

Both parts of the documentation are deployed to our documentation server for
every branch pushed to the main repository. You will find the latest
[user manual](https://hermes.iup.uni-heidelberg.de/dorie_doc/master/html/) and
[C++ code documentation](https://hermes.iup.uni-heidelberg.de/dorie_doc/master/doxygen/html/)
there. The documentation for other branches can be accessed via the
[overview page](https://hermes.iup.uni-heidelberg.de/dorie_doc/).

The documentation can also be built locally after DORiE has been properly
configured following the step-by-step instructions above. To build the
documentation, move to the `dorie/build-cmake` directory and simply run

    make doc

You will then find the index page of the Sphinx user documentation at
`dorie/build-cmake/doc/html/index.html` and the index page of the Doxygen
source code documentation at `dorie/build-cmake/doc/doxygen/html/index.html`.

## Usage

DORiE provides a command line interface (CLI) for all its user functions.
The required Python modules and all their dependencies are readily installed
into a Python virtual environment (`venv`), which has to be activated within a
shell session. You can do so by activating it in your current session
(Manual Installation only) or by running the Docker application.

### Run the `venv` using the Docker application
If you did not install DORiE locally, you can use the Docker application to
boot up the virtual environment in a mounted directory of your choice.

Start up the Docker application by calling

    docker run -it -v <dir>:/mnt <img>

where you replace `<dir>` with a local directory for storing input and output
data, and `<img>` with `dorie/dorie[:<tag>]`. We recommend moving into the
designated input and output directory on your local machine and inserting
`$PWD` as `<dir>` to mount the current directory into the container.

The command boots up a (`bash`) shell inside a Docker container and mounts
the directory `<dir>` and all its subdirectories into the directory `/mnt`
inside the container. Your shell session starts in this directory with the
virtual environment activated.

Notice, that you can only use **local file paths** in all configuration
settings due to the directory mount.

### Activate the `venv` locally
To activate the virtual environment within your current shell session, execute

    source <path/to/>dorie/build-cmake/activate

where you replace `<path/to/>` with the path to the appropriate directory.

Your shell will now display the prefix `(dune-env)` to indicate that it is
configured appropriately. You can exit the environent at any time by simply
executing

    deactivate

Notice that any virtual environment only applies to, and lasts for, your
current terminal session!

_With the virtual environment activated,_ you can now navigate to any directory
that you would like to contain your simulation input and/or output data.

### Execute the application
Any command to the DORiE application has the signature

    dorie <cmd> [<opts>] [<args>]

Using the `-h` or `--help` option, you can find all available commands and
further help. To start your first simulation run, create a new directory and
enter it.

#### 1 — Default input files
Create some exemplary configuration files along with parameter and boundary
condition data files by calling

    dorie create

The data files are valid input files for very limited scenarios. The main
configuration file `config.ini` requires tweaking by the user. Most `UNDEFINED`
values must be properly defined before starting the simulation. A cheat sheet
for the single config file entries as well as manuals on how the boundary
condition and parameter files are used can be found in the user documentation.

#### 2 — _Optional:_ Create a random field
DORiE implements a lightweight wrapper around the `dune-randomfield`
generator. You can use it to easily create a heterogeneous soil architecture.
This step is optional. Tweak the parameters of `parfield.ini` to your liking
and then call

    dorie pfg parfield.ini

A cheat sheet for this config file is also available from the documentation.

#### 3 — Perform a simulation
The DORiE main routine is executed with the `run` command.
Tweak the parameters of `config.ini` to your liking. You will need to
reference several additional input files for soil parameters, boundary
conditions, GMSH grid files (optional), and grid mappings (optional).
Refer to the documentation for further information.

Once prepared, call

    dorie run config.ini

to execute the solver.

## Troubleshooting

CMake heavily caches the results of its configuration process. In case you
encounter errors or strange behavior, especially after an update, you should
delete the DORiE build folder (called `build-cmake` by default) and re-build
DORiE using `dunecontrol`.

If the problem persists, take a look at the
[list of Issues](https://ts-gitlab.iup.uni-heidelberg.de/dorie/dorie/issues),
and feel free to create an Issue yourself if the problem is not yet reported.

### Debugging
DORiE can be built with debugging flags via CMake. To do so, enter the
`build-cmake` directory and execute

    cmake -DCMAKE_BUILD_TYPE=Debug ..
    make all

After building, a debugger can hook into the executables.

**Note:** If no `CMAKE_BUILD_TYPE` is specified during re-configuration, the
last configuration build type is used. If no CMake files exist, it defaults to
`Release`. You will find the actual value displayed in the final output of
CMake.

To re-create a release build, configure DORiE with the release build type by
executing

    cmake -DCMAKE_BUILD_TYPE=Release ..
    make all

or by calling `dunecontrol` with the supplied options file as indicated in the
installation instructions above.

### Running System Tests
DORiE includes a testing system for comparing its results the ones of ODE
solvers or former versions of itself. This ensures that DORiE is running
correctly and producing the expected results. We distinguish _unit tests_ for
testing certain features of the code, and _system tests_ for verifying the
results of the final application. As system tests require executing the DUNE
solvers, it is recommended to build them in a `Release` environment.
Additionaly, there is a set of tests for the Python module.

| Test category | Build tests | Execute tests | Recommended build type |
| ------------- | ----------- | ------------- | ---------------------- |
| Unit tests | `make build_unit_tests` | `make unit_tests` | `Debug` |
| System tests | `make build_system_tests` | `make system_tests` | `Release` |
| Python tests | _Not required_ | `make test_python` | _Any_ |
| Cookbook examples (no testing performed) | `make all` | `make example_tests` | `Release` |

The `make` commands are to be executed from within the `build-cmake` directory.

#### Code Coverage Report
To enable code coverage reports, configure DORiE with the CMake option
`COVERAGE_REPORT` enabled, like so (from the `build-cmake` directory):

    cmake -DCOVERAGE_REPORT=On ..

This will add the appropriate compiler flags to _all_ targets. You then have to
re-build all binaries. After running tests or executing the application, you
can retrieve code coverage information using the
[`gcovr`](https://gcovr.com/index.html) utility.

### Developing Python Code
DORiE installs its Python module into the DUNE-controlled Python virtual
environment (`venv`) during the CMake configuration phase. This means that
changes to its source code are only reflected after running CMake again. For
less tedious Python code development, set the option
`DUNE_PYTHON_INSTALL_EDITABLE` in the `build.opts` file to `TRUE`. This will
install the Python packages in editable mode, meaning that they are "symlinked"
to their original location and changes to the source code are immediately
reflected inside the virtual environment.

Note however, that this is not compatible with a re-locatable installation
because editable installs require the source code in its original location.

### Further Help
[Open an Issue](https://ts-gitlab.iup.uni-heidelberg.de/dorie/dorie/issues/new),
on GitLab or write to the
[DORiE developer mailing list](mailto:dorieteam@iup.uni-heidelberg.de).
