# Contributing to DORiE

**Thank you for taking your time and contributing to DORiE!** 👍

## License and Distribution

DORiE is licensed under the
[GNU General Public License Version 3](https://www.gnu.org/licenses/gpl-3.0.en.html).
Contributors to the DORiE project and source code agree that their contributions
are published and may be distributed according to this license. All contributors
will be acknowledged in the [authors list](COPYING.md#copyright-holders).

## Code of Conduct

Everybody participating in and contributing to this project is expected to
uphold our attached [Code of Conduct](CODE_OF_CONDUCT.md). Report any
unacceptable behavior to the [DORiE Developers][mailinglist]!

## How to Contribute

DORiE is open source software. We strive for making every stage of development
public, sharing our advances, and incorporating community contributions. We
therefore prefer public contributions of any kind via GitLab.

### GitLab Account

The DORiE repository is hosted on the private GitLab instance of the
[TS-CCEES](http://ts.iup.uni-heidelberg.de/) research group at the
[Institute of Environmental Physics (IUP) Heidelberg](http://www.iup.uni-heidelberg.de/).

As the GitLab instance hosts many of our group-internal projects, new users
will be registered as
[external users](https://docs.gitlab.com/ee/user/permissions.html#external-users-core-only)
with access to the public repositories only and without rights to create their
own projects. Use the "Request Access" button if you want to become a member
of a certain project.

### Questions and Bug Reports

Before asking questions or reporting bugs, please make sure your issue cannot be
resolved by following the instructions in the
[Troubleshooting](README.md#troubleshooting) section of the `README.md`.

If you want to report a bug, [raise an issue][issue] and use the `bug-report`
description template to provide as much information as possible. We prefer
discussing even minor issues in public, so feel free to open an issue for
presumably minor questions as well.

### Code Development

Suggest features or plan implementations in GitLab issues. We provide the
`task` and `meta-task` description templates for structuring your planning
and for providing the required information to other developers.

Consult the [Doxygen documentation][doxygen] of the DORiE source code for
information on how certain features of DORiE are used within the C++ source
code.

Any changes to source code should be accompanied by a (unit) test verifying the
functionality. Designing this test ideally happens in the planning phase of
a change. DORiE vendors [Google Test](https://github.com/google/googletest) as
unit test framework. Check out the [Doxygen documentation][doxygen] for
information on how to employ the framework in DORiE.

After a proper discussion of the issue, and the resulting implementation, open
a merge request. Again, we encourage you to use one of the description
templates. Provide information on how your code changes and additions solve the
problem or implement the feature. Make sure that your MR fulfills the the
criteria for being merged.

#### C++ Coding Style

The DORiE repository contains a `.clang-format` file in the top-level directory specifying the desired style for C++ code in DORiE.
We ask developers to use this style when writing new code, ideally by instructing their editor and/or [ClangFormat](https://clang.llvm.org/docs/ClangFormat.html) tool to do so.
Every change to C++ files introduced by Git commits should be formatted according to this style.

DORiE vendors [clang-format-hooks](https://github.com/barisione/clang-format-hooks) to automate formatting and avoid committing unformated changes.
It contains a Git pre-commit hook which checks the changes right before committing.
If the changes to not comply to the coding style, the user is shown the suggested style and prompted to apply the changes or abort the commit.
Follow these instructions for registering the hook:

1. Install `clang-format`.

    On **macOS**:

    ```bash
    brew install clang-format
    ```

    On **Ubuntu**:

    ```bash
    apt install clang-format
    ```

2. Register the hook. From the top-level DORiE source directory, execute

    ```bash
    ./plugins/vendor/clang-format-hooks/git-pre-commit-format install
    ```

When running in trouble, consult the [original installation instructions](https://github.com/barisione/clang-format-hooks#setup).

Happy coding!

### Releases

Tasks for creating and publishing a release are listed in the `version-rollout`
and `patch-release` issue description templates. For patching a release, an
additional `patch-release` merge request description template is provided.

### Old-Fashioned Email

Of course, you can always contact the developers directly
[via mail][mailinglist].

[mailinglist]: mailto:dorieteam@iup.uni-heidelberg.de
[issue]: https://ts-gitlab.iup.uni-heidelberg.de/dorie/dorie/-/issues/new?
[doxygen]: https://hermes.iup.uni-heidelberg.de/dorie_doc/master/doxygen/html/index.html
