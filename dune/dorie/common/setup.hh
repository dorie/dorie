#ifndef DUNE_DORIE_COMMON_SETUP_HH
#define DUNE_DORIE_COMMON_SETUP_HH

#include <iostream>
#include <memory>
#include <vector>
#include <tuple>
#include <thread>
#include <chrono>

#include <dune/common/exceptions.hh>
#include <dune/common/parametertree.hh>
#include <dune/common/parametertreeparser.hh>
#include <dune/common/parallel/mpihelper.hh>

#include <dune/dorie/common/logging.hh>

namespace Dune{
namespace Dorie{

/**
 * @brief      Helper namespace to configurate/setup input files
 * @ingroup Init
 */
namespace Setup
{
    /**
     * @addtogroup Init
     * @{
     */

    /// Initialize the base logger and config file
    /** Create the Dune MPI Helper, read the parameter file tree, and
     *  initialize the base logger. Apply log level at primary process
     *  after displaying greetings and name of config file loaded.
     *
     *  \param argc Number of arguments of the main program
     *  \param argv Argument character array of the main program
     *  \param greetings Initial message of the logger (log-level `info`,
     *      will *always* be displayed)
     */
    auto init (int& argc,
               char**& argv,
               const std::string greetings="Initializing program")
        -> std::tuple<Dune::ParameterTree,
                      std::shared_ptr<spdlog::logger>,
                      Dune::MPIHelper&>
    {
        // initialize the helper
        auto& helper = Dune::MPIHelper::instance(argc, argv);

        // Initialize global logger
		auto log = Dune::Dorie::create_base_logger(helper,
												   spdlog::level::info);
        log->info(greetings);

        // read arguments
        if (argc!=2) {
			log->error("This routine expects the config file "
					   "as single argument!");
			DUNE_THROW(Dune::IOError, "Wrong number of arguments");
		}
		const std::string inifilename = argv[1];

        // Read ini file
		log->info("Reading configuration file: {}", inifilename);
		Dune::ParameterTree inifile;
		Dune::ParameterTreeParser ptreeparser;
		ptreeparser.readINITree(inifilename, inifile);

        // set appropriate log level
        const auto log_level = inifile.get<std::string>("output.logLevel");
		if (helper.rank() == 0) {
			log->set_level(spdlog::level::from_str(log_level));
		}
		if (helper.size() > 1) {
			log->debug("Running in parallel on {} nodes",
					   helper.size());
		}

        return std::tuple<Dune::ParameterTree,
                          std::shared_ptr<spdlog::logger>,
                          Dune::MPIHelper&>
                         (inifile, log, helper);
    }

    /// Halt the process for a debugger.
    /** Stops the current process for attaching a debugger.
     *  This is useful when running multiple process in parallel, which
     *  requires one debugger to attach to every process. After attaching,
     *  set the integer `i` to a value other than zero (`0`), to continue.
     *
     *  The respective process ID is displayed in the log messages.
     */
    void debug_hook ()
    {
        // cache log level
        const auto log = get_logger(log_base);
        const auto _log_level_cache = log->level();
        log->set_level(spdlog::level::info);

        // inform the user
        log->warn("Process halted due to debug mode");
        log->warn("Attach a debugger and "
                  "set variable i to a value != 0");
        log->flush();

        // actual sleeping
        int i = 0;
        using namespace std::chrono_literals;
        while (i == 0) {
            std::this_thread::sleep_for(50ms);
        }

        // we continue
        log->info("Commencing ...");
        log->set_level(_log_level_cache);
    }

    /*------------------------------------------------------------------------*//**
     * @brief      Set up the configuration keys for Richards models
     *
     * @param      ini   The inifile
     */
    Dune::ParameterTree prep_ini_for_richards(const Dune::ParameterTree& ini)
    {
        const auto log = get_logger(log_base);
        Dune::ParameterTree richards_ini(ini.sub("richards"));

        const std::string adapt_policy_str = ini.get<std::string>("adaptivity.policy");

        // copy adaptivity keywords into richards category
        if (adapt_policy_str == "waterFlux") {
            richards_ini.sub("adaptivity") = ini.sub("adaptivity");
        } else if (adapt_policy_str != "none") {
            log->error("Unknown adaptivity policy: {}", adapt_policy_str);
            DUNE_THROW(NotImplemented, "Unknown adaptativity policy");
        }

        return richards_ini;
    }

    /*------------------------------------------------------------------------*//**
     * @brief      Set up the configuration keys for Transport models
     *
     * @param      ini   The inifile
     */
    Dune::ParameterTree prep_ini_for_transport(const Dune::ParameterTree& ini)
    {
        Dune::ParameterTree transport_ini(ini.sub("transport"));

        const std::string adapt_policy_str = ini.get<std::string>("adaptivity.policy");

        // copy adaptivity keywords into transport category
        if (adapt_policy_str == "soluteFlux") {
            transport_ini.sub("adaptivity") = ini.sub("adaptivity");
        }

        return transport_ini;
    }

    /**
     * @}
     */
};

} // namespace Dorie
} // namespace Dune

#endif // DUNE_DORIE_COMMON_SETUP_HH
