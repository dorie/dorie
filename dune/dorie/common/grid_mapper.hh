#ifndef DUNE_DORIE_UTIL_GRID_MAPPER_HH
#define DUNE_DORIE_UTIL_GRID_MAPPER_HH

#include <vector>
#include <array>
#include <numeric>
#include <memory>
#include <map>

#include <dune/common/fvector.hh>
#include <dune/common/parametertree.hh>
#include <dune/common/float_cmp.hh>

#include <dune/grid/common/mcmgmapper.hh>

#include <dune/dorie/common/logging.hh>
#include <dune/dorie/common/h5file.hh>
#include <dune/dorie/common/utility.hh>

namespace Dune {
namespace Dorie {

/// Type of grid loading mode. Defines how parameters are read from the grid.
/** \ingroup Grid
 */
enum class GridMode {
    /// Read a GMSH file containing parameter and boundary mappings
    gmsh,
    /// Build a grid internally and read parameter mappings from files
    rectangular
};

/// Base class for mappers for assembling local element and boundary index maps
/** This class provides common members and methods for retrieving the maps,
 *  and broadcasting data to all processes.
 *
 *  \tparam Grid Type of the grid used
 *  \ingroup Grid
 *  \author Lukas Riedel
 *  \date 2018
 */
template<typename Grid>
class GridMapperBase
{
public:
    /// Type of the maps returned by this mapper
    using Map = std::vector<int>;

protected:
    /// Type of the grid view
    using GridView = typename Grid::LevelGridView;
    /// Type of the logger of this instance
    using Logger = std::shared_ptr<spdlog::logger>;
    /// Type of the local entity index mapper
    using Mapper = typename Dune::MultipleCodimMultipleGeomTypeMapper<GridView>;
    /// Index type used by the mapper
    using Index = typename Mapper::Index;

    /// Shared pointer to the grid (required for load-balancing)
    const std::shared_ptr<Grid> _grid;
    /// The view on the grid
    const GridView _gv;
    /// The logger of this instance
    const Logger _log;

    /// The mapper for local grid indices of entities
    Mapper _mapper;
    /// Maps the (local) index of a grid entity to an arbitrary index.
    Map _element_index_map;
    /// Maps the (local) index of a grid boundary to an arbitrary index.
    Map _boundary_index_map;

public:
    /// Construct the mapper. Initialize the grid view to level 0.
    /** \param grid Shared pointer to the grid
     *  \param log The logger of this instance (defaults to log_base).
     */
    GridMapperBase (const std::shared_ptr<Grid> grid,
                    const Logger log=get_logger(log_base)):
        _grid(grid),
        _gv(_grid->levelGridView(0)),
        _log(log),
        _mapper(this->_gv, Dune::mcmgElementLayout())
    { }

    /// Return the local element index map
    const Map& element_index_map () const { return _element_index_map; }
    /// Return the local boundary index map
    const Map& boundary_index_map () const { return _boundary_index_map; }

protected:
    /// Broadcast the element id map to all processors
    /** \param cont The container to broadcast (and hence manipulated)
     */
    template<typename Container>
    void broadcast_associative_cont (Container& cont) const
    {
        // get MPI communicator
        const auto& comm = _gv.comm();

        // broadcast the size of the map
        auto size = cont.size();
        comm.broadcast(&size, 1, 0);

        // build two vectors from keys and values and fill them
        using key_t = typename Container::key_type;
        using mapped_t = typename Container::mapped_type;
        std::vector<key_t> keys;
        std::vector<mapped_t> values;
        for (auto&& [key, value] : cont) {
            keys.push_back(key);
            values.push_back(value);
        }

        // extend to correct size on all processes
        keys.resize(size);
        values.resize(size);

        // broadcast the data
        comm.broadcast(keys.data(), size, 0);
        comm.broadcast(values.data(), size, 0);

        // clear the container and fill it with the values for this process
        cont.clear();
        std::transform(keys.begin(), keys.end(), values.begin(),
            std::inserter(cont, cont.end()),
            [](const auto key, const auto val) {
                return std::make_pair(key, val);
            }
        );
    }

    /// Broadcast a container of indices to all processes
    /** \param cont The container to broadcast
     */
    template<typename Container>
    void broadcast_contiguous_container (Container& cont) const
    {
        // get MPI communicator
        const auto& comm = _gv.comm();

        // fill a vector with the values
        using value_t = typename Container::value_type;
        std::vector<value_t> values(cont.begin(), cont.end());

        // extend to correct size on all processes (data is safe)
        auto size = values.size();
        comm.broadcast(&size, 1, 0);
        values.resize(size);

        // actual broadcast
        comm.broadcast(values.data(), size, 0);

        // refill original container
        cont.clear();
        std::copy(values.begin(), values.end(),
                  std::inserter(cont, cont.end()));
    }

};

#ifndef DOXYGEN

/// Unspecified grid mapper specizalization
template<typename Grid, GridMode>
class GridMapper : public GridMapperBase<Grid>
{ };

#endif // DOXYGEN

/// Specialized mapper for grids loaded from GMSH
/** The maps are retrieved together with the acutal grid by the
 *  Dune::GmshReader. DORiE only supports UGGrid in this case.
 *
 *  Due to unintended behavior of UGGrid, the indices of boundary intersections
 *  are consistent across all processors. Therefore, we only need to broadcast
 *  the corresponding map.
 *
 *  For the element map, we have to create a global ID map, broadcast it,
 *  load-balance the grid, and re-create a local element index map.
 *
 *  \note Only works for UG grids built by the Dune::GmshReader
 *  \note Only works for grids that exist on a single process.
 *  \todo Add capability for re-balancing. Assume that local maps exist on
 *      every process and use functions like `gatherv` for balancing all data.
 *
 *  \ingroup Grid
 *  \author Lukas Riedel
 *  \date 2018
 */
template<typename Grid>
class GridMapper<Grid, GridMode::gmsh> : public GridMapperBase<Grid>
{
private:
    /// Type of the base class
    using Base = GridMapperBase<Grid>;
    /// Type of the grid view
    using GridView = typename Base::GridView;
    /// Type of the global ID map for all entities
    using IdSet = typename Grid::GlobalIdSet;
    /// Type of the IDs used by the IdSet
    using IdType = typename IdSet::IdType;

    /// The globally persistent ID set of grid entities
    const IdSet& _idset;

public:
    /// Type of the maps returned by this mapper
    using Map = typename Base::Map;

    /// Construct this mapper with local maps retrieved from the GMSH file
    /** If run sequentially, only copy the local maps. Otherwise, build a
     *  globally consistent ID map, load-balance the grid, and re-build the
     *  index maps locally.
     *
     *  \param grid Shared pointer to the grid
     *  \param element_index_map_local The local element index map retrieved
     *      from the GmshReader.
     *  \param boundary_index_map_local The local boundary index map retrieved
     *      from the GmshReader.
     */
    explicit GridMapper (const std::shared_ptr<Grid> grid,
                         const Map& element_index_map_local,
                         Map& boundary_index_map_local) :
        Base(grid),
        _idset(this->_grid->globalIdSet())
    {
        if (this->_gv.comm().size() > 1)
        {
            this->_log->debug("Load-balancing index maps");

            // create a mapping using global persistent IDs
            auto element_id_map = build_element_id_map(element_index_map_local);
            // broadcast this map
            this->broadcast_associative_cont(element_id_map);

            // load-balance the grid to all processors
            this->_grid->loadBalance();
            this->_mapper.update();

            // rebuild the index map for local grid partition
            this->_element_index_map = rebuild_local_index_map(element_id_map);

            // broadcast the (global) boundary map
            this->broadcast_contiguous_container(boundary_index_map_local);
            this->_boundary_index_map = boundary_index_map_local;
        }
        // just copy in sequential mode
        else {
            this->_element_index_map = element_index_map_local;
            this->_boundary_index_map = boundary_index_map_local;
        }
    }

private:
    /// Build a (global) id map from a local element index map
    /**
     *  \param element_index_map_local Local mapping of element indices
     *  \return Global ID map with the same mapping as element_index_map.
     */
    std::map<IdType, int> build_element_id_map (
        const Map& element_index_map_local
    ) const
    {
        // iterate over grid elements
        std::map<IdType, int> ret;
        for (auto&& elem : elements(this->_gv)) {
            const auto index = this->_mapper.index(elem);
            const auto id = _idset.id(elem);
            ret.emplace(id, element_index_map_local.at(index));
        }

        return ret;
    }

    /// Build a local element index map from a global ID mapping
    /** The global ID is consistent across all processors and is used for
     *  identifying the respective cell.
     *
     *  \param id_map The ID map to build the index map from
     *  \return Local element index map for elements on this process
     */
    Map rebuild_local_index_map (const std::map<IdType, int>& id_map)
    {
        Map ret;
        ret.resize(this->_mapper.size());

        // iterate over local grid elements
        for (auto&& elem : elements(this->_gv)) {
            const auto index = this->_mapper.index(elem);
            const auto id = _idset.id(elem);
            ret.at(index) = id_map.at(id);
        }

        return ret;
    }
};

/// Specialized mapper for grids built by StructuredGridFactory
/**
 *  ## Overview
 *  The maps are built by loading mapping data files specified in the config
 *  file. If no files are given, global default mappings are applied.
 *
 *  The behavior is independent of the current load-balancing of the grid.
 *  The H5 files containing global mappings are (collectively) read on every
 *  process. The local mapping is built from transferring global coordinates
 *  to indices of the datasets.
 *
 *  The mapper works very similar for cell and boundary mappings. To read the
 *  latter, the mapper ignores the spatial dimension in the normal plane of the
 *  respective boundary and reads a `_dim-1`-dimensional dataset. Just like for
 *  the cell mapping, the boundary segments are identified by their global
 *  coordinates.
 *
 *  ## Algorithm
 *
 *  The mapping process works as follows:
 *
 *  1. Get the entry of the `grid.mapping.file`.
 *
 *     - If it is *unspecified*, interpret all other values of `grid.mapping`
 *       as global indices the respective elements should map to. Store
 *       the maps. *Go to 5.*
 *
 *  2. If the value is *valid*, open the respective H5 file.
 *
 *  3. Read the cell mapping dataset given by `grid.mapping.volume`.
 *
 *     - If the name can be evaluated as `int`, map all cells of the grid to
 *       this index.
 *
 *  4. Iterate over all boundaries. Read the respective boundary mapping
 *     dataset given by `grid.mapping.boundaryXYZ`.
 *
 *     - If the name can be evaluated as `int`, map the entire boundary to
 *       this index.
 *
 *  5. Iterate over all grid cells. Take the index returned by the Dune mapper
 *     and map it to the index given by the input datasets. This constructs
 *     the actual element map.
 *
 *  6. Iterate over all boundary intersections. Take the respective boundary
 *     index and map it to the index given by the input datasets.
 *     This constructs the actual boundary map.
 *
 *  \todo Add input of boundary mappings and mapping files
 *
 *  \ingroup Grid
 *  \author Lukas Riedel
 *  \date 2018
 */
template<typename Grid>
class GridMapper<Grid, GridMode::rectangular> : public GridMapperBase<Grid>
{
private:
    /// Type of the base class
    using Base = GridMapperBase<Grid>;
    /// Type of the grid view
    using GridView = typename Base::GridView;
    /// Spatial dimensions of the grid
    static constexpr int _dim = Grid::dimension;
    /// Coordinate type (Domain<_dim> field)
    using DF = typename Grid::ctype;
    /// Position vector type
    template<int dim>
    using Domain = Dune::FieldVector<DF, dim>;
    /// Type for multi-indexing coordinates
    template<size_t dim>
    using MultiIndex = std::array<size_t, dim>;

    /// The config file
    const Dune::ParameterTree& _config;
    /// The global extensions of the grid
    const Domain<_dim> _extensions;
    /// The number of grid cells in each direction
    const MultiIndex<_dim> _cells;

    /// Define boundary side helpers
    struct Boundary
    {
        static_assert(_dim == 2 or _dim == 3, "This can only be used in 2D "
                                              "or 3D");

        /// Convenience enum for specifying boundary sides
        enum Side {
            Left,
            Right,
            Lower,
            Upper,
            Front,
            Back
        };

        /// Return the boundaries that exist in the current dimensions
        /** \return Array with boundary sides to iterate over
         */
        static constexpr auto boundaries ()
        {
            if constexpr (_dim == 2) {
                return std::array<Side, 4>{
                    Side::Left,
                    Side::Right,
                    Side::Lower,
                    Side::Upper
                };
            }
            else if (_dim == 3) {
                return std::array<Side, 6>{
                    Side::Left,
                    Side::Right,
                    Side::Lower,
                    Side::Upper,
                    Side::Front,
                    Side::Back
                };
            }
        }

        /// Get the name of a boundary side
        /** \param side The boundary side
         *  \return The name of the boundary as string
         */
        static std::string to_string (const Side side)
        {
            std::map<Side, std::string> lookup_names {
                {Side::Left, "Left"},
                {Side::Right, "Right"},
                {Side::Lower, "Lower"},
                {Side::Upper, "Upper"},
                {Side::Front, "Front"},
                {Side::Back, "Back"}
            };

            auto it = lookup_names.find(side);
            if (it == lookup_names.end()) {
                DUNE_THROW(Dune::Exception, "Could not find name of boundary. "
                                            "You probably inserted an enum "
                                            "with invalid value.");
            }
            return it->second;
        }

        /// Determine the projection coordinate of a boundary side
        /** This dimension is the redundant one for this boundary and can be
         *  ignored when querying indices and positions.
         *
         *  \param boundary The boundary side of interest
         *  \return The dimension/coordinate that can be ignored
         */
        static constexpr size_t dimension (const Boundary::Side boundary)
        {
            if (boundary == Boundary::Left
                or boundary == Boundary::Right)
            {
                return 0;
            }

            if constexpr (_dim == 2) {
                if (boundary == Boundary::Lower
                    or boundary == Boundary::Upper)
                {
                    return 1;
                }
                else {
                    throw std::runtime_error("Inserted boundary does not "
                                             "exist in 2D!");
                }
            }

            else if (_dim == 3) {
                if (boundary == Boundary::Front
                    or boundary == Boundary::Back)
                {
                    return 1;
                }
                else {
                    return 2;
                }
            }
        }

        /// Return the boundary side for a position
        /**
         *  \note This assumes that the grid is rectangular, and that its
         *      lower left (front) corner is located at the origin.
         *  \param pos The global position to check
         *  \param extensions The extensions of the entire grid
         *  \return The Boundary::Side for a given positions
         */
        static Side side (const Domain<_dim>& pos,
                          const Domain<_dim>& extensions)
        {
            using namespace Dune::FloatCmp;

            if (eq(pos[0], 0.0)) {
                return Boundary::Left;
            }
            else if (eq(pos[0], extensions[0])) {
                return Boundary::Right;
            }

            if constexpr (_dim == 2) {
                if (eq(pos[1], 0.0)) {
                    return Boundary::Lower;
                }
                else if (eq(pos[1], extensions[1])) {
                    return Boundary::Upper;
                }
            }

            else if (_dim == 3) {
                if (eq(pos[1], 0.0)) {
                    return Boundary::Front;
                }
                else if (eq(pos[1], extensions[1])) {
                    return Boundary::Back;
                }
                else if (eq(pos[2], 0.0)) {
                    return Boundary::Lower;
                }
                else if (eq(pos[2], extensions[2])) {
                    return Boundary::Upper;
                }
            }

            // no match
            DUNE_THROW(Exception, "Given coordinate does not belong to any "
                                  "boundary");
        }
    };

public:
    /// Type of the maps returned by this mapper
    using Map = typename Base::Map;

    /// Read config settings, load global maps, and localize them.
    /** Load the global maps from files (or create a mapping to a single
     *  value). Then localize this mapping by using the (local) MCMGMapper,
     *  and by inferring the global element index via global coordinates.
     *
     *  \param grid Shared pointer to the grid
     *  \param config The config file settings
     */
    explicit GridMapper (const std::shared_ptr<Grid> grid,
                         const Dune::ParameterTree& config):
        Base(grid),
        _config(config),
        _extensions(get_grid_extensions(this->_grid).second),
        _cells(config.get<MultiIndex<_dim>>("grid.cells"))
    {
        Map element_index_map_global;
        std::map<typename Boundary::Side, Map> boundary_index_map_global;

        // load global mapping (on every process)
        const auto file_path
            = _config.get<std::string>("grid.mapping.file", "");

        if (is_none(file_path)) {
            // global only
            const auto global_index = _config.get<int>("grid.mapping.volume");
            element_index_map_global = create_global_index_map(global_index,
                                                               _cells);

            // .. on to boundaries!
            const std::string config_section = "grid.mapping.boundary";
            for (auto&& side : Boundary::boundaries())
            {
                const auto boundary_cells = with_dim_ignored(_cells, side);
                const auto config_key = config_section
                                        + Boundary::to_string(side);
                const auto bc_index = _config.get<int>(config_key);
                const auto side_index_map
                    = create_global_index_map(bc_index, boundary_cells);
                boundary_index_map_global.emplace(side, side_index_map);
            }
        }
        else {
            // open H5 file
            H5File mapping_file(file_path);

            // first the volume
            const auto dataset_volume
                = _config.get<std::string>("grid.mapping.volume");
            element_index_map_global = read_mapping_dataset(mapping_file,
                                                            dataset_volume,
                                                            _cells);

            // .. on to boundaries!
            const std::string config_section = "grid.mapping.boundary";
            for (auto&& side : Boundary::boundaries())
            {
                const auto boundary_cells = with_dim_ignored(_cells, side);
                const auto config_key = config_section
                                        + Boundary::to_string(side);
                const auto dataset_boundary
                    = _config.get<std::string>(config_key);
                const auto side_index_map
                    = read_mapping_dataset(mapping_file,
                                           dataset_boundary,
                                           boundary_cells);
                boundary_index_map_global.emplace(side, side_index_map);
            }
        }

        if (this->_gv.comm().size() > 1)
        {
            // load-balance the grid to all processors
            // (might have happened already)
            this->_grid->loadBalance();
            this->_mapper.update();

            // broadcast the index vectors
            // this->broadcast_contiguous_container(element_index_map_global);

            // localize
            this->_element_index_map
                = localize_element_index_map(element_index_map_global);
        }
        // just copy in sequential mode
        else {
            this->_element_index_map = element_index_map_global;
        }

        this->_boundary_index_map
            = build_local_boundary_index_map(boundary_index_map_global);
    }

private:
    /// Map a (global) position on the grid to continuous index
    /** \param position The (center) position of a grid element
     *  \param extensions The physical extensions of the global grid
     *  \param cells The number of cells in each direction of the global grid
     *  \return Index for querying the continuous array.
     */
    template<int dim1, size_t dim2>
    size_t map_position_to_index (const Domain<dim1>& position,
                                  const Domain<dim1>& extensions,
                                  const MultiIndex<dim2>& cells) const
    {
        const auto multi_index = map_to_multi_index(position,
                                                    extensions,
                                                    cells);
        return multi_index_to_index(multi_index, cells);
    }

    /// Map a (global) position on the grid to a multi index
    /** \param position The (center) position of a grid element
     *  \param extensions The physical extensions of the global grid
     *  \param cells The number of cells in each direction of the global grid
     *  \return The multi index for querying a element mapping: (x, y[, z])
     */
    template<int dim1, size_t dim2>
    MultiIndex<dim2> map_to_multi_index (const Domain<dim1>& position,
                                        const Domain<dim1>& extensions,
                                        const MultiIndex<dim2>& cells) const
    {
        MultiIndex<dim2> ret;
        for (size_t i = 0; i < dim2; ++i) {
            ret.at(i) = std::floor(
                position[i] * cells.at(i) / extensions[i]);
        }

        return ret;
    }

    /// Create a single index from multi indices
    /** This assumes that the first index one runs fastest: x->y->z.
     *  \param multi_index Multi index to transform
     *  \param cells The number of cells in each direction of the global grid
     *  \return Index for querying the continuous array.
     */
    template<size_t dim>
    size_t multi_index_to_index (const MultiIndex<dim>& multi_index,
                                 const MultiIndex<dim>& cells) const
    {
        size_t ret = 0;

        // stack the cell count with lower dimensions
        auto stack_cell_count = [&cells](const size_t i) {
            return std::accumulate(cells.begin(),
                                   std::next(cells.begin(), i),
                                   1,
                                   std::multiplies<size_t>());
        };

        // accumulate lower dimensions
        for (size_t i = 0; i < dim; ++i) {
            ret += multi_index.at(i) * stack_cell_count(i);
        }

        return ret;
    }

    /// Return element mapping retrieved from a mapping file
    /** Load the `Grid::dimension`-dimensional element mapping dataset and
     *  compare its shape with the grid cells. Return the flattened array.
     *
     *  This function first tries to interpret the `dataset_name` as integer.
     *  If this succeeds, the function calls `create_global_index_map` with
     *  this number instead of opening a dataset.
     *
     *  \param mapping_file The H5file to read from
     *  \param dataset_name The name of the dataset to open
     *  \param cells_expected The expected extensions of the dataset
     *  \return Global element index map (directly read from file)
     *  \throw Dataset does not conform to grid shape
     */
    template<size_t dim>
    Map read_mapping_dataset (H5File& mapping_file,
                              const std::string dataset_name,
                              const MultiIndex<dim>& cells_expected) const
    {
        // dataset_name might be an index
        try {
            const auto global_index = std::stoi(dataset_name);
            this->_log->trace("Interpreting specified dataset name '{}' as "
                              "global mapping index '{}'",
                              dataset_name, global_index);
            return create_global_index_map(global_index, cells_expected);
        }
        catch (std::invalid_argument& exc) {
            // continue with reading dataset
        }
        catch (...) {
            throw; // unexpected error
        }

        // load heterogeneous mapping
        this->_log->debug("Loading mapping from file. "
                          "File: {}, Dataset: {}",
                          mapping_file.path(), dataset_name);

        // Read the data
        Map element_index_map_global;
        Map cells;
        mapping_file.read_dataset(dataset_name,
                                  H5T_NATIVE_INT,
                                  element_index_map_global,
                                  cells
                                 );

        // shape has to be reversed
        std::reverse(begin(cells), end(cells));

        // check if cells has correct size and extensions
        if (cells.size() != dim) {
            this->_log->error("Mapping dataset dimensions do not match "
                              "grid dimensions. Expected: {}, Read: {}",
                              dim, cells.size());
            DUNE_THROW(IOError, "Mapping dataset has wrong dimensions!");
        }
        if (not std::equal(cells.begin(), cells.end(), cells_expected.begin()))
        {
                this->_log->error("Mapping dataset shape does not match "
                                  "grid cells. Expected: {}, Read: {}",
                                  to_string(cells_expected), to_string(cells));
                DUNE_THROW(IOError, "Mapping dataset does not match to"
                                    " the grid");
        }

        return element_index_map_global;
    }

    /// Return element mapping for a global index
    /** Read the global index from the entry in the config file
     *  \param index The index every entity should map to
     *  \param cells The number of cells in each direction
     *      (used for calculating the total size)
     *  \return Global element index map mapping to the single global index.
     */
    template<size_t dim>
    Map create_global_index_map (const size_t index,
                                 const MultiIndex<dim> cells) const
    {
        const auto total_size = std::accumulate(cells.begin(),
                                                cells.end(),
                                                1,
                                                std::multiplies<size_t>());

        // fill index map with the global id and copy over the cells
        // this->_log->debug("Mapping global medium index '{}' to entire grid",
                        //   index);
        return Map(total_size, index);
    }

    /// Build a local element_index_map from the global one and cell indices.
    /** This method assumes that the global index mapping is based on the
     *  cells' positions on the grid and that indices follow the spatial
     *  dimensions, where x runs faster than y and z.
     *
     *  \param element_index_map_global Global index map to localize
     *  \return Local element index map
     */
    Map localize_element_index_map (const Map& element_index_map_global) const
    {
        // prepare new map in correct size for this process
        Map ret(this->_mapper.size(), 0);

        // iterate over cells on this process
        for (const auto& cell : elements(this->_gv)) {
            const auto index = this->_mapper.index(cell);
            const auto pos = cell.geometry().center();
            ret.at(index) =
                element_index_map_global.at(map_position_to_index(pos,
                                                                  _extensions,
                                                                  _cells));
        }

        return ret;
    }

    /// Build a local boundary index map from the global one and coordinates
    /** This method assumes that the mapping is based on the boundary faces'
     *  positions on the grid and that indices follow the spatial dimensions,
     *  where x runs fastest.
     *
     *  \param boundary_index_map_global A map containing a `dim-1`-dimensional
     *      mapping dataset for each boundary side.
     *  \return Map from boundary segment index to specified index
     */
    Map build_local_boundary_index_map (
        const std::map<typename Boundary::Side, Map>& boundary_index_map_global
    ) const
    {
        Map ret(this->_grid->numBoundarySegments(), 0);

        // iterate over cells on this process
        for (const auto& cell : elements(this->_gv)) {
            if (not cell.hasBoundaryIntersections()) {
                continue;
            }

            // iterate over cell boundaries
            for (const auto& is : intersections(this->_gv, cell))
            {
                if (not is.boundary()) {
                    continue;
                }

                // get the map of the boundary side
                const auto pos = is.geometry().center();
                const auto side = Boundary::side(pos, _extensions);
                const auto it = boundary_index_map_global.find(side);
                if (it == boundary_index_map_global.end()) {
                    DUNE_THROW(InvalidStateException, "Boundary side not part "
                                                      "of global boundary "
                                                      "index map");
                }
                const auto& side_map = it->second;

                // get cells and extension for this side
                const auto pos_side = with_dim_ignored(pos, side);
                const auto ext_side = with_dim_ignored(_extensions, side);
                const auto cells_side = with_dim_ignored(_cells, side);

                // use the mapping now
                const auto bc_index = is.boundarySegmentIndex();
                ret.at(bc_index) = side_map.at(map_position_to_index(
                    pos_side, ext_side, cells_side));
            }
        }

        return ret;
    }

    /// Remove an unused dimension from a MultiIndex
    /** \param in The MultiIndex to be shortened
     *  \param boundary The boundary side where the MultiIndex will be applied
     *      to.
     *  \return The MultiIndex with the projection dimension removed
     */
    template<size_t dim>
    MultiIndex<dim-1> with_dim_ignored (const MultiIndex<dim>& in,
                                        const typename Boundary::Side boundary) const
    {
        const auto ignore_dim = Boundary::dimension(boundary);
        MultiIndex<dim-1> ret;
        copy_if_not_ignored(in, ret, ignore_dim);
        return ret;
    }

    /// Remove an unused dimension from a position
    /** \param in The position to be shortened
     *  \param boundary The boundary side where the position vector
     *      will be applied to.
     *  \return The position with the projection dimension removed
     */
    template<int dim>
    Domain<dim-1> with_dim_ignored (const Domain<dim>& in,
                                    const typename Boundary::Side boundary) const
    {
        const auto ignore_dim = Boundary::dimension(boundary);
        Domain<dim-1> ret;
        copy_if_not_ignored(in, ret, ignore_dim);
        return ret;
    }

    /// Copy all but one dimension from one array to the other
    /** \param[in] vec1 The full vector to copy
     *  \param[out] vec2 The new vector
     *  \param[in] ignored The dimension which will be ignored when copying
     */
    template<typename Vec1, typename Vec2>
    void copy_if_not_ignored (const Vec1& vec1,
                              Vec2& vec2,
                              const size_t ignored) const
    {
        auto it = vec2.begin();
        for (size_t i = 0; i < vec1.size(); ++i)
        {
            if (i == ignored) {
                continue;
            }

            *it = vec1[i];
            it++;
        }
    }
};

} // namespace Dorie
} // namespace Dune

#endif // DUNE_DORIE_UTIL_GRID_MAPPER_HH
