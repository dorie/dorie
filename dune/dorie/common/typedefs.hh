#ifndef DUNE_DORIE_TYPEDEFS_HH
#define DUNE_DORIE_TYPEDEFS_HH

#include <vector>

#include <dune/common/fvector.hh>
#include <dune/common/fmatrix.hh>

#include <dune/geometry/type.hh>

namespace Dune {
namespace Dorie {

namespace Operator {

//! BC types implemented in the local operator(s)
enum class BCType
{
    Neumann, //!< Fixed flux at boundary
    Dirichlet, //!< Fixed matric head at boundary
	Outflow, //!< Free flow of solute through boundary
    none //!< No BC specified
};

/// Modes for the Dirichlet BC of the solute transport local operators
struct DirichletMode
{
  enum Type { SoluteConcentration, //!< Concentration in water phase
              TotalSolute, //!< Concentration in total volume
            };
};

} // namespace Operator

/**
 * @brief      Enum class for output policy. It defines when a simulation class
 *             should produce an output
 */
enum class OutputPolicy {None, EndOfTransportStep, EndOfRichardsStep};

/**
 * @brief      Enum class for output policy. It defines which variable
 *             is the target in order to mark the grid
 */
enum class AdaptivityPolicy {None, WaterFlux, SoluteFlux};

/// A wrapper for begin and end time stamps of a time interval
/** \tparam TF Arithmetic data type for time stamps
 */
template<typename TF>
struct TimeInterval {
    TF begin;
    TF end;
};

/// Traits struct defining basic types based on grid an geometry
template<class GridType, Dune::GeometryType::BasicType GeometryType>
struct BaseTraits
{
    static constexpr int dim = GridType::dimension;
    static constexpr Dune::GeometryType::BasicType GridGeometryType = GeometryType;

    using TF = double;
    using TimeField = TF;

    using RF = double;
    using RangeField = RF;
    using Array = std::vector<RangeField>;
    using Scalar = Dune::FieldVector<RF,1>;
    using Vector = Dune::FieldVector<RF,dim>;
    using Tensor = Dune::FieldMatrix<RF,dim,dim>;
    using Index = unsigned int;
    using IndexArray = Dune::FieldVector<Index,dim>;

    using Grid = GridType;
    using DomainField = typename Grid::ctype;
    using DF = DomainField;
    using Domain = Dune::FieldVector<DF,dim>;
    using IntersectionDomain = Dune::FieldVector<DF,dim-1>;

    using GV = typename Grid::LeafGridView;
    using GridView = GV;
    using Element = typename GV::Traits::template Codim<0>::Entity;
    using Intersection = typename GV::Traits::Intersection;
    //using GFS = GridFunctionSpaceHelper<GV,RF,order,GeometryType>;
    //using AdaptivityGFS = GridFunctionSpaceHelper<GV,RF,0,GeometryType>;
    //using LinearSolverGFS = GridFunctionSpaceHelper<GV,RF,0,GeometryType>;

};

} // namespace Dorie
} // namespace Dune

#endif
