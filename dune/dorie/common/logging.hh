#ifndef DUNE_DORIE_LOGGING_HH
#define DUNE_DORIE_LOGGING_HH

#include <memory>
#include <string>

#include <spdlog/spdlog.h>
#include <spdlog/sinks/stdout_color_sinks.h>

#include <dune/common/exceptions.hh>
#include <dune/common/parallel/mpihelper.hh>

namespace Dune {
namespace Dorie {

/**
 * \addtogroup Logging
 * @{
 */

/// The name of the basic/common logger
inline const std::string log_base = "base";
/// The name of the Richards model logger
inline const std::string log_richards = "RIC";
/// The name of the Transport model logger
inline const std::string log_transport = "SOL";
/// The name of the Richards-Transport coupling model logger
inline const std::string log_coupling = "CPL";

/// Wrapper around the function spdlog::get, which throws exceptions
/** \param name The name of the logger to retrieve
 *  \throw InvalidStateException The logger does not exist
 */
inline std::shared_ptr<spdlog::logger> get_logger (const std::string name)
{
    auto logger = spdlog::get(name);
    if (not logger) {
        DUNE_THROW(InvalidStateException, "Logger " << name <<
                   " does not exist");
    }
    return logger;
}

/// Restrict this function to the current scope
namespace
{

    /// Create a logger or retrieve it if it already exists
    /** \param name The name of the logger (also used for lookup)
     *  \param warn_on_exist Issue a warning if the logger already exists.
     */
    std::shared_ptr<spdlog::logger> create_or_get (
        const std::string name,
        const bool warn_on_exist=true)
    {
        auto logger = spdlog::get(name);
        if (not logger) {
            logger = spdlog::stdout_color_mt(name);
        }
        else if (logger && warn_on_exist) {
            logger->warn("Skipping creation of logger '{}' because "
                        "it already exists.", name);
        }

        return logger;
    }
}

/// Create a logger with a certain name and log level.
/** If created in parallel, reduce log level for loggers that are not
 *  on the primary process (rank 0). Sets new logging levels if the
 *  logger(s) already exist(s).
 *  \param name Name of the logger
 *  \param helper The Dune MPIHelper
 *  \param level The log level of the (primary) logger.
 *  \param level_par The log level of the secondary loggers, if the
 *      program runs in parallel.
 */
inline std::shared_ptr<spdlog::logger> create_logger (
    const std::string name,
    const Dune::MPIHelper& helper,
    const spdlog::level::level_enum level = spdlog::level::warn,
    const spdlog::level::level_enum level_par = spdlog::level::err
)
{
    // create the logger if it does not already exist
    auto logger = create_or_get(name);

    // Set to secondary level if logger runs on secondary process
    if (helper.rank() == 0) {
        logger->set_level(level);
    }
    else {
        logger->set_level(level_par);
    }

    // If run in parallel, add process ID to pattern
    if (helper.size() > 1) {
        // "[HH:MM:SS.mmm level] (Proc ID) [logger]  <message>"
        logger->set_pattern("%^[%T.%e %L]%$ (%P) [%n]  %v");
    }
    else {
        // "[HH:MM:SS.mmm level] [logger]  <message>"
        logger->set_pattern("%^[%T.%e %L]%$ [%n]  %v");
    }
    return logger;
}

/// Create the basic DORiE logger with a certain log level and fixed name.
/** If created in parallel, reduce log level for loggers that are not
 *  on the primary process (rank 0). Sets new logging levels if the
 *  logger(s) already exist(s). The logger automatically receives the name
 *  Dune::Dorie::log_base and can be retrieved with its value.
 *  \param helper The Dune MPIHelper
 *  \param level The log level of the (primary) logger.
 *  \param level_par The log level of the secondary loggers, if the
 *      program runs in parallel.
 */
inline std::shared_ptr<spdlog::logger> create_base_logger (
    const Dune::MPIHelper& helper,
    const spdlog::level::level_enum level = spdlog::level::warn,
    const spdlog::level::level_enum level_par = spdlog::level::err
)
{
    // retrieve the usual logger
    auto logger = create_logger(log_base,
                                helper,
                                level,
                                level_par);

    // Set different pattern without logger name
    // If run in parallel, add process ID to pattern
    if (helper.size() > 1) {
        // "[HH:MM:SS.mmm level] (Proc ID)  <message>"
        logger->set_pattern("%^[%T.%e %L]%$ (%P)  %v");
    }
    else {
        // "[HH:MM:SS.mmm level]  <message>"
        logger->set_pattern("%^[%T.%e %L]%$  %v");
    }

    return logger;
}

/// Create the basic DORiE logger with a certain log level and fixed name.
/** The logger automatically receives the name Dune::Dorie::log_base
 *  and can be retrieved with its value. This functions creates copies of the
 *  logger on all processes and is intended to be used for (fatal) error
 *  messages upon stack unwinding.
 *  \param level The log level of the logger.
 */
inline std::shared_ptr<spdlog::logger> create_base_logger (
    const spdlog::level::level_enum level = spdlog::level::warn
)
{
    // create or fetch the logger, do not issue warnings
    auto logger = create_or_get(log_base, false);

    // "[HH:MM:SS.mmm level]  <message>"
    logger->set_pattern("%^[%T.%e %L]%$  %v");

    return logger;
}

template<typename Vec>
inline std::string to_string (const Vec& vec)
{
    std::string ret;
    for (auto&& value : vec) {
        static_assert(std::is_arithmetic_v<
                        std::remove_reference_t<decltype(value)>>,
                      "Only arithmetic types can be transformed to a string");
        ret += std::to_string(value) + ", ";
    }
    // remove final comma
    return ret.substr(0, ret.length()-2);
}

/**
 * @} // addtogroup Logging
 */

} // namespace Dorie
} // namespace Dune

#endif // DUNE_DORIE_LOGGING_HH
