#ifndef DUNE_DORIE_UTIL_BOUNDARY_CONDITION_HH
#define DUNE_DORIE_UTIL_BOUNDARY_CONDITION_HH

#include <vector>
#include <array>
#include <string>
#include <sstream>
#include <fstream>
#include <iostream>

#include <dune/common/exceptions.hh>

#include <dune/dorie/common/typedefs.hh>

namespace Dune{
namespace Dorie{

	/// Abstract Class providing the BC readout interface.
	/** Define the virtual public functions queried by the Dune::Dorie::FlowBoundary and
	 *  Dune::Dorie::CalculationController classes, as well as the BC Side struct for convenience.
	 *  This class should be inherited by all classes implementing a BC file readout interface.
	 *
	 *  @ingroup NonCollective
	 */
	template<typename Traits>
	class BCReadoutInterface{

	protected:

		typedef typename Traits::RangeField         RF;
		typedef typename Traits::Domain             Domain;

		enum {dim = Traits::dim};

		//! BC Side definition for better readability
		struct BC{
			enum Side {North,South,West,East,Front,Back,None};
		};

	public:

		/// Empty Constructor
		BCReadoutInterface () = default;

		/// Empty Destructor
		virtual ~BCReadoutInterface () = default;

		virtual BoundaryCondition::Type getBCtype (const Domain& xGlobal, const RF& time) const = 0;

		virtual RF getDirichletValue (const Domain& xGlobal, const RF& time) const = 0;

		virtual RF getNeumannValue (const Domain& xGlobal, const RF& time) const = 0;

		virtual RF getNextTimeStamp (const RF& time) const = 0;

	};

	/// Boundary Condition Input Adapter for Rectangular Grid input
	/** This class inherits the BCReadoutInterface class and provides the input
	 *  interface for a rectangular grid input: The boundary is segmented into rectangles.
	 *  Different BCs and condition values can be specified for every rectangle.
	 *  The BCs are not steady over the borders of the rectangles.
	 */
	template<typename Traits>
	class RectangularGrid final : public BCReadoutInterface<Traits>
	{

	private:

		typedef typename Traits::RangeField         RF;
		typedef typename Traits::Domain             Domain;
		typedef typename Traits::DomainField 		DF;
		typedef typename Dune::Dorie::BCReadoutInterface<Traits>::BC BC;

		enum {dim = Traits::dim};

		/// Container for BC data of a single segment (2D) or rectangle (3D)
		/** Every vector should have as many entries as there are changes in the BCs, i.e. time stamps.
		 *  If the BC type does not account for a respective value, it is set to 0.0. This means that,
		 *  for example, a flux value could be called for a Dirichlet BC. This has to be avoided at the
		 *  value query itself.\n
		 *  For the special case of a No-Flow-Boundary, the arrays have the size 1. Which boundaries are
		 *  declared as such is saved in the noFlowBoundary variable.
		 *  \see initBCdata()
		 *  \see readBCresolution()
		 */
		struct bcSectionData {
			std::vector<BoundaryCondition::Type> type; //!< Type of Boundary Condition \see Dune::Dorie::BoundaryCondition
			std::vector<RF> fluxValue; //!< Value of flux
			std::vector<RF> headValue; //!< Value of matric head
		};

		std::vector<RF> bcTimeStamps; //!< Vector containig all times at which BCs change

		const Domain extensions; 				//!< spatial dimensions of the domain
		const DF width 	= extensions[0]; 		//!< Domain width
		const DF height = extensions[dim-1]; 	//!< Domain height. This must be set to the maximum height of the domain!
		const DF depth 	= extensions[1]; 		//!< Domain depth. depth=height for dim=2
		const std::string bcFilePath; 	//!< Path to file containing BC data
		const RF tStart; 	//!< Starting time of model
		const RF tEnd; 		//!< Finish time of model

		//! Boolean whether consecutive BC values of the same type will be interpolated.
		const bool interpolate;
		//! Number of boundaries. Only necessary for setup.
		const unsigned int bcSides = (dim==2 ? 4 : 6);
		//! Number of lines in the BC data file containing the spatial resolution. Only necessary for setup.
		const unsigned int headerLines = (dim==2 ? 4 : 12);
		//! Array containing the vectors with entries of spatial resolution. Row(2D:4 - 3D:12): BC border. Entries (col): Segmentation.
		std::array<std::vector<DF>, (dim==2 ? 4 : 12)> spatialResolution;
		//! Array with the number of BC indices for every side
		std::array<unsigned int, (dim==2 ? 4 : 6)> bcIndices;
		//! Array stating if a boundary has no segmentation and no user specified BC values. Boundary will be set to j=0 (no flow)
		std::array<bool, (dim==2 ? 4 : 6)> noFlowBoundary;
		//! Matrix containing all BC information. Row: BC side. Col: BC index on this side.
		std::array<std::vector<bcSectionData>, (dim==2 ? 4 : 6)> bcData;

		const RF eps; 		//!< Error margin for spatial query

	public:

		/*-----------------------------------------------------------------------*//**
		 * @brief      Read necessary parameters. Then read out BC data file
		 *             with rectangular boundary decomposition.
		 *
		 * @throws     Dune::IOError  BC File path not specified
		 *
		 * @param[in]  config  The configuration
		 */

		RectangularGrid (const ParameterTree& config)
		: BCReadoutInterface<Traits>()
		, extensions(config.get<Domain>("grid.extensions"))
		, bcFilePath(config.get<std::string>("boundary.file"))
		, tStart(config.get<RF>("time.start"))
		, tEnd(config.get<RF>("time.end"))
		, interpolate(config.get<bool>("boundary.interpolateBCvalues"))
		, eps(1e-8)
		{
			// Check if bcFilePath is specified
			if(bcFilePath.empty())
				DUNE_THROW(IOError,"BC file path not specified!");

			// initialize array
			for(unsigned int i=0; i<bcSides; i++)
				noFlowBoundary[i]=false;
			// read file and save data
			readBCfile();
		}

		/// Returns the time value of the next BC change. Returns -1 if there is none.
		RF getNextTimeStamp (const RF& time) const override
		{
			for(auto it : bcTimeStamps){
				if(it>time)
					return it;
			}
			return -1.0;
		}


	/******************************************
	*	MAIN BC QUERY FUNCTIONS
	*	Replacing virtual function definitions
	* 	Called by RichardsBoundary class
	******************************************/

		/// Return Boundary Condition Type at given global position and time
		/** * Determine boundary side
		 *  * Check for noFlowBoundary
		 *  * Determine boundary part index
		 *  * Determine time index
		 *  * Read out BC data \see Dune::Dorie::RectangularGrid::bcSectionData
		 *  \param xGlobal Global coordinates
		 *  \param time Current time
		 *  \return BoundaryCondition::Type
		 */
		BoundaryCondition::Type getBCtype (const Domain& xGlobal, const RF& time) const override
		{
			const typename BC::Side bcSide = getBCside(xGlobal);

			if (bcSide==BC::None)
				return BoundaryCondition::Other;
			if (noFlowBoundary[bcSide]){
				return bcData[bcSide][0].type[0];
			}

			const int bcIndex = getBCindex(xGlobal,bcSide);
			const unsigned int bcTimeIndex = getBCtime(time);

			return bcData[bcSide][bcIndex].type[bcTimeIndex];
		}

		/// Return Dirihclet Value at given global position and time
		/** * Determine boundary side
		 *  * Determine boundary part index
		 *  * Determine time index
		 *  * Interpolate (if enabled)
		 *  * Read out BC data \see Dune::Dorie::RectangularGrid::bcSectionData
		 *  \param xGlobal Global coordinates
		 *  \param time Current time
		 *  \return Dirihclet Value
		 *  \throw Dune::Exception Boundary side not determined successfully
		 *  \throw Dune::Exception Dirichlet BC is queried at no-flow boundary
		 */
		RF getDirichletValue (const Domain& xGlobal, const RF& time) const override
		{
			const typename BC::Side bcSide = getBCside(xGlobal);

			if (bcSide==BC::None)
				DUNE_THROW(Exception,"Could not determine at which boundary we are!");
			if (noFlowBoundary[bcSide])
				DUNE_THROW(Exception,"Query for Dirichlet BC value at no flow boundary!");

			const int bcIndex = getBCindex(xGlobal,bcSide);
			const unsigned int bcTimeIndex = getBCtime(time);

			const RF val0 = bcData[bcSide][bcIndex].headValue[bcTimeIndex];

			if(interpolate && bcTimeIndex+1<bcTimeStamps.size())	// we are not at the last time stamp
			{
				const RF val1 = bcData[bcSide][bcIndex].headValue[bcTimeIndex+1];
				return interpolateValues(val0,val1,bcTimeStamps[bcTimeIndex],bcTimeStamps[bcTimeIndex+1],time);
			}

			return val0;
		}

		/// Return Neumann Value at given global position and time
		/** * Determine boundary side
		 *  * Determine boundary part index
		 *  * Determine time index
		 *  * Interpolate (if enabled)
		 *  * Read out BC data \see Dune::Dorie::RectangularGrid::bcSectionData
		 *  \param xGlobal Global coordinates
		 *  \param time Current time
		 *  \return Neumann Value
		 *  \throw Dune::Exception Boundary side not determined successfully
		 */
		RF getNeumannValue (const Domain& xGlobal, const RF& time) const override
		{
			const typename BC::Side bcSide = getBCside(xGlobal);

			if (bcSide==BC::None)
				DUNE_THROW(Exception,"Could not determine at which boundary we are!");
			if (noFlowBoundary[bcSide]){
				return bcData[bcSide][0].fluxValue[0];
			}

			const int bcIndex = getBCindex(xGlobal,bcSide);
			const unsigned int bcTimeIndex = getBCtime(time);

			const RF val0 = bcData[bcSide][bcIndex].fluxValue[bcTimeIndex];

			if(interpolate && bcTimeIndex+1<bcTimeStamps.size()) 	// we are not at the last time stamp
			{
				const RF val1 = bcData[bcSide][bcIndex].fluxValue[bcTimeIndex+1];
				return interpolateValues(val0,val1,bcTimeStamps[bcTimeIndex],bcTimeStamps[bcTimeIndex+1],time);
			}

			return val0;
		}

	private:

	/******************************************
	*	BC QUERY HELPER FUNCTIONS
	* 	Translating coordinates and time into
	* 	class-internal indices
	******************************************/

		/// Linear interpolation between the points (t0,y0) and (t1,y1), given t
		RF interpolateValues (const RF& y0, const RF& y1, const RF& t0, const RF& t1, const RF& t) const
		{
			return y0 + (y1-y0)*(t-t0)/(t1-t0);
		}

		/// Returns time stamp index according to real time value
		unsigned int getBCtime (const RF& time) const
		{
			auto it=bcTimeStamps.begin();
			unsigned int n=0;
			while(it!=bcTimeStamps.end() && *it<=time){
				it++; n++;
			}
			return n-1;
		}

		/// Returns boundary index as enum (can be interpreted as int), given the global Coordinate
		/** \todo Simplify spatial query ?
		 */
		typename BC::Side getBCside(const Domain& xGlobal) const
		{
			if(dim==2){
				if(xGlobal[1]<eps && xGlobal[0]>eps && xGlobal[0]<width-eps)
					return BC::South;
				if(xGlobal[0]<eps && xGlobal[1]>eps)
					return BC::West;
				if(xGlobal[0]>width-eps && xGlobal[1]>eps)
					return BC::East;
				if(xGlobal[1]>eps && xGlobal[0]>eps && xGlobal[0]<width-eps)
					return BC::North;
			}
			if(dim==3){
				if(xGlobal[0]<eps && xGlobal[2]>eps && xGlobal[1]>eps && xGlobal[1]<depth-eps)
					return BC::West;
				if(xGlobal[0]>width-eps && xGlobal[2]>eps && xGlobal[1]>eps && xGlobal[1]<depth-eps)
					return BC::East;
				if(xGlobal[1]<eps && xGlobal[2]>eps && xGlobal[0]>eps && xGlobal[0]<width-eps)
					return BC::Front;
				if(xGlobal[1]>depth-eps && xGlobal[2]>eps && xGlobal[0]>eps && xGlobal[0]<width-eps)
					return BC::Back;
				if(xGlobal[2]<eps && xGlobal[0]>eps && xGlobal[0]<width-eps && xGlobal[1]>eps && xGlobal[1]<depth-eps)
					return BC::South;
				if(xGlobal[2]>eps && xGlobal[0]>eps && xGlobal[0]<width-eps && xGlobal[1]>eps && xGlobal[1]<depth-eps)
					return BC::North;
			}
			return BC::None;
		}

		/// Returns the BC index, given the boundary side and the coordinate
		/** For two dimensions we simply iterate through one spatial resolution array until a resolution value is
		 *  larger than the coordinate. For three dimensions, we need to do this for two spatial resolution arrays.
		 *  Afterwards, we have to calculate the index. The first vector of spatial resolution always runs fastest.
		 *  \throw Dune::Exception Position query exceeds last segment of spatial resolution
		 *      (This should not happen if specified extensions are >= true grid extensions)
		 */
		int getBCindex(const Domain& xGlobal, const int& bcSide) const
		{
			unsigned int index = 0;

			if(dim==2){
				// determine along which axis spatial resoultion applies
				unsigned int xGlobalIndex;
				if (bcSide == BC::North || bcSide == BC::South)
					xGlobalIndex = 0;
				else // (bcSide == BC::West || bcSide == BC::East)
					xGlobalIndex = 1;

				// run along axis
				auto it=spatialResolution[bcSide].begin();
				while(*it<xGlobal[xGlobalIndex]){
					it++; index++;
					if(it==spatialResolution[bcSide].end())
						DUNE_THROW(Exception,"BC spatial query exceeds the last segment!");
				}
			} // dim == 2
			else{
				unsigned int x0,x1,sr0,sr1;
				if(bcSide==BC::North){
					x0=0; 	// West-East
					x1=1; 	// Front-Back
					sr0=0; sr1=1;
				}
				else if(bcSide==BC::South){
					x0=0; 	// West-East
					x1=1; 	// Front-Back
					sr0=2; sr1=3;
				}
				else if(bcSide==BC::West){
					x0=2; 	// South-North
					x1=1;	// Front-Back
					sr0=4; sr1=5;
				}
				else if(bcSide==BC::East){
					x0=2; 	// South-North
					x1=1;	// Front-Back
					sr0=6; sr1=7;
				}
				else if(bcSide==BC::Front){
					x0=2; 	// South-North
					x1=0; 	// West-East
					sr0=8; sr1=9;
				}
				else if(bcSide==BC::Back){
					x0=2; 	// South-North
					x1=0; 	// West-East
					sr0=10; sr1=11;
				}
				else {
					DUNE_THROW(Exception, "Unrecognized boundary condition type");
				}

				// Spatial query
				unsigned int c0=0,c1=0;
				auto it0 = spatialResolution[sr0].begin();
				auto it1 = spatialResolution[sr1].begin();
				while(*it0<xGlobal[x0]){
					it0++; c0++;
					if(it0==spatialResolution[sr0].end())
						DUNE_THROW(Exception,"BC spatial query exceeds the last segment.");
				}
				while(*it1<xGlobal[x1]){
					it1++; c1++;
					if(it1==spatialResolution[sr1].end())
						DUNE_THROW(Exception,"BC spatial query exceeds the last segment.");
				}
				// index = row*(indicesPerRow)+col
				index = c1*spatialResolution[sr0].size() +c0;
			}

			return index;
		}


	/******************************************
	*	BC INPUT MAIN FUNCTION
	******************************************/

		/// Handle readout of the BC file specified by the user.
		/** * Read spatial segmentation
		 *  * Read number of time stamps
		 *  * Read time stamps and BCs
		 *  \throw Dune::IOError BC data file cannot be opened
		 *  \throw Dune::IOError BC data file has more lines than expected
		 */
		void readBCfile ()
		{
			std::ifstream bcFile(bcFilePath.c_str());
			if (!bcFile.is_open())
				DUNE_THROW(IOError,"Cannot open boundary condition file!");

			// initialize variables
			unsigned int changeTimes = 0;
			unsigned int counter = 0;
			std::string line;

			while (std::getline(bcFile,line)){
			if(line.find('#')==std::string::npos && !line.empty()) //ignore empty or commented lines
			{
				counter++;
				std::istringstream instream(line.c_str());

				// Spatial segmentation
				if(counter <= headerLines){
					readSpatialResolution(instream, counter);
				}

				// Number of time stamps
				else if(counter==headerLines+1){
					changeTimes = getNumberTimeStamps(instream, counter);
					// initialize data structure now that spatial resolution is known
					finalizeSpatialResolution();
					initBCdata();
				}

				// time stamps and BCs
				else if (counter > headerLines+1 && counter <= headerLines+1+changeTimes){
					readBCresolution(instream, counter);
				}

				else
					DUNE_THROW(IOError,"In BC file line " + std::to_string(counter)
							+ ": Too many lines!");
			}
			}

			// make final input checks
			userInputCheck(changeTimes);
			// DEBUGGING
			//plotBCdata();
		}


	/******************************************
	*	BC INPUT: DATA READOUT FUNCTIONS
	******************************************/

		/// Read the spatial segmentation scheme
		/** \throw Dune::IOError Previously defined No Flow Boundary receives segmentation
		 *  \throw Dune::IOError Previously segmented boundary receives No Flow Boundary
		 *  \throw Dune::IOError Segmentation does not match number of segments
		 *  \throw Dune::IOError Spatial resolution values are not stated in increasing order
		 *  \throw Dune::IOError Number of segments is integer < -1
		 *  \throw Dune::IOError Spatial resolution keywords do not match the mandatory order
		 */
		void readSpatialResolution (std::istringstream& instream, const int& counter)
		{
			std::string buffer;
			instream >> buffer;

			// Check whether BC data file is formatted correctly
			if ((dim==2 && 	((counter==1 && buffer == "spatial_resolution_north") ||
							 (counter==2 && buffer == "spatial_resolution_south") ||
							 (counter==3 && buffer == "spatial_resolution_west")  ||
							 (counter==4 && buffer == "spatial_resolution_east"))  )
				||
				(dim==3 && 	((counter == 1 && buffer == "spatial_resolution_north_we") ||
							 (counter == 2 && buffer == "spatial_resolution_north_fb") ||
							 (counter == 3 && buffer == "spatial_resolution_south_we") ||
							 (counter == 4 && buffer == "spatial_resolution_south_fb") ||
							 (counter == 5 && buffer == "spatial_resolution_west_sn")  ||
							 (counter == 6 && buffer == "spatial_resolution_west_fb")  ||
							 (counter == 7 && buffer == "spatial_resolution_east_sn")  ||
							 (counter == 8 && buffer == "spatial_resolution_east_fb")  ||
							 (counter == 9 && buffer == "spatial_resolution_front_sn") ||
							 (counter == 10 && buffer == "spatial_resolution_front_we")||
							 (counter == 11 && buffer == "spatial_resolution_back_sn") ||
							 (counter == 12 && buffer == "spatial_resolution_back_we")) )
				)
			{
				instream >> buffer;
				const int numberSegments = stoi(buffer); 	// number of segments to be read

				// User specified segmentation has to be read
				if(numberSegments>0)
				{
					// check if the other boundary border has been set to no flow
					if(dim==3 && counter>1
					 && std::floor((counter-eps)/2) == std::floor((counter-1-eps)/2)
					 && noFlowBoundary[(int)((counter-1-eps)/2)])
						DUNE_THROW(IOError,"In BC file line " + std::to_string(counter)
							+ ": Previously specified no flow boundary cannot receive segmentation!");

					// loop over user segmentation
					for(int i=0; i<numberSegments; i++)
					{
						buffer.clear();
						instream >> buffer;
						if(buffer.empty())
							DUNE_THROW(IOError,"In BC file line " + std::to_string(counter)
								+ ": Not enough segments specified!");

						spatialResolution[counter-1].push_back(stod(buffer));

						// Check if input makes sense
						if(i>0 && spatialResolution[counter-1][i]<=spatialResolution[counter-1][i-1])
							DUNE_THROW(IOError,"In BC file line " + std::to_string(counter)
								+ ": Spatial resolution values have to be stated in increasing order!");
					}

					// check if there are stray segments given
					buffer.clear();
					instream >> buffer;
					if(!buffer.empty())
						DUNE_THROW(IOError,"In BC file line " + std::to_string(counter)
								+ ": Too many segments specified!");
				}

				// User specified that whole border is one segment
				else if(numberSegments==0){
					// check if the other boundary border has been set to no flow
					if(dim==3 && counter>1
					 && std::floor((counter-eps)/2) == std::floor((counter-1-eps)/2)
					 && noFlowBoundary[(int)((counter-1-eps)/2)])
						DUNE_THROW(IOError,"In BC file line " + std::to_string(counter)
							+ ": Previously specified no flow boundary cannot receive active boundary segment!");
					// check if there are stray segments given
					buffer.clear();
					instream >> buffer;
					if(!buffer.empty())
						DUNE_THROW(IOError,"In BC file line " + std::to_string(counter)
								+ ": Too many segments specified!");
				}

				// User specified that whole border is no flow boundary
				else if(numberSegments==-1)
				{
					if(dim==2)
						noFlowBoundary[counter-1]=true;
					else if (dim==3){
						// check if the other boundary border received segmentation
						if(counter>1
						 && std::floor((counter-eps)/2) == std::floor((counter-1-eps)/2)
						 && !noFlowBoundary[(int)((counter-1-eps)/2)])
							DUNE_THROW(IOError,"In BC file line " + std::to_string(counter)
								+ ": Previously specified segmented boundary cannot receive no flow BC!");
						noFlowBoundary[(int)((counter-eps)/2)]=true;
					}
				}

				else // numberSegments<-1
					DUNE_THROW(IOError,"In BC file line " + std::to_string(counter)
						+ ": Number of segments must be an integer >=-1!");
			}

			else // formatting incorrect
				DUNE_THROW(IOError,"In BC file line " + std::to_string(counter) + ": File structure incorrect!");
		}

		/// Read out the number of time stamps for BC changes (needs to be called only once)
		/** \throw Dune::IOError Number of time stamps is stated incorrectly
		 */
		unsigned int getNumberTimeStamps (std::istringstream& instream, const int& counter) const
		{
			std::string buffer;
			instream >> buffer;
			if(buffer != "number_BC_change_times")
				DUNE_THROW(IOError,"In BC file line " + std::to_string(counter)
					+ ": Please state 'number_BC_change_times <int>' here!");
			buffer.clear();
			instream >> buffer;
			if(buffer.empty())
				DUNE_THROW(IOError,"In BC file line " + std::to_string(counter)
					+ ": Please state 'number_BC_change_times <int>' here!");
			if(stoi(buffer)<1)
				DUNE_THROW(IOError,"In BC file line " + std::to_string(counter)
					+ ": You have to state at least one time stamp!");
			return stoi(buffer);
		}

		/// Read out the time resolution
		/** \throw Dune::IOError First time stamp does not coincide with time.start key
		 *  \throw Dune::IOError Time stamps are not stated in increasing order
		 *  \throw Dune::IOError Number of BC declarations does not match spatial segmentation
		 *  \throw Dune::IOError BC type is unknown
		 *  \throw Dune::IOError Activated interpolation only: Time stamps are not the same if any BC changes in type
		 */
		void readBCresolution (std::istringstream& instream, const int& counter)
		{
			std::string buffer;
			instream >> buffer;
			// read the time
			bcTimeStamps.push_back(stod(buffer));
			// first time stamp must be at t=tstart
			if(bcTimeStamps[0]!=tStart)
				DUNE_THROW(IOError,"In BC file line " + std::to_string(counter)
					+ ": First time stamp must be time.start!");
			// check if buffer >= last time stamp
			if(bcTimeStamps.size()>1 && *(bcTimeStamps.end()-1) < *(bcTimeStamps.end()-2))
				DUNE_THROW(IOError,"In BC file line " + std::to_string(counter)
					+ ": Time stamps must be stated in increasing order!");

			// Iterate over all boundaries
			for(unsigned int i=0; i<bcSides; i++){
				if(!noFlowBoundary[i]){
					// loop over all BC indices
					for(unsigned int j=0; j<bcIndices[i]; j++){
						buffer.clear();
						instream >> buffer;
						if(buffer.empty())
							DUNE_THROW(IOError,"In BC file line " + std::to_string(counter)
								+ ": Expected " + std::to_string(bcIndices[i]-j)
								+ " more BC declaration(s) for BC side " + std::to_string(i) + "!");
						// Check the BC type
						if(buffer == "neumann"){
							bcData[i][j].type.push_back(BoundaryCondition::Neumann);
							instream >> buffer;
							bcData[i][j].fluxValue.push_back(stod(buffer));
							bcData[i][j].headValue.push_back(0.0);
						}
						else if(buffer == "dirichlet"){
							bcData[i][j].type.push_back(BoundaryCondition::Dirichlet);
							instream >> buffer;
							bcData[i][j].headValue.push_back(stod(buffer));
							bcData[i][j].fluxValue.push_back(0.0);
						}
						else if(buffer == "outflow"){
							bcData[i][j].type.push_back(BoundaryCondition::Outflow);
							instream >> buffer;
							bcData[i][j].headValue.push_back(0.0);
							bcData[i][j].fluxValue.push_back(stod(buffer));
						}
						else
							DUNE_THROW(IOError,"In BC file line " + std::to_string(counter) + ": BC side "
								+ std::to_string(i) + ": BC declaration " + std::to_string(j+1) + ": BC type unknown!");

						// Check input
						if(interpolate 											// we want to interpolate
						 && bcTimeStamps.size()>1 									// more than one BC change time
						 && *(bcTimeStamps.end()-1)!=*(bcTimeStamps.end()-2) 			// time stamp not the same as before
						 && *(bcData[i][j].type.end()-1)!=*(bcData[i][j].type.end()-2)) 	// BC type changed
							DUNE_THROW(IOError,"In BC file line " + std::to_string(counter)
								+ ": Time stamp must be the same as before in order to change a BC type!");
					}
				}
			}

			// Check for stray BCs
			buffer.clear();
			instream >> buffer;
			if(!buffer.empty())
				DUNE_THROW(IOError,"In BC file line " + std::to_string(counter)
					+ ": Too many BCs specified!");
		}


	/******************************************
	* 	BC INPUT: UTILITY FUNCTIONS
	* 	Adding static data, preparing readout
	******************************************/

		/// Add the extension of the grid at the end of every segmentation entry.
		/** \todo Check if values consistent
		 */
		void finalizeSpatialResolution ()
		{
			if(dim==2){
				spatialResolution[0].push_back(width);
				spatialResolution[1].push_back(width);
				spatialResolution[2].push_back(height);
				spatialResolution[3].push_back(height);

				for(unsigned int i=0; i<bcSides; i++)
					// number of BC indices is is given by the number of segments on the border
					bcIndices[i]=spatialResolution[i].size();
			}
			else if(dim==3){
				spatialResolution[0].push_back(width);
				spatialResolution[1].push_back(depth);
				spatialResolution[2].push_back(width);
				spatialResolution[3].push_back(depth);
				spatialResolution[4].push_back(height);
				spatialResolution[5].push_back(depth);
				spatialResolution[6].push_back(height);
				spatialResolution[7].push_back(depth);
				spatialResolution[8].push_back(height);
				spatialResolution[9].push_back(width);
				spatialResolution[10].push_back(height);
				spatialResolution[11].push_back(width);

				for(unsigned int i=0; i<bcSides; i++){
					bcIndices[i]=1;
					for(unsigned int j=i*2; j<i*2+2; j++)
						// number of BC indices is given by the product of the number of segments on the adjoined borders
						bcIndices[i] *= spatialResolution[j].size();
				}
			}
		}

		/// Initialize the bcData matrix
		void initBCdata ()
		{
			for(unsigned int i=0; i<bcSides; i++){ 	// loop over all BC sides
				if(noFlowBoundary[i]){ 			// create one BC: Neumann, j=0
					bcSectionData d;
					d.type.push_back(BoundaryCondition::Neumann);
					d.fluxValue.push_back(0.0);
					bcData[i].push_back(d);
				}
				else{ 	// create dummy data for subsequent input
					for(unsigned int j=0; j<bcIndices[i]; j++){
						bcSectionData d;
						bcData[i].push_back(d);
					}
				}
			}
		}


	/******************************************
	*	BC INPUT: HELPER FUNCTIONS
	* 	Checking input for errors
	******************************************/


		/// Make sure saved BC data makes sense
		/** \throw Dune::IOError Number of saved timestamps does not match number_BC_change_times key
		 *  \throw Dune::IOError Last time stamp is larger than time.end key
		 *  \throw Dune::IOError BC arrays have incorrect size (i.e. size /= number of time stamps)
		 *  \throw Dune::IOError Segmentation does not match saved number of BC indices
		 */
		void userInputCheck (const unsigned int& changeTimes) const
		{
			if(bcTimeStamps.size()>changeTimes || bcTimeStamps.size()<changeTimes)
				DUNE_THROW(IOError,"BC Input: Number of saved timestamps (" + std::to_string(bcTimeStamps.size())
					+ ") does not match number_BC_change_times!");

			if(*(bcTimeStamps.end()-1)>tEnd)
				DUNE_THROW(IOError,"BC Input: Last BC change value has time stamp >time.end!");

			for(unsigned int i=0; i<bcSides; i++){ 	// loop over all boundary sides
				unsigned int count=0;
				for(auto j=bcData[i].begin(); j<bcData[i].end(); j++){ 	// loop over all indices
					if(!noFlowBoundary[i]){
						if((*j).type.size()!=bcTimeStamps.size())
							DUNE_THROW(IOError,"BC Input: bcData["+std::to_string(i)+"]["+std::to_string(count)
								+"]: Wrong number of time stamps saved!");
						 // Does not work because not all values are saved to every vector!
						if((*j).fluxValue.size()!=bcTimeStamps.size())
							DUNE_THROW(IOError,"BC Input: bcData["+std::to_string(i)+"]["+std::to_string(count)
								+"]: Wrong number of time stamps saved!");
						if((*j).headValue.size()!=bcTimeStamps.size())
							DUNE_THROW(IOError,"BC Input: bcData["+std::to_string(i)+"]["+std::to_string(count)
								+"]: Wrong number of time stamps saved!");
					}
					count++;
				}
				if(count!=bcIndices[i])
					DUNE_THROW(IOError,"BC Input: For boundary " +std::to_string(i)
						+ ": Number of segments does not match bcIndices["+std::to_string(i)+"]!");
			}
		}

		/**
		 * \brief Write out saved BC data matrix for Debugging
		 */
		void plotBCdata () const
		{
			std::cout << "--- BC DATA ---" << std::endl;
			std::cout << "-> Spatial Resolution:" << std::endl;
			for(unsigned int i=0; i<headerLines; i++){
				std::cout << "Header Line " << i << ": ";
				for(auto it=spatialResolution[i].begin(); it<spatialResolution[i].end(); it++){
					std::cout << *it << " ";
				}
				std::cout << std::endl;
			}

			std::cout << "-> Time Resolution: " << std::endl;
			for(auto it=bcTimeStamps.begin(); it<bcTimeStamps.end(); it++)
				std::cout << *it << " ";
			std::cout << std::endl;

			std::cout << "-> BC Resolution: " << std::endl;
			for(unsigned int i=0; i<bcSides; i++){
				std::cout << "BC Side " << i << ":" << std::endl;
				unsigned int index=0;
				if(noFlowBoundary[i])
					std::cout << "noFlowBoundary: " << bcData[i][0].fluxValue[0] << std::endl;
				else{
					for(unsigned int j=0; j<bcIndices[i]; j++){
						for(unsigned int k=0; k<bcTimeStamps.size(); k++){
							std::cout << index << ": " << bcTimeStamps[k] << ": ";
							if(bcData[i][j].type[k]==BoundaryCondition::Neumann)
								std::cout << "neumann : ";
							else if(bcData[i][j].type[k]==BoundaryCondition::Outflow)
								std::cout << "outflow : ";
							else if(bcData[i][j].type[k]==BoundaryCondition::Dirichlet)
								std::cout << "dirichlet : ";
							else if(bcData[i][j].type[k]==BoundaryCondition::Outflow)
								std::cout << "outflow : ";
						}
						index++;
					}
				}
			}
		}

	};


}
}

#endif
