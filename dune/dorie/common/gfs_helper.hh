#ifndef DUNE_DORIE_COMMON_GFS_HELPER_HH
#define DUNE_DORIE_COMMON_GFS_HELPER_HH

#include <dune/geometry/type.hh>

#include <dune/pdelab/gridfunctionspace/gridfunctionspace.hh>
#include <dune/pdelab/constraints/p0.hh>
#include <dune/pdelab/finiteelementmap/p0fem.hh>
#include <dune/pdelab/finiteelementmap/qkdg.hh>
#include <dune/dorie/common/finite_element/pk_dg_fem.hh>

namespace Dune{
namespace Dorie{

/// Provide types and construction of the GFS for the linear solver
template<typename GridView, typename RF, Dune::GeometryType::BasicType GeometryType>
struct LinearSolverGridFunctionSpaceHelper
{};

/// Provide types and construction of the GridFunctionSpace
template<typename GridView, typename RF>
struct LinearSolverGridFunctionSpaceHelper<GridView,RF,Dune::GeometryType::BasicType::simplex>
{
private:
	static constexpr int dim = GridView::dimension;
	using DF = typename GridView::Grid::ctype;

public:
	/// Entity set of the GFS
	using ES = Dune::PDELab::OverlappingEntitySet<GridView>;
	/// FiniteElementMap type of GFS
	using FEM = typename Dune::PDELab::P0LocalFiniteElementMap<DF,RF,dim>;
	/// Constraints type of the GFS
	using CON = Dune::PDELab::P0ParallelConstraints;
	/// GFS type
	using Type = typename Dune::PDELab::GridFunctionSpace<ES,FEM,CON,
		Dune::PDELab::ISTL::VectorBackend<>>;
	/// GFS Constraints Container type
	using CC = typename Type::template ConstraintsContainer<RF>::Type;

	/// create GFS from GridView
	static Type create (const GridView& gv)
	{
		ES es(gv);
		auto fem = std::make_shared<FEM>(Dune::GeometryTypes::simplex(dim));
		auto con = std::make_shared<CON>();
		return Type(es,fem,con);
	}
};

/// Provide types and construction of the GridFunctionSpace
template<typename GridView, typename RF>
struct LinearSolverGridFunctionSpaceHelper<GridView,RF,Dune::GeometryType::BasicType::cube>
{
private:
	static constexpr int dim = GridView::dimension;
	using DF = typename GridView::Grid::ctype;

public:
	/// Entity set of the GFS
	using ES = Dune::PDELab::OverlappingEntitySet<GridView>;
	/// FiniteElementMap type of GFS
	using FEM = typename Dune::PDELab::P0LocalFiniteElementMap<DF,RF,dim>;
	/// Constraints type of the GFS
	using CON = Dune::PDELab::P0ParallelConstraints;
	/// GFS type
	using Type = typename Dune::PDELab::GridFunctionSpace<ES,FEM,CON,
		Dune::PDELab::ISTL::VectorBackend<>>;
	/// GFS Constraints Container type
	using CC = typename Type::template ConstraintsContainer<RF>::Type;

	/// create GFS from GridView
	static Type create (const GridView& gv)
	{
		ES es(gv);
		auto fem = std::make_shared<FEM>(Dune::GeometryTypes::cube(dim));
		auto con = std::make_shared<CON>();
		return Type(es,fem,con);
	}
};

/// Provide types and construction of the GridFunctionSpace. Has to be specialized.
template<typename GridView, typename RF, int order, Dune::GeometryType::BasicType GeometryType>
struct GridFunctionSpaceHelper
{};

/// Provide types and construction of the GridFunctionSpace
/*  \todo add constraints specialization depending on grid
 */
template<typename GridView, typename RF, int order>
struct GridFunctionSpaceHelper<GridView,RF,order,Dune::GeometryType::BasicType::simplex>
{
private:
	static constexpr int dim = GridView::dimension;
	using DF = typename GridView::Grid::ctype;

public:
	/// Entity set of the GFS
	using ES = Dune::PDELab::OverlappingEntitySet<GridView>;
	/// FiniteElementMap type of GFS
	using FEM = typename Dune::Dorie::PkDGLocalFiniteElementMap<DF, RF, dim, order>;
	/// Constraints type of the GFS
	using CON = Dune::PDELab::P0ParallelConstraints;
	/// GFS type
	using Type = typename Dune::PDELab::GridFunctionSpace<ES,FEM,CON,
		Dune::PDELab::ISTL::VectorBackend<
			Dune::PDELab::ISTL::Blocking::fixed,
			Dune::PB::pk_size(order, dim)
		>
	>;

	/// GFS Constraints Container type
	using CC = typename Type::template ConstraintsContainer<RF>::Type;

	/// create GFS from GridView
	static Type create (const GridView& gv)
	{
		ES es(gv);
		auto fem = std::make_shared<FEM>();
		auto con = std::make_shared<CON>();
		return Type(es,fem,con);
	}
};

/// Provide types and construction of the GridFunctionSpace
template<typename GridView, typename RF, int order>
struct GridFunctionSpaceHelper<GridView,RF,order,Dune::GeometryType::BasicType::cube>
{
private:
	static constexpr int dim = GridView::dimension;
	using DF = typename GridView::Grid::ctype;

public:
	/// Entity set of the GFS
	using ES = Dune::PDELab::OverlappingEntitySet<GridView>;
	/// FiniteElementMap type of GFS
	using FEM = typename Dune::PDELab::QkDGLocalFiniteElementMap<DF,RF,order,dim>;
	/// Constraints type of the GFS
	using CON = Dune::PDELab::P0ParallelConstraints;
	/// GFS type
	using Type = typename Dune::PDELab::GridFunctionSpace<ES,FEM,CON,
		Dune::PDELab::ISTL::VectorBackend<Dune::PDELab::ISTL::Blocking::fixed>
	>;
	/// GFS Constraints Container type
	using CC = typename Type::template ConstraintsContainer<RF>::Type;

	/// create GFS from GridView
	static Type create (const GridView& gv)
	{
		ES es(gv);
		auto fem = std::make_shared<FEM>();
		auto con = std::make_shared<CON>();
		return Type(es,fem,con);
	}
};

} // namespace Dorie
} // namespace Dune

#endif // DUNE_DORIE_COMMON_GFS_HELPER_HH
