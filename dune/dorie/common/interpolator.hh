#ifndef DUNE_DORIE_UTIL_INTERPOLATOR_HH
#define DUNE_DORIE_UTIL_INTERPOLATOR_HH

#include <cmath>
#include <memory>
#include <vector>
#include <utility>
#include <numeric>
#include <algorithm>
#include <string>
#include <string_view>

#include <dune/common/fvector.hh>
#include <dune/common/exceptions.hh>
#include <dune/common/parametertree.hh>

#include <dune/dorie/common/logging.hh>
#include <dune/dorie/common/utility.hh>
#include <dune/dorie/common/h5file.hh>

#include <yaml-cpp/yaml.h>

namespace Dune {
namespace Dorie {

/// An abstract interpolator. Stores all data necessary for evaluation.
/**
 *  \see InterpolatorFactory for conveniently creating derived objects
 *  \ingroup Interpolators
 *  \author Lukas Riedel
 *  \date 2018
 */
template<typename DataType, int dim>
class Interpolator
{
protected:
    /// Coordinate Type
    using DF = double;
    /// Physical vector type
    using Domain = Dune::FieldVector<DF, dim>;

    const std::vector<DataType> _data; //!< contiguously stored data of the array
    const std::vector<size_t> _shape; //!< inverted shape of the original array
    //! physical extensions of the dataset
    const Domain _extensions;
    //! origin offset of the dataset
    const Domain _offset;

    /// Substract the offset from the given position
    Domain sub_offset (const Domain& x) const
    {
        Domain ret = x;
        std::transform(x.begin(), x.end(), _offset.begin(), ret.begin(),
            [](const auto a, const auto b){ return a - b; });
        return ret;
    }

    /// Verify that a position is within the given extensions
    /** \param x Position with offset substracted.
     *  \return True if x is within the given extensions.
     */
    bool inside_extensions (const Domain& x) const
    {
        // check if any value is too small (below 0)
        if (std::any_of(std::begin(x), std::end(x),
                        [](const auto val){ return val < 0; }))
        {
            return false;
        }

        // check if any value is too large (> extensions)
        std::vector<bool> is_larger(x.size(), false);
        auto is_true = [](const bool val) { return val; }; // trivial, really
        using value_t = typename Domain::value_type;
        std::transform(std::begin(x),
                       std::end(x),
                       std::begin(_extensions),
                       std::begin(is_larger),
                       std::greater<value_t>()
        );
        return std::none_of(begin(is_larger), end(is_larger), is_true);
    }

public:
    /// Constructor using perfect forwarding of arguments.
    template<typename Data, typename Shape, typename Domain1, typename Domain2>
    Interpolator (Data&& data,
                  Shape&& shape,
                  Domain1&& domain,
                  Domain2&& offset) :
        _data(std::forward<Data>(data)),
        _shape(std::forward<Shape>(shape)),
        _extensions(std::forward<Domain1>(domain)),
        _offset(std::forward<Domain2>(offset))
    {
        static_assert(dim == 2 or dim == 3,
                      "This interpolator only works in 2D or 3D");
    }

    /// Delete the interpolator
    virtual ~Interpolator () = default;

    /// Evaluate the interpolator at a certain global position
    /** \param pos Position to evaluate
     *  \return Value of the interpolated function
     */
    virtual DataType evaluate (const Domain& pos) const = 0;
};

/// A nearest-neighbor interpolator in 2D and 3D
/** This interpolator iterprets its data like *cell*-data.
 *  When using a dataset with three values across the extensions of two,
 *  the data values shange at positions 2/3 and 4/3.
 *
 *  \ingroup Interpolators
 */
template<typename DataType, int dim>
class NearestNeighborInterpolator: public Interpolator<DataType, dim>
{
private:
    using Base = Interpolator<DataType, dim>;
    using Domain = typename Base::Domain;

    using DF = typename Base::DF;
    static constexpr DF eps = 1e-9;

public:
    /// Re-use the base class constructor
    using Base::Base;

    /// Export the type of this intepolator
    static inline std::string type = "nearest";

    /// Evaluate the interpolator
    DataType evaluate (const Domain& x) const override
    {
        const auto pos = this->sub_offset(x);
        if (not this->inside_extensions(pos)) {
            DUNE_THROW(Exception, "Querying interpolator data outside its "
                                  "extensions!");
        }

        Domain unit_pos = pos;
        unit_pos[0] = std::max(this->_shape[0] * pos[0] / this->_extensions[0] - 0.5, 0.0) + eps;
        unit_pos[1] = std::max(this->_shape[1] * pos[1] / this->_extensions[1] - 0.5, 0.0) + eps;
        if constexpr (dim == 3)
            unit_pos[2] = std::max(this->_shape[2] * pos[2] / this->_extensions[2] - 0.5, 0.0) + eps;

        size_t ipos[dim];
        ipos[0] = std::min((size_t)round(unit_pos[0]), this->_shape[0]-1);
        ipos[1] = std::min((size_t)round(unit_pos[1]), this->_shape[1]-1);
        if constexpr (dim == 3)
            ipos[2] = std::min((size_t)round(unit_pos[2]), this->_shape[2]-1);

        size_t index = ipos[0] + ipos[1]*this->_shape[0];
        if constexpr (dim == 3)
            index += ipos[2]*this->_shape[0]*this->_shape[1];

        return this->_data[index];
    }

    /// Delete this interpolator.
    ~NearestNeighborInterpolator() override = default;
};

/// Linear interpolator for 2D and 3D.
/** The implementation is based on articles
 *  https://en.wikipedia.org/wiki/Bilinear_interpolation
 *  and https://en.wikipedia.org/wiki/Trilinear_interpolation
 *
 *  \ingroup Interpolators
 *  \author Lukas Riedel
 *  \date 2019
 */
template<typename DataType, int dim>
class LinearInterpolator : public Interpolator<DataType, dim>
{
private:
    using Base = Interpolator<DataType, dim>;
    using Domain = typename Base::Domain;
    using DF = typename Base::DF;
    using MultiIdx = std::array<size_t, dim>;

    /// Get the position vector in mesh units
    /** \param pos Global position vector
     *  \return Position vector in mesh units
     */
    Domain get_pos_unit (const Domain& pos) const
    {
        Domain pos_unit;
        for (size_t i = 0; i < pos.size(); ++i) {
            pos_unit[i] = pos[i] * (this->_shape[i] - 1)
                                 / this->_extensions[i];
        }

        return pos_unit;
    }

    /// Return the position difference vector in mesh units
    /** The difference is calculated by using the (front) lower left corner
     *  as origin.
     *  \param pos_unit Position vector in mesh units, see get_pos_unit()
     *  \pararm indices The multi index of (front) lower left corner
     *  \return Position difference vector in mesh units
     */
    Domain get_pos_unit_diff (const Domain& pos_unit, const MultiIdx& indices)
        const
    {
        Domain pos_diff;
        for (size_t i = 0; i < pos_unit.size(); ++i) {
            pos_diff[i] = pos_unit[i] - indices[i];
        }
        return pos_diff;
    }

    /// Get the indices for accessing the data knots
    /** \param pos_unit The position in mesh units
     *  \return Indices in every dimension of the mesh
     */
    std::pair<MultiIdx, MultiIdx> get_knot_indices (
        const Domain& pos_unit
    ) const
    {
        // round mesh unit positions up and down
        MultiIdx idx_lower, idx_upper;
        for (size_t i = 0; i < idx_lower.size(); ++i) {
            idx_lower[i] = std::max(std::floor(pos_unit[i]), 0.0);
            idx_upper[i] = std::min<size_t>(std::ceil(pos_unit[i]),
                                            this->_shape[i]-1);
        }

        return std::make_pair(idx_lower, idx_upper);
    }

    /// Transform a multi index into a index for accessing the data array
    /** \param indices The multi index to transform
     *  \return The index to query the data set #_data
     */
    size_t to_index (const MultiIdx& indices) const
    {
        size_t index = indices[0] + indices[1] * this->_shape[0];
        if constexpr (dim == 3) {
            index += indices[2] * this->_shape[1] * this->_shape[0];
        }

        return index;
    }

public:
    /// Export the type of this intepolator
    static inline constexpr std::string_view type = "linear";

    /// Re-use the base class constructor
    using Base::Base;

    /// Delete this interpolator
    ~LinearInterpolator () override = default;

    /// Evaluate the data at a global position
    DataType evaluate (const Domain& x) const override
    {
        const auto pos = this->sub_offset(x);
        if (not this->inside_extensions(pos)) {
            DUNE_THROW(Exception, "Querying interpolator data outside its "
                                  "extensions!");
        }

        // compute indices and position vectors in mesh units
        const auto pos_unit = get_pos_unit(pos);
        const auto [idx_lower, idx_upper] = get_knot_indices(pos_unit);
        const auto pos_unit_diff = get_pos_unit_diff(pos_unit, idx_lower);
        const auto& data = this->_data;


        DataType c_00, c_01, c_10, c_11;
        DF y_d, z_d;

        // actual computation
        if constexpr (dim == 2) {
            c_00 = data[to_index(idx_lower)];
            c_01 = data[to_index({idx_lower[0], idx_upper[1]})];
            c_10 = data[to_index({idx_upper[0], idx_lower[1]})];
            c_11 = data[to_index(idx_upper)];

            // use y, z here for common computation after if-clause
            y_d = pos_unit_diff[0]; // actually x
            z_d = pos_unit_diff[1]; // actually y
        }
        else {
            DataType c_000, c_001, c_010, c_100, c_011, c_110, c_101, c_111;
            c_000 = data[to_index(idx_lower)];
            c_100 = data[to_index({idx_upper[0], idx_lower[1], idx_lower[2]})];
            c_010 = data[to_index({idx_lower[0], idx_upper[1], idx_lower[2]})];
            c_001 = data[to_index({idx_lower[0], idx_lower[1], idx_upper[2]})];
            c_011 = data[to_index({idx_lower[0], idx_upper[1], idx_upper[2]})];
            c_110 = data[to_index({idx_upper[0], idx_upper[1], idx_lower[2]})];
            c_101 = data[to_index({idx_upper[0], idx_lower[1], idx_upper[2]})];
            c_111 = data[to_index(idx_upper)];

            DF x_d = pos_unit_diff[0];
            y_d = pos_unit_diff[1];
            z_d = pos_unit_diff[2];

            c_00 = c_000 * (1 - x_d) + c_100 * x_d;
            c_01 = c_001 * (1 - x_d) + c_101 * x_d;
            c_10 = c_010 * (1 - x_d) + c_110 * x_d;
            c_11 = c_011 * (1 - x_d) + c_111 * x_d;
        }

        const DataType c_0 = c_00 * (1 - y_d) + c_10 * y_d;
        const DataType c_1 = c_01 * (1 - y_d) + c_11 * y_d;

        const DataType c = c_0 * (1 - z_d) + c_1 * z_d;
        return c;
    }
};

/// Factory for creating interpolators
/**
 *  \see Interpolator for the base class of created objects
 *  \ingroup Interpolators
 *  \author Lukas Riedel
 *  \date 2018
 */
template<typename Traits>
struct InterpolatorFactory
{

using RF = typename Traits::RF;
using Domain = typename Traits::Domain;

/// Create the interpolator.
/** Use perfect forwarding for the data.
 *  \param type The type of interpolator to use
 *  \param data The data array for the interpolator
 *  \param shape The shape of the original dataset
 *  \param extensions The physical extensions to map the data to
 *  \param offset The offset of the field with respect to the origin
 *  \param log The logger for the created object, defaults to
 *      Dune::Dorie::log_base
 *  \return Shared pointer to the interpolator
 */
template<typename Data, typename Shape, typename SpaceVector>
static auto create (
    const std::string& type,
    Data&& data,
    Shape&& shape,
    SpaceVector&& extensions,
    SpaceVector&& offset,
    const std::shared_ptr<spdlog::logger> log=get_logger(log_base)
)
    -> std::shared_ptr<Interpolator<
        typename std::decay_t<Data>::value_type,
        Traits::dim>>
{
    log->debug("Creating interpolator of type: {}", type);
    using data_t = typename std::decay_t<Data>::value_type;
    using NNInterp = NearestNeighborInterpolator<data_t, Traits::dim>;
    using LinearInterp = LinearInterpolator<data_t, Traits::dim>;

    if (type == NNInterp::type) {
        return std::make_shared<NNInterp>(
            std::forward<Data>(data),
            std::forward<Shape>(shape),
            std::forward<SpaceVector>(extensions),
            std::forward<SpaceVector>(offset)
        );
    }
    else if (type == LinearInterp::type) {
        return std::make_shared<LinearInterp>(
            std::forward<Data>(data),
            std::forward<Shape>(shape),
            std::forward<SpaceVector>(extensions),
            std::forward<SpaceVector>(offset)
        );
    }
    else {
        log->error("Unknown interpolator type: {}", type);
        DUNE_THROW(Dune::NotImplemented, "Unknown interpolator type");
    }
}

/// Create the interpolator.
/** Use perfect forwarding for the data. Retrieve extensions and offset from
 *  grid view.
 *  \param type The type of interpolator to use
 *  \param data The data array for the interpolator
 *  \param shape The shape of the original dataset
 *  \param grid_view The grid view to use
 *  \param log The logger for the created object, defaults to
 *      Dune::Dorie::log_base
 *  \return Shared pointer to the interpolator
 */
template<typename Data, typename Shape, typename GridView>
static auto create (
    const std::string& type,
    Data&& data,
    Shape&& shape,
    const GridView& grid_view,
    const std::shared_ptr<spdlog::logger> log=get_logger(log_base)
)
    -> std::shared_ptr<Interpolator<
        typename std::decay_t<Data>::value_type,
        Traits::dim>>
{
    /// retrieve extensions and offset from grid extensions
    const auto level_gv = grid_view.grid().levelGridView(0);
    const auto [offset, upper_right] = get_grid_extensions(level_gv);
    const auto extensions = upper_right - offset;

    /// call the actual creator
    return create(type,
                  std::forward<Data>(data),
                  std::forward<Shape>(shape),
                  extensions,
                  offset,
                  log);
}

/// Create an interpolator from a ini file config
/** \param config Dune config file tree
 *  \param grid_view The grid view to use for determining extensions
 *  \param log The logger for the created object
 */
template<typename GridView>
static auto create (
    const Dune::ParameterTree& config,
    const GridView& grid_view,
    const std::shared_ptr<spdlog::logger> log=get_logger(log_base)
)
    -> std::shared_ptr<Interpolator<RF, Traits::dim>>
{
    const auto filename = config.get<std::string>("file");
    const auto dataset = config.get<std::string>("dataset");
    const auto [data, shape] = read_h5_dataset(filename, dataset, log);

    const auto interpolation = config.get<std::string>("interpolation");
    return create(interpolation,
                  data,
                  shape,
                  grid_view,
                  log);
}

/// Create an interpolator from a YAML config node
/// Create an interpolator from a ini file config
/** \param cfg YAML config tree node
 *  \param node_name Name of the YAML config node for better error messages
 *  \param grid_view The grid view to use for determining extensions
 *  \param log The logger for the created object
 */
template<typename GridView>
static auto create (
    const YAML::Node& cfg,
    const std::string& node_name,
    const GridView& grid_view,
    const std::shared_ptr<spdlog::logger> log=get_logger(log_base)
)
    -> std::shared_ptr<Interpolator<RF, Traits::dim>>
{
    log->trace("Creating interpolator from YAML cfg node: {}", node_name);
    const auto dim = GridView::Grid::dimension;

    // open the H5 file
    const auto filename = cfg["file"].as<std::string>();
    const auto dataset = cfg["dataset"].as<std::string>();
    const auto [data, shape] = read_h5_dataset(filename, dataset, log);

    // read extensions and offset
    const auto cfg_ext = cfg["extensions"];
    const auto cfg_off = cfg["offset"];
    if (cfg_ext and cfg_off)
    {
        const auto ext = cfg_ext.as<std::vector<RF>>();
        const auto off = cfg_off.as<std::vector<RF>>();
        if (shape.size() != dim) {
            log->error("Expected {}-dimensional dataset. File: {}, "
                       "Dataset: {}, Dimensions: {}",
                       dim, filename, dataset, shape.size());
            DUNE_THROW(Dune::IOError, "Invalid dataset for interpolator");
        }
        if (ext.size() != dim) {
            log->error("Expected {}-dimensional sequence as "
                       "'extensions' at scaling section node: {}",
                       dim, node_name);
            DUNE_THROW(Dune::IOError, "Invalid interpolator input");
        }
        if (off.size() != dim) {
            log->error("Expected {}-dimensional sequence as 'offset' "
                       "at scaling section node: {}",
                       dim, node_name);
            DUNE_THROW(Dune::IOError, "Invalid interpolator input");
        }

        // copy into correct data structure
        Domain extensions;
        std::copy(begin(ext), end(ext), extensions.begin());
        Domain offset;
        std::copy(begin(off), end(off), offset.begin());

        // call other factory function
        return create(
            cfg["interpolation"].template as<std::string>(),
            data,
            shape,
            extensions,
            offset,
            log
        );
    }
    // extensions or offset not given
    else
    {
        // call other factory function
        return create(
            cfg["interpolation"].template as<std::string>(),
            data,
            shape,
            grid_view,
            log
        );
    }
}

private:

    /// Read a dataset from an H5 file
    /** \param file_path Path to the H5 file
     *  \param dataset_name Name of the dataset
     *  \return Pair: 1D data, dataset shape.
     */
    static std::pair<std::vector<RF>, std::vector<size_t>> read_h5_dataset (
        const std::string& file_path,
        const std::string& dataset_name,
        const std::shared_ptr<spdlog::logger> log
    )
    {
        // open file
        H5File h5file(file_path, log);

        // read the dataset
        std::vector<RF> data;
        std::vector<size_t> shape;
        h5file.read_dataset(dataset_name, H5T_NATIVE_DOUBLE, data, shape);
        std::reverse(begin(shape), end(shape));

        return std::make_pair(data, shape);
    }
};

} // namespace Dorie
} // namespace Dune

#endif
