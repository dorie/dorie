#ifndef DUNE_DORIE_COMMON_UTILITY_HH
#define DUNE_DORIE_COMMON_UTILITY_HH

#include <cmath>
#include <string>
#include <algorithm>
#include <cctype>
#include <memory>
#include <exception>
#include <utility>

#include <sys/types.h>
#include <sys/stat.h>
#include <cstdio>
#include <cerrno>

#include <yaml-cpp/yaml.h>

#include <dune/common/exceptions.hh>
#include <dune/common/fvector.hh>

#include <dune/geometry/type.hh>
#include <dune/grid/common/gridview.hh>

namespace Dune {
namespace Dorie {

/// Return the estimation of entries per matrix row for the spatial GridOperator
/** This supposedly decreases matrix assembly time.
 *  The values specify the *blocks* per row. DG assembles one block for the
 *  actual element and one for each of its neighbors.
 *  \param dim Spatial dimension
 *  \param geo Geometry type of grid entities
 *  \return Estimated number of blocks per matrix row
 */
template<typename R = std::size_t>
constexpr R estimate_mbe_entries (const int dim,
                                  const Dune::GeometryType::BasicType geo)
{
	if (geo==Dune::GeometryType::BasicType::cube){
		return 2*dim + 1;
	}
	else if (geo==Dune::GeometryType::BasicType::simplex){
		return dim + 2;
	}

	throw std::runtime_error("Cannot provide matrix backend entry estimate "
							 "for given geometry type!");
}

/// Retrieve the grid extensions as a position vector
/// Retrieve a pair of vectors spanning the grid domain
/** Iterate over all vertices of the coarsest level grid view, determine the
 *  locally largest and smallest extensions per dimension, and compute the
 *  respective maxima and minima across all processes.
 *
 *  \param grid_view Local view of the grid
 *  \return Pair of vectors (lower left corver, upper right corner)
 *      which span a rectangle that covers the entire simulation domain
 *      (and possibly more if the grid is irregularly shaped)
 *  \ingroup Grid
 */
template<typename GridView>
auto get_grid_extensions (const GridView& grid_view)
    -> std::pair<
        Dune::FieldVector<typename GridView::Grid::ctype,
                          GridView::dimension>,
        Dune::FieldVector<typename GridView::Grid::ctype,
                          GridView::dimension>>
{
    using Domain = Dune::FieldVector<typename GridView::Grid::ctype,
                                     GridView::dimension>;

    // initialize return vectors
    Domain lower_left, upper_right;
    std::fill(lower_left.begin(), lower_left.end(), 0.0);
    std::fill(upper_right.begin(), upper_right.end(), 0.0);

    // define transformation lambdas
    auto min = [](const auto a, const auto b){ return std::min(a, b); };
    auto max = [](const auto a, const auto b){ return std::max(a, b); };

    // iterate over all grid vertices
    for (auto&& vertex : vertices(grid_view)) {
        const auto pos = vertex.geometry().center();
        std::transform(pos.begin(), pos.end(), lower_left.begin(),
                       lower_left.begin(),
                       min);
        std::transform(pos.begin(), pos.end(), upper_right.begin(),
                       upper_right.begin(),
                       max);
    }

    // compute extrema on all processes
    auto& comm = grid_view.comm();
    const auto size = lower_left.size();
    comm.min(&lower_left[0], size);
    comm.max(&upper_right[0], size);

    // return the pair
    return std::make_pair(lower_left, upper_right);
}

/// Retrieve a pair of vectors spanning the grid domain
/** Wrapper operating on a pointer to the grid instead of a grid view.
 *
 *  \param grid Shared pointer to the grid
 *  \return Pair of vectors (lower left corver, upper right corner)
 *      which span a rectangle that covers the entire simulation domain
 *      (and possibly more if the grid is irregularly shaped)
 *  \ingroup Grid
 */
template<typename Grid>
auto get_grid_extensions (const std::shared_ptr<Grid> grid)
    -> std::pair<
        Dune::FieldVector<typename Grid::ctype, Grid::dimension>,
        Dune::FieldVector<typename Grid::ctype, Grid::dimension>>
{
    auto grid_view = grid->levelGridView(0);
    return get_grid_extensions(grid_view);
}

/// Turn a string into lowercase characters
/** \param in Input string
 *  \return Lowercase output string
 */
inline std::string to_lower (const std::string& in)
{
    std::string out = in;
    std::transform(in.begin(), in.end(), out.begin(),
        [](unsigned char c){ return std::tolower(c); }
    );
    return out;
}

/// Check if a string read as value from a file is none
/** Check if the input string is empty or its lowercase version is equal to
 *  `none`.
 *  \param in Input string to check
 *  \return True if the string is `none` or empty
 */
inline bool is_none (const std::string& in)
{
    if (in.empty() or to_lower(in) == "none")
        return true;

    return false;
}

/// Recursively create directories to match the given path
/** This function follows the implementation suggestion in
 *  http://pubs.opengroup.org/onlinepubs/009695399/functions/mkdir.html.
 *
 *  \param path String representation of the path to be created.
 *  \param exist_ok Set to `true` to make this function ignore errors related
 *                  to the fact that the target directory already exists.
 *  \throw std::runtime_error If directories cannot be created.
 */
inline void create_directories (const std::string& path,
                                const bool exist_ok=true)
{
    int status = mkdir(path.c_str(),
                       S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
    if (status != 0) {
        // existing directory may be accepted
        if (exist_ok and errno == EEXIST) {
            return;
        }
        throw std::runtime_error("Error creating directory '"
                                 + path + "': " + std::strerror(errno));
    }
}

/*-------------------------------------------------------------------------*//**
 * @brief      Transform a Yaml node with sequences to map
 * @details    This function check all the nodes within 'in' and transform every
 *             sequence into a map. In particular it transform vectors and
 *             tensors into single key-value map pair by adding a suffix
 *             indicating the component.
 *
 * @param[in]  in    Map Yaml node.
 *
 * @return     Map Yaml node with only key-double pairs.
 */
inline YAML::Node yaml_sequence_to_map(const YAML::Node& in)
{
  YAML::Node out;
  for (auto&& node : in)
  {
    const auto key = node.first.as<std::string>();
    const YAML::Node& value = node.second;

    if (value.IsScalar()) {
      out[key] = value;
    } else if (value.IsSequence()) {
      if (value.size() == 1) { // scalar
        out[key] = value[0].as<double>();
      } else if (value.size() == 2) { // assume vector in 2D
        out[key+"_x"] = value[0].as<double>();
        out[key+"_y"] = value[1].as<double>();
      } else if (value.size() == 3) { // assume vector in 3D
        out[key+"_x"] = value[0].as<double>();
        out[key+"_y"] = value[1].as<double>();
        out[key+"_z"] = value[2].as<double>();
      } else if (value.size() == 4) { // assume tensor in 2D
        out[key+"_xx"] = value[0].as<double>();
        out[key+"_xy"] = value[1].as<double>();
        out[key+"_yx"] = value[2].as<double>();
        out[key+"_yy"] = value[3].as<double>();
      } else if (value.size() == 9) { // assume tensor in 3D
        out[key+"_xx"] = value[0].as<double>();
        out[key+"_xy"] = value[1].as<double>();
        out[key+"_xz"] = value[2].as<double>();
        out[key+"_yx"] = value[3].as<double>();
        out[key+"_yy"] = value[4].as<double>();
        out[key+"_yz"] = value[5].as<double>();
        out[key+"_zx"] = value[6].as<double>();
        out[key+"_zy"] = value[7].as<double>();
        out[key+"_zz"] = value[8].as<double>();
      }
    } else {
      DUNE_THROW(Dune::IOError,
        "Yaml sequence '" << key << "' is not convertible to map pair!");
    }
  }
  return out;
}

} // namespace Dorie
} // namespace Dune

#endif // DUNE_DORIE_COMMON_UTILITY_HH
