#ifndef DUNE_DORIE_RAVIART_THOMAS_FEM_HH
#define DUNE_DORIE_RAVIART_THOMAS_FEM_HH

#include <dune/pdelab/finiteelementmap/raviartthomasfem.hh>

#include <dune/dorie/common/finite_element/raviart_thomas_dg_fem.hh>

namespace Dune{
namespace Dorie{

/*-------------------------------------------------------------------------*//**
 * @brief      Raviart thomas finite element map selector. It siwtches between
 *             dune-pdelab implementation and dorie/dune-localfunctions
 *             implementation
 *
 * @author     Santiago Ospina De Los Ríos
 * @date       2018
 *
 * @tparam     GV         Grid view.
 * @tparam     DF         Domain field.
 * @tparam     RF         Range field.
 * @tparam     k          Order.
 * @tparam     gt         Geometry type.
 * @tparam     <unnamed>  Never instantiate this argument! Implementation
 *                        detail: Curiously recurring template pattern is used
 *                        to select the correct map.
 */
template<class GV, class DF, class RF, std::size_t k, 
                GeometryType::BasicType gt, class = void>
struct RaviartThomasLocalFiniteElementMapBaseSelector
{
  using type = Dune::PDELab::RaviartThomasLocalFiniteElementMap<GV,DF,RF,k,gt>;
};

template<class GV, class DF, class RF, std::size_t k>
struct RaviartThomasLocalFiniteElementMapBaseSelector<
                GV,DF,RF,k,GeometryType::simplex,
                std::enable_if_t<(GV::dimension>2)>
              >
{
  // We have to use dg here because the original elements are not oriented!
  using type = RaviartThomasSimplexDGLocalFiniteElementMap<GV,DF,RF,k>;
};

template<class GV, class DF, class RF, std::size_t k>
struct RaviartThomasLocalFiniteElementMapBaseSelector<
                GV,DF,RF,k,GeometryType::simplex,
                std::enable_if_t<(GV::dimension==2 && k>1)>
              >
{
  // We have to use dg here because the original elements are not oriented!
  using type = RaviartThomasSimplexDGLocalFiniteElementMap<GV,DF,RF,k>;
};

/*-------------------------------------------------------------------------*//**
 * @brief      Finite element map for raviart thomas elements
 *             @f$\mathcal{RT}_k^{(dg)}\f$.
 * @details    This class is equivalent to
 *             Dune::PDELab::RaviartThomasLocalFiniteElementMap plus elements
 *             for raviart thomas in dg in simplices (see
 *             Dune::Dorie::RaviartThomasSimplexDGLocalFiniteElementMap). A big
 *             difference with the dune-pdelab map is that these extra elements
 *             are not oriented, which make them to be not conforming in normal
 *             direction. In other words, they cannot be used for conforming 
 *             finite element approximations. Nevertheless they are still useful
 *             for flux reconstruction since we prescribe the continuity by
 *              using the numerical fluxes solved by the dG/FV methods.
 *
 *             Available elements: <table> <tr> <th colspan="2">Order</th>
 *             <th>0</th> <th>1</th> <th>2</th> <th>3</th> </tr> <tr> <td
 *             rowspan="2"><br>2D</td> <td>Simplex</td> <td>✓</td> <td>✓</td>
 *             <td>✓</td> <td>✓ <SUP>&dagger;</SUP></td> </tr> <tr>
 *             <td>Cube</td> <td>✓</td> <td>✓</td> <td>✓</td> <td>✗</td> </tr>
 *             <tr> <td rowspan="2"><br>3D</td> <td>Simplex</td> <td>✓
 *             <SUP>&dagger;</SUP></td> <td>✓ <SUP>&dagger;</SUP></td> <td>✓
 *             <SUP>&dagger;</SUP></td> <td>✓ <SUP>&dagger;</SUP></td> </tr>
 *             <tr> <td>Cube</td> <td>✓</td> <td>✓</td> <td>✗</td> <td>✗</td>
 *             </tr> </table>
 *
 *             &dagger;: Implemented with DG local coefficients (see
 *             Dune::Dorie::RaviartThomasSimplexDGLocalFiniteElement and
 *             Dune::Dorie::DGLocalCoefficients).
 *
 * @warning    This map should not be used to solve finite element problems. For
 *             that purpose use the
 *             Dune::PDELab::RaviartThomasLocalFiniteElementMap class directly.
 *
 * @author     Santiago Ospina De Los Ríos
 * @date       2018
 * @ingroup    FluxReconstruction
 * @ingroup    FiniteElement
 *
 * @tparam     GV    The Gird view.
 * @tparam     DF    The domain field.
 * @tparam     RF    The range field.
 * @tparam     k     The order.
 * @tparam     gt    The geometry type.
 * @tparam     d     The dimension.
 */
template<class GV, class DF, class RF, int k, Dune::GeometryType::BasicType gt>
class RaviartThomasLocalFiniteElementMap
  : public Dune::PDELab::RaviartThomasLocalFiniteElementMap<GV,DF,RF,k,gt>
{
  using Base = Dune::PDELab::RaviartThomasLocalFiniteElementMap<GV,DF,RF,k,gt>;
public:
  RaviartThomasLocalFiniteElementMap(const GV& gv) : Base(gv) {}
};

template<class GV, class DF, class RF, int k>
class RaviartThomasLocalFiniteElementMap<GV,DF,RF,k,Dune::GeometryType::simplex>
  : public Dune::Dorie::RaviartThomasLocalFiniteElementMapBaseSelector<
                  GV,DF,RF,k,Dune::GeometryType::simplex>::type
{
  using Base = typename Dune::Dorie::
                    RaviartThomasLocalFiniteElementMapBaseSelector<
                        GV,DF,RF,k,Dune::GeometryType::simplex
                      >::type;
public:

  /*-----------------------------------------------------------------------*//**
   * @brief      Constructor of the finite element map.
   *
   * @param[in]  gv    The grid view.
   */
  RaviartThomasLocalFiniteElementMap(const GV& gv) : Base(gv) {}
};

} // namespace Dorie
} // namespace Dune

#endif // DUNE_DORIE_RAVIART_THOMAS_FEM_HH
