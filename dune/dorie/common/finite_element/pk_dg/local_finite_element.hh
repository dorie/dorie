#ifndef DUNE_DORIE_PK_DG_LOCAL_FINITE_ELEMENT_HH
#define DUNE_DORIE_PK_DG_LOCAL_FINITE_ELEMENT_HH

#include <dune/localfunctions/utility/localfiniteelement.hh>
#include <dune/localfunctions/lagrange/pk.hh>

#include <dune/dorie/common/finite_element/dg/local_coefficients.hh>

namespace Dune{
namespace Dorie{

/*-------------------------------------------------------------------------*//**
 * @brief      Discontinuous Galerkin for
 *             @f$\mathcal{P}_k^{dg}\f$ simplices
 * @details    This class is equivalent to Dune::PkLocalFiniteElement but with
 *             local keys corresponding to discontinuous galerkin methods, that
 *             is, all shape functions are associated to entities of codimension
 *             0.
 * @author     Santiago Ospina De Los Ríos
 * @date       2018
 * @ingroup    FiniteElement
 * @see        Dune::Dorie::DGLocalCoefficients
 *
 * @tparam     DF    Domain field.
 * @tparam     RF    Range field.
 * @tparam     dim   World dimension.
 * @tparam     k     Order of polynomials.
 */
template<class DF, class RF, int dim, int k>
class PkDGLocalFiniteElement
  : public Dune::PkLocalFiniteElement<DF,RF,dim,k>
{
  using LocalCoefficients = Dune::Dorie::DGLocalCoefficients;
  using Base = Dune::PkLocalFiniteElement<DF,RF,dim,k>;
public:

  using Traits = Dune::LocalFiniteElementTraits< 
      typename Base::Traits::LocalBasisType,
      LocalCoefficients,
      typename Base::Traits::LocalInterpolationType>;

  static constexpr unsigned int N = Traits::LocalBasisType::N;
  
  /*-----------------------------------------------------------------------*//**
   * @brief      Constructor of the class.
   */
  PkDGLocalFiniteElement() 
    : local_coefficients(N)
  {}

  /*-----------------------------------------------------------------------*//**
   * @brief      The local basis for this finite element.
   *
   * @return     The local basis.
   */
  const typename Traits::LocalBasisType& localBasis () const
  {
    return Base::localBasis();
  }

  /*-----------------------------------------------------------------------*//**
   * @brief      The local coefficients for this finite element.
   * @see        Dune::Dorie::DGLocalCoefficients
   *
   * @return     The local coefficients.
   */
  const typename Traits::LocalCoefficientsType& localCoefficients () const
  {
    return local_coefficients;
  }

  /*-----------------------------------------------------------------------*//**
   * @brief      The local interpolation for this finite element.
   *
   * @return     The local interpolation.
   */
  const typename Traits::LocalInterpolationType& localInterpolation () const
  {
    return Base::localInterpolation();
  }

private:
  LocalCoefficients local_coefficients;
};


} // namespace Dorie
} // namespace Dune

#endif // DUNE_DORIE_PK_DG_LOCAL_FINITE_ELEMENT_HH
