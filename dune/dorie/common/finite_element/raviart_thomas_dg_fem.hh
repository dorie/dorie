#ifndef DUNE_DORIE_RAVIART_THOMAS_DG_FEM_HH
#define DUNE_DORIE_RAVIART_THOMAS_DG_FEM_HH

#include <dune/pdelab/finiteelementmap/finiteelementmap.hh>

#include <dune/dorie/common/finite_element/raviart_thomas_dg/local_finite_element.hh>

namespace Dune{
namespace Dorie{

/**
 * @brief      Finite element map for discontinuous galerkin raviart thomas
 *             elements
 *             @f$\mathcal{RT}_k^{dg}\f$ in simplicies.
 * @details    This class implements a dune-pdelab finite element map  
 *             Dune::Dorie::RaviartThomasSimplexDGLocalFiniteElement
 *
 *             Available elements: 
 *             <table> <tr> <th colspan="2">Order</th> <th>0</th> <th>1</th>
 *             <th>2</th> <th>3</th> </tr> <tr> <td>2D</td> <td>Simplex</td>
 *             <td>✓</td> <td>✓</td> <td>✓</td> <td>✓</td> </tr> <tr>
 *             <td>3D</td> <td>Simplex</td> <td>✓</td> <td>✓</td> <td>✓</td>
 *             <td>✓</td> </tr> </table>
 *             
 * @author     Santiago Ospina De Los Ríos
 * @date       2018
 * @ingroup    FluxReconstruction
 * @ingroup    FiniteElement
 * 
 * @tparam     GV    Grid view type.
 * @tparam     DF    Domain field type.
 * @tparam     RF    Range field type.
 * @tparam     k     Order.
 */
template<class GV, class DF, class RF, std::size_t k>
class RaviartThomasSimplexDGLocalFiniteElementMap
  : public Dune::PDELab::SimpleLocalFiniteElementMap<
      Dune::Dorie::RaviartThomasSimplexDGLocalFiniteElement<GV::dimension,DF,RF>,
      GV::dimension>
{
  static constexpr unsigned int dim = GV::dimension;
  static constexpr unsigned int size_face = Dune::PB::PkSize<k,dim-1>::value;
  static constexpr unsigned int faces = dim+1;
  static constexpr unsigned int size_skeleton = faces*size_face;
  static constexpr unsigned int size_volume = (dim == 2) 
                                                ? dim*k*(k+1)/2       // for 2D
                                                : dim*k*(k+1)*(k+2)/6;// for 3D
  static constexpr unsigned int size_rt = (dim == 2)
                                                ? (k+1)*(k+3)         // for 2D
                                                : (k+1)*(k+2)*(k+4)/2;// for 3D

  // Just in case
  static_assert((size_skeleton + size_volume) == size_rt, 
    "(size_skeleton + size_volume) != size_rt");

  static_assert(dim == 2 || dim == 3, 
    "Raviart Thomas for simplices are only implemented for dimension 2 and 3!");

  using FE = Dune::Dorie::
             RaviartThomasSimplexDGLocalFiniteElement<GV::dimension,DF,RF>;
  using Base = Dune::PDELab::SimpleLocalFiniteElementMap<FE,GV::dimension>;
public:
  RaviartThomasSimplexDGLocalFiniteElementMap(const GV& gv) 
    : Base(FE(Dune::GeometryTypes::simplex(GV::dimension),k))
  {}

  /*-----------------------------------------------------------------------*//**
   * @brief      A FiniteElementMap is fixedSize iif the size of the local
   *             functions space for each GeometryType is fixed.
   *
   * @return     A fixed size bool.
   */
  static constexpr bool fixedSize()
  {
    return true;
  }

  /*-----------------------------------------------------------------------*//**
   * @brief      Return if FiniteElementMap has degrees of freedom for given
   *             codimension.
   *
   * @param[in]  codim  The codimension.
   *
   * @return     True if has degreed of freedom for codimension codim, False
   *             otherwise.
   */
  static constexpr bool hasDOFs(int codim)
  {
    return (codim == 0);
  }

  /*-----------------------------------------------------------------------*//**
   * @brief      If the FiniteElementMap is fixedSize, the size methods computes
   *             the number of DOFs for given GeometryType.
   *
   * @param[in]  geometry_type  The geometry type.
   *
   * @return     The size of the fem for fixed size map.
   */
  std::size_t size(GeometryType geometry_type) const
  {
    return (geometry_type.dim() == dim) ? size_rt: 0;
  }

  /*-----------------------------------------------------------------------*//**
   * @brief      Computes an upper bound for the local number of DOFs.
   *
   * @details    This upper bound is used to avoid reallocations in std::vectors
   *             used during the assembly.
   *
   * @return     The maximum local size of all finite elements used by the map.
   */
  std::size_t maxLocalSize() const
  {
    return size_rt;
  }
};

} // namespace Dorie
} // namespace Dune

#endif // DUNE_DORIE_RAVIART_THOMAS_DG_FEM_HH
