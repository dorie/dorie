#ifndef DUNE_DORIE_RAVIART_THOMAS_VOLUME_BASIS_HH
#define DUNE_DORIE_RAVIART_THOMAS_VOLUME_BASIS_HH

#include <dune/localfunctions/common/localbasis.hh>
#include <dune/localfunctions/lagrange/pk.hh>
#include <dune/localfunctions/lagrange/qk.hh>

#include <dune/geometry/type.hh>

#include <dune/common/exceptions.hh>
#include <dune/common/power.hh>
#include <dune/common/typetraits.hh>

#include <dune/pdelab/finiteelementmap/qkdg.hh>

#include <vector>

namespace Dune{
namespace Dorie{

/*-------------------------------------------------------------------------*//**
 * @brief      Class for local basis of the volume raviart thomas finite element
 *             @f$\mathcal{RT}_k^{vol}\f$.
 * @details    This class is used to reconstruct fluxes from local operators
 *             that solve fluxes in the residual vector. In particular, this
 *             volume raviart thomas \f$\mathcal{RT}_k^{vol}\f$ is equivalent to 
 *             the usual raviart thomas element but excluding the degrees of
 *             freedom located on the faces (codim = 1).
 *
 * @author     Santiago Ospina De Los Ríos implemented it in DORiE out of source
 *             files sent by Marian Piatkowski.
 * @date       2018
 * @see        Dune::Dorie::RaviartThomasVolumeLocalFiniteElement.
 *
 * @tparam     DF    The domain field.
 * @tparam     RF    The range field.
 * @tparam     k     The order.
 * @tparam     d     The dimension.
 * @tparam     gt    The geometry type.
 */
template<class DF, class RF, int k, int d, Dune::GeometryType::BasicType gt>
class RaviartThomasVolumeLocalBasis
{
  //! Identify when there is no implementation of this class.
  static_assert(Dune::AlwaysFalse<DF>::value, 
    "Volume Raviart Thomas basis function is not implemented!");

#ifdef DOXYGEN /* Documentation of the code */

  //! Order of the polynomials.
  static constexpr int k;
  //! World dimension.
  static constexpr int dim;
  //! Number of shape functions
  static constexpr int volsize;

  /*-----------------------------------------------------------------------*//**
   * @brief      Traits of local basis. 
   * @details    It follows the traits structure for local basis defined by
   *             dune-localfunctions (Dune::LocalBasisTraits).
   */
  struct Traits {};

  /*-----------------------------------------------------------------------*//**
   * @brief      Number of shape functions (i.e. degrees of freedom).
   *
   * @return     The number of shape functions.
   */
  constexpr unsigned int size() const;

  /*-----------------------------------------------------------------------*//**
   * @brief      Evaluates the each local base function (in the vector out) in
   *             the position in.
   *
   * @param[in]  in    Position to evaluate the base function in local
   *                   coordinates.
   * @param      out   Vector of values. Each value represents one local base
   *                   function evaluated in the position in.
   */
  inline void evaluateFunction(const typename Traits::DomainType& in,
                               std::vector<typename Traits::RangeType>& out) const;

  /*-----------------------------------------------------------------------*//**
   * @brief      Evaluates the jacobian of each local base function (in the
   *             vector out) in the position in.
   *
   * @param[in]  in    Position to evaluate the base function in local
   *                   coordinates.
   * @param      out   Vector of jacobians. Each entry i in the vector
   *                   represents the jacobian of the local base function i
   *                   evaluated in the position in.
   */
  inline void
  evaluateJacobian(const typename Traits::DomainType& in,
                   std::vector<typename Traits::JacobianType>& out) const;

  /*-----------------------------------------------------------------------*//**
   * @brief      Evaluate partial derivatives of any order of all shape
   *             functions.
   *
   * @param[in]  order  Order of the partial derivatives, in the classic
   *                    multi-index notation.
   * @param[in]  in     Position to evaluate the base function in local
   *                    coordinates.
   * @param      out    The desired partial derivatives.
   */
  void partial(const std::array<unsigned int,dim>& order,
               const typename Traits::DomainType& in,
               std::vector<typename Traits::RangeType>& out) const;

/*-------------------------------------------------------------------------*//**
 * @brief Polynomial order of the shape functions for quadrature
 *
 * @return     The order.
 */
  constexpr unsigned int order() const;
#endif /* Documentation of the code */
};

#ifndef DOXYGEN /* Actual code */

// This classes only implement the parts needed for the flux reconstruction.
// Never use them to generate a grid function space in dune-pdelab!

template<class DF, class RF>
class RaviartThomasVolumeLocalBasis<DF,RF,0,2,Dune::GeometryType::simplex>
{
  static constexpr int k = 0;
  static constexpr int dim = 2;
  static constexpr int volsize = (dim == 2) ? dim*k*(k+1)/2 
                                            : dim*k*(k+1)*(k+2)/6;

public:

  using Traits = LocalBasisTraits<DF,dim,Dune::FieldVector<DF,dim>,
                                  RF,dim,Dune::FieldVector<RF,dim>,
                                  Dune::FieldMatrix<RF,dim,dim> >;

  constexpr unsigned int size() const
  {
    return volsize;
  }

  inline void evaluateFunction(const typename Traits::DomainType& in,
                               std::vector<typename Traits::RangeType>& out) const
  {}

  inline void
  evaluateJacobian(const typename Traits::DomainType& in,
                   std::vector<typename Traits::JacobianType>& out) const
  {}

  void partial(const std::array<unsigned int,dim>& order,
               const typename Traits::DomainType& in,
               std::vector<typename Traits::RangeType>& out) const
  {}

  constexpr unsigned int order() const
  {
    return k;
  }
};

template<class DF, class RF>
class RaviartThomasVolumeLocalBasis<DF,RF,0,3,Dune::GeometryType::simplex>
{
  static constexpr int k = 0;
  static constexpr int dim = 3;
  static constexpr int volsize = (dim == 2) ? dim*k*(k+1)/2 
                                            : dim*k*(k+1)*(k+2)/6;

public:

  using Traits = LocalBasisTraits<DF,dim,Dune::FieldVector<DF,dim>,
                                  RF,dim,Dune::FieldVector<RF,dim>,
                                  Dune::FieldMatrix<RF,dim,dim> >;


  constexpr unsigned int size() const
  {
    return volsize;
  }

  inline void evaluateFunction(const typename Traits::DomainType& in,
                               std::vector<typename Traits::RangeType>& out) const
  {}

  inline void
  evaluateJacobian(const typename Traits::DomainType& in,
                   std::vector<typename Traits::JacobianType>& out) const
  {}

  void partial(const std::array<unsigned int,dim>& order,
               const typename Traits::DomainType& in,
               std::vector<typename Traits::RangeType>& out) const
  {}

  constexpr unsigned int order() const
  {
    return k;
  }
};

template<class DF, class RF>
class RaviartThomasVolumeLocalBasis<DF,RF,1,2,Dune::GeometryType::simplex>
{
  static constexpr int k = 1;
  static constexpr int dim = 2;
  static constexpr int volsize = (dim == 2) ? dim*k*(k+1)/2 
                                            : dim*k*(k+1)*(k+2)/6;

public:

  using Traits = LocalBasisTraits<DF,dim,Dune::FieldVector<DF,dim>,
                                  RF,dim,Dune::FieldVector<RF,dim>,
                                  Dune::FieldMatrix<RF,dim,dim> >;

  constexpr unsigned int size() const
  {
    return volsize;
  }

  inline void evaluateFunction(const typename Traits::DomainType& in,
                               std::vector<typename Traits::RangeType>& out) const
  {
    out.resize(volsize);

    out[0][0] = 1.0; out[1][0] = 0.0;
    out[0][1] = 0.0; out[1][1] = 1.0;
  }

  inline void
  evaluateJacobian(const typename Traits::DomainType& in,
                   std::vector<typename Traits::JacobianType>& out) const
  {
    DUNE_THROW(Dune::NotImplemented,
      "evaluateJacobian for RaviartThomasVolumeLocalBasis not implemented!");
  }

  void partial(const std::array<unsigned int,dim>& order,
               const typename Traits::DomainType& in,
               std::vector<typename Traits::RangeType>& out) const
  {
    DUNE_THROW(Dune::NotImplemented,
      "partial for RaviartThomasVolumeLocalBasis not implemented!");
  }

  constexpr unsigned int order() const
  {
    return k;
  }
};

template<class DF, class RF>
class RaviartThomasVolumeLocalBasis<DF,RF,1,3,Dune::GeometryType::simplex>
{
  static constexpr int k = 1;
  static constexpr int dim = 3;
  static constexpr int volsize = (dim == 2) ? dim*k*(k+1)/2 
                                            : dim*k*(k+1)*(k+2)/6;

public:

  using Traits = LocalBasisTraits<DF,dim,Dune::FieldVector<DF,dim>,
                                  RF,dim,Dune::FieldVector<RF,dim>,
                                  Dune::FieldMatrix<RF,dim,dim> >;

  constexpr unsigned int size() const
  {
    return volsize;
  }

  inline void evaluateFunction(const typename Traits::DomainType& in,
                               std::vector<typename Traits::RangeType>& out) const
  {
    out.resize(volsize);

    out[0][0] = 1.0; out[1][0] = 0.0; out[2][0] = 0.0;
    out[0][1] = 0.0; out[1][1] = 1.0; out[2][1] = 0.0;
    out[0][2] = 0.0; out[1][2] = 0.0; out[2][2] = 1.0;
  }

  inline void
  evaluateJacobian(const typename Traits::DomainType& in,
                   std::vector<typename Traits::JacobianType>& out) const
  {
    DUNE_THROW(Dune::NotImplemented,
      "evaluateJacobian for RaviartThomasVolumeLocalBasis not implemented!");
  }

  void partial(const std::array<unsigned int,dim>& order,
               const typename Traits::DomainType& in,
               std::vector<typename Traits::RangeType>& out) const
  {
    DUNE_THROW(Dune::NotImplemented,
      "partial for RaviartThomasVolumeLocalBasis not implemented!");
  }

  constexpr unsigned int order() const
  {
    return k;
  }
};

template<class DF, class RF>
class RaviartThomasVolumeLocalBasis<DF,RF,2,2,Dune::GeometryType::simplex>
{
  static constexpr int k = 2;
  static constexpr int dim = 2;
  static constexpr int volsize = (dim == 2) ? dim*k*(k+1)/2 
                                            : dim*k*(k+1)*(k+2)/6;

public:

  using Traits = LocalBasisTraits<DF,dim,Dune::FieldVector<DF,dim>,
                                  RF,dim,Dune::FieldVector<RF,dim>,
                                  Dune::FieldMatrix<RF,dim,dim> >;

  constexpr unsigned int size() const
  {
    return volsize;
  }

  inline void evaluateFunction(const typename Traits::DomainType& in,
                               std::vector<typename Traits::RangeType>& out) const
  {
    out.resize(volsize);

    out[0][0] = 1.0; out[1][0] = in[0]; out[2][0] = in[1];
    out[0][1] = 0.0; out[1][1] = 0.0;   out[2][1] = 0.0;

    out[3][0] = 0.0; out[4][0] = 0.0;   out[5][0] = 0.0;
    out[3][1] = 1.0; out[4][1] = in[0]; out[5][1] = in[1];
  }

  inline void
  evaluateJacobian(const typename Traits::DomainType& in,
                   std::vector<typename Traits::JacobianType>& out) const
  {
    DUNE_THROW(Dune::NotImplemented,
      "evaluateJacobian for RaviartThomasVolumeLocalBasis not implemented!");
  }

  void partial(const std::array<unsigned int,dim>& order,
               const typename Traits::DomainType& in,
               std::vector<typename Traits::RangeType>& out) const
  {
    DUNE_THROW(Dune::NotImplemented,
      "partial for RaviartThomasVolumeLocalBasis not implemented!");
  }

  constexpr unsigned int order() const
  {
    return k;
  }
};

template<class DF, class RF>
class RaviartThomasVolumeLocalBasis<DF,RF,2,3,Dune::GeometryType::simplex>
{
  static constexpr int k = 2;
  static constexpr int dim = 3;
  static constexpr int volsize = (dim == 2) ? dim*k*(k+1)/2 
                                            : dim*k*(k+1)*(k+2)/6;

public:

  using Traits = LocalBasisTraits<DF,dim,Dune::FieldVector<DF,dim>,
                                  RF,dim,Dune::FieldVector<RF,dim>,
                                  Dune::FieldMatrix<RF,dim,dim> >;

  constexpr unsigned int size() const
  {
    return volsize;
  }

  inline void evaluateFunction(const typename Traits::DomainType& in,
                               std::vector<typename Traits::RangeType>& out) const
  {
    out.resize(volsize);

    out[0][0] = 1.0; out[1][0] = in[0]; out[2][0] = in[1]; out[3][0] = in[2];
    out[0][1] = 0.0; out[1][1] = 0.0;   out[2][1] = 0.0;   out[3][1] = 0.0;
    out[0][2] = 0.0; out[1][2] = 0.0;   out[2][2] = 0.0;   out[3][2] = 0.0;

    out[4][0] = 0.0; out[5][0] = 0.0;   out[6][0] = 0.0;   out[7][0] = 0.0;
    out[4][1] = 1.0; out[5][1] = in[0]; out[6][1] = in[1]; out[7][1] = in[2];
    out[4][2] = 0.0; out[5][2] = 0.0;   out[6][2] = 0.0;   out[7][2] = 0.0;

    out[8][0] = 0.0; out[9][0] = 0.0;   out[10][0] = 0.0;   out[11][0] = 0.0;
    out[8][1] = 0.0; out[9][1] = 0.0;   out[10][1] = 0.0;   out[11][1] = 0.0;
    out[8][2] = 1.0; out[9][2] = in[0]; out[10][2] = in[1]; out[11][2] = in[2];
  }

  inline void
  evaluateJacobian(const typename Traits::DomainType& in,
                   std::vector<typename Traits::JacobianType>& out) const
  {
    DUNE_THROW(Dune::NotImplemented,
      "evaluateJacobian for RaviartThomasVolumeLocalBasis not implemented!");
  }

  void partial(const std::array<unsigned int,dim>& order,
               const typename Traits::DomainType& in,
               std::vector<typename Traits::RangeType>& out) const
  {
    DUNE_THROW(Dune::NotImplemented,
      "partial for RaviartThomasVolumeLocalBasis not implemented!");
  }

  constexpr unsigned int order() const
  {
    return k;
  }
};

//********************** now cubes **********************//

template<class DF, class RF>
class RaviartThomasVolumeLocalBasis<DF,RF,0,2,Dune::GeometryType::cube>
{
  static constexpr int k = 0;
  static constexpr int dim = 2;
  static constexpr int volsize = dim*Dune::StaticPower<k+1,dim-1>::power*k;

public:

  using Traits = LocalBasisTraits<DF,dim,Dune::FieldVector<DF,dim>,
                                  RF,dim,Dune::FieldVector<RF,dim>,
                                  Dune::FieldMatrix<RF,dim,dim> >;

  constexpr unsigned int size() const
  {
    return volsize;
  }

  inline void evaluateFunction(const typename Traits::DomainType& in,
                               std::vector<typename Traits::RangeType>& out) const
  {}

  inline void
  evaluateJacobian(const typename Traits::DomainType& in,
                   std::vector<typename Traits::JacobianType>& out) const
  {}

  void partial(const std::array<unsigned int,dim>& order,
               const typename Traits::DomainType& in,
               std::vector<typename Traits::RangeType>& out) const
  {}

  constexpr unsigned int order() const
  {
    return k;
  }
};

template<class DF, class RF>
class RaviartThomasVolumeLocalBasis<DF,RF,0,3,Dune::GeometryType::cube>
{
  static constexpr int k = 0;
  static constexpr int dim = 3;
  static constexpr int volsize = dim*Dune::StaticPower<k+1,dim-1>::power*k;

public:

  using Traits = LocalBasisTraits<DF,dim,Dune::FieldVector<DF,dim>,
                                  RF,dim,Dune::FieldVector<RF,dim>,
                                  Dune::FieldMatrix<RF,dim,dim> >;

  constexpr unsigned int size() const
  {
    return volsize;
  }

  inline void evaluateFunction(const typename Traits::DomainType& in,
                               std::vector<typename Traits::RangeType>& out) const
  {}

  inline void
  evaluateJacobian(const typename Traits::DomainType& in,
                   std::vector<typename Traits::JacobianType>& out) const
  {}

  void partial(const std::array<unsigned int,dim>& order,
               const typename Traits::DomainType& in,
               std::vector<typename Traits::RangeType>& out) const
  {}

  constexpr unsigned int order() const
  {
    return k;
  }
};

template<class DF, class RF>
class RaviartThomasVolumeLocalBasis<DF,RF,1,2,Dune::GeometryType::cube>
{
  static constexpr int k = 1;
  static constexpr int dim = 2;
  static constexpr int volsize = dim*Dune::StaticPower<k+1,dim-1>::power*k;

public:

  using Traits = LocalBasisTraits<DF,dim,Dune::FieldVector<DF,dim>,
                                  RF,dim,Dune::FieldVector<RF,dim>,
                                  Dune::FieldMatrix<RF,dim,dim> >;

  constexpr unsigned int size() const
  {
    return volsize;
  }

  inline void evaluateFunction(const typename Traits::DomainType& in,
                               std::vector<typename Traits::RangeType>& out) const
  {
    out.resize(volsize);

    out[0][0] = 1.0; out[1][0] = in[1]; out[2][0] = 0.0; out[3][0] = 0.0;
    out[0][1] = 0.0; out[1][1] = 0.0;   out[2][1] = 1.0; out[3][1] = in[0];
  }

  inline void
  evaluateJacobian(const typename Traits::DomainType& in,
                   std::vector<typename Traits::JacobianType>& out) const
  {
    DUNE_THROW(Dune::NotImplemented,
      "evaluateJacobian for RaviartThomasVolumeLocalBasis not implemented!");
  }

  void partial(const std::array<unsigned int,dim>& order,
               const typename Traits::DomainType& in,
               std::vector<typename Traits::RangeType>& out) const
  {
    DUNE_THROW(Dune::NotImplemented,
      "partial for RaviartThomasVolumeLocalBasis not implemented!");
  }

  constexpr unsigned int order() const
  {
    return k;
  }
};

template<class DF, class RF>
class RaviartThomasVolumeLocalBasis<DF,RF,1,3,Dune::GeometryType::cube>
{
  static constexpr int k = 1;
  static constexpr int dim = 3;
  static constexpr int volsize = dim*Dune::StaticPower<k+1,dim-1>::power*k;

public:

  using Traits = LocalBasisTraits<DF,dim,Dune::FieldVector<DF,dim>,
                                  RF,dim,Dune::FieldVector<RF,dim>,
                                  Dune::FieldMatrix<RF,dim,dim> >;

  constexpr unsigned int size() const
  {
    return volsize;
  }

  inline void evaluateFunction(const typename Traits::DomainType& in,
                               std::vector<typename Traits::RangeType>& out) const
  {
    out.resize(volsize);

    out[0] = {1.0,         0.0, 0.0};
    out[1] = {in[1],       0.0, 0.0};
    out[2] = {in[2],       0.0, 0.0};
    out[3] = {in[1]*in[2], 0.0, 0.0};

    out[4] = {0.0, 1.0,         0.0};
    out[5] = {0.0, in[0],       0.0};
    out[6] = {0.0, in[2],       0.0};
    out[7] = {0.0, in[0]*in[2], 0.0};

    out[8] =  {0.0, 0.0, 1.0        };
    out[9] =  {0.0, 0.0, in[0]      };
    out[10] = {0.0, 0.0, in[1]      };
    out[11] = {0.0, 0.0, in[0]*in[1]};
  }

  inline void
  evaluateJacobian(const typename Traits::DomainType& in,
                   std::vector<typename Traits::JacobianType>& out) const
  {
    DUNE_THROW(Dune::NotImplemented,
      "evaluateJacobian for RaviartThomasVolumeLocalBasis not implemented!");
  }

  void partial(const std::array<unsigned int,dim>& order,
               const typename Traits::DomainType& in,
               std::vector<typename Traits::RangeType>& out) const
  {
    DUNE_THROW(Dune::NotImplemented,
      "partial for RaviartThomasVolumeLocalBasis not implemented!");
  }

  constexpr unsigned int order() const
  {
    return k;
  }
};

template<class DF, class RF>
class RaviartThomasVolumeLocalBasis<DF,RF,2,2,Dune::GeometryType::cube>
{
  static constexpr int k = 2;
  static constexpr int dim = 2;
  static constexpr int volsize = dim*Dune::StaticPower<k+1,dim-1>::power*k;

public:

  using Traits = LocalBasisTraits<DF,dim,Dune::FieldVector<DF,dim>,
                                  RF,dim,Dune::FieldVector<RF,dim>,
                                  Dune::FieldMatrix<RF,dim,dim> >;

  constexpr unsigned int size() const
  {
    return volsize;
  }

  inline void evaluateFunction(const typename Traits::DomainType& in,
                               std::vector<typename Traits::RangeType>& out) const
  {
    out.resize(volsize);

    out[0][0] = 1.0; out[1][0] = in[1]; out[2][0] = in[1]*in[1];
    out[0][1] = 0.0; out[1][1] = 0.0;   out[2][1] = 0.0;

    out[3][0] = 0.0; out[4][0] = 0.0;   out[5][0] = 0.0;
    out[3][1] = 1.0; out[4][1] = in[0]; out[5][1] = in[0]*in[0];

    out[6][0] = in[0]; out[7][0] = in[0]*in[1]; out[8][0] = in[0]*in[1]*in[1];
    out[6][1] = 0.0;   out[7][1] = 0.0;         out[8][1] = 0.0;

    out[9][0] = 0.0;   out[10][0] = 0.0;         out[11][0] = 0.0;
    out[9][1] = in[1]; out[10][1] = in[1]*in[0]; out[11][1] = in[1]*in[0]*in[0];
  }

  inline void
  evaluateJacobian(const typename Traits::DomainType& in,
                   std::vector<typename Traits::JacobianType>& out) const
  {
    DUNE_THROW(Dune::NotImplemented,
      "evaluateJacobian for RaviartThomasVolumeLocalBasis not implemented!");
  }

  void partial(const std::array<unsigned int,dim>& order,
               const typename Traits::DomainType& in,
               std::vector<typename Traits::RangeType>& out) const
  {
    DUNE_THROW(Dune::NotImplemented,
      "partial for RaviartThomasVolumeLocalBasis not implemented!");
  }

  constexpr unsigned int order() const
  {
    return k;
  }
};

template<class DF, class RF>
class RaviartThomasVolumeLocalBasis<DF,RF,2,3,Dune::GeometryType::cube>
{
  static constexpr int k = 2;
  static constexpr int dim = 3;
  static constexpr int volsize = dim*Dune::StaticPower<k+1,dim-1>::power*k;

public:

  using Traits = LocalBasisTraits<DF,dim,Dune::FieldVector<DF,dim>,
                                  RF,dim,Dune::FieldVector<RF,dim>,
                                  Dune::FieldMatrix<RF,dim,dim> >;

  constexpr unsigned int size() const
  {
    return volsize;
  }

  inline void evaluateFunction(const typename Traits::DomainType& in,
                               std::vector<typename Traits::RangeType>& out) const
  {
    out.resize(volsize);

    // x-direction fastest, y-direction middle, z-direction slowest
    std::size_t global_iter = 0.0;

    for(std::size_t i2=0; i2<k+1; ++i2)
      for(std::size_t i1=0; i1<k+1; ++i1)
        for(std::size_t i0=0; i0<k; ++i0) // reduced
        {
          out[global_iter] = {poly_km1.p(i0,in[0])*poly_k.p(i1,in[1])*poly_k.p(i2,in[2]), 0.0, 0.0};
          global_iter++;
        }

    for(std::size_t i2=0; i2<k+1; ++i2)
      for(std::size_t i1=0; i1<k; ++i1) // reduced
        for(std::size_t i0=0; i0<k+1; ++i0)
        {
          out[global_iter] = {0.0, poly_k.p(i0,in[0])*poly_km1.p(i1,in[1])*poly_k.p(i2,in[2]), 0.0};
          global_iter++;
        }

    for(std::size_t i2=0; i2<k; ++i2) // reduced
      for(std::size_t i1=0; i1<k+1; ++i1)
        for(std::size_t i0=0; i0<k+1; ++i0)
        {
          out[global_iter] = {0.0, 0.0, poly_k.p(i0,in[0])*poly_k.p(i1,in[1])*poly_km1.p(i2,in[2])};
          global_iter++;
        }

#if 0
    // x-direction fastest, y-direction middle, z-direction slowest
    out[0] =  {1.0,            0.0, 0.0};
    out[1] =  {in[0],           0.0, 0.0};
    out[2] =  {in[1],           0.0, 0.0};
    out[3] =  {in[0]*in[1],      0.0, 0.0};
    out[4] =  {in[1]*in[1],      0.0, 0.0};
    out[5] =  {in[0]*in[1]*in[1], 0.0, 0.0};
    //--------------------------------------------
    out[6] =  {in[2],                0.0, 0.0};
    out[7] =  {in[0]*in[2],           0.0, 0.0};
    out[8] =  {in[1]*in[2],           0.0, 0.0};
    out[9] =  {in[0]*in[1]*in[2],      0.0, 0.0};
    out[10] = {in[1]*in[1]*in[2],      0.0, 0.0};
    out[11] = {in[0]*in[1]*in[1]*in[2], 0.0, 0.0};
    //--------------------------------------------
    out[12] = {in[2]*in[2],                0.0, 0.0};
    out[13] = {in[0]*in[2]*in[2],           0.0, 0.0};
    out[14] = {in[1]*in[2]*in[2],           0.0, 0.0};
    out[15] = {in[0]*in[1]*in[2]*in[2],      0.0, 0.0};
    out[16] = {in[1]*in[1]*in[2]*in[2],      0.0, 0.0};
    out[17] = {in[0]*in[1]*in[1]*in[2]*in[2], 0.0, 0.0};

    out[18] = {0.0, 1.0,            0.0};
    out[19] = {0.0, in[0],           0.0};
    out[20] = {0.0, in[0]*in[0],      0.0};
    out[21] = {0.0, in[1],           0.0};
    out[22] = {0.0, in[0]*in[1],      0.0};
    out[23] = {0.0, in[0]*in[0]*in[1], 0.0};
    //--------------------------------------------
    out[24] = {0.0, in[2],                0.0};
    out[25] = {0.0, in[0]*in[2],           0.0};
    out[26] = {0.0, in[0]*in[0]*in[2],      0.0};
    out[27] = {0.0, in[1]*in[2],           0.0};
    out[28] = {0.0, in[0]*in[1]*in[2],      0.0};
    out[29] = {0.0, in[0]*in[0]*in[1]*in[2], 0.0};
    //--------------------------------------------
    out[30] = {0.0, in[2]*in[2],                0.0};
    out[31] = {0.0, in[0]*in[2]*in[2],           0.0};
    out[32] = {0.0, in[0]*in[0]*in[2]*in[2],      0.0};
    out[33] = {0.0, in[1]*in[2]*in[2],           0.0};
    out[34] = {0.0, in[0]*in[1]*in[2]*in[2],      0.0};
    out[35] = {0.0, in[0]*in[0]*in[1]*in[2]*in[2], 0.0};

    out[36] = {0.0, 0.0, 1.0};
    out[37] = {0.0, 0.0, in[0]};
    out[38] = {0.0, 0.0, in[0]*in[0]};
    out[39] = {0.0, 0.0, in[1]};
    out[40] = {0.0, 0.0, in[0]*in[1]};
    out[41] = {0.0, 0.0, in[0]*in[0]*in[1]};
    out[42] = {0.0, 0.0, in[1]*in[1]};
    out[43] = {0.0, 0.0, in[0]*in[1]*in[1]};
    out[44] = {0.0, 0.0, in[0]*in[0]*in[1]*in[1]};
    //--------------------------------------------
    out[45] = {0.0, 0.0, in[2]};
    out[46] = {0.0, 0.0, in[0]*in[2]};
    out[47] = {0.0, 0.0, in[0]*in[0]*in[2]};
    out[48] = {0.0, 0.0, in[1]*in[2]};
    out[49] = {0.0, 0.0, in[0]*in[1]*in[2]};
    out[50] = {0.0, 0.0, in[0]*in[0]*in[1]*in[2]};
    out[51] = {0.0, 0.0, in[1]*in[1]*in[2]};
    out[52] = {0.0, 0.0, in[0]*in[1]*in[1]*in[2]};
    out[53] = {0.0, 0.0, in[0]*in[0]*in[1]*in[1]*in[2]};
#endif

#if 0
    out[0][0] = 1.0; out[1][0] = in[1]; out[2][0] = in[1]*in[1]; out[3][0] = in[2]; out[4][0] = in[2]*in[2]; out[5][0] = in[1]*in[2]; out[6][0] = in[1]*in[2]*in[2]; out[7][0] = in[1]*in[1]*in[2]; out[8][0] = in[1]*in[1]*in[2]*in[2];
    out[0][1] = 0.0; out[1][1] = 0.0;  out[2][1] = 0.0;       out[3][1] = 0.0;  out[4][1] = 0.0;       out[5][1] = 0.0;       out[6][1] = 0.0;            out[7][1] = 0.0;            out[8][1] = 0.0;
    out[0][2] = 0.0; out[1][2] = 0.0;  out[2][2] = 0.0;       out[3][2] = 0.0;  out[4][2] = 0.0;       out[5][2] = 0.0;       out[6][2] = 0.0;            out[7][2] = 0.0;            out[8][2] = 0.0;

    out[9][0] = 0.0; out[10][0] = 0.0;  out[11][0] = 0.0;       out[12][0] = 0.0;  out[13][0] = 0.0;       out[14][0] = 0.0;       out[15][0] = 0.0;            out[16][0] = 0.0;            out[17][0] = 0.0;
    out[9][1] = 1.0; out[10][1] = in[0]; out[11][1] = in[0]*in[0]; out[12][1] = in[2]; out[13][1] = in[2]*in[2]; out[14][1] = in[0]*in[2]; out[15][1] = in[0]*in[2]*in[2]; out[16][1] = in[0]*in[0]*in[2]; out[17][1] = in[0]*in[0]*in[2]*in[2];
    out[9][2] = 0.0; out[10][2] = 0.0;  out[11][2] = 0.0;       out[12][2] = 0.0;  out[13][2] = 0.0;       out[14][2] = 0.0;       out[15][2] = 0.0;            out[16][2] = 0.0;            out[17][2] = 0.0;

    out[18][0] = 0.0; out[19][0] = 0.0;  out[20][0] = 0.0;       out[21][0] = 0.0;  out[22][0] = 0.0;       out[23][0] = 0.0;       out[24][0] = 0.0;            out[25][0] = 0.0;            out[26][0] = 0.0;
    out[18][1] = 0.0; out[19][1] = 0.0;  out[20][1] = 0.0;       out[21][1] = 0.0;  out[22][1] = 0.0;       out[23][1] = 0.0;       out[24][1] = 0.0;            out[25][1] = 0.0;            out[26][1] = 0.0;
    out[18][2] = 1.0; out[19][2] = in[0]; out[20][2] = in[0]*in[0]; out[21][2] = in[1]; out[22][2] = in[1]*in[1]; out[23][2] = in[0]*in[1]; out[24][2] = in[0]*in[1]*in[1]; out[25][2] = in[0]*in[0]*in[1]; out[26][2] = in[0]*in[0]*in[1]*in[1];

    out[27][0] = in[0]; out[28][0] = in[0]*in[1]; out[29][0] = in[0]*in[1]*in[1]; out[30][0] = in[0]*in[2]; out[31][0] = in[0]*in[2]*in[2]; out[32][0] = in[0]*in[1]*in[2]; out[33][0] = in[0]*in[1]*in[2]*in[2]; out[34][0] = in[0]*in[1]*in[1]*in[2]; out[35][0] = in[0]*in[1]*in[1]*in[2]*in[2];
    out[27][1] = 0.0;  out[28][1] = 0.0;       out[29][1] = 0.0;            out[30][1] = 0.0;       out[31][1] = 0.0;            out[32][1] = 0.0;            out[33][1] = 0.0;                 out[34][1] = 0.0;                 out[35][1] = 0.0;
    out[27][2] = 0.0;  out[28][2] = 0.0;       out[29][2] = 0.0;            out[30][2] = 0.0;       out[31][2] = 0.0;            out[32][2] = 0.0;            out[33][2] = 0.0;                 out[34][2] = 0.0;                 out[35][2] = 0.0;

    out[36][0] = 0.0;  out[37][0] = 0.0;       out[38][0] = 0.0;            out[39][0] = 0.0;       out[40][0] = 0.0;            out[41][0] = 0.0;            out[42][0] = 0.0;                 out[43][0] = 0.0;                 out[44][0] = 0.0;
    out[36][1] = in[1]; out[37][1] = in[1]*in[0]; out[38][1] = in[1]*in[0]*in[0]; out[39][1] = in[1]*in[2]; out[40][1] = in[1]*in[2]*in[2]; out[41][1] = in[1]*in[0]*in[2]; out[42][1] = in[1]*in[0]*in[2]*in[2]; out[43][1] = in[1]*in[0]*in[0]*in[2]; out[44][1] = in[1]*in[0]*in[0]*in[2]*in[2];
    out[36][2] = 0.0;  out[37][2] = 0.0;       out[38][2] = 0.0;            out[39][2] = 0.0;       out[40][2] = 0.0;            out[41][2] = 0.0;            out[42][2] = 0.0;                 out[43][2] = 0.0;                 out[44][2] = 0.0;

    out[45][0] = 0.0;  out[46][0] = 0.0;       out[47][0] = 0.0;            out[48][0] = 0.0;       out[49][0] = 0.0;            out[50][0] = 0.0;            out[51][0] = 0.0;                 out[52][0] = 0.0;                 out[53][0] = 0.0;
    out[45][1] = 0.0;  out[46][1] = 0.0;       out[47][1] = 0.0;            out[48][1] = 0.0;       out[49][1] = 0.0;            out[50][1] = 0.0;            out[51][1] = 0.0;                 out[52][1] = 0.0;                 out[53][1] = 0.0;
    out[45][2] = in[2]; out[46][2] = in[2]*in[0]; out[47][2] = in[2]*in[0]*in[0]; out[48][2] = in[2]*in[1]; out[49][2] = in[2]*in[1]*in[1]; out[50][2] = in[2]*in[0]*in[1]; out[51][2] = in[2]*in[0]*in[1]*in[1]; out[52][2] = in[2]*in[0]*in[0]*in[1]; out[53][2] = in[2]*in[0]*in[0]*in[1]*in[1];
#endif
  }

  inline void
  evaluateJacobian(const typename Traits::DomainType& in,
                   std::vector<typename Traits::JacobianType>& out) const
  {
    DUNE_THROW(Dune::NotImplemented,
      "evaluateJacobian for RaviartThomasVolumeLocalBasis not implemented!");
  }

  void partial(const std::array<unsigned int,dim>& order_,
               const typename Traits::DomainType& in,
               std::vector<typename Traits::RangeType>& out) const
  {
    DUNE_THROW(Dune::NotImplemented,
      "partial for RaviartThomasVolumeLocalBasis not implemented!");
  }

  constexpr unsigned int order() const
  {
    return k;
  }
private:
  Dune::QkStuff::GaussLobattoLagrangePolynomials<DF,RF,k>   poly_k;
  Dune::QkStuff::GaussLobattoLagrangePolynomials<DF,RF,k-1> poly_km1;
};

#endif /* Actual code */

} // namespace Dorie
} // namespace Dune

#endif // DUNE_DORIE_RAVIART_THOMAS_VOLUME_BASIS_HH
