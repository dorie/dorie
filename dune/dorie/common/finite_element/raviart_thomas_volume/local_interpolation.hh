#ifndef DUNE_DORIE_RAVIART_THOMAS_VOLUME_INTERPOLATION_HH
#define DUNE_DORIE_RAVIART_THOMAS_VOLUME_INTERPOLATION_HH

#include <dune/common/exceptions.hh>

#include <vector>

namespace Dune{
namespace Dorie{

/*-------------------------------------------------------------------------*//**
 * @brief      Class for local interpolation of the volume raviart thomas finite
 *             element @f$\mathcal{RT}_k^{vol}\f$.
 * @details    This class is used to reconstruct fluxes from local operators
 *             that solve fluxes in the residual vector. In particular, the
 *             content of the class is never needed for the reconstruction, and
 *             so, the usage of this class always throws an error.
 *
 * @author     Santiago Ospina De Los Ríos
 * @date       2018
 * @see        Dune::Dorie::RaviartThomasVolumeLocalFiniteElement.
 */
class RaviartThomasVolumeLocalInterpolation
{
public:

  RaviartThomasVolumeLocalInterpolation()
  {}

  template<typename F, typename C>
  void interpolate (const F& f, std::vector<C>& out) const
  {
    DUNE_THROW(Dune::NotImplemented,
      "interpolate for RaviartThomasVolumeLocalInterpolation not implemented!");
  }
};

} // namespace Dorie
} // namespace Dune

#endif // DUNE_DORIE_RAVIART_THOMAS_VOLUME_INTERPOLATION_HH
