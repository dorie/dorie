#ifndef DUNE_DORIE_RAVIART_THOMAS_VOLUME_COEFFICIENTS_HH
#define DUNE_DORIE_RAVIART_THOMAS_VOLUME_COEFFICIENTS_HH

#include <dune/localfunctions/common/localkey.hh>

#include <dune/geometry/type.hh>

#include <dune/common/exceptions.hh>
#include <dune/common/power.hh>

#include <vector>

namespace Dune{
namespace Dorie{

/*-------------------------------------------------------------------------*//**
 * @brief      Class for local coefficients of the volume raviart thomas finite
 *             element @f$\mathcal{RT}_k^{vol}\f$.
 * @details    This class is used to reconstruct fluxes from local operators
 *             that solve fluxes in the residual vector. In particular, since
 *             the volume part is always associated to the codimension 0, all
 *             the local keys are the same. Equivalent to DGLocalCoefficients.
 *
 * @author     Santiago Ospina De Los Ríos
 * @date       2018
 * @see        Dune::Dorie::RaviartThomasVolumeLocalFiniteElement.
 *
 * @tparam     DF    The domain field.
 * @tparam     RF    The range field.
 * @tparam     k     The order.
 * @tparam     dim   The dimension.
 * @tparam     gt    The geometry type.
 */
template<class DF, class RF, int k, int dim, Dune::GeometryType::BasicType gt>
class RaviartThomasVolumeLocalCoefficients
{
  //! Identify when there is no implementation of this class.
  static_assert( (gt == Dune::GeometryType::simplex) || 
                 (gt == Dune::GeometryType::cube), 
                 "Volume Raviart Thomas coefficients is not implemented!");

  //! Number of shape functions
  static constexpr int volsize = (gt == Dune::GeometryType::simplex)
        ? ((dim == 2) ? dim*k*(k+1)/2 : dim*k*(k+1)*(k+2)/6) // dim for simplex
        : (dim*Dune::StaticPower<k+1,dim-1>::power*k);       // dim for cubes
public:

  /*-----------------------------------------------------------------------*//**
   * @brief      Constructs the coeffcients. 
   * @details    All locall keys are associated to codimension 0.
   */
  RaviartThomasVolumeLocalCoefficients()
    : li(size())
  {
    for (unsigned int i = 0; i < size(); ++i)
      li[i] = LocalKey(0,0,i);
  }

  /*-----------------------------------------------------------------------*//**
   * @brief      Number of local coefficients
   *
   * @return     The size of the local coefficients
   */
  constexpr std::size_t size() const
  {
    return volsize;
  }

  /**
   * @brief      Local key for a given index.
   *
   * @param[in]  i     The index that identifies a shape function (see
   *                   Dune::Dorie::RaviartThomasVolumeLocalBasis).
   *
   * @return     The local key associated to the index i. (see Dune:.LocalKey in
   *             dune-localfunctions)
   */
  const LocalKey& localKey (std::size_t i) const
  {
    return li[i];
  }

private:
  //! The vector of local keys.
  std::vector<LocalKey> li;
};

} // namespace Dorie
} // namespace Dune

#endif // DUNE_DORIE_RAVIART_THOMAS_VOLUME_COEFFICIENTS_HH
