#ifndef DUNE_DORIE_RAVIART_THOMAS_VOLUME_LOCAL_FINITE_ELEMENT_HH
#define DUNE_DORIE_RAVIART_THOMAS_VOLUME_LOCAL_FINITE_ELEMENT_HH

#include <dune/geometry/type.hh>

#include <dune/dorie/common/finite_element/raviart_thomas_volume/local_basis.hh>
#include <dune/dorie/common/finite_element/raviart_thomas_volume/local_coefficients.hh>
#include <dune/dorie/common/finite_element/raviart_thomas_volume/local_interpolation.hh>

namespace Dune{
namespace Dorie{

/*-------------------------------------------------------------------------*//**
 * @brief      Class for volume part of the raviart thomas finite element
 *             @f$\mathcal{RT}_k^{vol}\f$.
 * @details    This class is used to reconstruct fluxes from local operators
 *             that solve fluxes in the residual vector. In particular, this
 *             volume raviart thomas is equivalent to the usual raviart thomas
 *             element but excluding the degrees of freedom located on the faces
 *             (i.e. codim = 1).
 *
 *             Available elements:
 *
 *             <table> <tr> <th colspan="2">Order</th> <th>0</th> <th>1</th>
 *             <th>2</th> <th>3</th> </tr> <tr> <td rowspan="2"><br>2D</td>
 *             <td>Simplex</td> <td>✓</td> <td>✓</td> <td>✓</td> <td>✗</td>
 *             </tr> <tr> <td>Cube</td> <td>✓</td> <td>✓</td> <td>✓</td>
 *             <td>✗</td> </tr> <tr> <td rowspan="2"><br>3D</td>
 *             <td>Simplex</td> <td>✓</td> <td>✓</td> <td>✓</td> <td>✗</td>
 *             </tr> <tr> <td>Cube</td> <td>✓</td> <td>✓</td> <td>✓</td>
 *             <td>✗</td> </tr> </table>
 *
 * @author     Santiago Ospina De Los Ríos
 * @date       2018
 * @ingroup    FiniteElement
 * @ingroup    FluxReconstruction
 * @see        Dune::Dorie::RaviartThomasVolumeLocalBasis.
 * @see        Dune::Dorie::RaviartThomasVolumeLocalCoefficients.
 * @see        Dune::Dorie::RaviartThomasVolumeLocalInterpolation.
 *
 * @tparam     DF    The domain field.
 * @tparam     RF    The range field.
 * @tparam     k     The order.
 * @tparam     d     The dimension.
 * @tparam     gt    The geometry type.
 */
template<class DF, class RF, int k, int d, Dune::GeometryType::BasicType gt>
class RaviartThomasVolumeLocalFiniteElement
{
public:

   //! Traits of local basis. It follows the traits structure for finite 
   //! elements defined by dune-localfunctions (Dune::LocalFiniteElementTraits).
  using Traits = Dune::LocalFiniteElementTraits<
                            RaviartThomasVolumeLocalBasis<DF,RF,k,d,gt>, 
                            RaviartThomasVolumeLocalCoefficients<DF,RF,k,d,gt>,
                            RaviartThomasVolumeLocalInterpolation>;

  /*-----------------------------------------------------------------------*//**
   * @brief      The local basis for this finite element.
   * @see        Dune::Dorie::RaviartThomasVolumeLocalBasis.
   *
   * @return     The local basis.
   */
  const typename Traits::LocalBasisType& localBasis() const
  {
    return _basis;
  }

  /*-----------------------------------------------------------------------*//**
   * @brief      The local coefficients for this finite element.
   * @see        Dune::Dorie::RaviartThomasVolumeLocalCoefficients.
   *
   * @return     The local coefficients.
   */
  const typename Traits::LocalCoefficientsType& localCoefficients() const
  {
    return _coefficients;
  }

  /*-----------------------------------------------------------------------*//**
   * @brief      The local interpolation for this finite element.
   * @see        Dune::Dorie::RaviartThomasVolumeLocalInterpolation.
   *
   * @return     The local interpolation.
   */
  const typename Traits::LocalInterpolationType& localInterpolation() const
  {
    return _interpolation;
  }

  /*-----------------------------------------------------------------------*//**
   * @brief      Number of shape functions (i.e. degrees of freedom).
   *
   * @return     The number of shape functions.
   */
  constexpr unsigned int size() const
  {
    return _basis.size();
  }

  /*-----------------------------------------------------------------------*//**
   * @brief      The geometry type for which this finite element is implemented.
   *
   * @todo       Fix the deprecated BasicGeometryType: This depends on the 
   *             decision that the dune community make about this type. Right 
   *             now there is not an easy way to replace it.
   *
   * @return     The geometry type.
   */
  static constexpr GeometryType type()
  {
    DUNE_NO_DEPRECATED_BEGIN
      return GeometryType(gt,d);
    DUNE_NO_DEPRECATED_END
  }

private:
  //! The local basis.
  RaviartThomasVolumeLocalBasis<DF,RF,k,d,gt>         _basis;
  //! The local coefficients.
  RaviartThomasVolumeLocalCoefficients<DF,RF,k,d,gt>  _coefficients;
  //! The local interpolation.
  RaviartThomasVolumeLocalInterpolation               _interpolation;
};

} // namespace Dorie
} // namespace Dune

#endif // DUNE_DORIE_RAVIART_THOMAS_VOLUME_LOCAL_FINITE_ELEMENT_HH