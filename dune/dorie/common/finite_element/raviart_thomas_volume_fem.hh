#ifndef DUNE_DORIE_RAVIART_THOMAS_VOLUME_FEM_HH
#define DUNE_DORIE_RAVIART_THOMAS_VOLUME_FEM_HH

#include <dune/geometry/type.hh>

#include <dune/dorie/common/finite_element/raviart_thomas_volume/local_finite_element.hh>

namespace Dune{
namespace Dorie{

/*-------------------------------------------------------------------------*//**
 * @brief      Finite element map for volume part of the raviart thomas
 *             @f$\mathcal{RT}_k^{vol}\f$.
 * @details    This class basically provides a volume raviart thomas finite
 *             element
 *             @f$\mathcal{RT}_k^{vol}\f$ for any entity following the finite
 *             element map defined by dune-pdelab (see
 *             Dune::PDELab::LocalFiniteElementMapInterface).
 *
 *             Available elements: <table> <tr> <th colspan="2">Order</th>
 *             <th>0</th> <th>1</th> <th>2</th> <th>3</th> </tr> <tr> <td
 *             rowspan="2"><br>2D</td> <td>Simplex</td> <td>✓</td> <td>✓</td>
 *             <td>✓</td> <td>✗</td> </tr> <tr> <td>Cube</td> <td>✓</td>
 *             <td>✓</td> <td>✓</td> <td>✗</td> </tr> <tr> <td
 *             rowspan="2"><br>3D</td> <td>Simplex</td> <td>✓</td> <td>✓</td>
 *             <td>✓</td> <td>✗</td> </tr> <tr> <td>Cube</td> <td>✓</td>
 *             <td>✓</td> <td>✓</td> <td>✗</td> </tr> </table>
 *
 * @author     Santiago Ospina De Los Ríos
 * @date       2018
 * @ingroup    FluxReconstruction
 * @see        Dune::Dorie::RaviartThomasVolumeLocalFiniteElement.
 *
 * @tparam     DF    The domain field.
 * @tparam     RF    The range field.
 * @tparam     k     The order.
 * @tparam     d     The dimension.
 * @tparam     gt    The geometry type.
 */
template<class DF, class RF, int k, int d, Dune::GeometryType::BasicType gt>
class RaviartThomasVolumeLocalFiniteElementMap
  : public Dune::PDELab::SimpleLocalFiniteElementMap<
                          RaviartThomasVolumeLocalFiniteElement<DF,RF,k,d,gt>,d>
{
public:
  /*-----------------------------------------------------------------------*//**
   * @brief      Constructor of RaviartThomasVolumeLocalFiniteElementMap
   */
  RaviartThomasVolumeLocalFiniteElementMap() {}

  /*-----------------------------------------------------------------------*//**
   * @brief      A FiniteElementMap is fixedSize iif the size of the local
   *             functions space for each GeometryType is fixed.
   *
   * @return     A fixed size bool.
   */
  static constexpr bool fixedSize()
  {
    return true;
  }

  /*-----------------------------------------------------------------------*//**
   * @brief      Return if FiniteElementMap has degrees of freedom for given
   *             codimension.
   *
   * @param[in]  codim  The codimension.
   *
   * @return     True if has degreed of freedom for codimension codim, False
   *             otherwise.
   */
  static constexpr bool hasDOFs(int codim)
  {
    return (codim == 0);
  }

  /*-----------------------------------------------------------------------*//**
   * @brief      If the FiniteElementMap is fixedSize, the size methods computes
   *             the number of DOFs for given GeometryType.
   *
   * @param[in]  geometry_type  The geometry type.
   *
   * @return     The size of the fem for fixed size map.
   */
  std::size_t size(GeometryType geometry_type) const
  {
    return (geometry_type.dim() == d) ? _fe.size(): 0;
  }

  /*-----------------------------------------------------------------------*//**
   * @brief      Computes an upper bound for the local number of DOFs.
   *
   * @details    This upper bound is used to avoid reallocations in std::vectors
   *             used during the assembly.
   *
   * @return     The maximum local size of all finite elements used by the map.
   */
  std::size_t maxLocalSize() const
  {
    return _fe.size();
  }

private:
  //! The raviart thomas finite element.
  const RaviartThomasVolumeLocalFiniteElement<DF,RF,k,d,gt> _fe;
};

} // namespace Dorie
} // namespace Dune

#endif // DUNE_DORIE_RAVIART_THOMAS_VOLUME_FEM_HH