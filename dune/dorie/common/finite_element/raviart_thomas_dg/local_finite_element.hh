#ifndef DUNE_DORIE_RAVIART_THOMAS_SIMPLEX_DG_LOCAL_FINITE_ELEMENT_HH
#define DUNE_DORIE_RAVIART_THOMAS_SIMPLEX_DG_LOCAL_FINITE_ELEMENT_HH

#include <dune/localfunctions/utility/localfiniteelement.hh>
#include <dune/localfunctions/raviartthomas/raviartthomassimplex.hh>

#include <dune/dorie/common/finite_element/dg/local_coefficients.hh>

namespace Dune{
namespace Dorie{

/*-------------------------------------------------------------------------*//**
 * @brief      Discontinuous Galerkin Raviart thomas for simplices 
 *             @f$\mathcal{RT}_k^{dg}\f$.
 * @details    This class is equivalent to
 *             Dune::RaviartThomasSimplexLocalFiniteElement but with local keys
 *             corresponding to discontinuous galerkin methods, that is, all
 *             shape functions are associated to entities of codimension 0.
 * @author     Santiago Ospina De Los Ríos
 * @date       2018
 * @ingroup    FiniteElement
 * @see        Dune::Dorie::DGLocalCoefficients
 *
 * @tparam     dim   World dimension.
 * @tparam     DF    Domain field.
 * @tparam     RF    Range field.
 * @tparam     SF    Storage field for basis matrix.
 * @tparam     CF    Compute field for basis matrix.
 */
template<unsigned int dim, class DF, class RF,class SF=RF, class CF=SF>
class RaviartThomasSimplexDGLocalFiniteElement
  : public Dune::RaviartThomasSimplexLocalFiniteElement<dim,DF,RF,SF,CF>
{
  using LocalCoefficients = Dune::Dorie::DGLocalCoefficients;
  using Base = Dune::RaviartThomasSimplexLocalFiniteElement<dim,DF,RF,SF,CF>;
public:

  using Traits = Dune::LocalFiniteElementTraits< 
      typename Base::Traits::LocalBasisType,
      LocalCoefficients,
      typename Base::Traits::LocalInterpolationType>;

  
  /*-----------------------------------------------------------------------*//**
   * @brief      Constructor of the class.
   *
   * @param[in]  gt    Geometry type (only simplices).
   * @param[in]  k     Order of the Raviart Thomas finite element.
   */
  RaviartThomasSimplexDGLocalFiniteElement(
    const Dune::GeometryType &gt, 
    unsigned int k
  ) : Base(gt, k)
    , local_coefficients((dim == 2) ? (k+1)*(k+3) : (k+1)*(k+2)*(k+4)/2) 
  {
    assert(gt.isSimplex());
  }

  /*-----------------------------------------------------------------------*//**
   * @brief      The local basis for this finite element.
   * @see        Dune::RaviartThomasSimplexLocalFiniteElement.
   *
   * @return     The local basis.
   */
  const typename Traits::LocalBasisType& localBasis () const
  {
    return Base::localBasis();
  }

  /*-----------------------------------------------------------------------*//**
   * @brief      The local coefficients for this finite element.
   * @see        Dune::Dorie::DGLocalCoefficients
   *
   * @return     The local coefficients.
   */
  const typename Traits::LocalCoefficientsType& localCoefficients () const
  {
    return local_coefficients;
  }

  /*-----------------------------------------------------------------------*//**
   * @brief      The local interpolation for this finite element.
   * @see        Dune::RaviartThomasSimplexLocalFiniteElement.
   *
   * @return     The local interpolation.
   */
  const typename Traits::LocalInterpolationType& localInterpolation () const
  {
    return Base::localInterpolation();
  }

private:
  LocalCoefficients local_coefficients;
};


} // namespace Dorie
} // namespace Dune

#endif // DUNE_DORIE_RAVIART_THOMAS_SIMPLEX_DG_LOCAL_FINITE_ELEMENT_HH
