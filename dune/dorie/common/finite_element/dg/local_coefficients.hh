#ifndef DUNE_DORIE_DG_LOCAL_COEFFICIENTS_HH
#define DUNE_DORIE_DG_LOCAL_COEFFICIENTS_HH

#include <vector>

#include <dune/localfunctions/common/localkey.hh>

namespace Dune{
namespace Dorie{

/*-------------------------------------------------------------------------*//**
 * @brief      General local keys for dg finite elements.
 *
 * @author     Santiago Ospina De Los Ríos
 * @date       2018
 * @ingroup    FiniteElement
 */
class DGLocalCoefficients
{
public:

  /*-----------------------------------------------------------------------*//**
   * @brief      Constructs the coeffcients.
   * @details    All locall keys are associated to codimension 0.
   *
   * @param[in]  s     The size of the local basis.
   */
  DGLocalCoefficients(unsigned int s)
    : _s(s), li(_s)
  {
    for (unsigned int i = 0; i < size(); ++i)
      li[i] = Dune::LocalKey(0,0,i);
  }

  /*-----------------------------------------------------------------------*//**
   * @brief      Number of local coefficients
   *
   * @return     The size of the local coefficients
   */
  std::size_t size() const
  {
    return _s;
  }

  /**
   * @brief      Local key for a given index.
   *
   * @param[in]  i     The index that identifies a shape function.
   *
   * @return     The local key associated to the index i. (see Dune:.LocalKey in
   *             dune-localfunctions)
   */
  const LocalKey& localKey (std::size_t i) const
  {
    return li[i];
  }

private:
  //! The size.
  std::size_t _s;
  //! The vector of local keys.
  std::vector<Dune::LocalKey> li;
};


} // namespace Dorie
} // namespace Dune

#endif // DUNE_DORIE_DG_LOCAL_COEFFICIENTS_HH
