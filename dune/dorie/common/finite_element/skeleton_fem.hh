#ifndef DUNE_DORIE_SKELETON_FEM_HH
#define DUNE_DORIE_SKELETON_FEM_HH

#include <dune/localfunctions/lagrange/pk.hh>
#include <dune/localfunctions/lagrange/qk.hh>

#include <dune/geometry/type.hh>

#include <dune/common/power.hh>

#include <dune/pdelab/finiteelement/l2orthonormal.hh>
#include <dune/pdelab/finiteelementmap/finiteelementmap.hh>

namespace Dune{
namespace Dorie{


/*-------------------------------------------------------------------------*//**
 * @brief      Finite element map for entities of codimension 1 (grid skeleton)
 *             @f$\mathcal{P}_k\f$.
 * @details    This class provides
 *             @f$\mathcal{P}_k\f$ finite element for any entities of
 *             codimension 1 following the finite element map defined by
 *             dune-pdelab (see Dune::PDELab::LocalFiniteElementMapInterface).
 *             In this case, the method `find()` expets an intersection, then,
 *             the mapping assign a finite element to the inside entity. It
 *             means that for finite element maps whit different finite elements
 *             (this is not the case) the local operator has to visit the two
 *             sides of the intersection in order to map to all finite elements.
 *
 *             Available elements:
 *
 *             <table> <tr> <th colspan="2">Order</th> <th>0</th> <th>1</th>
 *             <th>2</th> <th>3</th> </tr> <tr> <td>2D</td> <td>Simplex</td>
 *             <td>✓</td> <td>✓</td> <td>✓</td> <td>✓</td> </tr> <tr>
 *             <td>3D</td> <td>Simplex</td> <td>✓</td> <td>✓</td> <td>✓</td>
 *             <td>✓</td> </tr> </table>
 *
 * @warning    This finite element map does not work for the usual finite
 *             element solutions. It's one for Flux Reconstruction purposes. It
 *             particuarlly fails when the usual pdelab local function space
 *             tries to bind an entity to this element. See
 *             Dune::Dorie::MinimalLocalFunctionSpace and
 *             Dune::PDELab::LocalFunctionSpace.
 *
 * @author     Santiago Ospina De Los Ríos
 * @date       2018
 * @ingroup    FluxReconstruction
 * @ingroup    FiniteElement
 * @see        Dune::Dorie::SkeletonQkFiniteElementMap.
 * @see        Dune::Dorie::SkeletonFiniteElementMap.
 *
 * @tparam     DF    The domain field.
 * @tparam     RF    The range field.
 * @tparam     dim   The world dimension.
 * @tparam     k     The order.
 */
template<class DF, class RF, int dim, int k>
class  SkeletonPkFiniteElementMap
  : public Dune::PDELab::SimpleLocalFiniteElementMap<
                            Dune::PkLocalFiniteElement<DF,RF,dim-1,k>,dim-1>
{
public:

  /*-----------------------------------------------------------------------*//**
   * @brief      A FiniteElementMap is fixedSize iif the size of the local
   *             functions space for each GeometryType is fixed.
   *
   * @return     A fixed size bool.
   */
  static constexpr bool fixedSize()
  {
    return true;
  }

  /*-----------------------------------------------------------------------*//**
   * @brief      Return if FiniteElementMap has degrees of freedom for given
   *             codimension.
   *
   * @param[in]  codim  The codimension.
   *
   * @return     True if has degreed of freedom for codimension codim (with 
   *             respect to the world dimension), False otherwise.
   */
  static constexpr bool hasDOFs(int codim)
  {
    return codim == 1;
  }

  /*-----------------------------------------------------------------------*//**
   * @brief      If the FiniteElementMap is fixedSize, the size methods computes
   *             the number of DOFs for given GeometryType.
   *
   * @param[in]  geometry_type  The geometry type.
   *
   * @return     The size of the fem for fixed size map.
   */
  static constexpr std::size_t size(GeometryType gt)
  {
    return Dune::PB::PkSize<k,dim-1>::value;
  }

  /*-----------------------------------------------------------------------*//**
   * @brief      Computes an upper bound for the local number of DOFs.
   *
   * @details    This upper bound is used to avoid reallocations in std::vectors
   *             used during the assembly.
   *
   * @return     The maximum local size of all finite elements used by the map.
   */
  static constexpr std::size_t maxLocalSize()
  {
    return Dune::PB::PkSize<k,dim-1>::value;
  }

};

/*-------------------------------------------------------------------------*//**
 * @brief      Finite element map for entities of codimension 1 (grid skeleton)
 *             @f$\mathcal{Q}_k\f$.
 * @details    This class provides
 *             @f$\mathcal{Q}_k\f$ finite element for any entities of
 *             codimension 1 following the finite element map defined by
 *             dune-pdelab (see Dune::PDELab::LocalFiniteElementMapInterface).
 *             In this case, the method `find()` expets an intersection, then,
 *             the mapping assign a finite element to the inside entity. It
 *             means that for finite element maps whit different finite elements
 *             (this is not the case) the local operator has to visit the two
 *             sides of the intersection in order to map to all finite elements.
 *
 *             Available elements:
 *
 *             <table> <tr> <th colspan="2">Order</th> <th>0</th> <th>1</th>
 *             <th>2</th> <th>3</th> </tr> <tr> <td>2D</td> <td>Cube</td>
 *             <td>✓</td> <td>✓</td> <td>✓</td> <td>✓</td> </tr> <tr>
 *             <td>3D</td> <td>Cube</td> <td>✓</td> <td>✓</td> <td>✓</td>
 *             <td>✓</td> </tr> </table>
 *             
 * @warning    This finite element map does not work for the usual finite
 *             element solutions. It's one for Flux Reconstruction purposes. It
 *             particuarlly fails when the usual pdelab local function space
 *             tries to bind an entity to this element. See
 *             Dune::Dorie::MinimalLocalFunctionSpace and
 *             Dune::PDELab::LocalFunctionSpace.
 *             
 * @author     Santiago Ospina De Los Ríos
 * @date       2018
 * @ingroup    FluxReconstruction
 * @ingroup    FiniteElement
 * @see        Dune::Dorie::SkeletonPkFiniteElementMap.
 * @see        Dune::Dorie::SkeletonFiniteElementMap.
 * 
 * @tparam     DF    The domain field.
 * @tparam     RF    The range field.
 * @tparam     dim   The world dimension.
 * @tparam     k     The order.
 */
template<class DF, class RF, int dim, int k>
class  SkeletonQkFiniteElementMap
  : public Dune::PDELab::SimpleLocalFiniteElementMap<
                            Dune::QkLocalFiniteElement<DF,RF,dim-1,k>,dim-1>
{
public:

  /*-----------------------------------------------------------------------*//**
   * @brief      A FiniteElementMap is fixedSize iif the size of the local
   *             functions space for each GeometryType is fixed.
   *
   * @return     A fixed size bool.
   */
  static constexpr bool fixedSize()
  {
    return true;
  }

  /*-----------------------------------------------------------------------*//**
   * @brief      Return if FiniteElementMap has degrees of freedom for given
   *             codimension.
   *
   * @param[in]  codim  The codimension.
   *
   * @return     True if has degreed of freedom for codimension codim (with 
   *             respect to the world dimension), False otherwise.
   */
  static constexpr bool hasDOFs(int codim)
  {
    return codim == 1;
  }

  /*-----------------------------------------------------------------------*//**
   * @brief      If the FiniteElementMap is fixedSize, the size methods computes
   *             the number of DOFs for given GeometryType.
   *
   * @param[in]  geometry_type  The geometry type.
   *
   * @return     The size of the fem for fixed size map.
   */
  static constexpr std::size_t size(GeometryType gt)
  {
    return Dune::StaticPower<k+1,dim-1>::power;
  }

  /*-----------------------------------------------------------------------*//**
   * @brief      Computes an upper bound for the local number of DOFs.
   *
   * @details    This upper bound is used to avoid reallocations in std::vectors
   *             used during the assembly.
   *
   * @return     The maximum local size of all finite elements used by the map.
   */
  static constexpr std::size_t maxLocalSize()
  {
    return Dune::StaticPower<k+1,dim-1>::power;
  }

};


/*-------------------------------------------------------------------------*//**
 * @brief      Finite element map for entities of codimension 1 (grid skeleton)
 *             @f$\mathcal{P}_k / \mathcal{Q}_k\f$.
 * @details    If gt is simplex then this class is
 *             Dune::Dorie::SkeletonPkFiniteElementMap, if gt is cube the it is
 *             Dune::Dorie::SkeletonQkFiniteElementMap, otherwise a compiler
 *             error will arise.
 *
 *             Available elements:
 *
 *             <table> <tr> <th colspan="2">Order</th> <th>0</th> <th>1</th>
 *             <th>2</th> <th>3</th> </tr> <tr> <td rowspan="2">2D</td>
 *             <td>Simplex</td> <td>✓</td> <td>✓</td> <td>✓</td> <td>✓</td>
 *             </tr> <tr> <td>Cube</td> <td>✓</td> <td>✓</td> <td>✓</td>
 *             <td>✓</td> </tr> <tr> <td rowspan="2">3D</td>
 *             <td>Simplex</td> <td>✓</td> <td>✓</td> <td>✓</td> <td>✓</td>
 *             </tr> <tr> <td>Cube</td> <td>✓</td> <td>✓</td> <td>✓</td>
 *             <td>✓</td> </tr> </table>
 *
 * @warning    This finite element map does not work for the usual finite
 *             element solutions. It's one for Flux Reconstruction purposes. It
 *             particuarlly fails when the usual pdelab local function space
 *             tries to bind an entity to this element. See
 *             Dune::Dorie::MinimalLocalFunctionSpace and
 *             Dune::PDELab::LocalFunctionSpace.
 *
 * @author     Santiago Ospina De Los Ríos
 * @date       2018
 * @ingroup    FluxReconstruction
 * @ingroup    FiniteElement
 * @see        Dune::Dorie::SkeletonQkFiniteElementMap.
 * @see        Dune::Dorie::SkeletonPkFiniteElementMap.
 *
 * @tparam     DF    The domain field.
 * @tparam     RF    The range field.
 * @tparam     dim   The world dimension.
 * @tparam     k     The order.
 * @tparam     gt    The geometry type
 */
template<class DF, class RF, int dim, int k, Dune::GeometryType::BasicType gt>
struct SkeletonFiniteElementMap
{
  static_assert(Dune::AlwaysFalse<DF>::value,"Not implemented!");
};

template<class DF, class RF, int dim, int k>
struct SkeletonFiniteElementMap<DF,RF,dim,k,Dune::GeometryType::simplex>
  : public SkeletonPkFiniteElementMap<DF,RF,dim,k>
{};

template<class DF, class RF, int dim, int k>
struct SkeletonFiniteElementMap<DF,RF,dim,k,Dune::GeometryType::cube>
  : public SkeletonQkFiniteElementMap<DF,RF,dim,k>
{};

} // namespace Dorie
} // namespace Dune

#endif // DUNE_DORIE_SKELETON_FEM_HH
