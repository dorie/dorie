#ifndef DUNE_DORIE_PK_DG_FEM_HH
#define DUNE_DORIE_PK_DG_FEM_HH

#include <dune/pdelab/finiteelementmap/finiteelementmap.hh>

#include <dune/dorie/common/finite_element/pk_dg/local_finite_element.hh>

namespace Dune{
namespace Dorie{

/**
 * @brief      Finite element map for discontinuous
 *             @f$\mathcal{P}_k^{dg}\f$ elements.
 * @details    This class implements a dune-pdelab finite element map  
 *             Dune::Dorie::PkDGLocalFiniteElement
 *
 *             
 * @author     Santiago Ospina De Los Ríos
 * @date       2018
 * @ingroup    FiniteElement
 * 
 * @tparam     GV    Grid view type.
 * @tparam     DF    Domain field type.
 * @tparam     RF    Range field type.
 * @tparam     k     Order.
 */
template<class DF, class RF, int dim, int k>
class PkDGLocalFiniteElementMap
  : public Dune::PDELab::SimpleLocalFiniteElementMap<
      Dune::Dorie::PkDGLocalFiniteElement<DF,RF,dim,k>,dim>
{
  using FE = Dune::Dorie::
             PkDGLocalFiniteElement<DF,RF,dim,k>;
  using Base = Dune::PDELab::SimpleLocalFiniteElementMap<FE,dim>;

  static constexpr unsigned int N = FE::Traits::LocalBasisType::N;

public:
  PkDGLocalFiniteElementMap()
  {}

  /*-----------------------------------------------------------------------*//**
   * @brief      A FiniteElementMap is fixedSize iif the size of the local
   *             functions space for each GeometryType is fixed.
   *
   * @return     A fixed size bool.
   */
  static constexpr bool fixedSize()
  {
    return true;
  }

  /*-----------------------------------------------------------------------*//**
   * @brief      Return if FiniteElementMap has degrees of freedom for given
   *             codimension.
   *
   * @param[in]  codim  The codimension.
   *
   * @return     True if has degreed of freedom for codimension codim, False
   *             otherwise.
   */
  static constexpr bool hasDOFs(int codim)
  {
    return (codim == 0);
  }

  /*-----------------------------------------------------------------------*//**
   * @brief      If the FiniteElementMap is fixedSize, the size methods computes
   *             the number of DOFs for given GeometryType.
   *
   * @param[in]  geometry_type  The geometry type.
   *
   * @return     The size of the fem for fixed size map.
   */
  std::size_t size(GeometryType geometry_type) const
  {
    return (geometry_type.dim() == dim) ? N : 0;
  }

  /*-----------------------------------------------------------------------*//**
   * @brief      Computes an upper bound for the local number of DOFs.
   *
   * @details    This upper bound is used to avoid reallocations in std::vectors
   *             used during the assembly.
   *
   * @return     The maximum local size of all finite elements used by the map.
   */
  std::size_t maxLocalSize() const
  {
    return N;
  }
};

} // namespace Dorie
} // namespace Dune

#endif // DUNE_DORIE_PK_DG_FEM_HH
