#ifndef DUNE_DORIE_INITIAL_CONDITION_FACTORY_HH
#define DUNE_DORIE_INITIAL_CONDITION_FACTORY_HH

#include <string>
#include <memory>

#include <dune/common/exceptions.hh>

#include <dune/dorie/common/initial_condition/analytic.hh>
#include <dune/dorie/common/logging.hh>
#include <dune/dorie/common/initial_condition/h5.hh>

namespace Dune{
namespace Dorie{

/*-------------------------------------------------------------------------*//**
 * @brief      Abstract Base Class for initial conditions.
 *
 * @ingroup    InitialConditions
 * @author     Santiago Ospina De Los Ríos
 * @date       2019
 *
 * @tparam     T     BaseTraits
 */
template<class T>
class InitialConditionFactory
{
protected:

  /// Create a user defined initial condition
  static std::unique_ptr<InitialCondition<T>> create(
    const Dune::ParameterTree& ini_file,
    const typename T::GV& grid_view,
    std::shared_ptr<spdlog::logger> log=get_logger(log_base)
  )
  {
    std::unique_ptr<InitialCondition<T>> ic;

    auto ic_type = ini_file.get<std::string>("initial.type");
    log->debug("Creating initial condition of type: {}", ic_type);

    if (ic_type == "data")
    {
      const auto ic_datafile = ini_file.get<std::string>("initial.file");

      // determine data type
      const auto ext_start = ic_datafile.find_last_of(".");
      if (ext_start == std::string::npos) {
        log->error("Cannot determine extension of initial condition "
                   "data file: {}",
                   ic_datafile);
        DUNE_THROW(IOError, "Initial condition datafile has no apparent "
                            "extension");
      }

      const auto file_type = ic_datafile.substr(ext_start + 1);
      if (file_type == "h5") {
        using ICH5 = InitialConditionH5<T>;
        ic = std::make_unique<ICH5>(grid_view, ini_file, log);

      } else if (file_type == "vtu") {
        DUNE_THROW(NotImplemented,
          "Initial condition from VTK data files is not implemented yet!");
      } else {
        log->error("Unsupported initial condition datafile type: .{}",
                   file_type);
        DUNE_THROW(NotImplemented,
                   "Unsupported initial condition data file type");
      }
    } // ic_type == "data"
    else if (ic_type == "analytic") 
    {
      auto equation_str = ini_file.get<std::string>("initial.equation");

      using ICAnalytic = InitialConditionAnalytic<T>;
      ic = std::make_unique<ICAnalytic>(log,grid_view,equation_str);
    }
    else {
      log->error("Unsupported initial condition type: {}", ic_type);
      DUNE_THROW(NotImplemented, "Unsupported initial condition type");
    }

    return ic;
  }

};

} // namespace Dorie
} // namespace Dune

#endif // DUNE_DORIE_INITIAL_CONDITION_FACTORY_HH
