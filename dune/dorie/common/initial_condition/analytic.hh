#ifndef DUNE_DORIE_INITIAL_CONDITION_ANALYTIC_HH
#define DUNE_DORIE_INITIAL_CONDITION_ANALYTIC_HH

#include <string>
#include <algorithm>

#include <dune/common/exceptions.hh>
#include <dune/common/parametertree.hh>
#include <dune/common/math.hh>

#include <dune/dorie/common/initial_condition/initial_condition.hh>

#include <muParser.h>

namespace Dune {
namespace Dorie {


/*-------------------------------------------------------------------------*//**
 * @brief      Class for analytic initial conditions.
 *
 * @ingroup    InitialConditions
 * @author     Santiago Ospina De Los Ríos
 * @date       2019
 *
 * @tparam     T     BaseTraits
 */
template<class T>
class InitialConditionAnalytic
     : public InitialCondition<T>
{
private:

  using RF = typename T::RF;
  using Domain = typename T::Domain;
  using GV = typename T::GV;
  using Base = InitialCondition<T>;
  static constexpr int dim = T::dim;


public:

  using Traits = typename Base::Traits;

  /// Initial Condition Class Constructor. Reads initial condition parameters
  /** \throw Dune::IOError BC File Type not supported
   */
  InitialConditionAnalytic (const std::shared_ptr<spdlog::logger> log,
                            const GV& gv, 
                            const std::string& equation_str)
    : Base(gv)
    , _log(log)
  {
    _log->trace("Setting up parser for evaluating analytic function");

    // initialize position vector
    std::fill(_pos_global.begin(), _pos_global.end(), 0.0);

    // initialize parser
    _parser.DefineConst("pi",
                        Dune::StandardMathematicalConstants<double>::pi());
    _parser.DefineConst("dim", dim);

    _parser.DefineVar("x", &_pos_global[0]);
    _parser.DefineVar("y", &_pos_global[1]);
    if constexpr (dim == 2) {
      _parser.DefineVar("h", &_pos_global[1]);
    }
    else if constexpr (dim == 3) {
      _parser.DefineVar("h", &_pos_global[2]);
      _parser.DefineVar("z", &_pos_global[2]);
    }

    // set up parser expression
    try {
      _parser.SetExpr(equation_str);
      // try to evaluate once
      _parser.Eval();
    } catch (mu::Parser::exception_type& e) {
      handle_parser_error(e);
    }
  }

  ~InitialConditionAnalytic() override {};

  /// Evaluate the initial condition at certain position
  /**
   *  \param e[in] Element entity
   *  \param x[in] Evaluation position in local element coordinates
   *  \param y[out] Value of the grid function
   */
  void evaluate ( const typename Traits::ElementType& e,
                  const typename Traits::DomainType& x,
                        typename Traits::RangeType& y) const override
  {
    // update position storage
    _pos_global = e.geometry().global(x);

    // evaluate the expression (with new position)
    try {
      y = _parser.Eval();
    }
    catch (mu::Parser::exception_type& e) {
      handle_parser_error(e);
    }
  }

private:
  /// The logger of this instance
  const std::shared_ptr<spdlog::logger> _log;
  /// Cache for the evaluation position
  mutable Domain _pos_global;
  /// The parser instance
  mu::Parser _parser;

  /// Output information on the parser error and throw DUNE exception
  /**
   *  \param e Exception thrown by the parser
   *  \throw IOError (always throws)
   */
  void handle_parser_error (const mu::Parser::exception_type& e) const
  {
    _log->error("Evaluating analytic initial condition failed:");
    _log->error("  Parsed expression:   {}",e.GetExpr());
    _log->error("  Token:               {}",e.GetToken());
    _log->error("  Error position:      {}",e.GetPos());
    _log->error("  Error code:          {}",e.GetCode());
    _log->error("  Error message:       {}",e.GetMsg());
    DUNE_THROW(IOError, "Error evaluating analytic initial condition");
  }
};

}
}

#endif // DUNE_DORIE_INITIAL_CONDITION_ANALYTIC_HH
