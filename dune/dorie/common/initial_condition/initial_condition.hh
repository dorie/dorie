#ifndef DUNE_DORIE_INITIAL_CONDITION_HH
#define DUNE_DORIE_INITIAL_CONDITION_HH

#include <memory>

#include <dune/pdelab/common/function.hh>

namespace Dune{
namespace Dorie{


/*-------------------------------------------------------------------------*//**
 * @brief      Abstract Base Class for initial conditions.
 *
 * @ingroup    InitialConditions
 * @author     Santiago Ospina De Los Ríos
 * @date       2019
 *
 * @tparam     T     BaseTraits
 */
template<class T>
class InitialCondition 
  : public Dune::PDELab::GridFunctionBase<
      Dune::PDELab::GridFunctionTraits<
        typename T::GV,
        typename T::RF,
        T::Scalar::dimension,
        typename T::Scalar>,
      InitialCondition<T>>
{

public:
  using Traits = Dune::PDELab::GridFunctionTraits<
                    typename T::GV,
                    typename T::RF,
                    T::Scalar::dimension,
                    typename T::Scalar>;

  using GV = typename Traits::GridViewType;

  /**
   * @brief      Constructor
   *
   * @param[in]  grid_view  The grid view
   */
  InitialCondition(const GV& grid_view)
    : _gv(grid_view)
  {}

  /*-----------------------------------------------------------------------*//**
   * @brief      Evaluate the initial condition at certain position
   *
   * @param[in]  e  Element enitity
   * @param[in]  x  Position in local element coordinates
   * @param[out] y  Value of model unknown variable at position x
   */
  virtual void evaluate ( const typename Traits::ElementType& e,
                          const typename Traits::DomainType& x,
                                typename Traits::RangeType& y) const = 0;

  //! \brief get a reference to the GridView
  inline const GV& getGridView () const
  {
    return _gv;
  }

  virtual ~InitialCondition() = default;

private:
  const GV _gv;
};

/// A parametric transformation of an initial condition
/** This serves as stub for the initial condition factories. Initial
 *  conditions can typically be stated in several quantities. Classes deriving
 *  from this interface transform initial conditions into the problem state
 *  variable and simultaneously obey the InitialCondition interface.
 *
 *  Derived classes must implement the original InitialCondition::evaluate
 *  function to comply to the interface.
 *
 *  \tparam T BaseTraits
 *  \ingroup InitialConditions
 *  \author Lukas Riedel
 *  \date 2019
 */
template<class T>
class InitialConditionTransformation : public InitialCondition<T>
{
public:
  using Base = InitialCondition<T>;

protected:
  using GV = typename Base::GV;

public:

  /// Construct the transformation from a grid view and an initial condition
  InitialConditionTransformation(const GV& grid_view,
                                 std::shared_ptr<Base> initial_condition):
    Base(grid_view),
    _initial_condition(initial_condition)
  { }


  virtual ~InitialConditionTransformation () override = default;

protected:
  /// The initial condition as stated by the user
  std::shared_ptr<Base> _initial_condition;
};

} // namespace Dorie
} // namespace Dune

#endif // DUNE_DORIE_INITIAL_CONDITION_HH
