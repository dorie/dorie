// -*- tab-width: 4; indent-tabs-mode: nil -*-
#ifndef DUNE_DORIE_INITIAL_CONDITION_H5_HH
#define DUNE_DORIE_INITIAL_CONDITION_H5_HH


#include <string>

#include <dune/common/exceptions.hh>
#include <dune/common/parametertree.hh>

#include <dune/dorie/common/interpolator.hh>
#include <dune/dorie/common/initial_condition/initial_condition.hh>

namespace Dune{
namespace Dorie{

/*-------------------------------------------------------------------------*//**
 * @brief      Class for initial conditions from hdf5 data files.
 *
 * @ingroup    InitialConditions
 * @author     Santiago Ospina De Los Ríos
 * @date       2019
 *
 * @tparam     T     BaseTraits
 */

template<class T>
class InitialConditionH5
  : public InitialCondition<T>
{
  using Base = InitialCondition<T>;
  using RF = typename T::RF;
  using Domain = typename T::Domain;
  using GV = typename T::GV;
  static constexpr int dim = T::dim;

public:

  using Traits = typename Base::Traits;

  InitialConditionH5( const GV& grid_view,
                      const Dune::ParameterTree& config, 
                      std::shared_ptr<spdlog::logger> log)
    : Base(grid_view),
      _interpolator(InterpolatorFactory<T>::create(config.sub("initial"),
                                                   grid_view,
                                                   log))
  { }

  /// Evaluate the initial condition at certain position
  /** \param y 
  * \param x Position in local element coordinates
  * \param e Element enitity
  */
  void evaluate ( const typename Traits::ElementType& e,
                  const typename Traits::DomainType& x,
                        typename Traits::RangeType& y) const override
  {
    auto x_global = e.geometry().global(x);
    y = _interpolator->evaluate(x_global);
  }

private:
  std::shared_ptr<Interpolator<RF, dim>> _interpolator;
};

} // namespace Dorie
} // namespace Dune

#endif // DUNE_DORIE_INITIAL_CONDITION_H5_HH
