#ifndef DUNE_DORIE_NORM_NORMAL_JUMP_SKELETON_HH
#define DUNE_DORIE_NORM_NORMAL_JUMP_SKELETON_HH

namespace Dune {
namespace Dorie {

/**
 * @brief      Norm of the jump in normal direction of every face in the
 *             skeleton.
 *
 * @param[in]  gf         A grid function of a vector space.
 * @param[in]  int_order  The integration order
 *
 * @tparam     GF         Grid function type
 *
 * @return     The \f$L^2\f$ norm of the jumps.
 */
template<class GF>
double norm_normal_jump_skeleton(const GF& gf, int int_order)
{
  using Range = typename GF::Traits::RangeType;
  constexpr int dimDomain = GF::Traits::dimDomain;
  constexpr int dimRange = GF::Traits::dimRange;
  static_assert(dimDomain==dimRange,
    "Function 'norm_normal_jump_skeleton' is only defined for vector fields "
    "with same dimension as the domain");

  auto& gv = gf.getGridView();
  auto& is = gv.indexSet();

  double norm2(0.);

  // Traverse grid view
  for (const auto& element : elements(gv))
  {
    // Traverse intersections
    for(const auto& intersection : intersections(gv,element))
    {
      // Only for skeleton
      if (intersection.neighbor())
      {
        // get entities
        const auto& entity_f = intersection;
        const auto& entity_i = intersection.inside();
        const auto& entity_o = intersection.outside();

        auto id_i = is.index(entity_i);
        auto id_o = is.index(entity_o);

        // Compute face only once
        if (id_o > id_i)
        {
          // Get element geometry
          auto geo_f = entity_f.geometry();
          auto geo_in_i = entity_f.geometryInInside();
          auto geo_in_o = entity_f.geometryInOutside();

          // Loop over quadrature points and integrate normal flux
          for (const auto& it : quadratureRule(geo_f,int_order))
          {
            const auto& position_f = it.position();

            // position of quadrature point in local coordinates of elements
            const auto position_i = geo_in_i.global(position_f);
            const auto position_o = geo_in_o.global(position_f);

            // face normal vector
            const auto normal_f = entity_f.unitOuterNormal(position_f);
            Range v_i, v_o;
            gf.evaluate(entity_i,position_i,v_i);
            gf.evaluate(entity_o,position_o,v_o);

            // integration factor
            auto factor = it.weight() * geo_f.integrationElement(position_f);

            auto jump = normal_f*(v_i-v_o);
            norm2 += jump*jump*factor;
          }
        }
      }
    }
  }
  return sqrt(norm2);
}

} // namespace Dorie
} // namespace Dune

#endif // DUNE_DORIE_NORM_NORMAL_JUMP_SKELETON_HH