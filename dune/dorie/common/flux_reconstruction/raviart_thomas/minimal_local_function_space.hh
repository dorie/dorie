#ifndef DUNE_DORIE_MINIMAL_LOCAL_FUNCTION_SPACE_HH
#define DUNE_DORIE_MINIMAL_LOCAL_FUNCTION_SPACE_HH

#include <dune/localfunctions/common/interfaceswitch.hh>

#include <dune/pdelab/gridfunctionspace/localfunctionspace.hh>

namespace Dune{
namespace Dorie{

/*-------------------------------------------------------------------------*//**
 * @brief      Class for minimal local function spaces.
 * @details    This is a special local function space for the raviart thomas
 *             engine. In particular, it is thought to work with one grid
 *             function space, that is, without composite or vector spaces so
 *             that we avoid anything of the type tree and ordering nightmare.
 *             It is simply not needed here. Indeed we don't even need the dof
 *             indices since the engine does a local solution and there is no
 *             need to map to entities. In any case, the reason of this object
 *             is because the user is expecting a local function space as an
 *             argument of the local operator (even though not too many really
 *             know how a local function space looks like ¬_¬).
 *             
 * @author     Santiago Ospina De Los Ríos
 * @ingroup    FluxReconstruction
 *
 * @tparam     GFS   The GridFunctionSpace.
 * @tparam     TAG   The dune-pdelab space tag.
 */
template <class GFS, class TAG=Dune::PDELab::AnySpaceTag>
class MinimalLocalFunctionSpace
{
  static_assert(not GFS::Traits::isComposite,
    "This local function space does not work with "
    "composited or power grid function spaces");

  //! The dof index type to store indices of each dof.
  using DOFIndex = typename Dune::PDELab::gfs_to_lfs<GFS>::DOFIndex;

public:

  //! Traits following the Dune::PDELab::LocalFunctionSpace structure.
  using Traits = typename Dune::PDELab::LocalFunctionSpace<GFS>::Traits;

  //! Finite element switch following the Dune::FiniteElementInterfaceSwitch 
  //! structure defined by dune-localfunctions.
  using FESwitch = Dune::FiniteElementInterfaceSwitch<
                                typename Traits::FiniteElementType>;

  /*-----------------------------------------------------------------------*//**
   * @brief      Constructs the local function space.
   *
   * @param[in]  gfs   The pointer to a grid function space.
   */
  MinimalLocalFunctionSpace(std::shared_ptr<const GFS> gfs) 
    : pgfs(gfs)
  {}

  /*-----------------------------------------------------------------------*//**
   * @brief      Constructs the local function space.
   *
   * @param[in]  gfs   The grid function space.
   */
  MinimalLocalFunctionSpace(const GFS & gfs) 
    : pgfs(stackobject_to_shared_ptr(gfs)) 
  {}

  /**
   * @brief      Copy constructs the local function space.
   *
   * @param[in]  lfs   The local function space.
   */
  MinimalLocalFunctionSpace(const MinimalLocalFunctionSpace & lfs) {}

  /*-----------------------------------------------------------------------*//**
   * @brief      Maps an index of the shape function to the index into the local
   *             function space.
   * @details    In composite and power grid function spaces is here where the
   *             magic of the typetree happens.
   *
   * @param[in]  index  The index of the shape function.
   *
   * @return     Index representing a degree of freedom of the local function
   *             space.
   */
  typename Traits::IndexContainer::size_type 
  localIndex(typename Traits::IndexContainer::size_type index) const
  {
    return index;
  }

  /*-----------------------------------------------------------------------*//**
   * @brief      Total size of the local function space.
   *
   * @return     The size.
   */
  typename Traits::IndexContainer::size_type size () const
  {
    return n;
  }

  /**
   * @brief      Maps given index in this local function space to its
   *             corresponding global MultiIndex.
   *
   * @param[in]  index  The local index value from the range 0,...,size()-1
   *
   * @return     A const reference to the associated, globally unique
   *             MultiIndex. Note that the returned object may (and must) be
   *             copied if it needs to be stored beyond the time of the next
   *             call to bind() on this LocalFunctionSpace (e.g. when the
   *             MultiIndex is used as a DOF identifier in a constraints
   *             container).
   */
  const typename Traits::DOFIndex& 
  dofIndex(typename Traits::IndexContainer::size_type index) const
  {
    DUNE_THROW(Dune::NotImplemented,
      "This local function space never constructs dof indices");
    return Traits::DOFIndex();
  }

  /*-----------------------------------------------------------------------*//**
   * @brief      Binds a given entity or intersection to the respective finite
   *             element corresponding to the finite element map.
   * @details    This is indeed instantiated as an intersection here. Since we
   *             don't care about the dof intex, a template is is ok to forward
   *             it to the finite element map. In other cases one whould have to
   *             hard code the type.
   *
   * @param      e       The entity or intersection.
   *
   * @tparam     Entity  The entity or intersection type.
   */
  template<class Entity>
  void bind(Entity& e) 
  {
    FESwitch::setStore(pfe, pgfs->finiteElementMap().find(e));
    n = FESwitch::basis(*pfe).size();
  }

  /*-----------------------------------------------------------------------*//**
   * @brief      Get finite element
   *
   * @return     The finite element.
   */
  const typename Traits::FiniteElementType& finiteElement () const
  {
    assert(pfe);
    return *pfe;
  }

private:
  //! Pointer to the grid function space.
  std::shared_ptr<const GFS> pgfs;
  //! Total size of the local function space.
  typename Traits::IndexContainer::size_type n;
  //! Pointer to the binded finite element.
  typename FESwitch::Store pfe;
};

} // namespace Dorie
} // namespace Dune

#endif // DUNE_DORIE_MINIMAL_LOCAL_FUNCTION_SPACE_HH
