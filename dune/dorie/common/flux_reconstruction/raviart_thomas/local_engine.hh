#ifndef DUNE_DORIE_RAVIART_THOMAS_RESIDUALENGINE_HH
#define DUNE_DORIE_RAVIART_THOMAS_RESIDUALENGINE_HH

#include <dune/pdelab/gridfunctionspace/localvector.hh>
#include <dune/pdelab/gridoperator/common/assemblerutilities.hh>
#include <dune/pdelab/gridoperator/common/localassemblerenginebase.hh>
#include <dune/pdelab/constraints/common/constraints.hh>
#include <dune/pdelab/localoperator/callswitch.hh>

#include <dune/common/dynvector.hh>
#include <dune/common/dynmatrix.hh>

#include <dune/dorie/common/flux_reconstruction/raviart_thomas/minimal_local_function_space.hh>

namespace Dune{
namespace Dorie{

/*-------------------------------------------------------------------------*//**
 * @brief      The local assembler engine which assembles the solution of a
 *             raviart thomas flux reconstruction.
 * @details    This assembler construct for each entity a local linear system
 *             where the mass matrix correspond to the integrals of the test
 *             functions of the LocalAssembler against the volume and skeleton
 *             grid function spaces while the b vector corresponds to the
 *             residuals returned by the local operator. Each linear system is
 *             solved when the cell unbinds the local function spaces. Once the
 *             local problem is solved its solution is assigned to the global
 *             solution vector on the ansatz function space. This particular
 *             configuration makes possible to evaluate the raviart thomas
 *             reconstruction into arbitrary order finite elements to local
 *             operators set up properly. Following these, ideas the local
 *             operator must be able to handle volume local functions spaces in
 *             its volume methods and the equivalent for the skeleton methods.
 *
 * @warning    When the order of test function is 0, the residuals are entered
 *             directly to the global solution vector.
 * @warning    This engine always enforce two side visit to the global
 *             assembler.
 *
 * @remark     This assembler was written following the
 *             Dune::PDELab::LocalAssemblerEngine interface.
 *
 * @author     Santiago Ospina De Los Ríos
 * @ingroup    FluxReconstruction
 *
 * @see        Dune::Dorie::RaviartThomasVolumeLocalFiniteElementMap.
 * @see        Dune::Dorie::SkeletonFiniteElementMap.
 * @see        Dune::Dorie::DefaultAssembler.
 * @see        Dune::Dorie::MinimalLocalFunctionSpace.
 *
 * @todo Check parallel setup.
 * @todo Bind the outside entity for skeleton gfsv instead of the inside.
 * @todo Speed-up with GPUs calculating all matrices in the postAssembly step.
 * @todo Check how would this work with constraints.
 *
 * @tparam     LA            The local assembler
 * @tparam     GFSVVolume    Grid function space for the volume part of the
 *                           ansatz space.
 * @tparam     GFSVSkeleton  Grid function space for the skeleton part of the
 *                           ansatz space.
 * @tparam     extended      Decides whether to perform an extended assembling,
 *                           that is, using the volume test function into the
 *                           skeleton and boundary terms (useful for discrete
 *                           gradient lifting).
 */
template<typename LA, typename GFSVVolume, typename GFSVSkeleton, bool extended = false>
class LocalRaviartThomasAssemblerEngine
  : public Dune::PDELab::LocalAssemblerEngineBase
{
public:

  template<typename TrialConstraintsContainer, typename TestConstraintsContainer>
  bool needsConstraintsCaching(const TrialConstraintsContainer& cu, 
                               const TestConstraintsContainer& cv) const
  {
    return false;
  }

  //! The type of the wrapping local assembler
  typedef LA LocalAssembler;

  //! The type of the local operator
  typedef typename LA::LocalOperator LOP;

  //! The type of the prescription vector
  typedef typename LA::Traits::Residual Prescription;
  typedef typename Prescription::ElementType PrescriptionElement;

  //! The type of the solution vector
  typedef typename LA::Traits::Solution Solution;
  typedef typename Solution::ElementType SolutionElement;

  //! The type of the residual vector
  using ResidualElement = SolutionElement;

  //! The type of the mass matrix
  typedef typename LA::Traits::Jacobian::ElementType MassMatrixElement;

  //! The local function spaces
  typedef typename LA::LFSU LFSU;
  typedef typename LA::LFSUCache LFSUCache;
  typedef typename LFSU::Traits::GridFunctionSpace GFSU;
  typedef typename LA::LFSV LFSW;
  typedef typename LA::LFSVCache LFSWCache;
  typedef typename LFSW::Traits::GridFunctionSpace GFSW;

  typedef typename Prescription::template ConstLocalView<LFSWCache> PrescriptionView;
  typedef typename Solution::template LocalView<LFSUCache> SolutionView;

  /*-----------------------------------------------------------------------*//**
   * @brief      Constructor
   *
   * @param[in]  local_assembler_  The local assembler object which creates this
   *                               engine
   * @param[in]  gfsv_volume_      The ansatz grid function spaces for volume.
   * @param[in]  gfsv_skeleton_    The ansatz grid function spaces for skeleton.
   * @param[in]  int_order_add_    Integration integer to add to the quadrature
   *                               rule.
   */
  LocalRaviartThomasAssemblerEngine(
    const LocalAssembler & local_assembler_,
    const GFSVVolume & gfsv_volume_,
    const GFSVSkeleton & gfsv_skeleton_,
    int int_order_add_ = 2,
    int quadrature_factor_ = 2
  )
    : local_assembler(local_assembler_),
      lop(local_assembler_.localOperator()),
      rl_view(rl,1.0),
      rn_view(rn,1.0),
      pgfsv_volume(stackobject_to_shared_ptr(gfsv_volume_)),
      pgfsv_skeleton(stackobject_to_shared_ptr(gfsv_skeleton_)),
      lfsv_volume(*pgfsv_volume),
      lfsv_skeleton(*pgfsv_skeleton),
      lfsv_skeleton_n(pgfsv_skeleton),
      int_order_add(int_order_add_),
      quadrature_factor(quadrature_factor_)
  {}

  /*-----------------------------------------------------------------------*//**
   * @brief      Constructor
   *
   * @param[in]  local_assembler_  The local assembler object which creates this
   *                               engine
   * @param[in]  gfsv_volume_      The ansatz grid function spaces for volume.
   * @param[in]  gfsv_skeleton_    The ansatz grid function spaces for skeleton.
   * @param[in]  int_order_add_    Integration integer to add to the quadrature
   *                               rule.
   */
  LocalRaviartThomasAssemblerEngine(
    const LocalAssembler & local_assembler_,
    const std::shared_ptr<const GFSVVolume> & gfsv_volume_,
    const std::shared_ptr<const GFSVSkeleton> & gfsv_skeleton_,
    int int_order_add_ = 2,
    int quadrature_factor_ = 2
  )
    : local_assembler(local_assembler_),
      lop(local_assembler_.localOperator()),
      rl_view(rl,1.0),
      rn_view(rn,1.0),
      lfsv_volume(*pgfsv_volume),
      pgfsv_volume(gfsv_volume_),
      pgfsv_skeleton(gfsv_skeleton_),
      lfsv_skeleton(*pgfsv_skeleton),
      lfsv_skeleton_n(pgfsv_skeleton),
      int_order_add(int_order_add_),
      quadrature_factor(quadrature_factor_)
  {}

  //! @name Query methods for the global grid assembler
  //! @{
  bool requireSkeleton() const
  { return ( local_assembler.doAlphaSkeleton() || local_assembler.doLambdaSkeleton() ); }
  bool requireSkeletonTwoSided() const
  { return true; }
  bool requireUVVolume() const
  { return local_assembler.doAlphaVolume(); }
  bool requireVVolume() const
  { return local_assembler.doLambdaVolume(); }
  bool requireUVSkeleton() const
  { return local_assembler.doAlphaSkeleton(); }
  bool requireVSkeleton() const
  { return local_assembler.doLambdaSkeleton(); }
  bool requireUVBoundary() const
  { return local_assembler.doAlphaBoundary(); }
  bool requireVBoundary() const
  { return local_assembler.doLambdaBoundary(); }
  bool requireUVVolumePostSkeleton() const
  { return local_assembler.doAlphaVolumePostSkeleton(); }
  bool requireVVolumePostSkeleton() const
  { return local_assembler.doLambdaVolumePostSkeleton(); }
  //! @}

  //! Public access to the wrapping local assembler
  const LocalAssembler & localAssembler() const
  {
    return local_assembler;
  }

  //! Trial space constraints
  const typename LocalAssembler::Traits::TrialGridFunctionSpaceConstraints& trialConstraints() const
  {
    return localAssembler().trialConstraints();
  }

  //! Test space constraints
  const typename LocalAssembler::Traits::TestGridFunctionSpaceConstraints& testConstraints() const
  {
    return localAssembler().testConstraints();
  }

  //! Set current prescription vector. Should be called prior to assembling.
  void setPrescription(const Prescription & prescription_)
  {
    global_pl_view.attach(prescription_);
    global_pn_view.attach(prescription_);
  }

  //! Set current solution vector. Should be called prior to assembling.
  void setSolution(Solution & solution_)
  {
    global_sl_view.attach(solution_);
    global_sn_view.attach(solution_);
  }

  //! @name Binding
  //! Called immediately after binding of local function space in global 
  //! assembler.
  //! @{
  template<typename EG, typename LFSUC, typename LFSWC>
  void onBindLFSUV(const EG & eg, const LFSUC & lfsu_cache, const LFSWC & lfsw_cache)
  {
    global_sl_view.bind(lfsu_cache);
    xl.resize(lfsu_cache.size());

    // local system to be solved on each element. Matrix has block form:
    // A_{eleme,eleme} A_{eleme,face1} A_{eleme,face2} ... A_{eleme,facen}
    // A_{face1,eleme} A_{face1,face1} A_{face1,face2} ... A_{face1,facen}
    // :                                                   :              
    // A_{facen,eleme} A_{facen,face1} A_{facen,face2} ... A_{facen,facen}
  
    r_vec = LocalResidualVector(lfsu_cache.size(),0.0);
    m_matrix = LocalMassMatrix(lfsu_cache.size(),lfsu_cache.size(),0.0);
  }

  template<typename EG, typename LFSWC>
  void onBindLFSV(const EG & eg, const LFSWC & lfsw_cache)
  {
    lfsv_volume.bind( eg.entity() );

    global_pl_view.bind(lfsw_cache);
    pl.resize(lfsw_cache.size());

    rl.assign(lfsv_volume.size(),0.0);
  }

  template<typename IG, typename LFSUC, typename LFSWC>
  void onBindLFSUVInside(const IG & ig, const LFSUC & lfsu_cache, const LFSWC & lfsw_cache)
  {
    lfsv_skeleton.bind( ig.intersection() );
  }

  template<typename IG, typename LFSUC, typename LFSWC>
  void onBindLFSUVOutside(const IG & ig,
                          const LFSUC & lfsu_s_cache, const LFSWC & lfsw_s_cache,
                          const LFSUC & lfsu_n_cache, const LFSWC & lfsw_n_cache)
  {
    lfsv_skeleton_n.bind( ig.intersection() ); // FIXME!! switch inside/outside

    // Check that local matrix will be conforming
    assert(lfsu_s_cache.size() == 
      (lfsv_volume.size() + lfsv_skeleton.size() * ig.inside().subEntities(1)));
    assert(lfsv_skeleton.size() == lfsv_skeleton_n.size());

    global_sn_view.bind(lfsu_n_cache);
    xn.assign(lfsu_n_cache.size(),0.0);

    global_pn_view.bind(lfsw_n_cache);
    pn.resize(lfsw_n_cache.size());
  }

  template<typename IG, typename LFSWC>
  void onBindLFSVInside(const IG & ig, const LFSWC & lfsw_cache)
  {
    rl.assign(lfsw_cache.size(),0.0);
  }

  template<typename IG, typename LFSWC>
  void onBindLFSVOutside(const IG & ig,
                         const LFSWC & lfsw_s_cache,
                         const LFSWC & lfsw_n_cache)
  {
    rn.assign(lfsw_n_cache.size(),0.0);
  }

  template<typename EG, typename LFSUC, typename LFSVC>
  void onUnbindLFSUV(const EG & eg, 
                     const LFSUC & lfsu_cache, 
                     const LFSVC & lfsv_cache)
  {
    auto& lfsu = lfsu_cache.localFunctionSpace();

    // get polynomial degree
    const int order  = lfsu.finiteElement().localBasis().order();
    
    // solution of the local problem.
    if (order > 1)
    {
      x_vec.resize(lfsu_cache.size(),0.0);
      m_matrix.solve(x_vec,r_vec);

      for (unsigned int i = 0; i < lfsu.size(); ++i)
        xl(lfsu,i) = x_vec[i];
      global_sl_view.write(xl);
    } else {
      for (unsigned int i = 0; i < lfsu.size(); ++i)
        xl(lfsu,i) = r_vec[i];
      global_sl_view.write(xl);
    }
  }
  //! @}

  //! @name Methods for loading of the local function's coefficients
  //! @{
  template<typename LFSUC>
  void loadCoefficientsLFSUInside(const LFSUC & lfsu_s_cache)
  {
    global_pl_view.read(pl);
  }
  template<typename LFSUC>
  void loadCoefficientsLFSUOutside(const LFSUC & lfsu_n_cache)
  {
    global_pn_view.read(pn);
  }
  template<typename LFSUC>
  void loadCoefficientsLFSUCoupling(const LFSUC & lfsu_c_cache)
  {
    DUNE_THROW(Dune::NotImplemented,"No coupling lfsu available for ");
  }
  //! @}

  //! @name Notifier functions, called immediately before and after assembling
  //! @{

  void preAssembly()
  {
    non_conforming_element_warning = false;
  }

  void postAssembly(const GFSU& gfsu, const GFSW& gfsw)
  {
    if(local_assembler.doPostProcessing())
      Dune::PDELab::constrain_residual(local_assembler.testConstraints()
                                      ,global_sl_view.container());

    if (non_conforming_element_warning)
      std::cout << "WARNING: Flux reconstruction for non conforming entities "
                << "produce non-continuos fluxes in normal direction of the "
                << "entity!" 
                << std::endl; // TODO Change to logger

  }

  //! @}

  //! @name Assembling methods
  //! @{

  /** Assemble on a given cell without function spaces.

      \return If true, the assembling for this cell is assumed to
      be complete and the assembler continues with the next grid
      cell.
   */
  template<typename EG>
  bool assembleCell(const EG & eg)
  {
    return LocalAssembler::isNonOverlapping && eg.entity().partitionType() != Dune::InteriorEntity;
  }

  template<typename EG, typename LFSUC, typename LFSWC>
  void assembleUVVolume(const EG & eg, const LFSUC & lfsu_cache, const LFSWC & lfsw_cache)
  {
    auto& lfsw = lfsw_cache.localFunctionSpace();

    rl_view.setWeight(local_assembler.weight());

    // Under certain circumstances volume element has no DOFs (e.g. finite volumes)
    if (lfsv_volume.size() > 0)
      Dune::PDELab::LocalAssemblerCallSwitch<LOP,LOP::doAlphaVolume>::
        alpha_volume(lop,eg,lfsw,pl,lfsv_volume,rl_view);

    // Load residual view into the local residual vector
    for (unsigned int i = 0; i < lfsv_volume.size(); ++i)
      r_vec[i] += rl(lfsv_volume,i);

    auto& lfsu = lfsu_cache.localFunctionSpace();

    // get polynomial degree
    const int order = lfsu.finiteElement().localBasis().order();
    
    // Assemble mass matrix
    if (order > 1)
    {
      using LFSU = typename LFSUC::LocalFunctionSpace;
      using FESwitchTrial = Dune::FiniteElementInterfaceSwitch<typename LFSU::Traits::FiniteElementType>;
      using BasisSwitchTrial = Dune::BasisInterfaceSwitch<typename FESwitchTrial::Basis>;
      using LFSURange = typename BasisSwitchTrial::Range;

      using FESwitchTest = Dune::FiniteElementInterfaceSwitch<typename LFSVVolume::Traits::FiniteElementType>;
      using BasisSwitchTest = Dune::BasisInterfaceSwitch<typename FESwitchTest::Basis>;
      using LFSVVolumeRange = typename BasisSwitchTest::Range;

      auto gt = eg.geometry();

      const int intorder = int_order_add+quadrature_factor*order;

      for (const auto& it : quadratureRule(gt,intorder))
      {
       // evaluate position in element local and global coordinates
        const auto p_local = it.position();

        auto BTransposed = gt.jacobianTransposed(p_local);
        auto detB = BTransposed.determinant();

        std::vector<LFSURange> phiu(lfsu.size());
        std::vector<LFSVVolumeRange> phiv(lfsv_volume.size());

        // evaluate basis functions
        lfsu.finiteElement().localBasis().evaluateFunction(p_local,phiu);
        lfsv_volume.finiteElement().localBasis().evaluateFunction(p_local,phiv);

        for (unsigned int i=0; i<lfsu.size(); i++)
        { // Piola transformation without 1/det
          LFSURange y(phiu[i]);
          BTransposed.mtv(y,phiu[i]);
        }

        for (unsigned int i=0; i<lfsv_volume.size(); i++)
        { // Piola transformation without 1/det
          LFSVVolumeRange y(phiv[i]);
          BTransposed.mtv(y,phiv[i]);
        }

        // integration factor
        auto factor = it.weight() * gt.integrationElement(p_local);

        for (unsigned int i = 0; i<lfsv_volume.size(); i++)
          for (unsigned int j = 0; j<lfsu.size(); j++)
            m_matrix[i][j] += (phiu[j]*phiv[i])/detB * factor/detB;
      }
    }
  }

  template<typename EG, typename LFSWC>
  void assembleVVolume(const EG & eg, const LFSWC & lfsw_cache)
  {}

  template<typename IG, typename LFSUC, typename LFSWC>
  void assembleUVSkeleton(const IG & ig, const LFSUC & lfsu_s_cache, const LFSWC & lfsw_s_cache,
                          const LFSUC & lfsu_n_cache, const LFSWC & lfsw_n_cache)
  {
    if (not ig.intersection().conforming())
      non_conforming_element_warning = true;

    rl_view.setWeight(local_assembler.weight());
    rn_view.setWeight(local_assembler.weight());
    Dune::PDELab::LocalAssemblerCallSwitch<LOP,LOP::doAlphaSkeleton>::
      alpha_skeleton(lop,ig,
                     lfsw_s_cache.localFunctionSpace(),pl,lfsv_skeleton,
                     lfsw_n_cache.localFunctionSpace(),pn,lfsv_skeleton_n,
                     rl_view,rn_view);

    unsigned int offset = lfsv_volume.size() + lfsv_skeleton.size()*ig.intersection().indexInInside();

    // Load residual view into the local residual vector
    for (unsigned int i = 0; i < lfsv_skeleton.size(); ++i){
      r_vec[i+offset] += rl(lfsv_skeleton,i);
    }

    if constexpr (extended) {
      rl.assign(lfsw_s_cache.size(),0.0);

      Dune::PDELab::LocalAssemblerCallSwitch<LOP,LOP::doAlphaSkeleton>::
        alpha_skeleton(lop,ig,
                       lfsw_s_cache.localFunctionSpace(),pl,lfsv_volume,
                       lfsw_n_cache.localFunctionSpace(),pn,lfsv_volume, //_n, //TODO add volume outside
                       rl_view,rn_view);

      // Load residual view into the local residual vector
      for (unsigned int i = 0; i < lfsv_volume.size(); ++i){
        r_vec[i] += rl(lfsv_volume,i);
      }
    }

    auto& lfsu_s = lfsu_s_cache.localFunctionSpace();

    // get polynomial degree
    const int order_s = lfsu_s.finiteElement().localBasis().order();
    
    if (order_s > 0)
    {
      using LFSU = typename LFSUC::LocalFunctionSpace;
      using FESwitchTrial = Dune::FiniteElementInterfaceSwitch<typename LFSU::Traits::FiniteElementType>;
      using BasisSwitchTrial = Dune::BasisInterfaceSwitch<typename FESwitchTrial::Basis>;
      using LFSURange = typename BasisSwitchTrial::Range;

      using FESwitchTest = Dune::FiniteElementInterfaceSwitch<typename LFSVSkeleton::Traits::FiniteElementType>;
      using BasisSwitchTest = Dune::BasisInterfaceSwitch<typename FESwitchTest::Basis>;
      using LFSVSkeletonRange = typename BasisSwitchTest::Range;

      const int intorder = int_order_add + quadrature_factor * order_s;

      auto gtface = ig.geometry();

      for (const auto& it : quadratureRule(gtface,intorder))
      {
        // position of quadrature point in local coordinates of elements
        const auto p_local_s = ig.geometryInInside().global(it.position());

        using LocalCoordinate = typename IG::LocalGeometry::LocalCoordinate;
        LocalCoordinate sub_p_local_s, sub_p_local_n;

        if (ig.intersection().conforming()) {
          sub_p_local_s = it.position();
          sub_p_local_n = it.position();
        } else {
          auto sub_entity_s_id = ig.intersection().indexInInside();
          auto sub_entity_s = ig.inside().template subEntity<1>(sub_entity_s_id);
          sub_p_local_s = sub_entity_s.geometry().local(gtface.global(it.position()));
        }

        Dune::FieldVector<SolutionElement,IG::Entity::mydimension> y;
        auto n_F = ig.unitOuterNormal(it.position());
        auto BTransposed = ig.inside().geometry().jacobianTransposed(p_local_s);
        auto detB = BTransposed.determinant();
        BTransposed.mv(n_F,y);

        std::vector<LFSURange> phiu_s(lfsu_s.size());
        std::vector<LFSVSkeletonRange> phiv_s(lfsv_skeleton.size());

        // evaluate basis functions
        lfsu_s.finiteElement().localBasis().evaluateFunction(p_local_s,phiu_s);
        lfsv_skeleton.finiteElement().localBasis().evaluateFunction(sub_p_local_s,phiv_s);

        // integration factor
        const auto factor = it.weight() * ig.geometry().integrationElement(it.position());

        for (unsigned int i = 0; i<lfsv_skeleton.size(); i++)
          for (unsigned int j = 0; j<lfsu_s.size(); j++){
            m_matrix[i+offset][j] += y*phiu_s[j]/detB * phiv_s[i] * factor;
          }
      }
    }
  }

  template<typename IG, typename LFSWC>
  void assembleVSkeleton(const IG & ig, const LFSWC & lfsw_s_cache, const LFSWC & lfsw_n_cache)
  {}

  template<typename IG, typename LFSUC, typename LFSWC>
  void assembleUVBoundary(const IG & ig, const LFSUC & lfsu_s_cache, const LFSWC & lfsw_s_cache)
  {
    rl_view.setWeight(local_assembler.weight());
    Dune::PDELab::LocalAssemblerCallSwitch<LOP,LOP::doAlphaBoundary>::
      alpha_boundary(lop,ig,lfsw_s_cache.localFunctionSpace(),pl,lfsv_skeleton,rl_view);

    unsigned int offset = lfsv_volume.size() + lfsv_skeleton.size()*ig.intersection().indexInInside();
    // Load residual view into the local residual vector
    for (unsigned int i = 0; i < lfsv_skeleton.size(); ++i){
      r_vec[i+offset] += rl(lfsv_skeleton,i);
    }

    if constexpr (extended) {
      rl.assign(lfsw_s_cache.size(),0.0);
      Dune::PDELab::LocalAssemblerCallSwitch<LOP,LOP::doAlphaBoundary>::
        alpha_boundary(lop,ig,lfsw_s_cache.localFunctionSpace(),pl,lfsv_volume,rl_view);

      // Load residual view into the local residual vector
      for (unsigned int i = 0; i < lfsv_volume.size(); ++i){
        r_vec[i] += rl(lfsv_volume,i);
      }
    }

    auto& lfsu_s = lfsu_s_cache.localFunctionSpace();

    // get polynomial degree
    const int order_s = lfsu_s.finiteElement().localBasis().order();

    if (order_s > 0)
    {
      using LFSU = typename LFSUC::LocalFunctionSpace;
      using FESwitchTrial = Dune::FiniteElementInterfaceSwitch<typename LFSU::Traits::FiniteElementType>;
      using BasisSwitchTrial = Dune::BasisInterfaceSwitch<typename FESwitchTrial::Basis>;
      using LFSURange = typename BasisSwitchTrial::Range;

      using FESwitchTest = Dune::FiniteElementInterfaceSwitch<typename LFSVSkeleton::Traits::FiniteElementType>;
      using BasisSwitchTest = Dune::BasisInterfaceSwitch<typename FESwitchTest::Basis>;
      using LFSVSkeletonRange = typename BasisSwitchTest::Range;

      // Calculate the mass matrix
      const int intorder = int_order_add + quadrature_factor * order_s;

      auto gtface = ig.geometry();

      for (const auto& it : quadratureRule(gtface,intorder))
      {
        // position of quadrature point in local coordinates of elements
        const auto p_local_s = ig.geometryInInside().global(it.position());

        Dune::FieldVector<SolutionElement,IG::Entity::mydimension> y;
        auto n_F = ig.unitOuterNormal(it.position());
        auto BTransposed = ig.inside().geometry().jacobianTransposed(p_local_s);
        auto detB = BTransposed.determinant();
        BTransposed.mv(n_F,y);

        std::vector<LFSURange> phiu_s(lfsu_s.size());
        std::vector<LFSVSkeletonRange> phiv_s(lfsv_skeleton.size());

        // evaluate basis functions
        lfsu_s.finiteElement().localBasis().evaluateFunction(p_local_s,phiu_s);
        lfsv_skeleton.finiteElement().localBasis().evaluateFunction(it.position(),phiv_s);

        // integration factor
        const auto factor = it.weight() * ig.geometry().integrationElement(it.position());

        for (unsigned int i = 0; i<lfsv_skeleton.size(); i++)
          for (unsigned int j = 0; j<lfsu_s.size(); j++)
            m_matrix[i+offset][j] += (y*phiu_s[j])/detB * phiv_s[i] * factor;
      }
    }
  }

  template<typename IG, typename LFSWC>
  void assembleVBoundary(const IG & ig, const LFSWC & lfsw_s_cache)
  {}

  template<typename IG, typename LFSUC, typename LFSWC>
  static void assembleUVEnrichedCoupling(const IG & ig,
                                         const LFSUC & lfsu_s_cache, const LFSWC & lfsw_s_cache,
                                         const LFSUC & lfsu_n_cache, const LFSWC & lfsw_n_cache,
                                         const LFSUC & lfsu_coupling_cache, const LFSWC & lfsw_coupling_cache)
  {
    DUNE_THROW(Dune::NotImplemented,"Assembling of coupling spaces is not implemented for ");
  }

  template<typename IG, typename LFSWC>
  static void assembleVEnrichedCoupling(const IG & ig,
                                        const LFSWC & lfsw_s_cache,
                                        const LFSWC & lfsw_n_cache,
                                        const LFSWC & lfsw_coupling_cache)
  {
    DUNE_THROW(Dune::NotImplemented,"Assembling of coupling spaces is not implemented for ");
  }

  template<typename EG, typename LFSUC, typename LFSWC>
  void assembleUVVolumePostSkeleton(const EG & eg, const LFSUC & lfsu_cache, const LFSWC & lfsw_cache)
  {
    rl_view.setWeight(local_assembler.weight());
    Dune::PDELab::LocalAssemblerCallSwitch<LOP,LOP::doAlphaVolumePostSkeleton>::
      alpha_volume_post_skeleton(lop,eg,lfsw_cache.localFunctionSpace(),pl,lfsv_volume,rl_view);
  }

  template<typename EG, typename LFSWC>
  void assembleVVolumePostSkeleton(const EG & eg, const LFSWC & lfsw_cache)
  {}

  //! @}

private:
  //! Reference to the wrapping local assembler object which
  //! constructed this engine
  const LocalAssembler & local_assembler;

  //! Reference to the local operator
  const LOP & lop;

  //! Pointer to the current prescription vector in which to assemble
  PrescriptionView global_pl_view;
  PrescriptionView global_pn_view;

  //! Pointer to the current residual vector in which to assemble
  SolutionView global_sl_view;
  SolutionView global_sn_view;

  //! @name The local vectors and matrices as required for assembling
  //! @{
  typedef Dune::PDELab::TrialSpaceTag LocalTrialSpaceTag;
  typedef Dune::PDELab::TestSpaceTag LocalTestSpaceTag;

  typedef Dune::PDELab::LocalVector<SolutionElement, LocalTrialSpaceTag> SolutionVector;
  typedef Dune::PDELab::LocalVector<ResidualElement, LocalTestSpaceTag> ResidualVector;
  typedef Dune::PDELab::LocalVector<PrescriptionElement, LocalTestSpaceTag> PrescriptionVector;

  //! Inside local coefficients
  SolutionVector xl;
  //! Outside local coefficients
  SolutionVector xn;
  //! Inside local residual
  ResidualVector rl;
  //! Outside local residual
  ResidualVector rn;
  //! Inside local prescription
  PrescriptionVector pl;
  //! Outside local prescription
  PrescriptionVector pn;
  //! Inside local residual weighted view
  typename ResidualVector::WeightedAccumulationView rl_view;
  //! Outside local residual weighted view
  typename ResidualVector::WeightedAccumulationView rn_view;
  //! @}

  const std::shared_ptr<const GFSVVolume>   pgfsv_volume;
  const std::shared_ptr<const GFSVSkeleton> pgfsv_skeleton;

  /* local function spaces */
  typedef Dune::Dorie::MinimalLocalFunctionSpace<GFSVVolume, LocalTestSpaceTag> LFSVVolume;
  typedef Dune::Dorie::MinimalLocalFunctionSpace<GFSVSkeleton, LocalTestSpaceTag> LFSVSkeleton;

  // local function spaces in local cell
  LFSVVolume lfsv_volume;
  LFSVSkeleton lfsv_skeleton;
  // local function spaces in neighbor
  LFSVSkeleton lfsv_skeleton_n;

  typedef Dune::DynamicVector<ResidualElement> LocalResidualVector;
  typedef Dune::DynamicVector<ResidualElement> LocalSolutionVector;
  typedef Dune::DynamicMatrix<ResidualElement> LocalMassMatrix;

  LocalResidualVector r_vec;
  LocalSolutionVector x_vec;
  LocalMassMatrix     m_matrix;

  bool non_conforming_element_warning;
  int int_order_add;
  int quadrature_factor;
};

} // namespace Dorie
} // namespace Dune

#endif // DUNE_DORIE_RAVIART_THOMAS_RESIDUALENGINE_HH
