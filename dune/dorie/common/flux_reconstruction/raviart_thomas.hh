#ifndef DUNE_DORIE_RAVIART_THOMAS_PROJECTION_HH
#define DUNE_DORIE_RAVIART_THOMAS_PROJECTION_HH

/*-------------------------------------------------------------------------*//**
 * @brief      Helper for the simplex raviart thomas flux reconstruction.
 * @details    This helper returns a bool that says whether the flux
 *             reconstruction method  in simplices is available for a given
 *             dimension and order.
 *
 * @tparam     dim    Grid dimension (2 or 3)
 * @tparam     order  Order of the flux reconstruction.
 */
#define DORIE_SUPPORT_RT_SIMPLEX(dim, order) (order <= 2)

/*-------------------------------------------------------------------------*//**
 * @brief      Helper for the cube raviart thomas flux reconstruction.
 * @details    This helper returns a bool that says whether the flux
 *             reconstruction method in cubes is available for a given
 *             dimension and order.
 *
 * @tparam     dim    Grid dimension (2 or 3)
 * @tparam     order  Order of the flux reconstruction.
 */
#define DORIE_SUPPORT_RT_CUBE(dim, order) (dim==2) ? (order <= 2) : (order <= 1)

#include <vector>
#include <string>

#include <dune/common/exceptions.hh>
#include <dune/common/parametertree.hh>

#include <dune/pdelab/backend/istl.hh>
#include <dune/pdelab/gridoperator/gridoperator.hh>

#include <dune/dorie/common/logging.hh>
#include <dune/dorie/common/utility.hh>

#include <dune/dorie/common/flux_reconstruction/raviart_thomas/assembler.hh>
#include <dune/dorie/common/flux_reconstruction/raviart_thomas/local_engine.hh>

#include <dune/dorie/common/finite_element/skeleton_fem.hh>
#include <dune/dorie/common/finite_element/raviart_thomas_volume_fem.hh>
#include <dune/dorie/common/finite_element/raviart_thomas_fem.hh>

#include <dune/dorie/common/flux_reconstruction/check_normal_jump_skeleton.hh>

namespace Dune{
namespace Dorie{

/*-------------------------------------------------------------------------*//**
 * @brief      Helper for the raviart thomas flux reconstruction.
 * @details    This helper exports a bool that says whether the flux
 *             reconstruction method is available for the combination of the
 *             used template parameters.
 *
 * @tparam     dim    Grid dimension
 * @tparam     order  Order of the flux reconstruction.
 * @tparam     gt     Geometry type.
 */
template<int dim, unsigned int order, Dune::GeometryType::BasicType gt>
struct RaviartThomasFluxReconstructionHelper
{
private:
    static_assert(dim==2||dim==3);
    static_assert(gt==GeometryType::simplex||gt==GeometryType::cube);
    // hard-coded bool helpers
    static constexpr bool val_simplex = DORIE_SUPPORT_RT_SIMPLEX(dim, order);
    static constexpr bool val_cube = DORIE_SUPPORT_RT_CUBE(dim, order);
public:
    static constexpr bool enable = (gt==GeometryType::simplex) ? val_simplex : val_cube;
};

/// Shortcut helper for querying if flux reconstruction is available
/** \see RaviartThomasFluxReconstructionHelper
 */
template<int dim, unsigned int rt_order, Dune::GeometryType::BasicType gt>
constexpr bool supports_flux_reconstr_v
    = RaviartThomasFluxReconstructionHelper<dim, rt_order, gt>::enable;

/*-------------------------------------------------------------------------*//**
 * @brief      Raviart Thomas flux reconstruction for discontinuous Galerkin
 *             grid operators.
 * @details    The local operator used in the grid operator must be able to
 *             handle non-conforming finite elements see @ref FluxReconstruction
 *             for details. Checks for conformity of fluxes at intersections is
 *             available automatically on each update.
 *
 * @author     Santiago Ospina De Los Ríos
 * @date       2018
 * @ingroup    FluxReconstruction
 * @ingroup    Collective
 *
 * @tparam     GO     The GridOperator type.
 * @tparam     gt     Geometry type of the grid.
 * @tparam     order  Order of the Raviart Thomas reconstruction.
 */
template<class GV, class RF, unsigned int order, Dune::GeometryType::BasicType gt>
class RaviartThomasFluxReconstruction 
  : public Dune::PDELab::GridFunctionBase<
        Dune::PDELab::GridFunctionTraits< GV,RF,GV::dimension,
                                          Dune::FieldVector<RF,GV::dimension>>,
        RaviartThomasFluxReconstruction<GV,RF,order,gt>>
{
public:
  using Traits = Dune::PDELab::GridFunctionTraits<GV,RF,GV::dimension,
                                    Dune::FieldVector<RF,GV::dimension>>;

private:
    using DF = typename Traits::DomainFieldType;

    enum { dim = GV::dimension };

    using FEMU = Dune::Dorie::
          RaviartThomasLocalFiniteElementMap<GV,DF,RF,order,gt>;
    using ES = Dune::PDELab::OverlappingEntitySet<GV>;
    using GFSU = Dune::PDELab::GridFunctionSpace<ES,FEMU>;
    using X = Dune::PDELab::Backend::Vector<GFSU, RF>;
    using GFDGPiola = Dune::PDELab::DiscreteGridFunctionPiola<GFSU,X>;

    using VolumeFEM = Dune::Dorie::
          RaviartThomasVolumeLocalFiniteElementMap<DF,RF,order,dim,gt>;
    using GFSVVolume = Dune::PDELab::GridFunctionSpace<ES,VolumeFEM>;
    using SkeletonFEM = typename Dune::Dorie::
          SkeletonFiniteElementMap<DF,RF,dim,order,gt>;
    using GFSVSkeleton = Dune::PDELab::GridFunctionSpace<ES,SkeletonFEM>;

public:

  /*-----------------------------------------------------------------------*//**
   * @brief      Constructor of the class
   *
   * @param[in]  log                The log
   * @param[in]  gv                 Grid view
   * @param[in]  int_order_add      Integration integer to add to the quadrature
   *                                rule order.
   * @param[in]  quadrature_factor  Factor to multiply the current order for the
   *                                quadrature rule
   * @param[in]  check_jumps_lvl    Severity of the error produced by failed to
   *                                check jumps: none, warn, error.
   * @param[in]  check_tol          The check tolerance
   */
  RaviartThomasFluxReconstruction(
      std::shared_ptr<spdlog::logger> log,
      const GV& gv, 
      int int_order_add = 2, 
      int quadrature_factor = 2,
      std::string check_jumps_lvl = "warn",
      RF check_tol = 1E-10
    ) : _log(log)
      , _gv(gv)
      , _femu(_gv)
      , _int_order_add(int_order_add)
      , _quadrature_factor(quadrature_factor)
      , _check_lvl(to_check_lvl(check_jumps_lvl))
      , _check_tol(check_tol)
  {}

  /*-----------------------------------------------------------------------*//**
   * @brief      Constructor of the class
   *
   * @param[in]  log     The log
   * @param[in]  gv      Grid view
   * @param[in]  config  The configuration file with flux reconstruction
   *                     keywords.
   */
  RaviartThomasFluxReconstruction(
      std::shared_ptr<spdlog::logger> log,
      const GV& gv, 
      const Dune::ParameterTree& config
    ) : _log(log)
      , _gv(gv)
      , _femu(_gv)
      , _int_order_add(2)
      , _quadrature_factor(2)
      , _check_lvl(to_check_lvl(config.get<std::string>("checkJumps")))
      , _check_tol(config.get<RF>("checkTol"))
  {}

  /*-----------------------------------------------------------------------*//**
   * @brief      Updates the RT elements w.r.t the prescription
   * @details    Once updated, this class is independent of the prescription
   *             space and the local operator.
   *
   * @param[in]  p               Prescription vector
   * @param[in]  gfsw            Prescription grid function space
   * @param      local_operator  The local operator able to support RT elements
   *
   * @tparam     Domain          DOF Vector type
   * @tparam     GFSW            Grid function space type
   * @tparam     LOP             Local operator type
   */
  template<class Domain, class GFSW, class LOP>
  void update (const Domain& p, const GFSW& gfsw, const LOP& local_operator)
  {
    _log->trace("Computing flux reconstruction with RT{} elements", order);

    // get entity set from grid operator
    const ES& entity_set = gfsw.entitySet();

    // create the Raviar Thomas grid function space
    _gfsu = std::make_shared<GFSU>(entity_set,_femu);

    // create the DOF vector
    _x = std::make_shared<X>(*_gfsu,0.0);
    
    // create the grid function adapter for RT
    _dgfp = std::make_shared<GFDGPiola>(*_gfsu,*_x);

    // Construct finite element maps
    VolumeFEM volume_fem;
    GFSVVolume gfsv_volume(entity_set,volume_fem);

    SkeletonFEM skeleton_fem;
    GFSVSkeleton gfsv_skeleton(entity_set,skeleton_fem);

    using MBE = Dune::PDELab::ISTL::BCRSMatrixBackend<>;
    using GOP = Dune::PDELab::GridOperator<GFSU,GFSW,LOP,MBE,DF,RF,RF>;

    // cast LOP to a non-const reference (this GOP doesn't modify it)
    LOP& _casted_local_operator = const_cast<LOP&>(local_operator);

    // Prepare grid operator with local assembler and local operator
    MBE mbe(0);
    GOP gop(*_gfsu,gfsw,_casted_local_operator,mbe);

    // Create global assembler
    // using Assember = Dune::PDELab::Assembler<...>;
    using Assember = Dune::Dorie::DefaultAssembler<GFSU,GFSW,
                      Dune::PDELab::EmptyTransformation,
                      Dune::PDELab::EmptyTransformation>;

    Assember global_assembler_gop(*_gfsu,gfsw);

    // Create local assembler
    using LA = typename GOP::LocalAssembler;
    const LA& local_assembler_gop = gop.localAssembler();

    // Construct RT engine
    using LocalRaviartThomasEngine = Dune::Dorie::
    LocalRaviartThomasAssemblerEngine<LA,GFSVVolume,GFSVSkeleton,true>;
    
    LocalRaviartThomasEngine 
      local_raviart_thomas_engine(
        local_assembler_gop,
        gfsv_volume,gfsv_skeleton,
        _int_order_add,
        _quadrature_factor);

    // Set prescription and solution vectors degrees of freedom
    local_raviart_thomas_engine.setPrescription(p);
    local_raviart_thomas_engine.setSolution(*_x);

    // Assemble and solve local problems into solution vector
    global_assembler_gop.assemble(local_raviart_thomas_engine);

    // Check that normal values are conforming i.e. admissible jumps
    if (_check_lvl != CheckLevel::none) {
      const int int_order = _int_order_add + _quadrature_factor * order;
      const auto jumps_norm = norm_normal_jump_skeleton(*this,int_order);
      const bool passed = jumps_norm < _check_tol;

      if (passed) {
        _log->trace("  Accumulated jumps of reconstructed flux in normal "
                      "direction: {} m/s",
                    jumps_norm);
      }
      else if (not passed && _check_lvl == CheckLevel::warn) {
        _log->warn("Accumulated jumps of reconstructed flux in normal "
                   "direction above tolerance: {} m/s",
                   jumps_norm);
      }
      else if (not passed && _check_lvl == CheckLevel::error) {
        _log->error("Accumulated jumps of reconstructed flux in normal "
                    "direction above tolerance: {} m/s",
                    jumps_norm);
        DUNE_THROW(MathError, "Reconstructed flux does not obey tolerance");
      }
    }
  }

    
  /*-----------------------------------------------------------------------*//**
   * @brief      Evaluates the reconstructed Raviart Thomas elements
   *
   * @param[in]  e     Grid entity.
   * @param[in]  x     Position in local coordinates of e.
   * @param      y     Result
   */
  inline void evaluate (const typename Traits::ElementType& e,
                      const typename Traits::DomainType& x,
                      typename Traits::RangeType& y) const
  {
      _dgfp->evaluate(e,x,y);
  }

  /*-----------------------------------------------------------------------*//**
   * @brief      Sets the time.
   * @details    Does nothing.
   *
   * @param[in]  time  The time.
   */
  void setTime (DF time)
  {}

  /*-----------------------------------------------------------------------*//**
   * @brief      get a reference to the GridView.
   *
   * @return     The grid view.
   */
  inline const typename Traits::GridViewType& getGridView () const
  {
    return _gv;
  }

private:
  /// Controls how to deal with jumps occurring in the reconstructed flux
  enum class CheckLevel { none, warn, error };

  std::shared_ptr<spdlog::logger> _log;
  GV _gv;
  FEMU _femu;
  std::shared_ptr<GFSU> _gfsu;
  std::shared_ptr<X> _x;
  std::shared_ptr<GFDGPiola> _dgfp;
  const int _int_order_add;
  const int _quadrature_factor;
  /// The jump check level
  const CheckLevel _check_lvl;
  const RF _check_tol;

  /// Return a check level for a given string
  CheckLevel to_check_lvl (const std::string check_lvl_str)
  {
    if (to_lower(check_lvl_str) == "none") {
      return CheckLevel::none;
    }
    else if (check_lvl_str == "warn") {
      return CheckLevel::warn;
    }
    else if (check_lvl_str == "error") {
      return CheckLevel::error;
    }
    else {
      _log->error("Unknown check for jumps in reconstructed flux: {}",
                  check_lvl_str);
      DUNE_THROW(IOError, "Unknown check for flux reconstruction jumps");
    }
  }
};

} // namespace Dorie
} // namespace Dune

#endif // DUNE_DORIE_RAVIART_THOMAS_PROJECTION_HH
