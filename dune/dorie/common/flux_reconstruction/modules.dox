
// These groups should gather information strongly related portions of the code. 
// In them we aim to explain to other developers:
// - The rationale of the design
// - How careful someone has to be when modifying it
// - Specific naming or other conventions upheld in the code

/**

@defgroup FluxReconstruction Flux reconstruction
@{
  @ingroup Common
  
  @brief Classes and engines to perform flux reconstruction

  ## Flux reconstruction with Raviart Thomas elements.
  Following the notation of Di Petro 2012 the discrete problem for 
  discontinuous galerkin problems can be written as:

  For all @f$T\in\mathcal{T}_h@f$ and for all @f$\xi\in\mathbb{P}^k_d(T)@f$,
  @f[
    \int_T G_h^l(u_h)\cdot\nabla\xi + \sum_{F\in\mathcal{F}_T}\epsilon_{T,F}
    \int_F\phi_F(u_h)\xi=\int_Tf\xi \qquad (1)
  @f]
  with @f$l\in\{k-1,k\}@f$,@f$\epsilon_{T,F}=n_F\cdot n_F@f$, and the 
  numerical flux
  @f[
    \phi(u_h):=-\{\{\nabla_hu_h\}\}\cdot n_F+\frac{\eta}{h_F}[[u_h]]
  @f]

  Following that, one can solve the degrees of freedom of Raviart Thomas element 
  @f$ \sigma_h^*\in\mathbb{RTN}^l_d(\mathcal{T}),\,l\in\{k-1,k\}@f$ problem 
  solving

  (i) For all @f$F\in\mathcal{F_h}@f$ and all @f$q\in\mathbb{P}^l_{d-1}(F)@f$,
  @f[
    \int_F(\sigma_h^*\cdot n_F)q:=\int_F\phi_F(u_h)q \qquad (2a)
  @f]
  
  (ii) For all @f$T\in\mathcal{T_h}@f$ and all 
  @f$r\in[\mathbb{P}^{l-1}_d(T)]^d@f$,
  @f[
    \int_T\sigma_h^*r:=-\int_TG_h^l(u_h)\cdot r \qquad (2b)
  @f]

  In the context of the flux reconstruction code 
  @f$r\in[\mathbb{P}^{l-1}_d(T)]^d@f$ is what we call the Volume Raviart 
  Thomas elements (see Dune::Dorie::RaviartThomasVolumeLocalFiniteElementMap), 
  @f$q\in\mathbb{P}^l_{d-1}(F)@f$ the skeleton elements (see 
  Dune::Dorie::SkeletonFiniteElementMap), @f$u_h@f$ the prescribed function 
  and @f$ \sigma_h^*@f$ the ansatz function (see 
  Dune::Dorie::RaviartThomasLocalFiniteElementMap).

  Since local operators usually solve the equation (1), it is not to hard to 
  modify them in order to accept non-conforming local functions spaces that 
  solve (2). Since integrals for skeleton and for volume integrals need 
  different test functions, it is implemented the 
  Dune::Dorie::LocalRaviartThomasAssemblerEngine which can forward the right 
  local function spaces the two different integrals to the local operator.

  ### Alpha volume

  In `alpha_volume` one can distinguish the *volume* Raviart Thomas element 
  from the DG element by the dimension of the range. Notice however that 
  *skeleton* Raviart Thomas elements are not used for `alpha_volume`.

  @code{.cpp}
  template<typename EG, typename LFSU, typename X, typename LFSV, typename R>
  void alpha_volume (const EG& eg, const LFSU& lfsu, const X& x, 
                                   const LFSV& lfsv, R& r) const
  {
    // Get test basis and finite element space
    using FETest = typename LFSV::Traits::FiniteElementType;
    using BasisTest = typename FETest::Traits::LocalBasisType;

    // extract its range dimension
    constexpr int dimRangeTest = BasisTest::Traits::dimRange;

    // ...
    // Geometry and integration set up

    // Get element geometry
    auto geo = eg.geometry();

    // Integration position in local coordinates
    auto p_local = ....

    // ...

    // Discrete gradient sign
    auto dg_sign = 1.0

    if constexpr (dimRangeTest == 1)                  // RT flux reconstruction
    {
      // Set up the test local function space as the gradient of v in ref-elem.

      // Retrieve the gradient in terms of the reference element
      lfsv.finiteElement().localBasis().evaluateFunction(p_local,gradphiv);

      // Piola transformation. 
      //   It ensures that gradients are well represented in the real entity.
      auto BTransposed = geo.jacobianTransposed(p_local);
      auto detB = BTransposed.determinant();
      for (unsigned int i=0; i<lfsv.size(); i++) {
        Vector y(gradphiv[i]);
        y /= detB;
        BTransposed.mtv(y,gradphiv[i]);

      // Volume integrals in RT are the negative of DG (when using lifting)
      dg_sign = -1.0
    } 
    else                                                   // Regular DG method
    {
      // Assume garlkin method
      gradphiv = gradphiu;
    }

    // ...
    // Integral computation
    
    // Use gradphiv to compute alpha integral e.g.

    if constexpr (dimRangeTest == 1)                  // RT flux reconstruction
    {
      // Discrete gradient integral (see lifting section)
    }
    else                                                   // Regular DG method
    {
      // DG integral
      for (unsigned int i = 0; i<lfsv.size(); i++)
        r.accumulate(lfsv,i, 
                     dg_sign * (gradu*gradphiv[i]) * integration_factor);
    }
    // ...

  }
  @endcode

  ### Alpha skeleton

  In `alpha_skeleton` one can distinguish the *volume* Raviart Thomas element 
  because its range dimension is equal to domain dimension, the *volume* Raviart Thomas element because its domain dimension is domain dimension minus one (there is no support for 1D local operator), and the usual DG operator because domain dimension is as usual and range dimension is one.

  @code{.cpp}
  template<class ....>
  template<typename IG, typename LFSU, typename X, typename LFSV, typename R>
    void alpha_skeleton (const IG& ig,
        const LFSU& lfsu_i, const X& x_i, const LFSV& lfsv_i,
        const LFSU& lfsu_o, const X& x_o, const LFSV& lfsv_o,
        R& r_i, R& r_o) const
  {
    // Get world dimension
    constexpr int dim = ...

    // Get test basis and finite element space
    using FETest = typename LFSV::Traits::FiniteElementType;
    using BasisTest = typename FETest::Traits::LocalBasisType;

    // extract its doman and range dimensions
    constexpr int dimDomainTest = BasisTest::Traits::dimDomain;
    constexpr int dimRangeTest = BasisTest::Traits::dimRange;

    // ...
    // Geometry and integration set up

    // Get element geometry
    auto geo_i = ig.inside().geometry();

    // Integration position in inside/outside local coordinates
    auto p_local_i = ....
    auto p_local_o = ....

    // ...

    // Discrete gradient sign
    auto dg_sign = 1.0

    // Here we take advantage that RT integrals use either phiv or phigradv, 
    // depending on the test function. Hence we only setup the one needed.

    if constexpr (dimDomainTest == dim-1)                                 // RT flux reconstruction (Skeleton)
    {
      // Set up the test local function space as v in ref-elem.

      // Fluxes on non conforming grids can be computed, but they won't be 
      // conforming in the normal direction of the face. (This defeats the 
      // engine purpose).
      if (not ig.intersection().conforming())
        DUNE_THROW(Exception,
          "Using non-conforming entities with Raviart Thomas engine");

      // Retrieve local basis
      auto basis_i = lfsv_i.finiteElement().localBasis();
      auto basis_o = lfsv_o.finiteElement().localBasis();

      // Evaluate inside/outside function in terms of local coordinates
      basis_i.evaluateFunction(p_local_i,phiv_i);
      basis_o.evaluateFunction(p_local_o,phiv_o);
    }
    else                                                                       // Regular DG method
    {
      phiv_i = phiu_i;
      phiv_o = phiu_o;
      gradphiv_i = gradphiu_i;
      gradphiv_o = gradphiu_o;
    }
  }
  @endcode

## Discrete Gradient and Lifting 

The operator @f$G_h^l:H^1(\mathcal{T}_h)\to [L^2(\Omega)]^d@f$ showed in (i) 
is what is called discrete gradient. And is defined for DG as follow: For all 
@f$v\in H^1(\mathcal{T}_h)@f$
  @f[
    G_h^l(v):=\nabla_h v-R_h^l([[v]])
  @f]
where @f$l\gt 0@f$ and @f$R_h^l([[v]])@f$ the lifting operator, which is the 
sum of a local lifting operator defined as follows: For all 
@f$\varphi\in L^2(F)@f$,
  @f[
    \int_\Omega r^l_F(\varphi)\cdot\tau_h=\int_F\{\{\tau_h\}\}\cdot n_F\varphi
    \qquad \forall\tau_h\in[\mathcal{P}_d^l(\mathcal{T}_h)]^d
  @f].

Since the lifting may be computed with Raviart Thomas elements, one can use 
the Raviart Thomas engine to compute the lifting as well. In such case, the 
`alpha_volume` computes the negative integral as the DG case and the `alpha_skeleton` must be able to receive *volume* Raviat Thomas finite 
elements.

  @code{.cpp}
  template<class ....>
  template<typename IG, typename LFSU, typename X, typename LFSV, typename R>
    void alpha_skeleton (const IG& ig,
        const LFSU& lfsu_i, const X& x_i, const LFSV& lfsv_i,
        const LFSU& lfsu_o, const X& x_o, const LFSV& lfsv_o,
        R& r_i, R& r_o) const
  {
    
    // As before ...

    if constexpr (dimRangeTest == 1)                                           // RT flux reconstruction (Volume) -> Lifting
    {
      // Set up the test local function space as the gradient of v in ref-elem.

      // Retrieve local basis
      auto basis_i = lfsv_i.finiteElement().localBasis();
      auto basis_o = lfsv_o.finiteElement().localBasis();

      // Retrieve the gradient in terms of the reference element
      basis_i.evaluateFunction(p_local_i,gradphiv_i);
      basis_o.evaluateFunction(p_local_o,gradphiv_o);

      // Piola transformation on inside element.
      auto BTransposed_i = geo_i.jacobianTransposed(p_local_i);
      auto detB = BTransposed.determinant();
      for (unsigned int i=0; i<lfsv_i.size(); i++) {
        Vector y(gradphiv_i[i]);
        y /= detB;
        BTransposed.mtv(y,gradphiv_i[i]);

      // Piola transformation on outside element.
      auto BTransposed_o = geo_o.jacobianTransposed(p_local_o);
      detB = BTransposed.determinant();
      for (unsigned int i=0; i<lfsv_o.size(); i++) {
        Vector y(gradphiv_o[i]);
        y /= detB;
        BTransposed.mtv(y,gradphiv_o[i]);

      // Volume integrals in RT are the negative of DG (when using lifting)
      dg_sign = -1.0
    }
    else
    {
      // Setup other cases
    }

    // Accumulate the right integral with if-else constexpr...

  }
  @endcode

  Moreover, lifting and flux reconstruction can be summed up and be computed 
  at the same time since the used Raviar Thomas elements are the same.

@}
**/
