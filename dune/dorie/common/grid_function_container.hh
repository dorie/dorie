#ifndef DUNE_DORIE_GRID_FUNCTION_CONTAINER_HH
#define DUNE_DORIE_GRID_FUNCTION_CONTAINER_HH

#include <list>
#include <memory>
#include <limits>

#include <dune/common/float_cmp.hh>
#include <dune/common/exceptions.hh>

#include <dune/pdelab/common/function.hh>

namespace Dune{
namespace Dorie{


/*-------------------------------------------------------------------------*//**
 * @brief      Policy to evaluate GridFunctionContainer on time.
 *
 * @details    Each policy implements the method `evaluate()`. This method
 *             evaluates the exact value of the grid function if the time
 *             associated to that grid function is given. Time values within the
 *             time interval might vary depending on the policy.
 *
 *             Two additional objects are passed to the method `evaluate()`:
 *               * `t`  The time to evaluate.
 *               * `it` A list iterator to the first grid function that
 *                      contains an associated time greater or equals to the
 *                      time to evaluate (next time).
 *                      
 *      | Symbol | Legend                                                     |
 *      |--------|------------------------------------------------------------|
 *      | `o`    | Time stamps of grid functions in the GridFunctionContainer.|
 *      | `-`    | Time intervals where policy decides what to evaluate.      |
 *             
 * @verbatim
            t     it
            |     |
   o----o---------o----o  
   @endverbatim                    
 *
 * @author     Santiago Ospina De Los Ríos
 * @date       2018
 * @ingroup    Common
 * 
 */
struct GridFunctionContainerPolicy {

  /*-----------------------------------------------------------------------*//**
   * @brief      Next step policy for GridFunctionContainer.
   * @author     Santiago Ospina De Los Ríos
   * @date       2018
   */
  struct NextStep {

     /*--------------------------------------------------------------------*//**
     * @brief   Evaluates the next time in GridFunctionContainer using this 
     *          policy.
     * @details 
     * 
     *  | Symbol | Legend                                                     |
     *  |--------|------------------------------------------------------------|
     *  | `o`    | Time stamps of grid functions in the GridFunctionContainer.|
     *  | `-`    | Time intervals where policy decides what to evaluate.      |
     *             
     * @verbatim
                t     it
                |     |
       o----o---------o----o 
                      |
                      y 
       @endverbatim  
     *
     * @param[in]  t     Time to evaluate.
     * @param[in]  it    Iterator of a pair to the next time stamp.
     * @param[in]  e     Entity of a grid.
     * @param[in]  x     Position in local coordinates with respect the entity.
     * @param      y     Value to evaluate.
     *
     * @tparam     It    Iterator type.
     * @tparam     TF    Time type.
     * @tparam     E     Entity type.
     * @tparam     D     Domain type.
     * @tparam     R     Range type.
     */
    template<class It, class TF, class E, class D, class R>
    static void evaluate(TF t, const It& it, const E& e, const D& d, R& y)
    {
      assert(it->first);
      it->first->evaluate(e,d,y);
    }
  };

  /*-----------------------------------------------------------------------*//**
   * @brief      Previous step policy for GridFunctionContainer.
   * @author     Santiago Ospina De Los Ríos
   * @date       2018
   */
  struct PreviousStep {

     /*--------------------------------------------------------------------*//**
     * @brief   Evaluates the previous time in GridFunctionContainer using 
     *          this policy.
     * @details 
     * 
     *  | Symbol | Legend                                                     |
     *  |--------|------------------------------------------------------------|
     *  | `o`    | Time stamps of grid functions in the GridFunctionContainer.|
     *  | `-`    | Time intervals where policy decides what to evaluate.      |
     *             
     * @verbatim
                t     it
                |     |
       o----o---------o----o 
            |
            y 
       @endverbatim  
     *
     * @param[in]  t     Time to evaluate.
     * @param[in]  it    Iterator of a pair to the next time stamp.
     * @param[in]  e     Entity of a grid.
     * @param[in]  x     Position in local coordinates with respect the entity.
     * @param      y     Value to evaluate.
     *
     * @tparam     It    Iterator type.
     * @tparam     TF    Time type.
     * @tparam     E     Entity type.
     * @tparam     D     Domain type.
     * @tparam     R     Range type.
     */
    template<class It, class TF, class E, class D, class R>
    static void evaluate(TF t, const It& it, const E& e, const D& x, R& y)
    {
      if (Dune::FloatCmp::eq(it->second,t))
      {
        assert(it->first);
        it->first->evaluate(e,x,y);
      } else {
        assert(std::prev(it)->first);
        std::prev(it)->first->evaluate(e,x,y);
      }
    }
  };

  /*-----------------------------------------------------------------------*//**
   * @brief      Linear interpolation policy for GridFunctionContainer.
   * @author     Santiago Ospina De Los Ríos
   * @date       2018
   */
  struct LinearInterpolation {

     /*--------------------------------------------------------------------*//**
     * @brief   Interpolates two time stamps in GridFunctionContainer using 
     *          this policy.
     * @details 
     * 
     *  | Symbol | Legend                                                     |
     *  |--------|------------------------------------------------------------|
     *  | `o`    | Time stamps of grid functions in the GridFunctionContainer.|
     *  | `-`    | Time intervals where policy decides what to evaluate.      |
     *             
     * @verbatim
                t     it
                |     |
       o----o---------o----o 
                |
                y 
       @endverbatim  
     *
     * @param[in]  t     Time to evaluate
     * @param[in]  it    Iterator of a pair to the next time stamp.
     * @param[in]  e     Entity of a grid.
     * @param[in]  x     Position in local coordinates with respect the entity.
     * @param      y     Value to evaluate.
     *
     * @tparam     It    Iterator type.
     * @tparam     TF    Time type.
     * @tparam     E     Entity type.
     * @tparam     D     Domain type.
     * @tparam     R     Range type.
     */
    template<class It, class TF, class E, class D, class R>
    static void evaluate(TF t, const It& it, const E& e, const D& x, R& y)
    {
      if (Dune::FloatCmp::eq(it->second,t)) 
      {
        assert(it->first);
        it->first->evaluate(e,x,y);
      } else if (Dune::FloatCmp::eq(std::prev(it)->second,t)) {
        assert(std::prev(it)->first);
        std::prev(it)->first->evaluate(e,x,y);
      } else {
        assert(it->first);
        assert(std::prev(it)->first);

        const TF t0 = std::prev(it)->second;
        const TF tf = it->second;

        R y0, yf;
        std::prev(it)->first->evaluate(e,x,y0);
        it->first->evaluate(e,x,yf);

        y = y0 + (t-t0)*(yf-y0)/(tf-t0);
      }
    }
  };
};

/*-------------------------------------------------------------------------*//**
 * @brief      Class for grid function container.
 * @details    This class shapes an instationary grid function and a queue (FIFO
 *             container). That is, one can push a pair (grid function and its
 *             associated time) which will be associated to the front or the
 *             most recent computation, while dropping a solution (method
 *             `pop()`) will discard the oldest pair.
 *
 * @see        Dune::Dorie::GridFunctionContainerPolicy
 * @author     Santiago Ospina De Los Ríos
 * @date       2018
 * @ingroup    Common
 *
 * @tparam     GF    Grid function type to store.
 * @tparam     TF    Time type to store time keys.
 * @tparam     Policy  The policy to evaluate intermediate results between to 
 *                     time stamps.
 */
template<class GF, class TF = double, 
         class Policy = GridFunctionContainerPolicy::NextStep>
class GridFunctionContainer
  : public Dune::PDELab::GridFunctionBase<
      typename GF::Traits,
      GridFunctionContainer<GF,TF,Policy>>
{
public:
  GridFunctionContainer() : _it(_list.end()) {}

  // export types for a queue
  using value_type =      std::pair<std::shared_ptr<GF>,const TF>;
  using container_type =  std::list<value_type>;
  using size_type =       typename container_type::size_type;
  using reference =       typename container_type::reference;
  using const_reference = typename container_type::const_reference;

private:
  using const_iterator = typename container_type::const_iterator;

public:
  // export types for the grid function
  using Traits = typename GF::Traits;
  using GridFunctionType = GF;

  /*-----------------------------------------------------------------------*//**
   * @brief      Front of the queue.
   *
   * @return     Reference the oldest pair (grid function,time) on the
   *             container.
   *             
   */
  reference front() {return _list.front();}

  /*-----------------------------------------------------------------------*//**
   * @brief      Front of the queue.
   *
   * @return     Reference the oldest pair (grid function,time) on the
   *             container.
   */
  const_reference front() const {return _list.front();}

  /*-----------------------------------------------------------------------*//**
   * @brief      Back of the queue.
   *
   * @return     Reference the most recent pair (grid function,time) on the
   *             container.
   */
  reference back() {return _list.back();}

  /*-----------------------------------------------------------------------*//**
   * @brief      Back of the queue.
   * 
   * @return     Reference the most recent pair (grid function,time) on the
   *             container.
   */
  const_reference back() const {return _list.back();}

  /*-----------------------------------------------------------------------*//**
   * @brief      Push a new pair to the container.
   * @details    This method sets the current time of the instationary grid
   *             function to the pushed time (see setTime()). The pushed time 
   *             must be greater the any other time in the container
   *
   * @param[in]  value  Pair of grid function and time.
   */
  void push( const value_type& value ) 
  {
    check_new_time(value.second);
    _list.push_back(value);
    setTime(value.second);
  };

  /*-----------------------------------------------------------------------*//**
   * @brief      Push a new pair to the container.
   * @details    This method sets the current time of the instationary grid
   *             function to the pushed time (see setTime()). The pushed time 
   *             must be greater the any other time in the container
   *
   * @param[in]  gf    Shared pointer to a grid function.
   * @param[in]  time  Time.
   */
  void push(const std::shared_ptr<GF>& gf, TF time)
  {
    check_new_time(time);
    _list.push_back({gf,time});
    setTime(time);
  };

  /*-----------------------------------------------------------------------*//**
   * @brief      Push a new pair to the container.
   * @details    This method sets the current time of the instationary grid
   *             function to the pushed time (see setTime()). The pushed time
   *             must be greater the any other time in the container
   * @note       This function will create a hard copy of the grid function 
   *             into the container.
   *
   * @param[in]  gf    Grid function.
   * @param[in]  time  Time.
   */
  void push(const GF& gf, TF time)
  {
    check_new_time(time);
    _list.push_back({std::make_shared<GF>(gf),time});
    setTime(time);
  };

  /**
   * @brief      Drop the oldest pair (grid function,time) in the container.
   */
  void pop() {
    if (_it == _list.begin())
    {
      _it = _list.end();
      _current_time = std::numeric_limits<TF>::lowest();
    }
    _list.pop_front();
  };

  /*-----------------------------------------------------------------------*//**
   * @brief      Checks if the container has no elements.
   *
   * @return     `true` if the container is empty, `false` otherwise.
   */
  bool empty() const {return _list.empty();}

  /*-----------------------------------------------------------------------*//**
   * @brief      Returns the number of elements in the queue.
   *
   * @return     The number of elements in the underlying container.
   */
  size_type size() const {return _list.size();}

  /*-----------------------------------------------------------------------*//**
   * @brief      Shrink the container to fit the current time.
   */
  void shrink_to_time() 
  {
    if (Dune::FloatCmp::eq(_it->second,_current_time))
      _list.erase(_list.begin(),_it);
    else
      _list.erase(_list.begin(),std::prev(_it));

    _list.erase(std::next(_it),_list.end());

    check_valid_state();
    check_in_range_time(_current_time);
  }

  /*-----------------------------------------------------------------------*//**
   * @brief      Evaluation of the grid function.
   * @details    Evaluation of the grid function for a given entity e in an
   *             entity position x on the time set with the method setTime(). If
   *             the time is not equal to some set time in the container, the
   *             evaluation policy will evaluate y taking into account the
   *             nearest time stamps.
   * @throws     If the current time is not contained, a Dune::InvalidStateException
   *             will be thrown.
   *
   * @param[in]  e     Entity of a grid.
   * @param[in]  x     Position in local coordinates with respect the entity.
   * @param      y     Value to evaluate.
   */
  inline void evaluate (const typename Traits::ElementType& e,
                        const typename Traits::DomainType& x,
                        typename Traits::RangeType& y) const
  {
    check_valid_state();
    Policy::evaluate(_current_time,_it,e,x,y);
  }

  /*-----------------------------------------------------------------------*//**
   * @brief      Function that returns a grid view valid for this grid function
   * @throws     If the current time is not contained, a Dune::InvalidStateException
   *             will be thrown.
   *
   * @return     Grid view
   */
  inline const typename Traits::GridViewType& getGridView () const
  {
    check_valid_state();
    return _it->first->getGridView();
  }

  /*-----------------------------------------------------------------------*//**
   * @brief      Sets a time to evaluate a the instationary grid function.
   * @throws     If the set time is not contained, a Dune::InvalidStateException
   *             will be thrown.
   *
   * @param[in]  time  The time to set.
   */
  void setTime(TF time)
  {
    if (empty())
      DUNE_THROW(Dune::InvalidStateException,
        "Grid function container is empty!");

    check_in_range_time(time);

    const auto compare = [&](auto& v)
                           {return Dune::FloatCmp::ge(v.second,time);};

    // Assume list is ordered: find first gf with greater or equal time
    _it = std::find_if(_list.begin(),_list.end(),compare);
    _current_time = time;

    check_valid_state();
  }

protected:

  /*-----------------------------------------------------------------------*//**
   * @brief      Checks that the new time is valid.
   *
   * @param[in]  time  The time
   */
  void check_new_time(const TF& time ) const
  {
    if (not _list.empty()){
      const auto& last_time = back().second;
      if (not Dune::FloatCmp::gt(time,last_time))
        DUNE_THROW(Dune::InvalidStateException,
          "New time " << time << " value is invalid!");      
    }
  }

  /*-----------------------------------------------------------------------*//**
   * @brief      Checks that the time is valid within the current ranges of the
   *             container.
   *
   * @param[in]  time  The time.
   */
  void check_in_range_time(const TF& time ) const
  {
    const auto& first_time = front().second;
    const auto& last_time = back().second;
    if (Dune::FloatCmp::lt(time,first_time))
      DUNE_THROW(Dune::InvalidStateException,
        "time below the range (" 
        << first_time << "," << last_time 
        << ") of the grid function container!");

    if (Dune::FloatCmp::gt(time,last_time))
      DUNE_THROW(Dune::InvalidStateException,
        "time above the range (" 
        << first_time << "," << last_time 
        << ") of the grid function container!");
  }

  /**
   * @brief      Checks that the container is in valid state.
   */
  void check_valid_state() const
  {
    if (empty())
      DUNE_THROW(Dune::InvalidStateException,
        "Grid function container is empty!");
    if (_it == _list.end())
      DUNE_THROW(Dune::InvalidStateException,
        "Grid function container is in invalid state!");
  }

private:
  container_type _list;
  const_iterator _it;
  TF _current_time;
};

}
}

#endif // DUNE_DORIE_GRID_FUNCTION_CONTAINER_HH