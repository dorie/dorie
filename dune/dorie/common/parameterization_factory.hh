#ifndef DUNE_DORIE_PARAMETERIZATION_READER_HH
#define DUNE_DORIE_PARAMETERIZATION_READER_HH

#include <unordered_map>
#include <memory>
#include <string>
#include <set>

#include <yaml-cpp/yaml.h>

#include <dune/common/exceptions.hh>

#include <dune/dorie/common/utility.hh>
#include <dune/dorie/common/logging.hh>

namespace Dune{
namespace Dorie{

/*-------------------------------------------------------------------------*//**
 * @brief      Interface and default reader for parameterization factories
 *
 * @tparam     Param  The parameter type to build
 */
template<class Param>
struct ParameterizationFactory
{
  virtual ~ParameterizationFactory() = default;

  /**
   * @brief      Parameterization selector
   * @details    Given a type_node in yaml, it selects a -polymorphic-
   *             parameter.
   *
   * @param[in]  type_node  YAML node describing the type of parameterization to
   *                        choose from.
   * @param[in]  name       Name that describes the parametrized material.
   *
   * @return     Shared pointer to the selected parameter.
   */
  virtual std::shared_ptr<Param> selector(
              const YAML::Node& type_node,
              const std::string& name) const = 0;


  /**
   * @brief      Parameterization reader
   * @details    It reads parameterizations and its parameters. For each volume,
   *             it assings a index to a parameterization (choosen by the
   *             parameterization selector) and later filled with the paramaters
   *             in the yaml file.
   *
   * @param[in]  param_file  YAML node with information about type and
   *                         parameters
   * @param[in]  param_name  Name of the parameterization.
   * @param[in]  log         Logger
   *
   * @return     (Unsorted) Map with parameterizations for each material.
   */
  std::unordered_map<int, std::shared_ptr<Param>> 
  reader( const YAML::Node& param_file, 
          const std::string& param_name,
          const std::shared_ptr<spdlog::logger> log) const
  {
    // mapping: index on grid to parameterization object (will be returned)
    std::unordered_map<int, std::shared_ptr<Param>> ret;

    // set to check for duplicate indices
    std::set<int> param_index_set;

    // iterate over all volume nodes
    for (auto&& node : param_file["volumes"])
    {
      const auto volume = node.first.as<std::string>();
      const YAML::Node param_info = node.second;

      // read relevant data
      const auto index = param_info["index"].as<int>();
      if (param_index_set.count(index) != 0) {
          log->error("Index '{}' in volume '{}' of parameterization '{}'"
                      "already exists.",
                      index, volume, param_name);
          DUNE_THROW(IOError, "Parameter file indices must be unique");
      }
      param_index_set.emplace(index);

      // get parameterization object
      const YAML::Node& type_node = param_info[param_name];
      auto parameterization = this->selector(type_node, volume);
      auto parameter_values = parameterization->parameters();

      // fill parameterization with parameter entries
      log->trace("  Reading parameters. Volume: {}, Index: {}",
                  volume, index);

      auto yaml_parameters = yaml_sequence_to_map(type_node["parameters"]);

      for (auto&& [name, value] : parameter_values)
      {
        try {
            value = yaml_parameters[name].template as<double>();
        }
        catch (...) {
          log->error("    Parameter '{}' is missing in '{}' parameterization,"
                     " volume {}, index {}.",
                      name, param_name, volume, index);
          DUNE_THROW(IOError, "Missing parameters!");
        }
      }

      ret.emplace(index, parameterization);
    }

    return ret;
  }
};

}
}

#endif // DUNE_DORIE_PARAMETERIZATION_READER_HH