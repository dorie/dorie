#ifndef DUNE_DORIE_RICHARDS_BOUNDARY_HH
#define DUNE_DORIE_RICHARDS_BOUNDARY_HH

#include <vector>
#include <memory>
#include <functional>

#include <yaml-cpp/yaml.h>

#include <dune/common/exceptions.hh>
#include <dune/common/parametertree.hh>
#include <dune/common/float_cmp.hh>

#include <dune/dorie/common/logging.hh>
#include <dune/dorie/common/typedefs.hh>
#include <dune/dorie/common/boundary_condition/factory.hh>

namespace Dune {
namespace Dorie {

/// Provides and manages boundary condition information
/** This class creates a boundary condition storage according to the user
 *  input and associates it with boundary segments. It provides queries to
 *  the boundary conditions through a cache based on the current simulation
 *  time.
 *
 *  To ensure a correct application of transient and temporally changing
 *  boundary conditions, this class maintains boundary conditions which overlap
 *  by exactly a single time value,
 *  \f{align*}{
 *      \Sigma_1 &= \left[ t_1, t_2 \right]\\
 *      \Sigma_2 &= \left[ t_2, t_3 \right]
 *  \f}
 *  where \f$ \Sigma_i \f$ is the time interval of boundary condition \f$ i \f$.
 *  A time step must thus not pass \f$ t_2 \f$, but reach it exactly. During
 *  this time step, boundary condition \f$ 1 \f$ is applied. At exactly
 *  \f$ t_2 \f$, a new time step must start, during which only boundary
 *  condition \f$ 2 \f$ is applied.
 *
 *  After initializing this class, the first boundary condition is selected
 *  whose begin time is the same or earlier than the simulation start time
 *  and whose end time is the same or later than the simulation start time.
 *  This is in contrast to the selection mechanism of set_time(), where the
 *  cached BC must have an end time which is later than the current simulation
 *  time. Calling set_time() with the simulation start time might lead to a
 *  different BC being cached. This allows for separate boundary conditions
 *  right at the start of the simulation, which is useful for computing an
 *  initial condition from the stationary problem before starting the actual
 *  simulation.
 *
 *  This class is intended to be used as follows:
 *  1. Build storage by constructing the class.
 *  2. At the beginning of a simulation time step, call set_time() with the
 *     current simulation time. This will update the cache.
 *  3. Retrieve the maximum time step considering all cached BCs by calling
 *     max_time_step(). Evaluating the retrieved boundary conditions for
 *     a later time will likely result in an error.
 *  4. Retrieve a shared pointer to the actual boundary condition for any
 *     boundary intersection by calling bc().
 *
 * @ingroup NonCollective
 *
 * @warning
 *  Methods of this class are @ref NonCollective. Objects of this class only
 *  store boundary conditions for the boundary indices given in the index map
 *  they are constructed with. This may lead to max_time_step() returning
 *  different values for each process when executed in a parallel application.
 *
 *  @ingroup Boundary
 */
template<typename Traits, BCMMode mode=BCMMode::none>
class BoundaryConditionManager
{
private:

    using RF = typename Traits::RF;
    using Intersection = typename Traits::Intersection;

    /// Alias of the (base) type of boundary conditions
    using BC = BoundaryCondition<RF>;

    /// The factory for building boundary conditions
    using BCF = BoundaryConditionFactory<RF, mode>;

    /// A simple list of boundary conditions
    using BCList = std::vector<std::shared_ptr<BC>>;

    /// The type used for storing boundary conditions
    /** Implements a mapping: The index of the outer vector encodes the
     *  boundary segment index. The inner vector stores a list of
     *  subsequent boundary conditions in time that will be applied
     *  on this boundary.
     */
    using BCStorage = std::vector<BCList>;

    /// The logger of this object
    const std::shared_ptr<spdlog::logger> _log;

    /// The BoundaryConditionFactory instance
    BCF _bcf;

    /// The complete boundary condition storage
    const BCStorage _bc_storage;

    /// The cache referring to the current boundary conditions
    /** The vector index corresponds to the boundary segment index.
     *  The respective shared pointer points to a single BC in the list of
     *  BCs in the boundary condition storage at the same boundary segment
     *  index.
     */
    BCList _bc_cache;

public:

    /// Construct the BC storage and initialize the cache
    /**
     *  \param config The configuration file tree
     *  \param boundary_index_map Map associating the boundary segment index
     *      (index) with a user defined index (value).
     *  \param log The logger for this instance
     */
    BoundaryConditionManager (
        const Dune::ParameterTree& config,
        const std::vector<int>& boundary_index_map,
        const std::shared_ptr<spdlog::logger> log=get_logger(log_richards))
    :
        _log(log),
        _bcf(_log),
        _bc_storage(build_bc_storage(config, boundary_index_map)),
        _bc_cache(build_bc_cache(config, _bc_storage))
    { }

    /// Return the maximum time step allowed with the current BCs
    /** \param time The current time
     *  \return The maximum time step which does not exceed the time interval
     *      of any boundary condition that is currently applied.
     *  \throw InvalidStateException if the current time 'overtook' the
     *      applied time interval (i.e., the time step returned by this
     *      function would be negative).
     *  \warning This algorithm is @ref NonCollective and may return different
     *      values for each (parallel) process it is invoked on.
     */
    RF max_time_step (const RF current_time) const
    {
        RF next_bc_time = std::numeric_limits<RF>::max();
        for (auto&& bc : _bc_cache) {
            next_bc_time = std::min(next_bc_time, bc->time_interval().end);
        }

        if (FloatCmp::eq(next_bc_time, current_time)) {
            _log->warn("The current time coincides with the next boundary "
                       "condition change. You should now call "
                       "BoundaryConditionManager::set_time to properly "
                       "update the boundary conditions");
            return 0.0;
        }
        else if (FloatCmp::lt(next_bc_time, current_time)) {
            _log->error("Current time exceeds the time interval of at least "
                        "one boundary condition that is currently applied.");
            DUNE_THROW(InvalidStateException, "Current time overtook boundary "
                                              "condition interval");
        }

        return next_bc_time - current_time;
    }

    /// Return the boundary condition to be applied at an intersection
    /**
     *  \note To receive the correct BC, call set_time() beforehand!
     *  \param is The intersection
     *  \return Shared pointer to the BoundaryCondition
     */
    std::shared_ptr<BC> bc (const Intersection& is) const
    {
        if (not is.boundary()) {
            _log->error("Querying boundary condition for intersection "
                        "which is not a boundary");
            DUNE_THROW(InvalidStateException, "Querying BC for interface "
                                              "intersection");
        }

        return _bc_cache[is.boundarySegmentIndex()];
    }

    /// Adapt the boundary condition cache to the current time
    /** Resets the cache, such that
     *  \f[
     *      t \in \left[t_s, t_e\right)_{i, j}, \forall j \in \mathcal{I}_b
     *  \f]
     *  where \f$ t \f$ is the time inserted, \f$ t_s \f$ and \f$ t_e \f$
     *  specify the time interval of boundary condition \f$ i \f$ at
     *  boundary \f$ j \f$, and \f$ \mathcal{I}_b \f$ is the set of all
     *  boundary segments.
     *
     *  \note This function should be called with the current time *once* at
     *  the *beginning* of a time step. This ensures that, throughout a time
     *  step, interpolation works correctly and no other boundary conditions
     *  can be queried. Make sure that time does not progress past the point
     *  indicated by max_time_step() when evaluating BCs.
     */
    void set_time (const RF time)
    {
        // define lambda for matching time and time interval of a BC
        auto time_matches_interval = [](const RF time,
                                        const std::shared_ptr<BC> bc)
            {
                const auto& time_int = bc->time_interval();
                return FloatCmp::ge(time, time_int.begin)
                       && FloatCmp::lt(time, time_int.end);
            };

        // iterate over all boundary segments
        for (size_t i = 0; i < _bc_storage.size(); ++i)
        {
            // check if cache needs to be adjusted
            if (time_matches_interval(time, _bc_cache[i]))
                continue;

            // Find new BC
            // NOTE: Use reverse iterators because we want to find the last
            //       element in the list for which the time matches. The list
            //       is sorted by begin *and* end times.
            const auto& bc_list = _bc_storage[i];
            const auto it = std::find_if(rbegin(bc_list),
                                         rend(bc_list),
                                         std::bind(time_matches_interval,
                                                   time,
                                                   std::placeholders::_1));

            if (it == rend(bc_list)) {
                _log->error("Time '{}' exceeds boundary conditions of "
                            "boundary segment index '{}'",
                            time, i);
                DUNE_THROW(InvalidStateException, "Setting time where BCs "
                                                  "are unspecified");
            }

            // update cache
            _bc_cache[i] = *it;
        }
    }

private:

    /// Build the boundary condition storage from the input file(s)
    /** Open a YAML input file to build the boundary condition storage.
     *  Match the storage to the boundary index map.
     *
     *  \param config The configuration file tree
     *  \param boundary_index_map Mapping from boundary segment index to
     *      user-defined segment index
     *  \return Complete set of BCs for this simulation
     */
    BCStorage build_bc_storage (const Dune::ParameterTree& config,
                                const std::vector<int>& boundary_index_map)
        const
    {
        const auto bc_file_name = config.get<std::string>("boundary.file");
        _log->info("Loading boundary condition data file: {}", bc_file_name);
        const YAML::Node bc_file = YAML::LoadFile(bc_file_name);

        const auto time_begin = config.get<RF>("time.start");
        const auto time_end = config.get<RF>("time.end");

        // Read the boundary conditions from the YAML file.
        // Store BCs in a list for every boundary index. Indices may be
        // missing, in which case the default BC is inserted.
        std::map<int, BCList> bc_association_map;
        for (const auto&& node : bc_file)
        {
            // extract keys and values from the node
            const auto& key = node.first;
            const auto& values = node.second;

            // handle default case differently?
            if (key.as<std::string>() == "default")
                continue;

            const auto index = values["index"].as<int>();

            // list up the conditions on this boundary segment
            BCList conditions_list;
            for (const auto& condition : values["conditions"]) {
                const auto name = condition.first.as<std::string>();
                _log->trace("  Creating boundary condition. "
                              "Mapping index: {}, Name: {}",
                            index, name);
                conditions_list.push_back(_bcf.create(name,
                                                      condition.second,
                                                      time_end));
            }

            if (conditions_list.empty()) {
                _log->error("Failed to read boundary conditions. "
                            "Boundary: {}, Index: {}",
                            key.as<std::string>(), index);
                DUNE_THROW(IOError, "Error reading conditions associated "
                                    "with a boundary");
            }

            bc_association_map.emplace(index, conditions_list);
        }

        // sanitize the association map
        _log->debug("  Filling gaps in boundary condition specs with "
                      "default boundary condition");
        for (auto&& it : bc_association_map)
        {
            auto& list = it.second;
            // sort by beginning of time interval
            std::sort(begin(list), end(list),
                      [](const auto bc1, const auto bc2)
                      {
                          const auto begin1 = bc1->time_interval().begin;
                          const auto end1 = bc1->time_interval().end;
                          const auto begin2 = bc2->time_interval().begin;
                          const auto end2 = bc2->time_interval().end;

                          return begin1 < begin2
                                 or (FloatCmp::eq(begin1, begin2)
                                     and end1 < end2);
                      });

            // insert an initial boundary condition, if needed
            const auto& bc_initial = list.front();
            const auto bc_initial_begin = bc_initial->time_interval().begin;
            if (FloatCmp::gt(bc_initial_begin, time_begin))
            {
                _log->trace("  Inserting default boundary condition "
                              "before condition: '{}'",
                            bc_initial->name());
                list.insert(list.begin(),
                            read_default_bc(bc_file["default"],
                                            time_begin,
                                            bc_initial_begin));
            }

            // insert a final boundary condition, if needed
            const auto& bc_final = list.back();
            const auto bc_final_end = bc_final->time_interval().end;
            if (FloatCmp::lt(bc_final_end, time_end))
            {
                _log->trace("  Inserting default boundary condition "
                              "after condition '{}'",
                            bc_final->name());
                list.insert(list.end(),
                            read_default_bc(bc_file["default"],
                                            bc_final_end,
                                            time_end));
            }

            // iterate over all BCs in this list but the last one
            for (auto it = list.begin(); std::next(it) != list.end(); ++it)
            {
                const auto& first = *it;
                const auto time_first_end = first->time_interval().end;
                const auto& second = *std::next(it);
                const auto time_second_begin = second->time_interval().begin;

                // check if time intervals overlap
                if (FloatCmp::gt(time_first_end, time_second_begin))
                {
                    _log->error("Two boundary conditions overlap in time: "
                                "'{}' ends at '{}', but '{}' begins at '{}'",
                                first->name(), time_first_end,
                                second->name(), time_second_begin);
                    DUNE_THROW(IOError, "Boundary conditions overlap");
                }

                // insert default condition if time interval is missing
                else if (FloatCmp::lt(time_first_end, time_second_begin))
                {
                    _log->trace("  Inserting default boundary condition "
                                "between conditions '{}' and '{}'",
                                first->name(), second->name());
                    list.insert(std::next(it),
                                read_default_bc(bc_file["default"],
                                                time_first_end,
                                                time_second_begin));
                }
            }
        }

        // create the actual storage
        BCStorage bc_storage(boundary_index_map.size());
        for (size_t i = 0; i < boundary_index_map.size(); ++i)
        {
            const auto index = boundary_index_map[i];
            auto it = bc_association_map.find(index);

            // no boundary conditions specified, insert default only
            if (it == bc_association_map.end()) {
                bc_storage[i].push_back(read_default_bc(bc_file["default"],
                                                        time_begin,
                                                        time_end));
            }
            // fill in the boundary conditions in the list
            else {
                bc_storage[i] = it->second;
            }
        }

        return bc_storage;
    }

    /// Build an initialized cache from a boundary condition storage
    /** This caches the first BC (according to the simulation start time) of
     *  every entry in the storage and checks if all storage lists are
     *  non-empty.
     *
     *  \param config The configuration file tree
     *  \param bc_storage The storage to initialize the cache from
     *  \return A list caching the first BC for every BC index
     */
    BCList build_bc_cache (const ParameterTree& config,
                           const BCStorage& bc_storage) const
    {
        // Retrieve the simulation start time (this is where we should be)
        const auto t_start = config.get<double>("time.start");

        // Define lambda for matching time and time interval of a BC
        // NOTE: This allows boundary conditions with the same time as
        //       begin and end times, useful for the stationary IC.
        auto time_matches_interval = [](const RF time,
                                        const std::shared_ptr<BC> bc)
            {
                const auto& time_int = bc->time_interval();
                return FloatCmp::ge(time, time_int.begin)
                       && FloatCmp::le(time, time_int.end);
            };

        // Iterate over all boundary segments
        BCList bc_cache(bc_storage.size());
        for (size_t i = 0; i < _bc_storage.size(); ++i)
        {
            // Retrieve the BC list for this boundary segment
            const auto& bc_list = _bc_storage[i];
            if (bc_list.empty()) {
                _log->error("Boundary condition storage has no entry for "
                            "boundary segment index '{}'",
                            i);
                DUNE_THROW(InvalidStateException, "Encountered empty BC"
                                                  "storage list");
            }

            // Find first BC matching the start time
            const auto it = std::find_if(begin(bc_list),
                                         end(bc_list),
                                         std::bind(time_matches_interval,
                                                   t_start,
                                                   std::placeholders::_1));

            if (it == end(bc_list)) {
                _log->error("Start time '{}' exceeds boundary conditions of "
                            "boundary segment index '{}'",
                            t_start, i);
                DUNE_THROW(InvalidStateException, "No BCs defined at "
                                                  "simulation start time");
            }

            // Update cache
            bc_cache[i] = *it;
        }

        return bc_cache;
    }

    /// Return the default boundary condition with custom time settings
    /** \param cfg_default The node containing the default BC config
     *  \param t_begin The begin time of the new BC
     *  \param t_end The end time of the new BC
     *  \return Shared pointer to a new BC with default settings and
     *      specified times
     */
    std::shared_ptr<BC> read_default_bc (YAML::Node cfg_default,
                                         const double t_begin,
                                         const double t_end) const
    {
        // face the 'time' node in the YAML file
        cfg_default["time"] = t_begin;

        return _bcf.create("default",
                           cfg_default,
                           t_end);
    }

};

} // namespace Dorie
} // namespace Dune

#endif // DUNE_DORIE_RICHARDS_BOUNDARY_HH
