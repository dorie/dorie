#ifndef DUNE_DORIE_COMMON_BOUNDARY_CONDITION_NEUMANN_HH
#define DUNE_DORIE_COMMON_BOUNDARY_CONDITION_NEUMANN_HH

#include <dune/dorie/common/boundary_condition/boundary_condition.hh>

namespace Dune {
namespace Dorie {

/// Class representing a Neumann boundary condition (fixed flux)
/** In the \ref RichardsModel model, this class typically models irrigation and
 *  evaporation from soil surfaces and therefore stores an additional switch
 *  which can be used to consider the surface tilt when evaluating the boundary
 *  condition.
 *
 *  If #_horizontal_projection is set to true, the local operator scales the
 *  boundary condition \f$ j \f$ with
 *  \f[
 *      j = j_N \left| \mathbf{\hat{n}}_F \cdot \mathbf{\hat{g}}_F \right|
 *  \f]
 *  where the vectors are the unit normal vector of the boundary face, and
 *  the unit vector along the gravitational force, respectively.
 *
 *  #_horizontal_projection is ignored in the \ref TransportModel model.
 *
 *  \ingroup Boundary
 */
template<typename RF>
class NeumannBoundaryCondition : public BoundaryCondition<RF>
{
protected:
    using Base = BoundaryCondition<RF>;
    using Time = typename Base::Time;

    /// Value of the surface adjustment switch
    const bool _horizontal_projection;

public:
    /// Construct a static boundary condition
    NeumannBoundaryCondition (const std::string_view name,
                              const TimeInterval<Time> interval,
                              const RF value,
                              const bool horizontal_projection)
    :
        Base(name, interval, value),
        _horizontal_projection(horizontal_projection)
    { }

    /// Construct an interpolated boundary condition
    NeumannBoundaryCondition (const std::string_view name,
                              const TimeInterval<Time> interval,
                              const RF value_begin,
                              const std::optional<RF> value_end,
                              const bool horizontal_projection)
    :
        Base(name, interval, value_begin, value_end),
        _horizontal_projection(horizontal_projection)
    { }

    /// Destroy this object
    virtual ~NeumannBoundaryCondition () override = default;

    /// Return the type of this boundary condition
    virtual Operator::BCType type () const override
    {
        return Operator::BCType::Neumann;
    }

    /// Return true the BC is to be projected horizontally
    bool horizontal_projection () const { return _horizontal_projection; }

    /// The type string of this boundary condition class
    static constexpr std::string_view type_str = "neumann";
};

} // namespace Dorie
} // namespace Dune

#endif // DUNE_DORIE_COMMON_BOUNDARY_CONDITION_NEUMANN_HH
