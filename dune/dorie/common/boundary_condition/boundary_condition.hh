#ifndef DUNE_DORIE_COMMON_BOUNDARY_CONDITION_BASE_HH
#define DUNE_DORIE_COMMON_BOUNDARY_CONDITION_BASE_HH

#include <string>
#include <string_view>
#include <optional>
#include <memory>

#include <dune/common/exceptions.hh>

#include <dune/dorie/common/logging.hh>
#include <dune/dorie/common/typedefs.hh>
#include <dune/common/float_cmp.hh>

namespace Dune {
namespace Dorie {

/// A generic boundary condition storage
/** Stores the boundary condition time interval and values,
 *  and interpolates them linearly
 *
 *  \tparam RF Type of the boundary codition value
 *  \ingroup Boundary
 */
template<typename RF>
class BoundaryCondition
{
public:
    /// The type used for identifying time
    using Time = double;

protected:
    /// The time interval in which the BC is applied
    const TimeInterval<Time> _time_interval;
    /// The starting (or overall) value
    const RF _value_begin;
    /// The optional final value
    const std::optional<RF> _value_end{};
    /// The name given by the user in the data file
    const std::string _name;

    /// Check if the given time is inside the time interval
    /** \param time The point in time to check
     *  \return True if the time is inside the interval of this BC
     */
    bool in_interval (const Time time) const
    {
        const auto& t_begin = _time_interval.begin;
        const auto& t_end = _time_interval.end;

        using namespace Dune::FloatCmp;
        if (lt(time, t_begin) or gt(time, t_end))
        {
            auto log = Dorie::get_logger(Dorie::log_base);
            log->error("Querying boundary condition '{}' outside its time "
                       "interval [{}, {}] with time: {}",
                       _name, t_begin, t_end, time);
            return false;
        }

        return true;
    }

public:

    /// Construct a static boundary condition
    BoundaryCondition (const std::string_view name,
                       const TimeInterval<Time> interval,
                       const RF value)
    :
        _time_interval(interval),
        _value_begin(value),
        _name(name)
    { }

    /// Construct an interpolated boundary condition
    BoundaryCondition (const std::string_view name,
                       const TimeInterval<Time> interval,
                       const RF value_begin,
                       const std::optional<RF> value_end)
    :
        _time_interval(interval),
        _value_begin(value_begin),
        _value_end(value_end),
        _name(name)
    { }

    /// Destroy this object
    virtual ~BoundaryCondition () = default;

    /// Return the name of this boundary condition
    std::string_view name () const { return _name; }

	/// Return the time interval of this boundary condition
	const TimeInterval<Time>& time_interval () const
    {
        return _time_interval;
    }

    /// Return the type of this boundary condition
    virtual Operator::BCType type () const
    {
        return Operator::BCType::none;
    }

    /// Return the value for a given point in time
    /** \param time The time at which to evaluate the BC
     *  \return The (possibly interpolated) value
     *  \throw InvalidStateException if the time is not inside the interval
     *  \todo Maybe use `assert` on in_interval
     */
    virtual RF evaluate (const Time time) const
    {
        if (not in_interval(time)) {
            DUNE_THROW(InvalidStateException, "Evaluating boundary condition "
                                              "with invalid time");
        }

        // without interpolation, return the first value
        if (not _value_end)
            return _value_begin;

        // else: interpolate values
        const auto& t_begin = _time_interval.begin;
        const auto& t_end = _time_interval.end;
        const auto& v_end = _value_end.value();

        return _value_begin
               + (time - t_begin)
               * (v_end - _value_begin)
               / (t_end - t_begin);
    }
};

} // namespace Dorie
} // namespace Dune

#endif // DUNE_DORIE_COMMON_BOUNDARY_CONDITION_BASE_HH
