#ifndef DUNE_DORIE_COMMON_BOUNDARY_CONDITION_DIRICHLET_HH
#define DUNE_DORIE_COMMON_BOUNDARY_CONDITION_DIRICHLET_HH

#include <dune/dorie/common/boundary_condition/boundary_condition.hh>

namespace Dune {
namespace Dorie {

/// Toggle for concentration represented by Dirichlet BC
/** Used for \ref TransportModel model only.
 *
 *  The relation of water_phase and total is given by
 *  \f[
 *      C_w = C_t \theta_w \, ,
 *  \f]
 *  where \f$ \theta_w \f$ is the volumetric water content.
 */
enum class SoluteConcentration {
    total, //!< Total concentration in the volume \f$ C_t \f$.
    water_phase, //!< Concentration in water phase \f$ C_w \f$.
    none //!< No type, used for \ref RichardsModel model.
};

/// Class representing a Dirichlet boundary condition (fixed solution)
/** In the \ref TransportModel model, the #_solute_concentration switch is used
 *  to determine which type of solute concentration the boundary condition
 *  refers to.
 *
 *  \ingroup Boundary
 */
template<typename RF>
class DirichletBoundaryCondition : public BoundaryCondition<RF>
{
protected:
    using Base = BoundaryCondition<RF>;
    using Time = typename Base::Time;

    /// Setting for the type of concentration encoded in this BC value
    const SoluteConcentration _solute_concentration;

public:
    /// Construct a static boundary condition
    DirichletBoundaryCondition (
        const std::string_view name,
        const TimeInterval<Time> interval,
        const RF value,
        const SoluteConcentration concentration=SoluteConcentration::none)
    :
        Base(name, interval, value),
        _solute_concentration(concentration)
    { }

    /// Construct an interpolated boundary condition
    DirichletBoundaryCondition (
        const std::string_view name,
        const TimeInterval<Time> interval,
        const RF value_begin,
        const std::optional<RF> value_end,
        const SoluteConcentration concentration=SoluteConcentration::none)
    :
        Base(name, interval, value_begin, value_end),
        _solute_concentration(concentration)
    { }

    /// Destroy this object
    virtual ~DirichletBoundaryCondition () override = default;

    /// Return the type of this boundary condition
    virtual Operator::BCType type () const override
    {
        return Operator::BCType::Dirichlet;
    }

    /// The type string of this boundary condition class
    static constexpr std::string_view type_str = "dirichlet";

    /// Return the quantity encoded in this Dirichlet value
    SoluteConcentration concentration_type () const
    {
        return _solute_concentration;
    }
};

} // namespace Dorie
} // namespace Dune

#endif // DUNE_DORIE_COMMON_BOUNDARY_CONDITION_DIRICHLET_HH
