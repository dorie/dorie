#ifndef DUNE_DORIE_COMMON_BOUNDARY_CONDITION_OUTFLOW_HH
#define DUNE_DORIE_COMMON_BOUNDARY_CONDITION_OUTFLOW_HH

#include <dune/dorie/common/boundary_condition/boundary_condition.hh>

namespace Dune {
namespace Dorie {

/// Class representing an Outflow BC (matter may leave through the boundary)
/** The exact implementation depends on the model.
 *
 *  \ingroup Boundary
 */
template<typename RF>
class OutflowBoundaryCondition : public BoundaryCondition<RF>
{
protected:
    using Base = BoundaryCondition<RF>;
    using Time = typename Base::Time;

public:
    /// Re-use base constructor
    using Base::Base;

    /// Destroy this object
    virtual ~OutflowBoundaryCondition () override = default;

    /// Return the type of this boundary condition
    virtual Operator::BCType type () const override
    {
        return Operator::BCType::Outflow;
    }

    /// The type string of this boundary condition class
    static constexpr std::string_view type_str = "outflow";  
};

} // namespace Dorie
} // namespace Dune

#endif // DUNE_DORIE_COMMON_BOUNDARY_CONDITION_OUTFLOW_HH
