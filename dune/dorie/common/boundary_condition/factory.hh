#ifndef DUNE_DORIE_COMMON_BOUNDARY_CONDITION_FACTORY_HH
#define DUNE_DORIE_COMMON_BOUNDARY_CONDITION_FACTORY_HH

#include <string>
#include <string_view>
#include <optional>
#include <memory>

#include <yaml-cpp/yaml.h>

#include <dune/common/exceptions.hh>

#include <dune/dorie/common/logging.hh>
#include <dune/dorie/common/typedefs.hh>
#include <dune/dorie/common/utility.hh>
#include <dune/dorie/common/boundary_condition/dirichlet.hh>
#include <dune/dorie/common/boundary_condition/neumann.hh>
#include <dune/dorie/common/boundary_condition/outflow.hh>

namespace Dune {
namespace Dorie {

/// Mode the boundary condition manager operates in
/** Based on this switch, the BoundaryConditionManager specializes for a
 *  certain model.
 *
 *  \ingroup Boundary
 */
enum class BCMMode { none, //!< All boundary conditions available
                     richards, //!< BCs of \ref RichardsModel model available
                     transport //!< BCs of \ref TransportModel model available
                   };

/// A factory for building boundary conditions
/** \tparam RF The range field type encoding the return type of the boundary
 *      condition value.
 *  \tparam mode BC restriction switch based on model this factory is called
 *      from.
 *  \ingroup Boundary
 */
template<typename RF, BCMMode mode=BCMMode::none>
struct BoundaryConditionFactory
{
private:
    using BC = BoundaryCondition<RF>;
    using Time = typename BC::Time;

    /// The logger for this instance
    std::shared_ptr<spdlog::logger> _log;

    /// A handle for communicating if a BC type is permitted in a model
    enum class TypeHandle { unknown, //!< Not an implemented BC type
                            known, //!< Known BC, but not permitted
                            permitted //!< BC supported by the model
                          };

    /// List of know type strings for boundary conditions
    static constexpr inline std::array<std::string_view, 3> known_type_strs = {
        DirichletBoundaryCondition<RF>::type_str,
        NeumannBoundaryCondition<RF>::type_str,
        OutflowBoundaryCondition<RF>::type_str
    };

    /// Return the type strings of permitted boundary conditions
    /** Currently, both models support all boundary condition types.
     */
    static constexpr auto get_permitted_type_strs ()
    {
        return known_type_strs;
    }

    /// Return true of the given BC type is permitted for this model
    /** \param type The BC type as string
     *  \return TypeHandle if this type is permitted by this model
     */
    TypeHandle type_handle (const std::string_view type) const
    {
        const auto permitted_types = get_permitted_type_strs();

        if (std::find(begin(permitted_types), end(permitted_types), type)
                != end(permitted_types))
            return TypeHandle::permitted;

        const auto& known_types = known_type_strs;
        if (std::find(begin(known_types), end(known_types), type)
                != end(known_types))
            return TypeHandle::known;

        return TypeHandle::unknown;
    }

    /// Return the time interval read from a config setting
    /** If the config is scalar, use the default end of the time interval
     *  to specify the boundary condition.
     *
     *  \param config The YAML node containing the boundary condition data.
     *  \param time_end_default The end of the time interval if not given
     *      in the YAML node
     *  \return The time interval of the boundary condition
     *  \throw IOError Invalid node or improper value sequence
     */
    TimeInterval<Time> get_time_interval (const YAML::Node& config,
                                          const Time time_end_default) const
    {
        const YAML::Node& node = config["time"];
        TimeInterval<Time> ret;

        if (node.IsScalar()) {
            ret.begin = node.as<Time>();
            ret.end = time_end_default;
        }
        else if (node.IsSequence() and node.size() == 2) {
            std::tie(ret.begin, ret.end)
                = node.as<std::pair<Time, Time>>();
        }
        else {
            _log->error("Boundary conditions only support a scalar value "
                        "or a sequence of two values as time interval");
            DUNE_THROW(IOError, "Error reading boundary condition time");
        }

        return ret;
    }

    /// Return the BC values read from a config setting
    /**
     *  \param config The YAML node containing the boundary condition data.
     *  \return A pair: The starting (or overall) value, and the optional
     *      final value (empty if the YAML node was scalar)
     *  \throw IOError Invalid node or improper value sequence
     */
    std::pair<RF, std::optional<RF>> get_values (
        const YAML::Node& config
    ) const
    {
        const YAML::Node& node = config["value"];

        RF v_begin;
        std::optional<RF> v_end;

        if (node.IsScalar()) {
            v_begin = node.as<RF>();
        }
        else if (node.IsSequence() && node.size() == 2) {
            std::tie(v_begin, v_end) = node.as<std::pair<RF, RF>>();
        }
        else {
            _log->error("Boundary conditions only support a scalar value "
                        "or a sequence of two values as value interval.");
            DUNE_THROW(IOError, "Error reading boundary condition value");
        }

        return std::make_pair(v_begin, v_end);
    }

    /// Return the concentration type for solute transport Dirichlet BCs
    /** The parameter is ignored for BCMMode::richards and must not be 'none'
     *  or empty for BCMMode::transport.
     *  \param config The YAML Node containing the boundary condition data
     *  \return SoluteConcentration as enum
     *  \throw IOError Invalid `concentration_type` node for Transport BC.
     */
    SoluteConcentration get_solute_c_type (const YAML::Node& config) const
    {
        const auto &node = config["concentration_type"];

        // Node does not exist
        if (not node)
        {
            if (mode == BCMMode::transport) {
                _log->error("Node 'concentration_type' must be given for "
                            "Transport simulation Dirichlet BC");
                DUNE_THROW(IOError, "Transport Dirichlet BC "
                                    "'concentration_type' not specified");
            }
            else {
                return SoluteConcentration::none;
            }
        }

        // Node exists, read value string
        const auto type_str = node.as<std::string>();

        // Value string is 'none' or empty
        if (is_none(type_str))
        {
            if (mode == BCMMode::richards) {
                _log->warn("Ignoring Dirichlet BC parameter "
                           "'solute_concentration' for Richards simulation");
                return SoluteConcentration::none;
            }
            else if (mode == BCMMode::transport) {
                _log->error("Invalid 'solute_concentration' parameter for "
                            "Transport simulation Dirichlet BC: {}",
                            type_str);
                DUNE_THROW(IOError, "Transport simulation Dirichlet BC "
                                    "requires valid 'solute_concentration'");
            }
            // BCMMode::none
            else {
                return SoluteConcentration::none;
            }
        }

        // Value string is anything else
        if (type_str == "water_phase") {
            return SoluteConcentration::water_phase;
        }
        else if (type_str == "total") {
            return SoluteConcentration::total;
        }
        else {
            _log->error("Unknown 'solute_concentration' type: {}",
                        type_str);
            DUNE_THROW(IOError, "Unknown 'concentration_type' in BC");
        }
    }

    /// Read the horizontal projection parameter from the BC configuration
    /** The parameter is ignored for BCMMode::transport.
     *  \param config The YAML Node containing the boundary condition data.
     *  \return True if horizontal projection should be applied in the BC.
     */
    bool get_horizontal_projection (const YAML::Node& config) const
    {
        const YAML::Node &node = config["horizontal_projection"];

        // Node exists
        if (node)
        {
            if (mode == BCMMode::transport) {
                _log->warn("Ignoring 'horizontal_projection' parameter for "
                           "Transport simulation Neumann BC");
                return false;
            }

            // Interpret the node
            return node.as<bool>();
        }

        // Node does not exist
        return false;
    }

public:

    /// Instantiate the Factory.
    /** \param log The logger this factory uses
     */
    explicit BoundaryConditionFactory (
        const std::shared_ptr<spdlog::logger> log=get_logger(log_base)
    ):
        _log(log)
    { }

    /// Create a boundary condition from a YAML node
    /** \param name The name of the boundary condition
     *  \param config The YAML node containing the boundary condition data
     *      as key-value pairs (node must be a mapping).
     *  \param time_end_default The default end of the time interval in which
     *      this boundary condition is applied (used if not specified in the
     *      configuration)
     *  \return Shared pointer to the boundary condition
     */
    std::shared_ptr<BoundaryCondition<RF>> create (
        const std::string_view name,
        const YAML::Node& config,
        const Time time_end_default
    ) const
    {
        const auto type = to_lower(config["type"].as<std::string>());

        // check if type is permitted
        const auto handle = type_handle(type);
        if (handle == TypeHandle::unknown) {
            _log->error("Unknown boundary condition type: {}", type);
            DUNE_THROW(NotImplemented, "Unknown boundary condition type");
        }
        else if (handle == TypeHandle::known) {
            _log->error("Boundary condition type '{}' not available for "
                        "this model",
                       type);
            DUNE_THROW(IOError, "Boundary condition type not supported "
                                "by model");
        }

        // read the time interval from the config
        const auto t_int = get_time_interval(config, time_end_default);

        // read the values from the config
        const auto [v_begin, v_end] = get_values(config);

        // create the object
        if (type == DirichletBoundaryCondition<RF>::type_str) {
            // check solute concentration type for Transport sim
            const auto solute_c_type = get_solute_c_type(config);
            return std::make_shared<DirichletBoundaryCondition<RF>>(
                name, t_int, v_begin, v_end, solute_c_type
            );
        }
        else if (type == OutflowBoundaryCondition<RF>::type_str) {
            return std::make_shared<OutflowBoundaryCondition<RF>>(
                name, t_int, v_begin, v_end
            );
        }
        else if (type == NeumannBoundaryCondition<RF>::type_str) {
            // check horizontal projection for Richards sim
            const bool project = get_horizontal_projection(config);
            return std::make_shared<NeumannBoundaryCondition<RF>>(
                name, t_int, v_begin, v_end, project
            );
        }
        // should never reach this part...
        else {
            DUNE_THROW(InvalidStateException, "Error creating boundary "
                                              "condition");
        }
    }
};

} // namespace Dorie
} // namespace Dune

#endif // DUNE_DORIE_COMMON_BOUNDARY_CONDITION_FACTORY_HH
