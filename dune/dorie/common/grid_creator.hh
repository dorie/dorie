#ifndef DUNE_DORIE_GRID_CREATOR_HH
#define DUNE_DORIE_GRID_CREATOR_HH

#include <memory>
#include <array>
#include <vector>
#include <type_traits>
#include <string>

#include <dune/common/fvector.hh>
#include <dune/common/exceptions.hh>
#include <dune/common/parametertree.hh>
#include <dune/common/parallel/mpihelper.hh>

#include <dune/grid/yaspgrid.hh>
#include <dune/grid/uggrid.hh>
#include <dune/grid/io/file/gmshreader.hh>
#include <dune/grid/common/gridfactory.hh>
#include <dune/grid/utility/structuredgridfactory.hh>

#include <dune/dorie/common/grid_mapper.hh>

namespace Dune {
namespace Dorie {


/// This class builds grids by reading GMSH files or using the Dune GridFactory.
/** Additionally, it provides the mapping for associating grid elements with
 *  soil layers (parameter sets) via the GridMapper.
 *
 *  \tparam Grid The type of the grid to build
 *  \ingroup Grid
 *  \author Lukas Riedel
 *  \date 2018
 */
template<typename Grid>
class GridCreator
{
private:
    //! Spatial dimensions of the grid
    static constexpr int _dim = Grid::dimension;
    //! Dune MPI helper for querying processor ranks
    const Dune::MPIHelper& _helper;
    //! Config file tree
    const Dune::ParameterTree& _config;
    //! Verbosity of this class
    const std::shared_ptr<spdlog::logger> _log;
    //! The mode for reading the grid.
    GridMode _grid_mode;
    //! Typedef for the created grid mapper
    using GridMapper = typename Dune::Dorie::GridMapperBase<Grid>;
    /// Type of the maps returned by the GridMapper
    using Map = typename GridMapper::Map;
    /// The grid
    std::shared_ptr<Grid> _grid;
    /// The element mapping
    Map _element_index_map;
    /// Boundary mapping
    Map _boundary_index_map;

public:

    /// Build the grid and retrieve mapping information
    /**
     *  \param config User config settings
     *  \param helper Instance of the MPI helper
     */
    explicit GridCreator (const Dune::ParameterTree& config,
                          const Dune::MPIHelper& helper):
        _helper(helper),
        _config(config),
        _log(Dorie::get_logger(log_base))
    {
        check_parallel_allowed();

        const auto grid_type = _config.get<std::string>("grid.gridType");
        if (grid_type == "rectangular")
        {
            _grid_mode = GridMode::rectangular;

            _grid = create_rect_grid();

            Dorie::GridMapper<Grid, GridMode::rectangular> mapper(_grid, _config);

            _element_index_map = mapper.element_index_map();
            _boundary_index_map = mapper.boundary_index_map();
        }
        else if (grid_type == "gmsh")
        {
            if constexpr (std::is_same_v<Grid, Dune::UGGrid<2>> or
                          std::is_same_v<Grid, Dune::UGGrid<3>>)
            {
                _grid_mode = GridMode::gmsh;

                auto [grid,
                    element_index_map,
                    boundary_index_map] = create_gmsh_grid();
                _grid = grid;

                Dorie::GridMapper<Grid, GridMode::gmsh> mapper(_grid,
                                                        element_index_map,
                                                        boundary_index_map);

                _element_index_map = mapper.element_index_map();
                _boundary_index_map = mapper.boundary_index_map();
            }
            else {
                _log->error("DORiE expects to work on UGGrid for "
                            "grid type: gmsh");
                DUNE_THROW(IOError, "Incompatible GridTypes and grid creation "
                    << "algorithms");
            }
        }
        else {
            _log->error("Unsupported grid type: {}", grid_type);
            DUNE_THROW(IOError, "Unsupported grid type!");
        }
    }

    /// Return a shared pointer to the created grid
    std::shared_ptr<Grid> grid () const { return _grid; }
    /// Return the local element index map
    const Map& element_index_map () const { return _element_index_map; }
    /// Return the local boundary index map
    const Map& boundary_index_map () const { return _boundary_index_map; }

    /// Return true if parallel execution is allowed. Issue a warning if not.
    /** Currently, we only allow parallel execution on YaspGrid due to
     *  constraints imposed by the linear solver.
     */
    bool check_parallel_allowed () const
    {
        if (_helper.size() > 1
            && not std::is_same_v<YaspGrid<Grid::dimension>, Grid>) {
            _log->warn("DORiE's solver does not support parallel "
                       "execution on unstructured grids. This will likely "
                       "result in a fatal exception (later on)");
            return false;
        }

        return true;
    }

private:

    /// Create an unstructured grid from a GMSH file
    /**
     *  \return Tuple: Shared pointer to grid; Element index map;
     *      Boundary index map
     */
    std::tuple<std::shared_ptr<Grid>, std::vector<int>, std::vector<int>>
        create_gmsh_grid () const
    {
        const int level = _config.get<int>("grid.initialLevel");
        const std::string meshfilename = _config.get<std::string>("grid.gridFile");

        _log->info("Creating an unstructured grid from GMSH input file: {}",
                   meshfilename);

        Dune::GridFactory<Grid> factory;
        std::vector<int> boundary_index_map;
        std::vector<int> element_index_map;
        if (_helper.rank() == 0) {
            Dune::GmshReader<Grid>::read(factory,
                                         meshfilename,
                                         boundary_index_map,
                                         element_index_map,
                                         false // verbose
                                        );
        }
        auto grid = std::shared_ptr<Grid>(factory.createGrid());

        _log->debug("  Applying global refinement of level: {}", level);
        grid->globalRefine(level);

        return std::make_tuple(grid, element_index_map, boundary_index_map);
    }

    /// Create a rectangular grid from config file specifications
    /** Creates a cell and boundary mapping after building the grid.
     *  \return Tuple: Shared pointer to grid; Element index map;
     *      Boundary index map; Number of cells in each direction
     */
    std::shared_ptr<Grid>
        create_rect_grid () const
    {
        const auto level = _config.get<int>("grid.initialLevel");
        const auto upperRight = _config.get<Dune::FieldVector<double, _dim>>("grid.extensions");
        const auto elements = _config.get<std::array<unsigned int, _dim>>("grid.cells");

        _log->info("Creating a rectangular grid in {}D", Grid::dimension);
        _log->debug("  Grid extensions: {}", to_string(upperRight));
        _log->debug("  Grid cells: {}", to_string(elements));

        auto grid = grid_cube_construction<Grid>(upperRight, elements);

        _log->debug("  Applying global refinement of level: {}", level);
        grid->globalRefine(level);

        return grid;
    }

    /// Default rectangular grid constructor. Calls StructuredGridFactory.
    /** \param extensions Extensions of the grid
     *  \param elements Number of elements in each direction
     *  \return Shared pointer to the grid
     */
    template<typename T=Grid>
    std::enable_if_t<
        not std::is_same_v<YaspGrid<_dim>, T>,
        std::shared_ptr<Grid>>
    grid_cube_construction (const Dune::FieldVector<double, _dim>& extensions,
        const std::array<unsigned int, _dim>& elements) const
    {
        const Dune::FieldVector<double, _dim> origin(.0);

        return Dune::StructuredGridFactory<Grid>::createCubeGrid(origin,
                                                                 extensions,
                                                                 elements);
    }

    /// Specialized constructor for parallel YaspGrid construction.
    /** This is necessary because of a bug in StructuredGridFactory which does
     *  not create a grid with overlap.
     *  \todo The bug was fixed. This function can be removed after the next
     *      DUNE release.
     *  \param extensions Extensions of the grid
     *  \param elements Number of elements in each direction
     *  \return Shared pointer to the grid
     */
    template<typename T=Grid>
    std::enable_if_t<
        std::is_same_v<YaspGrid<_dim>, T>,
        std::shared_ptr<Grid>>
    grid_cube_construction (const Dune::FieldVector<double, _dim>& extensions,
        const std::array<unsigned int, _dim>& elements) const
    {
        std::array<int, _dim> elements_;
        std::copy(elements.cbegin(),elements.cend(),elements_.begin());

        return std::make_shared<YaspGrid<_dim>>(extensions, elements_);
    }
};

} // namespace Dorie
} // namespace Dune

#endif // DUNE_DORIE_GRID_CREATOR_HH
