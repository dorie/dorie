#ifndef DUNE_DORIE_CFL_CONDITION_HH
#define DUNE_DORIE_CFL_CONDITION_HH

#include <dune/common/float_cmp.hh>

#include <limits>

namespace Dune{
namespace Dorie{

  /**
   * @brief      Computes the CFL-condition (\f$\mathcal{CFL}\f$) for a grid
   *             function representing a velocity field. In particular, the
   *             return value is the suggested timestep for the lowest CFL
   *             ratio for each cell:
   *
   * @f{eqnarray*}{
   *    \mathcal{CFL} = \min_{T\in\mathcal{T}_h} ||\beta||^{-1}_{[L^\infty(T)]^d}h_T
   * @f}
   *            where \f$\mathcal{T}_h\f$ is a triangulation, \f$\beta\f$ the
   *            velocity field, and \f$h_T\f$ the diameter of the cell
   *            \f$T\in \mathcal{T}_h\f$.
   *            As is usual with the CFL-condition, one can restrict the time
   *            step (\f$\Delta t\f$) for explicit schemes using a given courant
   *            number \f$\varrho \le 1\f$:
   *
   * @f{eqnarray*}{
   *    \Delta t \le \varrho \cdot \mathcal{CFL}
   * @f}
   *
   * @param[in]  gf        Grid function representing the veolcity field
   *                       \f$\beta\f$.
   * @param[in]  intorder  The integration order. This value determines the
   *                       quadrature points to evaluate
   *                       \f$||\beta||_{[L^\infty(T)]^d}h_T\f$.
   *
   * @tparam     GF        The grid function type.
   * @tparam     RF        The range field. Type to represent CFL values.
   *
   * @ingroup    Common
   * @remark     @ref Collective
   *
   * @return     The CFL-condition (\f$\mathcal{CFL}\f$).
   */
  template<class VectorGF, class ScalarGF, class RF = double>
  RF cfl_condition(const VectorGF& water_flux,
                   const ScalarGF& water_content,
                   const unsigned int intorder = 1)
  {
    const auto& gv = water_flux.getGridView();

    RF suggested_dt = std::numeric_limits<RF>::max();

    // Loop over the grid
    for (const auto& cell : Dune::elements(gv,Dune::Partitions::interior))
    {
      auto geo = cell.geometry();
      // Check that cell is not a vertex
      assert(geo.corners()>1);

      using DF = typename VectorGF::Traits::DomainFieldType;
      DF h_T = std::numeric_limits<DF>::max();

      // Compute shortest axis of cell
      for (int i = 1; i < geo.corners(); ++i)
        for (int j = 0; j < i-1; ++j)
          h_T = std::min(h_T, (geo.corner(i)-geo.corner(j)).two_norm());

      // Check that h_T is not zero in debug mode, who knows.
      assert(Dune::FloatCmp::gt(h_T,0.));

      typename VectorGF::Traits::RangeFieldType max_velocity = 0.;

      // Find the maximum velocity in the cell
      for (const auto& point : quadratureRule(geo, intorder))
      {
        typename VectorGF::Traits::RangeType velocity;
        typename ScalarGF::Traits::RangeType scale;
        const auto& x = point.position();

        water_flux.evaluate(cell, x, velocity);
        water_content.evaluate(cell, x, scale);
        velocity /= scale;
        max_velocity = std::max(max_velocity, velocity.two_norm());
      }

      // Calculate the cfl for this cell 
      if (Dune::FloatCmp::gt(max_velocity, 0.))
        suggested_dt = std::min(suggested_dt, h_T/max_velocity);
    }

    // Communicate the lowest cfl number to all procesors
    suggested_dt = gv.comm().min(suggested_dt);
    return suggested_dt;
  }
}
}

#endif
