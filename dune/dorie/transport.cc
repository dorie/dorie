#ifdef HAVE_CONFIG_H
  #include "config.h"
#endif

#include <string>
#include <exception>

#include <dune/common/timer.hh>
#include <dune/common/exceptions.hh>

#include <dune/dorie/common/setup.hh>
#include <dune/dorie/model/factory_transport.hh>

//===============================================================
// Main program with grid setup
//===============================================================

int main(int argc, char** argv)
{
  try{
    Dune::Timer timer;

    // Initialize all the stuff!
    const std::string grt = "Starting DORiE";
    auto [inifile, log, helper] = Dune::Dorie::Setup::init(argc, argv, grt);

    // halt process for debugger
    if (inifile.get<bool>("misc.debugMode")) {
      Dune::Dorie::Setup::debug_hook();
    }

    // create and run model
    auto model = Dune::Dorie::TransportFactory::create(inifile,
                                                       helper);
    model->run();

    log->info("DORiE finished after {:.2e}s :)",
           timer.elapsed());

    return 0;
  }
  catch (Dune::Exception &exc){
    auto log = Dune::Dorie::create_base_logger();
    log->critical("Aborting DORiE after exception: {}", exc.what());
    return 1;
  }
  catch (std::exception& exc) {
    auto log = Dune::Dorie::create_base_logger();
    log->critical("Aborting DORiE after exception: {}", exc.what());
    return 1;
  }
  catch (...){
    auto log = Dune::Dorie::create_base_logger();
    log->error("Unknown error occurred");
    log->critical("Aborting DORiE after uncaught exception");
    return 1;
  }
}
