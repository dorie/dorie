#ifdef HAVE_CONFIG_H
    #include "config.h"
#endif

#include <string>
#include <iostream>
#include <exception>
#include <map>
#include <exception>

#include <dune/common/exceptions.hh>
#include <dune/common/parametertree.hh>
#include <dune/common/parametertreeparser.hh>
#include <dune/common/parallel/mpihelper.hh>
#include <dune/common/float_cmp.hh>

#include <dune/dorie/common/setup.hh>
#include <dune/dorie/common/grid_creator.hh>
#include <dune/dorie/model/transport/solute_parameters.hh>
#include <dune/dorie/model/transport/parameterization/interface.hh>

#include "test-parameterization.hh"

/// Return parameter values based on the material index
/** \param index index assigned to the material
 *  \return Map of parameters and associated values
 *  \throw runtime_error index is not known
 */
std::multimap<std::string, double> get_parameter_map (const int index)
{
    if (index == 0) {
        return std::multimap<std::string, double> {
            {"char_length", 1.5E-11},
            {"mol_diff", 2.E-9},
            {"hydrodynamic_disp_xx", 2.E-8},
            {"hydrodynamic_disp_yy", 2.E-8},
            {"hydrodynamic_disp_xy", 0},
            {"hydrodynamic_disp_yx", 0}
        };
    }
    else if (index == 1) {
        return std::multimap<std::string, double> {
            {"char_length", 1.5E-11},
            {"mol_diff", 2.E-9},
            {"gamma", 0.8},
            {"alpha", 1.17}
        };
    }
    else if (index == 2) {
        return std::multimap<std::string, double> {
            {"char_length", 1.5E-11},
            {"mol_diff", 2.E-9},
            {"eff_diff", 2.E-8},
            {"eff_hydromechanic_disp_xx", 2.E-8},
            {"eff_hydromechanic_disp_yy", 2.E-8},
            {"eff_hydromechanic_disp_xy", 0},
            {"eff_hydromechanic_disp_yx", 0}
        };
    }
    else if (index == 3) {
        return std::multimap<std::string, double> {
            {"char_length", 1.5E-11},
            {"mol_diff", 2.E-9},
            {"phi", 0.34},
            {"lambda_t", 0.0025},
            {"lambda_l", 0.025}
        };
    }
    else if (index == 4) {
        return std::multimap<std::string, double> {
            {"char_length", 1.5E-11},
            {"mol_diff", 2.E-9},
            {"phi", 0.34},
            {"eff_hydromechanic_disp_xx", 2.E-8},
            {"eff_hydromechanic_disp_yy", 2.E-8},
            {"eff_hydromechanic_disp_xy", 0},
            {"eff_hydromechanic_disp_yx", 0}
        };
    }
    else {
        throw std::runtime_error("Unknown index " + std::to_string(index));
    }
}

template<typename ParamMap, typename ParamGetter>
bool compare_parameter_maps(const ParamMap& param_map, const ParamGetter& param_getter)
{
    bool pass = true;
    for (const auto& [index,param] : param_map)
    {
        pass &= compare_maps(param->parameters(),param_getter(index));
    }
    return pass;
}

/// Test the parameters and parameterization functions
/** The standard parameters for Sand and Silt are hardcoded here.
 *  We check the actual values and then the return values of the
 *  parameterization fuctions.
 *  \param sparam Dune::Dorie::SoluteParameters object
 *  \param grid Shared pointer to the grid
 */
template<class SoluteParameters, class Grid>
void test_new_parameters (const SoluteParameters& sparam,
                                std::shared_ptr<Grid> grid)
{
    // iterate over grid cells and verify parameterization
    auto level_gv = grid->leafGridView();
    for (auto&& cell: elements(level_gv))
    {
        constexpr int dim = Grid::dimension;
        using Scalar = Dune::FieldVector<double,1>;
        using Vector = Dune::FieldVector<double,dim>;
        using Tensor = Dune::FieldMatrix<double,dim,dim>;

        // bind parameterization to cell and fetch parameters
        sparam.bind(cell);
        auto par = sparam.cache();
        auto name = par->get_name();
        auto parameters = par->parameters();

        Scalar   wc(0.5);
        Vector   flux(0.);
        flux[dim-1] = -1e-8;
        flux[0] = 1e-8;
        auto fluxn = flux.two_norm();
        // Compute peclet and dispersion with some parameters
        Tensor disp = sparam.hydrodynamic_dispersion_f()(flux,wc);
        Scalar pe = sparam.peclet_f()(flux,wc);

        namespace FloatCmp = Dune::FloatCmp;

        // compare values to defined parameter sets
        if (name == "material_0") { // const hd disp
            assert(compare_maps(get_parameter_map(0), parameters));
            
            for (int i = 0; i < dim; ++i)
                for (int j = 0; j < dim; ++j)
                    if (i==j){
                        assert(FloatCmp::eq(disp[i][j],2.E-8));
                    }
                    else
                        assert(FloatCmp::eq(disp[i][j],0.));
            auto char_len = parameters.find("char_length")->second;
            auto mol_diff = parameters.find("mol_diff")->second;
            assert(FloatCmp::eq(pe,char_len*fluxn/(mol_diff*wc)));
        } 
        else if (name == "material_1") // power_law hd disp
        {
            assert(compare_maps(get_parameter_map(1), parameters));

            auto alpha = parameters.find("alpha")->second;
            auto gamma = parameters.find("gamma")->second;
            auto mol_diff = parameters.find("mol_diff")->second;

            auto _disp = mol_diff*gamma*std::pow(pe,alpha);
            
            for (int i = 0; i < dim; ++i)
                for (int j = 0; j < dim; ++j)
                    if (i==j)
                        assert(FloatCmp::eq(disp[i][j],_disp));
                    else
                        assert(FloatCmp::eq(disp[i][j],0.));

            auto char_len = parameters.find("char_length")->second;
            assert(FloatCmp::eq(pe,char_len*fluxn/(mol_diff*wc)));
        }
        else if (name == "material_2") // const diff + const hm disp
        {
            assert(compare_maps(get_parameter_map(2), parameters));
            for (int i = 0; i < dim; ++i)
                for (int j = 0; j < dim; ++j)
                    if (i==j)
                        assert(FloatCmp::eq(disp[i][j],2*2.E-8));
                    else
                        assert(FloatCmp::eq(disp[i][j],0.));

            auto char_len = parameters.find("char_length")->second;
            auto mol_diff = parameters.find("mol_diff")->second;
            assert(FloatCmp::eq(pe,char_len*fluxn/(mol_diff*wc)));
        } 
        else if (name == "material_3") // MC1 + iso
        {
            assert(compare_maps(get_parameter_map(3), parameters));

            // MC1
            auto mol_diff = parameters.find("mol_diff")->second;
            auto porosity = parameters.find("phi")->second;
            auto diff = mol_diff*std::pow(wc,7./3.)/std::pow(porosity,2./3.);

            // iso
            auto lt = parameters.find("lambda_t")->second;
            auto ll = parameters.find("lambda_l")->second;
            Tensor _disp(0.);
            for (int i = 0; i < dim; ++i) {
                for (int j = 0; j < dim; ++j) {
                    _disp[i][j] += (ll-lt)*flux[i]*flux[j]/(fluxn*wc);
                    if (i==j)
                        _disp[i][j] += lt*fluxn/wc + diff;
                }
            }

            for (int i = 0; i < dim; ++i)
                for (int j = 0; j < dim; ++j)
                        assert(FloatCmp::eq(disp[i][j],_disp[i][j]));

            auto char_len = parameters.find("char_length")->second;
            
            assert(FloatCmp::eq(pe,char_len*fluxn/(mol_diff*wc)));
        }
        else if (name == "material_4") // MC2 + const hm disp
        {
            assert(compare_maps(get_parameter_map(4), parameters));

            // MC2
            auto mol_diff = parameters.find("mol_diff")->second;
            auto porosity = parameters.find("phi")->second;
            auto diff = mol_diff*wc/std::pow(porosity,2./3.);

            // const hm disp
            for (int i = 0; i < dim; ++i)
                for (int j = 0; j < dim; ++j)
                    if (i==j)
                        assert(FloatCmp::eq(disp[i][j],double(diff)+2.E-8));
                    else
                        assert(FloatCmp::eq(disp[i][j],0.));

            auto char_len = parameters.find("char_length")->second;
            
            assert(FloatCmp::eq(pe,char_len*fluxn/(mol_diff*wc)));
        } 
        else 
        {
            DUNE_THROW(Dune::IOError,"Transport parameterization has an error");
        }
    }
}

/// Check if parameters can be manipulated
/** Manipulate parameters using the `parameters()` map or
 *  a dynamic cast to the derived class.
 *  \param sparam Dune::Dorie::SoluteParameters object
 *  \param grid Shared pointer to the grid
 */
template<class SoluteParameters, class Grid>
void test_parameter_manipulation (SoluteParameters& sparam,
                                  std::shared_ptr<Grid> grid)
{
    // bind to any cell
    auto level_gv = grid->leafGridView();
    const auto& cell = *elements(level_gv).begin();
    sparam.bind(cell);

    // manipulate via returned map (this is the preferred way)
    auto par = sparam.cache();
    auto parameters = par->parameters();

    // modify all keys
    for (auto it : parameters)
        it.second = 0.;

    { // modify specific key
        auto [begin,end] = parameters.equal_range("mol_diff");
        for (auto p = begin; p != end; ++p)
            p->second = 2.5E-9;
        assert(par->_mol_diff.value == 2.5E-9);
    }
    { // modify specific key
        auto [begin,end] = parameters.equal_range("char_length");
        for (auto p = begin; p != end; ++p)
            p->second = 1E-11;
        assert(par->_char_length.value == 1E-11);
    }

}

template<class SoluteParameters, class Grid>
bool compare_parameters (const SoluteParameters& sparam1,
                         const SoluteParameters& sparam2,
                         const std::shared_ptr<Grid>& grid)
{
    // iterate over grid cells and verify parameterization
    auto level_gv = grid->leafGridView();
    for (auto&& cell: elements(level_gv))
    {
        // bind parameterization to cell and fetch parameters
        sparam1.bind(cell);
        sparam2.bind(cell);
        auto par1 = sparam1.cache();
        auto par2 = sparam2.cache();
        auto parameters1 = par1->parameters();
        auto parameters2 = par2->parameters();

        // compare values between the two parameterizations
        if (not compare_maps(parameters1, parameters2))
            return false;
    }
    return true;
}

int main (int argc, char** argv)
{
    try{
        // Initialize ALL the things!
        auto [inifile, log, helper] = Dune::Dorie::Setup::init(argc, argv);

        // create the grid
        Dune::Dorie::GridCreator<Traits<2>::Grid> gc(inifile, helper);
        auto grid = gc.grid();
        const auto index_map = gc.element_index_map();

        inifile = Dune::Dorie::Setup::prep_ini_for_transport(inifile);

        // create the Richards logger because SoluteParameters expects it
        const auto log_level = inifile.get<std::string>("output.logLevel");
        log = Dune::Dorie::create_logger(Dune::Dorie::log_transport,
                                         helper,
                                         spdlog::level::from_str(log_level));

        const Dune::Dorie::SoluteParameters<Traits<2>> const_param(inifile,
                                                                   grid,
                                                                   index_map);
        Dune::Dorie::SoluteParameters<Traits<2>> param(const_param); // deep copy
        // perform the actual tests
        test_new_parameters(const_param, grid);
        test_new_parameters(param, grid);

        assert(compare_parameters(const_param, param, grid));
        test_parameter_manipulation(param, grid);
        assert(not compare_parameters(const_param, param, grid));

        return 0;
    }

    catch (Dune::Exception &e){
        std::cerr << "Dune reported error: " << e << std::endl;
        return 1;
    }
    catch (std::exception &e) {
        std::cerr << "Exception thrown: " << e.what() << std::endl;
        return 1;
    }
    catch(...) {
        std::cerr << "Exception occurred!" << std::endl;
        throw;
    }
}