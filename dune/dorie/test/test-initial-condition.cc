#include <gtest/gtest.h>

#include <vector>
#include <memory>
#include <string>

#ifdef HAVE_CONFIG_H
    #include "config.h"
#endif

#include <dune/common/fvector.hh>

#include <dune/grid/yaspgrid.hh>
#include <dune/grid/common/gridview.hh>

#include <dune/dorie/common/setup.hh>
#include <dune/dorie/common/grid_creator.hh>
#include <dune/common/float_cmp.hh>

#include <dune/dorie/model/richards/flow_parameters.hh>
#include <dune/dorie/model/richards/initial_condition.hh>
#include <dune/dorie/model/transport/initial_condition.hh>

// Command line arguments
int argc = 0;
char **argv;

/// Traits stub used for this test
template<int d>
struct Traits
{
    static constexpr int dim = d;
    using Grid = Dune::YaspGrid<dim>;
    using GridView = typename Grid::LeafGridView;
    using GV = GridView;
    using DF = typename Grid::ctype;
    using Domain = Dune::FieldVector<DF, dim>;

    using RF = double;
    using Scalar = Dune::FieldVector<RF, 1>;
    using Vector = Dune::FieldVector<RF, dim>;
};

/// Test an initial condition against a lambda function on the grid
template<typename GridView, class IC, class Function>
void test_ic_against_function (
    const std::shared_ptr<GridView> gv,
    const std::shared_ptr<IC>& ic,
    const Function func
)
{
    using Range = typename IC::Traits::RangeType;

    // iterate over the grid vertices
    for (auto&& e : Dune::elements(*gv))
    {
        Range y;
        const auto x = e.geometry().local(e.geometry().center());
        ic->evaluate(e, x, y);
        EXPECT_DOUBLE_EQ(func(e, x), y);
    }
}

/// Test the basic initial conditions (without transformation)
/** \tparam T Traits
 */
template<typename T>
void test_ic_base (const std::shared_ptr<Dune::Dorie::InitialCondition<T>> ic,
                   const std::shared_ptr<typename T::GridView> gv,
                   const Dune::ParameterTree& config)
{
    const auto type = config.get<std::string>("initial.type");
    if (type == "analytic") {
        // define testing function
        const auto equation = config.get<std::string>("initial.equation");
        EXPECT_EQ(equation, "-1+2*y");
        auto test_f = [](const auto e, const auto x){
            using Range = typename T::Scalar;
            constexpr int dim = T::Grid::dimension;
            const auto x_global = e.geometry().global(x);
            Range y = -1.0 + x_global[dim-1] * 2.0;
            return y;
        };
        test_ic_against_function(gv, ic, test_f);
    }
    else if (type == "data") {
        // define testing function
        auto test_f = [](const auto e, const auto x){
            using Range = typename T::Scalar;
            Range y = 2.0;
            return y;
        };
        test_ic_against_function(gv, ic, test_f);
    }
    else {
        FAIL() << "IC input combination not covered by test!";
    }
}

/// Test the water content initial conditions (with transformation)
/** \tparam T Traits
 */
template<typename T>
void test_ic_wc (const std::shared_ptr<Dune::Dorie::InitialCondition<T>> ic,
                 const std::shared_ptr<typename T::GridView> gv,
                 const Dune::ParameterTree& config)
{
    const auto type = config.get<std::string>("initial.type");
    if (type == "data") {
        // define testing function
        auto test_f = [](const auto e, const auto x){
            using Range = typename T::Scalar;
            Range y = 0;
            return y;
        };
        test_ic_against_function(gv, ic, test_f);
    }
    else {
        FAIL() << "IC input combination not covered by test!";
    }
}

class ICFixture : public ::testing::Test
{
protected:
    using T = Traits<2>;
    using Grid = typename T::Grid;
    using GridView = typename Grid::Traits::LeafGridView;
    using RichardsParam = Dune::Dorie::FlowParameters<T>;

    Dune::ParameterTree _config;
    Dune::ParameterTree _config_richards;
    Dune::ParameterTree _config_transport;
    std::shared_ptr<spdlog::logger> _log;
    std::shared_ptr<Grid> _grid;
    std::shared_ptr<GridView> _gv;
    std::vector<int> _index_map;

    /// Update the stored grid view and return a new parameterization instance
    std::shared_ptr<RichardsParam> create_param()
    {
      _gv = std::make_shared<GridView>(_grid->leafGridView());
      return std::make_shared<RichardsParam>(
        _config_richards, _grid, _index_map);
    }

    void SetUp () override
    {
        // Initialize configs
        using namespace Dune::Dorie::Setup;
        auto [cfg, log, helper] = init(argc, argv);
        log.reset();  // Avoid warning of unused variable
        _config = cfg;
        _config_richards = prep_ini_for_richards(_config);
        _config_transport = prep_ini_for_transport(_config);

        // Initialize loggers
        auto log_level = _config_richards.get<std::string>("output.logLevel");
        Dune::Dorie::create_logger(Dune::Dorie::log_richards,
                                   helper,
                                   spdlog::level::from_str(log_level)
        );
        log_level = _config_transport.get<std::string>("output.logLevel");
        Dune::Dorie::create_logger(Dune::Dorie::log_transport,
                                   helper,
                                   spdlog::level::from_str(log_level)
        );

        // Create the grid
        Dune::Dorie::GridCreator<Grid> gc(_config, helper);
        _grid = gc.grid();
        _gv = std::make_shared<GridView>(_grid->leafGridView());
        _index_map = gc.element_index_map();
    }

    void TearDown () override
    {
        // Remove the loggers
        spdlog::drop_all();
    }
};

TEST_F (ICFixture, Richards)
{
    // Create Richards parameterization
    auto param = create_param();
    // Create the initial condition
    using ICF = Dune::Dorie::RichardsInitialConditionFactory<T, RichardsParam>;
    using IC = Dune::Dorie::InitialCondition<T>;
    const std::shared_ptr<IC> ic = ICF::create(_config_richards, *_gv, param);

    const auto quantity = _config_richards.get<std::string>(
        "initial.quantity"
    );
    if (quantity == "matricHead") {
        test_ic_base<T>(ic, _gv, _config_richards);
    }
    else if (quantity == "waterContent") {
        test_ic_wc<T>(ic, _gv, _config_richards);
    }
    else {
        FAIL() << "Initial condition quantity not covered by test!";
    }
}

TEST_F (ICFixture, Transport)
{
    using ICF = Dune::Dorie::TransportInitialConditionFactory<T>;
    using IC = Dune::Dorie::InitialCondition<T>;
    const std::shared_ptr<IC> ic = ICF::create(_config_transport, *_gv);

    const auto quantity = _config_transport.get<std::string>(
        "initial.quantity"
    );
    if (quantity == "soluteConcentration") {
        test_ic_base<T>(ic, _gv, _config_transport);
    }
    else {
        FAIL() << "Initial condition quantity not covered by test!";
    }
}

// --- Main Function --- //

int main(int argc_t, char **argv_t)
{
    // Parse input arguments
    ::testing::InitGoogleTest(&argc_t, argv_t);
    argc = argc_t;
    argv = argv_t;

    return RUN_ALL_TESTS();
}
