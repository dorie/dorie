#ifdef HAVE_CONFIG_H
    #include "config.h"
#endif

#include <cassert>
#include <exception>
#include <vector>
#include <cmath>
#include <numeric>
#include <iostream>

#include <dune/common/exceptions.hh>
#include <dune/common/float_cmp.hh>
#include <dune/common/parallel/mpihelper.hh>
#include <dune/common/fvector.hh>

#include <dune/dorie/common/interpolator.hh>

template<int dimension>
struct InterpolatorTraits
{
    static constexpr int dim = dimension;
    using Domain = Dune::FieldVector<double, dim>;
    using DF = double;
    using RF = double;
};

/// Test the nearest neighbor interpolator.
/** 
 *  * Create a 3x3 grid with extensions 3x3.
 *  * Offset by -1x-1.
 *  * Check values.
 */
template<int dim=2>
void test_nearest_neighbor ()
{
    std::vector<double> data(std::pow(3, dim));
    std::iota(begin(data), end(data), 0.0);

    std::vector<size_t> shape(dim);
    std::fill(begin(shape), end(shape), 3);

    Dune::FieldVector<double, dim> extensions(3.0);
    // std::fill(begin(extensions), end(extensions), 3.0);

    Dune::FieldVector<double, dim> offset(0.0);
    // std::fill(begin(offset), end(offset), 0.0);

    // build interpolator
    auto interp = Dune::Dorie::InterpolatorFactory<InterpolatorTraits<dim>>
        ::create("nearest",
                 data,
                 shape,
                 extensions,
                 offset);
    
    // check without offset
    using Dune::FloatCmp::eq; // floating-point comparison
    std::vector<Dune::FieldVector<double, dim>> corners;
    if constexpr (dim == 2) {
        corners.resize(4);
        corners[0] = {0.0, 0.0};
        corners[1] = {1.0, 0.0};
        corners[2] = {0.0, 1.0};
        corners[3] = {1.0, 1.0};
        assert(eq(interp->evaluate(corners[0]), 0.0));
        assert(eq(interp->evaluate(corners[1]), 1.0));
        assert(eq(interp->evaluate(corners[2]), 3.0));
        assert(eq(interp->evaluate(corners[3]), 4.0));
    }
    else if (dim == 3) {
        corners.resize(8);
        corners[0] = {0.0, 0.0, 0.0};
        corners[1] = {1.0, 0.0, 0.0};
        corners[2] = {0.0, 1.0, 0.0};
        corners[3] = {1.0, 1.0, 0.0};
        corners[4] = {0.0, 0.0, 1.0};
        corners[5] = {1.0, 0.0, 1.0};
        corners[6] = {0.0, 1.0, 1.0};
        corners[7] = {1.0, 1.0, 1.0};
        assert(eq(interp->evaluate(corners[0]), 0.0));
        assert(eq(interp->evaluate(corners[1]), 1.0));
        assert(eq(interp->evaluate(corners[2]), 3.0));
        assert(eq(interp->evaluate(corners[3]), 4.0));
        assert(eq(interp->evaluate(corners[4]), 9.0));
        assert(eq(interp->evaluate(corners[5]), 10.0));
        assert(eq(interp->evaluate(corners[6]), 12.0));
        assert(eq(interp->evaluate(corners[7]), 13.0));
    }

    // check with offset
    std::fill(offset.begin(), offset.end(), -1.0);
    interp = Dune::Dorie::InterpolatorFactory<InterpolatorTraits<dim>>
        ::create("nearest",
                 data,
                 shape,
                 extensions,
                 offset);

    if constexpr (dim == 2) {
        corners.resize(4);
        corners[0] = {0.0, 0.0};
        corners[1] = {1.0, 0.0};
        corners[2] = {0.0, 1.0};
        corners[3] = {1.0, 1.0};
        assert(eq(interp->evaluate(corners[0]), 4.0));
        assert(eq(interp->evaluate(corners[1]), 5.0));
        assert(eq(interp->evaluate(corners[2]), 7.0));
        assert(eq(interp->evaluate(corners[3]), 8.0));
    }
    else if (dim == 3) {
        corners.resize(8);
        corners[0] = {0.0, 0.0, 0.0};
        corners[1] = {1.0, 0.0, 0.0};
        corners[2] = {0.0, 1.0, 0.0};
        corners[3] = {1.0, 1.0, 0.0};
        corners[4] = {0.0, 0.0, 1.0};
        corners[5] = {1.0, 0.0, 1.0};
        corners[6] = {0.0, 1.0, 1.0};
        corners[7] = {1.0, 1.0, 1.0};
        assert((interp->evaluate(corners[0]), 13.0));
        assert((interp->evaluate(corners[1]), 14.0));
        assert((interp->evaluate(corners[2]), 16.0));
        assert((interp->evaluate(corners[3]), 17.0));
        assert((interp->evaluate(corners[4]), 22.0));
        assert((interp->evaluate(corners[5]), 23.0));
        assert((interp->evaluate(corners[6]), 25.0));
        assert((interp->evaluate(corners[7]), 26.0));
    }
}

/// Test the linear interpolator
template<int dim>
void test_linear ();

/// Test the linear interpolator in 2D
template<>
void test_linear<2> ()
{
    constexpr int dim = 2;
    std::vector<double> data({0.0, 0.0, 1.0,
                              1.0, 1.0, 2.0,
                              3.0, 3.0, 4.0});

    std::vector<size_t> shape(dim);
    std::fill(begin(shape), end(shape), 3);

    Dune::FieldVector<double, dim> extensions(1.0);
    Dune::FieldVector<double, dim> offset(0.0);

    // build interpolator
    auto interp = Dune::Dorie::InterpolatorFactory<InterpolatorTraits<dim>>
        ::create("linear",
                 data,
                 shape,
                 extensions,
                 offset);
    
    // check without offset
    using Dune::FloatCmp::eq; // floating-point comparison
    std::vector<Dune::FieldVector<double, dim>> points(7);

    points[0] = {0.0, 0.0};
    points[1] = {1.0, 0.0};
    points[2] = {0.0, 1.0};
    points[3] = {1.0, 1.0};
    points[4] = {0.5, 0.5};
    points[5] = {0.25, 0.5};
    points[6] = {1.0, 0.75};
    assert(eq(interp->evaluate(points[0]), 0.0));
    assert(eq(interp->evaluate(points[1]), 1.0));
    assert(eq(interp->evaluate(points[2]), 3.0));
    assert(eq(interp->evaluate(points[3]), 4.0));
    assert(eq(interp->evaluate(points[4]), 1.0));
    assert(eq(interp->evaluate(points[5]), 1.0));
    assert(eq(interp->evaluate(points[6]), 3.0));

    // check with offset
    std::fill(offset.begin(), offset.end(), -0.5);
    interp = Dune::Dorie::InterpolatorFactory<InterpolatorTraits<dim>>
        ::create("linear",
                 data,
                 shape,
                 extensions,
                 offset);

    assert(eq(interp->evaluate(points[0]), 1.0));
    assert(eq(interp->evaluate(points[5]), 3.5));
}

/// Test the linear interpolator in 3D
template<>
void test_linear<3> ()
{
    constexpr int dim = 3;
    std::vector<double> data({0.0, 0.0, 0.0, 0.0,
                              1.0, 1.0, 2.0, 2.0});

    std::vector<size_t> shape(dim);
    std::fill(begin(shape), end(shape), 2);

    Dune::FieldVector<double, dim> extensions(2.0);
    Dune::FieldVector<double, dim> offset(0.0);

    // build interpolator
    auto interp = Dune::Dorie::InterpolatorFactory<InterpolatorTraits<dim>>
        ::create("linear",
                 data,
                 shape,
                 extensions,
                 offset);

    // check without offset
    using Dune::FloatCmp::eq; // floating-point comparison
    std::vector<Dune::FieldVector<double, dim>> points(5);

    points[0] = {0.0, 0.0, 0.0};
    points[1] = {2.0, 0.0, 0.0};
    points[2] = {0.0, 0.0, 2.0};
    points[3] = {2.0, 2.0, 2.0};
    points[4] = {1.0, 1.0, 2.0};
    assert(eq(interp->evaluate(points[0]), 0.0));
    assert(eq(interp->evaluate(points[1]), 0.0));
    assert(eq(interp->evaluate(points[2]), 1.0));
    assert(eq(interp->evaluate(points[3]), 2.0));
    assert(eq(interp->evaluate(points[4]), 1.5));

    // check with offset
    std::fill(offset.begin(), offset.end(), -2.0);
    interp = Dune::Dorie::InterpolatorFactory<InterpolatorTraits<dim>>
        ::create("linear",
                 data,
                 shape,
                 extensions,
                 offset);

    points.resize(2);
    points[0] = {0.0, 0.0, 0.0};
    points[1] = {-1.0, -1.0, 0.0};
    assert(eq(interp->evaluate(points[0]), 2.0));
    assert(eq(interp->evaluate(points[1]), 1.5));
}

int main (int argc, char** argv)
{
    try{
        // initialize MPI if needed
        auto& helper = Dune::MPIHelper::instance(argc, argv);
        auto log = Dune::Dorie::create_base_logger(helper);
        log->set_level(spdlog::level::trace);

        // test the NearestNeighbor interpolator
        test_nearest_neighbor<2>();
        test_nearest_neighbor<3>();

        test_linear<2>();
        test_linear<3>();
    }

    catch (Dune::Exception &e) {
        std::cerr << "Dune reported error: " << e << std::endl;
        return 1;
    }
    catch (std::exception &e) {
        std::cerr << "Exception thrown: " << e.what() << std::endl;
        return 1;
    }
    catch (...) {
        std::cerr << "Unknown exception!" << std::endl;
        return 1;
    }
}
