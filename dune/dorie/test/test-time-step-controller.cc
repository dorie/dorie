#include <gtest/gtest.h>

#ifdef HAVE_CONFIG_H
    #include "config.h"
#endif

#include <dune/common/float_cmp.hh>

#include <dune/dorie/common/setup.hh>
#include <dune/dorie/common/logging.hh>
#include <dune/dorie/common/time_step_controller.hh>

int argc = 0;
char **argv;

template<typename T>
std::unique_ptr<Dune::Dorie::TimeStepController<T>> initialize (
    const Dune::ParameterTree& config,
    const std::shared_ptr<spdlog::logger> log
)
{
    return std::make_unique<Dune::Dorie::TimeStepController<T>>(config, log);
}

class ControllerFixture : public ::testing::Test
{
    using TF = double;

protected:
    Dune::ParameterTree config;
    std::shared_ptr<spdlog::logger> log;
    std::unique_ptr<Dune::Dorie::TimeStepController<TF>> ctrl;

    TF dt;
    TF dt_inc;
    TF dt_dec;
    TF dt_min;
    TF dt_max;
    TF t_begin;
    TF t_end;

    void SetUp() override
    {
        auto [cfg, lg, helper] = Dune::Dorie::Setup::init(argc, argv);
        config = Dune::Dorie::Setup::prep_ini_for_richards(cfg);
        log = lg;
        log->debug("MPI helper is fake: ", helper.isFake);
        ctrl = initialize<TF>(config, log);

        dt = config.template get<TF>("time.startTimestep");
        dt_inc = config.template get<TF>("time.timestepIncreaseFactor");
        dt_dec = config.template get<TF>("time.timestepDecreaseFactor");
        dt_min = config.template get<TF>("time.minTimestep");
        dt_max = config.template get<TF>("time.maxTimestep");
        t_begin = config.template get<TF>("time.start");
        t_end = config.template get<TF>("time.end");
    }

    void TearDown () override
    {
        // remove the logger
        spdlog::drop(Dune::Dorie::log_base);
    }
};

class InputSanityCheck : public ::testing::Test
{
protected:
    Dune::ParameterTree config;
    std::shared_ptr<spdlog::logger> log;

    void SetUp() override
    {
        auto [cfg, lg, helper] = Dune::Dorie::Setup::init(argc, argv);
        config = Dune::Dorie::Setup::prep_ini_for_richards(cfg);
        log = lg;
        log->debug("MPI helper is fake: ", helper.isFake);
    }

    void TearDown () override
    {
        // remove the logger
        spdlog::drop(Dune::Dorie::log_base);
    }
};

// --- Controller Tests --- //

TEST_F (ControllerFixture, Initialize)
{
    EXPECT_DOUBLE_EQ(ctrl->get_time_step(), dt);
    EXPECT_DOUBLE_EQ(ctrl->get_time_step_min(), dt_min);
    EXPECT_DOUBLE_EQ(ctrl->get_time_step_max(), dt_max);
    EXPECT_DOUBLE_EQ(ctrl->get_time_begin(), t_begin);
    EXPECT_DOUBLE_EQ(ctrl->get_time_end(), t_end);
}

TEST_F (ControllerFixture, IncrementStep)
{
    ctrl->increase_time_step();
    EXPECT_DOUBLE_EQ(dt * dt_inc, ctrl->get_time_step());

    ctrl->increase_time_step(2.0);
    EXPECT_DOUBLE_EQ(dt * dt_inc * 2.0, ctrl->get_time_step());
}

TEST_F (ControllerFixture, DecrementStep)
{
    ctrl->decrease_time_step(0.5);
    EXPECT_DOUBLE_EQ(dt * 0.5, ctrl->get_time_step());

    ctrl->decrease_time_step();
    EXPECT_DOUBLE_EQ(dt * 0.5 * dt_dec, ctrl->get_time_step());
}

TEST_F (ControllerFixture, StepSuggestion)
{
    auto dt_new = ctrl->get_time_step() / 2.0;
    EXPECT_DOUBLE_EQ(dt_new, ctrl->suggest_time_step(dt_new));
    EXPECT_DOUBLE_EQ(dt_new, ctrl->get_time_step(0.0));

    // check that controller avoids time steps < dt_min
    // Note: Current time step is smaller than suggestion, so suggestion
    //       would normally be rejected
    dt_new = ctrl->get_time_step() + dt_min;
    EXPECT_DOUBLE_EQ(dt_new, ctrl->suggest_time_step(dt_new));

    // check that controller does not accept very low suggestion
    log->info("Expecting a warning: 'Suggestion below minimal time step:'");
    dt_new = dt_min / 10.0;
    EXPECT_DOUBLE_EQ(dt_min, ctrl->suggest_time_step(dt_new));
}

TEST_F (ControllerFixture, SetStep)
{
    EXPECT_DOUBLE_EQ(dt * 2, ctrl->set_time_step(dt * 2));

    // check clamping
    EXPECT_DOUBLE_EQ(dt_min, ctrl->set_time_step(dt_min / 10.0));
    EXPECT_DOUBLE_EQ(dt_max, ctrl->set_time_step(dt_max * 10.0));

    // check forcing
    const auto dt_new = dt_max * 10.0;
    EXPECT_DOUBLE_EQ(dt_new, ctrl->set_time_step(dt_new, true));
}

TEST_F (ControllerFixture, TimeEndAdjustment)
{
    // check adjustment to time end
    ctrl->set_time_step(dt_max);
    EXPECT_DOUBLE_EQ(1.0, ctrl->get_time_step(t_end - 1.0));

    // check skipping adjustment to time end
    ctrl->increase_time_step();
    const auto dt = ctrl->get_time_step();
    EXPECT_DOUBLE_EQ(dt, ctrl->get_time_step(t_end + 1.0));
}

// --- Test Input Sanity Check --- //

TEST_F (InputSanityCheck, TimeStepIncrement)
{
    config["time.timestepIncreaseFactor"] = "0.9";
    EXPECT_THROW(initialize<double>(config, log), Dune::IOError);
}

TEST_F (InputSanityCheck, TimeStepDecrement)
{
    config["time.timestepDecreaseFactor"] = "1.1";
    EXPECT_THROW(initialize<double>(config, log), Dune::IOError);
}

TEST_F (InputSanityCheck, TimeStepLimits)
{
    const auto dt_min = config.template get<double>("time.minTimestep");
    config["time.maxTimestep"] = std::to_string(dt_min * 0.5);
    EXPECT_THROW(initialize<double>(config, log), Dune::IOError);
}

TEST_F (InputSanityCheck, TimeStepValid)
{
    const auto dt_min = config.template get<double>("time.minTimestep");
    config["time.startTimestep"] = std::to_string(dt_min * 0.5);
    EXPECT_THROW(initialize<double>(config, log), Dune::IOError);
}

// --- Main Function --- //

int main(int argc_t, char **argv_t)
{
    // Parse input arguments
    ::testing::InitGoogleTest(&argc_t, argv_t);
    argc = argc_t;
    argv = argv_t;

    return RUN_ALL_TESTS();
}
