import h5py
import numpy as np
import os
import argparse


def create_and_write_datasets(args):
    zeros = np.zeros((2, 2))
    ones = np.ones((2, 2))
    twos = np.ones((2, 2)) * 2

    shift = np.zeros((2, 1))
    shift[1, ...] = 1

    porosity = np.ones((2, 2)) * 0.1

    f = h5py.File(args.path, "w")
    f.create_dataset("zeros", data=zeros)
    f.create_dataset("ones", data=ones)
    f.create_dataset("twos", data=twos)
    f.create_dataset("shift", data=shift)
    f.create_dataset("porosity", data=porosity)

    f.close()


def parse_cli():
    parser = argparse.ArgumentParser(
        description="Write parameter mapping datasets for all tests"
    )
    parser.add_argument(
        "path", type=os.path.realpath, help="File path to write the data"
    )
    return parser.parse_args()


if __name__ == "__main__":
    args = parse_cli()
    create_and_write_datasets(args)
