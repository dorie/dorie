#ifdef HAVE_CONFIG_H
  #include "config.h"
#endif

#include <dune/dorie/common/grid_function_container.hh>

#include <dune/pdelab/function/const.hh>

#include <dune/grid/yaspgrid.hh>
#include <dune/grid/utility/structuredgridfactory.hh>
#include <dune/grid/io/file/vtk/vtkwriter.hh>

#include <dune/common/filledarray.hh>
#include <dune/common/exceptions.hh>

template<class GFContainer, class GV>
bool test_container(GV& gv)
{
  bool failed = false;

  using GF = typename GFContainer::GridFunctionType;
  std::shared_ptr<GF> gf0 = std::make_shared<GF>(gv,0.);
  std::shared_ptr<GF> gf1 = std::make_shared<GF>(gv,1.);
  std::shared_ptr<GF> gf2 = std::make_shared<GF>(gv,2.);
  std::shared_ptr<GF> gf3 = std::make_shared<GF>(gv,3.);

  GFContainer gf_container;

  failed |= (gf_container.size() != 0);

  // check queue capabilities
  gf_container.push(gf0,0.0);
  try {
    gf_container.push(gf0,-1.0);
    failed |= true;
  } catch (...) {}
  try {
    gf_container.push(gf0,0.0);
    failed |= true;
  } catch (...) {}

  // retreive grid view
  gf_container.getGridView();

  gf_container.push(gf1,1.0);
  gf_container.push({gf0,2.0});
  gf_container.push(std::pair<std::shared_ptr<GF>,double>(gf2,3.0));
  gf_container.push(*gf0,4.0);
  try {
    gf_container.push(gf2,3.0);
    failed |= true;
  } catch (...) {}

  failed |= (gf_container.size() != 5);

  gf_container.pop();
  if (Dune::FloatCmp::ne(gf_container.front().second,1.0))    
    failed |= true;
  if (Dune::FloatCmp::ne(gf_container.back().second,4.0))    
    failed |= true;

  failed |= (gf_container.size() != 4);

  gf_container.pop();
  if (Dune::FloatCmp::ne(gf_container.front().second,2.0))    
    failed |= true;
  if (Dune::FloatCmp::ne(gf_container.back().second,4.0))    
    failed |= true;

  gf_container.pop();
  if (Dune::FloatCmp::ne(gf_container.front().second,3.0))    
    failed |= true;
  if (Dune::FloatCmp::ne(gf_container.back().second,4.0))    
    failed |= true;

  gf_container.pop();
  if (Dune::FloatCmp::ne(gf_container.front().second,4.0))    
    failed |= true;
  if (Dune::FloatCmp::ne(gf_container.back().second,4.0))    
    failed |= true;

  gf_container.pop();
  if (not gf_container.empty())    
    failed |= true;

 try {
    // retreive grid view of empty container
    gf_container.getGridView();
    failed |= true;
  } catch (...) {}

  // gf_container.front(); // undefined behavior
  // gf_container.back();  // undefined behavior

  // check instationary grid function capabilities
  gf_container.push(gf0,0.0);
  gf_container.push(gf1,1.0);
  gf_container.push(gf0,2.0);
  gf_container.push(gf2,3.0);
  gf_container.push(gf3,4.0);

  gf_container.setTime(0.0);
  gf_container.setTime(4.0);

  try {
    gf_container.setTime(-1.0);
    failed |= true;
  } catch (...) {}
  try {
    gf_container.setTime(5.0);
    failed |= true;
  } catch (...) {}

  // Obtain the firts entity of the grid
  const auto entity = *(gv.template begin<0>());
  // center of entity in local coordinates
  const auto geo = entity.geometry();
  const auto x = geo.local(geo.center());
  // range type
  typename GF::Traits::RangeType y;

  // check that it maps to the right value when time is "exact"
  gf_container.setTime(0.0);
  gf_container.evaluate(entity,x,y);
  if (not Dune::FloatCmp::eq(y[0],0.0))
    failed |= true;

  gf_container.setTime(2.0);
  gf_container.evaluate(entity,x,y);
  if (not Dune::FloatCmp::eq(y[0],0.0))
    failed |= true;

  gf_container.setTime(4.0);
  gf_container.evaluate(entity,x,y);
  if (not Dune::FloatCmp::eq(y[0],3.0))
    failed |= true;

  // evaluate a dropped grid function should rise an error 
  gf_container.setTime(0.0);
  gf_container.pop();
  try {
    gf_container.evaluate(entity,x,y);
    failed |= true;
  } catch (...) {}

  // try again another valid time
  gf_container.setTime(1.0);
  gf_container.evaluate(entity,x,y);
  if (not Dune::FloatCmp::eq(y[0],1.0))
    failed |= true;

  // shrink to some arbitrary time
  gf_container.setTime(1.5);
  gf_container.shrink_to_time();
  gf_container.evaluate(entity,x,y);
  failed |= gf_container.size()!=2;
  try {
    gf_container.setTime(0.5);
    gf_container.setTime(1.5);
    failed |= true;
  } catch (...) {}

  // shrink to some exact contained time
  gf_container.setTime(1.);
  gf_container.shrink_to_time();
  gf_container.evaluate(entity,x,y);
  failed |= gf_container.size()!=1;
  try {
    gf_container.setTime(0.5);
    gf_container.setTime(1.5);
    failed |= true;
  } catch (...) {}

  // pop until container is empty
  while (not gf_container.empty())
    gf_container.pop();

  // check that container is void
  failed |= gf_container.size() != 0;

  // evaluate an empty container should rise an error 
  try {
    gf_container.evaluate(entity,x,y);
    failed |= true;
  } catch (...) {}

  return failed;
}


int main(int argc, char **argv)
{
  bool failed = false;

  Dune::MPIHelper::instance(argc, argv);

  try {

    static const int dim = 2;
    using GridType = Dune::YaspGrid<dim>;
    using GV = typename GridType::LeafGridView;

    // define the extensions of the domain: a unit square with N by N cells
    Dune::FieldVector<double,dim> lowerleft(0.0);
    Dune::FieldVector<double,dim> upperright(1.0);
    auto N = Dune::filledArray<dim, unsigned int>(15);

    // build a structured grid
    auto grid = Dune::StructuredGridFactory<GridType>
                        ::createCubeGrid(lowerleft, upperright, N);

    // Extract the grid view we would like to use
    GV gv = grid->leafGridView();

    // Test with contant grid function
    using GF = Dune::PDELab::ConstGridFunction<GV,double>;

    // Container type with next step policy
    using GFContainer1 = Dune::Dorie::GridFunctionContainer<
              GF,double,Dune::Dorie::GridFunctionContainerPolicy::NextStep>;

    failed |= test_container<GFContainer1>(gv);

    // Container type with previous step policy
    using GFContainer2 = Dune::Dorie::GridFunctionContainer<
              GF,double,Dune::Dorie::GridFunctionContainerPolicy::PreviousStep>;

    failed |= test_container<GFContainer2>(gv);

    // Container type with linear interpolation policy
    using GFContainer3 = Dune::Dorie::GridFunctionContainer<
              GF,double,Dune::Dorie::GridFunctionContainerPolicy::LinearInterpolation>;

    failed |= test_container<GFContainer3>(gv);

    // test policies
    std::shared_ptr<GF> gf0 = std::make_shared<GF>(gv,0.);
    std::shared_ptr<GF> gf1 = std::make_shared<GF>(gv,1.);

    // Obtain the firts entity of the grid
    const auto entity = *(gv.template begin<0>());
    // center of entity in local coordinates
    const auto geo = entity.geometry();
    const auto x = geo.local(geo.center());
    // range type
    typename GF::Traits::RangeType y;

    const auto eps = 10*std::numeric_limits<double>::epsilon();

    GFContainer1 gf_container_1;

    gf_container_1.push(gf0,0.0);
    gf_container_1.push(gf1,1.0);

    //  o---------o
    //  01111111111

    gf_container_1.setTime(0.0 + eps);
    gf_container_1.evaluate(entity,x,y);
    if (not Dune::FloatCmp::eq(y[0],1.0))
      failed |= true;

    gf_container_1.setTime(1.0 - eps);
    gf_container_1.evaluate(entity,x,y);
    if (not Dune::FloatCmp::eq(y[0],1.0))
      failed |= true;

    GFContainer2 gf_container_2;

    gf_container_2.push(gf0,0.0);
    gf_container_2.push(gf1,1.0);

    //  o---------o
    //  00000000001

    gf_container_2.setTime(0.0 + eps);
    gf_container_2.evaluate(entity,x,y);
    if (not Dune::FloatCmp::eq(y[0],0.0))
      failed |= true;

    gf_container_2.setTime(1.0 - eps);
    gf_container_2.evaluate(entity,x,y);
    if (not Dune::FloatCmp::eq(y[0],0.0))
      failed |= true;


    GFContainer3 gf_container_3;

    gf_container_3.push(gf0,0.0);
    gf_container_3.push(gf1,1.0);

    //  o----------------------o
    //  0xxx linear interp. xxx1

    for (int i = 0; i < 100; ++i)
    {
      double val = i/100.;
      gf_container_3.setTime(val);
      gf_container_3.evaluate(entity,x,y);
      if (not Dune::FloatCmp::eq(y[0],val))
        failed |= true;
    }

  } catch (Dune::Exception &e){
    std::cerr << "Dune reported error: " << e << std::endl;
    failed |= true;
  }

  catch(...) {
    std::cerr << "Exception occurred!" << std::endl;
    failed |= true;
  }
  
  return failed;
}