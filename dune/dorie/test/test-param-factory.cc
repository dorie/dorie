#ifdef HAVE_CONFIG_H
    #include "config.h"
#endif

#include <yaml-cpp/yaml.h>

#include <dune/common/parallel/mpihelper.hh>
#include <dune/common/fvector.hh>
#include <dune/common/fmatrix.hh>

#include <dune/dorie/common/parameterization_factory.hh>

#include "test-parameterization.hh"

/// Return parameter values based on the material index
/** \param index index assigned to the material
 *  \return Map of parameters and associated values
 *  \throw runtime_error index is not known
 */
std::multimap<std::string, double> get_parameter_map (const int index)
{
    if (index == 0) { // scalar
        return std::multimap<std::string, double> {
            {"val", 1.}
        };
    }
    else if (index == 1) { // 2Dvector
        return std::multimap<std::string, double> {
            {"val_x", 1.},
            {"val_y", 2.}
        };
    }
    else if (index == 2) { // 2Dtensor
        return std::multimap<std::string, double> {
            {"val_xx", 1.},
            {"val_xy", 2.},
            {"val_yx", 3.},
            {"val_yy", 4.}
        };
    }
    else if (index == 3) { // 3Dvector
        return std::multimap<std::string, double> {
            {"val_x", 1.},
            {"val_y", 2.},
            {"val_z", 3.}
        };
    }
    else if (index == 4) { // 3Dtensor
        return std::multimap<std::string, double> {
            {"val_xx", 1.},
            {"val_xy", 2.},
            {"val_xz", 3.},
            {"val_yx", 4.},
            {"val_yy", 5.},
            {"val_yz", 6.},
            {"val_zx", 7.},
            {"val_zy", 8.},
            {"val_zz", 9.}
        };
    }
    else {
        throw std::runtime_error("Unknown index " + std::to_string(index));
    }
}

struct Param {
  virtual ~Param() = default;
  virtual std::multimap<std::string, double&> parameters() = 0;
};

struct ParamScalar : public Param {
  std::multimap<std::string, double&> parameters() {return {{"val",val[0]}};}
  Dune::FieldVector<double,1> val;
};

struct Param2DVec : public Param {
  std::multimap<std::string, double&> parameters() {
    return {  {"val_x",val[0]},
              {"val_y",val[1]} };
  }
  Dune::FieldVector<double,2> val;
};

struct Param3DVec : public Param {
  std::multimap<std::string, double&> parameters() {
    return {  {"val_x",val[0]},
              {"val_y",val[1]},
              {"val_z",val[2]} };
  }
  Dune::FieldVector<double,3> val;
};

struct Param2DTensor : public Param {
std::multimap<std::string, double&> parameters() {
    return {  {"val_xx",val[0][0]},
              {"val_xy",val[0][1]},
              {"val_yx",val[1][0]},
              {"val_yy",val[1][1]} };
  }
  Dune::FieldMatrix<double,2,2> val;
};

struct Param3DTensor : public Param {
std::multimap<std::string, double&> parameters() {
    return {  {"val_xx",val[0][0]},
              {"val_xy",val[0][1]},
              {"val_xz",val[0][2]},
              {"val_yx",val[1][0]},
              {"val_yy",val[1][1]},
              {"val_yz",val[1][2]},
              {"val_zx",val[2][0]},
              {"val_zy",val[2][1]},
              {"val_zz",val[2][2]}, };
  }
  Dune::FieldMatrix<double,3,3> val;
};

// Instaintiate factory for Param class
struct ParamFactory  : public Dune::Dorie::ParameterizationFactory<Param>
{
  std::shared_ptr<Param> selector(
    const YAML::Node& type_node,
    const std::string& name) const override
  {
    const auto type = type_node["type"].as<std::string>();
    if (type == "scalar")
      return std::make_shared<ParamScalar>();
    else if (type == "2Dvector")
      return std::make_shared<Param2DVec>();
    else if (type == "2Dtensor")
      return std::make_shared<Param2DTensor>();
    else if (type == "3Dvector")
      return std::make_shared<Param3DVec>();
    else if (type == "3Dtensor")
      return std::make_shared<Param3DTensor>();
    else
      DUNE_THROW(Dune::NotImplemented,
        "Parameterization {" << type << "} is not implemented");
  }
};


template<typename ParamMap, typename ParamGetter>
bool compare_parameter_maps(const ParamMap& param_map, const ParamGetter& param_getter)
{
    bool pass = true;
    for (const auto& [index,param] : param_map)
    {
        pass &= compare_maps(param->parameters(),param_getter(index));
    }
    return pass;
}

int main (int argc, char** argv)
{
    try{
        // initialize MPI if needed
        auto& helper = Dune::MPIHelper::instance(argc, argv);

        // build the logger
        const auto log = Dune::Dorie::create_base_logger(helper,
                                                         spdlog::level::trace);

        const YAML::Node param_file = YAML::LoadFile("test-param-factory.yml");

        // Create factory and read parameters
        ParamFactory factory;
        auto param_map = factory.reader(param_file,"param",log);

        // Wrap map getter in a lambda
        auto parameter_getter = [](int index){return get_parameter_map(index);};

        // Compare parameters from yaml with hardcoded values
        return !compare_parameter_maps(param_map,parameter_getter);
    }

    catch (Dune::Exception &e) {
        std::cerr << "Dune reported error: " << e << std::endl;
        return 1;
    }
    catch (std::exception &e) {
        std::cerr << "Exception thrown: " << e.what() << std::endl;
        return 1;
    }
    catch (...) {
        std::cerr << "Unknown exception!" << std::endl;
        return 1;
    }
}
