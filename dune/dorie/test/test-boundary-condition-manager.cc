#include <gtest/gtest.h>

#ifdef HAVE_CONFIG_H
    #include "config.h"
#endif

#include <memory>

#include <yaml-cpp/yaml.h>

#include <dune/common/exceptions.hh>
#include <dune/common/float_cmp.hh>
#include <dune/common/parallel/mpihelper.hh>

#include <dune/grid/yaspgrid.hh>
#include <dune/grid/common/gridview.hh>

#include <dune/dorie/common/setup.hh>
#include <dune/dorie/common/logging.hh>
#include <dune/dorie/common/grid_creator.hh>
#include <dune/dorie/common/boundary_condition/manager.hh>
#include <dune/dorie/common/boundary_condition/factory.hh>

// Store command line arguments
int _argc;
char **_argv;

/// The traits mock-up for this test
struct Traits
{
    static constexpr int dim = 2;
    using RF = double;
    using Grid = Dune::YaspGrid<dim>;
    using GV = typename Grid::LeafGridView;
    using Intersection = typename GV::Traits::Intersection;
};

/// Alias for the boundary condition type
using BC = Dune::Dorie::BoundaryCondition<typename Traits::RF>;

/* ------------------------------------------------------------------------- */
// Test BC factory creating BCs from YAML nodes
// This tests checks if the construction throws appropriate exceptions for
// incompatible input.

/// BCFactory fixture. Create three factories with a different mode each.
class BCFactory : public ::testing::Test
{
protected:
    using Mode = Dune::Dorie::BCMMode;

    template <Mode mode>
    using BCF = Dune::Dorie::BoundaryConditionFactory<double, mode>;

    /// Initialize the DUNE MPI Helper from the command line arguments
    Dune::MPIHelper& helper = Dune::MPIHelper::instance(_argc, _argv);

    std::unique_ptr<BCF<Mode::none>> _bcf_none;
    std::unique_ptr<BCF<Mode::richards>> _bcf_richards;
    std::unique_ptr<BCF<Mode::transport>> _bcf_transport;

    void SetUp () override
    {
        auto log = Dune::Dorie::create_logger(Dune::Dorie::log_base, helper);
        _bcf_none = std::make_unique<BCF<Mode::none>>(log);
        _bcf_richards = std::make_unique<BCF<Mode::richards>>(log);
        _bcf_transport = std::make_unique<BCF<Mode::transport>>(log);
    }

    void TearDown () override
    {
        // Remove loggers
        spdlog::drop_all();
    }
};

TEST_F(BCFactory, Dirichlet)
{
    YAML::Node config = YAML::Load("{type: Dirichlet,"
                                    "value: 1.0,"
                                    "time: 0.0"
                                   "}");
    EXPECT_NO_THROW(_bcf_none->create("name", config, 1.0));
    EXPECT_NO_THROW(_bcf_richards->create("name", config, 1.0));
    EXPECT_THROW(_bcf_transport->create("name", config, 1.0), Dune::IOError);

    config = YAML::Load("{type: Dirichlet,"
                         "value: 1.0,"
                         "time: 0.0,"
                         "concentration_type: water_phase"
                        "}");
    EXPECT_NO_THROW(_bcf_none->create("name", config, 1.0));
    EXPECT_NO_THROW(_bcf_richards->create("name", config, 1.0));
    EXPECT_NO_THROW(_bcf_transport->create("name", config, 1.0));

    config = YAML::Load("{type: Dirichlet,"
                         "value: 1.0,"
                         "time: 0.0,"
                         "concentration_type: none"
                        "}");
    EXPECT_NO_THROW(_bcf_none->create("name", config, 1.0));
    EXPECT_NO_THROW(_bcf_richards->create("name", config, 1.0));
    EXPECT_THROW(_bcf_transport->create("name", config, 1.0), Dune::IOError);
}

TEST_F(BCFactory, Neumann)
{
    YAML::Node config = YAML::Load("{type: Neumann,"
                                    "value: 1.0,"
                                    "time: 0.0,"
                                    "horizontal_projection: true"
                                   "}");
    EXPECT_NO_THROW(_bcf_none->create("name", config, 1.0));
    EXPECT_NO_THROW(_bcf_richards->create("name", config, 1.0));
    EXPECT_NO_THROW(_bcf_transport->create("name", config, 1.0));
}

TEST_F(BCFactory, Outflow)
{
    YAML::Node config = YAML::Load("{type: Outflow,"
                                    "value: 1.0,"
                                    "time: 0.0"
                                   "}");
    EXPECT_NO_THROW(_bcf_none->create("name", config, 1.0));
    EXPECT_NO_THROW(_bcf_richards->create("name", config, 1.0));
    EXPECT_NO_THROW(_bcf_transport->create("name", config, 1.0));
}

/* ------------------------------------------------------------------------- */
// Test BC manager and loaded BC data.
// This verifies that YAML data is properly interpreted

/// BoundaryConditionManager fixture
/** \tparam T BCMMode wrapped into an integral constant to appear as type
 */
// template<typename T>
class BCManager : public ::testing::Test
{
protected:
    /// Initialize the DUNE MPI Helper from the command line arguments
    Dune::MPIHelper& helper = Dune::MPIHelper::instance(_argc, _argv);

    /// Pointer to the grid
    std::shared_ptr<Traits::Grid> _grid;

    using BCM = Dune::Dorie::BoundaryConditionManager<
        Traits, Dune::Dorie::BCMMode::none
    >;
    /// Pointer to the manager
    std::unique_ptr<BCM> _manager;

    /// Initialize
    void SetUp () override
    {
        // Initialize ALL the things!
        auto [inifile, log, helper] = Dune::Dorie::Setup::init(_argc, _argv);
        
        // create the grid
        Dune::Dorie::GridCreator<Traits::Grid> gc(inifile, helper);
        _grid = gc.grid();
        const auto& index_map = gc.boundary_index_map();

        inifile = Dune::Dorie::Setup::prep_ini_for_richards(inifile);

        const auto log_level = inifile.get<std::string>("output.logLevel");
        log = Dune::Dorie::create_logger(Dune::Dorie::log_richards,
                                         helper,
                                         spdlog::level::from_str(log_level));

        _manager = std::make_unique<BCM>(inifile, index_map);
    }

    void TearDown () override
    {
        // remove loggers
        spdlog::drop_all();
    }
};

TEST_F(BCManager, TimeSteps)
{
    // check correct maximum time steps
    double max_dt = 0.0;
    double time_prev = 0.0;

    for (auto time : {time_prev, 0.5, 1.5})
    {
        EXPECT_DOUBLE_EQ(time, time_prev+max_dt);
        time_prev = time;
        this->_manager->set_time(time);
        max_dt = this->_manager->max_time_step(time);
    }
    EXPECT_DOUBLE_EQ(max_dt, 0.5);

    // assert exception if out of interval
    EXPECT_THROW(this->_manager->set_time(-0.5), Dune::InvalidStateException);
    EXPECT_THROW(this->_manager->set_time(2.0), Dune::InvalidStateException);
}

TEST_F(BCManager, Neumann)
{
    auto gv = _grid->leafGridView();
    auto cell = *Dune::elements(gv).begin();
    if (not cell.hasBoundaryIntersections()) {
        FAIL();
    }
    for (auto&& is : Dune::intersections(gv, cell)) {
        const auto coords = is.geometry().center();
        if (Dune::FloatCmp::eq(coords[1], 1.0))
        {
            using namespace Dune::Dorie;

            // Check initial cache (before setting time)
            const auto bc = _manager->bc(is);
            EXPECT_EQ(bc->name(), "neumann_initial");
            EXPECT_EQ(bc->type(), Operator::BCType::Neumann);
            EXPECT_DOUBLE_EQ(bc->evaluate(0.0), 0.0);
            auto &time_int = bc->time_interval();
            EXPECT_DOUBLE_EQ(time_int.begin, 0.0);
            EXPECT_DOUBLE_EQ(time_int.end, 0.0);

            // Check BCs after setting times
            for (auto time: {0.0, 1.0, 1.5, 1.9}) {
                _manager->set_time(time);
                const auto bc = _manager->bc(is);
                EXPECT_EQ(bc->name(), "neumann");
                EXPECT_EQ(bc->type(), Operator::BCType::Neumann);
                EXPECT_DOUBLE_EQ(bc->evaluate(time), 1.0);
                auto &time_int = bc->time_interval();
                EXPECT_DOUBLE_EQ(time_int.begin, 0.0);
                EXPECT_DOUBLE_EQ(time_int.end, 2.0);

                auto &bc_neumann = 
                    dynamic_cast<NeumannBoundaryCondition<double>&>(*bc);
                EXPECT_TRUE(bc_neumann.horizontal_projection());
            }
            // test passed
            return;
        }
    }

    FAIL();
}

TEST_F(BCManager, Dirichlet)
{
    auto gv = _grid->leafGridView();
    auto cell = *Dune::elements(gv).begin();
    if (not cell.hasBoundaryIntersections()) {
        FAIL();
    }
    for (auto&& is : Dune::intersections(gv, cell)) {
        const auto coords = is.geometry().center();
        if (Dune::FloatCmp::eq(coords[1], 0.0))
        {
            using namespace Dune::Dorie;

            // Check initial cache (before setting time)
            const auto bc = _manager->bc(is);
            EXPECT_EQ(bc->name(), "default");
            EXPECT_EQ(bc->type(), Operator::BCType::Neumann);
            EXPECT_DOUBLE_EQ(bc->evaluate(0.0), 0.0);

            // Check BCs after setting times
            for (auto time: {0.5, 1.4}) {
                _manager->set_time(time);
                const auto bc = _manager->bc(is);
                EXPECT_EQ(bc->name(), "dirichlet");
                EXPECT_EQ(bc->type(), Operator::BCType::Dirichlet);
                EXPECT_DOUBLE_EQ(bc->evaluate(time), time-0.5);
                auto &time_int = bc->time_interval();
                EXPECT_DOUBLE_EQ(time_int.begin, 0.5);
                EXPECT_DOUBLE_EQ(time_int.end, 1.5);

                auto &bc_dirichlet =
                    dynamic_cast<DirichletBoundaryCondition<double>&>(*bc);
                EXPECT_EQ(bc_dirichlet.concentration_type(),
                          SoluteConcentration::water_phase);
            }

            for (auto time: {0.0, 1.5, 1.9}) {
                _manager->set_time(time);
                const auto bc = _manager->bc(is);
                EXPECT_EQ(bc->name(), "default");
                EXPECT_EQ(bc->type(), Operator::BCType::Neumann);
                EXPECT_DOUBLE_EQ(bc->evaluate(time), 0.0);
            }
            // test passed
            return;
        }
    }

    FAIL();
}

TEST_F(BCManager, Outflow)
{
    auto gv = _grid->leafGridView();
    auto cell = *Dune::elements(gv).begin();
    if (not cell.hasBoundaryIntersections()) {
        FAIL();
    }
    for (auto&& is : Dune::intersections(gv, cell)) {
        const auto coords = is.geometry().center();
        if (Dune::FloatCmp::eq(coords[0], 0.0))
        {
            using namespace Dune::Dorie;

            // Check initial cache (before setting time)
            const auto bc = _manager->bc(is);
            EXPECT_EQ(bc->name(), "outflow_initial");
            EXPECT_EQ(bc->type(), Operator::BCType::Outflow);
            EXPECT_DOUBLE_EQ(bc->evaluate(0.0), 1.0);
            auto &time_int = bc->time_interval();
            EXPECT_DOUBLE_EQ(time_int.begin, -1.0);
            EXPECT_DOUBLE_EQ(time_int.end, 0.0);

            // Check BCs after setting times
            for (auto time: {0.0, 1.0, 1.5, 1.9}) {
                _manager->set_time(time);
                const auto bc = _manager->bc(is);
                EXPECT_EQ(bc->name(), "outflow");
                EXPECT_EQ(bc->type(), Operator::BCType::Outflow);
                EXPECT_DOUBLE_EQ(bc->evaluate(time), 0.0);
                auto &time_int = bc->time_interval();
                EXPECT_DOUBLE_EQ(time_int.begin, 0.0);
                EXPECT_DOUBLE_EQ(time_int.end, 2.0);
            }
            // test passed
            return;
        }
    }

    FAIL();
}

// --- Main Function --- //

int main(int argc, char **argv)
{
    // Parse input arguments
    ::testing::InitGoogleTest(&argc, argv);
    _argc = argc;
    _argv = argv;

    return RUN_ALL_TESTS();
}
