#include <gtest/gtest.h>

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <exception>
#include <random>
#include <string>
#include <vector>

#include <dune/common/parametertree.hh>

#include <dune/dorie/common/grid_creator.hh>
#include <dune/dorie/common/setup.hh>
#include <dune/dorie/model/richards/flow_parameters.hh>
#include <dune/dorie/model/richards/parameterization/interface.hh>

#include "test-parameterization.hh"

int argc = 0;
char** argv;

class ScaledParamTest : public ::testing::Test
{
protected:
  using T = Traits<2>;
  using Grid = typename T::Grid;
  using Domain = typename T::Domain;
  using Scaling = Dune::Dorie::Parameterization::ScalingFactors<typename T::RF>;

  std::shared_ptr<spdlog::logger> log;
  Dune::ParameterTree config;
  std::shared_ptr<typename T::Grid> grid;
  std::vector<int> index_map;

  std::mt19937 rng;
  std::uniform_real_distribution<double> dist_head;
  std::uniform_real_distribution<double> dist_sat;
  std::uniform_real_distribution<double> dist_wc;

  void SetUp() override
  {
    auto [cfg, lg, helper] = Dune::Dorie::Setup::init(argc, argv);
    log = lg;
    config = cfg;

    const auto log_level = config.get<std::string>("richards.output.logLevel");
    log = Dune::Dorie::create_logger(
      Dune::Dorie::log_richards, helper, spdlog::level::from_str(log_level));

    // Create the grid
    Dune::Dorie::GridCreator<Grid> gc(cfg, helper);
    grid = gc.grid();
    index_map = gc.element_index_map();

    // Set up RNG
    rng = std::mt19937(1);
    dist_head = std::uniform_real_distribution<double>(-50.0, 0.0);
    dist_sat = std::uniform_real_distribution<double>(0.0, 1.0);
    // NOTE: Depends on parameter set and porosity scaling in test!
    dist_wc = std::uniform_real_distribution<double>(0.03, 0.2);
  }

  void TearDown() override { spdlog::drop_all(); }
};

using Parameters = Dune::Dorie::FlowParameters<Traits<2>>;

template<class Grid>
Parameters
create_flow_parameters(const std::string& parameters_file,
                       Dune::ParameterTree config,
                       const std::shared_ptr<Grid> grid,
                       const std::vector<int>& index_map)
{
  config["parameters.file"] = parameters_file;
  return Parameters(config, grid, index_map);
}

// --- Test cases --- //

TEST_F(ScaledParamTest, HeadSaturation)
{
  auto param_unscaled = create_flow_parameters(
    "test-param-richards-unscaled.yml", config, grid, index_map);
  auto param_scaled = create_flow_parameters(
    "test-param-richards-scaled.yml", config, grid, index_map);

  auto gv = grid->leafGridView();
  for (auto&& cell : elements(gv))
  {
    // Attach to element
    param_scaled.bind(cell);
    param_unscaled.bind(cell);

    // Retrieve stored scaling information for element
    auto scale = std::get<Scaling>(param_scaled.cache());

    // Retrieve parameterization functions
    auto sat_f_s = param_scaled.saturation_f();
    auto sat_f_u = param_unscaled.saturation_f();
    const auto scale_head = scale.scale_head;

    // Perform test
    const auto head = dist_head(rng);
    EXPECT_DOUBLE_EQ(sat_f_s(head), sat_f_u(head * scale_head));

    // Retrieve inverse functions
    auto head_f_s = param_scaled.matric_head_f();
    auto head_f_u = param_unscaled.matric_head_f();

    // Perform test
    const auto sat = dist_sat(rng);
    EXPECT_DOUBLE_EQ(head_f_s(sat), head_f_u(sat) / scale_head);
  }
}

TEST_F(ScaledParamTest, SaturationToConductivity)
{
  auto param_unscaled = create_flow_parameters(
    "test-param-richards-unscaled.yml", config, grid, index_map);
  auto param_scaled = create_flow_parameters(
    "test-param-richards-scaled.yml", config, grid, index_map);

  auto gv = grid->leafGridView();
  for (auto&& cell : elements(gv))
  {
    // Attach to element
    param_scaled.bind(cell);
    param_unscaled.bind(cell);

    // Retrieve stored scaling information for element
    auto scale = std::get<Scaling>(param_scaled.cache());

    // Retrieve parameterization functions
    auto cond_f_s = param_scaled.conductivity_f();
    auto cond_f_u = param_unscaled.conductivity_f();
    const auto scale_cond = scale.scale_cond;

    const auto sat = dist_sat(rng);
    EXPECT_DOUBLE_EQ(cond_f_s(sat), cond_f_u(sat) * scale_cond * scale_cond);
  }
}

TEST_F(ScaledParamTest, SaturationWaterContent)
{
  auto param_unscaled = create_flow_parameters(
    "test-param-richards-unscaled.yml", config, grid, index_map);
  auto param_scaled = create_flow_parameters(
    "test-param-richards-scaled.yml", config, grid, index_map);

  auto gv = grid->leafGridView();
  for (auto&& cell : elements(gv))
  {
    // Attach to element
    param_scaled.bind(cell);
    param_unscaled.bind(cell);

    // Retrieve stored scaling information for element
    auto scale = std::get<Scaling>(param_scaled.cache());

    // Retrieve parameterization functions
    auto wc_f_s = param_scaled.water_content_f();
    auto wc_f_u = param_unscaled.water_content_f();
    const auto scale_por = scale.scale_por;

    const auto sat = dist_sat(rng);
    EXPECT_DOUBLE_EQ(wc_f_s(sat), wc_f_u(sat) - sat * scale_por);

    // Retrieve inverse functions
    auto sat_f_s = param_scaled.water_content_f_inv();
    auto sat_f_u = param_unscaled.water_content_f_inv();

    // Retrieve parameters for element
    using Param = Dune::Dorie::Parameterization::Richards<Traits<2>>;
    auto param = std::get<std::shared_ptr<Param>>(param_scaled.cache());
    const auto theta_r = param->_theta_r.value;

    const auto wc = dist_wc(rng);
    EXPECT_DOUBLE_EQ(1.0 / sat_f_s(wc),
                     1.0 / sat_f_u(wc) - scale_por / (wc - theta_r));
  }
}

TEST(RichardsParamInterface, PorosityScalingVerify)
{
  using Interface =
    Dune::Dorie::Parameterization::MualemVanGenuchten<Traits<2>>;
  Interface ic("name");

  // Define maximum bounds for water contents
  ic._theta_r = Interface::ResidualWaterContent({ 0.0 });
  ic._theta_s = Interface::SaturatedWaterContent({ 1.0 });

  // No scaling should always work
  EXPECT_NO_THROW(ic.water_content_f(0.0));
  EXPECT_NO_THROW(ic.water_content_f_inv(0.0));

  // Scaling up to 1.0 should work
  EXPECT_NO_THROW(ic.water_content_f(0.2));
  EXPECT_NO_THROW(ic.water_content_f_inv(0.8));

  // Scaling above 1.0 and below 0.0 should not work
  EXPECT_THROW(ic.water_content_f(1.2), Dune::IOError);
  EXPECT_THROW(ic.water_content_f_inv(-0.8), Dune::IOError);
}

// --- Main Function --- //

int
main(int _argc, char** _argv)
{
  // Parse input arguments
  ::testing::InitGoogleTest(&_argc, _argv);
  argc = _argc;
  argv = _argv;

  return RUN_ALL_TESTS();
}
