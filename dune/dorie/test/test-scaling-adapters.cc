#include <gtest/gtest.h>

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <functional>
#include <random>
#include <string>
#include <vector>

#include <yaml-cpp/yaml.h>

#include <dune/common/float_cmp.hh>
#include <dune/common/fvector.hh>
#include <dune/common/parallel/mpihelper.hh>
#include <dune/common/parametertree.hh>
#include <dune/common/parametertreeparser.hh>

#include <dune/grid/yaspgrid.hh>

#include <dune/dorie/common/grid_creator.hh>
#include <dune/dorie/common/setup.hh>
#include <dune/dorie/model/richards/parameterization/scaling.hh>

int argc = 0;
char** argv;

struct Traits
{
  static constexpr int dim = 2;
  using RF = double;
  using DF = double;
  using Grid = Dune::YaspGrid<dim, Dune::EquidistantOffsetCoordinates<DF, dim>>;
  using Domain = Dune::FieldVector<DF, dim>;
  using GridView = typename Grid::LeafGridView;
};

/// Fixture storing scaling settings, grid, and sampling positions
class ScalingFixture : public ::testing::Test
{
protected:
  using Grid = typename Traits::Grid;
  using Domain = typename Traits::Domain;
  using SAF = Dune::Dorie::Parameterization::ScalingAdapterFactory<Traits>;
  using Scaling =
    Dune::Dorie::Parameterization::ScalingFactors<typename Traits::RF>;

  std::shared_ptr<spdlog::logger> log;
  std::shared_ptr<typename Traits::Grid> grid;
  YAML::Node scale_file;
  /// Positions where the scaling adapter will be sampled
  std::vector<Domain> pos_sample;

  void SetUp() override
  {
    auto [cfg, lg, helper] = Dune::Dorie::Setup::init(argc, argv);
    log = lg;
    scale_file = YAML::LoadFile(cfg["_scaling_file_name"]);

    // Create the grid
    Dune::Dorie::GridCreator<Grid> gc(cfg, helper);
    grid = gc.grid();

    // Refine the grid so we sample more than the coarsest grid vertices
    grid->globalRefine(1);

    // Store the sampling positions
    for (auto&& vertex : vertices(grid->leafGridView()))
    {
      pos_sample.emplace_back(vertex.geometry().center());
    }
  }

  void TearDown() override { spdlog::drop_all(); }
};

// Inject into correct namespace for Google Test
namespace Dune::Dorie::Parameterization
{

/// Compare two scaling factor structs
template<typename T>
std::enable_if_t<std::is_arithmetic_v<T>, bool>
operator==(const ScalingFactors<T>& a, const ScalingFactors<T>& b)
{
  using namespace Dune::FloatCmp;
  if (eq(a.scale_cond, b.scale_cond) and eq(a.scale_head, b.scale_head)
      and eq(a.scale_por, b.scale_por))
  {
    return true;
  }

  return false;
}

} // namespace Dune::Dorie::Parameterization

/// Create a scaling adapter
template<class GV>
decltype(auto)
create_scaling(const YAML::Node& settings,
               const GV& grid_view,
               const std::shared_ptr<spdlog::logger> log)
{
  using SAF = Dune::Dorie::Parameterization::ScalingAdapterFactory<Traits>;
  const auto type = settings["type"].as<std::string>();
  return SAF::create(type, settings["data"], grid_view, log);
}

// --- Test cases --- //

TEST_F(ScalingFixture, DummyScaling)
{
  auto scaling = create_scaling(scale_file["dummy"], grid->leafGridView(), log);
  const Scaling reference{ 1.0, 1.0, 0.0 };
  for (auto&& pos : pos_sample)
  {
    const auto value = scaling->evaluate(pos);
    EXPECT_EQ(value, reference) << "Position : " << pos[0] << ", " << pos[1];
  }
}

TEST_F(ScalingFixture, MillerAutomaticExtension)
{
  auto scaling = create_scaling(
    scale_file["miller_automatic_extension"], grid->leafGridView(), log);
  const Scaling reference{ 2.0, 2.0, 0.0 };
  for (auto&& pos : pos_sample)
  {
    const auto value = scaling->evaluate(pos);
    EXPECT_EQ(value, reference) << "Position : " << pos[0] << ", " << pos[1];
  }
}

TEST_F(ScalingFixture, MillerCustomExtension)
{
  auto scaling = create_scaling(
    scale_file["miller_custom_extension"], grid->leafGridView(), log);
  const Scaling reference{ 2.0, 2.0, 0.0 };
  for (auto&& pos : pos_sample)
  {
    const auto value = scaling->evaluate(pos);
    EXPECT_EQ(value, reference) << "Position : " << pos[0] << ", " << pos[1];
  }
}

TEST_F(ScalingFixture, MillerShift)
{
  auto scaling =
    create_scaling(scale_file["miller_shift"], grid->leafGridView(), log);
  const Scaling ref_low{ 0.0, 0.0, 0.0 }, ref_high{ 1.0, 1.0, 0.0 };
  for (auto&& pos : pos_sample)
  {
    const auto value = scaling->evaluate(pos);
    auto reference = ref_low;
    if (int(pos[1]) > 0)
      reference = ref_high;
    EXPECT_EQ(value, reference) << "Position : " << pos[0] << ", " << pos[1];
  }
}

TEST_F(ScalingFixture, MilPor)
{
  auto scaling =
    create_scaling(scale_file["milpor"], grid->leafGridView(), log);
  const Scaling reference{ 0.0, 0.0, 2.0 };
  for (auto&& pos : pos_sample)
  {
    const auto value = scaling->evaluate(pos);
    EXPECT_EQ(value, reference) << "Position : " << pos[0] << ", " << pos[1];
  }
}

// --- Main Function --- //

int
main(int _argc, char** _argv)
{
  // Parse input arguments
  ::testing::InitGoogleTest(&_argc, _argv);
  argc = _argc;
  argv = _argv;

  return RUN_ALL_TESTS();
}
