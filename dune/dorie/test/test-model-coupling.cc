#include <gtest/gtest.h>

#include <random>

#ifdef HAVE_CONFIG_H
    #include "config.h"
#endif

#include <dune/dorie/common/setup.hh>
#include <dune/dorie/model/factory_transport.hh>

#include <dune/dorie/model/coupling/richards_coupling.hh>
#include <dune/dorie/model/coupling/richards_coupling.cc>

// Include model member definitions
#include <dune/dorie/model/richards/richards.cc>
#include <dune/dorie/model/transport/transport.cc>

// Store command line arguments
int _argc;
char **_argv;

// --- Main Function --- //

int main(int argc, char **argv)
{
    // Parse input arguments
    ::testing::InitGoogleTest(&argc, argv);
    _argc = argc;
    _argv = argv;

    return RUN_ALL_TESTS();
}

// --- Model Coupling Fixture --- //

namespace Dune::Dorie {

using Traits = ModelRichardsTransportCouplingTraits<
    BaseTraits<Dune::YaspGrid<DORIE_DIM>, GeometryType::BasicType::cube>,
    DORIE_RORDER,
    DORIE_TORDER
>;
using Model = ModelRichardsTransportCoupling<Traits>;

class ModelCoupling :
    public ::testing::TestWithParam<std::tuple<double, int, int>>
{
protected:
    /// A pointer to the model
    std::shared_ptr<ModelBase> _model;

    void SetUp () override
    {
        // Initialize ALL the things!
        auto [inifile, log, helper] = Dune::Dorie::Setup::init(_argc, _argv);
        log.reset();

        _model = TransportFactory::create(inifile, helper);
    }

    void TearDown () override
    {
        spdlog::drop_all();
    }
};

// --- Model Coupling Tests --- //

TEST_P(ModelCoupling, TimeSteps)
{
    // Get a pointer to the actual model class
    auto &model = dynamic_cast<Model&>(*_model);

    const auto richards_steps = std::get<1>(GetParam());
    for (int i = 0; i < richards_steps; ++i) {
        model.step();
    }
    EXPECT_EQ(model._richards->osm->result().successful.timesteps,
              richards_steps);

    const auto time = std::get<0>(GetParam());
    EXPECT_DOUBLE_EQ(model.current_time(), time);
    EXPECT_DOUBLE_EQ(model._richards->current_time(), time);
    EXPECT_DOUBLE_EQ(model._transport->current_time(), time);

    const auto transport_steps = std::get<2>(GetParam());
    EXPECT_EQ(model._transport->time_steps, transport_steps);
}

INSTANTIATE_TEST_SUITE_P(ModelCouplingTimeSteps,
                         ModelCoupling,
                         testing::Values(
                             std::make_tuple(10.0, 1, 1),
                             std::make_tuple(30.0, 2, 3),
                             std::make_tuple(70.0, 3, 5),
                             std::make_tuple(100.0, 4, 6)
                         ));

// --- Parallel Model Coupling Tests --- //

class ModelCouplingParallel :
    public ::testing::Test
{
protected:
    /// A pointer to the model
    std::shared_ptr<ModelBase> _model;
    /// Time step for this process
    double dt;
    /// Minimum time step across all processes
    double dt_min;

    void SetUp () override
    {
        // Initialize ALL the things!
        auto [inifile, log, helper] = Dune::Dorie::Setup::init(_argc, _argv);
        log.reset();

        _model = TransportFactory::create(inifile, helper);

        // Create random time steps for every process, and find minimum
        auto comm = helper.getCollectiveCommunication();
        std::mt19937 gen(comm.rank());
        std::uniform_real_distribution<double> dist;
        dt = dist(gen);
        dt_min = comm.min(dt);
    }

    void TearDown () override
    {
        spdlog::drop_all();
    }
};

/// Check that models communicate minimal time step between processes
TEST_F(ModelCouplingParallel, Richards)
{
    // Get a pointer to the actual model class
    auto &model = dynamic_cast<Model&>(*_model);

    // Set time steps on each process, then run
    model.suggest_timestep(dt);
    model.step();
    EXPECT_DOUBLE_EQ(model.current_time(), dt_min);
}

TEST_F(ModelCouplingParallel, Transport)
{
    // Get a pointer to the actual model class
    auto &model = dynamic_cast<Model&>(*_model);

    // Create instationary grid function containers
    using GFWaterContentContainer  = typename Traits::GFWaterContentContainer;
    using GFWaterFluxContainer = typename Traits::GFWaterFluxContainer;
    auto igf_water_content = std::make_shared<GFWaterContentContainer>();
    auto igf_water_flux = std::make_shared<GFWaterFluxContainer>();

    // Set initial state of the water content to container
    auto gf_water_content_begin = model._richards->get_water_content();
    igf_water_content->push(gf_water_content_begin, 0.0);
    igf_water_content->push(gf_water_content_begin, 1.0);

    // Set initial state of the water flux to container
    auto gf_water_flux_begin = model._richards->get_water_flux_reconstructed();
    igf_water_flux->push(gf_water_flux_begin, 0.0);
    igf_water_flux->push(gf_water_flux_begin, 1.0);

    // Set data in transport solver
    model._transport->set_water_flux(igf_water_flux);
    model._transport->set_water_content(igf_water_content);

    // Perform step in transport solver
    model._transport->suggest_timestep(dt);
    model._transport->step();
    EXPECT_DOUBLE_EQ(model._transport->current_time(), dt_min);
}

} // namespace Dune::Dorie
