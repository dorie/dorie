#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <iostream>
#include <cstdio>
#include <thread>

#include <dune/common/exceptions.hh>
#include <dune/common/parametertree.hh>
#include <dune/common/parametertreeparser.hh>
#include <dune/common/parallel/mpihelper.hh>

#include <dune/grid/yaspgrid.hh>
#include <dune/grid/uggrid.hh>

#include <dune/dorie/common/setup.hh>
#include <dune/dorie/test/test-grid-creation.hh>

int main(int argc, char** argv)
{
  try{
        // Initialize ALL the things!
		auto [inifile, log, helper] = Dune::Dorie::Setup::init(argc, argv);
        log.reset(); // silence compiler warning

        if (inifile.get<bool>("misc.debugMode"))
            Dune::Dorie::Setup::debug_hook();

        const auto dimensions = inifile.get<int>("grid.dimensions");
        if (dimensions == 2) {
            constexpr int dim = 2;

            if (inifile["grid.gridType"] == "gmsh") {
                create_grid_and_test_mapping<Dune::UGGrid<dim>>(inifile, helper, 0);
            }
            else if (inifile["grid.gridType"] == "rectangular") {
                create_grid_and_test_mapping<Dune::UGGrid<dim>>(inifile, helper, 1);
                create_grid_and_test_mapping<Dune::YaspGrid<dim>>(inifile, helper, 2);
            }
            else
                DUNE_THROW(Dune::IOError, "Unsupported grid type!");
        }

        else if (dimensions == 3) {
            constexpr int dim = 3;

            if (inifile["grid.gridType"] == "gmsh") {
                create_grid_and_test_mapping<Dune::UGGrid<dim>>(inifile, helper, 0);
            }
            else if (inifile["grid.gridType"] == "rectangular") {
                create_grid_and_test_mapping<Dune::UGGrid<dim>>(inifile, helper, 1);
                create_grid_and_test_mapping<Dune::YaspGrid<dim>>(inifile, helper, 2);
            }
            else
                DUNE_THROW(Dune::IOError, "Unsupported grid type!");
        }

        else {
            DUNE_THROW(Dune::IOError, "This test only supports 2D and 3D!");
        }

        return 0;
    }
    catch (Dune::Exception &e){
        std::cerr << "Dune reported error: " << e << std::endl;
        return 1;
    }
    catch (...){
        std::cerr << "Unknown exception thrown!" << std::endl;
        throw;
        return 1;
    }
}
