#ifndef DUNE_DORIE_TEST_PARAMETERIZATION_HH
#define DUNE_DORIE_TEST_PARAMETERIZATION_HH

#include <dune/common/fvector.hh>
#include <dune/common/fmatrix.hh>
#include <dune/common/float_cmp.hh>

#include <dune/grid/yaspgrid.hh>

/// Compare the key-values pairs for two maps
/** This does not check if the set of keys compares equal.
 *  map_b may have more keys than map_a.
 * \return True of the values match
 */
template<typename MapA, typename MapB>
bool compare_maps (const MapA& map_a, const MapB& map_b)
{
    auto it_a = map_a.begin();
    auto it_b = map_b.begin();
    while (it_a!=map_a.end() && it_b!=map_b.end())
    {
        auto&& [key_a,value_a] = *it_a;
        auto&& [key_b,value_b] = *it_b;
        
        if (key_a<key_b) ++it_a;
        else if (key_b<key_a) ++it_b;
        else {
            if (not Dune::FloatCmp::eq(value_a, value_b)) {
                std::cout << "Error comparing maps: " << std::endl;
                std::cout << "  MapA[" << key_a << "]:" << value_a << std::endl;
                std::cout << "  MapB[" << key_b << "]:" << value_b << std::endl;
                return false;
            }
            else {
                ++it_a;
                ++it_b;
            }
        }
    }
    return true;
}

/// Mockup traits for this test
template<int dimension>
struct Traits
{
    static constexpr int dim = dimension;
    using RF = double;
    using Scalar = Dune::FieldVector<RF, 1>;
    using Vector = Dune::FieldVector<RF, dim>;
    using Tensor = Dune::FieldMatrix<RF, dim,dim>;

    using Grid = Dune::YaspGrid<dim>;
    using DF = typename Grid::ctype;
    using Domain = Dune::FieldVector<DF, dim>;
    using GridView = typename Grid::LeafGridView;
};

#endif // DUNE_DORIE_TEST_PARAMETERIZATION_HH