#include <gtest/gtest.h>

#ifdef HAVE_CONFIG_H
    #include "config.h"
#endif

// Disable MPI for testing
// In this case, this gives us the FakeMPIHelper instead of the regular one
#ifdef HAVE_MPI
    #undef HAVE_MPI
#endif

#include <dune/common/float_cmp.hh>
#include <dune/common/parallel/mpihelper.hh>

#include <dune/dorie/model/base.hh>

/// Fixture for all test cases
class ModelFixture : public ::testing::Test
{
protected:

    /// Initialize the DUNE MPI Helper from the command line arguments
    Dune::MPIHelper& helper = Dune::MPIHelper::instance(0, nullptr);

    /// Dummy model for testing only. Implements all pure virtual methods.
    class DummyModel : public Dune::Dorie::ModelBase
    {
    private:
        using Base = Dune::Dorie::ModelBase;

    public:
        DummyModel (Dune::MPIHelper& helper) :
            Base("dummy",
                 "debug",
                 helper,
                 Dune::Dorie::OutputPolicy::None,
                 Dune::Dorie::AdaptivityPolicy::None),
            _dt(.1),
            _current_time(begin_time()),
            _end_time(1.0),
            _grid_was_adapted(false),
            _data_was_written(false)
        { }

        double begin_time() const override { return 0.; }
        double end_time() const override { return _end_time; }
        double current_time() const override { return _current_time; }
        void suggest_timestep(double dt) override { _dt = std::min(dt,_dt); }
        void step() override { _current_time+=_dt; }

        /// Do nothing.
        void mark_grid () override {};

        /// Adapt the grid in our minds.
        void adapt_grid () override
        {
            _grid_was_adapted = true;
        }

        void write_data() const override
        {
            this->_log->info("Current time: {}", _current_time);
            _data_was_written = true;
        }

    private:
        double _dt;
        double _current_time;

    public:
        double _end_time;
        bool _grid_was_adapted;
        mutable bool _data_was_written;
    } model = DummyModel(helper);
};

/// Test if the model is correctly initialized
TEST_F (ModelFixture, Initialize)
{
    EXPECT_EQ(model._end_time, 1.0);
    EXPECT_FALSE(model._grid_was_adapted);
    EXPECT_FALSE(model._data_was_written);
    EXPECT_EQ(model.begin_time(), 0.0);
    EXPECT_EQ(model.current_time(), 0.0);
    EXPECT_EQ(model.output_policy(), Dune::Dorie::OutputPolicy::None);
    EXPECT_EQ(model.adaptivity_policy(), Dune::Dorie::AdaptivityPolicy::None);
}

/// Test if policies are correctly set
TEST_F (ModelFixture, Policies)
{
    const auto adapt_policy = Dune::Dorie::AdaptivityPolicy::WaterFlux;
    model.set_policy(adapt_policy);
    EXPECT_EQ(model.adaptivity_policy(), adapt_policy);

    const auto output_policy = Dune::Dorie::OutputPolicy::EndOfRichardsStep;
    model.set_policy(output_policy);
    EXPECT_EQ(model.output_policy(), output_policy);
}

/// Test if the model correctly performs the run() algorithm
TEST_F (ModelFixture, Run)
{
    // perform a step
    model.step();
    EXPECT_TRUE(Dune::FloatCmp::eq(model.current_time(),
                                   model.begin_time() + 0.1));

    // run multiple steps
    model.run();
    EXPECT_TRUE(Dune::FloatCmp::eq(model.current_time(),
                                   model.end_time()));
    EXPECT_FALSE(model._grid_was_adapted);
    EXPECT_FALSE(model._data_was_written);

    // set new policies
    model.set_policy(Dune::Dorie::AdaptivityPolicy::WaterFlux);
    model.set_policy(Dune::Dorie::OutputPolicy::EndOfRichardsStep);
    model._end_time += 1.0;

    // run multiple steps, check if policies are applied
    model.run();
    EXPECT_TRUE(Dune::FloatCmp::eq(model.current_time(),
                                   model.end_time()));
    EXPECT_TRUE(model._grid_was_adapted);
    EXPECT_TRUE(model._data_was_written);
}
