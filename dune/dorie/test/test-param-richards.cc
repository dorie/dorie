#ifdef HAVE_CONFIG_H
    #include "config.h"
#endif

#include <string>
#include <iostream>
#include <exception>
#include <map>
#include <exception>

#include <dune/common/exceptions.hh>
#include <dune/common/parametertree.hh>
#include <dune/common/parametertreeparser.hh>
#include <dune/common/parallel/mpihelper.hh>
#include <dune/common/float_cmp.hh>

#include <dune/dorie/common/setup.hh>
#include <dune/dorie/common/grid_creator.hh>
#include <dune/dorie/model/richards/flow_parameters.hh>
#include <dune/dorie/model/richards/parameterization/interface.hh>

#include "test-parameterization.hh"

/// Return parameter values based on the medium name
/** \param medium Name of the medium
 *  \return Map of parameters and associated values
 *  \throw runtime_error Medium name is not known
 */
std::multimap<std::string, double> get_parameter_map (const std::string medium)
{
    if (medium == "sand") {
        return std::multimap<std::string, double> {
            {"alpha", -2.3},
            {"n", 4.17},
            {"k0", 2.2e-5},
            {"theta_r", 0.03},
            {"theta_s", 0.31},
            {"tau", -1.1}
        };
    }
    else if (medium == "silt") {
        return std::multimap<std::string, double> {
            {"alpha", -0.7},
            {"n", 1.3},
            {"k0", 1.0e-5},
            {"theta_r", 0.01},
            {"theta_s", 0.41},
            {"tau", 0.0}
        };
    }
    else {
        throw std::runtime_error("Unknown medium name" + medium);
    }
}

/// Test the parameters and parameterization functions
/** The standard parameters for Sand and Silt are hardcoded here.
 *  We check the actual values and then the return values of the
 *  parameterization fuctions.
 *  \param fparam Dune::Dorie::FlowParameters object
 *  \param grid Shared pointer to the grid
 */
template<class FlowParameters, class Grid>
void test_new_parameters (const FlowParameters& fparam,
                                std::shared_ptr<Grid> grid)
{
    // define parameter sets to check against
    const auto param_sand = get_parameter_map("sand");
    const auto param_silt = get_parameter_map("silt");

    // iterate over grid cells and verify parameterization
    auto level_gv = grid->leafGridView();
    for (auto&& cell: elements(level_gv))
    {
        // bind parameterization to cell and fetch parameters
        fparam.bind(cell);
        using RP = Dune::Dorie::Parameterization::Richards<Traits<2>>;
        auto par = std::get<std::shared_ptr<const RP>>(fparam.cache());
        auto parameters = par->parameters();

        // compare values to defined parameter sets
        assert(compare_maps(param_sand, parameters)
            or compare_maps(param_silt, parameters));

        namespace FloatCmp = Dune::FloatCmp;

        // check evaluation of functions
        auto sat_f = fparam.saturation_f();
        assert(FloatCmp::eq(sat_f(0.0), 1.0));

        auto cond_f = fparam.conductivity_f();
        assert(FloatCmp::eq(cond_f(1.0), param_sand.find("k0")->second)
            or FloatCmp::eq(cond_f(1.0), param_silt.find("k0")->second));

        auto wc_f = fparam.water_content_f();
        assert(FloatCmp::eq(wc_f(1.0), param_sand.find("theta_s")->second)
            or FloatCmp::eq(wc_f(1.0), param_silt.find("theta_s")->second));
        assert(FloatCmp::eq(wc_f(0.0), param_sand.find("theta_r")->second)
            or FloatCmp::eq(wc_f(0.0), param_silt.find("theta_r")->second));
    }
}

/// Check if parameters can be manipulated
/** Manipulate parameters using the `parameters()` map or
 *  a dynamic cast to the derived class.
 *  \param fparam Dune::Dorie::FlowParameters object
 *  \param grid Shared pointer to the grid
 */
template<class FlowParameters, class Grid>
void test_parameter_manipulation (FlowParameters& fparam,
                                  std::shared_ptr<Grid> grid)
{
    // bind to any cell
    auto level_gv = grid->leafGridView();
    const auto& cell = *elements(level_gv).begin();
    fparam.bind(cell);

    // manipulate via returned map (this is the preferred way)
    using RP = Dune::Dorie::Parameterization::Richards<Traits<2>>;
    auto par = std::get<std::shared_ptr<RP>>(fparam.cache());
    auto parameters = par->parameters();
    parameters.find("theta_r")->second = 0.0;
    assert(par->_theta_r.value == 0.0);

    // manipulate directly via casting (this is not recommended)
    using MvG = Dune::Dorie::Parameterization::MualemVanGenuchten<Traits<2>>;
    auto& par_mvg = dynamic_cast<MvG&>(*par);
    parameters.find("alpha")->second = -10.0;
    assert(par_mvg._alpha.value = -10.0);
}

template<class FlowParameters, class Grid>
bool compare_parameters (const FlowParameters& fparam1,
                         const FlowParameters& fparam2,
                               std::shared_ptr<Grid> grid)
{
    // iterate over grid cells and verify parameterization
    auto level_gv = grid->leafGridView();
    for (auto&& cell: elements(level_gv))
    {
        // bind parameterization to cell and fetch parameters
        fparam1.bind(cell);
        fparam2.bind(cell);
        using RP = Dune::Dorie::Parameterization::Richards<Traits<2>>;
        auto par1 = std::get<std::shared_ptr<const RP>>(fparam1.cache());
        auto par2 = std::get<std::shared_ptr<const RP>>(fparam2.cache());
        auto parameters1 = par1->parameters();
        auto parameters2 = par2->parameters();

        // compare values between the two parameterizations
        if (not compare_maps(parameters1, parameters2))
            return false;
    }
    return true;
}

int main (int argc, char** argv)
{
    try{
        // Initialize ALL the things!
        auto [inifile, log, helper] = Dune::Dorie::Setup::init(argc, argv);

        // create the grid
        Dune::Dorie::GridCreator<Traits<2>::Grid> gc(inifile, helper);
        auto grid = gc.grid();
        const auto index_map = gc.element_index_map();

        inifile = Dune::Dorie::Setup::prep_ini_for_richards(inifile);

        // create the Richards logger because FlowParameters expects it
        const auto log_level = inifile.get<std::string>("output.logLevel");
        log = Dune::Dorie::create_logger(Dune::Dorie::log_richards,
                                         helper,
                                         spdlog::level::from_str(log_level));

        const Dune::Dorie::FlowParameters<Traits<2>> const_param(inifile,
                                                                 grid,
                                                                 index_map);
        Dune::Dorie::FlowParameters<Traits<2>> param(const_param); // deep copy

        // perform the actual tests
        test_new_parameters(const_param, grid);
        test_new_parameters(param, grid);

        assert(compare_parameters(const_param, param, grid));
        test_parameter_manipulation(param, grid);
        assert(not compare_parameters(const_param, param, grid));

        return 0;
    }

    catch (Dune::Exception &e){
        std::cerr << "Dune reported error: " << e << std::endl;
        return 1;
    }
    catch (std::exception &e) {
        std::cerr << "Exception thrown: " << e.what() << std::endl;
        return 1;
    }
    catch(...) {
        std::cerr << "Exception occurred!" << std::endl;
        throw;
    }
}