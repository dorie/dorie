#ifndef TEST_GRID_CREATION_HH
#define TEST_GRID_CREATION_HH

#include <string>

#include <dune/common/exceptions.hh>
#include <dune/common/parametertree.hh>
#include <dune/common/parallel/mpihelper.hh>

#include <dune/grid/common/gridview.hh>
#include <dune/grid/io/file/vtk/vtkwriter.hh>
#include <dune/grid/common/mcmgmapper.hh>

#include <dune/dorie/common/grid_creator.hh>
#include <dune/dorie/common/logging.hh>

/// Create the grid and check if the mapping is as expected
template<typename Grid>
void create_grid_and_test_mapping (const Dune::ParameterTree& config,
                                   const Dune::MPIHelper& helper,
                                   const int count=0)
{
    // create the grid mapper
    Dune::Dorie::GridCreator<Grid> gc(config, helper);

    // get grid and data
    auto grid = gc.grid();
    const auto element_map = gc.element_index_map();
    const auto boundary_map = gc.boundary_index_map();

    // report maps
    auto log = Dune::Dorie::get_logger(Dune::Dorie::log_base);
    log->debug("Read element map: {}",Dune::Dorie::to_string(element_map));
    log->debug("Read boundary map: {}", Dune::Dorie::to_string(boundary_map));

    // check overall contents of boundary map
    if ((std::is_same_v<Grid, Dune::UGGrid<2>>
         or std::is_same_v<Grid, Dune::UGGrid<3>>)
        and helper.size() > 1) {
        // nothing
    }
    else {
        assert(std::none_of(begin(boundary_map), end(boundary_map),
                            [](const auto index){ return index == 0; }));
    }

    // check mapping
    auto gv = grid->levelGridView(0);
    Dune::MultipleCodimMultipleGeomTypeMapper<
        typename Grid::LevelGridView>
        mapper(gv, Dune::mcmgElementLayout());

    Dune::VTKWriter writer(gv);
    writer.addCellData(element_map, "mapping");
    writer.write("mapping-" + std::to_string(count));

    for (auto&& cell : elements(gv)) {
        const auto index = mapper.index(cell);
        const auto pos = cell.geometry().center();
        if (pos[Grid::dimension-1] > 1.0) {
            assert(element_map.at(index) == 1);
        }
        else {
            assert(element_map.at(index) == 0);
        }

        if (cell.hasBoundaryIntersections()) {
            for (auto&& is : intersections(gv, cell)) {
                if (not is.boundary()) {
                    continue;
                }

                const auto pos = is.geometry().center();
                const auto index = is.boundarySegmentIndex();
                if (pos[Grid::dimension-1] > 1.0) {
                    assert(boundary_map.at(index) == 3);
                }
                else {
                    assert(boundary_map.at(index) == 2);
                }
            }
        }
    }
}

#endif // TEST_GRID_CREATION_HH
