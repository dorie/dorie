#include <gtest/gtest.h>

#ifdef HAVE_CONFIG_H
    #include "config.h"
#endif

#include <type_traits>
#include <string>

#include <dune/common/exceptions.hh>

#include <dune/dorie/common/typedefs.hh>
#include <dune/dorie/common/boundary_condition/factory.hh>

using RF = double;

/// Initialize the BC base class
/** \param value_end Set to True if an optional final value is to be added.
 */
template<typename BC>
BC initialize_bc (bool value_end=false)
{
    using namespace Dune::Dorie;
    TimeInterval<RF> t_int({1.0, 2.0});
    const RF v_begin = 1.0;
    const RF v_end = 2.0;
    const std::string name = "bc";

    if constexpr (std::is_same_v<BC, NeumannBoundaryCondition<RF>>)
    {
        if (value_end) {
            return BC(name, t_int, v_begin, v_end, false);
        }
        else {
            return BC(name, t_int, v_begin, false);
        }
    }
    else {
        if (value_end) {
            return BC(name, t_int, v_begin, v_end);
        }
        else {
            return BC(name, t_int, v_begin);
        }
    }
}

/* ------------------------------------------------------------------------- */
// Test common features of all boundary conditions

template<typename BC>
class BoundaryCondition : public ::testing::Test
{
protected:
    BC _bc = initialize_bc<BC>();
};

using BCTypes = ::testing::Types<Dune::Dorie::NeumannBoundaryCondition<RF>,
                                 Dune::Dorie::BoundaryCondition<RF>,
                                 Dune::Dorie::DirichletBoundaryCondition<RF>,
                                 Dune::Dorie::OutflowBoundaryCondition<RF>>;
TYPED_TEST_SUITE(BoundaryCondition, BCTypes);

TYPED_TEST(BoundaryCondition, Initialize)
{
    const auto &time_interval = this->_bc.time_interval();
    EXPECT_DOUBLE_EQ(1.0, time_interval.begin);
    EXPECT_DOUBLE_EQ(2.0, time_interval.end);
    EXPECT_EQ(this->_bc.name(), "bc");
}

TYPED_TEST(BoundaryCondition, EvaluateThrow)
{
    // inside time interval
    EXPECT_NO_THROW(this->_bc.evaluate(1.0));
    EXPECT_NO_THROW(this->_bc.evaluate(1.5));
    EXPECT_NO_THROW(this->_bc.evaluate(2.0));

    // outside time interval
    EXPECT_THROW(this->_bc.evaluate(-1.0), Dune::InvalidStateException);
    EXPECT_THROW(this->_bc.evaluate(0.0), Dune::InvalidStateException);
    EXPECT_THROW(this->_bc.evaluate(2.5), Dune::InvalidStateException);
}

TYPED_TEST(BoundaryCondition, Evaluate)
{
    EXPECT_DOUBLE_EQ(this->_bc.evaluate(1.0), 1.0);
    EXPECT_DOUBLE_EQ(this->_bc.evaluate(1.5), 1.0);
    EXPECT_DOUBLE_EQ(this->_bc.evaluate(2.0), 1.0);
}

TEST(BoundaryCondition, EvaluateInterpolate)
{
    auto bc = initialize_bc<Dune::Dorie::BoundaryCondition<RF>>(true);
    EXPECT_DOUBLE_EQ(bc.evaluate(1.0), 1.0);
    EXPECT_DOUBLE_EQ(bc.evaluate(1.5), 1.5);
    EXPECT_DOUBLE_EQ(bc.evaluate(2.0), 2.0);
}

/* ------------------------------------------------------------------------- */
// Test Dirichlet BC

class Dirichlet :
    public testing::TestWithParam<Dune::Dorie::SoluteConcentration>
{
protected:
    using BC = Dune::Dorie::DirichletBoundaryCondition<RF>;
    BC _bc = BC("bc", {1.0, 2.0}, 1.0, GetParam());
};

TEST_P(Dirichlet, SoluteConcentration)
{
    EXPECT_EQ(_bc.concentration_type(), GetParam());
}

TEST_P(Dirichlet, StaticInformation)
{
    EXPECT_EQ(_bc.type(), Dune::Dorie::Operator::BCType::Dirichlet);
    EXPECT_EQ(_bc.type_str, "dirichlet");
}

INSTANTIATE_TEST_SUITE_P(
    DirichletConcentrationType,
    Dirichlet,
    testing::Values(Dune::Dorie::SoluteConcentration::total,
                    Dune::Dorie::SoluteConcentration::water_phase,
                    Dune::Dorie::SoluteConcentration::none));


/* ------------------------------------------------------------------------- */
// Test Neumann BC

class Neumann :
    public testing::TestWithParam<bool>
{
protected:
    using BC = Dune::Dorie::NeumannBoundaryCondition<RF>;
    BC _bc = BC("bc", {1.0, 2.0}, 1.0, GetParam());
};

TEST_P(Neumann, HorizontalProjection)
{
    EXPECT_EQ(_bc.horizontal_projection(), GetParam());
}

TEST_P(Neumann, StaticInformation)
{
    EXPECT_EQ(_bc.type(), Dune::Dorie::Operator::BCType::Neumann);
    EXPECT_EQ(_bc.type_str, "neumann");
}

INSTANTIATE_TEST_SUITE_P(NeumannConcentrationType,
                         Neumann,
                         testing::Bool());

/* ------------------------------------------------------------------------- */
// Test Outflow BC

TEST(Outflow, StaticInformation)
{
    using namespace Dune::Dorie;
    auto bc = initialize_bc<OutflowBoundaryCondition<RF>>();
    EXPECT_EQ(bc.type(), Operator::BCType::Outflow);
    EXPECT_EQ(bc.type_str, "outflow");
}
