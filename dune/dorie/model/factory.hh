#ifndef DUNE_DORIE_MODEL_FACTORY_HH
#define DUNE_DORIE_MODEL_FACTORY_HH


#ifndef DORIE_DIM
    #error DORIE_DIM macro must be defined for factory builds
#else
    static_assert(DORIE_DIM == 2 or DORIE_DIM == 3,
                  "DORiE can only be built in 2D or 3D");
#endif

#include <memory>
#include <utility>

#include <dune/common/parametertree.hh>
#include <dune/common/parallel/mpihelper.hh>

#include <dune/geometry/type.hh>

#include <dune/grid/uggrid.hh>
#include <dune/grid/yaspgrid.hh>

#include <dune/dorie/common/setup.hh>
#include <dune/dorie/common/logging.hh>
#include <dune/dorie/common/utility.hh>
#include <dune/dorie/common/grid_creator.hh>
#include <dune/dorie/common/utility.hh>

#include <dune/dorie/model/base.hh>

namespace Dune {
namespace Dorie {

/// Base class for factories building grids and models
/** Derived classes use preprocessor definitions to define the supported
 *  model options.
 *
 *  \ingroup BuildProcess
 *  \author Lukas Riedel
 *  \date 2019
 */
struct ModelFactory
{
protected:
    /// An unstructured grid (simplices & rectangles, adaptive)
    template<int dim>
    using UnstructuredGrid = Dune::UGGrid<dim>;

    /// A structured grid (rectangles only)
    template<int dim>
    using StructuredGrid = Dune::YaspGrid<dim>;

    template<class Grid>
    using BaseTraitsSimplex
        = BaseTraits<Grid,
                     Dune::GeometryType::BasicType::simplex>;

    template<class Grid>
    using BaseTraitsCube
        = BaseTraits<Grid,
                     Dune::GeometryType::BasicType::cube>;

    /// Return the grid settings
    /** \return Pair: Grid type/mode setting, bool if adaptivity is enabled
     */
    static std::pair<GridMode, bool> get_grid_config (
        const Dune::ParameterTree& config
    )
    {
        const auto log = get_logger(log_base);

        // check if adaptivity is enabled
        const auto policy = config.get<std::string>("adaptivity.policy");
        const bool adaptivity_enbaled = not is_none(policy);

        // check grid type
        GridMode grid_mode;
        const auto grid_type = config.get<std::string>("grid.gridType");
        if (grid_type == "gmsh") {
            grid_mode = GridMode::gmsh;
        }
        else if (grid_type == "rectangular") {
            grid_mode = GridMode::rectangular;
        }
        else {
            log->error("Unknown grid type: {}",
                       grid_type);
            DUNE_THROW(IOError, "Unknown grid type");
        }

        return {grid_mode, adaptivity_enbaled};
    }

    /// Return the adaptivity policy read from a config tree
    static AdaptivityPolicy get_adaptivity_policy (
        const Dune::ParameterTree& config
    )
    {
        std::string adapt_policy_str
            = config.get<std::string>("adaptivity.policy");

        if (adapt_policy_str == "waterFlux") {
            return AdaptivityPolicy::WaterFlux;
        }
        else if (adapt_policy_str == "soluteFlux") {
            return AdaptivityPolicy::SoluteFlux;
        }
        else if (is_none(adapt_policy_str)) {
            return AdaptivityPolicy::None;
        }
        else {
            const auto log = get_logger(log_base);
            log->error("Unknown adaptivity policy: {}", adapt_policy_str);
            DUNE_THROW(IOError, "Unknown adaptivity policy");
        }
    }

    /// Raise an error that the FV solver does not support unstructured grids
    static void raise_error_fv_grid ()
    {
        const auto log = get_logger(log_base);
        log->error("The FV solver does not support unstructured grids. "
                   "Use rectangular grids and disable grid adaptivity");
        DUNE_THROW(IOError, "FV solver does not support unstructured grids.");
    }
};

} // namespace Dorie
} // namespace Dune

#endif // DUNE_DORIE_MODEL_FACTORY_HH
