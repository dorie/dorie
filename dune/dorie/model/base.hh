#ifndef DUNE_DORIE_MODEL_BASE_HH
#define DUNE_DORIE_MODEL_BASE_HH

#include <dune/dorie/common/logging.hh>
#include <dune/dorie/common/typedefs.hh>

#include <dune/common/exceptions.hh>
#include <dune/common/float_cmp.hh>

#include <dune/dorie/common/typedefs.hh>

namespace Dune{
namespace Dorie{

/*-------------------------------------------------------------------------*//**
 * @brief      Base class for models.
 * @details    This class is used to handle models with a common interface.
 * @author     Santiago Ospina De Los Ríos
 * @date       2018
 * @todo       Dune::Dorie::OutputPolicy can handle specific intermediate times
 *             of the stepping.
 * @ingroup    Models
 * @ingroup    Collective
 */
class ModelBase
{
public:

  /**
   * @brief      Constructs the ModelBase.
   *
   * @param[in]  log_name  Name of this model. Appears in the log.
   * @param[in]  log_level  The log level of the logger.
   * @param[in]  helper  The Dune::MPIHelper instance of this process.
   * @param[in]  output_policy  The output policy.
   *                            Defaults to OutputPolicy::None.
   * @param[in]  adapt_policy  The adapt policy.
   *                           Defaults to AdaptiviyPolicy::None.
   */
  ModelBase(
    const std::string log_name,
    const std::string log_level,
    const Dune::MPIHelper& helper,
    OutputPolicy output_policy=OutputPolicy::None,
    AdaptivityPolicy adapt_policy=AdaptivityPolicy::None
  )
    : _output_policy(output_policy)
    , _adapt_policy(adapt_policy)
    , _time(0.0)
    , _log(create_logger(log_name, helper, spdlog::level::from_str(log_level)))
  { }

  virtual ~ModelBase () = default;
  /*-----------------------------------------------------------------------*//**
   * @brief      Sets the output policy.
   *
   * @remark     @ref NonCollective
   * @param[in]  output_policy  The output policy,
   */
  void set_policy(OutputPolicy output_policy) {_output_policy = output_policy;}

  /*-----------------------------------------------------------------------*//**
   * @brief      Sets the adaptivity policy.
   *
   * @param[in]  adapt_policy  The adaptivity policy.
   */
  void set_policy(AdaptivityPolicy adapt_policy)
  {
    _adapt_policy = adapt_policy;
  }

  /*-----------------------------------------------------------------------*//**
   * @brief      Returns the current output policy.
   *
   * @remark     @ref NonCollective
   * @return     The current output policy.
   */
  OutputPolicy     output_policy() const {return _output_policy;}


  /*-----------------------------------------------------------------------*//**
   * @brief      Returns the current adaptivity policy.
   *
   * @return     The current adaptivity policy.
   */
  AdaptivityPolicy adaptivity_policy() const {return _adapt_policy;}

  /// Set the logger for this model
  /** @param logger The new logger
   */
  void set_logger (const std::shared_ptr<spdlog::logger> logger)
  {
    _log = logger;
  }

  /// Return the logger of this model
  std::shared_ptr<spdlog::logger> logger() const { return _log; }

  /*-----------------------------------------------------------------------*//**
   * @brief      Writes the data.
   */
  void virtual write_data() const
  {
    if (_output_policy != OutputPolicy::None) {
      _log->error("This model does not implement writing data. "
                  "Override this function if you set a different output policy "
                  "than None");
      DUNE_THROW(Dune::IOError, "'write_data' not implemented!");
    }
  }

  /*-----------------------------------------------------------------------*//**
   * @brief      Mark the grid in order to improve the current model.
   */
  virtual void mark_grid()
  {
    _log->error("This model does not implement an algorithm for marking "
                "the grid");
    DUNE_THROW(Dune::NotImplemented, "'mark_grid' not implemented!");
  }

  /*-----------------------------------------------------------------------*//**
   * @brief      Operations before adaptation of the grid
   */
  virtual void pre_adapt_grid() {};

  /*-----------------------------------------------------------------------*//**
   * @brief      Adapt the grid together it every dependency of the grid (e.g.
   *             solution vector and grid function spaces).
   */
  virtual void adapt_grid()
  {
    if (_adapt_policy != AdaptivityPolicy::None) {
      _log->error("This model does not implement a grid adaptation "
                  "algorithm.");
      DUNE_THROW(Dune::NotImplemented, "'adapt_grid' not implemented");
    }
    else {
      _log->error("Calling 'adapt_grid' on a model with "
                  "AdaptivityPolicy::None. Grid adaptation refused");
      DUNE_THROW(Dune::InvalidStateException, "Invalid adaptation policy");
    }
  }

  /*-----------------------------------------------------------------------*//**
   * @brief      Operations after adaptation of the grid
   */
  virtual void post_adapt_grid() {};

  /*-----------------------------------------------------------------------*//**
   * @brief      Method that provides the begin time of the model.
   *
   * @return     Begin time of the model.
   */
  virtual double begin_time() const = 0;

  /*-----------------------------------------------------------------------*//**
   * @brief      Method that provides the end time of the model.
   *
   * @return     End time of the model.
   */
  virtual double end_time() const = 0;

  /*-----------------------------------------------------------------------*//**
   * @brief      Method that provides the current time of the model.
   *
   * @return     Current time of the model.
   */
  virtual double current_time() const { return _time; }

  /*-----------------------------------------------------------------------*//**
   * @brief      Suggest a time step to the model.
   *
   * @param[in]  dt    { parameter_description }
   * @param[in]  suggestion  for the internal time step of the model.
   */
  virtual void suggest_timestep(double dt) = 0;

  /*-----------------------------------------------------------------------*//**
   * @brief      Performs one steps in direction to end_time(). The time-step
   *             should never result on a bigger step than the one suggested in
   *             suggest_timestep().
   */
  virtual void step() = 0;

  /*-----------------------------------------------------------------------*//**
   * @brief      Runs the model performing steps until current_time() equals
   *             end_time()
   */
  virtual void run()
  {
    if (output_policy() != OutputPolicy::None)
      write_data();

    while( Dune::FloatCmp::lt( current_time(), end_time()) )
    {
      step();

      if (adaptivity_policy() != AdaptivityPolicy::None)
        if ( Dune::FloatCmp::lt( current_time(), end_time()) )
        {
          mark_grid();
          pre_adapt_grid();
          adapt_grid();
          post_adapt_grid();
        }
    }
  }

private:
  OutputPolicy      _output_policy;
  AdaptivityPolicy  _adapt_policy;

protected:
  /// The current time of the model
  double _time;
  //! The logger of this model
  std::shared_ptr<spdlog::logger> _log;
};

/**
@{
  @class ModelBase

  ## Base class for models

  ### Stepping:
  
    The method `step()` must be an abstract method that always has to be 
    implemented by the derived class. The step must succeed always, otherwise, 
    it should throw a `ModelStepException`. It means that time adaptivity  
    must be done internally so that the time step succeeds.

  ### Writing data:

    The method `step()` must be able to decide whether to write data or not. 
    (Say that in one case one wants that transport writes that every step and in 
    another case one wants that it writes at the end of the Richards step. The 
    first case can't be covered writing data in the `run()` method of the 
    coupled model.) Then, enum class called `OutputPolicy`  must decide 
    whether to call the method `write_data()`. This policy can be set and be 
    extracted by the methods `set_policy(OutputPolicy)` and `output_policy()` 
    respectively.

    The method `write_data()` should be in charge of writing the data. 
    In principle, this method should be only used following the `OutputPolicy`. 
    Usually called at the end of each step and at the beginning of the 
    model. Since the `step()` method is in charge of it, it is assumed that 
    this method writes the *current* state of the model (e.g. intermediate 
    results of the coupled model for the transport state).

  ### Adaptivity:

    For the purpose of having a generic `run()` method in this class, the sketch
    of the adaptivity must be provided here. First, adaptivity must be never
    done inside the method `step()`. Additionally, if the model can perform
    adaptivity, it must provide the method `mark_grid()`, otherwise, it must
    throw a `NotImplemented` exception. For completion, there must be a method
    called `adapt_grid()` that pack all the known degrees of freedom and, the
    grid function spaces and perform the adaptivity.

    Similar to the idea of `OutputPolicy` there must be an `AdaptivityPolicy`
    set by the methods `set_policy(AdaptivityPolicy)` and `adaptivity_policy()`
    respectively which will be set by default to `AdaptivityPolicy::None`.

    Having this separation between marking the grid and adaptivity of solutions
    leave coupled models to decide which model will mark the grid inside its
    method of `mark_grid()` and still handle the adaptivity implementing
    `adapt_grid()` separately.

  ### Time control:

    A minimal interface is only providing `begin_time()`, `end_time()` and
    `current_time()`.

    In order to keep solution vectors minimal at the time of adaptivity, DORiE 
    expect the several models synchronized in time. Therefore, a good and still 
    minimal interface is only suggesting the timestep to the model via 
    `suggest_timestep(double dt)`. Such suggestion has to be taken into account 
    within the model, and the model must ensure that the timestep is done in 
    `step()` is never bigger than the suggested one so that coupled models can 
    achieve synchronization. Naturally, models have to ensure that 
    `begin_time()` \f$=\f$ `current_time()` when they are constructed.

  ### Local Operator Setup:

    Since adaptivity can modify the complete setup of the model, and since 
    the method `adapt_grid()` is not necessarily called by the model. 
    There is a need for the method `post_adapt_grid()` which is in charge of
    setup everything again to the new grid. Same argument holds for
    `pre_adapt_grid()`.

  ### Run:

    With all the definitions given before, a general algorithm to run the
    program can be given by following code:

    ```
    virtual void run()
    {
      if (output_policy() != OutputPolicy::None)
        write_data();

      while( current_time() < end_time() )
      {
        step();

        if (adaptivity_policy() != AdaptivityPolicy::None)
          if ( current_time() < end_time() )
          {
            mark_grid();
            pre_adapt_grid();
            adapt_grid();
            post_adapt_grid();
          }
      }
    }
    ```
@}

*/

} // namespace Dorie
} // namespace Dune

#endif // DUNE_DORIE_MODEL_BASE_HH
