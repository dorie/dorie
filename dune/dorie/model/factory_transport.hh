#ifndef DUNE_DORIE_MODEL_FACTORY_TRANSPORT_HH
#define DUNE_DORIE_MODEL_FACTORY_TRANSPORT_HH

#include <dune/dorie/model/factory.hh>

#include <dune/dorie/model/richards/richards.hh>
#include <dune/dorie/model/coupling/richards_coupling.hh>

// Checks for custom setup
#ifndef DORIE_TORDER
    #error DORIE_TORDER macro must be defined for Transport factory builds
#else
    static_assert(DORIE_TORDER >= 0,
                  "Negative polynomial orders do not make sense");
    static_assert(DORIE_TORDER <= 6,
                  "DORiE only supports polynomial orders up to 6");
#endif

#ifdef DORIE_RT_ORDER
    #error DORIE_RT_ORDER must be undefined
#endif

#if DORIE_RORDER == 0
    #define DORIE_RT_ORDER 0
#else
    #define DORIE_RT_ORDER DORIE_RORDER-1
#endif

namespace Dune {
namespace Dorie {

/// A factory for building Transport models
/**
 *  \ingroup TransportModel
 *  \author Lukas Riedel
 *  \date 2019
 */
struct TransportFactory : public ModelFactory
{
private:
    using Base = ModelFactory;

    // -- Retrieve typedefs from the base class

    /// An unstructured grid (simplices & rectangles, adaptive)
    template<int dim>
    using UnstructuredGrid = Base::UnstructuredGrid<dim>;

    /// A structured grid (rectangles only)
    template<int dim>
    using StructuredGrid = Base::StructuredGrid<dim>;

    template<class Grid>
    using BaseTraitsSimplex = Base::BaseTraitsSimplex<Grid>;

    template<class Grid>
    using BaseTraitsCube = Base::BaseTraitsCube<Grid>;

    // -- Model specific typedefs

    template<typename Traits>
    using ModTransport = ModelRichardsTransportCoupling<Traits>;

    /// Convenience typedef for a shorter name
    template<class BaseTraits, int order_richards, int order_transport>
    using TransportTraits
        = ModelRichardsTransportCouplingTraits<BaseTraits,
                                                    order_richards,
                                                    order_transport>;

    template<class Grid, int order_richards, int order_transport>
    using ModTransportSimplex
        = ModTransport<TransportTraits<BaseTraitsSimplex<Grid>,
                                       order_richards,
                                       order_transport>>;

    template<class Grid, int order_richards, int order_transport>
    using ModTransportCube
        = ModTransport<TransportTraits<BaseTraitsCube<Grid>,
                                       order_richards,
                                       order_transport>>;

    /// Verify that the run-time config matches the compile-time settings
    static void check_settings (const Dune::ParameterTree& config)
    {
        const auto dim = config.get<int>("grid.dimensions");
        const auto r_order = config.get<int>("richards.numerics.FEorder");
        const auto t_order = config.get<int>("transport.numerics.FEorder");

        const auto log = get_logger(log_base);
        if (dim != DORIE_DIM
            or r_order != DORIE_RORDER
            or t_order != DORIE_TORDER)
        {
            log->error("Config settings do not match compile-time settings:");
            log->error("  DORIE_DIM: {}, grid.dimensions: {}",
                       DORIE_DIM, dim);
            log->error("  DORIE_RORDER: {}, richards.numerics.FEorder: {}",
                       DORIE_RORDER, r_order);
            log->error("  DORIE_TORDER: {}, transport.numerics.FEorder: {}",
                       DORIE_TORDER, t_order);
            DUNE_THROW(IOError, "Config settings not supported");
        }
    }

    /// Raise an error that flux reconstruction is not available
    static void raise_error_flux_reconstruction ()
    {
        const auto log = get_logger(log_base);
        log->error("Flux reconstruction required for the model coupling does "
                   "not support this combination of dimension and polynomial "
                   "order. See the documentation for details.");
        DUNE_THROW(IOError, "Model coupling does not support config settings.");
    }

public:
    /// Create a Richards Model from input parameters
    static std::shared_ptr<ModelBase> create (
        const Dune::ParameterTree& config,
        Dune::MPIHelper& helper
    )
    {
        check_settings(config);

        // pointer to return
        std::shared_ptr<ModelBase> mod;

        constexpr int dim = DORIE_DIM;
        const auto [grid_mode, grid_adaptive] = get_grid_config(config);
        auto config_richards = Setup::prep_ini_for_richards(config);
        auto config_transport = Setup::prep_ini_for_transport(config);

        if (grid_mode == GridMode::gmsh)
        {
            #if (DORIE_RORDER == 0 || DORIE_TORDER == 0)
                raise_error_fv_grid();
            #elif DORIE_SUPPORT_RT_SIMPLEX(DORIE_DIM, DORIE_RT_ORDER)
                using GridType = UnstructuredGrid<dim>;
                GridCreator<GridType> grid_creator(config, helper);

                mod = std::make_shared<ModTransportSimplex<GridType,
                                                           DORIE_RORDER,
                                                           DORIE_TORDER>>(
                    config_richards, config_transport, grid_creator, helper
                );
            #else
                raise_error_flux_reconstruction();
            #endif
        }
        else // GridMode::rectangular
        {
            #if DORIE_SUPPORT_RT_CUBE(DORIE_DIM, DORIE_RT_ORDER)
                if (grid_adaptive) {
                    #if (DORIE_RORDER == 0 || DORIE_TORDER == 0)
                        raise_error_fv_grid();
                    #else
                        using GridType = UnstructuredGrid<dim>;
                        GridCreator<GridType> grid_creator(config, helper);

                        mod = std::make_shared<ModTransportSimplex<
                            GridType, DORIE_RORDER, DORIE_TORDER>>
                        (
                            config_richards,
                            config_transport,
                            grid_creator,
                            helper
                        );
                    #endif
                }
                // grid not adaptive
                else {
                    using GridType = StructuredGrid<dim>;
                    GridCreator<GridType> grid_creator(config, helper);

                    mod = std::make_shared<ModTransportCube<GridType,
                                                            DORIE_RORDER,
                                                            DORIE_TORDER>>(
                        config_richards, config_transport, grid_creator, helper
                    );
                }
            #else
                raise_error_flux_reconstruction();
            #endif
        }

        // set policy and return
        const auto adaptivity_policy = get_adaptivity_policy(config);
        mod->set_policy(adaptivity_policy);

        return mod;
    }
};

} // namespace Dorie
} // namespace Dune

#endif // DUNE_DORIE_MODEL_FACTORY_TRANSPORT_HH
