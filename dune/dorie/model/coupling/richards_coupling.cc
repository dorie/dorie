#include <dune/dorie/model/coupling/richards_coupling.hh>

#include <dune/pdelab/adaptivity/adaptivity.hh>

namespace Dune{
namespace Dorie{

template<typename Traits>
ModelRichardsTransportCoupling<Traits>::ModelRichardsTransportCoupling(
  Dune::ParameterTree& inifile_richards,
  Dune::ParameterTree& inifile_transport,
  const GridCreator& grid_creator,
  Dune::MPIHelper& helper
) : ModelBase(log_coupling,
                   inifile_transport.get<std::string>("output.logLevel"),
                   helper)
  , _grid(grid_creator.grid())
  , _inifile_richards(inifile_richards)
  , _inifile_transport(inifile_transport)
{
  auto richards_file = _inifile_richards.get<std::string>("output.fileName");
  auto transport_file = _inifile_transport.get<std::string>("output.fileName");

  // avoid output overwriting
  if (richards_file == transport_file) {
    this->_log->error(
        "Clashing names for Richards and Transport output filenames: {}",
        richards_file);
    DUNE_THROW(Dune::IOError,"Clashing names for VTKWriters!");
  }

  this->_log->trace("Setting up Richards model");
  // create richards model
  _richards = std::make_unique<ModelRichards>(_inifile_richards,grid_creator,helper);

  auto time_begin = _richards->begin_time();

  this->_log->trace("Setting up Transport model");
  // set initial water state into the transport model
  auto igf_water_content = std::make_shared<GFWaterContentContainer>();
  auto igf_water_flux    = std::make_shared<GFWaterFluxContainer>();

  auto gf_water_content = _richards->get_water_content();
  igf_water_content->push(gf_water_content,time_begin);

  auto gf_water_flux   = _richards->get_water_flux_reconstructed();
  igf_water_flux->push(gf_water_flux,time_begin);

  // create transport model
  _transport = std::make_unique<ModelTransport>(_inifile_transport,
                                                     grid_creator,
                                                     helper);
  _transport->set_water_flux(igf_water_flux);
  _transport->set_water_content(igf_water_content);

  this->set_policy(_transport->output_policy());
  
  this->_log->info("Setup complete");
}

template<typename Traits>
void ModelRichardsTransportCoupling<Traits>::step()
{ 
  TimeField time_begin = _richards->current_time();

  // store richards sate in case of intermediate adaptivity
  if (adaptivity_policy() == AdaptivityPolicy::SoluteFlux) 
    richards_state_before_step = _richards->current_state();

  // create instationary grid function containers
  auto igf_water_content = std::make_shared<GFWaterContentContainer>();
  auto igf_water_flux    = std::make_shared<GFWaterFluxContainer>();

  // set initial state of the water content to container
  auto gf_water_content_begin = _richards->get_water_content();
  igf_water_content->push(gf_water_content_begin,time_begin);

  // set initial state of the water flux to container
  auto gf_water_flux_begin   = _richards->get_water_flux_reconstructed();
  igf_water_flux->push(gf_water_flux_begin,time_begin);

  // always do an step of richards
  _richards->step();

  TimeField time_end = _richards->current_time();

  // set final state of the water content to container
  auto gf_water_content_end = _richards->get_water_content();
  igf_water_content->push(gf_water_content_end,time_end);

  // set final state of the water flux to container
  auto gf_water_flux_end    = _richards->get_water_flux_reconstructed();
  igf_water_flux->push(gf_water_flux_end,time_end);

  auto _time_transport = _transport->current_time();
  auto _time_richards = _richards->current_time();

  if ( Dune::FloatCmp::ge(_time_transport, _transport->end_time()) )
  {
    this->_time = _time_richards;
    return;
  }

  // set instatonary grid functions to the transport solver
  this->_log->trace("Transfer data from Richards to Transport");
  _transport->set_water_flux(igf_water_flux);
  _transport->set_water_content(igf_water_content);

  // solve transport until synchronization with richards
  while (Dune::FloatCmp::lt(_time_transport,_time_richards))
  {
    _transport->suggest_timestep(_time_richards - _time_transport);
    _transport->step();
    // intermediate adapt if policy targets solute
    if (adaptivity_policy() == AdaptivityPolicy::SoluteFlux and
        Dune::FloatCmp::lt(_transport->current_time(),_transport->end_time()))
    {
        mark_grid();
        adapt_grid();
        post_adapt_grid();
    }

    _time_transport = _transport->current_time();
  }
  this->_time = _time_richards;

  // drop the old grid functions in transport model.
  igf_water_content->pop();
  igf_water_flux->pop();

  // write data in case of not autonomous management in transport
  if (_transport->output_policy() == OutputPolicy::EndOfRichardsStep)
    _transport->write_data();
}

template<typename Traits>
void ModelRichardsTransportCoupling<Traits>::adapt_grid()
{

  this->_log->debug("Adapting grid");
  Dune::Timer timer;

  if (!_grid)
    DUNE_THROW(Dune::InvalidStateException,"Dune grid is in invalid state!");

  if(Traits::BaseTraits::GridGeometryType 
      == Dune::GeometryType::BasicType::cube){
    DUNE_THROW(Dune::NotImplemented,"Coupling between richards and transport "
      << "is not implemented for grids with cubes due to the Raviart Thomas "
      << "flux reconstruction!");
  }

  const int add_int_order = 2;
  const int quadrature_factor = 2;
  const int richards_order = ModelRichardsTraits::order;
  const int transport_order = ModelTransportTraits::order;
  auto richards_int_order = quadrature_factor*richards_order + add_int_order;
  auto transport_int_order = quadrature_factor*transport_order + add_int_order;

  // get current state of both models
  auto richards_state = _richards->current_state();
  auto transport_state = _transport->current_state();

  // get gfs and DOF vector of richards
  auto& richards_gfs = *(richards_state.grid_function_space);
  auto& richards_coeff = *(richards_state.coefficients);

  // get gfs and DOF vector of transport
  auto& transport_gfs = *(transport_state.grid_function_space);
  auto& transport_coeff = *(transport_state.coefficients);

  // pack transport state
  auto transport_pack = Dune::PDELab::transferSolutions(transport_gfs,transport_int_order,transport_coeff);

  if (adaptivity_policy() == AdaptivityPolicy::WaterFlux)
  { // adaptivity on water flux assumes that model are synchronized
    // pack richards state
    auto richards_pack = Dune::PDELab::transferSolutions(richards_gfs,richards_int_order,richards_coeff);
    // adapt grid and packs
    Dune::PDELab::adaptGrid(*_grid,richards_pack,transport_pack);
  } 
  else if (adaptivity_policy() == AdaptivityPolicy::SoluteFlux) 
  { // adaptivity on solute flux assumes that model are not synchronized
    // get DOF vector richards at begging of step
    auto& richards_coeff_before = *(richards_state_before_step.coefficients);
    // pack richards current and before state
    auto richards_pack = Dune::PDELab::transferSolutions(richards_gfs,richards_int_order,richards_coeff,richards_coeff_before);
    // adapt grid and packs
    Dune::PDELab::adaptGrid(*_grid,richards_pack,transport_pack);
  }

  const auto t_adapt = timer.elapsed();
  this->_log->trace("  Grid adaptation: {:.2e}s", t_adapt);
}

template<typename Traits>
void  ModelRichardsTransportCoupling<Traits>::post_adapt_grid()
{
  this->_log->debug("Post-adapt grid");
  // if water state was adapted, containers have to be reset
  if (adaptivity_policy() == AdaptivityPolicy::SoluteFlux)
  {
    // create instationary grid function containers
    auto igf_water_content = std::make_shared<GFWaterContentContainer>();
    auto igf_water_flux    = std::make_shared<GFWaterFluxContainer>();

    // set initial state of the water content to container
    auto gf_water_content_begin 
        = _richards->get_water_content(richards_state_before_step);
    igf_water_content->push(gf_water_content_begin,
                            richards_state_before_step.time);

    // set initial state of the water flux to container
    auto gf_water_flux_begin 
        = _richards->get_water_flux_reconstructed(richards_state_before_step);
    igf_water_flux->push(gf_water_flux_begin,richards_state_before_step.time);

    // set final state of the water content to container
    auto gf_water_content_end 
        = _richards->get_water_content(richards_state_after_step);
    igf_water_content->push(gf_water_content_end,
                            richards_state_after_step.time);

    // set final state of the water flux to container
    auto gf_water_flux_end
        = _richards->get_water_flux_reconstructed(richards_state_after_step);
    igf_water_flux->push(gf_water_flux_end,richards_state_after_step.time);
    
    _transport->set_water_flux(igf_water_flux);
    _transport->set_water_content(igf_water_content);
  }

  // post adapt in richards and transport (e.g. operator setup)
  _richards->post_adapt_grid();
  _transport->post_adapt_grid();
}

} // namespace Dorie
} // namespace Dune
