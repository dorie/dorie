#ifndef DUNE_DORIE_MODEL_COUPLING_HH
#define DUNE_DORIE_MODEL_COUPLING_HH

#include <dune/dorie/common/grid_function_container.hh>

#include <dune/dorie/model/richards/richards.hh>
#include <dune/dorie/model/transport/transport.hh>

#ifdef GTEST
  #include <gtest/gtest.h>
#endif

namespace Dune{
namespace Dorie{

/*-------------------------------------------------------------------------*//**
 * @brief      Traits for the class ModelRichardsTransportCoupling.
 * @details    This class extends BaseTraits to be used for the implementation
 *             of the Richards and Transport models.
 * @author     Santiago Ospina De Los Ríos
 * @date       2018-2019
 * @ingroup    ModelTransport
 * @ingroup    ModelRichards
 *
 * @tparam     BaseTraits      Traits defining domain and range field properties
 *                             of the model.
 * @tparam     RichardsOrder   Order of the polynomial degree used for the basis
 *                             functions in the Richards DG method.
 * @tparam     TransportOrder  Order of the polynomial degree used for the basis
 *                             functions in the Transport DG/FV method.
 */
template<class BaseTraits, unsigned int RichardsOrder, unsigned int TransportOrder>
struct ModelRichardsTransportCouplingTraits : public BaseTraits
{
private:
  using ModelRichardsTraits = Dune::Dorie::ModelRichardsTraits<BaseTraits,RichardsOrder>;
public:
  static_assert(ModelRichardsTraits::enable_rt_engine,
    "Flux reconstruction is not available in the Richards model!");

  /// Container for the water content grid function
  /** \note Interpolates linearly between time steps.
   *  \see GridFunctionContainerPolicy
   */
  using GFWaterContentContainer = GridFunctionContainer<
    const typename ModelRichardsTraits::GFWaterContent,
    double,
    GridFunctionContainerPolicy::LinearInterpolation
  >;

  /// Container for the water flux grid function
  /** \note Returns the next future value
   *  \see GridFunctionContainerPolicy
   */
  using GFWaterFluxContainer = GridFunctionContainer<const typename ModelRichardsTraits::GFFluxReconstruction>;

private:
  using ModelTransportTraits = Dune::Dorie::ModelTransportTraits<
                    BaseTraits,
                    GFWaterFluxContainer,
                    GFWaterContentContainer,
                    TransportOrder
                  >;
public:
  using ModelRichards = Dune::Dorie::ModelRichards<ModelRichardsTraits>;
  using ModelTransport = Dune::Dorie::ModelTransport<ModelTransportTraits>;

  // -- DORiE Class Definitions -- //
  /// Wrapper for grid and volume & boundary mappings
  using GridCreator = Dune::Dorie::GridCreator<typename BaseTraits::Grid>;
};


/*-------------------------------------------------------------------------*//**
 * @brief      Class for richards transport coupling model.
 * @details    For given ModelRichardsTransportCouplingTraits, this class
 *             manages the model for the coupling between transport and
 *             richards models.
 * @author     Santiago Ospina De Los Ríos
 * @date       2018-2019
 * @ingroup    ModelTransport
 * @ingroup    ModelRichards
 * @ingroup    Collective
 *
 * @todo Implement source term.
 *
 * @tparam     T     Traits containing the type definitions which this class
 *                   will use. (see ModelRichardsTransportCouplingTraits)
 */
template<class T>
class ModelRichardsTransportCoupling : public ModelBase
{
public:
  using Traits = T;
private:
  using ModelRichards  = typename Traits::ModelRichards;
  using ModelTransport = typename Traits::ModelTransport;

  using ModelRichardsTraits  = typename ModelRichards::Traits;
  using ModelTransportTraits = typename ModelTransport::Traits;

  using GFWaterContentContainer  = typename Traits::GFWaterContentContainer;
  using GFWaterFluxContainer = typename Traits::GFWaterFluxContainer;

  using GridCreator = typename Traits::GridCreator;
  using TimeField = typename Traits::TimeField;

  using RichardsState = typename ModelRichardsTraits::State;
  
  std::shared_ptr<typename Traits::BaseTraits::Grid> _grid;
  Dune::ParameterTree _inifile_richards, _inifile_transport;

  std::unique_ptr<ModelRichards>  _richards;
  std::unique_ptr<ModelTransport> _transport;

  RichardsState richards_state_before_step, richards_state_after_step;

public:

  /*-----------------------------------------------------------------------*//**
   * @brief      Construct Coupled model.
   *
   * @param      inifile_richards   The richards inifile.
   * @param      inifile_transport  The transport inifile.
   * @param[in]  _grid_creator      The grid creator.
   * @param      helper             The mpi helper.
   *
   * @todo Fix logger warning.
   */
  ModelRichardsTransportCoupling (
    Dune::ParameterTree& inifile_richards,
    Dune::ParameterTree& inifile_transport,
    const GridCreator& _grid_creator,
    Dune::MPIHelper& helper
  );

  ~ModelRichardsTransportCoupling() = default;
  
  /*------------------------------------------------------------------------*//**
   * @brief      Compute a time step. Models the time step requirement of
   *             ModelBase
   *
   * @throws     Dune::Exception  Fatal error occurs during computation
   */
  void step() override;

  /**
   * @brief      Mark the grid in order to improve the current model.
   */
  void mark_grid() override
  {
    // select model and mark the grid
    if (adaptivity_policy() == AdaptivityPolicy::WaterFlux)
    {
      _richards->set_policy(adaptivity_policy());
      _richards->mark_grid();
    } 
    else if (adaptivity_policy() == AdaptivityPolicy::SoluteFlux)
    {
      _transport->set_policy(adaptivity_policy());
      _transport->mark_grid();
    } else if (adaptivity_policy() != AdaptivityPolicy::None)
      DUNE_THROW(Dune::NotImplemented,"Not known adaptivity policy!");
  }

  /**
   * @brief      Operations before adaptation of the grid
   */
  void pre_adapt_grid() override 
  {
    _richards->pre_adapt_grid();
    _transport->pre_adapt_grid();
  };

  /**
   * @brief      Adapt the grid together it every dependency of the 
   *             grid (e.g. solution vector and grid function spaces).
   */
  void adapt_grid() override;

  /**
   * @brief      Operations after adaptation of the grid
   */
  void post_adapt_grid() override;

  /*------------------------------------------------------------------------*//**
   * @brief      Method that provides the begin time of the model.
   *
   * @return     Begin time of the model.
   */
  double begin_time() const override
  {
    return _richards->begin_time();
  }

  /*------------------------------------------------------------------------*//**
   * @brief      Method that provides the end time of the model.
   *
   * @return     End time of the model.
   */
  double end_time() const override
  {
    return _richards->end_time();
  }

  /*------------------------------------------------------------------------*//**
   * @brief      Suggest a time step to the model.
   *
   * @param[in]  dt    Suggestion for the internal time step of the model. The
   *                   new internal time step shall not be bigger than dt
   */
  void suggest_timestep(double dt) override
  {
    _richards->suggest_timestep(dt);
  }

  /*------------------------------------------------------------------------*//**
   * @brief      Write the data using the VTKWriter. VTKWriters is always
   *             cleared after used.
   *
   * @param[in]  time  The time to print in th pvd file.
   */
  void write_data () const override
  {
    if (_richards->output_policy() != OutputPolicy::None)
      _richards->write_data();
    if (_transport->output_policy() != OutputPolicy::None)
      _transport->write_data();
  }

private:

#ifdef GTEST
  FRIEND_TEST(ModelCoupling, TimeSteps);
  FRIEND_TEST(ModelCouplingParallel, Richards);
  FRIEND_TEST(ModelCouplingParallel, Transport);
#endif
};


} // namespace Dorie
} // namespace Dune

#endif // DUNE_DORIE_MODEL_COUPLING_HH
