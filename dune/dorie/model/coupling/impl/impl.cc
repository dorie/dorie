#ifdef HAVE_CONFIG_H
    #include "config.h"
#endif

#ifndef DORIE_DIM
    #error DORIE_DIM must be defined
#endif

#ifndef DORIE_RORDER
    #error DORIE_RORDER must be defined
#endif

#ifndef DORIE_TORDER
    #error DORIE_TORDER must be defined
#endif

#ifdef DORIE_RT_ORDER
    #error DORIE_RT_ORDER must be undefined
#endif

#if DORIE_RORDER == 0
    #define DORIE_RT_ORDER 0
#else
    #define DORIE_RT_ORDER DORIE_RORDER-1
#endif

#include <dune/grid/yaspgrid.hh>
#include <dune/grid/uggrid.hh>

#include <dune/geometry/type.hh>

#include <dune/dorie/common/flux_reconstruction/raviart_thomas.hh>
#include <dune/dorie/model/transport/transport.cc>
#include <dune/dorie/model/coupling/richards_coupling.cc>

namespace Dune{
namespace Dorie{

using Geo = Dune::GeometryType::BasicType;

#if DORIE_SUPPORT_RT_CUBE(DORIE_DIM, DORIE_RT_ORDER)
    template class ModelRichardsTransportCoupling<ModelRichardsTransportCouplingTraits<BaseTraits<YaspGrid<DORIE_DIM>, Geo::cube>,
                                                                                       DORIE_RORDER,
                                                                                       DORIE_TORDER>>;
#else
    #warning Coupling for cubes is not available for this order and dimension. Case (YaspGrid) will be ignored
#endif

#if (DORIE_TORDER > 0) && (DORIE_RORDER > 0)
#if DORIE_SUPPORT_RT_CUBE(DORIE_DIM, DORIE_RT_ORDER)
    template class ModelRichardsTransportCoupling<ModelRichardsTransportCouplingTraits<BaseTraits<UGGrid<DORIE_DIM>, Geo::cube>,
                                                                                        DORIE_RORDER,
                                                                                        DORIE_TORDER>>;
#else
    #warning Coupling for cubes is not available for this order and dimension. Case (UGGrid, cubes) will be ignored
#endif

#if DORIE_SUPPORT_RT_SIMPLEX(DORIE_DIM, DORIE_RT_ORDER)
    template class ModelRichardsTransportCoupling<ModelRichardsTransportCouplingTraits<BaseTraits<UGGrid<DORIE_DIM>, Geo::simplex>,
                                                                                        DORIE_RORDER,
                                                                                        DORIE_TORDER>>;
#else
    #warning Coupling for simplices is not available for this order. Case (UGGrid, simplices) will be ignored
#endif
#endif

} // namespace Dorie
} // namespace Dune
