#ifndef DUNE_DORIE_RICHARDS_OPERATOR_FV_HH
#define DUNE_DORIE_RICHARDS_OPERATOR_FV_HH

#include <dune/common/float_cmp.hh>

#include <dune/geometry/referenceelements.hh>

#include <dune/pdelab/common/quadraturerules.hh>
#include <dune/pdelab/localoperator/pattern.hh>
#include <dune/pdelab/localoperator/flags.hh>
#include <dune/pdelab/localoperator/idefault.hh>
#include <dune/pdelab/localoperator/defaultimp.hh>

#include <dune/dorie/common/typedefs.hh>
#include <dune/dorie/common/boundary_condition/neumann.hh>
#include <dune/dorie/model/richards/local_operator_cfg.hh>

namespace Dune {
namespace Dorie {
namespace Operator {

/*-------------------------------------------------------------------------*//**
 * @brief      Spatial local operator for the Richards equation in a finite 
 *             volume scheme.
 * @details    It solves the spatial part of the Richards equation.
 * 
 * @author     Santiago Ospina De Los Ríos
 * @date       2019
 * @ingroup    LocalOperators
 * @ingroup    RichardsModel
 * 
 * @tparam     Parameter                 Type of the class providing parameters
 * @tparam     Boundary                  Type of the class providing boundary
 *                                       conditions
 */
template<class Parameter, class Boundary>
class RichardsFVSpatialOperator
  : public Dune::PDELab::NumericalJacobianVolume<
              RichardsFVSpatialOperator<Parameter,Boundary>>
  , public Dune::PDELab::NumericalJacobianSkeleton<
              RichardsFVSpatialOperator<Parameter,Boundary>>
  , public Dune::PDELab::NumericalJacobianBoundary<
              RichardsFVSpatialOperator<Parameter,Boundary>>
  , public Dune::PDELab::FullSkeletonPattern
  , public Dune::PDELab::FullVolumePattern
  , public Dune::PDELab::LocalOperatorDefaultFlags
  , public Dune::PDELab::InstationaryLocalOperatorDefaultMethods<double>
{
public:
  // Pattern assembly flags
  enum { doPatternVolume = true };
  enum { doPatternSkeleton = true };

  // Residual assembly flags
  enum { doAlphaBoundary = true };
  enum { doAlphaSkeleton  = true };


  /**
   * @brief      Constructor of RichardsFVSpatialOperator
   *
   * @param[in]  param     The parameter class
   * @param[in]  boundary  The boundary class
   */
  RichardsFVSpatialOperator(
    const std::shared_ptr<const Parameter>& param,
    const std::shared_ptr<const Boundary>& boundary,
    const RichardsUpwinding upwinding = RichardsUpwinding::none
  ) : _param(param)
    , _boundary(boundary)
    , _upwinding(upwinding)
    , _time(0.)
  {}

   /*----------------------------------------------------------------------*//**
    * @brief      Skeleton integral depending on test and ansatz functions. Each
    *             face is only visited once since this method is symmetric
    *
    * @param[in]  ig      The intersection entity of the grid (inside + outside
    *                     entities)
    * @param[in]  lfsu_i  The inside ansatz local function space
    * @param[in]  x_i     The coefficients of the lfsu_i
    * @param[in]  lfsv_i  The inside test local function space
    * @param[in]  lfsu_o  The outside ansatz local function space
    * @param[in]  x_o     The coefficients of the lfsu_o
    * @param[in]  lfsv_o  The outside test local function space
    * @param[out] r_i     The view of the residual vector w.r.t lfsu_i
    * @param[out] r_o     The view of the residual vector w.r.t lfsu_o
    *
    * @tparam     IG      The type for ig
    * @tparam     LFSU    The type for lfsu_i and lfsu_o
    * @tparam     X       The type for x_i and x_o
    * @tparam     LFSV    The type for lfsv_i and lfsv_o
    * @tparam     R       The type for r_i and r_o
    */
  template<typename IG, typename LFSU, typename X, typename LFSV, typename R>
  void alpha_skeleton(const IG& ig, 
    const LFSU& lfsu_i, const X& x_i, const LFSV& lfsv_i,
    const LFSU& lfsu_o, const X& x_o, const LFSV& lfsv_o, 
    R& r_i, R& r_o) const
  {
    // Get local basis traits from local function space
    using LocalBasisTraitsU = typename LFSU::Traits::FiniteElementType::
                        Traits::LocalBasisType::Traits;

    // Get range, range field and gradient for trial space
    using RangeU = typename LocalBasisTraitsU::RangeType;

    // Assert that polynomial degree is always 0
    assert(lfsu_i.finiteElement().localBasis().order()==0);
    assert(lfsu_o.finiteElement().localBasis().order()==0);
    assert(lfsv_i.finiteElement().localBasis().order()==0);
    assert(lfsv_o.finiteElement().localBasis().order()==0);

    // Define entities for each frame of reference
    const auto& entity_f = ig.intersection();
    const auto& entity_i = ig.inside();
    const auto& entity_o = ig.outside();

    // Get geometries
    const auto geo_f = entity_f.geometry();
    const auto geo_i = entity_i.geometry();
    const auto geo_o = entity_o.geometry();

    // Get volume of entities
    const auto volume_f = geo_f.volume();

    // Get normal
    const auto normal_f = entity_f.centerUnitOuterNormal();

    // Entity centers in global coordinates
    const auto center_position_i_g = geo_i.center();
    const auto center_position_o_g = geo_o.center();

    // Distance between the two entity centers
    const auto center_position_diff = center_position_i_g
                                      - center_position_o_g;
    const auto distance = center_position_diff.two_norm();

    // Inside/outside solute value
    const RangeU u_i = x_i(lfsu_i,0);
    const RangeU u_o = x_o(lfsu_o,0);

    // Finite difference of u between the two entities
    RangeU dudn = (u_o-u_i) / distance;

    // Update gradient with gravity vector
    dudn -= _param->gravity() * normal_f;

    // bind parameterization and retrieve functions
    _param->bind(entity_i);
    const auto saturation_f_i = _param->saturation_f();
    const auto conductivity_f_i = _param->conductivity_f();
    
    _param->bind(entity_o);
    const auto saturation_f_o = _param->saturation_f();
    const auto conductivity_f_o = _param->conductivity_f();

    // Compute the conductivity
    const RangeU cond_i = conductivity_f_i(saturation_f_i(u_i));
    const RangeU cond_o = conductivity_f_o(saturation_f_o(u_o));

    // Interface conductivity factor (Upwinding applies)
    RangeU cond_f(0.0);
    if (_upwinding == RichardsUpwinding::none)
    {
      // Harmonic average for interface conductivity
      cond_f = 2.0/(   1.0/(cond_i + 1E-30)
                     + 1.0/(cond_o + 1E-30) );
    }
    else if (_upwinding == RichardsUpwinding::semiUpwind)
    {
      // Harmonic average of saturated conductivities
      const RangeU cond_sat_i = conductivity_f_i(1.0);
      const RangeU cond_sat_o = conductivity_f_o(1.0);

      // Upwinding conductivity factor
      RangeU cond_upwind_factor(0.0);
      // NOTE: Sign seems inverted due to definition of 'water_flux_n' below.
      if (dudn > 0.0) {
        // inward flux
        cond_upwind_factor = cond_o / cond_sat_o;
      }
      else {
        // outward flux
        cond_upwind_factor = cond_i / cond_sat_i;
      }

      cond_f = 2.0 * cond_upwind_factor / (   1.0/(cond_sat_i + 1E-30)
                                            + 1.0/(cond_sat_o + 1E-30) );
    }
    else // RichardsFVUpwinding::fullUpwind
    {
      // No average: Use upwind conductivity
      if (dudn > 0.0) {
        // inward flux
        cond_f = cond_o;
      }
      else {
        // outward flux
        cond_f = cond_i;
      }
    }

    // Water flux in normal direction w.r.t the intersection
    const auto water_flux_n = - cond_f * dudn;

    // Symmetric contribution to residual on inside element
    r_i.accumulate(lfsv_i, 0,  water_flux_n*volume_f );
    r_o.accumulate(lfsv_o, 0, -water_flux_n*volume_f );
  }

  /*-----------------------------------------------------------------------*//**
   * @brief      Boundary integral depending on test and ansatz functions.
   *
   * @param[in]  ig      The intersection entity of the grid (inside + outside
   *                     entities)
   * @param[in]  lfsu_i  The inside ansatz local function space
   * @param[in]  x_i     The coefficients of the lfsu_i
   * @param[in]  lfsv_i  The inside test local function space
   * @param[out] r_i     The view of the residual vector w.r.t lfsu_i
   *
   * @tparam     IG      The type for ig
   * @tparam     LFSU    The type for lfsu_i
   * @tparam     X       The type for x_i
   * @tparam     LFSV    The type for lfsv_i
   * @tparam     R       The type for r_i
   */
  template<typename IG, typename LFSU, typename X, typename LFSV, typename R>
  void alpha_boundary (const IG& ig, const LFSU& lfsu_i, const X& x_i,
                       const LFSV& lfsv_i, R& r_i) const
  {
    // Get local basis traits from local function space
    using LocalBasisTraitsU = typename LFSU::Traits::FiniteElementType::
                        Traits::LocalBasisType::Traits;

    // Get range, range field and gradient for trial space
    using RangeU = typename LocalBasisTraitsU::RangeType;
    using RangeFieldU = typename LocalBasisTraitsU::RangeFieldType;
    
    // Assert that polynomial degree is always 0
    assert(lfsu_i.finiteElement().localBasis().order()==0);
    assert(lfsv_i.finiteElement().localBasis().order()==0);

    // Get entities
    const auto& entity_f = ig.intersection(); 
    const auto& entity_i = ig.inside();

    // Get geometries
    const auto& geo_f = entity_f.geometry();

    // Face volume for integration
    const auto volume_f = geo_f.volume();

    // Normal vector of intersection
    const auto normal_f = ig.centerUnitOuterNormal();

    // query the boundary condition
    const auto bc = _boundary->bc(ig.intersection());

    // bind parameterization and retrieve functions
    _param->bind(entity_i);
    const auto saturation_f_i = _param->saturation_f();
    const auto conductivity_f_i = _param->conductivity_f();

    const auto bc_type = bc->type();
    if (bc_type == BCType::Dirichlet or bc_type == BCType::Outflow)
    {
      const auto geo_i = entity_i.geometry();
      const auto center_position_i_g = geo_i.center();

      // Face center in global coordinates
      const auto center_position_f_g = geo_f.center();

      // Compute distance of these two points
      const auto position_diff = center_position_i_g - center_position_f_g;
      const auto distance = position_diff.two_norm();

      // Evaluate Dirichlet condition
      const auto g = bc->evaluate(_time);

      // Inside unknown value
      const RangeU u_i = x_i(lfsu_i,0);

      // Compute the conductivity
      const RangeU cond_i = conductivity_f_i(saturation_f_i(u_i));

      // Finite difference of u between the two entities
      RangeU dudn = (g-u_i)/distance;

      // Update gradient with gravity vector
      dudn -= _param->gravity()*normal_f;

      // Water flux in normal direction w.r.t the intersection
      const auto water_flux_n = - cond_i*dudn;

      // Avoid accumulation of inflow for outflow BC
      if (bc_type == BCType::Outflow and
          FloatCmp::lt(double(water_flux_n), 0.0)) {
        return;
      }

      // Contribution to residual from Dirichlet boundary
      r_i.accumulate(lfsv_i, 0, water_flux_n*volume_f);
    }
    else if (bc_type == BCType::Neumann)
    {
      // Contribution to residual from Neumann boundary
      RangeFieldU water_flux_f = bc->evaluate(_time);

      // adjust flux to surface tilt
      const auto& bc_neumann
        = static_cast<const NeumannBoundaryCondition<RangeFieldU>&>(*bc);
      if (bc_neumann.horizontal_projection())
        water_flux_f *= std::abs( normal_f * _param->gravity() );

      r_i.accumulate(lfsv_i, 0, water_flux_f*volume_f );
    }
  }

  /*-----------------------------------------------------------------------*//**
   * @brief      Sets the time.
   *
   * @param[in]  t           time of type RangeField
   *
   * @tparam     RangeField  type of the range field
   */
  template<class RangeField>
  void setTime (RangeField t)
  {
    _time = t;
  }

private:
  /// Class providing equation parameters
  const std::shared_ptr<const Parameter> _param;
  /// Class providing boundary conditions
  const std::shared_ptr<const Boundary> _boundary;
  /// Upwinding setting
  const RichardsUpwinding _upwinding;
  /// Operator internal time
  double _time;
};

/**
 * @brief      Temporal local operator Richards equation in a finite volume
 *             scheme.
 * @details    It solves the temporal part of the Richards equation.
 * @author     Santiago Ospina De Los Ríos
 * @date       2018
 * @ingroup    LocalOperators
 * @ingroup    RichardsModel
 *
 * @tparam     Parameter                 Type of the class providing parameters
 */
template<class Parameter>
  class RichardsFVTemporalOperator
  : public Dune::PDELab::NumericalJacobianVolume<
              RichardsFVTemporalOperator<Parameter>>
  , public Dune::PDELab::NumericalJacobianApplyVolume<
              RichardsFVTemporalOperator<Parameter>>
  , public Dune::PDELab::FullSkeletonPattern
  , public Dune::PDELab::FullVolumePattern
  , public Dune::PDELab::LocalOperatorDefaultFlags
  , public Dune::PDELab::InstationaryLocalOperatorDefaultMethods<double>
{
public:

  // Pattern assembly flags
  enum { doPatternVolume = true };

  // Residual assembly flags
  enum { doAlphaVolume = true };

public:

  /**
   * @brief      Constructor of RichardsFVTemporalOperator
   *
   * @param[in]  param     The parameter class
   */
  RichardsFVTemporalOperator(const std::shared_ptr<const Parameter>& param)
    : _param(param)
    , _time(0.)
  {}

  /*-----------------------------------------------------------------------*//**
   * @brief      Volume integral depending on test and ansatz functions
   *
   * @param[in]  eg    The entity of the grid
   * @param[in]  lfsu  The ansatz local function space
   * @param[in]  x     The coefficients of the lfsu
   * @param[in]  lfsv  The test local function space
   * @param[out] r     The view of the residual vector w.r.t lfsu
   *
   * @tparam     EG    The type for eg
   * @tparam     LFSU  The type for lfsu
   * @tparam     X     The type for x
   * @tparam     LFSV  The type for lfsv
   * @tparam     R     The type for r
   */
  template<typename EG, typename LFSU, typename X, typename LFSV, typename R>
  void alpha_volume (const EG& eg, const LFSU& lfsu, const X& x, 
                     const LFSV& lfsv, R& r) const
  {
    // Get local basis traits from local function space
    using LocalBasisTraitsU = typename LFSU::Traits::FiniteElementType::
                        Traits::LocalBasisType::Traits;
    // Get range for trial space
    using RangeU = typename LocalBasisTraitsU::RangeType;
    const RangeU u = x(lfsu,0);

    // entity geometry
    const auto geo = eg.geometry();

    // bind parameterization and retrieve functions
    _param->bind(eg.entity());
    const auto saturation_f = _param->saturation_f();
    const auto water_content_f = _param->water_content_f();

    // Calculate water content from matric head
    const RangeU water_content = water_content_f(saturation_f(u));

    // update residual
    r.accumulate(lfsv ,0 , water_content*geo.volume());
  }

  /**
   * @brief      Sets the time.
   *
   * @param[in]  t     time of type RangeField
   *
   * @tparam     RF    Type of the range field
   */
  template<class RF>
  void setTime (RF t)
  {
    _time = t;
  }

private:
  /// class providing equation parameters
  const std::shared_ptr<const Parameter> _param;
  double _time;
};


} // namespace Operator
} // namespace Dorie
} // namespace Dune


#endif // DUNE_DORIE_RICHARDS_OPERATOR_FV_HH
