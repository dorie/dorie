#ifndef DUNE_DORIE_RICHARDS_INITIAL_CONDITION
#define DUNE_DORIE_RICHARDS_INITIAL_CONDITION

#include <memory>
#include <string>

#include <dune/common/exceptions.hh>

#include <dune/dorie/common/initial_condition/factory.hh>
#include <dune/dorie/model/richards/adapters/matric_head.hh>

namespace Dune {
namespace Dorie {

/// Transforms water content into matric head, serving as initial condition
template<typename Traits, typename Param>
class WaterContentInitialCondition :
    public InitialConditionTransformation<Traits>
{
public:
  using ICTransf = InitialConditionTransformation<Traits>;
  using GFTraits = typename ICTransf::Traits;

  using Base = InitialCondition<Traits>;

protected:
  using GV = typename ICTransf::GV;
  using MHA = WaterContentToMatricHeadAdapter<Traits, Param, Base>;

public:

    /// Construct the transformation from a grid view and an initial condition
    WaterContentInitialCondition (const GV& grid_view,
                                  std::shared_ptr<Base> ic,
                                  const std::shared_ptr<const Param> param ):
        ICTransf(grid_view, ic),
        _adapter(std::make_shared<MHA>(grid_view, ic, param))
    { }

    void evaluate ( const typename GFTraits::ElementType& e,
                    const typename GFTraits::DomainType& x,
                    typename GFTraits::RangeType& y) const override
    {
        _adapter->evaluate(e, x, y);
    };

private:
    std::shared_ptr<MHA> _adapter;
};


/// An initial condition factory for the Richards solver
/**
 *  \tparam Traits
 *  \tparam Param
 *
 *  \ingroup InitialConditions
 *  \author Lukas Riedel
 *  \date 2019
 */
template<typename Traits, typename Param>
class RichardsInitialConditionFactory : public InitialConditionFactory<Traits>
{
private:
    using Base = InitialConditionFactory<Traits>;

public:
    /// Create an initial condition for the Richards solver
    /**
     *  \note In case the user wants the initial condition to be computed from
     *        the stationary problem, an empty pointer is returned.
     */
    static std::unique_ptr<InitialCondition<Traits>> create (
        const Dune::ParameterTree& config,
        const typename Traits::GridView& grid_view,
        const std::shared_ptr<const Param> param,
        const std::shared_ptr<spdlog::logger> log=get_logger(log_richards)
    )
    {
        using IC = InitialCondition<Traits>;

        if (config.get<std::string>("initial.type") == "stationary") {
            // NOTE: Return empty pointer to communicate that the IC is
            //       computed internally.
            return std::unique_ptr<IC>();
        }

        const auto quantity = config.get<std::string>("initial.quantity");
        // matric head: no transformation necessary
        if (quantity == "matricHead") {
            return Base::create(config, grid_view, log);
        }
        else if (quantity == "waterContent") {
            // create initial condition grid function from input data
            std::shared_ptr<IC> ic_gf = Base::create(config, grid_view, log);

            // plug into adapter
            using WCIC = WaterContentInitialCondition<Traits, Param>;
            return std::make_unique<WCIC>(grid_view, ic_gf, param);
        }
        else {
            log->error("Unsupported quantity for initial condition: {}",
                       quantity);
            DUNE_THROW(NotImplemented,
                       "Unsupported initial condition quantity");
        }
    }
};

} // namespace Dorie
} // namespace Dune

#endif // DUNE_DORIE_RICHARDS_INITIAL_CONDITION
