// -*- tab-width: 4; indent-tabs-mode: nil -*-
#ifndef DUNE_DORIE_RICHARDS_SOURCE_HH
#define DUNE_DORIE_RICHARDS_SOURCE_HH

#include <dune/common/parametertree.hh>

namespace Dune{
  namespace Dorie{
    /**
    * @brief Sink and source terms for flow equation
    *
    * This class implements sinks (q<0) and sources (q>0) inside of the grid.
    */
    template<typename Traits>
    class FlowSource
    {

      private:

        typedef typename Traits::RangeField         RF;
        typedef typename Traits::Domain             Domain;
        typedef typename Traits::Element            Element;

        enum {dim=Traits::dim};

        const ParameterTree& config; //!< Parameter File Parser

      public:

        FlowSource(const Dune::ParameterTree& config_)
          : config(config_)
        {}


        /// Return value of the sink/source at certain position and time
        /** \param elem Element enitity
        * \param x Position in local entity coordinates
        * \param time Time value
        * \return Sink/source term value
        * \todo To do: make head available in q to calculate root water uptake dependent of the potential,
        *  return correct values
        */
        RF q (const Element& elem, const Domain& x, RF time) const
        {

          return 0.0;
        }

    };
  }
}

#endif
