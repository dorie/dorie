#ifndef DUNE_DORIE_CONDUCTIVITY_ADAPTER_HH
#define DUNE_DORIE_CONDUCTIVITY_ADAPTER_HH

#include <dune/pdelab/common/function.hh>

namespace Dune{
  namespace Dorie{

    /*---------------------------------------------------------------------*//**
     * @brief      Converts a parametrization P that contains conductivity
     *             information into a grid function (in PDELab sense)
     * @ingroup    RichardsParam
     * 
     * @tparam     T     BasicTraits with information about the function domain
     * @tparam     P     Parametrization class with saturated conductivity
     *                   information
     */
    template<typename T, typename P>
    class ConductivityAdapter
      : public Dune::PDELab::GridFunctionBase<
                      Dune::PDELab::GridFunctionTraits<
                        typename T::GridView, 
                        typename T::RangeField,
                        1, 
                        typename T::Scalar 
                      >
                      ,ConductivityAdapter<T,P> 
                    >
    {
    public:
      using Traits = Dune::PDELab::GridFunctionTraits<
                          typename T::GridView,
                          typename T::RangeField,
                          1, 
                          typename T::Scalar
                        >;

    private:
      using RF     = typename T::RangeField;
      using Domain = typename T::Domain;

    public:
      /*-------------------------------------------------------------------*//**
       * @brief      Constructor for the ConductivityAdapter
       *
       * @param      gv    GridView
       * @param      p     Parametrization class
       * @see        RichardsEquationParameter
       */
      ConductivityAdapter(const typename Traits::GridViewType& gv,
                          std::shared_ptr<const P> p)
        : _gv(gv)
        , _p(p)
      {}

      /*-------------------------------------------------------------------*//**
       * @brief      Evaluation of the conductivity for a given entity e in an
       *             entity position x
       *
       * @param[in]  e     Entity of a grid
       * @param[in]  x     Position in local coordinates with respect the entity
       * @param      y     Saturated conductivity at position x
       */
      void evaluate ( const typename Traits::ElementType& e,
                      const typename Traits::DomainType& x,
                            typename Traits::RangeType& y) const
      {
        // bind to entity
        _p->bind(e);
        // evaluate saturated conductivity
        y = _p->conductivity_f()(1.0);
      }

      /*-------------------------------------------------------------------*//**
       * @brief      Function that returns a grid view valid for this grid
       *             function
       *
       * @return     Grid view
       */
      const typename Traits::GridViewType& getGridView() const
      {
        return _gv;
      }
      
    private:
      const typename Traits::GridViewType _gv;
      const std::shared_ptr<const P> _p;
    };

  }
}

#endif