#ifndef DUNE_DORIE_WATER_CONTENT_ADAPTER_HH
#define DUNE_DORIE_WATER_CONTENT_ADAPTER_HH

#include <dune/pdelab/common/function.hh>

namespace Dune{
  namespace Dorie{

    /*---------------------------------------------------------------------*//**
     * @brief      Converts a parametrization P that contains conductivity
     *             information and a matric head grid function GF into a grid
     *             function (in PDELab sense) of water content.
     * @ingroup    RichardsParam
     * 
     * @tparam     T     BasicTraits with information about the function domain
     * @tparam     P     Parametrization class with conductivity information
     * @tparam     GF    Grid function of the matric head
     */
    template<typename T, typename P, typename GF>
    class WaterContentAdapter
      : public Dune::PDELab::GridFunctionBase<
                        Dune::PDELab::GridFunctionTraits<
                          typename T::GridView,
                          typename T::RangeField,
                          1, 
                          typename T::Scalar 
                        >
                      ,WaterContentAdapter<T,P,GF> 
                    >
    {
    public:
      using Traits = Dune::PDELab::GridFunctionTraits<
                        typename T::GridView,
                        typename T::RangeField,
                        1, 
                        typename T::Scalar
                      >;

    private:
      using Domain  = typename T::Domain;
      using RF      = typename T::RangeField;

    public:
      /*-------------------------------------------------------------------*//**
       * @brief      Constructor for the WaterContentAdapter
       *
       * @param      gf_h  The gf h
       * @param      gv    GridView
       * @param      p     Parametrization class
       * @see        RichardsEquationParameter
       */
      WaterContentAdapter(std::shared_ptr<const GF> gf_h,
                          const typename Traits::GridViewType& gv,
                          std::shared_ptr<const P> p)
        : _gf_h(gf_h)
        , _gv(gv)
        , _p(p)
      {}

      /*-------------------------------------------------------------------*//**
       * @brief      Evaluation of the water content for a given entity e in an
       *             entity position x
       *
       * @param[in]  e     Entity of a grid
       * @param[in]  x     Position in local coordinates with respect the entity
       * @param      y     Water content at position x
       */
      void evaluate ( const typename Traits::ElementType& e,
                      const typename Traits::DomainType& x,
                            typename Traits::RangeType& y) const
      {
        typename Traits::RangeType head;
        _gf_h->evaluate(e, x, head);

        // Calculate water content
        _p->bind(e);
        auto saturation_f = _p->saturation_f();
        y = _p->water_content_f()(saturation_f(head));
      }

      /*-------------------------------------------------------------------*//**
       * @brief      Function that returns a grid view valid for this grid
       *             function
       *
       * @return     Grid view
       */
      const typename Traits::GridViewType& getGridView() const
      {
        return _gv;
      }

    private:
      const std::shared_ptr<const GF> _gf_h;
      const typename Traits::GridViewType _gv;
      const std::shared_ptr<const P> _p;
    };
  }
}

#endif
