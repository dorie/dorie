#ifndef DUNE_DORIE_RICHARDS_ADAPTERS_HEAD_HH
#define DUNE_DORIE_RICHARDS_ADAPTERS_HEAD_HH

#include <memory>
#include <algorithm>
#include <dune/pdelab/common/function.hh>

/// An adapter for translating water content to matric head
/** \tparam T BaseTraits
 *  \tparam GF Grid function representing water content
 *  \tparam P Type of the parameter interface
 */
template<typename T, typename P, typename GF>
class WaterContentToMatricHeadAdapter :
    public Dune::PDELab::GridFunctionBase<
            Dune::PDELab::GridFunctionTraits<typename T::GridView,
                                             typename T::RF,
                                             1,
                                             typename T::Scalar>,
            WaterContentToMatricHeadAdapter<T, GF, P>
           >
{
public:
    using Traits = Dune::PDELab::GridFunctionTraits<typename T::GridView,
                                                    typename T::RF,
                                                    1,
                                                    typename T::Scalar>;

private:
    using GV = typename Traits::GridViewType;
    using RF = typename Traits::RangeType;

    const GV& _gv;
    const std::shared_ptr<const GF> _gf;
    const std::shared_ptr<const P> _param;

public:
    /// Constructor
    /*
     * \param config Dune config file tree
     * \param gv GridView
     * \param gf Grid function to interpolate from
     * \param param Parameter object
     */
    WaterContentToMatricHeadAdapter (const GV& gv,
                                     const std::shared_ptr<const GF> gf,
                                     const std::shared_ptr<const P> param)
    :
        _gv(gv),
        _gf(gf),
        _param(param)
    { }

    /**
    * \brief Evaluation of grid function at certain position
    * \param e Element entity
    * \param x Position in local entity coordinates
    * \param y Return value
    * \return Value of grid function
    */
    void evaluate (const typename Traits::ElementType& e,
                   const typename Traits::DomainType& x,
                   typename Traits::RangeType& y) const
    {
        // evaluate water content
        typename Traits::RangeType water_content;
        _gf->evaluate(e, x, water_content);

        // evaluate matric head
        _param->bind(e);
        const auto saturation_f = _param->water_content_f_inv();
        const auto matric_head_f = _param->matric_head_f();

        const auto sat = saturation_f(water_content);
        y = matric_head_f(sat);
    }

    const GV& getGridView () const
    {
        return _gv;
    }
};

#endif // DUNE_DORIE_RICHARDS_ADAPTERS_HEAD_HH
