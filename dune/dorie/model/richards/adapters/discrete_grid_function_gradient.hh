#ifndef DUNE_DORIE_DISCRETE_GRID_FUNCTION_GRADIENTS_ADAPTER_HH
#define DUNE_DORIE_DISCRETE_GRID_FUNCTION_GRADIENTS_ADAPTER_HH

#include <vector>
#include <memory>

#include <dune/pdelab/common/function.hh>
#include <dune/pdelab/gridfunctionspace/localfunctionspace.hh>
#include <dune/pdelab/gridfunctionspace/lfsindexcache.hh>


namespace Dune{
  namespace Dorie{

    /*---------------------------------------------------------------------*//**
     * @brief      Converts a single component function space with a grid
     *             function representing the gradient
     *
     * @tparam     GFS   GridFunctionSpace type
     * @tparam     X     CoefficientVector type
     */
    template<typename GFS, typename X>
    class DiscreteGridFunctionGradient
      : public Dune::PDELab::DiscreteGridFunctionGradient<GFS,X>
    {

      using Base = Dune::PDELab::DiscreteGridFunctionGradient<GFS,X>;

    public:

      /*-------------------------------------------------------------------*//**
       * @brief      Construct a DiscreteGridFunctionGradient
       *
       * @param      gfs   The GridFunctionsSpace
       * @param      x     The coefficients vector
       */
      DiscreteGridFunctionGradient(const std::shared_ptr<const GFS> gfs, std::shared_ptr<const X> x)
        : Base(*gfs,*x)
        , pgfs(gfs)
        , px(x)
      {}

    private:
      const std::shared_ptr<const GFS> pgfs;
      const std::shared_ptr<const X> px;
    };

  }
}

#endif