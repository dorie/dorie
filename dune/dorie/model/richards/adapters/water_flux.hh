#ifndef DUNE_DORIE_RICHARDS_WATER_FLUX_ADAPTER_HH
#define DUNE_DORIE_RICHARDS_WATER_FLUX_ADAPTER_HH

#include <dune/pdelab/common/function.hh>
#include <dune/dorie/model/richards/adapters/discrete_grid_function_gradient.hh>

namespace Dune{
  namespace Dorie{

    /*---------------------------------------------------------------------*//**
     * @brief      Converts an grid function space GFS and some particular
     *             coefficients for such space X to match the water flux
     *             equation with a GridFunctionInterface.
     * @ingroup    RichardsParam
     * 
     * @tparam     GFS   Grid Function Space
     * @tparam     X     Coefficients for the GFS
     * @tparam     P     Parameters
     */
    template<typename GFS, typename X, typename P>
    class WaterFluxAdapter : public Dune::PDELab::GridFunctionInterface<
                                          typename Dune::Dorie::DiscreteGridFunctionGradient<GFS,X>::Traits,
                                          WaterFluxAdapter<GFS,X,P>
                                        >
    {
      using DiscreteGridFunction = Dune::PDELab::DiscreteGridFunction<GFS,X>;
      using DiscreteGridFunctionGradient = Dune::Dorie::DiscreteGridFunctionGradient<GFS,X>;
      using Parameters = P;

    public:
      using Traits = typename Dune::Dorie::DiscreteGridFunctionGradient<GFS,X>::Traits;

      /*-------------------------------------------------------------------*//**
       * @brief      Constructor for the WaterFluxAdapter
       *
       * @param[in]  gfs   Shared pointer to a Grid Function Space
       * @param[in]  x     Shared pointer to a coefficients for a gfs
       * @param[in]  p     Parameters for the richards equation
       */
      WaterFluxAdapter(const std::shared_ptr<const GFS> gfs,
                       const std::shared_ptr<const X> x,
                       const std::shared_ptr<const Parameters> p)
        : _gfs(gfs)
        , _p(p)
        , _x(x)
        , _dgf_h(_gfs,_x)
        , _dgf_h_grad(_gfs,_x)
      {}

      /*-------------------------------------------------------------------*//**
       * @brief      Evaluation of the water flux for a given entity e in an
       *             entity position x
       *
       * @param[in]  e     Entity of a grid
       * @param[in]  x     Position in local coordinates with respect the entity
       * @param      y     Water content at position x
       */
      void evaluate ( const typename Traits::ElementType& e,
                      const typename Traits::DomainType& x,
                            typename Traits::RangeType& y) const
      {
        using Scalar = typename DiscreteGridFunction::Traits::RangeType;

        // evaluate gradient
        _dgf_h_grad.evaluate(e, x, y);
        y -= _p->gravity();

        // evaluate matric head
        Scalar head;
        _dgf_h.evaluate(e, x, head);

        // get parameterization functions
        _p->bind(e);
        auto saturation_f = _p->saturation_f();
        auto conductivity_f = _p->conductivity_f();

        y *= - conductivity_f(saturation_f(head));
      }

      /*-------------------------------------------------------------------*//**
       * @brief      Function that returns a grid view valid for this grid
       *             function
       *
       * @return     Grid view
       */
      const typename Traits::GridViewType& getGridView() const
      {
        return _dgf_h.getGridView();
      }

    private:
      const std::shared_ptr<const GFS>            _gfs;
      const std::shared_ptr<const Parameters>     _p;
      const std::shared_ptr<const X>              _x;
      const DiscreteGridFunction                  _dgf_h;
      const DiscreteGridFunctionGradient          _dgf_h_grad;
    };

  }
}

#endif