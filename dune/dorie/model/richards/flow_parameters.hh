#ifndef DUNE_DORIE_PARAM_INTERFACE_HH
#define DUNE_DORIE_PARAM_INTERFACE_HH

#include <cmath>
#include <vector>
#include <unordered_map>
#include <utility>
#include <functional>
#include <memory>

#include <yaml-cpp/yaml.h>

#include <dune/common/exceptions.hh>
#include <dune/grid/common/mcmgmapper.hh>

#include <dune/dorie/common/logging.hh>
#include <dune/dorie/model/richards/parameterization/factory.hh>
#include <dune/dorie/model/richards/parameterization/scaling.hh>

namespace Dune {
namespace Dorie {

/// Top-level parameterization interface for the local operator.
/** This class loads and stores the parameters, maps them to grid entities,
 *  and provides functions to access the parameterization. For doing so, the
 *  FlowParameters::bind function has to be called first to cache the
 *  parameters of the given grid entity. Parameters are only stored for entities
 *  of codim 0.
 *
 *  The parameterization consists of three structures:
 *      - This top-level class serves as interface and storage. It also casts to
 *          data types used by the DUNE operators
 *      - The Dorie::RichardsParameterization interface that provides strong
 *          data types for the base parameters and purely virtual functions that
 *          need to be defined by derived parameterizations
 *      - The actual parameterization defining all parameters and functions
 *
 *  \warning Load-balancing the grid *after* this object has been instantiated
 *      is currently not supported.
 *
 *  \todo Make this object support load-balancing. This would require a global
 *      ID mapper and communicating the data in the _param storage. The latter
 *      is rather involved because of non-standard data types like maps and
 *      Dorie::ScalingFactors
 *
 *  \author Lukas Riedel
 *  \date 2018
 *  \ingroup RichardsParam
 */
template <typename Traits>
class FlowParameters
{
private:
    using RF = typename Traits::RF;
    using Grid = typename Traits::Grid;
    using LevelGridView = typename Grid::LevelGridView;
    using Mapper = typename Dune::MultipleCodimMultipleGeomTypeMapper<
        LevelGridView
    >;
    using Vector = typename Traits::Vector;

    // Define the storage and cache for parameterizations and scaling
    /// Index type used by the element mapper
    using Index = typename Mapper::Index;
    /// Base class of the parameterization
    using ParameterizationType = Dorie::Parameterization::Richards<Traits>;
    /// Parameterization factory
    using ParameterizationFactory = Dorie::Parameterization::RichardsFactory<Traits>;
    /// Struct for storing scaling factors
    using Scaling = Dorie::Parameterization::ScalingFactors<RF>;
    /// Base class for scaling adapters
    using ScaleAdapter = Dorie::Parameterization::ScalingAdapter<Traits>;

    /// Storage for parameters and skaling factors
    using ParameterStorage = std::unordered_map<Index,
        std::tuple<std::shared_ptr<ParameterizationType>, Scaling>
    >;

    /// Need this gruesome typedef because 'map::value_type' has const key
    using Cache = std::tuple<std::shared_ptr<ParameterizationType>, Scaling>;

    using ConstCache = std::tuple<std::shared_ptr<const ParameterizationType>, Scaling>;
    /// Configuration file tree
    const Dune::ParameterTree& _config;
    /// Grid view of the coarsest grid configuration (level 0)
    const LevelGridView _gv;
    /// Logger for this instance
    const std::shared_ptr<spdlog::logger> _log;
    /// Mapper for mapping grid elements to indices
    Mapper _mapper;
    /// Map for storing parameterization information for each grid entity
    ParameterStorage _param;
    /// Currently cached parameterization (bound to element)
    mutable Cache _cache;
    /// Gravity vector
    Vector _gravity;

private:

    /// Check if an entity has been cached
    void verify_cache () const
    {
        if (not std::get<std::shared_ptr<ParameterizationType>>(_cache)) {
            _log->error("Parameterization cache is empty. Call 'bind' before "
                        "accessing the cache or the parameterization");
            DUNE_THROW(Dune::InvalidStateException,
                       "No parameterization cached");
        }
    }

public:
    /// Copy constructor for flow parameters.
    /** Create a level grid view of level 0, create a mapper on it, and store
     *  the config tree.
     */
    FlowParameters (
        const FlowParameters& flow_param
    ):
        _config(flow_param._config),
        _gv(flow_param._gv),
        _log(flow_param._log),
        _mapper(flow_param._mapper),
        _gravity(flow_param._gravity)
    {
        // copy parameterization map
        this->_param.clear();
        for(const auto& [index, tuple] : flow_param._param) {
            const auto& [p, sk] = tuple;
            // make a hard copy of parameterization
            std::shared_ptr<Parameterization::Richards<Traits>> _p = p->clone();
            this->_param.emplace(index,std::make_tuple(_p,sk));
        }
    }

    /// Copy constructor
    /** Create a level grid view of level 0, create a mapper on it, and store
     *  the config tree.
     *  \param config Configuration file tree
     *  \param grid Shared pointer to the grid
     *  \param element_index_map The mapping from grid entity index to
     *      parameterization index.
     */
    FlowParameters (
        const Dune::ParameterTree& config,
        const std::shared_ptr<Grid> grid,
        const std::vector<int>& element_index_map
    ):
        _config(config),
        _gv(grid->levelGridView(0)),
        _log(Dorie::get_logger(log_richards)),
        _mapper(_gv, Dune::mcmgElementLayout()),
        _gravity(0.0)
    {
        _gravity[Traits::dim - 1] = -1.0;
        build_parameterization(element_index_map);
    }

    /// Return normalized gravity vector
    const Vector& gravity() const { return _gravity; }

    /// Return a copy of the current cache with constant parameterization
    ConstCache cache() const
    {
        verify_cache();
        return _cache;
    }

    /// Return the current cache
    const Cache& cache()
    {
        verify_cache();
        return _cache;
    }

    /// Bind to a grid entity. Required to call parameterization functions!
    /** \param entity Grid entity (codim 0) to bind to
     */
    template <class Entity>
    void bind (Entity entity) const
    {
        static_assert(Entity::codimension == 0,
                      "Parameters are mapped to codim 0 entities only!");

        // retrieve index of top father element of chosen entity
        while(entity.hasFather()) {
            entity = entity.father();
        }
        const auto index = _mapper.index(entity);
        _cache = _param.find(index)->second;
    }

    /// Return a scaled version of the Conductivity function
    /** Uses the function of the underlying parameterization and applies
     *  scaling as specified by SkalingType. Cast to operator-internal RF.
     *  \return Function: Saturation -> Conductivity
     */
    std::function<RF(RF)> conductivity_f () const
    {
        verify_cache();
        const auto& par =
            std::get<std::shared_ptr<ParameterizationType>>(_cache);
        const auto cond_f = par->conductivity_f();
        using Saturation = typename ParameterizationType::Saturation;

        const auto& xi_cond = std::get<Scaling>(_cache).scale_cond;

        return [cond_f, &xi_cond](const RF saturation){
            return cond_f(Saturation{saturation}).value * xi_cond * xi_cond;
        };
    }

    /// Return a scaled version of the Saturation function
    /** Uses the function of the underlying parameterization and applies
     *  scaling as specified by SkalingType. Cast to operator-internal RF.
     *  \return Function: Matric Head -> Saturation
     */
    std::function<RF(RF)> saturation_f () const
    {
        verify_cache();
        const auto& par =
            std::get<std::shared_ptr<ParameterizationType>>(_cache);
        const auto sat_f = par->saturation_f();
        using MatricHead = typename ParameterizationType::MatricHead;

        const auto& scale_head = std::get<Scaling>(_cache).scale_head;

        return [sat_f, &scale_head](const RF matric_head){
            return sat_f(MatricHead{matric_head * scale_head}).value;
        };
    }

    /// Return a scaled version of the Water Content function
    /** Uses the function of the underlying parameterization and applies
     *  scaling as specified by SkalingType. Cast to operator-internal RF.
     *  \return Function: Saturation -> Water Content
     */
    std::function<RF(RF)> water_content_f () const
    {
        verify_cache();
        const auto& par =
            std::get<std::shared_ptr<ParameterizationType>>(_cache);

        // get water content function and apply the scaling
        const auto& scale_por = std::get<Scaling>(_cache).scale_por;
        const auto wc_f = par->water_content_f(scale_por);

        using Saturation = typename ParameterizationType::Saturation;
        return [wc_f](const RF saturation) {
            return wc_f(Saturation{saturation}).value;
        };
    }

    /// Return a scaled version of the inverse Water Content function
    /** Uses the function of the underlying parameterization and applies
     *  scaling as specified by ScalingType. Cast to operator-internal RF.
     *  \return Function: Water content -> Saturation
     *  \see water_content_f() for the inverse function
     */
    std::function<RF(RF)> water_content_f_inv () const
    {
        verify_cache();
        const auto& par =
            std::get<std::shared_ptr<ParameterizationType>>(_cache);

        // get water content function and apply the scaling
        const auto& scale_por = std::get<Scaling>(_cache).scale_por;
        const auto wc_f_inv = par->water_content_f_inv(scale_por);

        using WaterContent = typename ParameterizationType::WaterContent;
        return [wc_f_inv](const RF water_content) {
            return wc_f_inv(WaterContent{water_content}).value;
        };
    }

    /// Return a scaled version of the Saturation function
    /** Uses the function of the underlying parameterization and applies
     *  scaling as specified by ScalingType. Cast to operator-internal RF.
     *  \return Function: Saturation -> Matric head
     *  \see saturation_f() for the inverse function
     */
    std::function<RF(RF)> matric_head_f () const
    {
        verify_cache();
        const auto& par =
            std::get<std::shared_ptr<ParameterizationType>>(_cache);
        const auto head_f = par->matric_head_f();
        using Saturation = typename ParameterizationType::Saturation;

        const auto& scale_head = std::get<Scaling>(_cache).scale_head;

        return [head_f, &scale_head](const RF saturation){
            return head_f(Saturation{saturation}).value / scale_head;
        };
    }


private:

    /// Build the parameterization from an element mapping and the input file.
    void build_parameterization (const std::vector<int>& element_index_map)
    {
        // Open the YAML file
        const auto param_file_name = _config.get<std::string>("parameters.file");
        _log->info("Loading parameter data file: {}", param_file_name);
        YAML::Node param_file = YAML::LoadFile(param_file_name);
        // create the parameterization data
        ParameterizationFactory factory;
        const auto parameterization_map = factory.reader(param_file,"richards",_log);

        // build global scaling
        using SAF = Dorie::Parameterization::ScalingAdapterFactory<Traits>;
        auto scale_cfg = param_file["scaling"];
        auto scaling_adapter = SAF::create(scale_cfg["type"].as<std::string>(),
                                           scale_cfg["data"],
                                           _gv.grid().leafGridView());

        // insert parameterization and global scaling for each element
        for (auto&& elem : elements(_gv))
        {
            // evaluate element index and position
            const auto index = _mapper.index(elem);
            const auto pos = elem.geometry().center();
            // read values and insert
            const auto p = parameterization_map.at(element_index_map.at(index));
            const auto scale = scaling_adapter->evaluate(pos);
            _param.emplace(index,std::make_tuple(p, scale));
        }
        // check that mapper can map to parameterization
        assert(_param.size() == _mapper.size());
    }
};

} // namespace Dune
} // namespace Dorie

#endif // DUNE_DORIE_PARAM_INTERFACE_HH
