#ifndef DUNE_DORIE_ERROR_INDICATOR_HH
#define DUNE_DORIE_ERROR_INDICATOR_HH

#include <vector>

#include <dune/common/fvector.hh>
#include <dune/common/parametertree.hh>

#include <dune/geometry/referenceelements.hh>

#include <dune/pdelab/common/quadraturerules.hh>
#include <dune/pdelab/localoperator/pattern.hh>
#include <dune/pdelab/localoperator/flags.hh>
#include <dune/pdelab/localoperator/idefault.hh>
#include <dune/pdelab/localoperator/defaultimp.hh>
#include <dune/pdelab/finiteelement/localbasiscache.hh>

#include <dune/dorie/common/typedefs.hh>
#include <dune/dorie/model/richards/local_operator_cfg.hh>

namespace Dune {
  namespace Dorie {
  namespace Operator {

    /// Local operator for residual-based error estimation.
    /*
     * A call to residual() of a grid operator space will assemble
     * the quantity \eta_T for each cell.
     * Skeleton integral is mostly the same as in the DG operator.
     *  
     * @ingroup    LocalOperators
     * @ingroup    RichardsModel
     */
    template<typename Traits, typename Parameter, typename Boundary>
    class WaterFluxErrorEstimator
      : public Dune::PDELab::LocalOperatorDefaultFlags
    {

    public:

      //typedef typename Parameter::Traits::GridTraits GridTraits;

      enum { dim = Traits::dim };

      typedef typename Traits::GridView           GridView;
      typedef typename Traits::RangeField         RF;
      typedef typename Traits::Scalar             Scalar;
      typedef typename Traits::Vector             Vector;
      typedef typename Traits::DomainField        DF;
      typedef typename Traits::Domain             Domain;
      typedef typename Traits::IntersectionDomain ID;

      // pattern assembly flags
      enum { doPatternVolume = false };
      enum { doPatternSkeleton = false };

      // residual assembly flags
      enum { doAlphaSkeleton  = true };
      enum { doAlphaBoundary  = true };

      const Parameter& param;
      const Boundary& boundary;
      const int intorderadd;
      const int quadrature_factor;
      RF penalty_factor;
      RF time;

      WaterFluxErrorEstimator (const Dune::ParameterTree& config,
        const Parameter& param_, const Boundary& boundary_,
        RichardsDGMethod method_ = RichardsDGMethod::SIPG,
        int intorderadd_ = 2, int quadrature_factor_ = 2)
      : param(param_), boundary(boundary_),
        intorderadd(intorderadd_), quadrature_factor(quadrature_factor_),
        penalty_factor(config.get<RF>("numerics.penaltyFactor")),
        time(0.0)
      {
        if (method_ == RichardsDGMethod::OBB)
          penalty_factor = 0.0;
      }

      /// skeleton integral depending on test and ansatz functions
      /** each face is only visited ONCE!
       */
      template<typename IG,
               typename LFSU,
               typename X,
               typename LFSV,
               typename R>
      void alpha_skeleton (const IG& ig,
                           const LFSU& lfsu_s, const X& x_s, const LFSV& lfsv_s,
                           const LFSU& lfsu_n, const X& x_n, const LFSV& lfsv_n,
                           R& r_s, R& r_n) const
      {
        // get polynomial degree
        const int order_s  = lfsu_s.finiteElement().localBasis().order();
        const int order_n  = lfsu_n.finiteElement().localBasis().order();
        const int degree   = std::max( order_s, order_n );
        const int intorder = intorderadd + quadrature_factor * degree;

        // geometric factor of penalty
        const RF h_F = std::min(ig.inside().geometry().volume(),ig.outside().geometry().volume())/ig.geometry().volume(); // Houston!

        // get element geometry
        auto gtface = ig.geometry();

        // container for sum of errors
        RF sum(0.0);

        // bind parameterization and retrieve functions
        param.bind(ig.inside());
        const auto saturation_f_s = param.saturation_f();
        const auto conductivity_f_s = param.conductivity_f();

        param.bind(ig.outside());
        const auto saturation_f_n = param.saturation_f();
        const auto conductivity_f_n = param.conductivity_f();

        // loop over quadrature points and integrate normal flux
        for (const auto& it : quadratureRule(gtface,intorder))
        {
          // retrieve unit normal vector
          const Dune::FieldVector<DF,dim> n_F = ig.unitOuterNormal(it.position());

          // position of quadrature point in local coordinates of elements
          const Domain local_s = ig.geometryInInside().global(it.position());
          const Domain local_n = ig.geometryInOutside().global(it.position());

          // evaluate basis functions
          std::vector<Scalar> phi_s(lfsu_s.size());
          std::vector<Scalar> phi_n(lfsu_n.size());
          lfsu_s.finiteElement().localBasis().evaluateFunction(local_s,phi_s);
          lfsu_n.finiteElement().localBasis().evaluateFunction(local_n,phi_n);

          // evaluate u
          RF u_s = 0., u_n = 0.;
          for (unsigned int i = 0; i<lfsu_s.size(); i++)
            u_s += x_s(lfsu_s,i) * phi_s[i];
          for (unsigned int i = 0; i<lfsu_n.size(); i++)
            u_n += x_n(lfsu_n,i) * phi_n[i];

          // evaluate gradient of basis functions (we assume Galerkin method lfsu = lfsv)
          typedef typename LFSU::Traits::FiniteElementType::Traits
            ::LocalBasisType::Traits::JacobianType JacobianType;
          std::vector<JacobianType> gradphi_s(lfsu_s.size());
          std::vector<JacobianType> gradphi_n(lfsu_n.size());
          lfsu_s.finiteElement().localBasis().evaluateJacobian(local_s,gradphi_s);
          lfsu_n.finiteElement().localBasis().evaluateJacobian(local_n,gradphi_n);

          // transform gradients to real element
          Dune::FieldMatrix<DF,dim,dim> jac;
          std::vector<Vector> tgradphi_s(lfsu_s.size());
          std::vector<Vector> tgradphi_n(lfsu_n.size());
          jac = ig.inside().geometry().jacobianInverseTransposed(local_s);
          for (unsigned int i = 0; i<lfsu_s.size(); i++)
            jac.mv(gradphi_s[i][0],tgradphi_s[i]);
          jac = ig.outside().geometry().jacobianInverseTransposed(local_n);
          for (unsigned int i = 0; i<lfsu_n.size(); i++)
            jac.mv(gradphi_n[i][0],tgradphi_n[i]);

          // compute gradient of u
          Vector gradu_s(0.), gradu_n(0.);
          for (unsigned int i = 0; i<lfsu_s.size(); i++)
            gradu_s.axpy(x_s(lfsu_s,i),tgradphi_s[i]);
          for (unsigned int i = 0; i<lfsu_n.size(); i++)
            gradu_n.axpy(x_n(lfsu_n,i),tgradphi_n[i]);

          // retrieve conductivity
          const RF cond_s = conductivity_f_s(saturation_f_s(u_s));
          const RF cond_n = conductivity_f_n(saturation_f_n(u_n));

          // compute jump in solution
          const RF h_jump = u_s - u_n;

          // apply gravity vector
          gradu_s -= param.gravity();
          gradu_n -= param.gravity();

          // penalty term
          const RF penalty = penalty_factor / h_F * degree * (degree + dim - 1);

          // integration factor
          const RF factor = it.weight() * ig.geometry().integrationElement(it.position());

          const RF grad_jump = (cond_s * (n_F * gradu_s))
                               - (cond_n * (n_F * gradu_n));

          const RF gamma = 2.0 * cond_s * cond_n / ( cond_s + cond_n );
          const RF head_jump = gamma * penalty * h_jump;

          const RF total_jump = grad_jump/2.0 + head_jump;

          sum += total_jump * total_jump * factor;
        }

        DF h_T_s = diameter(ig.inside().geometry());
        DF h_T_n = diameter(ig.outside().geometry());

        DF C_P_T = 1.0 / M_PI;
        DF C_F_T_s = h_T_s * ig.geometry().volume() / ig.inside().geometry().volume();
        C_F_T_s *= C_P_T * (2.0 / dim + C_P_T);
        DF C_F_T_n = h_T_n * ig.geometry().volume() / ig.outside().geometry().volume();
        C_F_T_n *= C_P_T * (2.0 / dim + C_P_T);

        // accumulate indicator
        r_s.accumulate(lfsv_s,0, h_T_s * C_F_T_s * sum );
        r_n.accumulate(lfsv_n,0, h_T_n * C_F_T_n * sum );
      }

    template<typename IG, typename LFSU, typename X, typename LFSV, typename R>
    void alpha_boundary (const IG& ig,
        const LFSU& lfsu_s, const X& x_s, const LFSV& lfsv_s,
        R& r_s) const
    {
      // query the boundary condition
      const auto bc = boundary.bc(ig.intersection());

      // check boundary condition type
      if (bc->type() != BCType::Dirichlet){
        return;
      }

      // get polynomial degree
      const int degree = lfsu_s.finiteElement().localBasis().order();
      const int intorder = intorderadd + quadrature_factor * degree;

      // geometric factor of penalty
      const RF h_F = ig.inside().geometry().volume() / ig.geometry().volume(); // Houston!

      // get element geometry
      auto gtface = ig.geometry();

      // container for sum of errors
      RF sum(0.0);

      // bind parameterization and retrieve functions
      param.bind(ig.inside());
      const auto saturation_f_s = param.saturation_f();
      const auto conductivity_f_s = param.conductivity_f();

      // loop over quadrature points and integrate normal flux
      for (const auto& it : quadratureRule(gtface,intorder))
      {
        // position of quadrature point in local coordinates of elements
        const Domain local_s = ig.geometryInInside().global(it.position());

        // evaluate basis functions
        std::vector<Scalar> phi_s(lfsu_s.size());
        lfsu_s.finiteElement().localBasis().evaluateFunction(local_s,phi_s);

        // evaluate u
        RF u_s = 0.;
        for (unsigned int i = 0; i<lfsu_s.size(); i++)
          u_s += x_s(lfsu_s,i) * phi_s[i];

        // boundary condition at position
        const RF g = bc->evaluate(time);

        // compute jump in solution
        const RF h_jump = u_s - g;

        // penalty term
        const RF penalty = penalty_factor / h_F * degree * (degree + dim - 1);

        // integration factor
        const RF factor = it.weight() * ig.geometry().integrationElement(it.position());

        // query conductivity
        const RF cond_s = conductivity_f_s(saturation_f_s(u_s));

        const RF head_jump = cond_s * penalty * h_jump;

        sum += head_jump * head_jump * factor;
      }

      DF h_T_s = diameter(ig.inside().geometry());

      DF C_P_T = 1.0 / M_PI;
      DF C_F_T_s = h_T_s * ig.geometry().volume() / ig.inside().geometry().volume();
      C_F_T_s *= C_P_T * (2.0 / dim + C_P_T);

      // accumulate indicator
      r_s.accumulate(lfsv_s,0, h_T_s * C_F_T_s * sum );
    }

    private:
      //! compute diameter of a grid cell
      template<class GEO>
      typename GEO::ctype diameter (const GEO& geo) const
      {
        typedef typename GEO::ctype DF;
        DF hmax = -1.0E00;
        const int dim = GEO::coorddimension;
        for (int i=0; i<geo.corners(); i++)
          {
            Dune::FieldVector<DF,dim> xi = geo.corner(i);
            for (int j=i+1; j<geo.corners(); j++)
              {
                Dune::FieldVector<DF,dim> xj = geo.corner(j);
                xj -= xi;
                hmax = std::max(hmax,xj.two_norm());
              }
          }
        return hmax;
      }

    public:
      //! set operator time
      void setTime (RF t)
      {
        time = t;
      }

    };

  } // namespace Operator
  } // namespace Dorie
} // namespace Dune

#endif // DUNE_DORIE_ERROR_INDICATOR_HH
