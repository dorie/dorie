#ifndef DUNE_DORIE_RICHARDS_ADAPTIVITY_HANDLER_HH
#define DUNE_DORIE_RICHARDS_ADAPTIVITY_HANDLER_HH

#include <iostream>
#include <string>
#include <memory>
#include <type_traits>

#include <dune/common/exceptions.hh>
#include <dune/common/ios_state.hh>
#include <dune/common/parametertree.hh>
#include <dune/common/timer.hh>

#include <dune/grid/uggrid.hh>

#include <dune/pdelab/backend/istl.hh>
#include <dune/pdelab/adaptivity/adaptivity.hh>

#include <dune/dorie/common/logging.hh>
#include <dune/dorie/model/richards/adaptivity/operator_error_indicator.hh>

namespace Dune{
namespace Dorie{


/// Adaptivity base class. Does nothing.
template<typename Traits, typename GFS, typename Param, typename Boundary, typename U, int order>
class RichardsAdaptivityHandler
{
private:
    using Grid = typename Traits::Grid;
    using GV = typename Traits::GV;
    using RF = typename Traits::RF;

public:
    RichardsAdaptivityHandler () = default;
    virtual ~RichardsAdaptivityHandler () = default;

    /// Do nothing.
    virtual void mark_grid (Grid& grid, GV& gv, GFS& gfs, const Param& param, const Boundary& boundary, const RF time, U& u) { }

    /// Do nothing.
    virtual void adapt_grid (Grid& grid, GFS& gfs, U& u) { }
};

/// Specialized AdaptivityHandlerBase class providing functions and members for adaptive grid refinement
template<typename Traits,
	typename GFS,
	typename Param,
	typename Boundary,
	typename U,
	int order>
class WaterFluxAdaptivityHandler : public RichardsAdaptivityHandler<Traits,GFS,Param,Boundary,U,order>
{
private:
	using RF = typename Traits::RF;
	using Grid = typename Traits::Grid;
	using GV = typename Traits::GV;

	/// Helper class for Adaptivity GFS. implements Problem GFS in order 0
	using AGFSHelper = GridFunctionSpaceHelper<GV,RF,0,Traits::GridGeometryType>;
	/// Adaptivity GFS
	using AGFS = typename AGFSHelper::Type;
	/// Error estimator local operator
	using ESTLOP = Dune::Dorie::Operator::WaterFluxErrorEstimator<Traits,Param,Boundary>;
	/// Empty constraints container
	using NoTrafo = Dune::PDELab::EmptyTransformation;
	/// Solution vector type
	using U0 = Dune::PDELab::Backend::Vector<AGFS,RF>;
	/// Matrix backend type
	using MBE = Dune::PDELab::ISTL::BCRSMatrixBackend<>;
	/// Grid operator for Error LOP
	using ESTGO = Dune::PDELab::GridOperator<GFS,AGFS,ESTLOP,MBE,RF,RF,RF,NoTrafo,NoTrafo>;

private:

	const Dune::ParameterTree& inifile; //!< Parameter file parser
	const int maxLevel; //!< Maximum local grid refinement level
	const int minLevel; //!< Minimum local grid refinement level
	const std::string strategy; //!< Refinement strategy
	const double alpha; //!< Refinement factor
	const double beta; //!< Coarsening factor
	const float adaptivityThreshold; //!< Global error threshold below which no refinement is applied
	const std::shared_ptr<spdlog::logger> _log; //!< Logger for this instance
	float total_time; //!< accumulated time of grid marking and pgrid

public:

	/// Initialize members from config file parameters.
	/** Disable grid closure for cubic grid.
	 *  \param _inifile Parameter file parser
	 *  \param grid Grid to adapt (reference is not saved)
	 */
	WaterFluxAdaptivityHandler (Dune::ParameterTree& _inifile, Grid& grid)
		: RichardsAdaptivityHandler<Traits,GFS,Param,Boundary,U,order>(),
		inifile(_inifile),
		maxLevel(inifile.get<int>("adaptivity.maxLevel")),
		minLevel(inifile.get<int>("adaptivity.minLevel")),
		strategy(inifile.get<std::string>("adaptivity.markingStrategy")),
		alpha(inifile.get<float>("adaptivity.refinementFraction")),
		beta(inifile.get<float>("adaptivity.coarseningFraction")),
		adaptivityThreshold(inifile.get<float>("adaptivity.threshold")),
		_log(spdlog::get(log_richards)),
		total_time(0.)
	{
		if(strategy!="elementFraction" && strategy!="errorFraction" && strategy!="threshold")
			DUNE_THROW(Dune::IOError,"Valid values of adaptivity.markingStrategy: elementFraction, errorFraction, threshold");

		// QkDG space cannot handle triagles arising from closure of adapted grid
		if(Traits::GridGeometryType == Dune::GeometryType::BasicType::cube){
			grid.setClosureType(Traits::Grid::ClosureType::NONE);
		}
	}

	/// Mark grid cells for adaptation.
	/** Marking is based on the estimated flux error and the strategy applied.
	 *  \param grid Grid to mark (and later adapt)
	 *  \param gv Current grid view
	 *  \param gfs Solution GridFunctionSpace
	 *  \param param Parameter interface
	 *  \param boundary Boundary condition interface
	 *  \param time The time for which boundary conditions are queried inside
	 * 		the error estimator operator.
	 *  \param u Solution vector
	 */
	void mark_grid (
		Grid& grid,
		GV& gv,
		GFS& gfs,
		const Param& param,
		const Boundary& boundary,
		const RF time,
		U& u) override
	{
		const int verbose = 0; // reduce verbosity of DUNE for now
		_log->debug("Marking grid for adaptation with strategy '{}'",
					strategy);

		double eta_alpha(0.0);
		double eta_beta(0.0);
		double maxeta(0.0);

		// set up helper GFS
		AGFS p0gfs = AGFSHelper::create(gv);
		ESTLOP estlop(inifile, param, boundary);
		estlop.setTime(time);
		MBE mbe(0);
		ESTGO estgo(gfs,p0gfs,estlop,mbe);
		U0 eta(p0gfs,0.0);

		// Compute error estimate eta
		Dune::Timer timer;
		estgo.residual(u, eta);
		const auto t_est = timer.elapsed();

		// unsquare errors
		using Dune::PDELab::Backend::native;
		auto& eta_nat = native(eta);
		std::transform(eta_nat.begin(), eta_nat.end(), eta_nat.begin(),
			[](const auto val) { return std::sqrt(val); }
		);

		// compute largest error
		maxeta = eta.infinity_norm();
		_log->trace("  Largest local error: {:.2e}m/s", maxeta);

		// Apply marking strategy
		timer.reset();
		if (strategy == "elementFraction")
			Dune::PDELab::element_fraction(eta, alpha, beta, eta_alpha, eta_beta, verbose);
		else if (strategy == "errorFraction")
			Dune::PDELab::error_fraction(eta, alpha, beta, eta_alpha, eta_beta, verbose);
		else { //((strategy == "threshold") || (strategy == "targetTolerance"))
			eta_alpha = alpha;
			eta_beta  = beta;
		}

		// Skip refinement if threshold is met, but still allow coarsening
		if (maxeta < adaptivityThreshold) {
			eta_alpha = maxeta + 1; // Only refine elements with error > maxerror + 1
			_log->trace("  ...against threshold {:.2e}m/s. Coarsening only.",
						adaptivityThreshold);
		}

		// mark the damn thing
		Dune::PDELab::mark_grid(grid, eta, eta_alpha, eta_beta, minLevel, maxLevel, verbose);

		// report
		const auto t_mark = timer.elapsed();
		_log->trace("  Error estimation:   {:.2e}s", t_est);
		_log->trace("  Marking grid cells: {:.2e}s", t_mark);
	}

	/// Adapt the grid, and update grid function space and solution.
	/** This assumes that the grid has been marked for refinement. Otherwise,
	 *  calling this function has no effect.
	 *  After adapting the grid, the grid function space is updated and the
	 * 	solution vector is interpolated onto the new grid.
	 *  \param grid The (marked) grid to adapt
	 *  \param gfs The solution grid function space
	 *  \param u The solution vector to be interpolated
	 */
	void adapt_grid (
		Grid& grid,
		GFS& gfs,
		U& u) override
	{
		_log->debug("Adapting grid");

		Dune::Timer timer;
		Dune::PDELab::adapt_grid(grid, gfs, u, 2*order);
		const auto t_adapt = timer.elapsed();

		_log->trace("  Grid adaptation: {:.2e}s", t_adapt);
	}
};

/// Factory class for creating an AdaptivityHandler based on Traits
/** \tparam Traits Base traits for models
 *  The constexpr boolean adapt_grid in Dune::Dorie::BaseTraits will
 *  define which create() function is enabled at compile time.
 */
template<typename Traits, typename GFS, typename Param, typename Boundary, typename U, int order>
struct RichardsAdaptivityHandlerFactory
{
private:

	/*---------------------------------------------------------------------*//**
	 * @brief      Helper to assert wheter a grid is adaptable.
	 *
	 * @tparam     GridType  Type of the grid
	 */
	template<class GridType>
	struct isAdaptable : std::false_type {};

	// The only known adaptable grid for dorie is UGGrid


	/*---------------------------------------------------------------------*//**
	 * @brief      Helper to assert wheter a grid is adaptable. UGGrid
	 *             specialization
	 *
	 * @tparam     dim   Dimension of the UGGrid
	 */
	template<int dim>
	struct isAdaptable<Dune::UGGrid<dim>> : std::true_type {};

	using Grid = typename Traits::Grid;

	using AHB = RichardsAdaptivityHandler<Traits,GFS,Param,Boundary,U,order>;

	Dune::ParameterTree& inifile;
	Grid& grid;

public:

	static constexpr bool enabled = isAdaptable<Grid>::value;

	/*---------------------------------------------------------------------*//**
	 * @brief      Create the factory, taking the parameters for building an
	 *             RichardsAdaptivityHandler
	 *
	 * @param      _inifile  Parameter file parser
	 * @param      _grid     Grid to adapt (reference is not saved)
	 */
	RichardsAdaptivityHandlerFactory (Dune::ParameterTree& _inifile, Grid& _grid) :
		inifile(_inifile),
		grid(_grid)
	{ }

	/// Create a dummy RichardsAdaptivityHandler
	/** Assert that adaptive grid (UG) is used.
	 */
	template<bool active = enabled>
	std::enable_if_t<!active,std::unique_ptr<AHB>> create ()
	{
		return std::make_unique<AHB>();
	}

	/// Create a true RichardsAdaptivityHandler
	template<bool active = enabled>
	std::enable_if_t<active,std::unique_ptr<AHB>> create ()
	{
		std::unique_ptr<AHB> p;
		if (inifile.hasSub("adaptivity") &&
			inifile.get<std::string>("adaptivity.policy") == "waterFlux")
			p = std::make_unique<WaterFluxAdaptivityHandler<Traits,GFS,Param,Boundary,U,order>>(inifile,grid);
		else 
			p = std::make_unique<AHB>();
		return p;
	}
};

} // namespace Dorie
} // namespace Dune

#endif // DUNE_DORIE_RICHARDS_ADAPTIVITY_HANDLER_HH
