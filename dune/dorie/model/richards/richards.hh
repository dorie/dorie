#ifndef DUNE_DORIE_MODEL_RICHARDS_HH
#define DUNE_DORIE_MODEL_RICHARDS_HH

#include <memory>
#include <string>
#include <iostream>

#include <dune/common/parametertree.hh>
#include <dune/common/parallel/mpihelper.hh>
#include <dune/common/float_cmp.hh>

#include <dune/pdelab/backend/istl.hh>
#include <dune/pdelab/gridoperator/gridoperator.hh>
#include <dune/pdelab/gridoperator/onestep.hh>
#include <dune/pdelab/newton/newton.hh>

#include <dune/dorie/common/typedefs.hh>
#include <dune/dorie/common/utility.hh>
#include <dune/dorie/common/gfs_helper.hh>
#include <dune/dorie/common/interpolator.hh>
#include <dune/dorie/common/time_step_controller.hh>
#include <dune/dorie/common/grid_creator.hh>
#include <dune/dorie/common/grid_function_writer.hh>
#include <dune/dorie/common/boundary_condition/manager.hh>
#include <dune/dorie/common/flux_reconstruction/raviart_thomas.hh>

#include <dune/dorie/model/base.hh>
#include <dune/dorie/model/richards/initial_condition.hh>
#include <dune/dorie/model/richards/adaptivity/handler.hh>
#include <dune/dorie/model/richards/parameterization/interface.hh>
#include <dune/dorie/model/richards/adapters/water_content.hh>
#include <dune/dorie/model/richards/adapters/saturation.hh>
#include <dune/dorie/model/richards/adapters/water_flux.hh>
#include <dune/dorie/model/richards/adapters/conductivity.hh>
#include <dune/dorie/model/richards/flow_parameters.hh>
#include <dune/dorie/model/richards/flow_source.hh>
#include <dune/dorie/model/richards/local_operator_DG.hh>
#include <dune/dorie/model/richards/local_operator_FV.hh>
#include <dune/dorie/model/richards/local_operator_cfg.hh>

#ifdef GTEST
	#include <gtest/gtest.h>
#endif

namespace Dune{
namespace Dorie{

/*-------------------------------------------------------------------------*//**
 * @brief      Traits for the class ModelRichards. In particular, this
 *             class extends BaseTraits to be used for the DG implementation of
 *             the Richards model
 *
 * @ingroup    ModelRichards
 *
 * @tparam     BaseTraits  Traits defining domain and range field properties of
 *                         the model.
 * @tparam     k           Order of the polynomial degree used for the basis
 *                         functions in the DG method
 */
template<class BaseTraits, int k>
struct ModelRichardsTraits : public BaseTraits
{
	static constexpr int dim = BaseTraits::dim;
	static constexpr int order = k;
	static constexpr int flux_order = (order==0) ? 0 : (order-1);
	using RF = typename BaseTraits::RF;
	using Scalar = typename BaseTraits::Scalar;
	using Grid = typename BaseTraits::Grid;
	using GV = typename BaseTraits::GV;

  /// GFS Helper
  using GFSHelper = 
    std::conditional_t<order==0,
      Dune::Dorie::LinearSolverGridFunctionSpaceHelper<GV,RF,BaseTraits::GridGeometryType>,
      Dune::Dorie::GridFunctionSpaceHelper<GV,RF,order,BaseTraits::GridGeometryType>>;

	/// Problem GFS
	using GFS = typename GFSHelper::Type;
	/// GFS Constraints Container
	using CC = typename GFSHelper::CC;


	/// LSGFS Helper
	using LSGFSHelper = Dune::Dorie::LinearSolverGridFunctionSpaceHelper<GV,RF,BaseTraits::GridGeometryType>;
	/// Linear solver GFS
	using LSGFS = typename LSGFSHelper::Type;
	/// LSGFS Constraints Container
	using LSCC = typename LSGFSHelper::CC;

	// -- DORiE Class Definitions -- //
	/// Wrapper for grid and volume & boundary mappings
	using GridCreator = Dune::Dorie::GridCreator<typename BaseTraits::Grid>;
	/// Class defining the soil parameterization
	using FlowParameters = Dune::Dorie::FlowParameters<BaseTraits>;
	/// Class defining boundary conditions
	using FlowBoundary = Dune::Dorie::BoundaryConditionManager<
		BaseTraits, BCMMode::richards>;
	/// Class defining source terms
	using FlowSource = Dune::Dorie::FlowSource<BaseTraits>;
	/// Class defining the initial condition
	using FlowInitial = Dune::Dorie::InitialCondition<BaseTraits>;
	/// Class defining the initial condition factory
	using FlowInitialFactory
		= Dune::Dorie::RichardsInitialConditionFactory<
			BaseTraits, FlowParameters>;
	/// Local spatial operator
  using SLOP = 
    std::conditional_t<order==0,
      Dune::Dorie::Operator::RichardsFVSpatialOperator<FlowParameters,FlowBoundary>,
      Dune::Dorie::Operator::RichardsDGSpatialOperator<BaseTraits,FlowParameters,FlowBoundary,FlowSource,typename GFSHelper::FEM,false>>;

	/// Local temporal operator
  using TLOP = 
    std::conditional_t<order==0,
      Dune::Dorie::Operator::RichardsFVTemporalOperator<FlowParameters>,
      Dune::Dorie::Operator::RichardsDGTemporalOperator<BaseTraits,FlowParameters,typename GFSHelper::FEM,false>>;

	/// Time controller
	using TimeStepController = Dune::Dorie::TimeStepController<double>;

	// -- DUNE Class Defintions -- //
	/// Matrix backend type
	using MBE = Dune::PDELab::ISTL::BCRSMatrixBackend<>;
	/// Grid operator for spatial LOP
	using GO0 = Dune::PDELab::GridOperator<GFS,GFS,SLOP,MBE,RF,RF,RF,CC,CC>;
	/// Grid operator for temporal LOP
	using GO1 = Dune::PDELab::GridOperator<GFS,GFS,TLOP,MBE,RF,RF,RF,CC,CC>;
	/// One step grid operator for merging both GOs
	using IGO = Dune::PDELab::OneStepGridOperator<GO0,GO1>;
	/// Solution vector type
	using U = typename IGO::Traits::Domain;
	/// Linear solver types
	using LS =
		std::conditional_t<order == 0,
			Dune::PDELab::ISTLBackend_BCGS_AMG_SSOR<IGO>,
			Dune::PDELab::ISTLBackend_OVLP_AMG_4_DG<IGO, CC, LSGFS, LSCC, Dune::PDELab::CG2DGProlongation, Dune::SeqSSOR, Dune::BiCGSTABSolver>>;
	/// Non-linear solver types
	using PDESOLVER = Dune::PDELab::Newton<IGO,LS,U>;
	/// Time stepping scheme
	using TimeStepScheme = Dune::PDELab::Alexander2Parameter<RF>;
	/// Methods computing the time step
	using OSM = Dune::PDELab::OneStepMethod<RF,IGO,PDESOLVER,U,U>;

	/// Model state provider
	struct ConstState 
	{
		using GridFunctionSpace = const GFS;
		using Coefficients = const U;
		using TimeField = const typename BaseTraits::TimeField;

		std::shared_ptr<GridFunctionSpace> grid_function_space;
		std::shared_ptr<Coefficients> coefficients;
		TimeField time;
	};

	/// Model state provider
	struct State 
	{
		using GridFunctionSpace = GFS;
		using Coefficients = U;
		using TimeField = typename BaseTraits::TimeField;

		std::shared_ptr<GridFunctionSpace> grid_function_space;
		std::shared_ptr<Coefficients> coefficients;
		TimeField time;

		// Implicit conversion to constant state
		operator ConstState() {
			return ConstState{grid_function_space,coefficients,time};
		}
	};

	// -- Grid Functions of state variables -- //
	/// Matric head
	using GFMatricHead = Dune::PDELab::DiscreteGridFunction<GFS,U>;
	/// Water Flux
	using GFWaterFlux = Dune::Dorie::WaterFluxAdapter<GFS,U,FlowParameters>;
	/// Conductivity
	using GFConductivity = Dune::Dorie::ConductivityAdapter<BaseTraits,FlowParameters>;
	/// Water content
	using GFWaterContent = Dune::Dorie::WaterContentAdapter<BaseTraits,FlowParameters,GFMatricHead>;
	/// Saturation
	using GFSaturation = Dune::Dorie::SaturationAdapter<BaseTraits,FlowParameters,GFMatricHead>;
	/// Water Flux reconstruction
	static constexpr bool enable_rt_engine = Dune::Dorie::RaviartThomasFluxReconstructionHelper<dim,flux_order,BaseTraits::GridGeometryType>::enable;
	using GFFluxReconstruction = std::conditional_t<enable_rt_engine,Dune::Dorie::RaviartThomasFluxReconstruction<GV,RF,flux_order,BaseTraits::GridGeometryType>,void>;

	// -- Utility Class Definitions -- //
	/// Custom VTK output writer
	using Writer = Dune::Dorie::GridFunctionVTKSequenceWriter<GV>;
	/// Grid Adaptivity Handler base class
	using AdaptivityHandler = Dune::Dorie::RichardsAdaptivityHandler<BaseTraits,GFS,FlowParameters,FlowBoundary,U,order>;
	/// Factory for creating the AdaptivityHandler
	using AdaptivityHandlerFactory = Dune::Dorie::RichardsAdaptivityHandlerFactory<BaseTraits,GFS,FlowParameters,FlowBoundary,U,order>;
};

/*-------------------------------------------------------------------------*//**
 * @brief      Class for Richards model. This class manages the model
 *             for the richards equation. It can perform time-steps and print
 *             the solution
 *
 * @ingroup    ModelRichards
 * @ingroup    Collective
 *
 * @tparam     ModelRichardsTraits  Traits containing the type definitions
 *                                       for Richards Model
 */
template<typename ModelRichardsTraits>
class ModelRichards : public ModelBase
{
public:
	using Traits = ModelRichardsTraits;

private:

	static constexpr int dim = Traits::dim;
	static constexpr int order = Traits::order;
	static constexpr int flux_order = Traits::flux_order;
	static constexpr bool enable_rt_engine = Traits::enable_rt_engine;
	using RF = typename Traits::RF;
	using Grid = typename Traits::Grid;
	using GV = typename Traits::GV;

	/// GFS Helper
	using GFSHelper = typename Traits::GFSHelper;
	/// Problem GFS
	using GFS = typename Traits::GFS;
	/// GFS Constraints Container
	using CC = typename Traits::CC;


	/// LSGFS Helper
	using LSGFSHelper = typename Traits::LSGFSHelper;
	/// Linear solver GFS
	using LSGFS = typename Traits::LSGFS;
	/// LSGFS Constraints Container
	using LSCC = typename Traits::LSCC;

	// -- DORiE Class Definitions -- //
	/// Wrapper for grid and volume & boundary mappings
	using GridCreator = typename Traits::GridCreator;
	/// Class defining the soil parameterization
	using FlowParameters = typename Traits::FlowParameters;
	/// Class defining boundary conditions
	using FlowBoundary = typename Traits::FlowBoundary;
	/// Class defining source terms
	using FlowSource = typename Traits::FlowSource;
	/// Class defining the initial condition
	using FlowInitial = typename Traits::FlowInitial;
	/// Class defining the initial condition factory
	using FlowInitialFactory = typename Traits::FlowInitialFactory;
	/// Local spatial operator
	using SLOP = typename Traits::SLOP;
	/// Local temporal operator
	using TLOP = typename Traits::TLOP;
	/// Time controller
	using TimeStepController = typename Traits::TimeStepController;

	// -- DUNE Class Defintions -- //
	/// Matrix backend type
	using MBE = typename Traits::MBE;
	/// Grid operator for spatial LOP
	using GO0 = typename Traits::GO0;
	/// Grid operator for temporal LOP
	using GO1 = typename Traits::GO1;
	/// One step grid operator for merging both GOs
	using IGO = typename Traits::IGO;
	/// Solution vector type
	using U = typename Traits::U;
	/// Linear solver types
	using LS = typename Traits::LS;

	/// Non-linear solver types
	using PDESOLVER = typename Traits::PDESOLVER;
	/// Time stepping scheme
	using TimeStepScheme = typename Traits::TimeStepScheme;
	/// Methods computing the time step
	using OSM = typename Traits::OSM;

	/// Model state provider
	using State = typename Traits::State;
	using ConstState = typename Traits::ConstState;

	// -- Grid Functions of state valriables -- //
	/// Matric head
	using GFMatricHead = typename Traits::GFMatricHead;
	/// Water Flux
	using GFWaterFlux = typename Traits::GFWaterFlux;
	/// Conductivity
	using GFConductivity = typename Traits::GFConductivity;
	/// Water content
	using GFWaterContent = typename Traits::GFWaterContent;
	/// Saturation
	using GFSaturation = typename Traits::GFSaturation;
	/// Water Flux reconstruction
	using GFFluxReconstruction = typename Traits::GFFluxReconstruction;


	// -- Utility Class Definitions -- //
	/// VTK Output writer base class
	using Writer = typename Traits::Writer;
	/// Grid Adaptivity Handler base class
	using AdaptivityHandler = typename Traits::AdaptivityHandler;
	/// Factory for creating the AdaptivityHandler
	using AdaptivityHandlerFactory = typename Traits::AdaptivityHandlerFactory;

protected:

	// -- Member variables -- //
	Dune::MPIHelper& helper;
	Dune::ParameterTree inifile;

	const Dune::VTK::OutputType output_type;

	/// Time step controller of this instance
	TimeStepController time_step_ctrl;
	/// Minimum number of Newton iterations (at maximum time step)
	const int newton_it_min;
	/// Maximum number of Newton iterations (at minimal time step)
	const int newton_it_max;

	std::shared_ptr<GFS> gfs;
	std::shared_ptr<Grid> grid;
	GV gv;
	std::unique_ptr<CC> cc;

	std::shared_ptr<LSGFS> lsgfs;
	std::shared_ptr<LSCC> lscc;

	std::shared_ptr<FlowParameters> fparam;
	std::shared_ptr<FlowBoundary> fboundary;
	std::shared_ptr<const FlowSource> fsource;
	std::shared_ptr<FlowInitial> finitial;

	std::unique_ptr<SLOP> slop;
	MBE mbe_slop;
	std::unique_ptr<TLOP> tlop;
	MBE mbe_tlop;
	std::unique_ptr<GO0> go0;
	std::unique_ptr<GO1> go1;
	std::unique_ptr<IGO> igo;
	std::unique_ptr<LS> ls;
	std::unique_ptr<PDESOLVER> pdesolver;
	std::unique_ptr<OSM> osm;
	TimeStepScheme alex2;

	std::shared_ptr<U> u;

	std::unique_ptr<Writer> vtkwriter;
	std::unique_ptr<AdaptivityHandler> adaptivity;

	mutable std::shared_ptr<GFFluxReconstruction> cache_water_flux_gf_rt;

	double time_before;

	const bool enable_fluxrc;
public:

	/*------------------------------------------------------------------------*//**
	 * @brief      Construct Richards model.
	 *
	 * @param      _inifile      Dune parameter file parser
	 * @param      _grid_mapper  Mapper for grid and volume/boundary data
	 * @param      _helper       Dune MPI instance controller
	 */
	ModelRichards (
		const Dune::ParameterTree& _inifile,
		const GridCreator& _grid_creator,
		Dune::MPIHelper& _helper
	);

	~ModelRichards() = default;

	/*------------------------------------------------------------------------*//**
	 * @brief      Compute a time step. Models the time step requirement of
	 *             ModelBase
	 *
	 * @throws     Dune::Exception  Fatal error occurs during computation
	 */
	void step () override;

	/*------------------------------------------------------------------------*//**
	 * @brief      Mark the grid in order to improve the current model.
	 */
	virtual void mark_grid() override;

	/*------------------------------------------------------------------------*//**
	 * @brief      Adapt the grid together it every dependency of the
	 *             grid (e.g. solution vector and grid function spaces).
	 */
	virtual void adapt_grid() override;

	/*------------------------------------------------------------------------*//**
	 * @brief      Setup operators to fit the new grid.
	 */
	virtual void post_adapt_grid() override;

	/*------------------------------------------------------------------------*//**
	 * @brief      Method that provides the begin time of the model.
	 *
	 * @return     Begin time of the model.
	 */
	double begin_time() const final {return time_step_ctrl.get_time_begin();}

	/*------------------------------------------------------------------------*//**
	 * @brief      Method that provides the end time of the model.
	 *
	 * @return     End time of the model.
	 */
	double end_time() const final {return time_step_ctrl.get_time_end();}

	/*------------------------------------------------------------------------*//**
	 * @brief      Provides an object with all the information that describes
	 *             the current state of the solved variable.
	 *
	 * @return     Current state of the model.
	 */
	State current_state() {
		return State{gfs,u,current_time()};
	}

	/*------------------------------------------------------------------------*//**
	 * @brief      Provides an object with all the information that describes
	 *             the current state of the solved variable.
	 *
	 * @return     (Const) Current state of the model.
	 */
	ConstState current_state() const {
		return ConstState{gfs,u,current_time()};
	}

	/*------------------------------------------------------------------------*//**
	 * @brief      Suggest a time step to the model.
	 *
	 * @param[in]  dt    Suggestion for the internal time step of the model. The
	 *                   new internal time step shall not be bigger than dt.
	 */
	void suggest_timestep(double dt) final
	{
		time_step_ctrl.suggest_time_step(dt);
	}

	/*------------------------------------------------------------------------*//**
	 * @brief      Write the data using the VTKWriter. VTKWriters is always
	 *             cleared after used.
	 */
	void virtual write_data() const override;

	/*------------------------------------------------------------------------*//**
	 * @brief      Generates a matric head grid function from a given state
	 *
	 * @return     Pointer to a matric head grid function
	 */
	std::shared_ptr<const GFMatricHead> get_matric_head(ConstState state) const
	{
		return std::make_shared<GFMatricHead>(state.grid_function_space,state.coefficients);
	}

	/*------------------------------------------------------------------------*//**
	 * @brief      Gets the matric head grid function
	 *
	 * @return     Pointer to a matric head grid function
	 */
	std::shared_ptr<const GFMatricHead> get_matric_head() const
	{
		return get_matric_head(current_state());
	}

	/*------------------------------------------------------------------------*//**
	 * @brief      Generates a conductivity grid function from a given state
	 *
	 * @return     Pointer to a conductivity grid function
	 */
	std::shared_ptr<const GFConductivity> get_conductivity(ConstState state) const
	{
		// hard copy of parameters
		auto _fparam = std::make_shared<FlowParameters>(*fparam);

		// create conductivity adapter
		return std::make_shared<GFConductivity>(gv, _fparam);
	}

	/*------------------------------------------------------------------------*//**
	 * @brief      Gets the conductivity grid function
	 *
	 * @return     Pointer to a conductivity grid function
	 */
	std::shared_ptr<const GFConductivity> get_conductivity() const
	{
		return get_conductivity(current_state());
	}

	/*------------------------------------------------------------------------*//**
	 * @brief      Generates a water content grid function from a given state
	 *
	 * @return     Pointer to a water content grid function
	 */
	std::shared_ptr<const GFWaterContent> get_water_content(ConstState state) const
	{
		// hard copy of parameters
		auto _fparam = std::make_shared<FlowParameters>(*fparam);

		// create water content adapter
		return std::make_shared<GFWaterContent>(get_matric_head(state), gv, _fparam);
	}

	/*------------------------------------------------------------------------*//**
	 * @brief      Gets the water content grid function
	 *
	 * @return     Pointer to a water content grid function
	 */
	std::shared_ptr<const GFWaterContent> get_water_content() const
	{
		return get_water_content(current_state());
	}

	/*------------------------------------------------------------------------*//**
	 * @brief      Generates a saturation grid function from a given state
	 *
	 * @return     Pointer to a saturation grid function
	 */
	std::shared_ptr<const GFSaturation> get_saturation(ConstState state) const
	{
		// hard copy of parameters
		auto _fparam = std::make_shared<FlowParameters>(*fparam);

		// create saturation adapter
		return std::make_shared<GFSaturation>(get_matric_head(state), gv, _fparam);
	}

	/*------------------------------------------------------------------------*//**
	 * @brief      Gets the saturation grid function
	 *
	 * @return     Pointer to a saturation grid function
	 */
	std::shared_ptr<const GFSaturation> get_saturation() const
	{
		return get_saturation(current_state());
	}

	/*------------------------------------------------------------------------*//**
	 * @brief      Generates a water flux grid function from a given state
	 *
	 * @return     Pointer to a water flux grid function
	 */
	std::shared_ptr<const GFWaterFlux> get_water_flux(ConstState state) const
	{
		// hard copy of parameters
		auto _fparam = std::make_shared<FlowParameters>(*fparam);

		// create water flux adapter
		return std::make_shared<GFWaterFlux>(state.grid_function_space, state.coefficients, _fparam);
	}

	/*------------------------------------------------------------------------*//**
	 * @brief      Gets the water flux grid function
	 *
	 * @return     Pointer to a water flux grid function
	 */
	std::shared_ptr<const GFWaterFlux> get_water_flux() const
	{
		return get_water_flux(current_state());
	}

	/*------------------------------------------------------------------------*//**
	 * @brief      Generates a (reconstructed) water flux grid function from a
	 *             given state
	 *
	 * @param[in]  state  The state
	 *
	 * @return     Pointer to a (reconstructed) water flux grid function
	 */
	template<bool enabled=enable_rt_engine>
	std::enable_if_t<enabled, std::shared_ptr<const GFFluxReconstruction>>
	get_water_flux_reconstructed(ConstState state) const
	{
		std::shared_ptr<GFFluxReconstruction> gf_ptr;

		auto& cache = cache_water_flux_gf_rt;

		if (state.grid_function_space != gfs
			or state.coefficients != u
			or state.time != current_time())
		{
			// if state is different to current state, create flux from zero

			gf_ptr = std::make_unique<GFFluxReconstruction>(
									this->_log,gv,inifile.sub("fluxReconstruction"));

			slop->setTime(state.time);

			// update it with the state
			gf_ptr->update(*(state.coefficients),
										*(state.grid_function_space),
										*slop);

			slop->setTime(current_time());
		}

		// if state is equal to current state, use cache.
		else if (not cache) {

			cache = std::make_unique<GFFluxReconstruction>(
									this->_log,gv,inifile.sub("fluxReconstruction"));

			// update it with current state
			cache->update(*u,*gfs,*slop);

			gf_ptr = cache;
		}

		else {
			gf_ptr = cache;
		}

		return gf_ptr;
	}

	/*------------------------------------------------------------------------*//**
	 * @brief      Gets the (reconstructed) water flux grid function
	 *
	 * @return     Pointer to a (reconstructed) water flux grid function
	 */
	template<bool enabled=enable_rt_engine>
	std::enable_if_t<enabled, std::shared_ptr<const GFFluxReconstruction>>
	get_water_flux_reconstructed() const
	{
		return get_water_flux_reconstructed(current_state());
	}

protected:

	/*------------------------------------------------------------------------*//**
	 * @brief      Create the Grid Operators, Solvers, and Time Step Operator.
	 *             This function must be called after changes to grid or GFS!
	 */
	void operator_setup ();

	/// Return the maximum number of iterations allowed in the Newton solver
	/** This serves as time step control. The allowed iterations decrease
	 *  logarithmically with increasing time step.
	 */
	int newton_iterations_max (const RF dt) const;

	/// Compute the initial condition based on the stationary problem
	void compute_initial_condition ();

private:

#ifdef GTEST
	FRIEND_TEST(ModelCoupling, TimeSteps);
#endif
};

} // namespace Dorie
} // namespace Dune

#endif // DUNE_DORIE_MODEL_RICHARDS_HH
