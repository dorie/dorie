#include <dune/dorie/model/richards/richards.hh>

namespace Dune{
namespace Dorie{

template<typename Traits>
ModelRichards<Traits>::ModelRichards (
	const Dune::ParameterTree& _inifile,
	const GridCreator&   _grid_creator,
	Dune::MPIHelper&     _helper
):
	ModelBase(log_richards,
				   _inifile.get<std::string>("output.logLevel"),
				   _helper),
	helper(_helper),
	inifile(_inifile),
	output_type(_inifile.get<bool>("output.asciiVtk") ?
		Dune::VTK::OutputType::ascii : Dune::VTK::OutputType::base64),
	time_step_ctrl(inifile, this->_log),
	newton_it_min(inifile.get<int>("time.minIterations")),
	newton_it_max(inifile.get<int>("time.maxIterations")),
	grid(_grid_creator.grid()),
	gv(grid->leafGridView()),
	mbe_slop(estimate_mbe_entries<typename MBE::size_type>(
				Traits::dim,Traits::GridGeometryType)),
	mbe_tlop(1),
	enable_fluxrc(_inifile.get<bool>("fluxReconstruction.enable"))
{
	// initialize time
	this->_time = time_step_ctrl.get_time_begin();
	time_before = this->_time;

	// --- Output Policy ---
	 std::string output_policy_str 
		= inifile.get<std::string>("output.policy");

	if (output_policy_str == "endOfRichardsStep") {
		this->set_policy(OutputPolicy::EndOfRichardsStep);
	} else if (output_policy_str == "none") {
		this->set_policy(OutputPolicy::None);
	} else {
		this->_log->error("Invalid output policy '{}'", output_policy_str);
		DUNE_THROW(NotImplemented,"Invalid output policy: " << output_policy_str);
	}

	// --- Grid Function Space ---
	this->_log->trace("Setting up GridFunctionSpace");
	gfs = std::make_shared<GFS>(GFSHelper::create(gv));
	gfs->name("matric_head");
	cc = std::make_unique<CC>();
	Dune::PDELab::constraints(*gfs,*cc,false);

	// --- Create the new parameter class
	auto element_map = _grid_creator.element_index_map();
	fparam = std::make_shared<FlowParameters>(inifile, grid, element_map);

	// --- Operator Helper Classes ---
	const auto& boundary_map = _grid_creator.boundary_index_map();
	fboundary = std::make_shared<FlowBoundary>(inifile, boundary_map, this->_log);
	fsource = std::make_shared<const FlowSource>(inifile);
	finitial = FlowInitialFactory::create(inifile, gv, fparam, this->_log);

	// Read local operator settings
	namespace OP = Dune::Dorie::Operator;
	const auto settings = OP::read_richards_operator_settings(inifile);

	// --- Local Operators ---
	if constexpr (order>0)
	{
		this->_log->debug("Setting up local grid operators: DG method");

		const auto upwinding = std::get<OP::RichardsUpwinding>(settings);
		const auto method = std::get<OP::RichardsDGMethod>(settings);
		const auto weights = std::get<OP::RichardsDGWeights>(settings);

		slop = std::make_unique<SLOP>(inifile, fparam, fboundary, fsource,
									  method, upwinding, weights);
		tlop = std::make_unique<TLOP>(inifile, fparam);
	}
	else {
		this->_log->debug("Setting up local grid operators: FV method");
		const auto upwinding = std::get<OP::RichardsUpwinding>(settings);
		slop = std::make_unique<SLOP>(fparam, fboundary, upwinding);
		tlop = std::make_unique<TLOP>(fparam);
	}

	// --- Solution Vector --- //
	u = std::make_shared<U>(*gfs,.0);

	// --- Operator Setup --- //
	operator_setup();

	// --- Initial Condition --- //
	// Initial condition exists. Just interpolate onto DOFs
	if (finitial) {
		Dune::PDELab::interpolate(*finitial, *gfs, *u);
	}
	// Comput initial condition from stationary problem
	else {
		try {
			compute_initial_condition();
		}
		catch (...) {
			this->_log->error("Computing the initial condition failed");
			throw;
		}
	}

	// --- Writer Setup --- //
	if (output_policy() != OutputPolicy::None)
	{
		const int subsamling_lvl =
			_inifile.get<int>("output.subsamplingLevel", 0);
		const auto output_path = inifile.get<std::string>("output.outputPath");
		this->_log->debug("Creating a VTK writer with subsampling level: {}",
						  subsamling_lvl);

		Dorie::create_directories(output_path);
		const auto subsamling_intervals = Dune::refinementLevels(subsamling_lvl);
		auto subsamling_vtk =
			std::make_shared<Dune::SubsamplingVTKWriter<GV>>(gv,
															 subsamling_intervals);
		vtkwriter = std::make_unique<Writer>(subsamling_vtk,
							inifile.get<std::string>("output.fileName"),
							output_path,
							"./");
	}

	// --- Adaptivity Setup --- //
	if constexpr (AdaptivityHandlerFactory::enabled)
	{
		AdaptivityHandlerFactory adaptivity_fac(inifile,*grid);
		adaptivity = adaptivity_fac.create();
	}

	this->_log->info("Setup complete");
}

template<typename Traits>
void ModelRichards<Traits>::operator_setup()
{
	auto dof_count = gfs->globalSize();
	this->_log->debug("Setting up grid operators and solvers");
	if (helper.size() > 1) {
		this->_log->debug("  DOF of this process: {}", dof_count);
	}
	dof_count = gv.comm().sum(dof_count);
	this->_log->debug("  Total number of DOF: {}", dof_count);

	Dune::PDELab::constraints(*this->gfs,*this->cc,false);
	// --- Grid Operators ---
	go0 = std::make_unique<GO0>(*gfs,*cc,*gfs,*cc,*slop,mbe_slop);
	go1 = std::make_unique<GO1>(*gfs,*cc,*gfs,*cc,*tlop,mbe_tlop);
	igo = std::make_unique<IGO>(*go0,*go1);

	// --- Solvers ---

	// FV linear solver
	if constexpr (order == 0) {
		// Solver args: GFS, maxiter, verbose, reuse, superlu
		ls = std::make_unique<LS>(*gfs, 1000, 0, true, true);
	}
	// DG linear solver
	else {
		lsgfs = std::make_unique<LSGFS>(LSGFSHelper::create(gv));
		lscc = std::make_unique<LSCC>();
		// Solver args: GO, constraints, LSGFS, LSGFS constraints, maxiter, verbose,
		//              reuse, superlu
		ls = std::make_unique<LS>(*igo, *cc, *lsgfs, *lscc, 1000, 0, true, true);
	}

	pdesolver = std::make_unique<PDESOLVER>(*igo,*ls);
	pdesolver->setParameters(inifile.sub("NewtonParameters"));
	pdesolver->setVerbosityLevel(0);

	// --- Time Step Operators ---
	Dune::PDELab::OneStepMethodResult osm_result;
	if(osm){
		osm_result = osm->result(); // cache old result
	}
	osm = std::make_unique<OSM>(alex2,*igo,*pdesolver);
	osm->setResult(osm_result);
	osm->setVerbosityLevel(0);

	gfs->update();
}

template<typename Traits>
void ModelRichards<Traits>::step()
{
	// long time step log for level 'debug'
	if (this->_log->should_log(spdlog::level::debug)) {
		this->_log->info("Time Step {}:",
						  osm->result().successful.timesteps);
	}

	// get time step suggestion from boundary conditions
	fboundary->set_time(this->_time);
	const auto dt_suggestion = fboundary->max_time_step(this->_time);

	// iterate until break or throw
	while (true)
	{
		// Fetch current time step
		// NOTE: Communicate minimal time step between processors!
		time_step_ctrl.suggest_time_step(dt_suggestion);
		auto dt = time_step_ctrl.get_time_step(this->_time);
		dt = gv.comm().min(dt);

		try
		{
			// long time step log for level 'debug'
			if (this->_log->should_log(spdlog::level::debug)) {
				this->_log->debug("  Time {:.2e} + {:.2e} -> {:.2e}",
								  this->_time, dt, this->_time+dt);
			}

			const auto iter = newton_iterations_max(dt);
			_log->trace("  Allowed iterations for Newton solver: {}", iter);
			pdesolver->setMaxIterations(iter);
			
			// create a DOF vector for next step
			std::shared_ptr<U> unext = std::make_shared<U>(*u);

			dwarn.push(false);
			osm->apply(this->_time, dt, *u, *unext);
			dwarn.pop();

			// short time step log for level 'info' (after success)
			if (not this->_log->should_log(spdlog::level::debug)) {
				this->_log->info("Time Step {}: {:.2e} + {:.2e} -> {:.2e}",
								 osm->result().successful.timesteps-1,
								 this->_time,
								 dt,
								 this->_time+dt);
			}

			auto result = osm->result();
			this->_log->trace("  Matrix assembly: {:.2e}s (total), {:.2e}s (success)",
							  result.total.assembler_time,
							  result.successful.assembler_time);
			this->_log->trace("  Linear solver:   {:.2e}s (total), {:.2e}s (success)",
							  result.total.linear_solver_time,
							  result.successful.linear_solver_time);

			u = unext;

			// saving last times for adaptivity
			time_before = this->_time;

			// update time and time step
			this->_time += dt;
			time_step_ctrl.increase_time_step();

			// leave loop
			break;
		}
		catch (Dune::PDELab::NewtonError &e){
			this->_log->debug("  Solver not converged");
			this->_log->trace("  Error in Newton solver: {}", e.what());
		}
		catch (Dune::ISTLError &e){
			this->_log->debug("  Solver not converged");
			this->_log->trace("  Error in linear solver: {}", e.what());
		}
		catch (Dune::Exception &e){
			this->_log->debug("  Solver not converged");
			this->_log->error("  Unexpected error in solver: {}", e.what());
			DUNE_THROW(Dune::Exception, "Critical error in Newton solver");
		}
		// rethrow anything else
		catch (...) {
			throw;
		}

		// abort if minimal time step is already reached
		if (Dune::FloatCmp::eq(dt, time_step_ctrl.get_time_step_min())) {
			this->_log->error("Computing a solution for the minimal time "
						"step failed");
			DUNE_THROW(Exception, "No solution for minimal time step");
		}

		time_step_ctrl.decrease_time_step();
	} // while(true)

	// invalidate cache for water flux reconstruction
	cache_water_flux_gf_rt.reset();

	if (this->output_policy() == OutputPolicy::EndOfRichardsStep)
		write_data();
}

template<typename Traits>
void ModelRichards<Traits>::mark_grid()
{
  if (adaptivity_policy() != AdaptivityPolicy::WaterFlux)
    DUNE_THROW(Dune::NotImplemented,"Not known adaptivity policy for transport solver");
	
	adaptivity->mark_grid(*grid, gv, *gfs, *fparam,
		*fboundary, time_before, *u);
}

template<typename Traits>
void ModelRichards<Traits>::adapt_grid()
{
	if constexpr (AdaptivityHandlerFactory::enabled)
		adaptivity->adapt_grid(*grid, *gfs, *u);
	else {
		this->_log->error("The grid type does not support adaptation");
		DUNE_THROW(NotImplemented, "Grid does not support adaptation");
	}

	// invalidate cache for water flux reconstruction
	cache_water_flux_gf_rt.reset();
}

template<typename Traits>
void ModelRichards<Traits>::post_adapt_grid()
{
	operator_setup();
}

template<typename Traits>
void ModelRichards<Traits>::write_data () const
{
	if (vtkwriter)
	{
		// create adapters
		auto head = get_matric_head();
		auto wflux = std::make_shared<const GFWaterFlux>(gfs, u, fparam);
		auto cond = std::make_shared<const GFConductivity>(gv, fparam);
		auto wc =  std::make_shared<const GFWaterContent>(head, gv, fparam);
		auto sat = std::make_shared<const GFSaturation>(head, gv, fparam);

		if (inifile.get<bool>("output.vertexData")) {
			vtkwriter->addVertexData(head,"head");
			vtkwriter->addVertexData(cond,"K_0");
			vtkwriter->addVertexData(wc,"theta_w");
			vtkwriter->addVertexData(sat,"Theta");

			if constexpr (order > 0) {
				vtkwriter->addVertexData(wflux, "flux");
			}
			if constexpr (enable_rt_engine)
				if (enable_fluxrc) {
					auto wfluxr = get_water_flux_reconstructed();
					auto RT_name = "flux_RT" + std::to_string(flux_order);
					vtkwriter->addVertexData(wfluxr,RT_name);
				}
		}
		// cell data
		else {
			vtkwriter->addCellData(head,"head");
			vtkwriter->addCellData(cond,"K_0");
			vtkwriter->addCellData(wc,"theta_w");
			vtkwriter->addCellData(sat,"Theta");

			if constexpr (order > 0) {
				vtkwriter->addCellData(wflux, "flux");
			}
			if constexpr (enable_rt_engine)
				if (enable_fluxrc) {
					auto wfluxr = get_water_flux_reconstructed();
					auto RT_name = "flux_RT" + std::to_string(flux_order);
					vtkwriter->addCellData(wfluxr,RT_name);
				}
		}

		try{
			this->_log->trace("Writing solution at time {:.2e}", this->_time);
			vtkwriter->write(this->_time, output_type);
		}
		catch (Dune::Exception& e) {
			this->_log->error("Writing VTK output failed: {}", e.what());
			DUNE_THROW(Dune::IOError, "Cannot write VTK output!");
		}
		catch(...){
			this->_log->error("Writing VTK output failed for unknown reason");
			DUNE_THROW(Dune::IOError, "Cannot write VTK output!");
		}
		vtkwriter->clear();
	} else {
		this->_log->error("Calling 'write_data' on object without VTK writer");
		DUNE_THROW(Dune::InvalidStateException, "No vtk writer configured!");
	}
}

template<typename Traits>
int ModelRichards<Traits>::newton_iterations_max (const RF dt) const
{
	const auto dt_min = time_step_ctrl.get_time_step_min();
	const auto dt_max = time_step_ctrl.get_time_step_max();
	return std::round( (newton_it_min - newton_it_max)
		* std::log10(dt - dt_min + 1.0)
		/ std::log10(dt_max - dt_min + 1.0)
		+ newton_it_max );
}

template<typename Traits>
void ModelRichards<Traits>::compute_initial_condition()
{
	this->_log->info("Computing stationary problem for initial condition");

	// Build new Newton solver
	using Newton = Dune::PDELab::Newton<GO0, LS, U>;
	Newton newton(*go0, *ls);
	newton.setParameters(inifile.sub("NewtonParameters"));
	newton.setVerbosityLevel(0);
	newton.setMaxIterations(50); // Just a large number to ensure convergence

	// Set time in local operator (for BCs)
	slop->setTime(this->_time);

	// Use hydrostatic equilibrium for an initial guess
	using T = typename Traits::BaseTraits;
	auto init = std::make_unique<InitialConditionAnalytic<T>>(
		this->_log, gv, "-h"
	);
	Dune::PDELab::interpolate(*init, *gfs, *u);

	// Lambda for applying the solver:
	// Return true if the solver converged, let DUNE errors pass
	auto solve_stationary = [&newton](U& u) -> bool
	{
		try {
			newton.apply(u);
			return true;
		}
		// Ignore Dune errors. The calling function will try again.
		catch (Exception&) {
			// pass
		}

		return false;
	};

	// Try solving
	auto solved = solve_stationary(*u);

	// Use static initial guesses if that did not work
	std::vector<double> init_values({0.0, -1.0, -10.0});
	for(size_t i = 0;
		not solved and i < init_values.size();
		++i)
	{
		this->_log->debug("Computing the initial condition failed. "
			"Retrying with different initial guess...");
		u = std::make_shared<U>(*gfs, init_values[i]);
		solved = solve_stationary(*u);
	}

	if (not solved) {
		DUNE_THROW(Exception, "No initial guess worked for computing the IC");
	}

	// Report times
	const auto& result = newton.result();
	this->_log->trace("  Matrix assembly: {:.2e}s", result.assembler_time);
	this->_log->trace("  Linear solver:   {:.2e}s", result.linear_solver_time);
}

} // namespace Dorie
} // namespace Dune
