#ifndef DUNE_DORIE_RICHARDS_LOP_CFG_HH
#define DUNE_DORIE_RICHARDS_LOP_CFG_HH

#include <dune/dorie/model/local_operator_cfg.hh>

namespace Dune {
namespace Dorie {
namespace Operator {

/**
 *  \addtogroup RichardsModel
 *  \{
 */

//! Richards Upwinding type
enum class RichardsUpwinding
{
    none,       //!< No upwinding
    fullUpwind, //!< Upwind conductivity
    semiUpwind  //!< Upwind conductivity factor
};

/// Read upwinding operator settings from a parameter file
/* \return richards upwinding option
 */
inline Operator::RichardsUpwinding read_richards_operator_upwinding_settings(
    const Dune::ParameterTree& inifile,
    const std::shared_ptr<spdlog::logger>& log)
{
    const auto upwinding_str
        = inifile.get<std::string>("numerics.upwinding");
    log->debug("  Upwinding scheme: {}", upwinding_str);
    if (upwinding_str == "none")
        return Operator::RichardsUpwinding::none;
    else if (upwinding_str == "semiUpwind")
        return Operator::RichardsUpwinding::semiUpwind;
    else if (upwinding_str == "fullUpwind")
        return Operator::RichardsUpwinding::fullUpwind;
    else {
        log->error("Unknown upwinding scheme: {}", upwinding_str);
        DUNE_THROW(Dune::IOError, "Unknown upwinding scheme");
    }
}

//! Richards DG method type
using RichardsDGMethod = DGMethod;

//! Richards DG weights type
using RichardsDGWeights = DGWeights;

/// Read operator settings from a parameter file
/* \return Tuple of RichardsUpwinding, RichardsDGMethod, and
 *    RichardsDGWeights to be inserted into the local operator constructor.
 */
inline auto read_richards_operator_settings(
    const Dune::ParameterTree& inifile)
        -> std::tuple<Operator::RichardsUpwinding,
                      Operator::RichardsDGMethod,
                      Operator::RichardsDGWeights>
{
    const auto log = Dorie::get_logger(log_richards);
    log->debug("Reading local operator settings:");

    RichardsUpwinding upwinding;
    RichardsDGMethod method = RichardsDGMethod::SIPG;
    RichardsDGWeights weights = RichardsDGWeights::weightsOn;

    // Upwinding
    upwinding = read_richards_operator_upwinding_settings(inifile,log);

    // Return here if the local operator is FV only
    if (inifile.get<int>("numerics.FEorder") == 0)
    {
        log->debug("  Ignoring settings 'numerics.DGMethod' and  "
                   "'numerics.DGWeights' for finite volume solver.");
        return std::make_tuple(upwinding, method, weights);
    }

    // DG Method
    method = read_operator_dg_method_settings(inifile,log);
    weights = read_operator_dg_weights_settings(inifile,log);

    return std::make_tuple(upwinding, method, weights);
}

/**
 *  \}
 *  // group RichardsModel
 */

} // namespace Operator
} // namespace Dorie
} // namespace Dune

#endif // DUNE_DORIE_RICHARDS_LOP_CFG_HH
