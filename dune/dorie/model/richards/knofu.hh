#ifndef DUNE_DORIE_KNOFU_HH
#define DUNE_DORIE_KNOFU_HH

namespace Dune{
namespace Dorie{

template<typename Traits, typename DGF, typename GV, typename Parameters>
class MatricHeadAdapter : public Dune::PDELab::GridFunctionBase
	<Dune::PDELab::GridFunctionTraits<GV,typename Traits::RangeField,1,typename Traits::Scalar>,
		MatricHeadAdapter<Traits,DGF,GV,Parameters> >
	{
	private:
		using GFTraits = Dune::PDELab::GridFunctionTraits<GV,typename Traits::RangeField,1,typename Traits::Scalar>;
	public:
			/*
			* \brief Constructor
			* \param _gv GridView
			* \param _dgf grid function to interpolate from
			* \param _param Parameter object
			*/
			MatricHeadAdapter (const GV& _gv, const DGF& _dgf, const Parameters& _param) : 
				gv(_gv), dgf(_dgf), param(_param)
			{}

			/**
			* \brief Evaluation of grid function at certain position
			* \param e Element entity
			* \param x Position in local entity coordinates
			* \param y Return value
			* \return Value of grid function
			*/
			void evaluate (const typename GFTraits::ElementType& e,
				const typename GFTraits::DomainType& x,
				typename GFTraits::RangeType& y) const
			{
				typename GFTraits::RangeType u;
				dgf.evaluate(e,x,u);

				// calculate matric head
				const typename GFTraits::DomainType xGlobal = e.geometry().global(x);
				const typename GFTraits::RangeType head = param.matricHead(u,xGlobal);
				y = param.headRefToMiller(head,xGlobal);
			}

			const GV& getGridView () const
			{
				return gv;
			}

		private:
			const GV& gv;
			const Parameters& param;
			const DGF& dgf;
		};

template<typename Traits, typename SimTraits>
struct KnoFuInterfaceTraits
{
private:
	using RF = typename Traits::RF;

public:
	
};

template<typename Traits>
class KnoFuInterface : public ModelRichards<Traits>
{
protected:
	using RF = typename Traits::RF;
	using Grid = typename Traits::Grid;

	using Base = ModelRichards<Traits>;

	using Parameters = typename Base::Parameters;
	/// Interface uses GV on constant level
	using KFGV = typename Traits::Grid::LevelGridView;
	/// Interface GFS implements Problem GFS
	using KFGFSHelper = GridFunctionSpaceHelper<KFGV,RF,Traits::fem_order,Traits::GridGeometryType>;
	using KFGFS = typename KFGFSHelper::Type;
	/// Solution vector type
	using KFU = Dune::PDELab::Backend::Vector<KFGFS,RF>;
	/// Discrete Grid Function of KnoFu solution
	using KFUDGF = Dune::PDELab::DiscreteGridFunction<KFGFS,KFU>;
	/// Adapter for translating water content back into matric head
	using HeadDGF = Dune::Dorie::MatricHeadAdapter<Traits,KFUDGF,KFGV,Parameters>; 

	/// Matric head
	using GFMatricHead = typename Base::GFMatricHead;
	/// Water content
	using GFWaterContent = typename Base::GFWaterContent;

	KFGV kfgv; //!< GridView for KnoFuInterface
	KFGFS kfgfs; //!< GFS for KnoFuInterface
	KFU kfu; //!< Solution for KnoFuInterface

public:

	/// Export raw solution vector type for external usage
	using SolutionContainer = typename KFU::Container;

	KnoFuInterface (Dune::MPIHelper& _helper, std::shared_ptr<Grid> _grid, Dune::ParameterTree& _inifile)
		: ModelRichards<Traits>(_helper,_grid,_inifile),
			kfgv(_grid->levelGridView(_inifile.get<int>("grid.initialLevel"))),
			kfgfs(KFGFSHelper::create(kfgv)),
			kfu(kfgfs,0.0)
	{
		static_assert(!Traits::adapt_grid,"Adaptive Grid Refinement does not work with this interface!");
	}

	void solution_to_water_content ()
	{
		GFMatricHead udgf(this->gfs,this->unew);
		// GFWaterContent waterdgf(this->gv,*this->param,udgf);
		// Dune::PDELab::interpolate(waterdgf,kfgfs,kfu);
	}

	void water_content_to_solution ()
	{
		KFUDGF kfudgf(kfgfs,kfu);
		HeadDGF headdgf(kfgv,kfudgf,*this->param);
		Dune::PDELab::interpolate(headdgf,*this->gfs,*this->unew);
		*this->uold = *this->unew;
	}

	SolutionContainer cache_solution ()
	{
		//auto x = this->unew->native();
		return Dune::PDELab::Backend::native(*this->unew);
	}

	SolutionContainer cache_water_content ()
	{
		//auto x = this->unew->native();
		return Dune::PDELab::Backend::native(kfu);
	}

	/// Propagate an initial condition over a certain amount of time
	/** Initial condition is expected as water content and will be transformed
	 *  into matric head. After computing the solution, the matric head state
	 *  is transformed back into water content and returned.
	 *  \param initial_condition Initial state as Dune::BlockVector
	 *  \param t_start Starting time
	 *  \param t_end Finish time
	 *  \return Solution (water content) as Dune::BlockVector
	 */
	SolutionContainer propagate (const SolutionContainer& initial_condition,
		const RF t_start, const RF t_end)
	{
		Dune::PDELab::Backend::native(kfu) = initial_condition;
		water_content_to_solution();

		this->controller->set_time(t_start);
		this->controller->set_t_end(t_end);
		this->run();

		solution_to_water_content();
		SolutionContainer res = Dune::PDELab::Backend::native(kfu);
		return res;
	}

	/// Return observation operator for a single position on the grid
	/** Scalar multiplication of the solution vector with this operator
	 *  will yield the observation.
	 *  \param pos Observation position
	 *  \return Observation vector
	 */
	std::vector<RF> observation_operator (const typename Traits::Domain pos)
	{
		SolutionContainer kfu_raw = cache_water_content();
		std::vector<RF> res(kfu_raw.size());

		typedef Dune::PDELab::LocalFunctionSpace<KFGFS> LFS;
		typedef Dune::PDELab::LFSIndexCache<LFS> LFSCache;
		typedef typename KFU::template ConstLocalView<LFSCache> XView;
		typedef FiniteElementInterfaceSwitch<
					typename Dune::PDELab::LocalFunctionSpace<KFGFS>::Traits::FiniteElementType
			> FESwitch;

		LFS lfs(kfgfs);
		LFSCache lfs_cache(lfs);
		XView x_view(kfu);
		std::vector<typename Traits::Scalar> fem_factors(kfgfs.maxLocalSize()); // FEM coefficient container

		// find entity containing the coordinate
		auto it = kfgv.template begin<0>();
		auto pos_local = pos;
		for( ; it != kfgv.template end<0>(); ++it){
			const auto geo_type = it->type();
			const auto& ref = Dune::ReferenceElements<typename Traits::DF,Traits::dim>::general(geo_type);
			pos_local = it->geometry().local(pos);
			if(ref.checkInside(pos_local))
				break;
		}
		if(it == kfgv.template end<0>())
			DUNE_THROW(Dune::Exception,"Measurement position is not inside grid domain!");

		lfs.bind(*it);
		lfs_cache.update();
		x_view.bind(lfs_cache);
		FESwitch::basis(lfs.finiteElement()).evaluateFunction(pos_local,fem_factors);
		for(std::size_t i = 0; i<fem_factors.size(); ++i){
			const auto index = x_view.cache().containerIndex(i)[0];
			res[index] = fem_factors[i][0];
		}

		return res;
	}

};

}
}

#endif // DUNE_DORIE_KNOFU_HH
