#ifndef DUNE_DORIE_PARAM_MVG_HH
#define DUNE_DORIE_PARAM_MVG_HH

#include <cmath>
#include <functional>
#include <string>

#include <dune/dorie/model/richards/parameterization/interface.hh>

namespace Dune {
namespace Dorie {
namespace Parameterization {

/// Representation of the Mualem–van Genuchten parameterization
/** Implements the following relations:
 *  @f{align*}{
 *      m &:= 1 - 1/n \\
 *      \Theta (h_m) &= \left[ 1 + \left[ \alpha h_m \right]^n \right]^{-m} \\
 *      K (\Theta) &= K_0 \Theta^\tau
 *                      \left[ 1 - \left[ 1 - \Theta^{1/m} \right]^m \right]^2
 *  @f}
 *  \ingroup RichardsParam
 *  \author Lukas Riedel
 *  \date 2018
 */
template <typename Traits>
class MualemVanGenuchten :
    public Richards<Traits>
{
private:
    using RF = typename Traits::RF;
    using Base = Richards<Traits>;

public:

    using Saturation = typename Base::Saturation;
    using Conductivity = typename Base::Conductivity;
    using MatricHead = typename Base::MatricHead;
    using SaturatedConductivity = typename Base::SaturatedConductivity;

    using ResidualWaterContent = typename Base::ResidualWaterContent;
    using SaturatedWaterContent = typename Base::SaturatedWaterContent;

    /// Parameter defining the value of alpha (air-entry value)
    struct Alpha
    {
        RF value;
        inline static const std::string name = "alpha";
    };

    /// Parameter defining the value of n (retention curve shape)
    struct N
    {
        RF value;
        inline static const std::string name = "n";
    };

    /// Parameter defining the value of tau (skew factor)
    struct Tau
    {
        RF value;
        inline static const std::string name = "tau";
    };

    Alpha _alpha; //!< Value of air-entry value Alpha
    Tau _tau; //!< Value of skew-factor Tau
    N _n; //!< Value of retention curve shape factor N

    /// The name of this parameterization type (This is not the instance name)
    static inline std::string type = "MvG";

public:

    /// Construct with default-initialized parameters
    /** \param name The name associated with this soil layer
     */
    MualemVanGenuchten (const std::string name) :
        Richards<Traits>(name)
    { }

    /// Construct from a tuple of parameters
    /** \param name The name associated with this soil layer
     *  \param parameters Tuple of parameters to use in this parameterization
     */
    template<typename... Args>
    MualemVanGenuchten (
            const std::string name,
            const std::tuple<Args...> parameters) :
        Richards<Traits>(name, parameters),
        _alpha(std::get<Alpha>(parameters)),
        _tau(std::get<Tau>(parameters)),
        _n(std::get<N>(parameters))
    { }

    /// Add default destructor to clarify override
    ~MualemVanGenuchten () override = default;

    /// Return a scaled conductivity function
    /** Saturation -> Conductivity
     */
    std::function<Conductivity(const Saturation)> conductivity_f () const
        override
    {
        const auto m = 1.0 - 1.0 / _n.value;
        return [this, m](const Saturation sat) {
            return Conductivity {
                this->_k0.value
                * std::pow(sat.value, _tau.value)
                * std::pow(1 - std::pow(1 - std::pow(sat.value, 1.0/m), m), 2)
            };
        };
    }

    /// Return a scaled saturation function
    /** Matric head -> Saturation
     */
    std::function<Saturation(const MatricHead)> saturation_f () const
        override
    {
        const auto m = 1.0 - 1.0 / _n.value;
        return [this, m](const MatricHead head) {
            if (head.value >= 0.0) {
                return Saturation{1.0};
            }
            return Saturation {
                std::pow(1 + std::pow(_alpha.value * head.value, _n.value), -m)
            };
        };
    }

    /// Return a scaled inverse saturation function
    /** \return Function: Water content -> Matric Head
     *  \see saturation_f for the inverse function
     */
    std::function<MatricHead(const Saturation)> matric_head_f () const
        override
    {
        const auto m = 1.0 - 1.0 / _n.value;
        return [this, m](const Saturation sat) {
            return MatricHead {
                std::pow(_alpha.value, -1)
                * std::pow(std::pow(sat.value, -1.0/m) - 1.0, 1.0/_n.value)
            };
        };
    }

    /// Return a map of parameter names and values for manipulation
    /** \return Map: Parameter name, parameter value in this object
     */
    std::multimap<std::string, double&> parameters () override
    {
        return {
            {ResidualWaterContent::name, this->_theta_r.value},
            {SaturatedWaterContent::name, this->_theta_s.value},
            {SaturatedConductivity::name, this->_k0.value},
            {Alpha::name, _alpha.value},
            {Tau::name, _tau.value},
            {N::name, _n.value}
        };
    }

    /// Return a map of parameter names and values for manipulation
    /** \return Map: Parameter name, parameter value in this object
     */
    std::multimap<std::string, const double&> parameters () const override
    {
        return {
            {ResidualWaterContent::name, this->_theta_r.value},
            {SaturatedWaterContent::name, this->_theta_s.value},
            {SaturatedConductivity::name, this->_k0.value},
            {Alpha::name, _alpha.value},
            {Tau::name, _tau.value},
            {N::name, _n.value}
        };
    }

    /// Clone the plymorphic class
    /** \return unique pointer to the cloned object
     */
    std::unique_ptr<Richards<Traits>> clone () const override
    {
        using ThisType = MualemVanGenuchten<Traits>;
        return std::make_unique<ThisType>(*this);
    }
};

} // namespace Parameterization
} // namespace Dorie
} // namespace Dune

#endif // DUNE_DORIE_PARAM_MVG_HH
