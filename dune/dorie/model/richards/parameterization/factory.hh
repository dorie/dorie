#ifndef DUNE_DORIE_PARAM_RICHARDS_FACTORY_HH
#define DUNE_DORIE_PARAM_RICHARDS_FACTORY_HH

#include <dune/common/exceptions.hh>

#include <dune/dorie/common/logging.hh>
#include <dune/dorie/common/parameterization_factory.hh>
#include <dune/dorie/model/richards/parameterization/interface.hh>
#include <dune/dorie/model/richards/parameterization/mualem_van_genuchten.hh>

namespace Dune {
namespace Dorie {
namespace Parameterization {

/*-------------------------------------------------------------------------*//**
 * @brief      Factory of Transport parameters
 *
 * @tparam     Traits  The base traits
 */
template<class Traits>
struct RichardsFactory 
    : public ParameterizationFactory<Richards<Traits>>
{
    /// Return a default-initialized parameterization object
    /** \param type The type indicating the parameterization implementation
     *  \param name The name of the parameterization object (layer type)
     */
    std::shared_ptr<Richards<Traits>> 
    selector(
        const YAML::Node& type_node,
        const std::string& name) const override
    {
        auto log = Dorie::get_logger(log_richards);
        const auto type = type_node["type"].as<std::string>();
        if (type == Parameterization::MualemVanGenuchten<Traits>::type) {
            return std::make_shared<Parameterization::MualemVanGenuchten<Traits>>(name);
        }
        else {
            log->error("Richards parameterization '{}' has unknown type '{}'",
                        name, type);
            DUNE_THROW(IOError, "Unknown Richards parameterization type");
        }
    }
};

} // namespace Parameterization
} // namespace Dorie
} // namespace Dune

#endif // DUNE_DORIE_PARAM_RICHARDS_FACTORY_HH