#ifndef DUNE_DORIE_PARAM_SCALING_HH
#define DUNE_DORIE_PARAM_SCALING_HH

#include <string>
#include <cctype>
#include <vector>
#include <memory>
#include <algorithm>

#include <yaml-cpp/yaml.h>

#include <dune/dorie/common/utility.hh>
#include <dune/dorie/common/logging.hh>
#include <dune/dorie/common/interpolator.hh>
#include <dune/dorie/common/h5file.hh>

namespace Dune {
namespace Dorie {
namespace Parameterization {

/// Define scaling factors used for any scaling type.
/** The variables are initialized to their default values
 *  (no effective scaling).
 *  \tparam RF Numeric type of scaling factors
 *  \see ScalingAdapter returns this object when evaluated at a certain position
 *  \ingroup RichardsParam
 */
template<typename RF>
struct ScalingFactors
{
    //! Scaling factor of matric head
    RF scale_head = 1.0;
    //! Scaling factor of conductivity
    RF scale_cond = 1.0;
    //! Scaling factor of porosity (a summand, actually!)
    RF scale_por = 0.0;
};

/// Abstract base class for scaling adapters. Provides the interface.
/** A ScalingAdapter implements the evaluate method which returns a set of
 *  ScalingFactors.
 *
 *  \tparam Traits Type traits of this adapter.
 *  \ingroup RichardsParam
 *  \author Lukas Riedel
 *  \date 2018
 *  \see ScalingAdapterFactory conveniently creates derived objects
 */
template<typename Traits>
class ScalingAdapter
{
protected:
    using RF = typename Traits::RF;
    using Domain = typename Traits::Domain;
    using GridView = typename Traits::GridView;
    using return_t = ScalingFactors<RF>;

    //! Interpolator storage
    std::vector<std::shared_ptr<Interpolator<RF, Traits::dim>>> _interpolators;
    /// Grid view optionally used for instatiating interpolators
    const GridView& _grid_view;
    /// Logger of this instance
    const std::shared_ptr<spdlog::logger> _log;

public:
    /// Create the adapter and store config data
    /** Take a YAML node as unnamed parameter to enforce derived classes to
     *  take one as argument, too.
     *  \log The logger for this instance (defaults to the Richards logger)
     */
    explicit ScalingAdapter (
        const YAML::Node&,
        const GridView& grid_view,
        const std::shared_ptr<spdlog::logger> log
    ):
        _grid_view(grid_view),
        _log(log)
    { }

    /// Evaluate this adapter.
    virtual return_t evaluate (const Domain& pos) const = 0;

    /// Destroy this adapter.
    virtual ~ScalingAdapter () = default;

protected:
    /// Add an interpolator based on the data in the config node
    /** \param cfg "data" node to read the interpolator data from
     *      This is typically a sub-node of the node given in the constructor.
     *  \param node_name Name of the ``cfg`` node, for nice error messages.
     */
    void add_interpolator (const YAML::Node& cfg, const std::string node_name)
    {
        this->_interpolators.emplace_back(
            InterpolatorFactory<Traits>::create(cfg,
                                                node_name,
                                                _grid_view,
                                                _log));
    }
};

/// A ScalingAdapter implementing Miller scaling.
/** Miller scaling assumes geometric similarity and only varies the local
 *  length scale of the porous medium.
 *
 *  The scaled values of matric head \f$ h_m \f$ and the conducitivity
 *  \f$ K \f$ with respect to the reference values \f$ h_m^* \f$ and
 *  \f$ K^* \f$ are calculated by applying the Miller scaling factor
 *  \f$ \xi \f$, which indicates a local length scale with respect to the
 *  reference length scale of the medium.
 *
 *  \f{align*}{
 *      h_m &= h_m^* \cdot \xi^{-1} \\
 *      K &= K^* \cdot \xi^2
 *  \f}
 *
 *  When entering data, the single dataset is used as Miller scaling factor
 *  \f$ \xi \f$. This scaling adapter effectively sets
 *
 *  \f{align*}{
 *      \xi_{h_m} = \xi_K = \xi
 *  \f}
 *
 *  \ingroup RichardsParam
 *  \author Lukas Riedel
 *  \date 2018
 */
template<typename Traits>
class MillerScalingAdapter : public ScalingAdapter<Traits>
{
private:
    using Base = ScalingAdapter<Traits>;
    using Domain = typename Base::Domain;
    using GridView = typename Base::GridView;
    using return_t = typename Base::return_t;

public:
    /// Create this adapter and the internal interpolator.
    explicit MillerScalingAdapter (
        const YAML::Node& scale_data_cfg,
        const GridView& grid_view,
        const std::shared_ptr<spdlog::logger> log
    ):
        Base(scale_data_cfg, grid_view, log)
    {
        this->add_interpolator(scale_data_cfg["scale_miller"], "scale_miller");
    }

    /// Export type of this scaling adapter
    static inline std::string type = "Miller";

    /// Evaluate this interpolator
    return_t evaluate (const Domain& pos) const override
    {
        const auto scaling_value = this->_interpolators[0]->evaluate(pos);
        return return_t{scaling_value, scaling_value};
    }

    /// Delete this adapter
    ~MillerScalingAdapter () override = default;
};

/// ScalingAdapter implementing Miller scaling with additional porosity scaling.
/**
 *  For the Description of Miller scaling, see MillerScalingAdapter.
 *  In addition, the porosity is scaled through an additive scaling
 *  \f$ \xi_\theta \f$:
 *  \f[
 *    \Theta (\theta) =
 *      \frac{\theta - \theta_r}{\theta_s - \xi_\theta - \theta_r}
 *  \f]
 *
 *  The MilPor scaling violates the geometric similarity assumption of Miller
 *  scaling. The choice of \f$ -\xi_\theta \f$ (instead of \f$ +\xi_\theta \f$)
 *  is due to a material with smaller length scale (Miller scaling factor
 *  \f$ \xi < 1 \f$) having a larger porosity.
 *
 *  \ingroup RichardsParam
 *  \author Hannes Bauser
 *  \date 2018
 */
template<typename Traits>
class MilPorScalingAdapter : public ScalingAdapter<Traits>
{
private:
    using Base = ScalingAdapter<Traits>;
    using Domain = typename Base::Domain;
    using GridView = typename Base::GridView;
    using return_t = typename Base::return_t;

public:
    /// Create this adapter and the internal interpolator.
    explicit MilPorScalingAdapter (
        const YAML::Node& scale_data_cfg,
        const GridView& grid_view,
        const std::shared_ptr<spdlog::logger> log
    ):
        Base(scale_data_cfg, grid_view, log)
    {
        this->add_interpolator(scale_data_cfg["scale_miller"], "scale_miller");
        this->add_interpolator(scale_data_cfg["scale_porosity"], "scale_porosity");
    }

    /// Export type of this scaling adapter
    static inline std::string type = "MilPor";

    /// Evaluate this interpolator
    return_t evaluate (const Domain& pos) const override
    {
        const auto miller_scaling_value = this->_interpolators[0]->evaluate(pos);
        const auto porosity_scaling_value = this->_interpolators[1]->evaluate(pos);
        return return_t{miller_scaling_value, miller_scaling_value, porosity_scaling_value};
    }

    /// Delete this adapter
    ~MilPorScalingAdapter () override = default;
};

/// A ScalingAdapter that does not implement any scaling.
/** This adapter returns a default-initialized instance of ScalingFactors
 *  when evaluated.
 *
 *  \see ScalingFactors
 *  \ingroup RichardsParam
 *  \author Lukas Riedel
 *  \date 2018
 */
template<typename Traits>
class DummyScalingAdapter : public ScalingAdapter<Traits>
{
private:
    using Base = ScalingAdapter<Traits>;
    using Domain = typename Base::Domain;
    using GridView = typename Base::GridView;
    using return_t = typename Base::return_t;

public:
    /// Create this adapter. Don't initialize members.
    explicit DummyScalingAdapter (
        const YAML::Node& scale_data_cfg,
        const GridView& grid_view,
        const std::shared_ptr<spdlog::logger> log
    ):
        Base(scale_data_cfg, grid_view, log)
    { }

    /// Export type of this scaling adapter
    static inline std::string type = "none";

    /// Evaluate this scaling (always returns no scaling)
    return_t evaluate (const Domain& pos) const override
    {
        return return_t{};
    }

    /// Delete this adapter
    ~DummyScalingAdapter () override = default;
};


/// Factory class for creating ScalingAdapters.
/**
 *  \ingroup RichardsParam
 *  \author Lukas Riedel
 *  \date 2018
 *  \see ScalingAdapter is the base class of instances created by this factory
 */
template<typename Traits>
struct ScalingAdapterFactory
{
private:
    using RF = typename Traits::RF;
    using GridView = typename Traits::GridView;

public:

    /// Create a scaling adapter using a YAML config node
    /** \param type Type of the scaling adapter to be created
     *  \param config YAML config node specifying the scale adapter settings
     *  \param grid_view The grid view to use when data extensions are not
     *      specified by the user.
     *  \log The logger to use in the created object. Defaults to
     *      Dune::Dorie::log_richards.
     *  \return Shared pointer to the scaling adapter
     */
    static std::shared_ptr<ScalingAdapter<Traits>> create (
        const std::string& type,
        const YAML::Node& config,
        const GridView& grid_view,
        const std::shared_ptr<spdlog::logger> log=get_logger(log_richards)
    )
    {
        log->debug("Creating scaling adapter of type: {}", type);

        if (type == MillerScalingAdapter<Traits>::type) {
            return std::make_shared<MillerScalingAdapter<Traits>>(config,
                                                                  grid_view,
                                                                  log);
        }
        else if (type == MilPorScalingAdapter<Traits>::type) {
            return std::make_shared<MilPorScalingAdapter<Traits>>(config,
                                                                  grid_view,
                                                                  log);
        }
        else if (to_lower(type) == DummyScalingAdapter<Traits>::type) {
            return std::make_shared<DummyScalingAdapter<Traits>>(config,
                                                                 grid_view,
                                                                 log);
        }
        else {
            log->error("Unknown scaling type: {}", type);
            DUNE_THROW(Dune::NotImplemented, "Unknown scaling type");
        }
    }
};

} // namespace Parameterization
} // namespace Dorie
} // namespace Dune

#endif // DUNE_DORIE_PARAM_SCALING_HH
