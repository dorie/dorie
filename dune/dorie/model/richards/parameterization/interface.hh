#ifndef DUNE_DORIE_PARAM_RICHARDS_INTERFACE_HH
#define DUNE_DORIE_PARAM_RICHARDS_INTERFACE_HH

#include <functional>
#include <string>
#include <map>

#include <dune/common/float_cmp.hh>

namespace Dune {
namespace Dorie {
namespace Parameterization {

/// Interface for parameterizations of Richards equation.
/** Defines the basic parameters and the relation between water content
 *  and saturation
 *  @f{align*}{
 *      \phi &:= \theta_s \\
 *      \theta (\Theta) &= \Theta \left[ \phi - \theta_r \right] + \theta_r
 *  @f}
 *  where \f$ \phi \f$ is the porosity, which might be scaled with a
 *  ScalingFactors::scale_por.
 *
 *  \see FlowParameters stores one instance of a derived class for every
 *      medium layer.
 *  \ingroup RichardsParam
 */
template <typename Traits>
class Richards
{
private:
    using RF = typename Traits::RF;

public:

    // These are standard types for all parameterizations of Richards Equation
    /// Type of water saturation
    struct Saturation
    {
        RF value;
    };

    /// Type of volumetric water content
    struct WaterContent
    {
        RF value;
    };

    /// Type of hydraulic conductivity
    struct Conductivity
    {
        RF value;
    };

    /// Type of matric head
    struct MatricHead
    {
        RF value;
    };

    /// Parameter defining the saturated hydraulic conductivity
    struct SaturatedConductivity
    {
        RF value;
        inline static const std::string name = "k0";
    };

    /// Parameter defining the residual water content
    struct ResidualWaterContent
    {
        RF value;
        inline static const std::string name = "theta_r";
    };

    /// Parameter defining the saturated water content
    struct SaturatedWaterContent
    {
        RF value;
        inline static const std::string name = "theta_s";
    };

    //! Value of the residual water content.
    ResidualWaterContent _theta_r;
    //! Value of the saturated water content.
    SaturatedWaterContent _theta_s;
    //! Value of the saturated hydraulic conductivity.
    SaturatedConductivity _k0;
    //! The name of this parameterization instance, associated with the layer.
    const std::string _name;

public:

    /// Construct with default-initialized parameters
    /** \param name The name associated with this soil layer
     *  \param parameters Tuple of parameters to use in this parameterization
     */
    Richards (const std::string name) :
        _name(name)
    { }

    /// Construct from a tuple of parameters
    /** \param name The name associated with this soil layer
     *  \param parameters Tuple of parameters to use in this parameterization
     */
    template<typename... Args>
    Richards (const std::string name,
                              const std::tuple<Args...> parameters) :
        _theta_r(std::get<ResidualWaterContent>(parameters)),
        _theta_s(std::get<SaturatedWaterContent>(parameters)),
        _k0(std::get<SaturatedConductivity>(parameters)),
        _name(name)
    { }

    /// Default constructor (virtual).
    virtual ~Richards () = default;

    /// Return the name of this parameterization instance.
    const std::string& get_name() const { return _name; }

    /// Return a bound version of the water content function
    /** \param scale_por Porosity scaling factor
     *  \return Function: Saturation -> Water content
     */
    std::function<WaterContent(const Saturation)> water_content_f (
        const RF& scale_por
    ) const
    {
        verify_porosity_scaling(scale_por);

        return [this, &scale_por](const Saturation sat) {
            return WaterContent{
                sat.value * (_theta_s.value - _theta_r.value - scale_por)
                    + _theta_r.value
            };
        };
    }

    /// Return a bound version of the inverse water content function
    /** \param scale_por Porosity scaling factor
     *  \return Function: Water content -> Saturation
     *  \see water_content_f() for the inverse function
     */
    std::function<Saturation(const WaterContent)> water_content_f_inv (
        const RF& scale_por
    ) const
    {
        verify_porosity_scaling(scale_por);

        return [this, &scale_por](const WaterContent theta)
        {
            // Small value to avoid numerical issues
            constexpr double eps_min = 1E-6;

            const double sat = (theta.value - _theta_r.value)
                               / (_theta_s.value - scale_por - _theta_r.value);
            return Saturation{std::clamp(sat, eps_min, 1.0)};
        };
    }

    /// Return a bound version of the saturation function
    /** \return Function: Matric Head -> Water content
     *  \see matric_head_f for the inverse function
     */
    virtual std::function<Saturation(const MatricHead)> saturation_f () const
        = 0;

    /// Return a bound version of the inverse saturation function
    /** \return Function: Saturation -> Matric Head
     *  \see saturation_f for the inverse function
     */
    virtual std::function<MatricHead(const Saturation)> matric_head_f () const
        = 0;


    /// Return a bound version of the conductivity function
    /** \return Function: Saturation -> Hydraulic conductivity
     */
    virtual std::function<Conductivity(const Saturation)> conductivity_f ()
        const = 0;

    /// Return a multimap referecing all parameters by their names.
    /** \return Map. Key: Name of parameter (string).
     *      Value(s): Value of parameter (double&)
     */
    virtual std::multimap<std::string, double&> parameters () = 0;

    /// Return a map referecing all parameters by their names.
    /** \return Map. Key: Name of parameter (string).
     *      Value: Value of parameter (const double&)
     */
    virtual std::multimap<std::string, const double&> parameters () const = 0;

    /// Return a clone of this object
    /** \return a unique pointer with a copy of this object.
     */
    virtual std::unique_ptr<Richards<Traits>> clone () const = 0;

private:
    /// Check if the porosity scaling factor conforms to the parameterization
    /** \param scaling The porosity scaling factor to check
     */
    void verify_porosity_scaling (const RF scaling) const
    {
        if (FloatCmp::eq(scaling, 0.0))
            return;

        if (scaling >= _theta_s.value - _theta_r.value) {
            DUNE_THROW(Dune::IOError, "Invalid porosity scaling "
            "(negative water content).");
        }
        else if(_theta_s.value - scaling > 1.0){
            DUNE_THROW(Dune::IOError, "Invalid porosity scaling "
            "(water content exceeds 1).");
        }
    }

};

} // namespace Parameterization
} // namespace Dorie
} // namespace Dune

#endif // DUNE_DORIE_PARAM_RICHARDS_INTERFACE_HH
