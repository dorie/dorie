#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#ifndef DORIE_DIM
#error DORIE_DIM must be defined
#endif

#ifndef DORIE_RORDER
#error DORIE_RORDER must be defined
#endif

#include <dune/grid/yaspgrid.hh>
#include <dune/grid/uggrid.hh>

#include <dune/geometry/type.hh>

#include <dune/dorie/model/richards/richards.cc>

namespace Dune{
namespace Dorie{

using Geo = Dune::GeometryType::BasicType;

template class ModelRichards<ModelRichardsTraits<BaseTraits<YaspGrid<DORIE_DIM>, Geo::cube>,
                                                 DORIE_RORDER>>;
#if DORIE_RORDER > 0
template class ModelRichards<ModelRichardsTraits<BaseTraits<UGGrid<DORIE_DIM>, Geo::cube>,
                                                    DORIE_RORDER>>;
template class ModelRichards<ModelRichardsTraits<BaseTraits<UGGrid<DORIE_DIM>, Geo::simplex>,
                                                    DORIE_RORDER>>;
#endif

} // namespace Dorie
} // namespace Dune
