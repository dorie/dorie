// -*- tab-width: 4; indent-tabs-mode: nil -*-
#ifndef DUNE_DORIE_RICHARDS_OPERATOR_HH
#define DUNE_DORIE_RICHARDS_OPERATOR_HH

#include <vector>
#include <memory>

#include <dune/common/parametertree.hh>
#include <dune/common/exceptions.hh>

#include <dune/geometry/referenceelements.hh>

#include <dune/grid/common/mcmgmapper.hh>

#include <dune/pdelab/common/quadraturerules.hh>
#include <dune/pdelab/localoperator/pattern.hh>
#include <dune/pdelab/localoperator/flags.hh>
#include <dune/pdelab/localoperator/idefault.hh>
#include <dune/pdelab/localoperator/defaultimp.hh>
#include <dune/pdelab/finiteelement/localbasiscache.hh>

#include <dune/dorie/common/typedefs.hh>
#include <dune/dorie/model/richards/local_operator_cfg.hh>

namespace Dune {
namespace Dorie {
namespace Operator {

/// A local operator for solving the Richards equation using upwinding.
/** Written by Ole Klein, IWR, Heidelberg and modified by the
 *  DORiE developers, IUP, Heidelberg \n
 *  In theory it is possible that one and the same local operator is
 *  called first with a finite element of one type and later with a
 *  finite element of another type.  Since finite elements of different
 *  type will usually produce different results for the same local
 *  coordinate they cannot share a cache.  Here we use a vector of caches
 *  to allow for different orders of the shape functions, which should be
 *  enough to support p-adaptivity.  (Another likely candidate would be
 *  differing geometry types, i.e. hybrid meshes.)
 *
 * @ingroup    LocalOperators
 * @ingroup    RichardsModel
 */
template<typename Traits, typename Parameter, typename Boundary, typename SourceTerm, typename FEM, bool adjoint>
  class RichardsDGSpatialOperator
  : public Dune::PDELab::NumericalJacobianVolume<RichardsDGSpatialOperator<Traits, Parameter, Boundary, SourceTerm, FEM, adjoint> >,
  public Dune::PDELab::NumericalJacobianSkeleton<RichardsDGSpatialOperator<Traits, Parameter, Boundary, SourceTerm, FEM, adjoint> >,
  public Dune::PDELab::NumericalJacobianBoundary<RichardsDGSpatialOperator<Traits, Parameter, Boundary, SourceTerm, FEM, adjoint> >,
  public Dune::PDELab::FullSkeletonPattern,
  public Dune::PDELab::FullVolumePattern,
  public Dune::PDELab::LocalOperatorDefaultFlags,
  public Dune::PDELab::InstationaryLocalOperatorDefaultMethods<typename Traits::RangeField>
{
protected:

  enum { dim = Traits::dim };
  static_assert(dim!=1, "RichardsDGSpatialOperator does not work for world dimension 1!");
  
  typedef typename Traits::RangeField         RF;
  typedef typename Traits::Scalar             Scalar;
  typedef typename Traits::Vector             Vector;
  typedef typename Traits::Tensor             Tensor;
  typedef typename Traits::DomainField        DF;
  typedef typename Traits::Domain             Domain;
  typedef typename Traits::IntersectionDomain ID;
  typedef typename Traits::Element            Element;


  typedef typename FEM::Traits::FiniteElementType::Traits::LocalBasisType LocalBasisType;
  typedef Dune::PDELab::LocalBasisCache<LocalBasisType> Cache;

  std::shared_ptr<const Parameter> param; //!< class providing equation parameters
  std::shared_ptr<const Boundary> boundary; //!< class providing boundary conditions
  std::shared_ptr<const SourceTerm> sourceTerm; //!< class providing source term information
  const RichardsDGMethod method; //!< Interior penalty type
  const RichardsUpwinding upwinding; //!< DG method weights switch
  const RichardsDGWeights weights; //!< Gradient weighting

  const int intorderadd; //!< integration order addend
  const int quadrature_factor; //!< factor to FEM integration order
  RF penalty_factor; //!< Interior penalty factor
  RF theta; //!< Symmetry factor
  RF time;  //!< operator internal time for BC, parameter, and source term queries

  std::vector<Cache> cache; //!< Local basis cache

  enum class LOPCase {DG,RTVolume,RTSkeleton}; //!< Local operator case

public:

  // pattern assembly flags
  enum { doPatternVolume = true };
  enum { doPatternSkeleton = true };

  // residual assembly flags
  enum { doAlphaVolume  = true };
  enum { doAlphaSkeleton  = true };
  enum { doAlphaBoundary  = true };
  enum { doLambdaVolume   = true };

  /// Create operator.
  /**
   *  \param param_ Object providing equation parameters
   *  \param boundary_ Object prividing boundary conditions
   *  \param sourceTerm_ Object providing source term information
   *  \param method_ DG method type \see RichardsDGMethod
   *  \param upwinding_ Upwinding type \see RichardsUpwinding
   *  \param weights_ DG weigths switch \see RichardsDGWeights
   *  \param intorderadd_ Addend to integration order
   *  \param quadrature_factor_ Factor to FEM integration order
   */
  RichardsDGSpatialOperator (
    const Dune::ParameterTree& config,
    std::shared_ptr<const Parameter> param_,
    std::shared_ptr<const Boundary> boundary_,
    std::shared_ptr<const SourceTerm> sourceTerm_,
    const RichardsDGMethod method_ = RichardsDGMethod::SIPG,
    const RichardsUpwinding upwinding_ = RichardsUpwinding::none,
    const RichardsDGWeights weights_ = RichardsDGWeights::weightsOn,
    int intorderadd_ = 2,
    int quadrature_factor_ = 2
  ) :
    Dune::PDELab::NumericalJacobianVolume<RichardsDGSpatialOperator<Traits,Parameter,Boundary,SourceTerm,FEM,adjoint> >(1.e-7),
    Dune::PDELab::NumericalJacobianSkeleton<RichardsDGSpatialOperator<Traits,Parameter,Boundary,SourceTerm,FEM,adjoint> >(1.e-7),
    Dune::PDELab::NumericalJacobianBoundary<RichardsDGSpatialOperator<Traits,Parameter,Boundary,SourceTerm,FEM,adjoint> >(1.e-7),
    param(param_),
    boundary(boundary_),
    sourceTerm(sourceTerm_),
    method(method_),
    upwinding(upwinding_),
    weights(weights_),
    intorderadd(intorderadd_),
    quadrature_factor(quadrature_factor_),
    penalty_factor(config.get<RF>("numerics.penaltyFactor")),
    time(0.0),
    cache(20)
  {
    theta = 0.; // IIP
    if (method == RichardsDGMethod::NIPG || method == RichardsDGMethod::OBB)
      theta = 1.;
    else if (method == RichardsDGMethod::SIPG)
      theta = -1.;

    if (method == RichardsDGMethod::OBB)
      penalty_factor = 0.;
  }

  /// Volume integral depending on test and ansatz functions
  /** Richards Equation - Strong formulation of volume integral
   *  \f[
   *    \int_{\Omega_e} K (\Theta (h_m^*)) \cdot (\nabla u - \mathbf{\hat{g}}) \cdot \nabla v \; dx
   *  \f]
   *  Miller Similarity Transformations:
   *  \f{eqnarray*}{
   *    h_m \; &\xrightarrow{\text{MS}^{-1}} \; & h_m^* \\
   *    K^*(\Theta(h_m^*)) \; &\xrightarrow{\text{MS}} \;  & K(\Theta(h_m^*))
   *  \f}
   */
  template<typename EG, typename LFSU, typename X, typename LFSV, typename R>
    void alpha_volume (const EG& eg, const LFSU& lfsu, const X& x, const LFSV& lfsv, R& r) const
    {
      // Switches between local and global interface in test local space
      using FETest = typename LFSV::Traits::FiniteElementType;
      using BasisTest = typename FETest::Traits::LocalBasisType;

      /* If range dimension of the test local function space is 1 we assume that
       * it is a P_k/Q_k space (probably conforming), if it is equal to world 
       * dimension we assume that it is a vector function space representing a 
       * gradient (e.g. Raviart Thomas) which is non conforming with respect to 
       * the ansatz space.*/

      constexpr int dimRangeTest = BasisTest::Traits::dimRange;
      constexpr LOPCase lopcase = (dimRangeTest==dim) ? LOPCase::RTVolume : LOPCase::DG;

      const int order = lfsu.finiteElement().localBasis().order();
      const int intorder = intorderadd + quadrature_factor * order;

      // get element geomentry
      auto gt = eg.geometry();

      // bind parameterization and retrieve functions
      param->bind(eg.entity());
      const auto saturation_f = param->saturation_f();
      const auto conductivity_f = param->conductivity_f();

      // loop over quadrature points
      for (const auto& it : quadratureRule(gt,intorder))
      {
        // evaluate position in element local and global coordinates
        const auto p_local = it.position();

        // evaluate basis functions
        auto& phiu = cache[order].evaluateFunction(p_local,lfsu.finiteElement().localBasis());

        // evaluate u
        RF u = 0.;
        for (unsigned int i = 0; i<lfsu.size(); i++)
          u += x(lfsu,i) * phiu[i];

        // evaluate gradient of basis function
        const auto& js = cache[order].evaluateJacobian(p_local,lfsu.finiteElement().localBasis());

        // transform gradients to real element
        const auto jac = gt.jacobianInverseTransposed(p_local);
        std::vector<Vector> gradphiu(lfsu.size());
        for (unsigned int i = 0; i<lfsu.size(); i++)
          jac.mv(js[i][0],gradphiu[i]);

        std::vector<Vector> gradphiv(lfsv.size());

        if constexpr (lopcase == LOPCase::RTVolume) {
          // (we assume lfsv = RTk)
          lfsv.finiteElement().localBasis().evaluateFunction(p_local,gradphiv);
          // Piola transformation
          auto BTransposed = gt.jacobianTransposed(p_local);
          auto detB = BTransposed.determinant();
          for (unsigned int i=0; i<lfsv.size(); i++) {
            Vector y(gradphiv[i]);
            y /= detB;
            BTransposed.mtv(y,gradphiv[i]);
          }
        } else if constexpr (lopcase == LOPCase::DG) {
          // (we assume Galerkin method lfsu = lfsv)
          gradphiv = gradphiu;
        } else {
          static_assert(Dune::AlwaysTrue<EG>::value,
            "alpha_volume() does not support RTSkeleton case!");
        }

        // compute gradient of u
        Vector gradu(0.);
        for (unsigned int i = 0; i<lfsu.size(); i++)
          gradu.axpy(x(lfsu,i),gradphiu[i]);

        gradu -= param->gravity();

        // compute conductivity
        const RF cond = conductivity_f(saturation_f(u));

        // integration factor
        const RF factor = it.weight() * gt.integrationElement(p_local);

        // update residual
        if constexpr (lopcase != LOPCase::RTSkeleton) {
          
          // discrete gradient sign: Negative for lifting
          double dg_sign = (lopcase == LOPCase::RTVolume) ? -1. : 1.;
          
          for (unsigned int i = 0; i<lfsv.size(); i++)
            r.accumulate(lfsv,i, dg_sign * cond * (gradu*gradphiv[i]) * factor);
        }
      }
    }

  /// Skeleton integral depending on test and ansatz functions
  /** Each face is only visited ONCE!\n
   * Weights for symmetry term:
   *  \f[
   *    \omega_x = \frac{ K_{0,y} }{ K_{0,x} + K_{0,y}} \; , \quad x,y \in \lbrace s,n \rbrace , \; x \neq y
   *  \f]
   * Solution jump over face:
   *  \f[
   *    \Delta u = u_s - u_n
   *  \f]
   * Penalty factor:
   *  \f{eqnarray*}{
   *    p &= \mathsf{p} \cdot k (k+d-1) / h_f \\
   *    h_f &= \frac{\min \lbrace \Omega_s,\Omega_n \rbrace }{\Omega_i}\\
   *    d \; &: \text{spatial dimension}\\
   *    k \; &: \text{polynomial order}\\
   *    \Omega_s,\Omega_n \in \mathbb{R}^d \; &: \text{element volumes}\\
   *    \Omega_i \in \mathbb{R}^{d-1} \; &: \text{interface surface}
   *  \f}
   */
  template<typename IG, typename LFSU, typename X, typename LFSV, typename R>
    void alpha_skeleton (const IG& ig,
        const LFSU& lfsu_s, const X& x_s, const LFSV& lfsv_s,
        const LFSU& lfsu_n, const X& x_n, const LFSV& lfsv_n,
        R& r_s, R& r_n) const
    {
      // Switches between local and global interface in test local space
      using FETest = typename LFSV::Traits::FiniteElementType;
      using BasisTest = typename FETest::Traits::LocalBasisType;

      /* If domain dimension of the test local function space is dim we assume
       * that it is a P_k/Q_k space (probably conforming), if it is dim-1 we
       * assume that it is a skeleton finite element function space
       * which is non conforming with respect to the ansatz space.*/

      constexpr int dimDomainLocalTest = BasisTest::Traits::dimDomain;
      constexpr int dimRangeTest = BasisTest::Traits::dimRange;
      constexpr LOPCase lopcase = (dimDomainLocalTest==dim-1) ? LOPCase::RTSkeleton : 
                                  (dimRangeTest==dim ? LOPCase::RTVolume : LOPCase::DG);

      // get polynomial degree
      const int order_s  = lfsu_s.finiteElement().localBasis().order();
      const int order_n  = lfsu_n.finiteElement().localBasis().order();
      const int degree   = std::max( order_s, order_n );
      const int intorder = intorderadd + quadrature_factor * degree;

      // get element geometry
      auto gtface = ig.geometry();

      // bind parameterization and retrieve functions
      param->bind(ig.inside());
      const auto saturation_f_s = param->saturation_f();
      const auto conductivity_f_s = param->conductivity_f();

      // cache saturated conductivity
      const RF satCond_s = conductivity_f_s(1.0);

      param->bind(ig.outside());
      const auto saturation_f_n = param->saturation_f();
      const auto conductivity_f_n = param->conductivity_f();

      const RF satCond_n = conductivity_f_n(1.0);

      // geometric factor of penalty
      const RF h_F = std::min(ig.inside().geometry().volume(),ig.outside().geometry().volume())/ig.geometry().volume(); // Houston!

      // loop over quadrature points and integrate normal flux
      for (const auto& it : quadratureRule(gtface,intorder))
      {
        // position of quadrature point in local coordinates of elements
        const auto p_local_s = ig.geometryInInside().global(it.position());
        const auto p_local_n = ig.geometryInOutside().global(it.position());

        // face normal vector
        const Domain normal = ig.unitOuterNormal(it.position());

        // evaluate basis functions of trial function
        const auto& phiu_s = cache[order_s].evaluateFunction(p_local_s,lfsu_s.finiteElement().localBasis());
        const auto& phiu_n = cache[order_n].evaluateFunction(p_local_n,lfsu_n.finiteElement().localBasis());

        std::vector<Scalar> phiv_s(lfsv_s.size());
        std::vector<Scalar> phiv_n(lfsv_n.size());

        // evaluate gradient of basis function
        const auto& jsu_s = cache[order_s].evaluateJacobian(p_local_s,lfsu_s.finiteElement().localBasis());
        const auto& jsu_n = cache[order_n].evaluateJacobian(p_local_n,lfsu_n.finiteElement().localBasis());

        // transform gradients to real element
        Tensor jac;
        std::vector<Vector> gradphiu_s(lfsu_s.size());
        std::vector<Vector> gradphiu_n(lfsu_n.size());
        jac = ig.inside().geometry().jacobianInverseTransposed(p_local_s);
        for (unsigned int i = 0; i<lfsu_s.size(); i++)
          jac.mv(jsu_s[i][0],gradphiu_s[i]);
        jac = ig.outside().geometry().jacobianInverseTransposed(p_local_n);
        for (unsigned int i = 0; i<lfsu_n.size(); i++)
          jac.mv(jsu_n[i][0],gradphiu_n[i]);

        std::vector<Vector> gradphiv_s(lfsv_s.size());
        std::vector<Vector> gradphiv_n(lfsv_n.size());

        // evaluate basis functions of test function
        if constexpr (lopcase == LOPCase::RTSkeleton) {
          // Evaluate position in local coordinates of the inside entity of codim 1
          using LocalCoordinate = typename IG::LocalGeometry::LocalCoordinate;
          LocalCoordinate sub_p_local_s, sub_p_local_n;

          if (ig.intersection().conforming()) {
            sub_p_local_s = it.position();
            sub_p_local_n = it.position();
          } else {
            auto sub_entity_s_id = ig.intersection().indexInInside();
            auto sub_entity_s = ig.inside().template subEntity<1>(sub_entity_s_id);
            sub_p_local_s = sub_entity_s.geometry().local(gtface.global(it.position()));

            auto sub_entity_n_id = ig.intersection().indexInOutside();
            auto sub_entity_n = ig.outside().template subEntity<1>(sub_entity_n_id);
            sub_p_local_n = sub_entity_n.geometry().local(gtface.global(it.position()));
          }

          // (we assume non-Galerkin method lfsu != lfsv)
          lfsv_s.finiteElement().localBasis().evaluateFunction(sub_p_local_s,phiv_s);
          lfsv_n.finiteElement().localBasis().evaluateFunction(sub_p_local_n,phiv_n);
        } else if constexpr (lopcase == LOPCase::RTVolume) {
          // (we assume lfsv = RTk)
          lfsv_s.finiteElement().localBasis().evaluateFunction(p_local_s,gradphiv_s);
          lfsv_n.finiteElement().localBasis().evaluateFunction(p_local_n,gradphiv_n);
          // Piola transformation
          auto BTransposed_s = ig.inside().geometry().jacobianTransposed(p_local_s);
          auto detB_s = BTransposed_s.determinant();
          for (unsigned int i=0; i<lfsv_s.size(); i++) {
            Vector y_s(gradphiv_s[i]);
            y_s /= detB_s;
            BTransposed_s.mtv(y_s,gradphiv_s[i]);
          }
          auto BTransposed_n = ig.outside().geometry().jacobianTransposed(p_local_n);
          auto detB_n = BTransposed_n.determinant();
          for (unsigned int i=0; i<lfsv_n.size(); i++) {
            Vector y_n(gradphiv_n[i]);
            y_n /= detB_n;
            BTransposed_n.mtv(y_n,gradphiv_n[i]);
          }

        } else {
          // (we assume Galerkin method lfsu = lfsv)
          phiv_s = phiu_s;
          phiv_n = phiu_n;
          gradphiv_s = gradphiu_s;
          gradphiv_n = gradphiu_n;
        }

        // evaluate u
        RF u_s = 0., u_n = 0.;
        for (unsigned int i = 0; i<lfsu_s.size(); i++)
          u_s += x_s(lfsu_s,i) * phiu_s[i];
        for (unsigned int i = 0; i<lfsu_n.size(); i++)
          u_n += x_n(lfsu_n,i) * phiu_n[i];

        // compute gradient of u
        Vector gradu_s(0.), gradu_n(0.);
        for (unsigned int i = 0; i<lfsu_s.size(); i++)
          gradu_s.axpy(x_s(lfsu_s,i),gradphiu_s[i]);
        for (unsigned int i = 0; i<lfsu_n.size(); i++)
          gradu_n.axpy(x_n(lfsu_n,i),gradphiu_n[i]);

        // update with gravity vector
        gradu_s -= param->gravity();
        gradu_n -= param->gravity();

        // compute jump in solution
        const RF jump = u_s - u_n;

        // conductivities
        const RF cond_s = conductivity_f_s(saturation_f_s(u_s));
        const RF cond_n = conductivity_f_n(saturation_f_n(u_n));

        // compute weights
        RF omega_s = 0.5, omega_n = 0.5;
        if (weights == RichardsDGWeights::weightsOn)
        {
          if (upwinding == RichardsUpwinding::none)
          {
            omega_s = cond_n / ((cond_s) + (cond_n));
            omega_n = cond_s / ((cond_s) + (cond_n));
          }
          else if (upwinding == RichardsUpwinding::semiUpwind
            || upwinding == RichardsUpwinding::fullUpwind)
          {
            omega_s = satCond_n / (satCond_s + satCond_n);
            omega_n = satCond_s / (satCond_s + satCond_n);
          }
        }

        // integration factor
        const RF factor = it.weight() * ig.geometry().integrationElement(it.position());

        // penalty factor
        const RF penalty = penalty_factor / h_F * degree * (degree + dim - 1);

        // relative conductivites
        const RF relCond_s = cond_s / satCond_s;
        const RF relCond_n = cond_n / satCond_n;

        // compute numerical flux
        RF numFlux_s(0.);
        RF numFlux_n(0.);
        
        numFlux_s = numFlux_n = penalty * jump;
        numFlux_s -= gradu_s * normal;
        numFlux_n -= gradu_n * normal;
        numFlux_s *= satCond_s;
        numFlux_n *= satCond_n;

        if (upwinding == RichardsUpwinding::none)
        {
          numFlux_s *= relCond_s;
          numFlux_n *= relCond_n;
        }
        const RF numFlux = omega_s * numFlux_s + omega_n * numFlux_n;

        // Upwind in relative conductivity
        RF relCond_upwind=0.0, satCond_upwind=0.0;
        if (upwinding == RichardsUpwinding::semiUpwind
          || upwinding == RichardsUpwinding::fullUpwind)
        {
          RF cond_upwind;
          if (numFlux > 0)
          {
            cond_upwind = conductivity_f_s(saturation_f_s(u_s));
            satCond_upwind = satCond_s;
          }
          else
          {
            cond_upwind = conductivity_f_n(saturation_f_n(u_n));
            satCond_upwind = satCond_n;
          }

          relCond_upwind = cond_upwind / satCond_upwind;
        }

        if constexpr (lopcase != LOPCase::RTVolume)
        {
          // update residual (flux term)
          // diffusion term
          // consistency term
          // + penalty term
          if (upwinding == RichardsUpwinding::none)
          {
            for (unsigned int i = 0; i < lfsv_s.size(); i++)
              r_s.accumulate(lfsv_s, i,   numFlux * phiv_s[i] * factor);
            for (unsigned int i = 0; i < lfsv_n.size(); i++)
              r_n.accumulate(lfsv_n, i, - numFlux * phiv_n[i] * factor);
          }
          else if (upwinding == RichardsUpwinding::semiUpwind)
          {
            for (unsigned int i = 0; i<lfsv_s.size(); i++)
              r_s.accumulate(lfsv_s,i,   relCond_upwind * numFlux * phiv_s[i] * factor);
            for (unsigned int i = 0; i<lfsv_n.size(); i++)
              r_n.accumulate(lfsv_n,i, - relCond_upwind * numFlux * phiv_n[i] * factor);
          }
          else if (upwinding == RichardsUpwinding::fullUpwind)
          {
            // Upwind numerical flux
            double numFlux_upwind = (numFlux > 0) ? numFlux_s : numFlux_n;

            for (unsigned int i = 0; i < lfsv_s.size(); i++)
              r_s.accumulate(lfsv_s, i,   relCond_upwind * numFlux_upwind * phiv_s[i] * factor);
            for (unsigned int i = 0; i < lfsv_n.size(); i++)
              r_n.accumulate(lfsv_n, i, - relCond_upwind * numFlux_upwind * phiv_n[i] * factor);
          }
        }

        if constexpr (lopcase != LOPCase::RTSkeleton)
        {
          // update residual (symmetry term)
          // (non-)symmetric IP term
          // symmetry term
          
          // discrete gradient sign: Negative for lifting
          double dg_sign = (lopcase == LOPCase::RTVolume) ? -1. : 1.;

          if (upwinding == RichardsUpwinding::none)
          {
            for (unsigned int i = 0; i < lfsv_s.size(); i++)
              r_s.accumulate(lfsv_s, i, dg_sign * theta * omega_s * (gradphiv_s[i] * normal) * relCond_s * satCond_s * jump * factor);
            for (unsigned int i = 0; i < lfsv_n.size(); i++)
              r_n.accumulate(lfsv_n, i, dg_sign * theta * omega_n * (gradphiv_n[i] * normal) * relCond_n * satCond_n * jump * factor);
          }
          else if (upwinding == RichardsUpwinding::semiUpwind)
          {
            for (unsigned int i = 0; i<lfsv_s.size(); i++)
              r_s.accumulate(lfsv_s,i, dg_sign * theta * omega_s * (gradphiv_s[i] * normal) * relCond_upwind * satCond_s * jump * factor);
            for (unsigned int i = 0; i<lfsv_n.size(); i++)
              r_n.accumulate(lfsv_n,i, dg_sign * theta * omega_n * (gradphiv_n[i] * normal) * relCond_upwind * satCond_n * jump * factor);
          }
          else if (upwinding == RichardsUpwinding::fullUpwind)
          {
            for (unsigned int i = 0; i < lfsv_s.size(); i++)
              r_s.accumulate(lfsv_s, i, dg_sign * theta * (gradphiv_s[i] * normal) * relCond_upwind * satCond_upwind * jump * factor);
            for (unsigned int i = 0; i < lfsv_n.size(); i++)
              r_n.accumulate(lfsv_n, i, dg_sign * theta * (gradphiv_n[i] * normal) * relCond_upwind * satCond_upwind * jump * factor);
          }
        }
      }
    }

  /**
   * @brief Boundary integral depending on test and ansatz functions
   * \todo there is additional code for the limited influx BC, which is currently commented out
   */
  template<typename IG, typename LFSU, typename X, typename LFSV, typename R>
    void alpha_boundary (const IG& ig,
        const LFSU& lfsu_s, const X& x_s, const LFSV& lfsv_s,
        R& r_s) const
    {
      // Switches between local and global interface in test local space
      using FETest = typename LFSV::Traits::FiniteElementType;
      using BasisTest = typename FETest::Traits::LocalBasisType;

      /* If domain dimension of the test local function space is dim we assume
       * that it is a P_k/Q_k space (probably conforming), if it is dim-1 we
       * assume that it is a skeleton finite element function space
       * which is non conforming with respect to the ansatz space.*/

      constexpr int dimDomainLocalTest = BasisTest::Traits::dimDomain;
      constexpr int dimRangeTest = BasisTest::Traits::dimRange;
      constexpr LOPCase lopcase = (dimDomainLocalTest==dim-1) ? LOPCase::RTSkeleton : 
                                  (dimRangeTest==dim ? LOPCase::RTVolume : LOPCase::DG);

      // get polynomial degree
      const int order_s  = lfsu_s.finiteElement().localBasis().order();
      const int degree   = order_s;
      const int intorder = intorderadd + quadrature_factor * degree;

      // geometric factor of penalty
      const RF h_F = ig.inside().geometry().volume()/ig.geometry().volume(); // Houston!

      // get element geometry
      auto gtface = ig.geometry();

      // bind parameterization and retrieve functions
      param->bind(ig.inside());
      const auto saturation_f_s = param->saturation_f();
      const auto conductivity_f_s = param->conductivity_f();

      // cache saturated conductivity
      const RF satCond_s = conductivity_f_s(1.0);

      // query the boundary condition
      const auto bc = boundary->bc(ig.intersection());
      const auto bc_type = bc->type();

      // loop over quadrature points and integrate normal flux
      for (const auto& it : quadratureRule(gtface,intorder))
      {
        // position of quadrature point in local coordinates of elements
        const auto p_local_s = ig.geometryInInside().global(it.position());

        // evaluate basis functions of trial function
        const auto& phiu_s = cache[order_s].evaluateFunction(p_local_s,lfsu_s.finiteElement().localBasis());

        std::vector<Scalar> phiv_s(lfsv_s.size());

        // evaluate gradient of basis functions (we assume Galerkin method lfsu = lfsv)
        const auto& jsu_s = cache[order_s].evaluateJacobian(p_local_s,lfsu_s.finiteElement().localBasis());

        // transform gradients to real element
        const auto js = ig.inside().geometry().jacobianInverseTransposed(p_local_s);
        std::vector<Vector> gradphiu_s(lfsu_s.size());
        for (unsigned int i = 0; i<lfsu_s.size(); i++)
          js.mv(jsu_s[i][0],gradphiu_s[i]);

        std::vector<Vector> gradphiv_s(lfsv_s.size());

        // evaluate basis functions of test function
        if constexpr (lopcase == LOPCase::RTSkeleton) {
          // (we assume non-Galerkin method lfsu != lfsv)
          lfsv_s.finiteElement().localBasis().evaluateFunction(it.position(),phiv_s);
        } else if constexpr (lopcase == LOPCase::RTVolume) {
          // (we assume lfsv = RTk)
          lfsv_s.finiteElement().localBasis().evaluateFunction(p_local_s,gradphiv_s);
          // Piola transformation
          auto BTransposed_s = ig.inside().geometry().jacobianTransposed(p_local_s);
          auto detB_s = BTransposed_s.determinant();
          for (unsigned int i=0; i<lfsv_s.size(); i++) {
            Vector y_s(gradphiv_s[i]);
            y_s /= detB_s;
            BTransposed_s.mtv(y_s,gradphiv_s[i]);
          }
        } else {
          // (we assume Galerkin method lfsu = lfsv)
          phiv_s = phiu_s;
          gradphiv_s = gradphiu_s;
        }
        // integration factor
        const RF factor = it.weight() * ig.geometry().integrationElement(it.position());

        // evaluate u
        RF u_s = 0.;
        for (unsigned int i = 0; i<lfsu_s.size(); i++)
          u_s += x_s(lfsu_s,i) * phiu_s[i];

        // update residual according to byType flag
        if (bc_type == BCType::Neumann)
        {
          // flux is given parallel to gravity vector
          RF normal_flux = bc->evaluate(time);

          // adjust flux to surface tilt
          const auto& bc_neumann
            = dynamic_cast<const NeumannBoundaryCondition<RF>&>(*bc);
          if (bc_neumann.horizontal_projection())
            normal_flux *= std::abs( ig.centerUnitOuterNormal() * param->gravity() );

          // update residual (flux term)
          if constexpr (lopcase != LOPCase::RTVolume)
            for (unsigned int i = 0; i<lfsv_s.size(); i++)
              r_s.accumulate(lfsv_s,i, normal_flux * phiv_s[i] * factor);

          continue;
        }

        else if (bc_type == BCType::Dirichlet or bc_type == BCType::Outflow)
        {
          // compute gradient of u
          Vector gradu_s(0.);
          for (unsigned int i = 0; i<lfsu_s.size(); i++)
            gradu_s.axpy(x_s(lfsu_s,i),gradphiu_s[i]);

          // update with gravity vector
          gradu_s -= param->gravity();

          // face normal vector
          const Domain normal = ig.unitOuterNormal(it.position());

          // jump relative to Dirichlet value
          const RF g = bc->evaluate(time);
          const RF jump = u_s - g;

          // conductivity factors
          const RF relCond_s =
            conductivity_f_s(saturation_f_s(u_s)) / satCond_s;
          const RF relCond_n =
            conductivity_f_s(saturation_f_s(g)) / satCond_s;

          // penalty
          const RF penalty = penalty_factor / h_F * degree * (degree + dim - 1);

          // compute numerical flux
          RF numFlux = satCond_s*(penalty * jump - gradu_s * normal);

          // compute conductivity factor
          RF relCond;
          if (upwinding == RichardsUpwinding::semiUpwind
            || upwinding == RichardsUpwinding::fullUpwind)
          {
            if (numFlux > 0)
              relCond = relCond_s;
            else
              relCond = relCond_n;
          }
          else // (upwinding == RichardsUpwinding::none)
          {
            // harmonic average of conductivity factors
            relCond = 2 * relCond_s * relCond_n / (relCond_s + relCond_n);
          }

          // Avoid accumulating inflow for outflow BC
          if (bc_type == BCType::Outflow and
              FloatCmp::lt(double(numFlux), 0.0)) {
            continue;
          }

          if constexpr (lopcase != LOPCase::RTVolume)
          {
            // update residual (flux term)
            // diffusion term
            // consistency term
            // + penalty term
            for (unsigned int i = 0; i<lfsv_s.size(); i++)
              r_s.accumulate(lfsv_s,i, relCond * numFlux * phiv_s[i] * factor);
          }

          if constexpr (lopcase != LOPCase::RTSkeleton)
          {
            // update residual (symmetry term)
            // (non-)symmetric IP term
            // symmetry term

            // discrete gradient sign: Negative for lifting
            double dg_sign = (lopcase == LOPCase::RTVolume) ? -1. : 1.;

            for (unsigned int i = 0; i<lfsv_s.size(); i++)
              r_s.accumulate(lfsv_s,i, dg_sign * theta * (gradphiv_s[i] * normal) * relCond * satCond_s * jump * factor);
          }
          continue;
        }

        else if (bc->type() == BCType::none)
          continue;

        // The following implementation leads to some upwinding and nicer numerical behavior, but does not allow for water
        // stowage on the soil surface. Consider reimplementation if numerical problems occur with a limitedFlux boundary condition.

/*            if (BoundaryCondition::isLimitedFlux(bc))
        {
          const RF satCond_s   = param->K(*(ig.inside()),local_s);
          normal_flux = boundary->j(ig.intersection(),it.position(),time);
          normal_flux *= std::abs( ig.centerUnitOuterNormal() * param->gravity() );

          if (normal_flux < - satCond_s)
          {
            //std::cout << "- satCond " << - satCond_s << " flux " << normal_flux << std::endl;
            for (unsigned int i = 0; i < lfsv_s.size(); i++)
              r_s.accumulate(lfsv_s,i, - satCond_s * phi_s[i] * factor);
          }
          else if (normal_flux > satCond_s)
          {
            //std::cout << "  satCond " <<   satCond_s << " flux " << normal_flux << std::endl;
            for (unsigned int i = 0; i < lfsv_s.size(); i++)
              r_s.accumulate(lfsv_s,i,   satCond_s * phi_s[i] * factor);
          }
          else
          {
            for (unsigned int i = 0; i < lfsv_s.size(); i++)
              r_s.accumulate(lfsv_s,i, normal_flux * phi_s[i] * factor);
          }

          continue;
        }*/

      }
    }

  /**
   * @brief Volume integral depending only on test functions
   */
  template<typename EG, typename LFSV, typename R>
    void lambda_volume (const EG& eg, const LFSV& lfsv, R& r) const
    {
      const int order = lfsv.finiteElement().localBasis().order();
      const int intorder = intorderadd + quadrature_factor * order;

      // get element geometry
      auto gt = eg.geometry();

      // loop over quadrature points
      for (const auto& it : quadratureRule(gt,intorder))
      {
        // evaluate position in element local coordinates
        const auto p_local = it.position();

        // evaluate values of shape functions (we assume Galerkin method lfsu = lfsv)
        std::vector<Scalar> phi(lfsv.size());
        lfsv.finiteElement().localBasis().evaluateFunction(p_local,phi);

        // scaled weight factor
        const RF factor = it.weight() * gt.integrationElement(p_local);

        const RF source = sourceTerm->q(eg.entity(),p_local,time) / gt.volume();

        // update residual
        for (unsigned int i = 0; i<lfsv.size(); i++)
          r.accumulate(lfsv,i, - source * phi[i] * factor);

        //std::cout << "lambda " << - source << std::endl;
      }
    }

  //! set operator time
  void setTime (RF t)
  {
    time = t;
  }

  //! set operator time and erase BC cache map
  void preStep (RF t, RF dt, int stages)
  {
    time = t;
  }
};

/**
 * @brief DG local operator for the temporal derivative of the Richards equation
 *
 * @ingroup LocalOperators
 * @ingroup RichardsModel
 */
template<typename Traits, typename Parameter, typename FEM, bool adjoint>
  class RichardsDGTemporalOperator
  : public Dune::PDELab::NumericalJacobianVolume<RichardsDGTemporalOperator<Traits, Parameter, FEM, adjoint> >,
  public Dune::PDELab::NumericalJacobianApplyVolume<RichardsDGTemporalOperator<Traits, Parameter, FEM, adjoint> >,
  public Dune::PDELab::FullSkeletonPattern,
  public Dune::PDELab::FullVolumePattern,
  public Dune::PDELab::LocalOperatorDefaultFlags,
  public Dune::PDELab::InstationaryLocalOperatorDefaultMethods<typename Traits::RangeField>
{
  //typedef typename Parameter::Traits::GridTraits GridTraits;

  enum { dim = Traits::dim };

  typedef typename Traits::RangeField RF;
  typedef typename Traits::Scalar Scalar;
  typedef typename Traits::DomainField DF;
  typedef typename Traits::Domain Domain;

  public:

  // pattern assembly flags
  enum { doPatternVolume = true };

  // residual assembly flags
  enum { doAlphaVolume = true };

  private:

  std::shared_ptr<const Parameter> param; //!< class providing equation parameters
  RF time;
  const int intorderadd;
  const int quadrature_factor;

  public:

  /**
   * @brief Constructor
   */
  RichardsDGTemporalOperator (
    const Dune::ParameterTree& config,
    std::shared_ptr<const Parameter> param_,
    int intorderadd_ = 0,
    int quadrature_factor_ = 2
  ) :
    Dune::PDELab::NumericalJacobianVolume<RichardsDGTemporalOperator<Traits, Parameter, FEM, adjoint> >(1.e-7),
    param(param_),
    intorderadd(intorderadd_),
    quadrature_factor(quadrature_factor_)
  { }

  /**
   * @brief Volume integral depending on test and ansatz functions
   */
  template<typename EG, typename LFSU, typename X, typename LFSV, typename R>
    void alpha_volume (const EG& eg, const LFSU& lfsu, const X& x, const LFSV& lfsv, R& r) const
    {
      // get polynomial degree
      const int order    = lfsu.finiteElement().localBasis().order();
      const int intorder = intorderadd + quadrature_factor * order;

      // get element geometry
      auto gt = eg.geometry();

      // bind parameterization and retrieve functions
      param->bind(eg.entity());
      const auto saturation_f = param->saturation_f();
      const auto water_content_f = param->water_content_f();

      // loop over quadrature points
      for (const auto& it : quadratureRule(gt,intorder))
      {
        // evaluate position in element local and global coordinates
        const auto p_local = it.position();

        // evaluate basis functions
        std::vector<Scalar> phi(lfsu.size());
        lfsu.finiteElement().localBasis().evaluateFunction(p_local, phi);

        // evaluate u
        RF u = 0.;
        for (unsigned int i = 0; i<lfsu.size(); i++)
          u += x(lfsu,i) * phi[i];

        // calculate water content from matric head
        const RF water_content = water_content_f(saturation_f(u));

        // integration factor
        const RF factor = it.weight() * gt.integrationElement(p_local);

        // update residual
        for (unsigned int i = 0; i<lfsv.size(); i++)
          r.accumulate(lfsv,i, water_content * phi[i] * factor);

      }
    }

  //! set operator time
  void setTime (RF t)
  {
    time = t;
  }


};

} // namespace Operator
} // namespace Dorie
} // namespace Dune


#endif // DUNE_DORIE_RICHARDS_OPERATOR_HH
