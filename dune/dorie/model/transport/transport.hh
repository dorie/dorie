#ifndef DUNE_DORIE_MODEL_TRANSPORT_HH
#define DUNE_DORIE_MODEL_TRANSPORT_HH

#include <memory>
#include <string>
#include <iostream>
#include <limits>
#include <algorithm>
#include <utility>

#include <dune/common/parametertree.hh>
#include <dune/common/parallel/mpihelper.hh>
#include <dune/common/float_cmp.hh>

#include <dune/pdelab/backend/istl.hh>
#include <dune/pdelab/gridoperator/gridoperator.hh>
#include <dune/pdelab/gridoperator/onestep.hh>
#include <dune/pdelab/stationary/linearproblem.hh>

#include <dune/dorie/common/typedefs.hh>
#include <dune/dorie/common/utility.hh>
#include <dune/dorie/common/gfs_helper.hh>
#include <dune/dorie/common/interpolator.hh>
#include <dune/dorie/common/time_step_controller.hh>
#include <dune/dorie/common/grid_creator.hh>
#include <dune/dorie/common/grid_function_writer.hh>
#include <dune/dorie/common/cfl_condition.hh>
#include <dune/dorie/common/flux_reconstruction/raviart_thomas.hh>
#include <dune/dorie/common/boundary_condition/manager.hh>
#include <dune/dorie/common/logging.hh>

#include <dune/dorie/model/base.hh>
#include <dune/dorie/model/transport/adapters/total_solute.hh>
#include <dune/dorie/model/transport/adapters/peclet.hh>
#include <dune/dorie/model/transport/adapters/effective_hydrodynamic_dispersion.hh>
#include <dune/dorie/model/transport/solute_parameters.hh>
#include <dune/dorie/model/transport/initial_condition.hh>
#include <dune/dorie/model/transport/local_operator_FV.hh>
#include <dune/dorie/model/transport/local_operator_DG.hh>

#ifdef GTEST
  #include <gtest/gtest.h>
#endif

namespace Dune{
namespace Dorie{

/*-------------------------------------------------------------------------*//**
 * @brief      Traits for the class ModelTransport.
 * @details    This class extends BaseTraits to be used for the
 *             implementation of the Transport model.
 * @author     Santiago Ospina De Los Ríos
 * @date       2018
 * @ingroup    TransportModel
 *
 * @todo       Implement higher order methods.
 *
 * @tparam     BaseTraits          Traits defining domain and range field
 *                                 properties of the model.
 * @tparam     GFWaterFluxType     Grid Function type of the water flux
 * @tparam     GFWaterContentType  Grid Function type of the water content
 * @tparam     k                   Order of the polynomial degree used for the
 *                                 basis functions in the DG method.
 *                                 Zero (0) enables the FV method.
 */
template<class BaseTraits,
         class GFWaterFluxType,
         class GFWaterContentType,
         unsigned int k>
struct ModelTransportTraits : public BaseTraits
{
  static constexpr int dim = BaseTraits::dim;
  static constexpr int order = k;
  static constexpr int flux_order = (order==0) ? 0 : (order-1);
  using RF = typename BaseTraits::RF;
  using Grid = typename BaseTraits::Grid;
  using GV = typename BaseTraits::GV;

  /// (Inestationary) Grid Function Type for water flux
  static_assert(GFWaterFluxType::Traits::dimRange == dim,
    "Water Flux Grid Function has wrong range");
  using GFWaterFlux = GFWaterFluxType;
  /// (Inestationary) Grid Function Type for water content
  static_assert(GFWaterContentType::Traits::dimRange == 1,
    "Water Content Grid Function has wrong range");
  using GFWaterContent = GFWaterContentType;

  /// GFS Helper
  using GFSHelper = std::conditional_t<
    order==0,
    Dune::Dorie::LinearSolverGridFunctionSpaceHelper<
      GV, RF, BaseTraits::GridGeometryType>,
    Dune::Dorie::GridFunctionSpaceHelper<
      GV, RF, order, BaseTraits::GridGeometryType>
  >;

  /// Problem GFS
  using GFS = typename GFSHelper::Type;
  /// GFS Constraints Container
  using CC = typename GFSHelper::CC;
  /// Finite Element Map Type
  using FEM = typename GFSHelper::FEM;

  /// LSGFS Helper
  using LSGFSHelper = Dune::Dorie::LinearSolverGridFunctionSpaceHelper<
    GV, RF, BaseTraits::GridGeometryType
  >;
  /// Linear solver GFS
  using LSGFS = typename LSGFSHelper::Type;
  /// LSGFS Constraints Container
  using LSCC = typename LSGFSHelper::CC;

  // -- DORiE Class Definitions -- //
  /// Wrapper for grid and volume & boundary mappings
  using GridCreator = Dune::Dorie::GridCreator<typename BaseTraits::Grid>;
  /// Class defining the solute parameterization
  using SoluteParameters = Dune::Dorie::SoluteParameters<BaseTraits>;
  /// Class defining boundary conditions
  using SoluteBoundary = Dune::Dorie::BoundaryConditionManager<
    BaseTraits, BCMMode::transport>;
  /// Class defining the initial condition
  using SoluteInitial = Dune::Dorie::InitialCondition<BaseTraits>;
  /// Class defining the initial condition factory
  using SoluteInitialFactory
    = Dune::Dorie::TransportInitialConditionFactory<BaseTraits>;

  /// Local spatial operator
  using SLOP = std::conditional_t<
    order==0,
    Dune::Dorie::Operator::TransportFVSpatialOperator<
      SoluteParameters, SoluteBoundary, GFWaterFlux, GFWaterContent>,
    Dune::Dorie::Operator::TransportDGSpatialOperator<
      FEM, SoluteParameters, SoluteBoundary, GFWaterFlux, GFWaterContent>
  >;

  /// Local temporal operator
  using TLOP = std::conditional_t<
    order==0,
    Dune::Dorie::Operator::TransportFVTemporalOperator<GFWaterContent>,
    Dune::Dorie::Operator::TransportDGTemporalOperator<FEM, GFWaterContent>
  >;

  /// Time controller
  using TimeStepController = Dune::Dorie::TimeStepController<double>;

  // -- DUNE Class Defintions -- //
  /// Matrix backend type
  using MBE = Dune::PDELab::ISTL::BCRSMatrixBackend<>;
  /// Grid operator for spatial LOP
  using GO0 = Dune::PDELab::GridOperator<GFS,GFS,SLOP,MBE,RF,RF,RF,CC,CC>;
  /// Grid operator for temporal LOP
  using GO1 = Dune::PDELab::GridOperator<GFS,GFS,TLOP,MBE,RF,RF,RF,CC,CC>;

  // -- setup -- //
  /// One step grid operator for merging both GOs
  using ImplicitIGO = Dune::PDELab::OneStepGridOperator<GO0, GO1, true>;
  using ExplicitIGO = Dune::PDELab::OneStepGridOperator<GO0, GO1, false>;
  /// Solution vector type
  using U = typename ImplicitIGO::Traits::Domain;
  /// Linear solver type
  using ImplicitLS = std::conditional_t<
    order == 0,
    Dune::PDELab::ISTLBackend_BCGS_AMG_SSOR<ImplicitIGO>,
    Dune::PDELab::ISTLBackend_OVLP_AMG_4_DG<ImplicitIGO,
                                            CC,
                                            LSGFS,
                                            LSCC,
                                            Dune::PDELab::CG2DGProlongation,
                                            Dune::SeqSSOR,
                                            Dune::BiCGSTABSolver>>;
  using ExplicitLS = std::conditional_t<
    order == 0,
    Dune::PDELab::ISTLBackend_BCGS_AMG_SSOR<ExplicitIGO>,
    Dune::PDELab::ISTLBackend_OVLP_AMG_4_DG<ExplicitIGO,
                                            CC,
                                            LSGFS,
                                            LSCC,
                                            Dune::PDELab::CG2DGProlongation,
                                            Dune::SeqSSOR,
                                            Dune::BiCGSTABSolver>>;
  /// Methods computing the time step
  using ImplicitSLPS
    = Dune::PDELab::StationaryLinearProblemSolver<ImplicitIGO, ImplicitLS, U>;
  using ImplicitOSM
    = Dune::PDELab::OneStepMethod<RF, ImplicitIGO, ImplicitSLPS, U, U>;
  using ExplicitOSM
    = Dune::PDELab::ExplicitOneStepMethod<RF, ExplicitIGO, ExplicitLS, U, U>;

  /// Model state provider
  struct ConstState 
  {
    using GridFunctionSpace = const GFS;
    using Coefficients = const U;
    using TimeField = const typename BaseTraits::TimeField;

    std::shared_ptr<GridFunctionSpace> grid_function_space;
    std::shared_ptr<Coefficients> coefficients;
    TimeField time;
  };

  /// Model state provider
  struct State 
  {
    using GridFunctionSpace = GFS;
    using Coefficients = U;
    using TimeField = typename BaseTraits::TimeField;

    std::shared_ptr<GridFunctionSpace> grid_function_space;
    std::shared_ptr<Coefficients> coefficients;
    TimeField time;

    // Implicit conversion to constant state
    operator ConstState() {
      return ConstState{grid_function_space,coefficients,time};
    }
  };

  // -- Grid Functions of state valriables -- //
  /// Solute
  using GFSolute = Dune::PDELab::DiscreteGridFunction<GFS,U>;
  /// TotalSolute
  using GFTotalSolute = Dune::Dorie::TotalSoluteAdapter<GFSolute,GFWaterContent>;
  /// Water Flux reconstruction
  static constexpr bool enable_rt_engine = Dune::Dorie::RaviartThomasFluxReconstructionHelper<dim,flux_order,BaseTraits::GridGeometryType>::enable;
  using GFFluxReconstruction = std::conditional_t<enable_rt_engine,Dune::Dorie::RaviartThomasFluxReconstruction<GV,RF,flux_order,BaseTraits::GridGeometryType>,void>;
  /// Peclet
  using GFPeclet = Dune::Dorie::PecletAdapter<BaseTraits,SoluteParameters,GFWaterFlux,GFWaterContent>;
  /// Effective Hydrodynamic Dipspersion
  using GFEffectiveHydrodynamicDispersion = Dune::Dorie::EffectiveHydrodynamicDispersionAdapter<BaseTraits,SoluteParameters,GFWaterFlux,GFWaterContent>;

  // -- Utility Class Definitions -- //
  /// VTK Output writer base class
  using Writer = Dune::Dorie::GridFunctionVTKSequenceWriter<GV>;

  /// Policy when checking for negative values in the solution
  enum class CheckNegativePolicy
  {
    Pass, //!< Always pass the check
    Warn, //!< Pass the check but issue warnings for negative values
    Retry //!< Reject the solution for negative values
  };
};


/*-------------------------------------------------------------------------*//**
 * @brief      Class for Transport model. 
 * @details    For given ModelTransportTraits, this class manages the 
 *             model for the transport equation.
 * @author     Santiago Ospina De Los Ríos
 * @date       2018
 * @ingroup    TransportModel
 * @ingroup    Collective
 *
 * @todo  Implement source term.
 * @todo  Implement more complex initial conditions.
 * @todo  Implement adaptivity.
 * @todo  Implement DG local operator.
 * 
 * @tparam     ModelTransportTraits  Traits containing the type definitions
 *                                        which this class will use.
 */
template<typename ModelTransportTraits>
class ModelTransport : public ModelBase
{
public:
  using Traits = ModelTransportTraits;

protected:

  static constexpr int dim = Traits::dim;
  static constexpr int order = Traits::order;
  static constexpr int flux_order = Traits::flux_order;
  static constexpr bool enable_rt_engine = Traits::enable_rt_engine;

  using TimeField = typename Traits::TimeField;
  using RF = typename Traits::RF;
  using Grid = typename Traits::Grid;
  using GV = typename Traits::GV;

  /// (Inestationary) Grid Function Type for water flux
  using GFWaterFlux = typename Traits::GFWaterFlux;
  /// (Inestationary) Grid Function Type for water content
  using GFWaterContent = typename Traits::GFWaterContent;

  /// GFS Helper
  using GFSHelper = typename Traits::GFSHelper;
  /// Problem GFS
  using GFS = typename Traits::GFS;
  /// GFS Constraints Container
  using CC = typename Traits::CC;

  /// LSGFS Helper
  using LSGFSHelper = typename Traits::LSGFSHelper;
  /// Linear solver GFS
  using LSGFS = typename Traits::LSGFS;
  /// LSGFS Constraints Container
  using LSCC = typename Traits::LSCC;

  // -- D/*ORiE Class Definitions -- //
  /// Wrapper for grid and volume & boundary mappings
  using GridCreator = typename Traits::GridCreator;
  /// Class defining the solute parameterization
  using SoluteParameters = typename Traits::SoluteParameters;
  /// Class defining boundary conditions
  using SoluteBoundary = typename Traits::SoluteBoundary;
  /// Class defining the initial condition
  using SoluteInitial = typename Traits::SoluteInitial;
  /// Class defining the initial condition factory
  using SoluteInitialFactory = typename Traits::SoluteInitialFactory;
  /// Local spatial operator
  using SLOP = typename Traits::SLOP;
  /// Local temporal operator
  using TLOP = typename Traits::TLOP;
  /// Time controller
  using TimeStepController = typename Traits::TimeStepController;

  // -- DUNE Class Defintions -- //
  /// Matrix backend type
  using MBE = typename Traits::MBE;
  /// Grid operator for spatial LOP
  using GO0 = typename Traits::GO0;
  /// Grid operator for temporal LOP
  using GO1 = typename Traits::GO1;
  /// One step grid operator for merging both GOs
  using ImplicitIGO = typename Traits::ImplicitIGO;
  using ExplicitIGO = typename Traits::ExplicitIGO;
  /// Solution vector type
  using U = typename Traits::U;
  /// Linear solver type
  using ImplicitLS = typename Traits::ImplicitLS;
  using ExplicitLS = typename Traits::ExplicitLS;
  /// Methods computing the time step
  using ImplicitSLPS = typename Traits::ImplicitSLPS;
  using ImplicitOSM = typename Traits::ImplicitOSM;
  using ExplicitOSM = typename Traits::ExplicitOSM;

  /// Model state provider
  using State = typename Traits::State;
  using ConstState = typename Traits::ConstState;

  // -- Grid Functions of state variables -- //
  /// Solute concentration
  using GFSolute = typename Traits::GFSolute;
  /// Total solute
  using GFTotalSolute = typename Traits::GFTotalSolute;
  /// Water Flux reconstruction
  using GFFluxReconstruction = typename Traits::GFFluxReconstruction;
  /// Peclet
  using GFPeclet = typename Traits::GFPeclet;
  /// Effective Hydrodynamic Dipspersion
  using GFEffectiveHydrodynamicDispersion = typename Traits::GFEffectiveHydrodynamicDispersion;

  // // -- Utility Class Definitions -- //
  /// VTK Output writer base class
  using Writer = Dune::Dorie::GridFunctionVTKSequenceWriter<GV>;

  /// Time stepping scheme
  using TimeSteppingParameter = Dune::PDELab::TimeSteppingParameterInterface<RF>;

  /// Policy for negative values in solution
  using CheckNegativePolicy = typename Traits::CheckNegativePolicy;


  // -- Member variables -- //
  Dune::MPIHelper& helper;
  Dune::ParameterTree& inifile;

  /// Time step controller of this instance
  TimeStepController time_step_ctrl;

  const Dune::VTK::OutputType output_type;

  std::shared_ptr<Grid> grid;
  GV gv;
  std::shared_ptr<GFS> gfs;
  std::unique_ptr<CC> cc;

  std::shared_ptr<SoluteParameters> sparam;
  std::shared_ptr<SoluteBoundary> sboundary;
  std::shared_ptr<SoluteInitial> sinitial;

  std::shared_ptr<LSGFS> lsgfs;
  std::shared_ptr<LSCC> lscc;

  std::unique_ptr<SLOP> slop;
  MBE mbe_slop;
  std::unique_ptr<TLOP> tlop;
  MBE mbe_tlop;
  std::shared_ptr<GO0> go0;
  std::unique_ptr<GO1> go1;
  std::unique_ptr<ImplicitIGO> implicit_igo;
  std::unique_ptr<ExplicitIGO> explicit_igo;
  std::unique_ptr<ImplicitLS>  implicit_ls;
  std::unique_ptr<ExplicitLS>  explicit_ls;
  std::unique_ptr<ImplicitSLPS> implicit_slps;
  std::unique_ptr<ImplicitOSM> implicit_osm;
  std::unique_ptr<ExplicitOSM> explicit_osm;
  std::unique_ptr<TimeSteppingParameter> ts_param;

  std::shared_ptr<U> u;

  std::shared_ptr<GFWaterFlux>            water_flux_gf;
  std::shared_ptr<GFWaterContent>         water_content_gf;

  std::unique_ptr<Writer> vtkwriter;

  mutable std::shared_ptr<GFFluxReconstruction> cache_solute_flux_gf_rt;

  unsigned int time_steps;

  const RF _courant_number;

  const bool enable_fluxrc;

  /// Stored policy for negative values in solution
  CheckNegativePolicy check_negative_policy;

public:

  /*-----------------------------------------------------------------------*//**
   * @brief      Construct model.
   *
   * @param      _inifile           Dune parameter file parser.
   * @param      _grid_creator      Mapper for grid and volume/boundary data.
   * @param      _helper            Dune MPI instance controller.
   */
  ModelTransport (
    Dune::ParameterTree&            _inifile,
    const GridCreator&              _grid_creator,
    Dune::MPIHelper&                _helper
  );

  ~ModelTransport() = default;

  /*------------------------------------------------------------------------*//**
   * @brief      Compute a time step. Models the time step requirement of
   *             ModelBase
   *
   * @throws     Dune::Exception  Fatal error occurs during computation.
   */
  void step () override;

  /*------------------------------------------------------------------------*//**
   * @brief      Mark the grid in order to improve the current model.
   */
  void mark_grid() override;

  /*------------------------------------------------------------------------*//**
   * @brief      Adapt the grid and the solutions of the transport simulation
   * @warning    This method does not adapt solutions of other simulations!
   */
  void adapt_grid() override;

  /*------------------------------------------------------------------------*//**
   * @brief      Setup operators to fit the new grid.
   */
  void post_adapt_grid() override;

  /*-----------------------------------------------------------------------*//**
   * @brief      Method that provides the begin time of the model.
   *
   * @return     Begin time of the model.
   */
  double begin_time() const final {return time_step_ctrl.get_time_begin();}

  /*-----------------------------------------------------------------------*//**
   * @brief      Method that provides the end time of the model.
   *
   * @return     End time of the model.
   */
  double end_time() const final {return time_step_ctrl.get_time_end();}

  /*------------------------------------------------------------------------*//**
   * @brief      Provides an object with all the information that describes
   *             the current state of the solved variable.
   *
   * @return     Current state of the model.
   */
  State current_state() {
    return State{gfs,u,current_time()};
  }

  /*------------------------------------------------------------------------*//**
   * @brief      Provides an object with all the information that describes
   *             the current state of the solved variable.
   *
   * @return     (Const) Current state of the model.
   */
  ConstState current_state() const {
    return ConstState{gfs,u,current_time()};
  }

  /*-----------------------------------------------------------------------*//**
   * @brief      Suggest a time step to the model.
   *
   * @param[in]  dt    Suggestion for the internal time step of the model. The
   *                   new internal time step shall not be bigger than dt.
   */
  void suggest_timestep(double dt) final
  {
    time_step_ctrl.suggest_time_step(dt);
  }

  /*------------------------------------------------------------------------*//**
   * @brief      Write the current data using the VTKWriter. VTKWriters is 
   *             always cleared after used.
   */
  virtual void write_data () const override;

  /*-----------------------------------------------------------------------*//**
   * @brief      Sets an instationary function for the water flux.
   *
   * @param[in]  _water_flux_gf  The grid function water flux.
   */
  void set_water_flux(std::shared_ptr<GFWaterFlux> _water_flux_gf)
  {
    if (_water_flux_gf)
      water_flux_gf = _water_flux_gf;
    else
      DUNE_THROW(Dune::InvalidStateException,
        "Pointer to water_flux_gf is invalid!");

    slop->set_water_flux(_water_flux_gf);
  }

  /*-----------------------------------------------------------------------*//**
   * @brief      Sets an instationary function for the water content.
   *
   * @param[in]  _water_content_gf  The grid function water content.
   */
  void set_water_content(std::shared_ptr<GFWaterContent> _water_content_gf)
  {
    if (_water_content_gf)
      water_content_gf = _water_content_gf;
    else
      DUNE_THROW(Dune::InvalidStateException,
        "Pointer to water_content_gf is invalid!");

    slop->set_water_content(_water_content_gf);
    tlop->set_water_content(_water_content_gf);
  }

  /*------------------------------------------------------------------------*//**
   * @brief      Generates a solute grid function from a given state
   *
   * @return     Pointer to a solute grid function
   */
  std::shared_ptr<const GFSolute> get_solute(ConstState state) const
  {
    return std::make_shared<GFSolute>(state.grid_function_space,state.coefficients);
  }

  /*------------------------------------------------------------------------*//**
   * @brief      Gets the solute grid function
   *
   * @return     Pointer to a solute grid function
   */
  std::shared_ptr<const GFSolute> get_solute() const
  {
    return get_solute(current_state());
  }

  /*------------------------------------------------------------------------*//**
   * @brief      Generates a total solute grid function from a given state
   *
   * @return     Pointer to a total solute grid function
   */
  std::shared_ptr<const GFTotalSolute> get_total_solute(ConstState state) const
  {
    auto solute_gf = get_solute(state);

    auto _water_content_gf 
      = std::make_shared<GFWaterContent>(*water_content_gf);
    _water_content_gf->setTime(state.time);
    _water_content_gf->shrink_to_time();
    
    return std::make_shared<GFTotalSolute>(solute_gf, _water_content_gf);
  }

  /*------------------------------------------------------------------------*//**
   * @brief      Gets the total solute grid function
   *
   * @return     Pointer to a total solute grid function
   */
  std::shared_ptr<const GFTotalSolute> get_total_solute() const
  {
    return get_total_solute(current_state());
  }

  /*-----------------------------------------------------------------------*//**
   * @brief      Generates a (reconstructed) solute flux grid function from a
   *             given state
   *
   * @param[in]  state  The state
   *
   * @return     Pointer to a solute grid function
   */
  template<bool enabled=enable_rt_engine>
	std::enable_if_t<enabled, std::shared_ptr<const GFFluxReconstruction>>
  get_solute_flux_reconstructed(ConstState state) const
  {
    std::shared_ptr<GFFluxReconstruction> gf_ptr;

    auto& cache = cache_solute_flux_gf_rt;

    if (state.grid_function_space != gfs
        or state.coefficients != u
        or state.time != current_time())
    {
      // if state is different to current state, create flux from zero

      gf_ptr = std::make_unique<GFFluxReconstruction>(
                      this->_log,gv,inifile.sub("fluxReconstruction"));

      slop->setTime(state.time);

      // update it with the state
      gf_ptr->update(*(state.coefficients),
                      *(state.grid_function_space),
                      *slop);

      slop->setTime(current_time());
    } else if (not cache) {
      // if state is equal to current state, use cache.

      cache = std::make_unique<GFFluxReconstruction>(
                      this->_log,gv,inifile.sub("fluxReconstruction"));

      slop->setTime(state.time);

      // update it with current state
      cache->update(*u,*gfs,*slop);

      slop->setTime(current_time());

      gf_ptr = cache;
    } else {
      gf_ptr = cache;
    }

    return gf_ptr;
  }

  /*------------------------------------------------------------------------*//**
   * @brief      Gets the (reconstructed) solute flux grid function
   *
   * @return     Pointer to a solute grid function
   */
  template<bool enabled=enable_rt_engine>
	std::enable_if_t<enabled, std::shared_ptr<const GFFluxReconstruction>>
  get_solute_flux_reconstructed() const
  {
    return get_solute_flux_reconstructed(current_state());
  }
protected:

  /*------------------------------------------------------------------------*//**
   * @brief      Create the Grid Operators, Solvers, and Time Step Operator.
   *             This function must be called after changes to grid or GFS!
   */
  void operator_setup ();

  /// Check the solution of a time step
  /** This function currently performs the following checks:
   *
   *  * No negative values in the resulting solution \p u_after
   *
   *  The method returns a pair of `bool` and `string`. The boolean indicates
   *  if the check was successful, and the string contains a check error
   *  message if it was not.
   *
   *  \param u_before Solution at the start of the time step
   *  \param u_after Computed solution at the end of the time step
   *  \return Pair of `bool` (check passed) and `string` (check fail
   *          message, if any)
   */
  std::pair<bool, std::string> check_solution (const U& u_before,
                                               const U& u_after) const;

private:

#ifdef GTEST
  FRIEND_TEST(ModelCoupling, TimeSteps);
#endif

};

} // namespace Dorie
} // namespace Dune

#endif // DUNE_DORIE_MODEL_TRANSPORT_HH
