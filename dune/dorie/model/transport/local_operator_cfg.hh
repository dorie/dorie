#ifndef DUNE_DORIE_TRANSPORT_LOP_CFG_HH
#define DUNE_DORIE_TRANSPORT_LOP_CFG_HH

#include <dune/dorie/model/local_operator_cfg.hh>

namespace Dune {
namespace Dorie {
namespace Operator {

/**
 *  \addtogroup TransportModel
 *  \{
 */

//! Transport Upwinding type
using TransportUpwinding = Upwinding;

//! Transport DG method type
using TransportDGMethod = DGMethod;

//! Transport DG weights type
using TransportDGWeights = DGWeights;

/// Read operator settings from a parameter file
/* \return Tuple of TransportUpwinding, TransportDGMethod, and
 *    TransportDGWeights to be inserted into the local operator constructor.
 */
inline auto
read_transport_operator_settings(const Dune::ParameterTree& inifile)
  -> std::tuple<Operator::TransportUpwinding,
                Operator::TransportDGMethod,
                Operator::TransportDGWeights>
{
  const auto log = Dorie::get_logger(log_transport);
  log->debug("Reading local operator settings:");

  TransportUpwinding upwinding;
  TransportDGMethod method = TransportDGMethod::SIPG;
  TransportDGWeights weights = TransportDGWeights::weightsOn;

  // Upwinding
  upwinding = read_operator_upwinding_settings(inifile, log);

  // Return here if the local operator is FV only
  if (inifile.get<int>("numerics.FEorder") == 0) {
    log->debug("  Ignoring settings 'numerics.DGMethod' and  "
               "'numerics.DGWeights' for finite volume solver.");
    return std::make_tuple(upwinding, method, weights);
  }

  // DG Method
  method = read_operator_dg_method_settings(inifile, log);
  weights = read_operator_dg_weights_settings(inifile, log);

  return std::make_tuple(upwinding, method, weights);
}

/**
 *  \}
 *  // group TransportModel
 */

} // namespace Operator
} // namespace Dorie
} // namespace Dune

#endif // DUNE_DORIE_TRANSPORT_LOP_CFG_HH
