#ifndef DUNE_DORIE_TRANSPORT_INITIAL_CONDITION
#define DUNE_DORIE_TRANSPORT_INITIAL_CONDITION

#include <memory>
#include <string>

#include <dune/common/exceptions.hh>

#include <dune/dorie/common/initial_condition/factory.hh>

namespace Dune {
namespace Dorie {

/// An initial condition factory for the transport solver
/**
 *  \tparam T Traits
 *  \ingroup InitialConditions
 *  \author Lukas Riedel
 *  \date 2019
 */
template<typename T>
class TransportInitialConditionFactory : public InitialConditionFactory<T>
{
private:
    using Base = InitialConditionFactory<T>;

public:
    /// Create an initial condition for the Richards solver
    static std::unique_ptr<InitialCondition<T>> create (
        const Dune::ParameterTree& config,
        const typename T::GridView& grid_view,
        const std::shared_ptr<spdlog::logger> log=get_logger(log_transport)
    )
    {
        const auto quantity = config.get<std::string>("initial.quantity");
        // solute concentration: no transformation necessary
        if (quantity == "soluteConcentration") {
            return Base::create(config, grid_view, log);
        }
        else {
            log->error("Unsupported quantity for initial condition: {}",
                       quantity);
            DUNE_THROW(NotImplemented,
                       "Unsupported initial condition quantity");
        }
    }
};

} // namespace Dorie
} // namespace Dune

#endif // DUNE_DORIE_TRANSPORT_INITIAL_CONDITION
