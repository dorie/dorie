add_subdirectory("adapters")
add_subdirectory("parameterization")

#install headers
install(FILES
    initial_condition.hh
    local_operator_cfg.hh
    local_operator_DG.hh
    local_operator_FV.hh
    solute_parameters.hh
    transport.cc
    transport.hh
    DESTINATION "${CMAKE_INSTALL_INCLUDEDIR}/dune/dorie/model/transport")
