#ifndef DUNE_DORIE_PECLET_ADAPTER_HH
#define DUNE_DORIE_PECLET_ADAPTER_HH

#include <dune/pdelab/common/function.hh>

namespace Dune{
namespace Dorie{

/**
 * @brief      Converts an interface to match a microscopic peclet number.
 * @ingroup    TransportParam
 * 
 * @tparam     T               The base traits
 * @tparam     Parameter       The transport parameter class
 * @tparam     GFWaterFlux     The water flux grid function class
 * @tparam     GFWaterContent  The water content grid function class
 */
template<class T, class Parameter, class GFWaterFlux, class GFWaterContent>
class PecletAdapter
  : public Dune::PDELab::GridFunctionBase<
                  Dune::PDELab::GridFunctionTraits<
                    typename T::GridView, 
                    typename T::RangeField,
                    1, 
                    typename T::Scalar
                  >
                  ,PecletAdapter<T,Parameter,GFWaterFlux,GFWaterContent> 
                >
{
public:
  using Traits = Dune::PDELab::GridFunctionTraits<
                      typename T::GridView,
                      typename T::RangeField,
                      1, 
                      typename T::Scalar
                    >;

private:
  using RF     = typename T::RangeField;
  using Domain = typename T::Domain;

public:
  /*-----------------------------------------------------------------------*//**
   * @brief      Constructor for the PecletAdapter
   *
   * @param      gv                GridView
   * @param[in]  param             The parameter
   * @param[in]  gf_water_flux     The gf water flux
   * @param[in]  gf_water_content  The gf water content
   * @param      p     Parametrization class
   */
  PecletAdapter(const typename Traits::GridViewType& gv,
                                 std::shared_ptr<const Parameter> param,
                                 std::shared_ptr<const GFWaterFlux> gf_water_flux,
                                 std::shared_ptr<const GFWaterContent> gf_water_content)
    : _gv(gv)
    , _param(param)
    , _gf_water_flux(gf_water_flux)
    , _gf_water_content(gf_water_content)
  {}

  /*-----------------------------------------------------------------------*//**
   * @brief      Evaluation of the microscopic peclet number for a given entity
   *             e in an entity position x
   *
   * @param[in]  e     Entity of a grid
   * @param[in]  x     Position in local coordinates with respect the entity
   * @param      y     Microscopic peclet number at position x
   */
  void inline evaluate ( const typename Traits::ElementType& e,
                  const typename Traits::DomainType& x,
                        typename Traits::RangeType& y) const
  {
    typename GFWaterFlux::Traits::RangeType water_flux;
    typename GFWaterContent::Traits::RangeType water_content;

    // evaluate water flux
    _gf_water_flux->evaluate(e,x,water_flux);
    // evaluate water flux
    _gf_water_content->evaluate(e,x,water_content);
    // bind to entity
    _param->bind(e);
    // evaluate peclet from parameterization
    y = _param->peclet_f()(water_flux,water_content);
  }

  /*-------------------------------------------------------------------*//**
   * @brief      Function that returns a grid view valid for this grid
   *             function
   *
   * @return     Grid view
   */
  const typename Traits::GridViewType& getGridView() const
  {
    return _gv;
  }
  
private:
  const typename Traits::GridViewType _gv;
  const std::shared_ptr<const Parameter> _param;
  const std::shared_ptr<const GFWaterFlux> _gf_water_flux;
  const std::shared_ptr<const GFWaterContent> _gf_water_content;
};

} // namespace Dune
} // namespace Dorie

#endif // DUNE_DORIE_PECLET_ADAPTER_HH