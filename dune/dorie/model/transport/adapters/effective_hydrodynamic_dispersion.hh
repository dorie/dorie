#ifndef DUNE_DORIE_HYDRODYNAMIC_DISPERSION_ADAPTER_HH
#define DUNE_DORIE_HYDRODYNAMIC_DISPERSION_ADAPTER_HH

#include <dune/pdelab/common/function.hh>

namespace Dune{
namespace Dorie{

/*-------------------------------------------------------------------------*//**
 * @brief      Converts an interface to match an effective hydrodynamic dispersion.
 * @details    For a given paramaterization, water flux and water content, this
 *             class returns the hydrodynamic dispersion tensor (od dimension
 *             dimRange) in an aliased vector.
 * @ingroup    TransportParam
 * 
 * @tparam     T               The base traits
 * @tparam     Parameter       The transport parameter class
 * @tparam     GFWaterFlux     The water flux grid function class
 * @tparam     GFWaterContent  The water content grid function class
 * @tparam     dimRange        The range dimension of the aliased tensor
 */
template<class T, class Parameter, class GFWaterFlux, class GFWaterContent, int dimRange = 3>
class EffectiveHydrodynamicDispersionAdapter
  : public Dune::PDELab::GridFunctionBase<
                  Dune::PDELab::GridFunctionTraits<
                    typename T::GridView, 
                    typename T::RangeField,
                    dimRange*dimRange, 
                    Dune::FieldVector<typename T::RangeField,dimRange*dimRange>
                  >
                  ,EffectiveHydrodynamicDispersionAdapter<T,Parameter,GFWaterFlux,GFWaterContent,dimRange> 
                >
{
public:
  using Traits = Dune::PDELab::GridFunctionTraits<
                      typename T::GridView,
                      typename T::RangeField,
                      dimRange*dimRange, 
                      Dune::FieldVector<typename T::RangeField,dimRange*dimRange>
                    >;

private:
  using RF     = typename T::RangeField;
  using Domain = typename T::Domain;

public:
  /*-------------------------------------------------------------------*//**
   * @brief      Constructor for the EffectiveHydrodynamicDispersionAdapter
   *
   * @param      gv    GridView
   * @param      p     Parametrization class
   * @see        RichardsEquationParameter
   */
  EffectiveHydrodynamicDispersionAdapter(const typename Traits::GridViewType& gv,
                                 std::shared_ptr<const Parameter> param,
                                 std::shared_ptr<const GFWaterFlux> gf_water_flux,
                                 std::shared_ptr<const GFWaterContent> gf_water_content)
    : _gv(gv)
    , _param(param)
    , _gf_water_flux(gf_water_flux)
    , _gf_water_content(gf_water_content)
  {}

  /*-----------------------------------------------------------------------*//**
   * @brief      Evaluation of the effective hydrodynamic dispersion for a given
   *             entity e in an entity position x
   *
   * @param[in]  e     Entity of a grid
   * @param[in]  x     Position in local coordinates with respect the entity
   * @param      y     Effective hydrodynamic dispersion at position x
   */
  void evaluate ( const typename Traits::ElementType& e,
                  const typename Traits::DomainType& x,
                        typename Traits::RangeType& y) const
  {
    typename GFWaterContent::Traits::RangeType water_content;
    typename GFWaterFlux::Traits::RangeType water_flux;

    // evaluate water flux
    _gf_water_flux->evaluate(e,x,water_flux);
    // evaluate water flux
    _gf_water_content->evaluate(e,x,water_content);
    // bind to entity
    _param->bind(e);
    // evaluate hydrodynamic dispersion
    auto y_tensor = _param->hydrodynamic_dispersion_f()(water_flux,water_content);

    using Range = typename Traits::RangeType;
    using TensorRange = decltype(y_tensor);

    int count = 0;
    y = Range(0.);

    for (int i = 0; i <  dimRange; ++i)
      for (int j = 0; j < dimRange; ++j, ++count)
        if (i<TensorRange::rows and j<TensorRange::rows)
          y[count] = y_tensor[i][j];
  }

  /*-------------------------------------------------------------------*//**
   * @brief      Function that returns a grid view valid for this grid
   *             function
   *
   * @return     Grid view
   */
  const typename Traits::GridViewType& getGridView() const
  {
    return _gv;
  }
  
private:
  const typename Traits::GridViewType _gv;
  const std::shared_ptr<const Parameter> _param;
  const std::shared_ptr<const GFWaterFlux> _gf_water_flux;
  const std::shared_ptr<const GFWaterContent> _gf_water_content;
};

} // namespace Dune
} // namespace Dorie

#endif // DUNE_DORIE_HYDRODYNAMIC_DISPERSION_ADAPTER_HH