#ifndef DUNE_DORIE_TOTAL_SOLUTE_ADAPTER_HH
#define DUNE_DORIE_TOTAL_SOLUTE_ADAPTER_HH

#include <dune/pdelab/common/function.hh>

namespace Dune{
  namespace Dorie{

    /*---------------------------------------------------------------------*//**
     * @brief      Adapter to obtain total solute
     *
     * @tparam     GFSolute        Solute concentration grid function 
     * @tparam     GFWaterContent  Water content grid function
     */
    template<class GFSolute, class GFWaterContent>
    class TotalSoluteAdapter
      : public Dune::PDELab::GridFunctionBase<
                      typename GFSolute::Traits,
                      TotalSoluteAdapter<GFSolute,GFWaterContent> 
                    >
    {
      static_assert(std::is_same_v<typename GFSolute::Traits,
                                   typename GFWaterContent::Traits>,
                    "GFSolute and GFWaterContent must have the same traits!");
    public:
      using Traits = typename GFSolute::Traits;

    public:
      /*-------------------------------------------------------------------*//**
       * @brief      Constructor for the TotalSoluteAdapter
       *
       * @param[in]  solute_gf         The solute grid function
       * @param[in]  water_content_gf  The water content grid function
       */
      TotalSoluteAdapter(std::shared_ptr<const GFSolute> solute_gf,
                        std::shared_ptr<const GFWaterContent> water_content_gf)
        : _solute_gf(solute_gf)
        , _water_content_gf(water_content_gf)
      {}

      /*-------------------------------------------------------------------*//**
       * @brief      Evaluation of the total solute for a given entity e in an
       *             entity position x
       *
       * @param[in]  e     Entity of a grid
       * @param[in]  x     Position in local coordinates with respect the entity
       * @param      y     Total solute at position x
       */
      void evaluate ( const typename Traits::ElementType& e,
                      const typename Traits::DomainType& x,
                            typename Traits::RangeType& y) const
      {
        typename Traits::RangeType solute;
        typename Traits::RangeType water_content;
        _solute_gf->evaluate(e, x, solute);
        _water_content_gf->evaluate(e, x, water_content);
        y = solute*water_content;
      }

      /*-------------------------------------------------------------------*//**
       * @brief      Function that returns a grid view valid for this grid
       *             function
       *
       * @return     Grid view
       */
      const typename Traits::GridViewType& getGridView() const
      {
        return _solute_gf->getGridView();
      }

    private:
      const std::shared_ptr<const GFSolute>       _solute_gf;
      const std::shared_ptr<const GFWaterContent> _water_content_gf;
    };

  }
}

#endif
