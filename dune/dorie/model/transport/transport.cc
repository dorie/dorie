#include <dune/dorie/model/transport/transport.hh>

namespace Dune{
namespace Dorie{

template<typename Traits>
ModelTransport<Traits>::ModelTransport(
  Dune::ParameterTree&              _inifile,
  const GridCreator&                _grid_creator,
  Dune::MPIHelper&                  _helper
) : ModelBase(log_transport,
              _inifile.get<std::string>("output.logLevel"),
              _helper)
  , helper(_helper)
  , inifile(_inifile)
  , time_step_ctrl(inifile, this->_log)
  , output_type(_inifile.get<bool>("output.asciiVtk") ? 
      Dune::VTK::OutputType::ascii : Dune::VTK::OutputType::base64)
  , grid(_grid_creator.grid())
  , gv(grid->leafGridView())
  , mbe_slop(estimate_mbe_entries<typename MBE::size_type>(Traits::dim,Traits::GridGeometryType))
  , mbe_tlop(1)
  , time_steps(0)
  , _courant_number(inifile.get<RF>("numerics.courant"))
  , enable_fluxrc(_inifile.get<bool>("fluxReconstruction.enable"))
{
  // initialize time
  this->_time = time_step_ctrl.get_time_begin();

  // --- Output Policy ---
  std::string output_policy_str 
      = inifile.get<std::string>("output.policy");

  if (output_policy_str == "endOfRichardsStep") {
    this->set_policy(OutputPolicy::EndOfRichardsStep);
  } else if (output_policy_str == "endOfTransportStep") {
    this->set_policy(OutputPolicy::EndOfTransportStep);
  } else if (output_policy_str == "none") {
    this->set_policy(OutputPolicy::None);
  } else {
    this->_log->error("Invalid output policy '{}'", output_policy_str);
    DUNE_THROW(NotImplemented,"Invalid output policy: " << output_policy_str);
  }

  // --- Solution Check Policy --- //
  const auto check_negative_policy_str
    = inifile.get<std::string>("solutionCheck.negativeConcentration");
  if (check_negative_policy_str == "pass") {
    check_negative_policy = CheckNegativePolicy::Pass;
  }
  else if (check_negative_policy_str == "warn") {
    check_negative_policy = CheckNegativePolicy::Warn;
  }
  else if (check_negative_policy_str == "retry") {
    check_negative_policy = CheckNegativePolicy::Retry;
  }
  else {
    this->_log->error("Invalid policy for negative solute concentrations: {}",
                      check_negative_policy_str);
    DUNE_THROW(IOError, "Invalid policy for negative concentrations");
  }

  // initialize time
  this->_time = time_step_ctrl.get_time_begin();

  // --- Grid Function Space ---
  this->_log->trace("Setting up GridFunctionSpace");
  gfs = std::make_shared<GFS>(GFSHelper::create(gv));
  gfs->name("solute");
  cc = std::make_unique<CC>();
  Dune::PDELab::constraints(*gfs,*cc,false);

  // --- Create the new parameter class
  auto element_map = _grid_creator.element_index_map();
  sparam = std::make_shared<SoluteParameters>(inifile, grid, element_map);

  // --- Operator Helper Classes ---
  const auto& boundary_index_map = _grid_creator.boundary_index_map();
  sboundary = std::make_shared<SoluteBoundary>(inifile,
                                               boundary_index_map,
                                               this->_log);
  sinitial = SoluteInitialFactory::create(inifile, gv, this->_log);

	// Read local operator settings
	namespace OP = Dune::Dorie::Operator;
	const auto settings = OP::read_transport_operator_settings(inifile);

  // --- Local Operators ---  
  if constexpr (order>0)
  {
    this->_log->debug("Setting up local grid operators: DG method");

    namespace OP = Dune::Dorie::Operator;
    const auto method = std::get<OP::TransportDGMethod>(settings);
    const auto upwinding = std::get<OP::TransportUpwinding>(settings);
    const auto weights = std::get<OP::TransportDGWeights>(settings);

    slop = std::make_unique<SLOP>(inifile, sparam, sboundary, 
                                  method, upwinding, 
                                  weights);
  }
  else {
    this->_log->debug("Setting up local grid operators: FV method");
    slop = std::make_unique<SLOP>(sparam, sboundary/*, upwinding*/);
  }

  tlop = std::make_unique<TLOP>();

  // --- Solution Vectors and Initial Condition ---
  const RF ini_value = inifile.get<RF>("initial.value",0.);
  u = std::make_shared<U>(*gfs,ini_value);
  
  Dune::PDELab::interpolate(*sinitial,*gfs,*u);

  // Select time stepping method
  std::string ts_method = _inifile.get<std::string>("numerics.timestepMethod");
  if (ts_method== "explicit_euler")
    ts_param = std::make_unique<Dune::PDELab::ExplicitEulerParameter<RF>>();
  else if (ts_method== "implicit_euler")
    ts_param = std::make_unique<Dune::PDELab::ImplicitEulerParameter<RF>>();
  else if (ts_method== "alex2")
    ts_param = std::make_unique<Dune::PDELab::Alexander2Parameter<RF>>();
  else
    DUNE_THROW(Dune::NotImplemented,"Time method not supported!");

  if (not ts_param->implicit())
  {
    if (FloatCmp::le(_courant_number, 0.0)) {
      this->_log->error("Courant number must be larger than zero");
      DUNE_THROW(IOError, "Invalid courant number");
    }
    else if (FloatCmp::gt(_courant_number, 1.0)) {
      this->_log->warn("Courant number larger than 1.0! Solver might not "
                       "converge, leading to instabilities in the solution.");
    }
  }

  // --- Writer Setup --- //
  if (output_policy() != OutputPolicy::None)
  {
    const int subsamling_lvl =
      _inifile.get<int>("output.subsamplingLevel", 0);
    const auto output_path = inifile.get<std::string>("output.outputPath");
		this->_log->debug("Creating a VTK writer with subsampling level: {}",
						  subsamling_lvl);

    Dorie::create_directories(output_path);
    const auto subsamling_intervals = Dune::refinementLevels(subsamling_lvl);
    auto sub_vtk = std::make_shared<Dune::SubsamplingVTKWriter<GV>>(gv,
      subsamling_intervals);

    vtkwriter = std::make_unique<Writer>(sub_vtk,
              inifile.get<std::string>("output.fileName"),
              output_path,
              "./");
  }

  // --- Operator Setup ---
  operator_setup();

  this->_log->info("Setup complete");
}


template<typename Traits>
void ModelTransport<Traits>::operator_setup()
{
  auto dof_count = gfs->globalSize();
  this->_log->debug("Setting up grid operators and solvers");
  if (helper.size() > 1) {
    this->_log->debug("  DOF of this process: {}", dof_count);
  }
  dof_count = gv.comm().sum(dof_count);
  this->_log->debug("  Total number of DOF: {}", dof_count);

  // --- Grid Operators ---
  go0 = std::make_shared<GO0>(*gfs,*cc,*gfs,*cc,*slop,mbe_slop);
  go1 = std::make_unique<GO1>(*gfs,*cc,*gfs,*cc,*tlop,mbe_tlop);
  if (ts_param->implicit())
    implicit_igo = std::make_unique<ImplicitIGO>(*go0,*go1);
  else
    explicit_igo = std::make_unique<ExplicitIGO>(*go0,*go1);

  // --- Solvers ---
  // Initialize helper spaces for DG linear solver only
  if constexpr (order > 0)
  {
    lsgfs = std::make_unique<LSGFS>(LSGFSHelper::create(gv));
    lscc = std::make_unique<LSCC>();
  }

  if (ts_param->implicit())
  {
    if constexpr (order == 0)
      implicit_ls = std::make_unique<ImplicitLS>(*gfs, 1000, 0, true, true);
    else
    {
      implicit_ls = std::make_unique<ImplicitLS>(
        *implicit_igo, *cc, *lsgfs, *lscc, 1000, 0, true, true);
    }
  }
  else
  {
    if constexpr (order == 0)
      explicit_ls = std::make_unique<ExplicitLS>(*gfs, 1000, 0, true, true);
    else
    {
      explicit_ls = std::make_unique<ExplicitLS>(
        *explicit_igo, *cc, *lsgfs, *lscc, 1000, 0, true, true);
    }
  }

  // --- Time Step Operators ---
  if (ts_param->implicit()){
    // Set linear problem solver verbosity to zero
    inifile["solverParameters.verbosity"] = "0";

    implicit_slps = std::make_unique<ImplicitSLPS>(
      *implicit_igo, *implicit_ls, inifile.sub("solverParameters")
    );
    implicit_osm = std::make_unique<ImplicitOSM>(
      *ts_param, *implicit_igo, *implicit_slps
    );
    implicit_osm->setVerbosityLevel(0);
  }
  else {
    explicit_osm = std::make_unique<ExplicitOSM>(
      *ts_param, *explicit_igo, *explicit_ls
    );
    explicit_osm->setVerbosityLevel(0);
  }

  gfs->update();
}

template<typename Traits>
void ModelTransport<Traits>::step()
{
  // long time step log for level 'debug'
  if (this->_log->should_log(spdlog::level::debug)) {
    this->_log->info("Time Step {}:",time_steps);
  }

  if (!water_flux_gf) {
    this->_log->error("Water flux grid function required");
    DUNE_THROW(Dune::InvalidStateException,
               "Pointer to water_flux_gf is invalid!");
  }
  if (!water_content_gf) {
    this->_log->error("Water content grid function required");
    DUNE_THROW(Dune::InvalidStateException,
               "Pointer to water_content_gf is invalid!"); 
  }

  // Suggest times step for explicit methods. 
  // NOTE: Do not futher increase time step on failure!
  if (not ts_param->implicit())
  {
    const auto cfl = Dune::Dorie::cfl_condition(*water_flux_gf,
                                                *water_content_gf,
                                                order);
    this->_log->trace("CFL condition: {}s", cfl);
    time_step_ctrl.suggest_time_step(_courant_number * cfl);
  }

  // get time step suggestion from boundary conditions
  sboundary->set_time(this->_time);
  const auto dt_suggestion = sboundary->max_time_step(this->_time);
  time_step_ctrl.suggest_time_step(dt_suggestion);

  // Allocate DOF vector for next time step
  auto unext = std::make_shared<U>(*u);

  // Obtain time variables
  // NOTE: Communicate minimal time step between processors!
  const auto time = this->_time;
  auto dt = time_step_ctrl.get_time_step(time);
  dt = gv.comm().min(dt);

  // Solver loop
  // NOTE: Runs infinitely until solution is computed and valid (causes break)
  //       or exception is thrown.
  while (true)
  {
    // Long time step log for level 'debug'
    if (this->_log->should_log(spdlog::level::debug)) {
      this->_log->debug("  Time {:.2e} + {:.2e} -> {:.2e}",
                time, dt, time+dt);
    }

    try
    {
      // Apply the solver
      dwarn.push(false);
      if (ts_param->implicit())
        implicit_osm->apply(time, dt, *u, *unext);
      else
        explicit_osm->apply(time, dt, *u, *unext);
      dwarn.pop();

      // Check the solution
      if (auto [valid, msg] = check_solution(*u, *unext);
          valid)
      {
        // Solution computed and valid: leave loop
        break;
      }
      else
      {
        this->_log->debug("  Invalid solution");
        this->_log->trace("  Error during solution check: {}", msg);
      }
    }
    catch (Dune::ISTLError &e){
      this->_log->debug("  Solver not converged");
      this->_log->trace("  Error in linear solver: {}", e.what());
    }
    catch (Dune::Exception &e){
      this->_log->error("  Unexpected error in solver: {}", e.what());
      DUNE_THROW(Dune::Exception, "Critical error in solver");
    }
    // rethrow anything else
    catch (...) {
      throw;
    }

    // abort if minimal time step is already reached
    if (Dune::FloatCmp::le(dt, time_step_ctrl.get_time_step_min())) {
      this->_log->error("Computing a solution for the minimal time "
                        "step failed");
      DUNE_THROW(Exception, "No solution for minimal time step");
    }

    // Reduce time step and try again
    // NOTE: Communicate minimal time step between processes!
    time_step_ctrl.decrease_time_step();
    dt = time_step_ctrl.get_time_step(time);
    dt = gv.comm().min(dt);
  } // while (true)

  // Short time step log for level 'info' (after success)
  if (not this->_log->should_log(spdlog::level::debug)) {
    this->_log->info("Time Step {}: {:.2e} + {:.2e} -> {:.2e}",
                     time_steps,
                     time,
                     dt,
                     time+dt);
  }

  // Print computing times in trace log
  if (ts_param->implicit())
  {
    auto result = implicit_osm->result();
    this->_log->trace("  Matrix assembly: {:.2e}s (total), {:.2e}s (success)",
                      result.total.assembler_time,
                      result.successful.assembler_time);
    this->_log->trace("  Linear solver:   {:.2e}s (total), {:.2e}s (success)",
                      result.total.linear_solver_time,
                      result.successful.linear_solver_time);
  }

  // Overwrite last solution with new solution
  u = unext;

  // Update time and time step
  this->_time += dt;
  time_step_ctrl.increase_time_step();
  time_steps++;

  cache_solute_flux_gf_rt.reset();

  if (this->output_policy() == OutputPolicy::EndOfTransportStep)
    write_data();
}

template<typename Traits>
void ModelTransport<Traits>::write_data () const
{
  if (vtkwriter)
  {   
    auto peclet = std::make_shared<const GFPeclet>(gv, sparam, water_flux_gf, water_content_gf);
    auto d_hd = std::make_shared<const GFEffectiveHydrodynamicDispersion>(gv, sparam, water_flux_gf, water_content_gf);

    if (inifile.get<bool>("output.vertexData")) {
      vtkwriter->addVertexData(get_solute(),"solute");
      vtkwriter->addVertexData(get_total_solute(),"total_solute");
      vtkwriter->addVertexData(peclet,"micro_peclet");
      
      if (inifile.get<bool>("output.writeDispersionTensor"))
        vtkwriter->addVertexData(d_hd,"eff_hd_dispersion");

      if constexpr (enable_rt_engine)
        if (enable_fluxrc) {
          auto RT_name = "flux_RT" + std::to_string(flux_order);
          vtkwriter->addVertexData(get_solute_flux_reconstructed(),RT_name);
        }
    } else {
      vtkwriter->addCellData(get_solute(),"solute");
      vtkwriter->addCellData(get_total_solute(),"total_solute");
      vtkwriter->addCellData(peclet,"micro_peclet");
      
      if (inifile.get<bool>("output.writeDispersionTensor"))
        vtkwriter->addCellData(d_hd,"eff_hd_dispersion");
      
      if constexpr (enable_rt_engine)
        if (enable_fluxrc) {
          auto RT_name = "flux_RT" + std::to_string(flux_order);
          vtkwriter->addCellData(get_solute_flux_reconstructed(),RT_name);
        }
    }

    try{
      this->_log->trace("Writing solution at time {:.2e}", this->_time);
      vtkwriter->write(this->_time, output_type);
    }
    catch (Dune::Exception& e) {
      this->_log->error("Writing VTK output failed: {}", e.what());
      DUNE_THROW(Dune::IOError, "Cannot write VTK output!");
    }
    catch(...){
      this->_log->error("Writing VTK output failed for unknown reason");
      DUNE_THROW(Dune::IOError, "Cannot write VTK output!");
    }
    vtkwriter->clear();
  } else {
    this->_log->error("Calling 'write_data' on object without VTK writer");
    DUNE_THROW(Dune::InvalidStateException, "No vtk writer configured!");
  }
}

template<typename Traits>
void ModelTransport<Traits>::mark_grid()
{
  DUNE_THROW(NotImplemented,
    "Dorie does not implement adaptivity for this simulation object!");
}

template<typename Traits>
void ModelTransport<Traits>::adapt_grid()
{
  DUNE_THROW(NotImplemented,
   "Dorie does not implement adaptivity for this simulation object!");
}


template<typename Traits>
void ModelTransport<Traits>::post_adapt_grid()
{
  operator_setup();
}

template<typename Traits>
std::pair<bool, std::string> ModelTransport<Traits>::check_solution(
  const U& u_before,
  const U& u_after
) const
{
  // Check for negative values
  if (check_negative_policy != CheckNegativePolicy::Pass)
  {
    // Compute tolerance from largest solute concentration and user input
    const auto max = *std::max_element(u_after.begin(), u_after.end());
    const auto eps = max * inifile.get<RF>("solutionCheck.negativeTol");

    // Check if values are negative and record minimum value encountered
    auto min = std::numeric_limits<RF>::max();
    auto check_negative = [eps, &min](const auto val)
                          {
                            min = std::min(min, val);
                            return val < -eps;
                          };

    // Perform the check
    if (std::any_of(u_after.begin(), u_after.end(), check_negative))
    {
      // NOTE: fmt library included via spdlog
      const std::string msg = fmt::format("Negative value in solution: {:.2e}",
                                          min);

      // Issue warning, but pass check
      if (check_negative_policy == CheckNegativePolicy::Warn) {
        this->_log->warn(msg);
      }
      // Do not pass check
      else {
        return std::make_pair(false, msg);
      }
    }
  }

  // Default: Pass without message
  return std::make_pair(true, "");
}

} // namespace Dorie
} // namespace Dune
