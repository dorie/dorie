#ifndef DUNE_DORIE_PARAM_TRANSPORT_FACTORY_HH
#define DUNE_DORIE_PARAM_TRANSPORT_FACTORY_HH

#include <dune/dorie/common/logging.hh>
#include <dune/dorie/common/parameterization_factory.hh>
#include <dune/dorie/model/transport/parameterization/interface.hh>
#include <dune/dorie/model/transport/parameterization/hydrodynamic_dispersion/const.hh>
#include <dune/dorie/model/transport/parameterization/hydrodynamic_dispersion/power_law.hh>
#include <dune/dorie/model/transport/parameterization/hydrodynamic_dispersion/eff_hydromechanic_dispersion/interface.hh>
#include <dune/dorie/model/transport/parameterization/hydrodynamic_dispersion/eff_hydromechanic_dispersion/const.hh>
#include <dune/dorie/model/transport/parameterization/hydrodynamic_dispersion/eff_hydromechanic_dispersion/isotropic.hh>
#include <dune/dorie/model/transport/parameterization/hydrodynamic_dispersion/eff_hydromechanic_dispersion/longitudinal/interface.hh>
#include <dune/dorie/model/transport/parameterization/hydrodynamic_dispersion/eff_hydromechanic_dispersion/longitudinal/const.hh>
#include <dune/dorie/model/transport/parameterization/hydrodynamic_dispersion/eff_hydromechanic_dispersion/transverse/interface.hh>
#include <dune/dorie/model/transport/parameterization/hydrodynamic_dispersion/eff_hydromechanic_dispersion/transverse/const.hh>
#include <dune/dorie/model/transport/parameterization/hydrodynamic_dispersion/eff_diffusion/interface.hh>
#include <dune/dorie/model/transport/parameterization/hydrodynamic_dispersion/eff_diffusion/const.hh>
#include <dune/dorie/model/transport/parameterization/hydrodynamic_dispersion/eff_diffusion/millington_quirk_1.hh>
#include <dune/dorie/model/transport/parameterization/hydrodynamic_dispersion/eff_diffusion/millington_quirk_2.hh>
#include <dune/dorie/model/transport/parameterization/hydrodynamic_dispersion/superposition.hh>

#include <muParser.h>

namespace Dune {
namespace Dorie {
namespace Parameterization {

/*-------------------------------------------------------------------------*//**
 * @brief      Factory of Transport parameters
 *
 * @tparam     Traits  The base traits
 */
template<class Traits>
struct TransportFactory 
    : public ParameterizationFactory<Transport<Traits>>
{

  /// Return a default-initialized parameterization object
  /** \param type The type indicating the parameterization implementation
   *  \param name The name of the parameterization object (layer type)
   */
  std::shared_ptr<Transport<Traits>> 
  selector(
      const YAML::Node& type_node,
      const std::string& name) const override
  {
    auto log = Dorie::get_logger(log_transport);

    std::shared_ptr<Transport<Traits>> hd_dips;

    uint type;

    try {
      mu::Parser parser;
      parser.DefineConst(ConstHydrodynamicDispersionParam<Traits>::type, 1);
      parser.DefineConst(PowerLawDispersion<Traits>::type, 2);

      parser.DefineConst(ConstEffectiveDiffusionParam<Traits>::type, 10);
      parser.DefineConst(MillingtonQuirk1<Traits>::type, 20);
      parser.DefineConst(MillingtonQuirk2<Traits>::type, 30);

      parser.DefineConst(ConstEffectiveHydromechanicDispersionParam<Traits>::type, 100);
      parser.DefineConst(IsotropicEffectiveHydromechanicDispersion<Traits>::type, 200);

      parser.SetExpr(type_node["type"].as<std::string>());
      type = parser.Eval();

    } catch (mu::Parser::exception_type& e) {
      log->error("Evaluating transport parameterization type failed:");
      log->error("  Parsed expression:   {}",e.GetExpr());
      log->error("  Token:               {}",e.GetToken());
      log->error("  Error position:      {}",e.GetPos());
      log->error("  Error code:          {}",e.GetCode());
      log->error("  Error message:       {}",e.GetMsg());
      DUNE_THROW(IOError, "Error evaluating transport parameterization type");
    }
    
    // get digit x from right to left of a uint.
    // uint ....... xxxxxx
    //              ^^  ^^
    // digit ...... 54..10
    auto get_digit = [](uint number, int x)
      {return (uint)(number*std::pow(10,-1*x))%10;};

    if (type == 1) {
      hd_dips = std::make_shared<ConstHydrodynamicDispersionParam<Traits>>(name);
    } else if (type == 2) {
      hd_dips = std::make_shared<PowerLawDispersion<Traits>>(name);
    } else if (get_digit(type,0) == 0) {
      std::shared_ptr<EffectiveDiffusionParam<Traits>> eff_diff;
  
      // Select effective diffusion according to digit 1 of 'type'
      uint type_1 = get_digit(type,1);

      // Build parametrization for effective diffusion
      if (type_1 == 1) {
          eff_diff = std::make_shared<ConstEffectiveDiffusionParam<Traits>>(name);
      } else if (type_1 == 2) {
          eff_diff = std::make_shared<MillingtonQuirk1<Traits>>(name);
      } else if (type_1 == 3) {
          eff_diff = std::make_shared<MillingtonQuirk2<Traits>>(name);
      } else {
        log->error("Effective Diffusion parameterization '{}' "
                  "has unknown type '{}'",
                    name, type_node["type"].as<std::string>());
        DUNE_THROW(IOError, "Unknown Transport parameterization type");
      }

      // Select type for effective hydromechanic dispersion
      std::shared_ptr<EffectiveHydromechanicDispersionParam<Traits>> eff_hm_disp;

      // Select effective hydromechanic dispersion according to digit 2 of 'type'
      uint type_2 = get_digit(type,2);

      // Build parametrization for effective hydromechanic dispersion
      if (type_2 == 1) {
          eff_hm_disp = std::make_shared<ConstEffectiveHydromechanicDispersionParam<Traits>>(name);
      } else if (type_2 == 2) {
        
        // Select type for longitudinal and transverse dispersivity
        std::shared_ptr<ConstLongitudinalDispersivityParam<Traits>> lambda_l;
        std::shared_ptr<ConstTransverseDispersivityParam<Traits>> lambda_t;

        // Build parametrization for longitudinal dispersivity
        // if (...) {
            lambda_l = std::make_shared<ConstLongitudinalDispersivityParam<Traits>>(name);
        // } else {
        //   log->error("Longitudinal Dispersivity parameterization '{}' "
        //             "has unknown type '{}'",
        //               name, long_disp_type);
        //   DUNE_THROW(IOError, "Unknown Transport parameterization type");
        // }

        // Build parametrization for transverse dispersivity
        // if (...) {
            lambda_t = std::make_shared<ConstTransverseDispersivityParam<Traits>>(name);
        // } else {
        //   log->error("Transverse Dispersivity parameterization '{}' "
        //             "has unknown type '{}'",
        //               name, trans_disp_type);
        //   DUNE_THROW(IOError, "Unknown Transport parameterization type");
        // }

        eff_hm_disp = std::make_shared<IsotropicEffectiveHydromechanicDispersion<Traits>>(name,lambda_l,lambda_t);
      } else {
        log->error("Effective Hydromechanic Dispersion parameterization '{}' "
                  "has unknown type '{}'",
                    name, type_node["type"].as<std::string>());
        DUNE_THROW(IOError, "Unknown Transport parameterization type");
      }

      hd_dips = std::make_shared<HydrodynamicDispersionSuperposition<Traits>>(name,eff_diff,eff_hm_disp);
    } else {
      log->error("Transport parameterization '{}' has unknown type '{}'",
                  name, type_node["type"].as<std::string>());
      DUNE_THROW(IOError, "Unknown Transport parameterization type");
    }
    return hd_dips;
  }
};

} // namespace Parameterization
} // namespace Dune
} // namespace Dorie

#endif // DUNE_DORIE_PARAM_TRANSPORT_FACTORY_HH