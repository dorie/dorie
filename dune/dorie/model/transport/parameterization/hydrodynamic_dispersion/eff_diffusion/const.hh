#ifndef DUNE_DORIE_PARAM_CONST_EFFECTIVE_DIFFUSION_HH
#define DUNE_DORIE_PARAM_CONST_EFFECTIVE_DIFFUSION_HH

#include <cmath>
#include <functional>
#include <string>

#include <dune/dorie/model/transport/parameterization/hydrodynamic_dispersion/eff_diffusion/interface.hh>

namespace Dune {
namespace Dorie {
namespace Parameterization {


/*-------------------------------------------------------------------------*//**
 * @brief      Class for constant effective diffusion.
 *
 * @ingroup    TransportParam
 * @author     Santiago Ospina De Los Ríos
 * @date       2019
 *
 * @tparam     Traits  the base traits
 */
template <class Traits>
class ConstEffectiveDiffusionParam 
    : public EffectiveDiffusionParam<Traits>
{
private:
    using RangeField = typename Traits::RF;
    using Base = EffectiveDiffusionParam<Traits>;

public:
    using EffectiveDiffusion = typename Base::EffectiveDiffusion;
    using WaterFlux = typename Base::WaterFlux;
    using WaterContent = typename Base::WaterContent;

    /// Parameter defining the constant effective diffusion
    struct ConstEffectiveDiffusion
    {
        RangeField value;
        inline static const std::string name = "eff_diff";
    };

    /// The name of this parameterization type (This is not the instance name)
    static inline std::string type = "Deff_const";

    ConstEffectiveDiffusion _const_eff_diff;

public:

    /// Construct with default-initialized parameters
    /** \param name The name associated with this soil layer
     */
    ConstEffectiveDiffusionParam (const std::string name) :
        EffectiveDiffusionParam<Traits>(name)
    { }

    /// Construct from a tuple of parameters
    /** \param name The name associated with this soil layer
     *  \param parameters Tuple of parameters to use in this parameterization
     */
    template<typename... Args>
    ConstEffectiveDiffusionParam (
            const std::string name,
            const std::tuple<Args...> parameters) :
        EffectiveDiffusionParam<Traits>(name, parameters),
        _const_eff_diff(std::get<ConstEffectiveDiffusion>(parameters))
    { }

    /// Add default destructor to clarify override
    ~ConstEffectiveDiffusionParam () override = default;

public:

    /// Return the effective diffusion
    /** {WaterFlux,WaterContent} -> EffectiveDiffusion
     */
    std::function<EffectiveDiffusion(const WaterFlux, const WaterContent)> 
    effective_diffusion_f () const override
    {
        return [this](const WaterFlux, const WaterContent) {
            return EffectiveDiffusion{_const_eff_diff.value};
        };
    }

    /// Return a map of parameter names and values for manipulation
    /** \return Map: Parameter name, parameter value in this object
     */
    std::multimap<std::string, double&> parameters () override
    {
        const std::string _const_eff_diff_name = ConstEffectiveDiffusion::name;
        return {{_const_eff_diff_name, _const_eff_diff.value}};
    }

    /// Return a map of parameter names and values for manipulation
    /** \return Map: Parameter name, parameter value in this object
     */
    std::multimap<std::string, const double&> parameters () const override
    {
        const std::string _const_eff_diff_name = ConstEffectiveDiffusion::name;
        return {{_const_eff_diff_name, _const_eff_diff.value}};
    }

    /// Clone the plymorphic class
    /** \return unique pointer to the cloned object
     */
    std::unique_ptr<EffectiveDiffusionParam<Traits>> clone () const override
    {
        using ThisType = ConstEffectiveDiffusionParam<Traits>;
        return std::make_unique<ThisType>(*this);
    }
};

} // namespace Parameterization
} // namespace Dune
} // namespace Dorie

#endif // DUNE_DORIE_PARAM_CONST_EFFECTIVE_DIFFUSION_HH