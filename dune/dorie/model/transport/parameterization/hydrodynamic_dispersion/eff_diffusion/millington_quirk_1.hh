#ifndef DUNE_DORIE_PARAM_MILLIGTON_QUIRK_1_HH
#define DUNE_DORIE_PARAM_MILLIGTON_QUIRK_1_HH

#include <cmath>
#include <functional>
#include <string>

#include <dune/dorie/model/transport/parameterization/hydrodynamic_dispersion/eff_diffusion/interface.hh>

namespace Dune {
namespace Dorie {
namespace Parameterization {

/*-------------------------------------------------------------------------*//**
 * @brief      Class for effective diffusion with Milington Quirk I.
 * @details    Implements the relation @f$ \mathsf{D}^\mathsf{eff}
 *             =\frac{\theta_w^{7/3}\mathsf{D}_\mathsf{m}}{\phi^2} @f$
 *             where @f$ \theta_w @f$ is the water content, 
 *             @f$ \mathsf{D}_\mathsf{m} @f$ the molecular diffusion, and 
 *             @f$ \phi @f$ the porosity of the medium.
 *             
 * @ingroup    TransportParam
 * @author     Santiago Ospina De Los Ríos
 * @date       2019
 *
 * @tparam     Traits  the base traits
 */
template <class Traits>
class MillingtonQuirk1
    : public EffectiveDiffusionParam<Traits>
{
private:
    using RangeField = typename Traits::RF;
    using Base = EffectiveDiffusionParam<Traits>;

public:
    using EffectiveDiffusion = typename Base::EffectiveDiffusion;
    using WaterFlux = typename Base::WaterFlux;
    using WaterContent = typename Base::WaterContent;

    /// Parameter defining molecular diffusion
    struct ModelcularDiffusionType
    {
        RangeField value;
        inline static const std::string name = "mol_diff";
    };

    /// Parameter defining porosity
    struct PorosityType
    {
        RangeField value;
        inline static const std::string name = "phi";
    };

    /// The name of this parameterization type (This is not the instance name)
    static inline std::string type = "Deff_MQ1";

    ModelcularDiffusionType _mol_diff;
    PorosityType _porosity;

public:

    /// Construct with default-initialized parameters
    /** \param name The name associated with this soil layer
     */
    MillingtonQuirk1 (const std::string name) :
        Base(name)
    { }

    /// Construct from a tuple of parameters
    /** \param name The name associated with this soil layer
     *  \param parameters Tuple of parameters to use in this parameterization
     */
    template<typename... Args>
    MillingtonQuirk1 (
            const std::string name,
            const std::tuple<Args...> parameters) :
        Base(name, parameters),
        _mol_diff(std::get<ModelcularDiffusionType>(parameters)),
        _porosity(std::get<PorosityType>(parameters))
    { }

    /// Add default destructor to clarify override
    ~MillingtonQuirk1 () override = default;

    /// Return the effective diffusion
    /** {WaterFlux,WaterContent} -> EffectiveDiffusion
     */
    std::function<EffectiveDiffusion(const WaterFlux, const WaterContent)> 
    effective_diffusion_f () const override
    {
        return [this](const WaterFlux water_flux, const WaterContent water_content) {
            auto val = _mol_diff.value*std::pow(water_content.value,7./3.)/std::pow(_porosity.value,2./3.);
            return EffectiveDiffusion{val};
        };
    }

    /// Return a map of parameter names and values for manipulation
    /** \return Map: Parameter name, parameter value in this object
     */
    std::multimap<std::string, double&> parameters () override
    {
        return {
            {ModelcularDiffusionType::name, _mol_diff.value},
            {PorosityType::name,            _porosity.value}
        };
    }

    /// Return a map of parameter names and values for manipulation
    /** \return Map: Parameter name, parameter value in this object
     */
    std::multimap<std::string, const double&> parameters () const override
    {
        return {
            {ModelcularDiffusionType::name, _mol_diff.value},
            {PorosityType::name,            _porosity.value}
        };
    }

    /// Return a map of parameter names and values for manipulation
    /** \return Map: Parameter name, parameter value in this object
     */
    std::unique_ptr<EffectiveDiffusionParam<Traits>> clone () const override
    {
        using ThisType = MillingtonQuirk1<Traits>;
        return std::make_unique<ThisType>(*this);
    }
};

} // namespace Parameterization
} // namespace Dune
} // namespace Dorie

#endif // DUNE_DORIE_PARAM_MILLIGTON_QUIRK_1_HH