#ifndef DUNE_DORIE_PARAM_CONST_HYDRODYNAMIC_DISPERSION_HH
#define DUNE_DORIE_PARAM_CONST_HYDRODYNAMIC_DISPERSION_HH

#include <cmath>
#include <functional>
#include <string>

#include <dune/dorie/model/transport/parameterization/interface.hh>

namespace Dune {
namespace Dorie {
namespace Parameterization {

/*-------------------------------------------------------------------------*//**
 * @brief      Class for constant hydrodynamic dispersion.
 *
 * @ingroup    TransportParam
 * @author     Santiago Ospina De Los Ríos
 * @date       2019
 *
 * @tparam     Traits  the base traits
 */
template <class Traits>
class ConstHydrodynamicDispersionParam :
    public Transport<Traits>
{
private:
    using Tensor = typename Traits::Tensor;
    using Base = Transport<Traits>;

public:
    using HydrodynamicDispersion = typename Base::HydrodynamicDispersion;
    using WaterFlux = typename Base::WaterFlux;
    using WaterContent = typename Base::WaterContent;

    /// Parameter defining the constant hydrodynamic dispersion tensor
    struct ConstHydrodynamicDispersion
    {
        Tensor value;
        inline static const std::string name = "hydrodynamic_disp";
    };

    /// The name of this parameterization type (This is not the instance name)
    static inline std::string type = "Dhd_const";

    ConstHydrodynamicDispersion _const_hydrodynamic_dispersion;

public:

    /// Construct with default-initialized parameters
    /** \param name The name associated with this soil layer
     */
    ConstHydrodynamicDispersionParam (const std::string name) :
        Transport<Traits>(name)
    { }

    /// Construct from a tuple of parameters
    /** \param name The name associated with this soil layer
     *  \param parameters Tuple of parameters to use in this parameterization
     */
    template<typename... Args>
    ConstHydrodynamicDispersionParam (
            const std::string name,
            const std::tuple<Args...> parameters) :
        Base(name, parameters),
        _const_hydrodynamic_dispersion(std::get<ConstHydrodynamicDispersion>(parameters))
    { }

    /// Add default destructor to clarify override
    ~ConstHydrodynamicDispersionParam () override = default;

    /// Return the hydrodynamic dispersion
    /** {WaterFlux,WaterContent} -> HydrodynamicDispersion
     */
    std::function<HydrodynamicDispersion(const WaterFlux, const WaterContent)> 
    hydrodynamic_dispersion_f () const override
    {
        return [this](const WaterFlux, const WaterContent) {
            return HydrodynamicDispersion{_const_hydrodynamic_dispersion.value};
        };
    }

    /// Return a map of parameter names and values for manipulation
    /** \return Map: Parameter name, parameter value in this object
     */
    std::multimap<std::string, double&> parameters () override
    {
        using Map = std::multimap<std::string, double&>;

        Map base_param = Transport<Traits>::parameters();
        Map new_param;
        Map compsite_map;
        
        auto _const_type_name = ConstHydrodynamicDispersion::name;
        
        if constexpr (Traits::dim == 2)
            new_param = Map({
                {_const_type_name+"_xx", _const_hydrodynamic_dispersion.value[0][0]},
                {_const_type_name+"_xy", _const_hydrodynamic_dispersion.value[1][0]},
                {_const_type_name+"_yx", _const_hydrodynamic_dispersion.value[0][1]},
                {_const_type_name+"_yy", _const_hydrodynamic_dispersion.value[1][1]}
            });
        else if constexpr (Traits::dim == 3)
            new_param = Map({
                {_const_type_name+"_xx", _const_hydrodynamic_dispersion.value[0][0]},
                {_const_type_name+"_xy", _const_hydrodynamic_dispersion.value[1][0]},
                {_const_type_name+"_xz", _const_hydrodynamic_dispersion.value[2][0]},
                {_const_type_name+"_yx", _const_hydrodynamic_dispersion.value[0][1]},
                {_const_type_name+"_yy", _const_hydrodynamic_dispersion.value[1][1]},
                {_const_type_name+"_yz", _const_hydrodynamic_dispersion.value[2][1]},
                {_const_type_name+"_zx", _const_hydrodynamic_dispersion.value[0][2]},
                {_const_type_name+"_xy", _const_hydrodynamic_dispersion.value[1][2]},
                {_const_type_name+"_zz", _const_hydrodynamic_dispersion.value[2][2]}
            });
        else
            static_assert(true,
                "ConstHydrodynamicDispersion not available for this dimension");

        compsite_map.insert(base_param.begin(),base_param.end());
        compsite_map.insert(new_param.begin(),new_param.end());

        return compsite_map;
    }

    /// Return a map of parameter names and values for manipulation
    /** \return Map: Parameter name, parameter value in this object
     */
    std::multimap<std::string, const double&> parameters () const override
    {
        using Map = std::multimap<std::string, const double&>;

        Map base_param = Transport<Traits>::parameters();
        Map new_param;
        Map compsite_map;
        
        auto _const_type_name = ConstHydrodynamicDispersion::name;
        
        if constexpr (Traits::dim == 2)
            new_param = Map({
                {_const_type_name+"_xx", _const_hydrodynamic_dispersion.value[0][0]},
                {_const_type_name+"_xy", _const_hydrodynamic_dispersion.value[1][0]},
                {_const_type_name+"_yx", _const_hydrodynamic_dispersion.value[0][1]},
                {_const_type_name+"_yy", _const_hydrodynamic_dispersion.value[1][1]}
            });
        else if constexpr (Traits::dim == 3)
            new_param = Map({
                {_const_type_name+"_xx", _const_hydrodynamic_dispersion.value[0][0]},
                {_const_type_name+"_xy", _const_hydrodynamic_dispersion.value[1][0]},
                {_const_type_name+"_xz", _const_hydrodynamic_dispersion.value[2][0]},
                {_const_type_name+"_yx", _const_hydrodynamic_dispersion.value[0][1]},
                {_const_type_name+"_yy", _const_hydrodynamic_dispersion.value[1][1]},
                {_const_type_name+"_yz", _const_hydrodynamic_dispersion.value[2][1]},
                {_const_type_name+"_zx", _const_hydrodynamic_dispersion.value[0][2]},
                {_const_type_name+"_xy", _const_hydrodynamic_dispersion.value[1][2]},
                {_const_type_name+"_zz", _const_hydrodynamic_dispersion.value[2][2]}
            });
        else
            static_assert(Dune::AlwaysFalse<Traits>::value,
                "ConstHydrodynamicDispersion not available for this dimension");

        compsite_map.insert(base_param.begin(),base_param.end());
        compsite_map.insert(new_param.begin(),new_param.end());

        return compsite_map;
    }

    /// Clone the plymorphic class
    /** \return unique pointer to the cloned object
     */
    std::unique_ptr<Transport<Traits>> clone () const override
    {
        using ThisType = ConstHydrodynamicDispersionParam<Traits>;
        return std::make_unique<ThisType>(*this);
    }
};

} // namespace Parameterization
} // namespace Dune
} // namespace Dorie

#endif // DUNE_DORIE_PARAM_CONST_HYDRODYNAMIC_DISPERSION_HH