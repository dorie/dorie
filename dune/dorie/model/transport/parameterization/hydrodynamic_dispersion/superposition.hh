#ifndef DUNE_DORIE_PARAM_SUPERPOSITION_HD_DISP_HH
#define DUNE_DORIE_PARAM_SUPERPOSITION_HD_DISP_HH

#include <cmath>
#include <functional>
#include <string>

#include <dune/dorie/model/transport/parameterization/interface.hh>
#include <dune/dorie/model/transport/parameterization/hydrodynamic_dispersion/eff_hydromechanic_dispersion/interface.hh>
#include <dune/dorie/model/transport/parameterization/hydrodynamic_dispersion/eff_diffusion/interface.hh>

namespace Dune {
namespace Dorie {
namespace Parameterization {

/*-------------------------------------------------------------------------*//**
 * @brief      Class for superposition hydrodynamic dispersion.
 * @details    Implements the relation @f$ \mathsf{D}_\mathsf{hd}
 *             =I \mathsf{D}^\mathsf{eff} + \mathsf{D}_\mathsf{hm}^\mathsf{eff}
 *             @f$ where @f$ I @f$ is the identity matrix, 
 *             @f$ \mathsf{D}^\mathsf{eff} @f$ the effective diffusion 
 *             coefficient, and  @f$ \mathsf{D}_\mathsf{hm}^\mathsf{eff} @f$ the 
 *             hydromechanic dispersion tensor. Both of them implemented by 
 *             polymorphic objects following EffectiveDiffusion and 
 *             EffectiveHydromechanicDispersion interfaces.
 *             
 * @ingroup    TransportParam
 * @author     Santiago Ospina De Los Ríos
 * @date       2019
 *
 * @tparam     Traits  the base traits
 */
template <class Traits>
class HydrodynamicDispersionSuperposition :
    public Transport<Traits>
{
private:
    using RangeField = typename Traits::RF;
    using Base = Transport<Traits>;
    static constexpr int dim = Traits::dim;

public:
    using HydrodynamicDispersion = typename Base::HydrodynamicDispersion;
    using WaterFlux = typename Base::WaterFlux;
    using WaterContent = typename Base::WaterContent;

    /// The name of this parameterization type (This is not the instance name)
    static inline std::string type = "superposition";

    std::shared_ptr<EffectiveDiffusionParam<Traits>>                 _eff_diff;
    std::shared_ptr<EffectiveHydromechanicDispersionParam<Traits>>   _eff_hm_disp;

public:

    /// Construct with polymorphic objects
    /** \param name The name associated with this soil layer
     * \param eff_diff The class for the effective diffusion coefficient
     * \param eff_hm_disp The class for the effective hydromechanic tensor
     */
    HydrodynamicDispersionSuperposition (const std::string name, 
        std::shared_ptr<EffectiveDiffusionParam<Traits>> eff_diff,
        std::shared_ptr<EffectiveHydromechanicDispersionParam<Traits>> eff_hm_disp
    ) : Base(name),
        _eff_diff(eff_diff),
        _eff_hm_disp(eff_hm_disp)
    { }


    /// Add default destructor to clarify override
    ~HydrodynamicDispersionSuperposition () override = default;

    /// Return the hydrodynamic dispersion
    /** {WaterFlux,WaterContent} -> HydrodynamicDispersion
     */
    std::function<HydrodynamicDispersion(const WaterFlux, const WaterContent)> 
    hydrodynamic_dispersion_f () const override
    {
        return [this](const WaterFlux water_flux, const WaterContent water_content) {
            auto eff_diff = _eff_diff->effective_diffusion_f()(water_flux,water_content);
            auto eff_hm_disp = _eff_hm_disp->effective_hydromechanic_dispersion_f()(water_flux,water_content);
            for (int i = 0; i < dim; ++i)
                    eff_hm_disp.value[i][i]+=eff_diff.value;
            return HydrodynamicDispersion{eff_hm_disp.value};
        };
    }

    /// Return a map of parameter names and values for manipulation
    /** \return Map: Parameter name, parameter value in this object
     */
    std::multimap<std::string, double&> parameters () override
    {
        auto base_param = Transport<Traits>::parameters();
        auto eff_hydr_disp_param = _eff_hm_disp->parameters();
        auto eff_diff_param = _eff_diff->parameters();
        
        std::multimap<std::string, double&> compsite_map;
        compsite_map.insert(base_param.begin(),base_param.end());
        compsite_map.insert(eff_hydr_disp_param.begin(),eff_hydr_disp_param.end());
        compsite_map.insert(eff_diff_param.begin(),eff_diff_param.end());

        return compsite_map;
    }

    /// Return a map of parameter names and values for manipulation
    /** \return Map: Parameter name, parameter value in this object
     */
    std::multimap<std::string, const double&> parameters () const override
    {
        auto base_param = Transport<Traits>::parameters();
        auto eff_hydr_disp_param = _eff_hm_disp->parameters();
        auto eff_diff_param = _eff_diff->parameters();

        std::multimap<std::string, const double&> compsite_map;
        compsite_map.insert(base_param.begin(),base_param.end());
        compsite_map.insert(eff_hydr_disp_param.begin(),eff_hydr_disp_param.end());
        compsite_map.insert(eff_diff_param.begin(),eff_diff_param.end());

        return compsite_map;
    }

    /// Return a map of parameter names and values for manipulation
    /** \return Map: Parameter name, parameter value in this object
     */
    std::unique_ptr<Transport<Traits>> clone () const override
    {
        using ThisType = HydrodynamicDispersionSuperposition<Traits>;
        return std::make_unique<ThisType>(this->get_name(),_eff_diff->clone(),_eff_hm_disp->clone());
    }
};

} // namespace Parameterization
} // namespace Dune
} // namespace Dorie

#endif // DUNE_DORIE_PARAM_SUPERPOSITION_HD_DISP_HH