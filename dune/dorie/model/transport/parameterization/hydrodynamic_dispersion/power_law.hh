#ifndef DUNE_DORIE_PARAM_POWER_LAW_HD_DISP_HH
#define DUNE_DORIE_PARAM_POWER_LAW_HD_DISP_HH

#include <cmath>
#include <functional>
#include <string>

#include <dune/dorie/model/transport/parameterization/interface.hh>

namespace Dune {
namespace Dorie {
namespace Parameterization {

/*-------------------------------------------------------------------------*//**
 * @brief      Class for power law hydrodynamic dispersion.
 * @details    Implements the relation @f$ \mathsf{D}_\mathsf{hd}
 *             =I \mathsf{D}_\mathsf{m}\gamma\mathsf{pe}^\alpha @f$
 *             where @f$ I @f$ is the identity matrix, 
 *             @f$ \mathsf{D}_\mathsf{m} @f$ the molecular diffusion, and 
 *             @f$ \alpha @f$ and @f$ \gamma @f$ parameters of the power law.
 *             
 * @ingroup    TransportParam
 * @author     Santiago Ospina De Los Ríos
 * @date       2019
 *
 * @tparam     Traits  the base traits
 */
template <class Traits>
class PowerLawDispersion :
    public Transport<Traits>
{
private:
    using RangeField = typename Traits::RF;
    using Base = Transport<Traits>;

public:

    using HydrodynamicDispersion = typename Base::HydrodynamicDispersion;
    using WaterFlux = typename Base::WaterFlux;
    using WaterContent = typename Base::WaterContent;
    using Peclet = typename Transport<Traits>::Peclet;

    /// Parameter defining the gamma constant
    struct Gamma
    {
        RangeField value;
        inline static const std::string name = "gamma";
    };

    /// Parameter defining the alpha constant
    struct Alpha
    {
        RangeField value;
        inline static const std::string name = "alpha";
    };

    /// The name of this parameterization type (This is not the instance name)
    static inline std::string type = "Dhd_pl";

    Gamma _gamma;
    Alpha _alpha;

public:

    /// Construct with default-initialized parameters
    /** \param name The name associated with this soil layer
     */
    PowerLawDispersion (const std::string name) :
        Base(name)
    { }

    /// Construct from a tuple of parameters
    /** \param name The name associated with this soil layer
     *  \param parameters Tuple of parameters to use in this parameterization
     */
    template<typename... Args>
    PowerLawDispersion (
            const std::string name,
            const std::tuple<Args...> parameters) :
        Base(name, parameters),
        _gamma(std::get<Gamma>(parameters)),
        _alpha(std::get<Alpha>(parameters))
    { }

    /// Add default destructor to clarify override
    ~PowerLawDispersion () override = default;

    /// Return the hydrodynamic dispersion
    /** {WaterFlux,WaterContent} -> HydrodynamicDispersion
     */
    std::function<HydrodynamicDispersion(const WaterFlux, const WaterContent)> 
    hydrodynamic_dispersion_f () const override
    {
        return [this](const WaterFlux water_flux, const WaterContent water_content) {
            auto diff = this->_mol_diff.value;
            auto pe = this->peclet_f()(water_flux,water_content).value;
            auto val = diff*_gamma.value*std::pow(pe,_alpha.value);
            HydrodynamicDispersion eff_hdr_disp{0.};
            for (int i = 0; i < Traits::dim; ++i)
                eff_hdr_disp.value[i][i] = val;
            return eff_hdr_disp;
        };
    }

    /// Return a map of parameter names and values for manipulation
    /** \return Map: Parameter name, parameter value in this object
     */
    std::multimap<std::string, double&> parameters () override
    {
        using Map = std::multimap<std::string, double&>;

        Map base_param = Transport<Traits>::parameters();
        Map new_param({{Alpha::name, _alpha.value},
                       {Gamma::name, _gamma.value}});
        Map compsite_map;

        compsite_map.insert(base_param.begin(),base_param.end());
        compsite_map.insert(new_param.begin(),new_param.end());

        return compsite_map;
    }

    /// Return a map of parameter names and values for manipulation
    /** \return Map: Parameter name, parameter value in this object
     */
    std::multimap<std::string, const double&> parameters () const override
    {
        using Map = std::multimap<std::string, const double&>;

        Map base_param = Transport<Traits>::parameters();
        Map new_param({{Alpha::name, _alpha.value},
                       {Gamma::name, _gamma.value}});
        Map compsite_map;

        compsite_map.insert(base_param.begin(),base_param.end());
        compsite_map.insert(new_param.begin(),new_param.end());

        return compsite_map;
    }

    /// Return a map of parameter names and values for manipulation
    /** \return Map: Parameter name, parameter value in this object
     */
    std::unique_ptr<Transport<Traits>> clone () const override
    {
        using ThisType = PowerLawDispersion<Traits>;
        return std::make_unique<ThisType>(*this);
    }
};

} // namespace Parameterization
} // namespace Dune
} // namespace Dorie

#endif // DUNE_DORIE_PARAM_POWER_LAW_HD_DISP_HH