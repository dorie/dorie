#ifndef DUNE_DORIE_PARAM_EFFECTIVE_HYDROMECHANIC_DISPERSION_INTERFACE_HH
#define DUNE_DORIE_PARAM_EFFECTIVE_HYDROMECHANIC_DISPERSION_INTERFACE_HH

#include <functional>
#include <string>
#include <map>

#include <dune/dorie/model/transport/parameterization/interface.hh>

namespace Dune {
namespace Dorie {
namespace Parameterization {

/*-------------------------------------------------------------------------*//**
 * @brief      Class for effective hydromechanic dispersion.
 *             
 * @ingroup    TransportParam
 * @author     Santiago Ospina De Los Ríos
 * @date       2019
 *
 * @tparam     Traits  the base traits
 */
template <class Traits>
class EffectiveHydromechanicDispersionParam
{
private:
    using BaseP = Transport<Traits>;
    using RangeField = typename Traits::RF;
    using Scalar = typename Traits::Scalar;
    using Vector = typename Traits::Vector;
    using Tensor = typename Traits::Tensor;
    
public:

    /// Type of the hydrodynamic dispersion
    struct EffectiveHydromechanicDispersion
    {
        Tensor value;
    };

    using WaterFlux = typename BaseP::WaterFlux;
    using WaterContent = typename BaseP::WaterContent;

    //! The name of this parameterization instance, associated with the layer.
    const std::string _name;

    /// Construct with default-initialized parameters
    /** \param name The name associated with this soil layer
     */
    EffectiveHydromechanicDispersionParam (const std::string name) 
        :   _name(name)
    { }

    /// Construct from a tuple of parameters
    /** \param name The name associated with this soil layer
     *  \param parameters Tuple of parameters to use in this parameterization
     */
    template<typename... Args>
    EffectiveHydromechanicDispersionParam (
            const std::string name,
            const std::tuple<Args...> parameters)
        :   _name(name)
    { }

    /// Default constructor (virtual).
    virtual ~EffectiveHydromechanicDispersionParam () = default;

    /// Return the name of this parameterization instance.
    const std::string& get_name() const { return _name; }

    /// Return a bound version of the hydromechanic dispersion tensor
    /** \return Function: {Water Flux, Water Content} -> Hydromechanic Disp. Tensor
     */
    virtual std::function<
        EffectiveHydromechanicDispersion(const WaterFlux, const WaterContent)> 
            effective_hydromechanic_dispersion_f () const = 0;

    /// Return a map referecing all parameters by their names.
    /** \return Map. Key: Name of parameter (string).
     *      Value: Value of parameter (double&)
     */
    virtual std::multimap<std::string, double&> parameters () = 0;

    /// Return a map referecing all parameters by their names.
    /** \return Map. Key: Name of parameter (string).
     *      Value: Value of parameter (const double&)
     */
    virtual std::multimap<std::string, const double&> parameters () const = 0;

    /// Clone the plymorphic class
    /** \return unique pointer to the cloned object
     */
    virtual std::unique_ptr<EffectiveHydromechanicDispersionParam<Traits>> clone () const = 0;
};

} // namespace Parameterization
} // namespace Dune
} // namespace Dorie

#endif // DUNE_DORIE_PARAM_EFFECTIVE_HYDROMECHANIC_DISPERSION_INTERFACE_HH