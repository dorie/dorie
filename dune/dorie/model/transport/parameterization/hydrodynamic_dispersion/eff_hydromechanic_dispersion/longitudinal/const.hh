#ifndef DUNE_DORIE_PARAM_CONST_LONGITUDINAL_DISPERSIVITY_INTERFACE_HH
#define DUNE_DORIE_PARAM_CONST_LONGITUDINAL_DISPERSIVITY_INTERFACE_HH

#include <functional>
#include <string>
#include <map>

#include <dune/dorie/model/transport/parameterization/hydrodynamic_dispersion/eff_hydromechanic_dispersion/isotropic.hh>

namespace Dune {
namespace Dorie {
namespace Parameterization {

/*-------------------------------------------------------------------------*//**
 * @brief      Class for constant longitudinal dispersivity.
 *
 * @ingroup    TransportParam
 * @author     Santiago Ospina De Los Ríos
 * @date       2019
 *
 * @tparam     Traits  the base traits
 */
template <class Traits>
class ConstLongitudinalDispersivityParam 
    : public LongitudinalDispersivityParam<Traits>
{
private:
    using Base = LongitudinalDispersivityParam<Traits>;
    using RangeField = typename Traits::RF;

public:
    using WaterFlux = typename Base::WaterFlux;
    using WaterContent = typename Base::WaterContent;
    using LongitudinalDispersivity = typename Base::LongitudinalDispersivity;

    /// Parameter defining the constant longitudinal dispersivity
    struct ConstLongitudinalDispersivity
    {
        RangeField value;
        inline static const std::string name = "lambda_l";
    };

    /// The name of this parameterization type (This is not the instance name)
    static inline std::string type = "const";

    ConstLongitudinalDispersivity _lambda_l;

public:

    /// Construct with default-initialized parameters
    /** \param name The name associated with this soil layer
     */
    ConstLongitudinalDispersivityParam (const std::string name) 
        :   Base(name)
    { }

    /// Construct from a tuple of parameters
    /** \param name The name associated with this soil layer
     *  \param parameters Tuple of parameters to use in this parameterization
     */
    template<typename... Args>
    ConstLongitudinalDispersivityParam (
            const std::string name,
            const std::tuple<Args...> parameters) :
        Base(name, parameters),
        _lambda_l(std::get<ConstLongitudinalDispersivity>(parameters))
    { }

    /// Add default destructor to clarify override
    ~ConstLongitudinalDispersivityParam () override = default;

    /// Return the longitudinal dispersivity
    /** {WaterFlux,WaterContent} -> LongitudinalDispersivity
     */
    std::function<
        LongitudinalDispersivity(const WaterFlux, const WaterContent)> 
            longitudinal_dispersivity_f () const override
    {
        return [this](const WaterFlux, const WaterContent) {
            return LongitudinalDispersivity{_lambda_l.value};
        };
    }

    /// Return a map of parameter names and values for manipulation
    /** \return Map: Parameter name, parameter value in this object
     */
    std::multimap<std::string, double&> parameters () override
    {
        return {{ConstLongitudinalDispersivity::name,_lambda_l.value}};
    }

    /// Return a map of parameter names and values for manipulation
    /** \return Map: Parameter name, parameter value in this object
     */
    std::multimap<std::string, const double&> parameters () const override
    {
        return {{ConstLongitudinalDispersivity::name,_lambda_l.value}};
    }

    /// Return a map of parameter names and values for manipulation
    /** \return Map: Parameter name, parameter value in this object
     */
    std::unique_ptr<LongitudinalDispersivityParam<Traits>> clone () const override
    {
        using ThisType = ConstLongitudinalDispersivityParam<Traits>;
        return std::make_unique<ThisType>(*this);
    }

private:
    const std::string _name;
};

} // namespace Parameterizatoion
} // namespace Dune
} // namespace Dorie

#endif // DUNE_DORIE_PARAM_CONST_LONGITUDINAL_DISPERSIVITY_INTERFACE_HH