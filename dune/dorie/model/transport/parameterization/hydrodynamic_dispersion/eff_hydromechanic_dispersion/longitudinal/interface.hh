#ifndef DUNE_DORIE_PARAM_LONGITUDINAL_DISPERSIVITY_INTERFACE_HH
#define DUNE_DORIE_PARAM_LONGITUDINAL_DISPERSIVITY_INTERFACE_HH

#include <functional>
#include <string>
#include <map>

namespace Dune {
namespace Dorie {
namespace Parameterization {

template<class Traits>
class IsotropicEffectiveHydromechanicDispersion;

/*-------------------------------------------------------------------------*//**
 * @brief      Class for longitudinal dispersivity.
 *
 * @ingroup    TransportParam
 * @author     Santiago Ospina De Los Ríos
 * @date       2019
 *
 * @tparam     Traits  the base traits
 */
template <class Traits>
class LongitudinalDispersivityParam
{
private:
    using BaseP = IsotropicEffectiveHydromechanicDispersion<Traits>;

public:

    using WaterFlux = typename BaseP::WaterFlux;
    using WaterContent = typename BaseP::WaterContent;
    using LongitudinalDispersivity = typename BaseP::LongitudinalDispersivity;

    //! The name of this parameterization instance, associated with the layer.
    const std::string _name;

    /// Construct with default-initialized parameters
    /** \param name The name associated with this soil layer
     */
    LongitudinalDispersivityParam (const std::string name) 
        :   _name(name)
    { }

    /// Construct from a tuple of parameters
    /** \param name The name associated with this soil layer
     *  \param parameters Tuple of parameters to use in this parameterization
     */
    template<typename... Args>
    LongitudinalDispersivityParam (
            const std::string name,
            const std::tuple<Args...> parameters)
        :   _name(name)
    { }

    /// Default constructor (virtual).
    virtual ~LongitudinalDispersivityParam () = default;

    /// Return the name of this parameterization instance.
    const std::string& get_name() const { return _name; }

    /// Return a bound version of the longitudinal dispersivity
    /** \return Function: {Water Flux, Water Content} -> Longitudinal Dispersivity
     */
    virtual std::function<
        LongitudinalDispersivity(const WaterFlux, const WaterContent)> 
            longitudinal_dispersivity_f () const = 0;

    /// Return a map referecing all parameters by their names.
    /** \return Map. Key: Name of parameter (string).
     *      Value: Value of parameter (double&)
     */
    virtual std::multimap<std::string, double&> parameters () = 0;

    /// Return a map referecing all parameters by their names.
    /** \return Map. Key: Name of parameter (string).
     *      Value: Value of parameter (const double&)
     */    
    virtual std::multimap<std::string, const double&> parameters () const = 0;

    /// Clone the plymorphic class
    /** \return unique pointer to the cloned object
     */
    virtual std::unique_ptr<LongitudinalDispersivityParam<Traits>> clone () const = 0;
};

} // namespace Parameterization
} // namespace Dune
} // namespace Dorie

#endif // DUNE_DORIE_PARAM_LONGITUDINAL_DISPERSIVITY_INTERFACE_HH