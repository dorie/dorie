#ifndef DUNE_DORIE_PARAM_ISOTROPIC_EFFECTIVE_HYDROMECHANIC_DISPERSION_HH
#define DUNE_DORIE_PARAM_ISOTROPIC_EFFECTIVE_HYDROMECHANIC_DISPERSION_HH

#include <cmath>
#include <functional>
#include <string>

#include <dune/dorie/model/transport/parameterization/hydrodynamic_dispersion/eff_hydromechanic_dispersion/interface.hh>
#include <dune/dorie/model/transport/parameterization/hydrodynamic_dispersion/eff_hydromechanic_dispersion/longitudinal/interface.hh>
#include <dune/dorie/model/transport/parameterization/hydrodynamic_dispersion/eff_hydromechanic_dispersion/transverse/interface.hh>

namespace Dune {
namespace Dorie {
namespace Parameterization {

/*-------------------------------------------------------------------------*//**
 * @brief      Class for isotropic effective hydromechanic dispersion.
 * @details    Implements an interface for the the relation 
 *             @f$ {\mathsf{D}_\mathsf{hm}^\mathsf{eff}}_{ij} = 
 *             [\lamda_l-\lambda_t] \frac{v_iv_j}{|v|}+\lambda_t|v|\delta_ij
 *             where @f$ v:=j_w/\theta @f$ is the water velocity, 
 *             @f$ \delta_ij @f$ the kroneker delta, @f$ \lambda_t @f$ the 
 *             transverse dispersivity, and @f$ \lambda_l @f$ the longitudinal
 *             dispersivity
 *            
 * @ingroup    TransportParam
 * @author     Santiago Ospina De Los Ríos
 * @date       2019
 *
 * @tparam     Traits  the base traits
 */
template <class Traits>
class IsotropicEffectiveHydromechanicDispersion 
    : public EffectiveHydromechanicDispersionParam<Traits>
{
private:
    using RangeField = typename Traits::RF;
    using Tensor = typename Traits::Tensor;
    using Base = EffectiveHydromechanicDispersionParam<Traits>;
    using LambdaT = TransverseDispersivityParam<Traits>;
    using LambdaL = LongitudinalDispersivityParam<Traits>;

public:
    using EffectiveHydromechanicDispersion = typename Base::EffectiveHydromechanicDispersion;
    using WaterFlux = typename Base::WaterFlux;
    using WaterContent = typename Base::WaterContent;

    /// Parameter defining the longinutinal dispersivity
    struct LongitudinalDispersivity
    {
        RangeField value;
    };

    /// Parameter defining the transverse dispersivity
    struct TransverseDispersivity
    {
        RangeField value;
    };

    /// The name of this parameterization type (This is not the instance name)
    static inline std::string type = "Dhm_iso";

    std::shared_ptr<LambdaL> _long_disp;
    std::shared_ptr<LambdaT> _trans_disp;

public:

    /// Construct with polymorphic objects
    /** \param name The name associated with this soil layer
     * \param long_disp The class for the longitudinal dispersivity
     * \param trans_disp The class for the transverse dispersivity
     */
    template<typename... Args>
    IsotropicEffectiveHydromechanicDispersion (
        const std::string name,
        std::shared_ptr<LambdaL> long_disp,
        std::shared_ptr<LambdaT> trans_disp) :
        Base(name),
        _long_disp(long_disp),
        _trans_disp(trans_disp)
    { }

    /// Add default destructor to clarify override
    ~IsotropicEffectiveHydromechanicDispersion () override = default;

    /// Return the effective hydromechanic dispersion
    /** {WaterFlux,WaterContent} -> EffectiveHydromechanicDispersion
     */
    std::function<EffectiveHydromechanicDispersion(const WaterFlux, const WaterContent)> 
    effective_hydromechanic_dispersion_f () const override
    {
        return [this](const WaterFlux water_flux, const WaterContent water_content) {
            const auto lambda_l_f = _long_disp->longitudinal_dispersivity_f();
            const LongitudinalDispersivity lambda_l = lambda_l_f(water_flux,water_content);
            const auto lambda_t_f = _trans_disp->transverse_dispersivity_f();
            const TransverseDispersivity lambda_t = lambda_t_f(water_flux,water_content);
            Tensor D;
            auto difference = (lambda_l.value-lambda_t.value);
            auto v_abs = water_flux.value.two_norm();
            for (int i = 0; i < Traits::dim; ++i)
                for (int j = 0; j < Traits::dim; ++j) {
                    D[i][j] = 0.;
                    if (Dune::FloatCmp::gt(v_abs,0.)) {
                        D[i][j] += difference*water_flux.value[i]*water_flux.value[j]/(v_abs*water_content.value); // !!!
                        if (i==j)
                            D[i][j] += lambda_t.value * v_abs/water_content.value;
                    }
                }
            return EffectiveHydromechanicDispersion{D};
        };
    }

    /// Return a map of parameter names and values for manipulation
    /** \return Map: Parameter name, parameter value in this object
     */
    virtual std::multimap<std::string, double&> parameters () override
    {
        auto lambda_l_param = _long_disp->parameters();
        auto lambda_t_param = _trans_disp->parameters();

        std::multimap<std::string, double&> compsite_map(lambda_l_param);
        compsite_map.insert(lambda_t_param.begin(),lambda_t_param.end());
        
        return compsite_map;
    }

    /// Return a map of parameter names and values for manipulation
    /** \return Map: Parameter name, parameter value in this object
     */
    virtual std::multimap<std::string, const double&> parameters () const override
    {
        auto lambda_l_param = _long_disp->parameters();
        auto lambda_t_param = _trans_disp->parameters();

        std::multimap<std::string, const double&> compsite_map;
        compsite_map.insert(lambda_l_param.begin(),lambda_l_param.end());
        compsite_map.insert(lambda_t_param.begin(),lambda_t_param.end());
        
        return compsite_map;
    }

    /// Return a map of parameter names and values for manipulation
    /** \return Map: Parameter name, parameter value in this object
     */
    std::unique_ptr<EffectiveHydromechanicDispersionParam<Traits>> clone () const override
    {
        using ThisType = IsotropicEffectiveHydromechanicDispersion<Traits>;
        return std::make_unique<ThisType>(this->get_name(),_long_disp->clone(),_trans_disp->clone());
    }
};

} // namespace Parameterization
} // namespace Dune
} // namespace Dorie

#endif // DUNE_DORIE_PARAM_ISOTROPIC_EFFECTIVE_HYDROMECHANIC_DISPERSION_HH