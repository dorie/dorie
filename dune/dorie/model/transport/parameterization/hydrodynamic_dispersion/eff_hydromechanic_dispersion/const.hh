#ifndef DUNE_DORIE_PARAM_CONST_EFF_HM_DISP_HH
#define DUNE_DORIE_PARAM_CONST_EFF_HM_DISP_HH

#include <cmath>
#include <functional>
#include <string>

#include <dune/dorie/model/transport/parameterization/hydrodynamic_dispersion/eff_hydromechanic_dispersion/interface.hh>

namespace Dune {
namespace Dorie {
namespace Parameterization {

/*-------------------------------------------------------------------------*//**
 * @brief      Class for constant effective hydromechanic dispersion.
 *
 * @ingroup    TransportParam
 * @author     Santiago Ospina De Los Ríos
 * @date       2019
 *
 * @tparam     Traits  the base traits
 */
template <class Traits>
class ConstEffectiveHydromechanicDispersionParam :
    public EffectiveHydromechanicDispersionParam<Traits>
{
private:
    using Tensor = typename Traits::Tensor;
    using Base = EffectiveHydromechanicDispersionParam<Traits>;

public:
    using EffectiveHydromechanicDispersion = typename Base::EffectiveHydromechanicDispersion;
    using WaterFlux = typename Base::WaterFlux;
    using WaterContent = typename Base::WaterContent;

    /// Parameter defining the constant hydromechanic dispersion tensor
    struct ConstEffectiveHydromechanicDispersion
    {
        Tensor value;
        inline static const std::string name = "eff_hydromechanic_disp";
    };

    /// The name of this parameterization type (This is not the instance name)
    static inline std::string type = "Dhm_const";

    ConstEffectiveHydromechanicDispersion _const_eff_hydromechanic_dispersion;

public:

    /// Construct with default-initialized parameters
    /** \param name The name associated with this soil layer
     */
    ConstEffectiveHydromechanicDispersionParam (const std::string name) :
        EffectiveHydromechanicDispersionParam<Traits>(name)
    { }

    /// Construct from a tuple of parameters
    /** \param name The name associated with this soil layer
     *  \param parameters Tuple of parameters to use in this parameterization
     */
    template<typename... Args>
    ConstEffectiveHydromechanicDispersionParam (
            const std::string name,
            const std::tuple<Args...> parameters) :
        Base(name, parameters),
        _const_eff_hydromechanic_dispersion(std::get<ConstEffectiveHydromechanicDispersion>(parameters))
    { }

    /// Add default destructor to clarify override
    ~ConstEffectiveHydromechanicDispersionParam () override = default;

    /// Return the hydromechanic dispersion
    /** {WaterFlux,WaterContent} -> EffectiveHydromechanicDispersion
     */
    std::function<EffectiveHydromechanicDispersion(const WaterFlux, const WaterContent)> 
    effective_hydromechanic_dispersion_f () const override
    {
        return [this](const WaterFlux, const WaterContent) {
            return EffectiveHydromechanicDispersion{_const_eff_hydromechanic_dispersion.value};
        };
    }

    /// Return a map of parameter names and values for manipulation
    /** \return Map: Parameter name, parameter value in this object
     */
    std::multimap<std::string, double&> parameters () override
    {
        const std::string _const_eff_hydromechanic_dispersion_name = ConstEffectiveHydromechanicDispersion::name;
        if constexpr (Traits::dim == 2)
            return {
                {_const_eff_hydromechanic_dispersion_name+"_xx", _const_eff_hydromechanic_dispersion.value[0][0]},
                {_const_eff_hydromechanic_dispersion_name+"_xy", _const_eff_hydromechanic_dispersion.value[0][1]},
                {_const_eff_hydromechanic_dispersion_name+"_yx", _const_eff_hydromechanic_dispersion.value[1][0]},
                {_const_eff_hydromechanic_dispersion_name+"_yy", _const_eff_hydromechanic_dispersion.value[1][1]}
            };
        else if constexpr (Traits::dim == 3)
            return {
                {_const_eff_hydromechanic_dispersion_name+"_xx", _const_eff_hydromechanic_dispersion.value[0][0]},
                {_const_eff_hydromechanic_dispersion_name+"_xy", _const_eff_hydromechanic_dispersion.value[0][1]},
                {_const_eff_hydromechanic_dispersion_name+"_xz", _const_eff_hydromechanic_dispersion.value[0][2]},
                {_const_eff_hydromechanic_dispersion_name+"_yx", _const_eff_hydromechanic_dispersion.value[1][0]},
                {_const_eff_hydromechanic_dispersion_name+"_yy", _const_eff_hydromechanic_dispersion.value[1][1]},
                {_const_eff_hydromechanic_dispersion_name+"_yz", _const_eff_hydromechanic_dispersion.value[1][2]},
                {_const_eff_hydromechanic_dispersion_name+"_zx", _const_eff_hydromechanic_dispersion.value[2][0]},
                {_const_eff_hydromechanic_dispersion_name+"_xy", _const_eff_hydromechanic_dispersion.value[2][1]},
                {_const_eff_hydromechanic_dispersion_name+"_zz", _const_eff_hydromechanic_dispersion.value[2][2]}
            };
        else
            static_assert(true,
                "ConstEffectiveHydromechanicDispersion not available for this dimension");
    }

    /// Return a map of parameter names and values for manipulation
    /** \return Map: Parameter name, parameter value in this object
     */
    std::multimap<std::string, const double&> parameters () const override
    {
        const std::string _const_eff_hydromechanic_dispersion_name = ConstEffectiveHydromechanicDispersion::name;
        if constexpr (Traits::dim == 2)
            return {
                {_const_eff_hydromechanic_dispersion_name+"_xx", _const_eff_hydromechanic_dispersion.value[0][0]},
                {_const_eff_hydromechanic_dispersion_name+"_xy", _const_eff_hydromechanic_dispersion.value[0][1]},
                {_const_eff_hydromechanic_dispersion_name+"_yx", _const_eff_hydromechanic_dispersion.value[1][0]},
                {_const_eff_hydromechanic_dispersion_name+"_yy", _const_eff_hydromechanic_dispersion.value[1][1]}
            };
        else if constexpr (Traits::dim == 3)
            return {
                {_const_eff_hydromechanic_dispersion_name+"_xx", _const_eff_hydromechanic_dispersion.value[0][0]},
                {_const_eff_hydromechanic_dispersion_name+"_xy", _const_eff_hydromechanic_dispersion.value[0][1]},
                {_const_eff_hydromechanic_dispersion_name+"_xz", _const_eff_hydromechanic_dispersion.value[0][2]},
                {_const_eff_hydromechanic_dispersion_name+"_yx", _const_eff_hydromechanic_dispersion.value[1][0]},
                {_const_eff_hydromechanic_dispersion_name+"_yy", _const_eff_hydromechanic_dispersion.value[1][1]},
                {_const_eff_hydromechanic_dispersion_name+"_yz", _const_eff_hydromechanic_dispersion.value[1][2]},
                {_const_eff_hydromechanic_dispersion_name+"_zx", _const_eff_hydromechanic_dispersion.value[2][0]},
                {_const_eff_hydromechanic_dispersion_name+"_xy", _const_eff_hydromechanic_dispersion.value[2][1]},
                {_const_eff_hydromechanic_dispersion_name+"_zz", _const_eff_hydromechanic_dispersion.value[2][2]}
            };
        else
            static_assert(Dune::AlwaysFalse<Traits>::value,
                "ConstEffectiveHydromechanicDispersion not available for this dimension");
    }

    /// Return a map of parameter names and values for manipulation
    /** \return Map: Parameter name, parameter value in this object
     */
    std::unique_ptr<EffectiveHydromechanicDispersionParam<Traits>> clone () const override
    {
        using ThisType = ConstEffectiveHydromechanicDispersionParam<Traits>;
        return std::make_unique<ThisType>(*this);
    }
};

} // namespace Parameterization
} // namespace Dune
} // namespaie

#endif // DUNE_DORIE_PARAM_CONST_EFF_HM_DISPERSION_HH