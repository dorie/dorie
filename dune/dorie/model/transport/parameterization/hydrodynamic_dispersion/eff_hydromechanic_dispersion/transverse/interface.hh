#ifndef DUNE_DORIE_PARAM_TRANSVERSE_DISPERSIVITY_INTERFACE_HH
#define DUNE_DORIE_PARAM_TRANSVERSE_DISPERSIVITY_INTERFACE_HH

#include <functional>
#include <string>
#include <map>

namespace Dune {
namespace Dorie {
namespace Parameterization {

template<class Traits>
class IsotropicEffectiveHydromechanicDispersion;

/*-------------------------------------------------------------------------*//**
 * @brief      Class for transverse dispersivity.
 *
 * @ingroup    TransportParam
 * @author     Santiago Ospina De Los Ríos
 * @date       2019
 *
 * @tparam     Traits  the base traits
 */
template <class Traits>
class TransverseDispersivityParam
{
private:
    using BaseP = IsotropicEffectiveHydromechanicDispersion<Traits>;

public:

    using WaterFlux = typename BaseP::WaterFlux;
    using WaterContent = typename BaseP::WaterContent;
    using TransverseDispersivity = typename BaseP::TransverseDispersivity;

    //! The name of this parameterization instance, associated with the layer.
    const std::string _name;

    /// Construct with default-initialized parameters
    /** \param name The name associated with this soil layer
     */
    TransverseDispersivityParam (const std::string name) 
        :   _name(name)
    { }

    /// Construct from a tuple of parameters
    /** \param name The name associated with this soil layer
     *  \param parameters Tuple of parameters to use in this parameterization
     */
    template<typename... Args>
    TransverseDispersivityParam (
            const std::string name,
            const std::tuple<Args...> parameters)
        :   _name(name)
    { }

    /// Default constructor (virtual).
    virtual ~TransverseDispersivityParam () = default;

    /// Return the name of this parameterization instance.
    const std::string& get_name() const { return _name; }

    /// Return a bound version of the transverse dispersivity
    /** \return Function: {Water Flux, Water Content} -> Transverse Dispersivity
     */
    virtual std::function<
        TransverseDispersivity(const WaterFlux, const WaterContent)> 
            transverse_dispersivity_f () const = 0;

    /// Return a map referecing all parameters by their names.
    /** \return Map. Key: Name of parameter (string).
     *      Value: Value of parameter (double&)
     */
    virtual std::multimap<std::string, double&> parameters () = 0;

    /// Return a map referecing all parameters by their names.
    /** \return Map. Key: Name of parameter (string).
     *      Value: Value of parameter (const double&)
     */    
    virtual std::multimap<std::string, const double&> parameters () const = 0;

    /// Return a clone of this object
    /** \return a unique pointer with a copy of this object.
     */
    virtual std::unique_ptr<TransverseDispersivityParam<Traits>> clone () const = 0;
};

} // namespace Parameterization
} // namespace Dune
} // namespace Dorie

#endif // DUNE_DORIE_PARAM_TRANSVERSE_DISPERSIVITY_INTERFACE_HH