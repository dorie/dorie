#ifndef DUNE_DORIE_PARAM_CONST_TRANSVERSE_DISPERSIVITY_INTERFACE_HH
#define DUNE_DORIE_PARAM_CONST_TRANSVERSE_DISPERSIVITY_INTERFACE_HH

#include <functional>
#include <string>
#include <map>

#include <dune/dorie/model/transport/parameterization/hydrodynamic_dispersion/eff_hydromechanic_dispersion/transverse/interface.hh>

namespace Dune {
namespace Dorie {
namespace Parameterization {

/*-------------------------------------------------------------------------*//**
 * @brief      Class for constant transverse dispersivity.
 *
 * @ingroup    TransportParam
 * @author     Santiago Ospina De Los Ríos
 * @date       2019
 *
 * @tparam     Traits  the base traits
 */
template <class Traits>
class ConstTransverseDispersivityParam 
    : public TransverseDispersivityParam<Traits>
{
private:
    using Base = TransverseDispersivityParam<Traits>;
    using RangeField = typename Traits::RF;
    
public:
    using WaterFlux = typename Base::WaterFlux;
    using WaterContent = typename Base::WaterContent;
    using TransverseDispersivity = typename Base::TransverseDispersivity;

    /// Parameter defining the constant transverse dispersivity
    struct ConstTransverseDispersivity
    {
        RangeField value;
        inline static const std::string name = "lambda_t";
    };

    /// The name of this parameterization type (This is not the instance name)
    static inline std::string type = "const";

    ConstTransverseDispersivity _lambda_t;

public:

    /// Construct with default-initialized parameters
    /** \param name The name associated with this soil layer
     */
    ConstTransverseDispersivityParam (const std::string name) 
        :   Base(name)
    { }

    /// Construct from a tuple of parameters
    /** \param name The name associated with this soil layer
     *  \param parameters Tuple of parameters to use in this parameterization
     */
    template<typename... Args>
    ConstTransverseDispersivityParam (
            const std::string name,
            const std::tuple<Args...> parameters) :
        Base(name, parameters),
        _lambda_t(std::get<ConstTransverseDispersivity>(parameters))
    { }

    /// Add default destructor to clarify override
    ~ConstTransverseDispersivityParam () override = default;

    /// Return the transverse dispersivity
    /** {WaterFlux,WaterContent} -> TransverseDispersivity
     */
    std::function<
        TransverseDispersivity(const WaterFlux, const WaterContent)> 
            transverse_dispersivity_f () const override
    {
        return [this](const WaterFlux, const WaterContent) {
            return TransverseDispersivity{_lambda_t.value};
        };
    }

    /// Return a map of parameter names and values for manipulation
    /** \return Map: Parameter name, parameter value in this object
     */
    std::multimap<std::string, double&> parameters () override
    {
        return {{ConstTransverseDispersivity::name,_lambda_t.value}};
    }

    /// Return a map of parameter names and values for manipulation
    /** \return Map: Parameter name, parameter value in this object
     */
    std::multimap<std::string, const double&> parameters () const override
    {
        return {{ConstTransverseDispersivity::name,_lambda_t.value}};
    }

    /// Return a map of parameter names and values for manipulation
    /** \return Map: Parameter name, parameter value in this object
     */
    std::unique_ptr<TransverseDispersivityParam<Traits>> clone () const override
    {
        using ThisType = ConstTransverseDispersivityParam<Traits>;
        return std::make_unique<ThisType>(*this);
    }

private:
    const std::string _name;
};

} // namespace Parameterization
} // namespace Dune
} // namespace Dorie

#endif // DUNE_DORIE_PARAM_CONST_TRANSVERSE_DISPERSIVITY_INTERFACE_HH