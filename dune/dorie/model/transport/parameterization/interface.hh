#ifndef DUNE_DORIE_PARAM_TRANSPORT_INTERFACE_HH
#define DUNE_DORIE_PARAM_TRANSPORT_INTERFACE_HH

#include <functional>
#include <string>
#include <map>

namespace Dune {
namespace Dorie {
namespace Parameterization {

/// Class defining the interface for parameterizations of Transport equation
template <class Traits>
class Transport
{
private:
    using RangeField = typename Traits::RF;
    using Vector = typename Traits::Vector;
    using Tensor = typename Traits::Tensor;

public:
    // These are standard types for all parameterizations of Transport Equation
    /// Type of the hydrodynamic dispersion
    struct HydrodynamicDispersion
    {
        Tensor value;
    };

    /// Type of water content
    struct WaterContent
    {
        RangeField value;
    };

    // Type of water flux
    struct WaterFlux
    {
        Vector value;
    };

    /// Type of microscopic peclet number
    struct Peclet
    {
        RangeField value;
    };

    /// Parameter defining the characteristic length
    struct CharacteristicLength
    {
        CharacteristicLength() {}
        RangeField value;
        inline static const std::string name = "char_length";
    };

    /// Parameter defining the molecluar diffusion
    struct MolecularDiffusion
    {
        MolecularDiffusion() {}
        RangeField value;
        inline static const std::string name = "mol_diff";
    };

    //! Value of the characteristic length.
    CharacteristicLength _char_length;
    //! Value of the molecluar diffusion.
    MolecularDiffusion _mol_diff;
    //! The name of this parameterization instance, associated with the layer.
    const std::string _name;

public:
    /// Construct with default-initialized parameters
    /** \param name The name associated with this soil layer
     */
    Transport (const std::string name) : _name(name)
    { }

    /// Construct from a tuple of parameters
    /** \param name The name associated with this soil layer
     *  \param parameters Tuple of parameters to use in this parameterization
     */
    template<typename... Args>
    Transport (
            const std::string name,
            const std::tuple<Args...> parameters) :
        _char_length(std::get<CharacteristicLength>(parameters)),
        _mol_diff(std::get<MolecularDiffusion>(parameters))
    { }

    /// Default constructor (virtual).
    virtual ~Transport () = default;

    /// Return the name of this parameterization instance.
    const std::string& get_name() const { return _name; }

    /// Return a bound version of the hydrodynamic dispersion tensor
    /** \return Function: {Water Flux, Water Content} -> Hydrodynamic Disp. Tensor
     */
    virtual std::function<
        HydrodynamicDispersion(const WaterFlux water_flux, const WaterContent water_content)> 
            hydrodynamic_dispersion_f () const = 0;

    /// Return a bound version of the microscopic peclet function
    /** \return Function: {Water Flux, Water Content} -> Peclet
     */
    std::function<
        Peclet(const WaterFlux, const WaterContent)> 
            peclet_f () const
    {
        return [this](const WaterFlux water_flux, const WaterContent water_content){
            return Peclet{_char_length.value*water_flux.value.two_norm()/(_mol_diff.value*water_content.value)};
        };
    }

    /// Return a map referecing all parameters by their names.
    /** \return Map. Key: Name of parameter (string).
     *      Value: Value of parameter (double&)
     */
    virtual std::multimap<std::string, double&> parameters ()
    {
        return {
            {_char_length.name, _char_length.value},
            {_mol_diff.name, _mol_diff.value}
        };
    }
    
    /// Return a map referecing all parameters by their names.
    /** \return Map. Key: Name of parameter (string).
     *      Value: Value of parameter (const double&)
     */
    virtual std::multimap<std::string, const double&> parameters () const
    {
        return {
            {_char_length.name, _char_length.value},
            {_mol_diff.name, _mol_diff.value}
        };
    }

    /// Clone the plymorphic class
    /** \return unique pointer to the cloned object
     */
    virtual std::unique_ptr<Transport<Traits>> clone () const = 0;
};

} // namespace Parameterization
} // namespace Dune
} // namespace Dorie

#endif // DUNE_DORIE_PARAM_TRANSPORT_INTERFACE_HH