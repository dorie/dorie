// -*- tab-width: 4; indent-tabs-mode: nil -*-
#ifndef DUNE_DORIE_TRANSPORT_OPERATOR_DG_HH
#define DUNE_DORIE_TRANSPORT_OPERATOR_DG_HH

#include <vector>

#include <dune/geometry/referenceelements.hh>

#include <dune/pdelab/common/quadraturerules.hh>
#include <dune/pdelab/localoperator/defaultimp.hh>
#include <dune/pdelab/localoperator/flags.hh>
#include <dune/pdelab/localoperator/idefault.hh>
#include <dune/pdelab/localoperator/pattern.hh>
#include <dune/pdelab/finiteelement/localbasiscache.hh>

#include <dune/dorie/common/typedefs.hh>
#include <dune/dorie/model/transport/local_operator_cfg.hh>

#include <dune/dorie/common/boundary_condition/dirichlet.hh>

namespace Dune {
namespace Dorie {
namespace Operator {

/**
 * @brief      Spatial local operator for the transport equation in unsaturated
 *             media in a discontinous galerkin scheme.
 * @details    It solves the spatial part of the transport equation:
 *             @f{align*}{
 *             \partial_t[\theta C_w] +
 *             \nabla\cdot [\textbf{j}_w C_w] -
 *             \nabla \cdot [\theta \mathsf{D}_\text{eff}\nabla C_w]&=0
 *                &&\qquad \text{in }
 *             \Omega\\
 *             C_{w, t} &= g &&\qquad \text{on } \Gamma_D
 *             \subseteq\partial\Omega\\
 *             \nabla C_w \cdot \textbf{n} &= \textbf{j}_{\scriptscriptstyle
 *             C_w}&& \qquad \text{on } \Gamma_N =\partial\Omega \backslash
 *             \Gamma_D
 *             @f}
 *
 *             The discretization follows (Di Pietro & Ern 2012, Sect. 4.6.2),
 *             @f{align*}{
 *              a^{\text{swip}, \text{uwp}}(u, v) =
 *                &\sum_{T \in \mathcal{T}_h} \int_T
 *                    \theta \mathsf{D}_\text{eff} \nabla u \cdot \nabla v
 *                  + u_\text{upw} \left[ \mathbf{j}_w \cdot \nabla v \right]
 *                \\
 *                &+ \sum_{F \in \mathcal{F}_h} \int_F \Big[
 *                    \{\!\!\{\theta \mathsf{D}_\text{eff}\nabla
 *                      u\}\!\!\}_\omega \cdot \mathbf{n}_F [\![ v ]\!]
 *                  + [\![ u ]\!] \{\!\!\{\theta \mathsf{D}_\text{eff} \nabla v
 *                    \}\!\!\}_\omega \cdot \mathbf{n}_F
 *                  \Big]
 *               \\
 *               &+ \sum_{F \in \mathcal{F}_h} \int_F
 *                  \left[ \mathbf{j}_w \cdot \mathbf{n}_F \right]
 *                  \{\!\!\{ u \}\!\!\} [\![ v ]\!]
 *               \\
 *               &+ \sum_{F \in \mathcal{F}_h} \int_F
 *                  \left[ \eta \frac{\gamma_{\mathsf{D}, F}}{h_F}
 *                    + \frac{1}{2} \left\lvert \mathbf{j}_w \cdot \mathbf{n}_F
 *                      \right\rvert \right]
 *                  [\![ u ]\!] [\![ v ]\!]
 *               \\
 *               &+ \sum_{F \in \mathcal{F}^b_h} \int_F
 *                  \left[ \mathbf{j}_w \cdot \mathbf{n}_F \right]^\oplus uv
 *             @f}
 *
 * @warning    This operator uses a cache for the local basis function values.
 *             It currently **cannot** be used for computations on grid
 *             function spaces with varying finite elements (changing
 *             polynomial order or element geometry). This would require
 *             separate caches for each element type or polynomial order.
 *
 * @ingroup    TransportModel
 * @ingroup    LocalOperators
 *
 * @tparam     FiniteElementMap  Type of finite element map
 * @tparam     Parameter     Type of class providing parameterization data
 * @tparam     Boundary                Type of the class providing boundary
 *                                     conditions
 * @tparam     GFWaterFlux   Type of a grid function which provides
 *                                     the water flux
 * @tparam     GFWaterContent  Type of a grid function which provides
 *                                     the water content
 */
template<class FiniteElementMap,
         class Parameter,
         class Boundary,
         class GFWaterFlux,
         class GFWaterContent>
class TransportDGSpatialOperator
  : public Dune::PDELab::NumericalJacobianVolume<
      TransportDGSpatialOperator<FiniteElementMap,
                                 Parameter,
                                 Boundary,
                                 GFWaterFlux,
                                 GFWaterContent>>
  , public Dune::PDELab::NumericalJacobianSkeleton<
      TransportDGSpatialOperator<FiniteElementMap,
                                 Parameter,
                                 Boundary,
                                 GFWaterFlux,
                                 GFWaterContent>>
  , public Dune::PDELab::NumericalJacobianBoundary<
      TransportDGSpatialOperator<FiniteElementMap,
                                 Parameter,
                                 Boundary,
                                 GFWaterFlux,
                                 GFWaterContent>>
  , public Dune::PDELab::FullSkeletonPattern
  , public Dune::PDELab::FullVolumePattern
  , public Dune::PDELab::LocalOperatorDefaultFlags
  , public Dune::PDELab::InstationaryLocalOperatorDefaultMethods<double>
{
public:
  // Pattern assembly flags
  enum
  {
    doPatternVolume = true
  };
  enum
  {
    doPatternSkeleton = true
  };

  // Residual assembly flags
  enum
  {
    doAlphaVolume = true
  };
  enum
  {
    doAlphaBoundary = true
  };
  enum
  {
    doAlphaSkeleton = true
  };

private:
  static_assert(
    std::is_same<typename GFWaterFlux::Traits::GridViewType,
                 typename GFWaterContent::Traits::GridViewType>::value,
    "TransportDGSpatialOperator: GFWaterFlux and"
    "GFWaterContent have to use the same grid view.");

  // Define domain field
  using DF = typename GFWaterContent::Traits::DomainFieldType;
  using RF = typename GFWaterContent::Traits::RangeFieldType;

  using WaterFlux = typename GFWaterFlux::Traits::RangeType;
  using WaterContent = typename GFWaterContent::Traits::RangeType;

  using LocalBasisType = typename FiniteElementMap::Traits
                                  ::FiniteElementType::Traits::LocalBasisType;
  /// Type of local basis function cache
  using Cache = Dune::PDELab::LocalBasisCache<LocalBasisType>;

  // Extract world dimension
  enum
  {
    dim = GFWaterContent::Traits::dimDomain
  };
  using Tensor = Dune::FieldMatrix<RF, dim, dim>;

  enum class LOPCase
  {
    DG,
    RTVolume,
    RTSkeleton
  }; //!< Local operator case
public:
  /**
   * @brief      Constructor of TransportDGSpatialOperator
   *
   * @param      boundary          The boundary terms
   * @param[in]  gf_water_flux     The grid function of water flux
   * @param[in]  gf_water_content  The grid function of water content
   * @param[in]  diff_coeff        The diffusion coefficient
   */
  TransportDGSpatialOperator(
    const Dune::ParameterTree& config,
    std::shared_ptr<const Parameter> param,
    std::shared_ptr<const Boundary> boundary,
    TransportDGMethod method = TransportDGMethod::SIPG,
    TransportUpwinding upwinding = TransportUpwinding::fullUpwind,
    TransportDGWeights weights = TransportDGWeights::weightsOn,
    int int_order_add = 2,
    int quadrature_factor = 2)
    : _param(param)
    , _boundary(boundary)
    , _method(method)
    , _upwinding(upwinding)
    , _weights(weights)
    , _int_order_add(int_order_add)
    , _quadrature_factor(quadrature_factor)
    , _penalty_factor(method == TransportDGMethod::OBB
                        ? 0.
                        : config.get<RF>("numerics.penaltyFactor"))
    , _time(0.)
  {
    _theta = 0.; // IIP
    if (_method == TransportDGMethod::SIPG)
    {
      _theta = -1.0;
    }
    else if (_method == TransportDGMethod::NIPG
             or _method == TransportDGMethod::OBB)
    {
      _theta = 1.0;
    }
  }

  /**
   * @brief      Volume integral depending on test and ansatz functions
   *
   * @param[in]  eg    THe entity of the grid
   * @param[in]  lfsu  The ansatz local function space
   * @param[in]  x     The coefficients of the lfsu
   * @param[in]  lfsv  The test local function space
   * @param      r     The view of the residual vector w.r.t lfsu
   *
   * @tparam     EG    The type for eg
   * @tparam     LFSU  The type for lfsu
   * @tparam     X     The type for x
   * @tparam     LFSV  The type for lfsv
   * @tparam     R     The type for r
   */
  template<typename EG, typename LFSU, typename X, typename LFSV, typename R>
  void alpha_volume(const EG& eg,
                    const LFSU& lfsu,
                    const X& x,
                    const LFSV& lfsv,
                    R& r) const
  {
    // Type definitions
    using LocalBasisTraitsU =
      typename LFSU::Traits::FiniteElementType::Traits::LocalBasisType::Traits;
    using LocalBasisTraitsV =
      typename LFSV::Traits::FiniteElementType::Traits::LocalBasisType::Traits;

    using RangeU = typename LocalBasisTraitsU::RangeType;

    using RangeFieldU = typename LocalBasisTraitsU::RangeFieldType;
    using RangeFieldV = typename LocalBasisTraitsV::RangeFieldType;

    using GradientU = Dune::FieldVector<RangeFieldU, dim>;
    using GradientV = Dune::FieldVector<RangeFieldV, dim>;

    // Figure out if this is called from the flux RT egine
    constexpr int dim = EG::Entity::dimension;
    constexpr int dimRangeV = LocalBasisTraitsV::dimRange;
    constexpr LOPCase lopcase =
      (dimRangeV == dim) ? LOPCase::RTVolume : LOPCase::DG;

    // Get polynomial degree
    const int order = lfsu.finiteElement().localBasis().order();
    const int int_order = _int_order_add + _quadrature_factor * order;

    // Retrieve the local bases
    const auto& lfsu_basis = lfsu.finiteElement().localBasis();
    const auto& lfsv_basis = lfsv.finiteElement().localBasis();

    // Get geometrical entity
    const auto& entity = eg.entity();
    const auto geo = entity.geometry();

    // Instantiate objects used in quadrature loop
    // Gradients of test and trial space basis functions
    std::vector<GradientU> tgradphiu(lfsu.size());
    std::vector<GradientV> tgradphiv(lfsv.size());

    // Geometric transformation for gradients
    using JacIT = typename EG::Entity::Geometry::JacobianInverseTransposed;
    JacIT jac;

    // Bind parameterization and retrieve functions
    _param->bind(entity);
    const auto hydrd_disp_f = _param->hydrodynamic_dispersion_f();

    // Loop over quadrature points to integrate over volume
    for (const auto& it : quadratureRule(geo, int_order))
    {
      // Position in local coordinates
      const auto& position = it.position();

      // Evaluate basis functions and gradients
      const auto& phiu = _cache.evaluateFunction(position, lfsu_basis);
      const auto& gradphiu = _cache.evaluateJacobian(position, lfsu_basis);

      // Transform gradients of shape functions to real element
      jac = geo.jacobianInverseTransposed(position);
      for (unsigned int i = 0; i < lfsu.size(); i++)
        jac.mv(gradphiu[i][0], tgradphiu[i]);

      // Discrete gradient sign.
      double dg_sign = 1.;

      // For RT, we assume lfsv have the interior degrees of freedom of a RTk
      if constexpr (lopcase == LOPCase::RTVolume)
      {
        lfsv_basis.evaluateFunction(position, tgradphiv);
        // Piola transformation
        const auto BTransposed = geo.jacobianTransposed(position);
        const auto detB = BTransposed.determinant();
        for (unsigned int i = 0; i < lfsv.size(); i++) {
          GradientV y(tgradphiv[i]);
          y /= detB;
          BTransposed.mtv(y, tgradphiv[i]);
        }
        dg_sign = -1.;
      }
      // For DG, we assume lfsu = lfsv
      else // (lopcase == LOPCase::DG)
      {
        tgradphiv = tgradphiu;
      }

      // Evaluate u and its gradient
      RangeU u = 0.;
      GradientU gradu(0.0);
      for (unsigned int i = 0; i < lfsu.size(); i++) {
        const auto& dof = x(lfsu, i);
        u += dof * phiu[i];
        gradu.axpy(dof, tgradphiu[i]);
      }

      // Evaluate water content
      WaterContent water_content;
      _gf_water_content->evaluate(entity, position, water_content);

      // Evaluate velocity field
      WaterFlux water_flux;
      _gf_water_flux->evaluate(entity, position, water_flux);

      // Compute the hydrodynamic dispersion
      const Tensor hydrd_disp = hydrd_disp_f(water_flux, water_content);

      // Calculate the diffusive flux
      GradientU diff_flux;
      hydrd_disp.mv(gradu, diff_flux);
      diff_flux *= water_content;

      // Integration factor
      const auto factor = it.weight() * geo.integrationElement(position);

      // Integrate volume terms
      for (unsigned int i = 0; i < lfsv.size(); i++)
        r.accumulate(lfsv,
                     i,
                     (diff_flux * tgradphiv[i]
                        - u * (water_flux * tgradphiv[i]))
                     * dg_sign * factor);
    }
  }

  /**
   * @brief      Skeleton integral depending on test and ansatz functions.
   *             Each face is only visited once since this method is symmetric
   *
   * @param[in]  ig      The intersection entity of the grid (inside + outside
   *                     entities)
   * @param[in]  lfsu_i  The inside ansatz local function space
   * @param[in]  x_i     The coefficients of the lfsu_i
   * @param[in]  lfsv_i  The inside test local function space
   * @param[in]  lfsu_o  The outside ansatz local function space
   * @param[in]  x_o     The coefficients of the lfsu_o
   * @param[in]  lfsv_o  The outside test local function space
   * @param      r_i     The view of the residual vector w.r.t lfsu_i
   * @param      r_o     The view of the residual vector w.r.t lfsu_o
   *
   * @tparam     IG      The type for ig
   * @tparam     LFSU    The type for lfsu_i and lfsu_o
   * @tparam     X       The type for x_i and x_o
   * @tparam     LFSV    The type for lfsv_i and lfsv_o
   * @tparam     R       The type for r_i and r_o
   */
  template<typename IG, typename LFSU, typename X, typename LFSV, typename R>
  void alpha_skeleton(const IG& ig,
                      const LFSU& lfsu_i,
                      const X& x_i,
                      const LFSV& lfsv_i,
                      const LFSU& lfsu_o,
                      const X& x_o,
                      const LFSV& lfsv_o,
                      R& r_i,
                      R& r_o) const
  {
    // Type definitions
    using LocalBasisTraitsU =
      typename LFSU::Traits::FiniteElementType::Traits::LocalBasisType::Traits;
    using LocalBasisTraitsV =
      typename LFSV::Traits::FiniteElementType::Traits::LocalBasisType::Traits;

    using RangeU = typename LocalBasisTraitsU::RangeType;
    using RangeV = typename LocalBasisTraitsV::RangeType;

    using RangeFieldU = typename LocalBasisTraitsU::RangeFieldType;
    using RangeFieldV = typename LocalBasisTraitsV::RangeFieldType;

    using GradientU = Dune::FieldVector<RangeFieldU, dim>;
    using GradientV = Dune::FieldVector<RangeFieldV, dim>;

    // Figure out if this is called from the flux RT engine
    constexpr int dim = IG::Entity::dimension;
    constexpr int dimDomainV = LocalBasisTraitsV::dimDomain;
    constexpr int dimRangeV = LocalBasisTraitsV::dimRange;
    constexpr LOPCase lopcase =
      (dimDomainV == dim - 1)
        ? LOPCase::RTSkeleton
        : (dimRangeV == dim ? LOPCase::RTVolume : LOPCase::DG);

    // Get polynomial degree
    const int order_i = lfsu_i.finiteElement().localBasis().order();
    const int order_o = lfsu_o.finiteElement().localBasis().order();
    const int order = std::max(order_i, order_o);
    const int int_order = _int_order_add + _quadrature_factor * order;

    // Retrieve local bases
    const auto& lfsu_basis_i = lfsu_i.finiteElement().localBasis();
    const auto& lfsu_basis_o = lfsu_o.finiteElement().localBasis();

    const auto& lfsv_basis_i = lfsv_i.finiteElement().localBasis();
    const auto& lfsv_basis_o = lfsv_o.finiteElement().localBasis();

    // Retrieve geometric entities
    const auto& entity_f = ig.intersection();
    const auto& entity_i = ig.inside();
    const auto& entity_o = ig.outside();

    const auto geo_f = entity_f.geometry();
    const auto geo_i = entity_i.geometry();
    const auto geo_o = entity_o.geometry();
    const auto geo_in_i = entity_f.geometryInInside();
    const auto geo_in_o = entity_f.geometryInOutside();

    // Instantiate objects used in quadrature loop
    // Gradients of trial space basis function in real element
    std::vector<GradientU> tgradphiu_i(lfsu_i.size());
    std::vector<GradientU> tgradphiu_o(lfsu_o.size());

    // Test space basis functions
    std::vector<RangeV> phiv_i(lfsv_i.size());
    std::vector<RangeV> phiv_o(lfsv_o.size());

    // Gradients of trial space basis function in real element
    std::vector<GradientV> tgradphiv_i(lfsv_i.size());
    std::vector<GradientV> tgradphiv_o(lfsv_o.size());

    // Geometric transformation for gradients
    using JacIT =  typename IG::Entity::Geometry::JacobianInverseTransposed;
    JacIT jac;

    // Bind parameterization and retrieve functions
    _param->bind(entity_i);
    const auto hydrd_disp_f_i = _param->hydrodynamic_dispersion_f();
    _param->bind(entity_o);
    const auto hydrd_disp_f_o = _param->hydrodynamic_dispersion_f();

    // Loop over quadrature points and integrate fluxes
    for (const auto& it : quadratureRule(geo_f, int_order))
    {
      // Position in local coordinates of intersection
      const auto& position_f = it.position();

      // Position in local coordinates of elements
      const auto position_i = geo_in_i.global(position_f);
      const auto position_o = geo_in_o.global(position_f);

      // Face normal vector at position
      const auto normal_f = entity_f.unitOuterNormal(position_f);

      // Evaluate trial space basis functions at both sides
      const auto& phiu_i = _cache.evaluateFunction(position_i, lfsu_basis_i);
      const auto& phiu_o = _cache.evaluateFunction(position_o, lfsu_basis_o);

      // Evaluate trial space gradient of basis functions
      const auto& gradphiu_i = _cache.evaluateJacobian(position_i,
                                                       lfsu_basis_i);
      const auto& gradphiu_o = _cache.evaluateJacobian(position_o,
                                                       lfsu_basis_o);

      // Transform gradients of shape functions to real element
      jac = geo_i.jacobianInverseTransposed(position_i);
      for (unsigned int i = 0; i < lfsu_i.size(); i++)
        jac.mv(gradphiu_i[i][0], tgradphiu_i[i]);
      jac = geo_o.jacobianInverseTransposed(position_o);
      for (unsigned int i = 0; i < lfsu_o.size(); i++)
        jac.mv(gradphiu_o[i][0], tgradphiu_o[i]);

      // For RT, we assume lfsv have the skeleton degrees of freedom of a RTk,
      // that is, a Pk/Qk finite element on codimension 1
      if constexpr (lopcase == LOPCase::RTSkeleton)
      {
        // Evaluate position in local coord of the inside entity of codim 1
        using LocalCoordinate = typename IG::LocalGeometry::LocalCoordinate;
        LocalCoordinate position_i_f, position_o_f;

        if (ig.intersection().conforming()) {
          // intersection <-> inside local coordinate
          position_i_f = it.position();
          position_o_f = it.position();
        } else {
          // intersection has to be transformed to local inside entity coord
          const auto position_g = geo_f.global(position_f);

          const auto id_i_f = entity_f.indexInInside();
          const auto entity_i_f = entity_i.template subEntity<1>(id_i_f);
          const auto geometry_i_f = entity_i_f.geometry();
          position_i_f = geometry_i_f.local(position_g);

          const auto id_o_f = entity_f.indexInOutside();
          const auto entity_o_f = entity_o.template subEntity<1>(id_o_f);
          const auto geometry_o_f = entity_o_f.geometry();
          position_o_f = geometry_o_f.local(position_g);
        }

        // (we assume non-Galerkin method lfsu != lfsv)
        lfsv_basis_i.evaluateFunction(position_i_f, phiv_i);
        lfsv_basis_o.evaluateFunction(position_o_f, phiv_o);
      }
      // For RT lifting, we assume lfsv have the interior degrees of freedom of a RTk
      else if constexpr (lopcase == LOPCase::RTVolume)
      {
        lfsv_basis_i.evaluateFunction(position_i, tgradphiv_i);
        lfsv_basis_o.evaluateFunction(position_o, tgradphiv_o);
        // Piola transformation
        const auto BTransposed_i = geo_i.jacobianTransposed(position_i);
        const auto detB_i = BTransposed_i.determinant();
        for (unsigned int i = 0; i < lfsv_i.size(); i++) {
          GradientV y_i(tgradphiv_i[i]);
          y_i /= detB_i;
          BTransposed_i.mtv(y_i, tgradphiv_i[i]);
        }
        const auto BTransposed_o = geo_o.jacobianTransposed(position_o);
        const auto detB_o = BTransposed_o.determinant();
        for (unsigned int i = 0; i < lfsv_o.size(); i++) {
          GradientV y_o(tgradphiv_o[i]);
          y_o /= detB_o;
          BTransposed_o.mtv(y_o, tgradphiv_o[i]);
        }

      }
      // For DG, we assume lfsu = lfsv
      else
      {
        phiv_i = phiu_i;
        phiv_o = phiu_o;
        tgradphiv_i = tgradphiu_i;
        tgradphiv_o = tgradphiu_o;
      }

      // Evaluate u and gradient of u
      RangeU u_i = 0., u_o = 0.;
      GradientU gradu_i(0.0), gradu_o(0.0);
      for (unsigned int i = 0; i < lfsu_i.size(); i++) {
        const auto& dof = x_i(lfsu_i, i);
        u_i += dof * phiu_i[i];
        gradu_i.axpy(dof, tgradphiu_i[i]);
      }
      for (unsigned int i = 0; i < lfsu_o.size(); i++) {
        const auto& dof = x_o(lfsu_o, i);
        u_o += dof * phiu_o[i];
        gradu_o.axpy(dof, tgradphiu_o[i]);
      }

      // Evaluate water content
      WaterContent water_content_i, water_content_o;
      _gf_water_content->evaluate(entity_i, position_i, water_content_i);
      _gf_water_content->evaluate(entity_o, position_o, water_content_o);

      // Evaluate water flux
      // NOTE: Assuming that field is in H(div) space, we may choose any side
      WaterFlux water_flux;
      _gf_water_flux->evaluate(entity_i, position_i, water_flux);
      const auto water_flux_n = water_flux * normal_f;

      // Compute the effective hydrodynamic dispersion
      const Tensor hydrd_disp_i = hydrd_disp_f_i(water_flux, water_content_i);
      const Tensor hydrd_disp_o = hydrd_disp_f_o(water_flux, water_content_o);

      // Calculate the diffusive flux
      GradientU diff_coeff_i, diff_coeff_o;
      hydrd_disp_i.mv(normal_f, diff_coeff_i);
      hydrd_disp_o.mv(normal_f, diff_coeff_o);

      diff_coeff_i *= water_content_i;
      diff_coeff_o *= water_content_o;

      // Normal components of diffusion coefficients
      const RangeU delta_i = (diff_coeff_i * normal_f);
      const RangeU delta_o = (diff_coeff_o * normal_f);

      // Compute weights
      RangeU omega_i, omega_o;
      if (_weights == TransportDGWeights::weightsOn)
      {
        omega_i = delta_o / (delta_i + delta_o + 1e-20);
        omega_o = delta_i / (delta_i + delta_o + 1e-20);
      }
      else
      {
        omega_i = omega_o = 0.5;
      }

      // Upwinding of solute
      RangeU omega_up_i(0.5), omega_up_o(0.5);
      if (_upwinding == TransportUpwinding::fullUpwind)
      {
        if (water_flux_n >= 0.0) {
          omega_up_i = 1.0;
          omega_up_o = 0.0;
        }
        else {
          omega_up_i = 0.0;
          omega_up_o = 1.0;
        }
      }

      // Integration factor
      const auto factor = it.weight() * geo_f.integrationElement(position_f);

      // jump
      const auto jump = (u_i - u_o) * factor;

      // Numerical fluxes for DG & RT Skeleton
      if constexpr (lopcase != LOPCase::RTVolume)
      {
        // Convection term
        const auto conv_num_flux = (omega_up_i * u_i + omega_up_o * u_o)
                                   * water_flux_n * factor;

        // Diffusion term
        const auto diff_num_flux = -(omega_i * (diff_coeff_i * gradu_i)
                                     + omega_o * (diff_coeff_o * gradu_o))
                                   * factor;
        // Volumes of entities
        const auto volume_o = geo_o.volume();
        const auto volume_f = geo_f.volume();
        const auto volume_i = geo_i.volume();

        // Geometric factor of penalty
        const auto diameter_f =
          std::min(volume_i, volume_o) / volume_f; // Houston!

        // Harmonic average of diffusion coefficients
        const RangeU harmonic_average = 2.0 * delta_i * delta_o
                                        / (delta_i + delta_o + 1e-20);
        // penalty factor
        const auto penalty_factor = (_penalty_factor / diameter_f)
                                    * harmonic_average * order
                                    * (order + dim - 1);

        // Penalty term
        const auto penalty = jump * penalty_factor;

        // Numerical flux
        const auto num_flux = conv_num_flux + diff_num_flux + penalty;

        for (unsigned int i = 0; i < lfsv_i.size(); i++)
          r_i.accumulate(lfsv_i, i, num_flux * phiv_i[i]);
        for (unsigned int i = 0; i < lfsv_o.size(); i++)
          r_o.accumulate(lfsv_o, i, -num_flux * phiv_o[i]);
      }

      // Fluxes for the discrete gradient lifting (Applied in DG and RT Volume cases)
      if constexpr (lopcase != LOPCase::RTSkeleton)
      {
        // Discrete gradient sign: Negative when doing reconstruction
        constexpr int dg_sign = (lopcase == LOPCase::RTVolume) ? -1 : 1;

        // Symmetry term (or discrete gradient lifting term)
        for (unsigned int i = 0; i < lfsv_i.size(); i++)
          r_i.accumulate(lfsv_i,
                         i,
                         jump * dg_sign * _theta * omega_i
                         * (diff_coeff_i * tgradphiv_i[i]));
        for (unsigned int i = 0; i < lfsv_o.size(); i++)
          r_o.accumulate(lfsv_o,
                         i,
                         jump * dg_sign * _theta * omega_o
                         * (diff_coeff_o * tgradphiv_o[i]));
      }
    }
  }

  /**
   * @brief      Boundary integral depending on test and ansatz functions.
   *
   * @param[in]  ig      The intersection entity of the grid (inside + outside
   *                     entities)
   * @param[in]  lfsu_i  The inside ansatz local function space
   * @param[in]  x_i     The coefficients of the lfsu_i
   * @param[in]  lfsv_i  The inside test local function space
   * @param      r_i     The view of the residual vector w.r.t lfsu_i
   *
   * @tparam     IG      The type for ig
   * @tparam     LFSU    The type for lfsu_i
   * @tparam     X       The type for x_i
   * @tparam     LFSV    The type for lfsv_i
   * @tparam     R       The type for r_i
   */
  template<typename IG, typename LFSU, typename X, typename LFSV, typename R>
  void alpha_boundary(const IG& ig,
                      const LFSU& lfsu_i,
                      const X& x_i,
                      const LFSV& lfsv_i,
                      R& r_i) const
  {
    // Type definitions
    using LocalBasisTraitsU =
      typename LFSU::Traits::FiniteElementType::Traits::LocalBasisType::Traits;
    using LocalBasisTraitsV =
      typename LFSV::Traits::FiniteElementType::Traits::LocalBasisType::Traits;

    using RangeU = typename LocalBasisTraitsU::RangeType;
    using RangeV = typename LocalBasisTraitsV::RangeType;

    using RangeFieldU = typename LocalBasisTraitsU::RangeFieldType;
    using RangeFieldV = typename LocalBasisTraitsV::RangeFieldType;

    using GradientU = Dune::FieldVector<RangeFieldU, dim>;
    using GradientV = Dune::FieldVector<RangeFieldV, dim>;

    using JacobianV = typename LocalBasisTraitsV::JacobianType;

    // Figure out if this is called from the flux RT engine
    constexpr int dim = IG::Entity::dimension;
    constexpr int dimDomainV = LocalBasisTraitsV::dimDomain;
    constexpr int dimRangeV = LocalBasisTraitsV::dimRange;
    constexpr LOPCase lopcase =
      (dimDomainV == dim - 1)
        ? LOPCase::RTSkeleton
        : (dimRangeV == dim ? LOPCase::RTVolume : LOPCase::DG);

    // Get polynomial degree
    const int order_i = lfsu_i.finiteElement().localBasis().order();
    const int order = order_i;
    const int int_order = _int_order_add + _quadrature_factor * order;

    // Retrieve local bases
    const auto& lfsu_basis_i = lfsu_i.finiteElement().localBasis();
    const auto& lfsv_basis_i = lfsv_i.finiteElement().localBasis();

    // Retrieve geometric entities
    const auto& entity_f = ig.intersection();
    const auto& entity_i = ig.inside();

    const auto geo_f = entity_f.geometry();
    const auto geo_i = entity_i.geometry();
    const auto geo_in_i = entity_f.geometryInInside();

    // Instantiate objects used in quadrature loop
    // Gradients of trial space basis functions
    std::vector<GradientU> tgradphiu_i(lfsu_i.size());

    // Test space basis functions
    std::vector<RangeV> phiv_i(lfsv_i.size());

    // Gradients of test space basis functions
    std::vector<JacobianV> gradphiv_i(lfsv_i.size());
    std::vector<GradientV> tgradphiv_i(lfsv_i.size());

    // Geometric transformation for gradients
    using JacIT = typename IG::Entity::Geometry::JacobianInverseTransposed;
    JacIT jac;

    // Bind parameterization and retrieve functions
    _param->bind(entity_i);
    const auto hydrd_disp_f_i = _param->hydrodynamic_dispersion_f();

    // Retrieve boundary condition
    const auto bc = _boundary->bc(entity_f);

    // Loop over quadrature points and integrate fluxes
    for (const auto& it : quadratureRule(geo_f, int_order))
    {
      // Position in local coordinates of intersection
      const auto& position_f = it.position();

      // Position in local coordinates of elements
      const auto position_i = geo_in_i.global(position_f);

      // Face normal vector
      const auto normal_f = entity_f.unitOuterNormal(position_f);

      // Evaluate trial space basis functions and gradients
      const auto& phiu_i = _cache.evaluateFunction(position_i, lfsu_basis_i);
      const auto& gradphiu_i = _cache.evaluateJacobian(position_i,
                                                       lfsu_basis_i);

      // Transform gradients of shape functions to real element
      jac = geo_i.jacobianInverseTransposed(position_i);
      for (unsigned int i = 0; i < lfsu_i.size(); i++)
        jac.mv(gradphiu_i[i][0], tgradphiu_i[i]);

      // For RT, we assume lfsv have the skeleton degrees of freedom of a RTk,
      // that is, a Pk/Qk finite element on codimension 1
      if constexpr (lopcase == LOPCase::RTSkeleton)
      {
        // (we assume non-Galerkin method lfsu != lfsv)
        lfsv_basis_i.evaluateFunction(position_f, phiv_i);
      }
      // For RT lifting, we assume lfsv have the interior degrees of freedom of a RTk
      else if constexpr (lopcase == LOPCase::RTVolume)
      {
        lfsv_basis_i.evaluateFunction(position_i, tgradphiv_i);
        // Piola transformation
        const auto BTransposed_i = geo_i.jacobianTransposed(position_i);
        const auto detB_i = BTransposed_i.determinant();
        for (unsigned int i = 0; i < lfsv_i.size(); i++) {
          GradientV y_i(tgradphiv_i[i]);
          y_i /= detB_i;
          BTransposed_i.mtv(y_i, tgradphiv_i[i]);
        }
      }
      // For DG, we assume lfsu = lfsv
      else
      {
        phiv_i = phiu_i;
        tgradphiv_i = tgradphiu_i;
      }

      // integration factor
      const auto factor = it.weight() * geo_f.integrationElement(position_f);

      // Evaluate the boundary condition
      const auto& bc_type = bc->type();
      const auto bc_value = bc->evaluate(_time);

      // Apply Neumann flux
      if (bc_type == BCType::Neumann)
      {
        if constexpr (lopcase != LOPCase::RTVolume)
        {
          // Evaluate flux boundary condition
          const auto& normal_flux = bc_value;

          // Neumann flux term
          for (unsigned int i = 0; i < lfsv_i.size(); i++)
            r_i.accumulate(lfsv_i, i, normal_flux * phiv_i[i] * factor);
        }

        // Continue to next quadrature point
        continue;
      }

      // Evaluate u
      RangeU u_i = 0.;
      for (unsigned int i = 0; i < lfsu_i.size(); i++)
        u_i += x_i(lfsu_i, i) * phiu_i[i];

      // Evaluate water flux
      // NOTE: Assuming that field is in H(div) space, we may choose any side
      WaterFlux water_flux;
      _gf_water_flux->evaluate(entity_i, position_i, water_flux);
      const auto water_flux_n = water_flux * normal_f;

      // Apply Outflow flux
      if (bc_type == BCType::Outflow)
      {
        if constexpr (lopcase != LOPCase::RTVolume)
        {
          // No further contribution to residual
          if (water_flux_n < 0.) {
            continue;
          }

          // Convection term (convective flux + BC flux)
          const auto conv_num_flux = (u_i * water_flux_n + bc_value) * factor;
          for (unsigned int i = 0; i < lfsv_i.size(); i++)
            r_i.accumulate(lfsv_i, i, conv_num_flux * phiv_i[i]);
        }
      }
      // Apply Dirichlet flux
      else if (bc_type == BCType::Dirichlet)
      {
        // Compute gradient of u
        GradientU gradu_i(0.0);
        for (unsigned int i = 0; i < lfsu_i.size(); i++)
          gradu_i.axpy(x_i(lfsu_i, i), tgradphiu_i[i]);

        // Evaluate water content
        WaterContent water_content_i;
        _gf_water_content->evaluate(entity_i, position_i, water_content_i);

        // Compute the effective hydrodynamic dispersion
        const Tensor hydrd_disp_i = hydrd_disp_f_i(water_flux,
                                                   water_content_i);

        // Calculate the diffusive flux
        GradientU diff_coeff_i;
        hydrd_disp_i.mv(normal_f, diff_coeff_i);
        diff_coeff_i *= water_content_i;

        // Upwinding
        RangeU omega_up_i(0.5), omega_up_o(0.5);
        if (_upwinding == TransportUpwinding::fullUpwind)
        {
          if (water_flux_n >= 0.0) {
            omega_up_i = 1.0;
            omega_up_o = 0.0;
          }
          else {
            omega_up_i = 0.0;
            omega_up_o = 1.0;
          }
        }

        // Evaluate Dirichlet condition
        auto g = bc_value;

        // Convert input to total solute if needed
        const auto& bc_dirichlet =
          dynamic_cast<const DirichletBoundaryCondition<RF>&>(*bc);
        if (bc_dirichlet.concentration_type() == SoluteConcentration::total) {
          if (Dune::FloatCmp::gt(water_content_i, WaterContent{ 0. })) {
            g /= water_content_i;
          }
          else {
            g = 0.;
          }
        }

        // Convection term
        const auto conv_num_flux = (omega_up_i * u_i + omega_up_o * g)
                                   * water_flux_n * factor;
        for (unsigned int i = 0; i < lfsv_i.size(); i++)
          r_i.accumulate(lfsv_i, i, conv_num_flux * phiv_i[i]);

        // Jump
        const auto jump = (u_i - g) * factor;

        if constexpr (lopcase != LOPCase::RTVolume)
        {
          // Diffusion term
          const auto diff_num_flux = -(diff_coeff_i * gradu_i) * factor;
          for (unsigned int i = 0; i < lfsv_i.size(); i++)
            r_i.accumulate(lfsv_i, i, diff_num_flux * phiv_i[i]);

          // Volumes of grid entities
          const auto volume_i = geo_i.volume();
          const auto volume_f = geo_f.volume();

          // Geometric factor of penalty
          const auto diameter_f = volume_i / volume_f; // Houston!

          // Harmonic average of diffusion coefficients (here: interior only)
          const RangeU harmonic_average = diff_coeff_i * normal_f;

          // Penalty factor
          const auto penalty_factor = (_penalty_factor / diameter_f)
                                      * harmonic_average * order 
                                      * (order + dim - 1);

          // Penalty term
          for (unsigned int i = 0; i < lfsv_i.size(); i++)
            r_i.accumulate(lfsv_i, i, jump * penalty_factor * phiv_i[i]);
        }

        if constexpr (lopcase != LOPCase::RTSkeleton)
        {
          // Discrete gradient sign: Negative for lifting
          const double dg_sign = (lopcase == LOPCase::RTVolume) ? -1. : 1.;

          // Symmetry term
          for (unsigned int i = 0; i < lfsv_i.size(); i++)
            r_i.accumulate(lfsv_i,
                           i,
                           dg_sign * jump * _theta
                           * (diff_coeff_i * tgradphiv_i[i]));
        }
      }
    }
  }

  /**
   * @brief      Residual contribution of boundary integral
   *
   * @param[in]  ig      The intersection entity of the grid (inside + outside
   *                     entities)
   * @param[in]  lfsv_i  The inside test local function space
   * @param      r_i     The view of the residual vector w.r.t lfsu_i
   *
   * @tparam     IG      The type for ig
   * @tparam     LFSV    The type for lfsv_i and
   * @tparam     R       The type for r_i
   */
  template<typename IG, typename LFSV, typename R>
  void lambda_boundary(const IG& ig, const LFSV& lfsv_i, R& r_i) const
  {}

  /**
   * @brief      Sets the time.
   *
   * @param[in]  t           time of type RangeField
   *
   * @tparam     RangeField  type of the range field
   */
  void setTime(double t)
  {
    _time = t;
    _gf_water_content->setTime(_time);
    _gf_water_flux->setTime(_time);
  }

  /**
   * @brief      Sets the the water content grid function.
   *
   * @param[in]  gf_water_content  The water content grid function
   */
  void set_water_content(std::shared_ptr<GFWaterContent> gf_water_content)
  {
    _gf_water_content = gf_water_content;
  }

  /**
   * @brief      Sets the the water flux grid function.
   *
   * @param[in]  gf_water_content  The water flux grid function
   */
  void set_water_flux(std::shared_ptr<GFWaterFlux> gf_water_flux)
  {
    _gf_water_flux = gf_water_flux;
  }

private:
  const std::shared_ptr<const Parameter> _param;
  const std::shared_ptr<const Boundary> _boundary;
  std::shared_ptr<GFWaterFlux> _gf_water_flux;
  std::shared_ptr<GFWaterContent> _gf_water_content;
  const TransportDGMethod _method;
  const TransportUpwinding _upwinding;
  const TransportDGWeights _weights;
  const int _int_order_add;
  const int _quadrature_factor;
  const double _penalty_factor;
  double _theta;
  double _time;
  Cache _cache;
};

/**
 * @brief      Temporal local operator for the transport equation in unsaturated
 *             media in a discontinuous galerkin scheme.
 * @details    It solves the temporal part of the transport equation:
 * @f{eqnarray*}{
 *             \partial_t[\theta C_w] +
 *             \nabla\cdot [\textbf{j}_w C_w] -
 *             \nabla [\theta \mathsf{D}_{eff}\nabla C_w]=0 &\qquad \text{in }
 *             \Omega\\
 *             C_w = g &\qquad \text{on } \Gamma_D
 *             \subseteq\partial\Omega\\
 *             \nabla C_w \cdot \textbf{n} = \textbf{j}_{\scriptscriptstyle
 *             C_w}& \qquad \text{on } \Gamma_N =\partial\Omega \backslash
 *             \Gamma_D
 * @f}
 * @warning    This operator uses a cache for the local basis function values.
 *             It currently **cannot** be used for computations on grid
 *             function spaces with varying finite elements (changing
 *             polynomial order or element geometry). This would require
 *             separate caches for each element type or polynomial order.
 *
 * @author     Santiago Ospina De Los Ríos
 * @date       2018
 * @ingroup    LocalOperators
 * @ingroup    TransportModel
 *
 * @tparam     FiniteElementMap  Type of the GFS finite element map
 * @tparam     GFWaterContent  Type of a grid function which provides
 *                                       the water content
 */
template<class FiniteElementMap,
         class GFWaterContent>
class TransportDGTemporalOperator
  : public Dune::PDELab::NumericalJacobianVolume<
      TransportDGTemporalOperator<FiniteElementMap, GFWaterContent>>
  , public Dune::PDELab::NumericalJacobianApplyVolume<
      TransportDGTemporalOperator<FiniteElementMap, GFWaterContent>>
  , public Dune::PDELab::FullSkeletonPattern
  , public Dune::PDELab::FullVolumePattern
  , public Dune::PDELab::LocalOperatorDefaultFlags
  , public Dune::PDELab::InstationaryLocalOperatorDefaultMethods<double>
{
private:
  using WaterContent = typename GFWaterContent::Traits::RangeType;

  using LocalBasisType = typename FiniteElementMap::Traits
                                  ::FiniteElementType::Traits::LocalBasisType;
  /// Type of local basis function cache
  using Cache = Dune::PDELab::LocalBasisCache<LocalBasisType>;

public:
  // Pattern assembly flags
  enum
  {
    doPatternVolume = true
  };

  // Eesidual assembly flags
  enum
  {
    doAlphaVolume = true
  };

  /// Constructor
  TransportDGTemporalOperator(const int int_order_add = 2,
                              const int quadrature_factor = 2):
    _int_order_add(int_order_add),
    _quadrature_factor(quadrature_factor),
    _time(0.)
  {}

  /**
   * @brief      Volume integral depending on test and ansatz functions
   *
   * @param[in]  eg    THe entity of the grid
   * @param[in]  lfsu  The ansatz local function space
   * @param[in]  x     The coefficients of the lfsu
   * @param[in]  lfsv  The test local function space
   * @param      r     The view of the residual vector w.r.t lfsu
   *
   * @tparam     EG    The type for eg
   * @tparam     LFSU  The type for lfsu
   * @tparam     X     The type for x
   * @tparam     LFSV  The type for lfsv
   * @tparam     R     The type for r
   */
  template<typename EG, typename LFSU, typename X, typename LFSV, typename R>
  void alpha_volume(const EG& eg,
                    const LFSU& lfsu,
                    const X& x,
                    const LFSV& lfsv,
                    R& r) const
  {
    // Type definitions
    using LocalBasisTraitsU =
      typename LFSU::Traits::FiniteElementType::Traits::LocalBasisType::Traits;

    using RangeU = typename LocalBasisTraitsU::RangeType;

    // Get polynomial degree
    const int order = lfsu.finiteElement().localBasis().order();
    const int int_order = _int_order_add + _quadrature_factor * order;

    // Get geometrical entity
    const auto& entity = eg.entity();
    const auto geo = entity.geometry();

    // Retrieve the local bases
    const auto& lfsu_basis = lfsu.finiteElement().localBasis();

    // Loop over quadrature points to integrate over volume
    for (const auto& it : quadratureRule(geo, int_order))
    {
      // Position in local coordinates
      const auto& position = it.position();

      // Evaluate basis functions
      // NOTE: Assume Galerkin: LFSU = LFSV
      const auto& phiu = _cache.evaluateFunction(position, lfsu_basis);
      const auto& phiv = phiu;

      // Evaluate u
      RangeU u = 0.;
      for (unsigned int i = 0; i < lfsu.size(); i++)
        u += x(lfsu, i) * phiu[i];

      // Evaluate the local water content
      WaterContent water_content;
      _gf_water_content->evaluate(entity, position, water_content);

      // Integration factor
      const auto factor = it.weight() * geo.integrationElement(position);

      // update residual
      for (unsigned int i = 0; i < lfsv.size(); i++)
        r.accumulate(lfsv, i, water_content * u * phiv[i] * factor);
    }
  }

  /**
   * @brief      Sets the time.
   *
   * @param[in]  t           time of type RangeField
   *
   * @tparam     RangeField  type of the range field
   */
  template<class RF>
  void setTime(RF t)
  {
    _time = t;
    _gf_water_content->setTime(_time);
  }

  /**
   * @brief      Sets the the water content grid function.
   *
   * @param[in]  gf_water_content  The water content grid function
   */
  void set_water_content(std::shared_ptr<GFWaterContent> gf_water_content)
  {
    _gf_water_content = gf_water_content;
  }

private:
  std::shared_ptr<GFWaterContent> _gf_water_content;
  const int _int_order_add = 2;
  const int _quadrature_factor = 2;
  double _time;
  Cache _cache;
};

} // namespace Operator
} // namespace Dorie
} // namespace Dune

#endif // DUNE_DORIE_TRANSPORT_OPERATOR_DG_HH
