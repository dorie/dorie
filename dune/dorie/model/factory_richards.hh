#ifndef DUNE_DORIE_MODEL_FACTORY_RICHARDS_HH
#define DUNE_DORIE_MODEL_FACTORY_RICHARDS_HH

#include <dune/dorie/model/factory.hh>

#include <dune/dorie/model/richards/richards.hh>

#ifndef DORIE_RORDER
    #error DORIE_RORDER macro must be defined for Richards factory builds
#else
    static_assert(DORIE_RORDER >= 0,
                  "Negative polynomial orders do not make sense");
    static_assert(DORIE_RORDER <= 6,
                  "DORiE only supports polynomial orders up to 6");
#endif

namespace Dune {
namespace Dorie {

/// A factory for building Richards models
/**
 *  \ingroup ModelRichards
 *  \author Lukas Riedel
 *  \date 2019
 */
struct RichardsFactory : public ModelFactory
{
private:
    using Base = ModelFactory;

    // -- Retrieve typedefs from the base class

    /// An unstructured grid (simplices & rectangles, adaptive)
    template<int dim>
    using UnstructuredGrid = Base::UnstructuredGrid<dim>;

    /// A structured grid (rectangles only)
    template<int dim>
    using StructuredGrid = Base::StructuredGrid<dim>;

    template<class Grid>
    using BaseTraitsSimplex = Base::BaseTraitsSimplex<Grid>;

    template<class Grid>
    using BaseTraitsCube = Base::BaseTraitsCube<Grid>;

    // -- Model specific typedefs

    template<typename Traits>
    using ModRichards = ModelRichards<Traits>;

    template<class Grid, int order>
    using ModRichardsSimplex
        = ModRichards<ModelRichardsTraits<BaseTraitsSimplex<Grid>,
                                               order>>;

    template<class Grid, int order>
    using ModRichardsCube
        = ModRichards<ModelRichardsTraits<BaseTraitsCube<Grid>,
                                               order>>;

    /// Verify that the run-time config matches the compile-time settings
    static void check_settings (const Dune::ParameterTree& config)
    {
        const auto dim = config.get<int>("grid.dimensions");
        const auto fem_order = config.get<int>("richards.numerics.FEorder");

        const auto log = get_logger(log_base);
        if (dim != DORIE_DIM or fem_order != DORIE_RORDER) {
            log->error("Config settings do not match compile-time settings:");
            log->error("  DORIE_DIM: {}, grid.dimensions: {}",
                       DORIE_DIM, dim);
            log->error("  DORIE_RORDER: {}, richards.numerics.FEorder: {}",
                       DORIE_RORDER, fem_order);
            DUNE_THROW(IOError, "Config settings not supported");
        }
    }

public:
    /// Create a Richards Model from input parameters
    static std::shared_ptr<ModelBase> create (
        const Dune::ParameterTree& config,
        Dune::MPIHelper& helper
    )
    {
        check_settings(config);
        std::shared_ptr<ModelBase> mod;

        constexpr int dim = DORIE_DIM;
        const auto [grid_mode, grid_adaptive] = get_grid_config(config);
        auto config_richards = Setup::prep_ini_for_richards(config);

        if (grid_mode == GridMode::gmsh)
        {
            using GridType = UnstructuredGrid<dim>;
            GridCreator<GridType> grid_creator(config, helper);

            #if (DORIE_RORDER > 0)
            mod = std::make_shared<ModRichardsSimplex<GridType, DORIE_RORDER>>(
                    config_richards, grid_creator, helper
            );
            #else
            raise_error_fv_grid();
            #endif
        }
        else // GridMode::rectangular
        {
            if (grid_adaptive) {
                using GridType = UnstructuredGrid<dim>;
                GridCreator<GridType> grid_creator(config, helper);

                #if (DORIE_RORDER > 0)
                mod = std::make_shared<ModRichardsCube<GridType, DORIE_RORDER>>(
                    config_richards, grid_creator, helper
                );
                #else
                raise_error_fv_grid();
                #endif
            }
            // grid not adaptive
            else {
                using GridType = StructuredGrid<dim>;
                GridCreator<GridType> grid_creator(config, helper);

                mod = std::make_shared<ModRichardsCube<GridType, DORIE_RORDER>>(
                    config_richards, grid_creator, helper
                );
            }
        }

        // set policy and return
        const auto adaptivity_policy = get_adaptivity_policy(config);
        mod->set_policy(adaptivity_policy);

        return mod;
    }

};

} // namespace Dorie
} // namespace Dune

#endif // DUNE_DORIE_MODEL_FACTORY_RICHARDS_HH
