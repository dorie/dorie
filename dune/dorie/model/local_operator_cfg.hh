#ifndef DUNE_DORIE_LOP_CFG_HH
#define DUNE_DORIE_LOP_CFG_HH

#include <tuple>
#include <string>

#include <dune/common/exceptions.hh>
#include <dune/common/parametertree.hh>

#include <dune/dorie/common/logging.hh>

namespace Dune {
namespace Dorie {
namespace Operator {

/**
 *  \addtogroup Models
 *  \{
 */

//! Interior penalty type
enum class DGMethod
{
    NIPG, //!< Non-symmetric interior penalty
    SIPG, //!< Symmetric interior penalty (default)
    OBB,  //!< Oden, Babuska, Baumann (no penalty term)
    IIP   //!< Incomplete interior penalty (no symmetry term)
};

//! Gradient weighting
enum class DGWeights
{
  weightsOn, //!< harmonic weighting of flux contributions
  weightsOff //!< no weighting of flux contributions
};

//! Upwinding type
enum class Upwinding
{
    none,      //!< No upwinding
    fullUpwind //!< Upwind
};

/// Read upwinding operator settings from a parameter file
/* \return Upwinding option
 */
inline Operator::Upwinding read_operator_upwinding_settings(
    const Dune::ParameterTree& inifile,
    const std::shared_ptr<spdlog::logger>& log)
{
    const auto upwinding_str
        = inifile.get<std::string>("numerics.upwinding");
    log->debug("  Upwinding: {}", upwinding_str);
    if (upwinding_str == "false")
        return Operator::Upwinding::none;
    else if (upwinding_str == "true")
        return Operator::Upwinding::fullUpwind;
    else {
        log->error("Unknown upwinding setting: {}", upwinding_str);
        DUNE_THROW(Dune::IOError, "Unknown upwinding setting");
    }
}

/// Read DG weights operator settings from a parameter file
/* \return DG weights option
 */
inline Operator::DGWeights read_operator_dg_weights_settings(
    const Dune::ParameterTree& inifile,
    const std::shared_ptr<spdlog::logger>& log)
{
    const auto weights_str = inifile.get<bool>("numerics.DGWeights");
    log->debug("  Harmonic weights for interface fluxes (DG): {}",
               weights_str);
    if (weights_str)
        return Operator::DGWeights::weightsOn;
    else
        return Operator::DGWeights::weightsOff;
}

/// Read DG method operator settings from a parameter file
/* \return DG method option
 */
inline Operator::DGMethod read_operator_dg_method_settings(
    const Dune::ParameterTree& inifile,
    const std::shared_ptr<spdlog::logger>& log)
{
    const auto method_str = inifile.get<std::string>("numerics.DGMethod");
    log->debug("  DG method: {}", method_str);
    if (method_str == "SIPG")
        return Operator::DGMethod::SIPG;
    else if (method_str == "NIPG")
        return Operator::DGMethod::NIPG;
    else if (method_str == "OBB")
        return Operator::DGMethod::OBB;
    else if (method_str == "IIP")
        return Operator::DGMethod::IIP;
    else {
        log->error("Unknown DG method: {}", method_str);
        DUNE_THROW(Dune::IOError, "Unknown DG method");
    }
}

/**
 *  \}
 *  // group Models
 */

} // namespace Operator
} // namespace Dorie
} // namespace Dune

#endif // DUNE_DORIE_LOP_CFG_HH
