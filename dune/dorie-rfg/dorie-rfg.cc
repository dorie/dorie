#ifdef HAVE_CONFIG_H
    #include "config.h"
#endif

#include <sys/types.h>
#include <sys/stat.h>
#include <cstdio>
#include <cerrno>

#include <dune/common/parametertree.hh>
#include <dune/common/parametertreeparser.hh>
#include <dune/common/exceptions.hh>
#include <dune/common/parallel/mpihelper.hh>

#include <dune/randomfield/randomfield.hh>

/// Set up dummy traits required by RandomField (usually supplied by grid)
template<int dimension>
struct GridTraits
{
    enum {dim = dimension};
    using RangeField = double;
    using Scalar = Dune::FieldVector<RangeField, 1>;
    using DomainField = double;
    using Domain = Dune::FieldVector<DomainField, dim>;
};

int main(int argc, char** argv)
{
    try{
        //Initialize Mpi
        Dune::MPIHelper::instance(argc, argv);

        if (argc!=2)
            DUNE_THROW(Dune::IOError,"No parameter file specified!");
        const std::string inifilename = argv[1];

        // Read ini file
        Dune::ParameterTree inifile;
        Dune::ParameterTreeParser ptreeparser;
        ptreeparser.readINITree(inifilename, inifile);
        const unsigned int dim = inifile.get<unsigned int>("grid.dimensions");

        // Attempt to create output directory
        const std::string outputPath
            = inifile.get<std::string>("general.tempDir");
        const int status = mkdir(outputPath.c_str(),
                                 S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
        // allow failure because directory exists
        if (status != 0 && errno != EEXIST)
            DUNE_THROW(Dune::IOError,
                       "Output folder " << outputPath << " not writable. "
                       "Error by system: " << std::strerror(errno));

        // fix output filename to ensure the file is found by Python frontend
        const std::string outputFile = outputPath + "/field";

        // standard values
        inifile["stochastic.anisotropy"] = "axiparallel";

        // extract seed
        const unsigned int seed = inifile.get<unsigned int>("stochastic.seed");

        // Create RFG objects
        if (dim == 2) {
            using Traits = GridTraits<2>;
            Dune::RandomField::RandomField<Traits> field(inifile);
            field.generate(seed);
            field.writeToFile(outputFile);
        }
        else if (dim == 3) {
            using Traits = GridTraits<3>;
            Dune::RandomField::RandomField<Traits> field(inifile);
            field.generate(seed);
            field.writeToFile(outputFile);
        }
        else {
            DUNE_THROW(Dune::NotImplemented,
                       "Only 2 and 3-dimensional fields are supported");
        }

        return 0;
    }

    catch (Dune::Exception &e)
    {
        std::cerr << "Dune reported error: " << e << std::endl;
        return 1;
    }
    catch (std::exception& e)
    {
        std::cerr << "Exception occurred: " << e.what() << std::endl;
        return 1;
    }
    catch (...)
    {
        std::cerr << "Unknown exception thrown!" << std::endl;
        throw;
        return 1;
    }
}
