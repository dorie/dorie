**********
Public API
**********

DORiE's Public API comprises the usage of the compiled program as well as the
main code segments for using instances of DORiE in other (DUNE) modules and
programs.

Command Line API
================

The compiled program and the Docker application are executed via the
:doc:`Command Line Interface </manual/cli>`. The specifications for the
respective configuration files are given in the
:doc:`Config File Guide </manual/config-file>`.

The main routine (`dorie run <config>`) also requires input files for
:ref:`boundary conditions <man-bc_file>` and
:ref:`soil parameters <man-parameter_file>`.

Code API
========

DORiE supplies the `Model` abstract base class.  All other models follow this 
structure. 

ABC Model
---------
.. doxygenclass:: Dune::Dorie::ModelBase
   :members:


Model Traits
------------

The model template requires compile-time type specifications wrapped in a
suitable `Traits` structure.

.. doxygenstruct:: Dune::Dorie::BaseTraits
   :members:
   :undoc-members:

Richards Model
--------------

.. doxygenclass:: Dune::Dorie::ModelRichards
   :members:

Transport Model
---------------

.. doxygenclass:: Dune::Dorie::ModelTransport
   :members:


Coupling Model
--------------

The coupling between Richards and Transport models is done by yet another 
model which is in charge of managing the steps of the two sub-models.

.. doxygenclass:: Dune::Dorie::ModelTransport
   :members:
