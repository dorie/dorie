DORiE Documentation
*******************

Welcome to the DORiE Documentation!

DORiE is a DUNE module for solving the Richards equation, and optionally the
passive transport equation for soil water flow.

The source code is available from the
`public repository <https://ts-gitlab.iup.uni-heidelberg.de/dorie/dorie>`_.
A ready-to-use application is available from
`Docker Hub <https://hub.docker.com/r/dorie/dorie/>`_.

DORiE is free software and licensed under the `GNU General Public License
Version 3 <https://www.gnu.org/licenses/gpl-3.0.en.html>`_. For further
information, refer to the :doc:`markdown/COPYING`.

DORiE complies to `Semantic Versioning <https://semver.org/>`_. Version number
assignment and increment are based on the :doc:`public-api`.

.. topic:: Just getting started?

   Here's some information on how to proceed:

   * The :doc:`/introduction/features` will show you which equations DORiE
     solves and which models it offers.
   * The :doc:`README </markdown/README>` contains information on how to set up
     DORiE on your machine.
   * Use the :doc:`Cook Book </cookbook/index>` to dive right into some
     exemplary simulations!
   * Consult the "Manual" section of you seek in-depth information on the single
     features of DORiE.


.. ifconfig:: not meta_ini_available

    .. warning::
       This documentation was built without the ``dune-testtools`` module.
       Example configuration files might be missing.


.. toctree::
   :maxdepth: 2
   :caption: Getting Started

   introduction/install-prep
   The README <markdown/README>
   introduction/features
   introduction/data-io

.. toctree::
   :maxdepth: 1
   :numbered:
   :caption: Cook Book

   cookbook/index
   cookbook/1-infiltration-sand/tutorial
   cookbook/2-paraview/tutorial
   cookbook/3-random-field/tutorial
   cookbook/restart

.. toctree::
   :maxdepth: 2
   :caption: Manual

   manual/cli
   manual/config-file
   manual/grid
   manual/parameter-file
   manual/bcfile
   manual/initial
   manual/time-steps
   manual/fluxes
   manual/interpolators
   public-api

.. toctree::
   :maxdepth: 1
   :caption: Python Modules
   :glob:

   python/*

.. toctree::
   :maxdepth: 1
   :caption: Miscellaneous

   Contribution Guide <markdown/CONTRIBUTING>
   Code of Conduct <markdown/CODE_OF_CONDUCT>
   Copyright Information <markdown/COPYING>

* :download:`GNU General Public License v3.0 </markdown/LICENSE>`
