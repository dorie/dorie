#
# .. cmake_function:: dorie_install_doc_utils
#
# Installs the software required for building the documentation
# into the `venv`. Resets the cache entry `SPHINX_EXECUTABLE`
# if it was found before and replaces it with the executable
# installed into the `venv`.
#
function(dorie_install_doc_utils)
	# install requirements into dune venv
	set(INSTALL_CMD -m pip install -r ${CMAKE_CURRENT_SOURCE_DIR}/requirements.txt)
	dune_execute_process(COMMAND "${DUNE_PYTHON_VIRTUALENV_EXECUTABLE}" "${INSTALL_CMD}"
		ERROR_MESSAGE "dune_python_install_package: Error installing doc utilities into virtualenv!")

	# find Sphinx in dune venv directory
	unset(SPHINX_EXECUTABLE CACHE)
	find_program(SPHINX_EXECUTABLE
		NAMES sphinx-build
		PATHS ${DUNE_PYTHON_VIRTUALENV_PATH}/bin
		NO_DEFAULT_PATH)
	include(FindPackageHandleStandardArgs)
	find_package_handle_standard_args(
		"Sphinx"
		DEFAULT_MSG
		SPHINX_EXECUTABLE
	)
endfunction()

dorie_install_doc_utils()

add_subdirectory("doxygen")
add_subdirectory("default_files")

file(RELATIVE_PATH
	 CHEATSHEET_PFG_RELPATH
	 ${CMAKE_CURRENT_SOURCE_DIR}/manual
	 ${CMAKE_CURRENT_BINARY_DIR}/default_files/field-parameters.rst)
file(RELATIVE_PATH
	 CHEATSHEET_DORIE_RELPATH
	 ${CMAKE_CURRENT_SOURCE_DIR}/manual
	 ${CMAKE_CURRENT_BINARY_DIR}/default_files/parameters.rst)
configure_file(manual/config-file.rst.in
			   ${CMAKE_CURRENT_SOURCE_DIR}/manual/config-file.rst)


configure_file(conf.py.in conf.py)

add_custom_target(sphinx_html
	COMMAND ${CMAKE_BINARY_DIR}/run-in-dune-env ${SPHINX_EXECUTABLE}
			-T -b html
			-c ${CMAKE_CURRENT_BINARY_DIR} # conf.py dir
			-d ${CMAKE_CURRENT_BINARY_DIR}/_doctrees # doctree pickles dir
			${CMAKE_CURRENT_SOURCE_DIR} # input dir
			${CMAKE_CURRENT_BINARY_DIR}/html # output dir
)

if(TARGET sphinx_html)
  add_dependencies(sphinx_html doxygen_dorie)
  add_dependencies(doc sphinx_html)
endif()
