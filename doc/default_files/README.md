# Configuration File Parameters

DORiE reads several interdependent parameters from INI configuration files
during execution. These parameters are listed in XML files inside this
directory. The `dorie.parscraper` module reads these XML files, tracks where
the respective parameters are queried in the source code, writes default
INI files for users, and creates a documentation of all parameters in RST.

There are four XML files:
* `field-parameters.xml`: The parameter tree used by the Parameter Field
  Generator module.
* `common-parameters.xml`: Parameters used by both models.
* `richards-parameters.xml`: Parameters used by the Richards model only.
* `transport-parameters.xml`: Parameters used by the Transport model only.

Model-exclusive parameter categories are prefixed with the lowercase model
name during processing.

## XML File Hierarchy

The XML files must have the following hierarchy:
```xml
<dorie> -> <category> -> <parameter>
```

Category attributes:
* `name` (required): The name as it appears in the config file.

Parameter attributes:
* `name` (required): The name as it appears in the config file.
* `hidden`: If a parameter is not queried by the DORiE source code itself
    (set to `false` by default).

Parameter tags with text only:
* `definition`: Meaning of the parameter as running text, displayed in docs.
* `suggestion`: Default value in created config files
* `values`: Allowed values, displayed in docs
* `comment`: Extra comment, will only show up in INI config files

Parameter tags with `version` attributes and text:
* `versionadded`: At which version the parameter was added.
* `versionchanged`: At which version the parameter key or behavior changed.
* `deprecated`: At which version the parameter became deprecated.

**Note:** When adding one of the version tags, set the `version` attribute to
`unreleased`. The proper version will be set during release.

All tags are optional. For any tag, the respective text field may be empty.

Text showing up in the docs may include RST markup. Paragraph breaks
may be indicated by two line breaks. This is useful for writing lists.
Parameter tags with `version` attributes strip all line breaks.

### Example XML Parameter Definition

```xml
<dorie>
    <category name="output">
        <parameter name="file" hidden="false">
            <definition>
                The path to write the output file to.

                Note:

                * The folder must already exist.

                * An existing file will be overwritten.
            </definition>
            <values> path </values>
            <suggestion> out.bin </suggestion>
            <comment> Choose an existing path </comment>
            <versionadded version="1.0"> </versionadded>
            <versionchanged version="1.1"> Target folder must exist
            </versionchanged>
        </parameter>
    </category>
</dorie>
```
