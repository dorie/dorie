**********************
Random Field Generator
**********************

DORiE includes the powerful Gaussian random field generator implemented in
`dune-randomfield <https://gitlab.dune-project.org/oklein/dune-randomfield>`_.
We will use this generator for two different use cases to conduct simulations
on heterogeneous media.

Basics
------

The random field generator creates a set of random numbers on a physical
domain. The random variable :math:`X` itself follows a Gaussian (normal)
distribution with mean :math:`\mu = 0` and a selectable variance
:math:`\sigma^2`,

.. math::

    X \sim \mathcal{N}(\mu = 0, \sigma^2)

The spatial distribution is determined by covariance functions and correlation
lengths. Finally, the random number generation itself is pseudo-random, meaning
that it is actually deterministic but appears random with respect to most
benchmarks. The seed of the generator therefore lets one replicate a random
field from an unchanged system configuration.

The random field generator is wrapped into a DORiE Python module which obtains
the field supplied by the generator and can apply several transformations to
it, which are tailored for specific use cases. The module is dubbed "Parameter
Field Generator" (PFG) and takes an INI-configuration file (default:
``parfield.ini``) as argument, whose keys are documented in
:doc:`/manual/config-file`. The module is called via the
:doc:`/manual/cli`.

Use Case: Miller-Similar Medium
-------------------------------

Small-scale heterogeneity in soil media are typically realized via scalings of
the parameterization functions, rather than changes of the individual
parameters themselves. In this section, we will apply
:ref:`Miller scaling <man-miller_scaling>` to a otherwise homogeneous medium.
This scaling is based on geometric similarity: It assumes that the local soil
heterogeneity can be described by a varying lengthscale of the soil grains
and pores.

Field Generation
^^^^^^^^^^^^^^^^

We will alter the work flow of the first tutorial by executing the parameter
field generator (PFG) module in the
:ref:`command line interface <man-cli-commands>` before starting the solver.
The default PFG configuration file is called ``parfield.ini``. Enter a
clean directory and create it together with the other default input files
by using the ``dorie create`` command.

The PFG module writes a dataset into an H5 file. The file may already exist,
in which case a new dataset will be added to it. If a dataset with the intended
name already exists, the script throws an error. Let's choose ``miller`` as
dataset name for the dataset containing the Miller scaling factors:

.. literalinclude:: parfield-miller.mini.in
   :language: ini
   :lines: 9-11
   :emphasize-lines: 3
   :caption: parfield.ini

The scaling factors of typical Miller-similar media are log-normally
distributed. A log-normal distribution is readily obtained from a normal
distribution by applying the exponential function to all values. This is
achieved in the PFG module by applying the ``exponential`` converter to the
data:

.. literalinclude:: parfield-miller.mini.in
   :language: ini
   :lines: 9,12
   :emphasize-lines: 2
   :caption: parfield.ini

Additionally, the exponential converter can apply a scaling to the resulting
distribution based on the variance of the original normal distribution. This
scaling is required to obtain the same mean conductivity as for the
homogeneous medium. The total transformation reads

.. math::

    f = \exp (x - \sigma^2),

where :math:`f` is the final dataset value and :math:`x` the corresponing value
of the Gaussian random field. The variance scaling can be enabled in the PFG
config file:

.. literalinclude:: parfield-miller.mini.in
   :language: ini
   :lines: 14-15
   :emphasize-lines: 2
   :caption: parfield.ini

Execute the field generator via

.. code-block:: bash

    dorie pfg parfield.ini

Simulation
^^^^^^^^^^

We now have to insert the resulting field into the simulation. The
:ref:`parameterization file <man-parameter_file>` of the Richars model
(default name: ``richards_param.yml``) contains a ``scaling`` section which
defaults to ``none``. Insert the YAML template for the
:ref:`Miller scaling <man-miller_scaling>` and specify the file path and
dataset path/name. In our current example, we use the default grid of
:math:`100 \times 100` cells on a :math:`1 \text{m} \times 1 \text{m}` domain
in both the PFG and the simulation, so we can set the interpolation to
``nearest``, and leave out ``offset`` and ``scale`` completely:

.. literalinclude:: richards_param_miller.yml
   :language: yaml
   :lines: 14-20
   :emphasize-lines: 2-7
   :caption: richards_param.yml

Now execute the simulation:

.. code-block:: bash

    dorie run config.ini

The following screenshot shows the saturated conductivity field (left) and the
flux magnitude after 15 time steps (right).

.. image:: https://ts-gitlab.iup.uni-heidelberg.de/dorie/dorie/wikis/uploads/1e2f4028dc1ed54ccaf3d0f9d477911c/miller.png
   :alt: ParaView screenshot of simulation result.

.. admonition:: Input files

  =================  ==========================================================
  PFG Configuration  :download:`parfield.ini <3-parfield-miller.ini>`
  Configuration      :download:`config.ini <3-config-miller.ini>`
  Parameters         :download:`richards_param.yml <richards_param_miller.yml>`
  =================  ==========================================================

Use Case: Heterogeneous Medium
------------------------------

In contrast to the continuous approach of Miller-similarity, soil architecture
can feature sharp material interfaces at the scale of interest. To produce
random structures with this quality, we can use the parameter field generator
(PFG) module to build a :ref:`mapping dataset <man-grid_mapping-dataset>` from
a random field. Every integer entry of this dataset will then determine the
parameter set of the medium at the respective grid cell.

Field Generation
^^^^^^^^^^^^^^^^

To clarify what the resulting field is used for, set the output file and
dataset names,

.. literalinclude:: parfield-het.mini.in
   :language: ini
   :lines: 9-11
   :emphasize-lines: 2-3
   :caption: parfield.ini

To compute the mapping dataset from the Gaussian random field for two media
with indices :math:`i_a` and :math:`i_b`, respectively, the transformation is
achieved by "cutting the distribution in half,"

.. math::

    f =
    \begin{cases}
        i_a \quad \text{if } x > 0, \\
        i_b \quad \text{else}.
    \end{cases}

Notice that the ``variance`` setting has therefore no effect on the resulting
distribution. The converter for this and the indices can be readily selected in
the PFG configuration file:

.. literalinclude:: parfield-het.mini.in
   :language: ini
   :lines: 9,12,13-15
   :emphasize-lines: 2,5
   :caption: parfield.ini

Execute the PFG module,

.. code-block:: bash

    dorie pfg parfield.ini

Simulation
^^^^^^^^^^

In the simulation configuration file (default: ``config.ini``), select the
mapping dataset inside the output H5 file to insert the medium mapping:

.. literalinclude:: config-het.mini.in
   :language: ini
   :lines: 9-11
   :emphasize-lines: 2-3
   :caption: config.ini

Now make sure that the indices chosen for the converter in the ``parfield.ini``
have valid parameter set entries in the
:ref:`parameterization file <man-parameter_file>` of the Richards model. By
default, index 0 maps to ``sand`` and index 1 maps to ``silt``:

.. literalinclude:: ../../default_files/richards_param.yml
   :emphasize-lines: 2-3,14-15
   :language: yaml
   :caption: richards_param.yml

Execute the simulation:

.. code-block:: bash

    dorie run config.ini

The following screenshot shows the water content (left) and the flux magnitude
(right) after 14 time steps.

.. image:: https://ts-gitlab.iup.uni-heidelberg.de/dorie/dorie/wikis/uploads/8ddc2e24341b21100aca6a1037009893/heterogeneous.png
   :alt: ParaView screenshot of simulation result.

.. admonition:: Input files

  =================  ==================================================================
  PFG Configuration  :download:`parfield.ini <3-parfield-heterogeneous.ini>`
  Configuration      :download:`config.ini <3-config-heterogeneous.ini>`
  Parameters         :download:`richards_param.yml </default_files/richards_param.yml>`
  =================  ==================================================================
