# Expand a metaini file into the current binary dir
function(expand_metaini)
    foreach(metaini ${ARGN})
        dune_execute_process(
            COMMAND ${PROJECT_BINARY_DIR}/run-in-dune-env
                    dune_expand_metaini.py
                    --cmake
                    --ini ${metaini}
                    --dir ${CMAKE_CURRENT_BINARY_DIR}
                    --file ${CMAKE_CURRENT_BINARY_DIR}/interface.log
            ERROR_MESSAGE "Error expanding ${metaini}"
        )
    endforeach()
endfunction()

dorie_add_metaini_test(TARGET dorie_example
                       METAINI config-miller.mini.in)
dorie_add_metaini_test(TARGET dorie_example
                       METAINI config-het.mini.in)

# Configure file paths in the config files
configure_file(parfield-miller.mini.in parfield-miller.mini)
configure_file(parfield-het.mini.in parfield-het.mini)

# Expand the meta-ini files into current binary dir
expand_metaini(${CMAKE_CURRENT_BINARY_DIR}/parfield-miller.mini
               ${CMAKE_CURRENT_BINARY_DIR}/parfield-het.mini
)

# Copy inis into source dir for Sphinx to read
file(COPY ${CMAKE_CURRENT_BINARY_DIR}/3-config-miller.ini
          ${CMAKE_CURRENT_BINARY_DIR}/3-parfield-miller.ini
          ${CMAKE_CURRENT_BINARY_DIR}/3-config-heterogeneous.ini
          ${CMAKE_CURRENT_BINARY_DIR}/3-parfield-heterogeneous.ini
     DESTINATION ${CMAKE_CURRENT_SOURCE_DIR}
)

# Copy files required for example runs into build dir
file(COPY richards_param_miller.yml
     DESTINATION ${CMAKE_CURRENT_BINARY_DIR})

# Add PFG calls as tests
_add_test(NAME dorie_example_3_pfg_miller
         COMMAND ${PROJECT_BINARY_DIR}/run-in-dune-env
                 dorie pfg ${CMAKE_CURRENT_BINARY_DIR}/3-parfield-miller.ini)
_add_test(NAME dorie_example_3_pfg_het
          COMMAND ${PROJECT_BINARY_DIR}/run-in-dune-env
                  dorie pfg ${CMAKE_CURRENT_BINARY_DIR}/3-parfield-heterogeneous.ini)

# Call "dorie create" before actual tests
_add_test(NAME dorie_example_3_create
          COMMAND ${PROJECT_BINARY_DIR}/run-in-dune-env
                  dorie create)

# Test dependency tree
set_tests_properties(dorie_example_3_create PROPERTIES FIXTURES_SETUP dorie_3)

set_tests_properties(dorie_example_3_pfg_miller PROPERTIES FIXTURES_SETUP dorie_3_miller)
set_tests_properties(dorie_example_3-config-miller PROPERTIES FIXTURES_REQUIRED "dorie_3;dorie_3_miller")

set_tests_properties(dorie_example_3_pfg_het PROPERTIES FIXTURES_SETUP dorie_3_het)
set_tests_properties(dorie_example_3-config-heterogeneous PROPERTIES FIXTURES_REQUIRED "dorie_3;dorie_3_het")
