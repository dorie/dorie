**************************
ParaView for DORiE Results
**************************

`ParaView`_ is a powerful data analysis and visualization application. Its many
features make for a rather overwhelming GUI. For analyzing the output of DORiE,
we typically only need a small subset of the available tools. This is a quick
introduction on how to use ParaView for first-time users.

Opening an Output File
======================

DORiE prints the output of every time step into a separate ``.vtu`` file
containing the grid information and the data on it. We typically do not want to
open single output files but the time series ``.pvd`` files. This file
references all output files and stores their respective simulation time stamp.

.. note::
   In case of a parallel run, there is one ``.vtu`` output file for each
   processor per time stamp. Additionally, a single ``.pvtu`` file collects
   all ``.vtu`` files for displaying the complete data set. The ``.pvd`` file
   will then reference the ``.pvtu`` files, so opening it gives you the data
   just like for a sequential run.

If you have ParaView installed and associated with the appropriate file types,
you can simply open the file with it by double-clicking the file in the folder
overview. Alternatively, open ParaView, choose *File* > *Open...*, and
select the file, or use the *Open* symbol (1).

The file will appear in the *Pipeline Browser* (2), where you can click on the
eye symbol to enable or disable the display of any object in your currently
selected *View*. You may then need to click *Apply* (3) in the
*Properties View* right below to actually load the data.

.. image:: https://ts-gitlab.iup.uni-heidelberg.de/dorie/dorie/wikis/uploads/bb836687637b4e2763379b04a857c932/1-load_data.png
   :alt: ParaView screenshot on how to load data.


Working with the Direct Data Visualization
==========================================

Inside the *Render View*, the content of your file is now displayed. The 2D or
3D render is interactive: You can drag the view around and rotate it. With the
symbols in the second toolbar (1) you can reset the view direction along
certain axes.

Choosing Datasets
-----------------
Select the dataset you want to evaluate using the dropdown menu (2). If the
dataset is non-scalar (like e.g., ``flux``) you can choose to evaluate certain
components or the magnitude of the quantity.

The visualization type can be chosen with the adjacent dropdown menu (2). Most
appropriate for 2D and 3D datasets are *Surface* and *Volume*, respectively.
You can additionally superimpose the grid structure onto the surface
visualization by choosing *Surface with Edges*.

Color Maps
----------
Use the *Rescale* buttons (3) to rescale the color map to the current data
range, the data range across all time steps, the visible data range, and a
custom range.

Select *Edit Color Map* (4) to open the *Color Map Editor* (5). Here, you can
tick *Use log scale when mapping [...]* to enable a logarithmic color map.
Switch between many preset color maps by clicking on the *Choose preset* button
(7), choosing a color map, and clicking *Apply*. The current color map can be
inverted by clicking the *Invert the transfer functions* button (6).

.. image:: https://ts-gitlab.iup.uni-heidelberg.de/dorie/dorie/wikis/uploads/2a84972a3796dffb4a3cad40b205b622/2-visualization.png
   :alt: ParaView screenshot on selecting datasets and color maps.

Animations
==========

All data displayed typically relates to a single dataset describing a single
moment in simulation time. Use the time controls (1) in the topmost toolbar to
step through the time sequence and play an animation. By default, the animation
mode is set to *Snap To TimeSteps* which displays every dataset for the same
amount of time, independently from the respective time stamp.

To visualize the sequence of datasets on a time axis, open the *Animation View*
(2) by selecting *View* > *Animation View*. For a real time sequence, choose
*RealTime* from the *Mode* dropdown menu. This mode will display the datasets
in the fraction of animation duration corresponding to their respective
simulation time. You can enter the desired total animation duration in seconds
in the *Duration* field (3).

.. image:: https://ts-gitlab.iup.uni-heidelberg.de/dorie/dorie/wikis/uploads/80da25e52379dccb97f1c075fea41be8/3-animation.png
   :alt: ParaView screenshot with animation view opened.

Line Plots
==========

The *Plot Over Line* filter evaluates the dataset across a line and displays
the data in a line plot. Select the filter from the *Filter* menu or the button
(1) in the third toolbar.

You can choose simple locations in the *Properties* window with the
*PlotOverLine* pipeline selected in the *Pipeline Browser*. You can also set
the endpoint coordinates explicitly here. Additionally, the line indicating the
evaluation locations can be dragged around in the *Render View*.

Applying the *Plot Over Line* filter opens a new *Line Chart View* (2) where
the data is displayed. The distance along the line is given on the x-axis and
the respective dataset values are displayed on the y-axis.

With the *Line Chart View* of the line plot and the *PlotOverLine* pipeline
**both** selected, you can choose the variables displayed in the line plot, and
modify their colors and legend names (3).

.. image:: https://ts-gitlab.iup.uni-heidelberg.de/dorie/dorie/wikis/uploads/5b1e19c051bfaa13c883626769ef9651/4-line_plot.png
   :alt: ParaView screenshot displaying a Plot Over Line pipeline.

.. tip::
   You can display multiple *Plot Over Line* pipelines inside a single *Line
   Chart View* by selecting the target view and enabling the desired pipelines
   with the eye symbol in the *Pipeline Browser*.

Exporting Line Plot Data
------------------------
If you want to further analyze the data displayed by a Line Plot, you can
export it into a CSV file. To do so, select the *LineChartView* displaying the
desired data and then select *File* > *Save Data...*. Choose *Comma or Tab
Delimited Files* in the *Files of type* dropdown menu, a destination
file name, and a directory, and click *OK*. If desired, you can change the
output floating point precision in the now opening *Configure Writer* window.
Confirming with *OK* will write the file.

.. tip::
   The resulting CSV file can be loaded into a ``numpy`` data array using the
   `numpy.loadtxt`_ function.

Visualizing Fluxes
==================

A useful tool for visualizing fluxes in a transient situation is the *Glyph*
filter. Apply it by first selecting the dataset in the *Pipeline Browser* and
then choosing the filter from the *Filter* menu or the symbol (1) in the third
toolbar. The glyphs will be superimposed onto the visualization in the *Render
View*.

In the *Properties* window (2), select ``flux`` or ``flux_RTx`` (if enabled) as
*Orientation Array* **and** *Scale Array*, and choose *Scale by Magnitude* as
the *Vector Scale Mode* (3). Since the magnitues are quite low, you will likely
see no glyphs at this time. Choose a *Scale Factor*  in the order of ``1E4`` to
get arrows with reasonable extensions (3).

Additionally, you can choose the flux magnitude or any other data for coloring
the glyphs in the *Coloring* sub-menu in the *Properties* window (2). You can
access the *Color Map Editor* for the glyphs like for any other dataset.

.. image:: https://ts-gitlab.iup.uni-heidelberg.de/dorie/dorie/wikis/uploads/5d2bd749b24e0d0965b5847b2c2c2a47/5-glyph.png
   :alt: ParaView screenshot displaying the use of glyphs.

.. _ParaView: https://www.paraview.org/
.. _numpy.loadtxt: https://docs.scipy.org/doc/numpy/reference/generated/numpy.loadtxt.html
