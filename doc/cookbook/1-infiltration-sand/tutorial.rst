**********************************
Infiltration in Homogeneous Medium
**********************************

One of the most simple DORiE simulations is the case of constant infiltration on
a 2D homogeneous medium. In particular, we will create a similar simulation to
the one conducted by Kurt Roth in his
`Soil Physics Lecture Notes <http://ts.iup.uni-heidelberg.de/teaching/#c520>`_,
Chapter 6.3.1.

Study Case
----------

Consider a uniform and isotropic soil with constant water table
(:math:`h_m = 0\,\text{m}`) at height :math:`y=0\,\text{m}` and vertical flux
in the vadose zone. We choose the :math:`y`-axis to point upwards.
For times :math:`t < 0`, the water phase is in static equilibrium, i.e.,
:math:`j_w = 0\,\text{ms}^{-1}` in the entire domain. The soil surface is
located at :math:`y=4\,\text{m}`. For :math:`t \leq 0`, the water flux through
the surface boundary is set to
:math:`j_w = 5.56 \cdot 10^{−6}\,\text{ms}^{-1} = 20\,\text{mm}\,\text{h}^{-1}`,
equivalent to heavy rainfall.

Configure DORiE Input Files
---------------------------

In what follows, we will set up input files and run the simulation step-by-step
reviewing the most important parts of the DORiE work-flow.

Simulation Mode
^^^^^^^^^^^^^^^
The first decision to make is to choose the mode of your simulation. In this
case, we are only interested in the water flow movement. Hence, the richards
mode in the configuration file is well suited for our purpose.

.. literalinclude:: tutorial-1.mini.in
   :language: ini
   :lines: 9-10
   :caption: config.ini

Grid Creation
^^^^^^^^^^^^^
For any simulation, the :doc:`grid settings </manual/grid>` have to be
setup exactly once since all models share the same grid.

In this case, we will create a ``rectangular`` grid by specifying the number
of ``cells`` and the ``extensions`` of the domain. Grids of this type are
directly created within DORiE, thus, the keyword ``gridFile`` is ignored.

.. note::
  The original simulation is one dimensional, but DORiE only supports two and
  three dimensions. Hence, we use a 2D simulation with 1 cell for the
  :math:`x`-direction, as the simulation is symmetrical along this axis.

We set ``1 4`` as the ``extensions`` of the domain. This means that the
rectangular grid will be generated with an extension of :math:`1\,\text{m}` in
the :math:`x`-direction and an extension of :math:`4\,\text{m}` in
the :math:`y`-direction. Notice that the :math:`x`-direction points to the
right, and the :math:`y`-direction upwards. The point of origin in DORiE's
reference frame is located at the lower left point.

Now we have to fill a domain of this size with rectangular grid cells by
specifying the number of cells into each direction. For the
:math:`x`-direction, we will set this to ``1``. For the :math:`y`-direction,
we choose a reasonable resolution of :math:`2\,\text{cm}` per cell, meaning
that we need ``200`` cells in total. That is, we set the pair ``1 200`` of
``cells`` in the config file.

.. literalinclude:: tutorial-1.mini.in
   :language: ini
   :lines: 12-16
   :caption: config.ini

Soil Parameterization
^^^^^^^^^^^^^^^^^^^^^

First, in the configuration file we set the
:ref:`parameterization file <man-parameter_file>` that we want to use. In this
case, we select the file ``richards_param.yml`` provided by the
``dorie create`` command.

.. literalinclude:: tutorial-1.mini.in
   :language: ini
   :lines: 21-22
   :caption: config.ini

Now, for homogeneous materials, the key ``grid.mapping.volume`` in the config
file refers to the keyword ``index`` to use in the parameterization file for
the whole domain. That said, if the parameterization file looks like this:

.. literalinclude:: ../../default_files/richards_param.yml
   :emphasize-lines: 2-3,14-15
   :language: yaml
   :caption: richards_param.yml

then, a ``volume`` set to ``0`` will have a parameterization for ``sand`` while
a ``volume`` set to ``1`` will have a parameterization for
``silt``. For now, let's say we want to simulate an homogeneous sand.

.. literalinclude:: tutorial-1.mini.in
   :language: ini
   :lines: 18-19
   :caption: config.ini

Initial Condition
^^^^^^^^^^^^^^^^^
The :doc:`initial condition </manual/initial>` can be fed with HDF5 data
files or with analytic functions. In this case, we set an initial
condition with the water table at :math:`y = 0\,\text{m}` with a fluid phase in
hydrostatic equilibrium. This can be represented by an ``analytic`` function
where the ``matricHead`` is simply set to ``-h``. See the documentation of
:ref:`analytic initial conditions <man-initial-types>` for details.

.. literalinclude:: tutorial-1.mini.in
   :language: ini
   :lines: 24-27
   :caption: config.ini

Boundary Condition
^^^^^^^^^^^^^^^^^^

The :doc:`boundary conditions file </manual/bcfile>` has to be specified by the
keyword ``richards.boundary.file`` in the configuration file. For now, we will
use the boundary condition file ``richards_bc.yml`` provided by the command
``dorie create``. By default, this file sets a constant infiltration of
:math:`j_w = -5.55\cdot 10^{-6}\,\text{ms}^{-1}` at the top, a constant matric
head of :math:`h_m = 0\,\text{m}` at the bottom, and a no-flux condition on the
sides of the domain.

.. literalinclude:: tutorial-1.mini.in
   :language: ini
   :lines: 29-30
   :caption: config.ini

Output
^^^^^^

Finally, we give a name and a path to the
:doc:`output files </introduction/data-io>` of the simulation:

.. literalinclude:: tutorial-1.mini.in
   :language: ini
   :lines: 32-34
   :caption: config.ini

Run DORiE
---------

Once everything is set up, we :doc:`run DORiE </manual/cli>` by calling the
command ``dorie run`` followed by the configuration file ``confing.ini``:

.. code-block:: bash

    dorie run config.ini

By doing this, the simulation should start and provide information about the
status of the simulation depending on the on the ``logLevel`` keyword on the
configuration file. A typical DORiE simulation has the following output:

.. code-block:: none

  [18:27:32.762 I]  Starting DORiE
  [18:27:32.762 I]  Reading configuration file: config.ini
  [18:27:32.768 I]  Creating output directory: ./
  [18:27:32.768 I]  Creating a rectangular grid in 2D
  [18:27:32.776 I] [RIC]  Loading parameter data file: richards_param.yml
  [18:27:32.779 I] [RIC]  Reading boundary condition data file: richards_bc.yml
  [18:27:32.784 I] [RIC]  Setup complete
  [18:27:33.296 I] [RIC]  Time Step 0: 0.00e+00 + 1.00e+01 -> 1.00e+01
  [18:27:33.581 I] [RIC]  Time Step 1: 1.00e+01 + 1.50e+01 -> 2.50e+01
  [18:27:33.899 I] [RIC]  Time Step 2: 2.50e+01 + 2.25e+01 -> 4.75e+01
  [18:27:34.177 I] [RIC]  Time Step 3: 4.75e+01 + 3.38e+01 -> 8.12e+01
  ...
  [18:27:46.863 I] [RIC]  Time Step 51: 7.67e+05 + 1.00e+05 -> 8.67e+05
  [18:27:46.894 I] [RIC]  Time Step 52: 8.67e+05 + 1.00e+05 -> 9.67e+05
  [18:27:46.923 I] [RIC]  Time Step 53: 9.67e+05 + 3.34e+04 -> 1.00e+06
  [18:27:46.938 I]  DORiE finished after 1.47e+01s :)

Results
-------

The :ref:`results <intro-io-output>` should have been written in several
:file:`.vtu` files, one for each time step, and gathered by a :file:`.pvd`
file. By opening the latter in Paraview_ (or VisIt_) it is possible to
visualize the dynamics of the matric head, water content, and water flux as
shown below.

  .. _Paraview: http://www.paraview.org/
  .. _VisIt: https://visit.llnl.gov/

.. image:: result.gif

.. admonition:: Input files

  =============  ======================================================================
  Configuration  :download:`config.ini <config.ini>`
  Boundary       :download:`richards_bc.yml </default_files/richards_bc.yml>`
  Parameters     :download:`richards_param.yml </default_files/richards_param.yml>`
  =============  ======================================================================
