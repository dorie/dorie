Restart
=======

.. warning:: This document is work in progress (WIP).

Currently, DORiE does not have an option to restart simulations. However, it
is possible to make a pseudo-restart from a past simulation using
:doc:`initial conditions </manual/initial>` from output data files.

Pseudo-restart
--------------

Since this is not a proper restart, it is necessary to setup the simulation so
that it will start a new one from the data of the last simulation. This
includes to modify the starting time and the initial condition.

* ``time.start``:
  Set the last time written by the restarted simulation.

* Initial condition:
  It is not possible to use VTK files directly as initial condition, however
  it is possible to transfer such data into HDF5 file which are accepted by
  DORiE. A possible script to make such transfer is the following one:

  .. todo:: create python script to write vtk data into h5 files.

* ``output.outputPath`` and ``output.fileName``:
  Output path and filename must be changed from last simulation, otherwise,
  data will be overwritten.

* Merge pvd files:
  The old simulation and the pseudo-reset simulation will output different
  pdv files because they are independent. However, merging pdv files so that
  they are shown in paraview as one simulation is an easy task.

  1. Open both pdv files and identify the data sets corresponding to each
     time-step.
  2. Copy the data sets into a new pdv file.

    Output file from the first simulation:

    .. code-block:: xml

      <?xml version="1.0"?>
      <VTKFile type="Collection" version="0.1" byte_order="LittleEndian">
      <Collection>
      <DataSet timestep="0" group="" part="0" name="" file="old_simulation/old_simulation-00000.vtu"/>
      </Collection>
      </VTKFile>

    Output file from the second simulation:

    .. code-block:: xml

      <?xml version="1.0"?>
      <VTKFile type="Collection" version="0.1" byte_order="LittleEndian">
      <Collection>
      <DataSet timestep="100" group="" part="0" name="" file="restarted/restarted_simulation-00000.vtu"/>
      </Collection>
      </VTKFile>

    Merged file:

    .. code-block:: xml

      <?xml version="1.0"?>
      <VTKFile type="Collection" version="0.1" byte_order="LittleEndian">
      <Collection>
      <DataSet timestep="0" group="" part="0" name="" file="old_simulation/old_simulation-00000.vtu"/>
      <DataSet timestep="100" group="" part="0" name="" file="restarted/restarted_simulation-00000.vtu"/>
      </Collection>
      </VTKFile>

Notice that this qualifies as a pseudo-restart because the degrees of freedom
of the last simulation are not completely recovered. Indeed, they are
degenerated! Hence, strictly speaking, a pseudo-restart will lead to different
results compared with respect to a one-run simulation.

.. todo:: Add files
.. todo:: Add test
