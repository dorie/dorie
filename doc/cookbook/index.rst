Introduction
************

This part of the documentation guides through increasingly complicated
use-cases of DORiE. It is intended for users who are using DORiE for the first
time. It explains the usage of the program, how to execute a simulation and how
to analyze its results. The relevant manual pages will be linked on the way.

Prerequisites
=============

You need a working application. You can either use the image shipped via
`Docker Hub <https://hub.docker.com/r/dorie/dorie/>`_ or use a local
installation. See the :doc:`installation manual </markdown/README>` for
details. Additionally, install Paraview_ for analyzing the output.

.. _Paraview: http://www.paraview.org/download/

Setup DORiE virtual environment
===============================

Before of any proper calculation, it is necessary to set up the DORiE virtual
environment in your terminal. This will allow you to call the DORiE commands
anywhere in your system. For this, follow the instructions in the
:doc:`command line interface documentation </manual/cli>`.

Once ready, create a directory where you want to perform the simulations.
For instance

.. code-block:: bash

  mkdir $HOME/Documents/dorie/ex1
  cd $HOME/Documents/dorie/ex1

Create DORiE Input Files
========================

DORiE needs multiple :doc:`input files </introduction/data-io>` to work.
Although these files seem to be quite overwhelming at the beginning, you will
notice that most of their parameters will not be modified in most of the
cases. Now, you can find an example of these input files using the command

.. code-block:: bash

    dorie create


which will provide the files for a simple DORiE application in your
current folder. Explore them!

.. tip::
  Most recipes in this cookbook provide a complete set of input files for the
  specified simulation. You will find them at the end of the recipe in blocks
  as the one below.

.. admonition:: Input files

  =============  ======================================================================
  Configuration  :download:`config.ini </default_files/config.ini>`
  Boundary       :download:`richards_bc.yml </default_files/richards_bc.yml>`,
                 :download:`transport_bc.yml </default_files/transport_bc.yml>`
  Parameters     :download:`richards_param.yml </default_files/richards_param.yml>`,
                 :download:`transport_param.yml </default_files/transport_param.yml>`
  =============  ======================================================================
