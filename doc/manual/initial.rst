Initial Conditions
==================

.. contents::
    :depth: 3
    :local:

DORiE computes transient solutions, and hence needs a solution from which it
can start. There are multiple ways of specifying an initial condition.
All have in common that the data provided must be interpolated on the
respective solution grid function space. Depending on the actual input data,
this means that information can be lost and specific features can be distorted.
Users are responsible to ensure that the solution grid function space and the
input data for initial conditions match in this sense.

Initial conditions can generally be stated in several physical quantities, as
long as the respective quantity has a unique transformation to the solver
solution quantity.

Initial condition input is controlled entirely via the
:doc:`Configuration File <config-file>`.

.. note::
   The initial condition is projected onto the actual solution function space.
   Depending on grid shape and resolution, function space (order), and
   interpolator (if applicable), the resulting solution may vary greatly from
   the actual input data.

.. _man-initial-types:

Input Types
-----------
This is an overview of all input types for initial conditions.
They are controlled by the ``initial.type`` key and shared between all models
unless otherwise specified.

Analytic
^^^^^^^^

* ``type = analytic``

An analytic function :math:`f(\vec{p})` which depends on the physical
position :math:`\vec{p}`. The function must be defined via the key
``initial.equation``. For parsing the input expression, we use muparser_
which supports a set of common mathematical functions. Additionally, the
following variables can be used:

Available variables:
  * ``x``: X-coordinate :math:`p_1 \, [\mathrm{m}]`.
  * ``y``: Y-coordinate :math:`p_2 \, [\mathrm{m}]`.
  * ``z``: Z-coordinate :math:`p_3 \, [\mathrm{m}]` (only in 3D).
  * ``h``: Height above origin. Synonymous to ``y`` in 2D and ``z`` in 3D.
  * ``pi``: Mathematical constant :math:`\pi`.
  * ``dim``: Number of physical dimensions.

.. tip::
    Assuming the target quantity is the matric head (see
    :ref:`initial-transformation`), typical initial conditions for a
    Richards model are

    * Hydrostatic equilibrium: A vertical gradient of :math:`-1` and a
      fixed value ``<v>`` at height :math:`h = 0 \, \mathrm{m}`::

        initial.equation = -h + <v>

    * Gravity flow: Constant value.

.. tip::
    The expression for a gaussian pulse of solute concentration centered at
    :math:`\vec{p} = [0.5, 0.5]^T \, \mathrm{m}` and variance of
    :math:`\sigma^2 = \left( 0.1 \, \mathrm{m} \right)^2` is::

      initial.equation = <m> * exp(-((x-0.5)^2+(y-0.5)^2)/(2.*0.1^2)) / (2*pi*0.1^2)

    where ``<m>`` is the total solute mass of the pulse
    :math:`m_s \, [\text{kg}]`.

Dataset
^^^^^^^

* ``type = data``

Load the initial condition from a data file ``initial.file`` by opening the
dataset ``initial.dataset`` inside this file. The data is interpreted as
function :math:`f(\mathbf{p})` of the physical position :math:`\mathbf{p}`
using one of the available :ref:`interpolators <man-interpolators>`, which
can be chosen using the setting ``initial.interpolation``. The input data
is automatically streched to match the grid extensions.

.. note:: For ``FEorder > 0``, linear interpolation is recommended.

Supported file extensions:

* ``.h5``: H5_ data file. ``initial.dataset`` may be a file-internal path
  to the target dataset.

.. _initial-type-stationary:

Stationary (Richards only)
^^^^^^^^^^^^^^^^^^^^^^^^^^

* ``type = stationary``

Compute the stationary part of the Richards equation,

.. math::

    - \nabla \cdot \left[ K \left[ \nabla h_m - \mathbf{\hat{g}} \right]
                    \right] = 0 \ ,

and use the solution as initial condition for the transient simulation. The
boundary conditions at the simulation start time are used to solve the
problem. No further input is needed, but the boundary condition file might
need to be adjusted.

.. note:: As this computes the actual solution, no transformation is applied.
          The config file key ``initial.quantity`` is ignored.

.. tip:: If you want the simulation to start with different boundary
         conditions than those used for computing the stationary initial
         condition, specify boundary conditions that *end* with the simulation
         *start time* and start at the same time or earlier, like so (assuming
         the simulation starts at :math:`t=0\,\text{s}`):

         .. code:: yaml

            upper:
              index: 1
              conditions:
                # Applied for computing the initial condition
                for_ic:
                  type: Neumann
                  value: -5e-6
                  time: [0.0, 0.0]
                # Applied during the entire transient simulation
                for_sim:
                  type: Dirichlet
                  value: -6
                  time: 0.0

.. _initial-transformation:

Transformation Types
--------------------
This is an overview of the transformation types of all models.
They are controlled via the ``initial.quantity`` key.

Richards
^^^^^^^^
Initial condition tranformations for the Richards solver.

.. object:: No Transformation

    * ``quantity = matricHead``

    The input data is directly interpreted as matric head
    :math:`f = h_m \, [\text{m}]`.

.. object:: Water Content to Matric Head

    * ``quantity = waterContent``

    The input data is interpreted as water content,
    :math:`f = \theta_w \, [\text{-}]`, and transformed into matric head via
    the :doc:`parameterization <parameter-file>` of the medium.

    Values greater than the porosity :math:`\phi` and less than the residual
    water content :math:`\theta_r` are automatically clamped to fit the allowed
    range. Additionally, any input value :math:`f(x_0)` at some position
    :math:`x_0` on the grid will result in a saturation greater zero,
    :math:`\Theta (x_0) > 0`, to avoid divergence of the matric head towards
    negative infinity.

Transport
^^^^^^^^^
Initial condition tranformations for the Transport solver.

.. object:: No Transformation

    * ``quantity = soluteConcentration``

    The input data is directly interpreted as solute concentration,
    :math:`f = c_w [\mathrm{kg}/\mathrm{m}^d]`, where :math:`d` indicates the
    spatial dimensions of the grid.

.. _H5: https://www.h5py.org/
.. _muparser: http://beltoforion.de/article.php?a=muparser&p=features
