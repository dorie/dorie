.. _man-interpolators:

Interpolators
=============

Interpolators are used at several points in DORiE to evaluate discrete input
data at every physical location on the grid.

Currently, interpolators are used in the
:doc:`Parameter File <parameter-file>`, and when loading an initial condition
from data with the options given in the
:doc:`Configuration File <config-file>`.

.. note:: The type of interpolator may change how the input data is
          interpreted. In particular, different interpolators require
          different input dataset shapes for the same grid configuration.

Interpolator Types
------------------

Every interpolator assumes that the input data spans a rectangle (2D) or cuboid
(3D). In case of initial conditions, the respective volume is streched to cover
the entire grid. For scaling fields, users may specify extensions and offset of
the volume, or opt for it to cover the entire grid by omitting the respective
keys in the parameter file.

.. object:: Nearest neighbor interpolator

    * ``interpolation: nearest``

    Interprets dataset values as *cell values*. No extrapolation is applied.
    The lowest supported dataset dimensions are ``(1, 1)`` (single value
    everywhere).

    Use this interpolator for cell-centered data like
    :ref:`scaling field values <man-parameter_scaling>` and initial conditions
    for a finite volume solver.

    .. tip:: To provide the exact scaling factors for each cell of a grid
             of :math:`1 \times 10` cells, use the ``nearest`` interpolator
             with a dataset of shape ``(10, 1)`` (dimensions are inverted).
             The dataset value are the function values at the cell barycenters.

.. object:: Linear interpolator

    * ``interpolation: linear``

    Interprets dataset values as *vertex values* and linearaly interpolates
    between them. No extrapolation is applied. The lowest supported dataset
    dimensions are ``(2, 2)`` (one value in each corner).

    Use this interpolator if input data should be interpreted as
    continuous functions like initial conditions for a DG solver.

    .. tip:: To provide the initial condition for a DG solver with finite
             element polynomials of order :math:`k = 1` on a grid of
             :math:`1 \times 10` cells, use the ``linear`` interpolator
             with a dataset of shape ``(11, 2)`` (dimensions are inverted).
             The dataset values are the function values at the grid vertices.
