Boundary Conditions
===================

Boundary conditions represent the *super-scale physics*, the forcing of the
(larger-scale) sourrounding system onto the simulated system.
After specifying the boundary segmentation when :doc:`creating and mapping a
grid </manual/grid>`, we are able to define the actual conditions applied at
each segment. Like the :ref:`parameterization data file <man-parameter_file>`,
the boundary condition data file is a YAML_ file. Boundary segments are
associated with their boundary conditions by means of the segment indices.

.. contents::
    :depth: 3
    :local:

.. _man-bc_file:

Boundary Condition File
-----------------------

The file determines the boundary conditions at certain times for each boundary
segment. Its file path must be given via the key ``boundary.file`` in the
:doc:`config file </manual/config-file>`.

After stating a certain boundary segment, the conditions are listed.
The index is the boundary segment index specified in the
:doc:`grid mapping </manual/grid>`. A boundary condition generally consists of
a type, a time interval, and a value. The latter two may be stated as sequences
of values. This way, the time interval may be limited, or a boundary condition
may be interpolated between the specified times. Certain types of boundary
conditions may require additional keys.

.. code:: yaml

    <boundary-name>:
      index: <int>
      conditions:
        <cond-1>:
          time: <scalar or sequence>
          type: <string>
          value: <scalar or sequence>

        # ... more conditions

    # ... more boundaries

    default:
      type: <string>
      value: <scalar>

Default Condition
^^^^^^^^^^^^^^^^^
The ``default`` boundary condition is the only required condition. It lacks
a time specification and is inserted whenever a boundary condition is missing.
This can happen if a certain time interval remains uncovered by the stated
conditions, or a boundary condition is missing from the file (which is allowed
and will not produce warnings).

Time Intervals
^^^^^^^^^^^^^^
When stating several boundary conditions on a boundary segment, all but the
(temporally) final condition must have a time *interval*, i.e., the value of
``time`` must be a sequence of two values. The final condition is automatically
extended until the end of the simulation.

Boundary conditions of the same boundary segment must not overlap in time.
If you wish one condition to follow exactly after the other, make sure that
the end of the first interval matches the start of the second interval exactly.

Boundary conditions whose time intervals start and end with the same time, or
lie completely outside the simulation time interval, are ignored. The only
exception occurs when computing an :doc:`initial condition </manual/initial>`
from the stationary problem. In this case, the earliest boundary condition
which overlaps with the simulation start time is chosen. In particular, its
time interval may only consist of the simulation start time stamp. See the docs
of the :ref:`stationary initial condition type <initial-type-stationary>` for
an example.

.. note:: DORiE automatically adjusts its time steps to match a change in
   boundary conditions.

Value Interpolation
^^^^^^^^^^^^^^^^^^^
Boundary condition values can be linearly interpolated between the begin and
end time of the condition. To that end, one may use a sequence of two values
as ``value``. The standard time step scheme is second order in time and space
and hence suitable for linear interpolation of boundary conditions.

Boundary Condition Types
------------------------

A boundary condition has the following template:

.. code:: yaml

    time: <scalar or sequence>
    type: <string>
    value: <scalar or sequence>
    # <additional keys>

Dirichlet
^^^^^^^^^

A Dirichlet boundary conditions determines the exact value of the solution
at the boundary.

Richards Model
    Quantity: Matric head :math:`h_m \, [\text{m}]`.

    .. tip::
        Typical use cases for Dirichlet conditions in a Richards Model
        include:

        * Height above a fixed water table at the lower boundary.
        * Evaporative potential at upper boundary, if :math:`h_m < z_\text{wt}`,
          where :math:`z_\text{wt}` is the height above the water table.

Transport Model
    Quantity: Total concentration :math:`C_t\,[\text{kg}\,\text{m}^{-d}]`
    or concentration in the water phase
    :math:`C_w\,[\text{kg}\,\text{m}^{-d}]`, depending on the value of
    ``concentration_type``. The integer :math:`d` denotes the spatial
    dimension.

Variables
    * ``type``: ``Dirichlet``
    * ``value``: Quantity as specified above.
    * ``concentration_type`` **(Transport only)**: Switch between ``total``
      (:math:`C_t`) or ``water_phase`` (:math:`C_w`) concentration as
      defined above.

Neumann
^^^^^^^

A Neumann boundary condition determines the value of target quantity flux
at the boundary.

Richards Model
    Quantity: Water flux :math:`\mathbf{j}_w \, [\text{ms}^{-1}]`.

    Precipitation and evapotranspiration fluxes are typically measured
    with respect to a flat surface. For modeling these fluxes, the
    ``horizontal_projection`` key is used, which scales the flux applied
    to the boundary with its inclination with respect to the direction of
    gravitational force,

    .. math::

        j_{N, \text{scaled}}
          = \left| \mathbf{\hat{n}} \cdot \mathbf{\hat{g}} \right| j_N .

    .. note:: Hydraulic conductivity decreases with water content. Evaporation
              fluxes must not infinitely exceed the stable limit, or else the
              solver will inevitably fail.

Transport Model
    Quantity: Solute flux
    :math:`\mathbf{j}_s \, [\text{kg}\,\text{m}^{-d+1}\,\text{s}^{-1}]`,
    where :math:`d` indicates the spatial dimensions.

Variables
    * ``type``: ``Neumann``
    * ``value``: Flux perpendicular to the surface :math:`j_N`, in the
      quantities defined above. Fluxes into the domain are negative, fluxes
      out of the domain positive.
    * ``horizontal_projection`` **(Richards only)**: Boolean if the
      actual boundary flux should be scaled with the surface inclination
      against the direction of gravity.

Outflow
^^^^^^^

An outflow boundary condition only allows flow out of the domain.

Richards Model
    Quantity: Matric head :math:`h_m \, [\text{m}]` .

    The outflow boundary condition is a variant of the Dirichlet boundary
    condition. It applies the specified matric head only if it leads to a
    net flux out of the domain. Otherwise, the flux is set to zero (no flow
    boundary condition).

Transport Model
    Quantity: Solute flux
    :math:`\mathbf{j}_s \, [\text{kg}\,\text{m}^{-d+1}\,\text{s}^{-1}]`,
    where :math:`d` indicates the spatial dimensions.

    The outflow boundary condition allows solute be transported throw the
    boundary by the water flux. As solute concentration and water flux are
    only modelled inside the domain, this effectively only allows the solute
    to leave the domain and be discarded. The boundary condition value is an
    additional solute flux for which all considerations of a Neumann
    boundary condition apply (see above).

    The total solute flux through the boundary is then given by

    .. math::

        j_s = \left| \mathbf{j}_s \cdot \mathbf{\hat{n}} \right| + j_N ,

    where :math:`\mathbf{j}_s = C_w \mathbf{j}_w` is the solute flux at the
    boundary, :math:`\mathbf{\hat{n}}` is the boundary unit normal vector
    and :math:`j_N` is the flux perpendicular to the surface as specified
    in the boundary condition ``value``. This flux is applied in addition to
    regular seepage through the boundary. It is recommended to set this
    value to 0 (zero).

Variables
    * ``type``: ``Outflow``
    * ``value``: Matric head :math:`h_m` or solute flux perpendicular to the
      surface :math:`j_N` in quantities defined above, depending on the model.

Example Files
-------------

DORiE provides a set of example files which can be placed into any directory
using the ``dorie create`` command of the
:ref:`Command Line Interface <man-cli-commands>`.

Using the default mapping of a rectangular grid, the default boundary condition
file for the Richards model yields a strong infiltration flux at the upper
boundary and a water table at the lower boundary, while all other boundaries
are impermeable. The ``horizontal_projection`` parameter ensures that the given
value is the rainfall per unit area in case the surface is tilted or
irregularly shaped.

.. literalinclude:: ../default_files/richards_bc.yml
   :language: yaml

The default boundary condition file for the Transport model yields a constant
concentration of solute at the upper boundary, similar to a homogeneous tracer
in the fluid. All other boundaries employ outflow conditions.

.. literalinclude:: ../default_files/transport_bc.yml
   :language: yaml

.. _YAML: https://gettaurus.org/docs/YAMLTutorial/
