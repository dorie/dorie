Parameterization
================

Specifying the domain properties requires input of the
:doc:`soil architecture <grid>`
(in relation to the grid) and of the soil parameters. The latter incorporate
the *subscale physics*, the representation of the soil hydraulic and solute
properties below the REV scale. DORiE requires several input files for
retrieving this information, depending on the type of grid used for the
computation.

.. contents::
   :depth: 3
   :local:

A YAML_ parameter file is always required. It specifies a set of
parameterizations, along with a medium index. This index is used to reference
the medium specified in the architecture files, or with the "global" indices
given in the config file.

Additionally, you can specify settings for the small-scale heterogeneities
of the soil. They require an input via H5 datasets that contain appropriate
scaling factors. You can conveniently create such datasets using the random
field generator of the :doc:`command line interface </manual/cli>`.

.. _man-parameter_file:

YAML Parameter File
-------------------
This file is used to specify the parameterization for each medium inside the
simulated domain. It follows a simple hierarchical syntax. The file path and
name must be specified via the ``<model>.parameters.file`` key of the
:doc:`config file <config-file>`.

The top-level mapping must contain the key ``volumes``. This key contains a
sequence of arbitrary parameterization names. These, in turn, contain the
medium ``index``, and the model type (``richards`` or ``transport``).
The medium index must be an **integer**. Each model key contains the
parameterization ``type``, and the actual ``parameters``.

.. note:: Parameterization data for model ``transport`` is only required if
          the model is actually enabled in the :doc:`config file settings
          <config-file>`, via ``simulation.mode = richards+transport``.

Heterogeneities throughout the entire domain are set via the top level key
``scaling``. It must contain a supported scaling ``type``, which may default
to ``none``. In case an actual scaling is to be applied, the section must
contain the key ``data``, which specifies the datasets required for this type
of scaling.

.. warning::
   Transport parameters like `porosity` or `characteristic length` currently
   are not affected by small scale heterogoeneities.

Every ``scaling_section`` has the same keys: The H5 filepath is given by
``file``, and the internal dataset path by ``dataset``. You can then choose an
``interpolation`` method for the dataset. You may optionally insert values
for the physical ``extensions`` of the dataset and the ``offset`` of its
(front) lower left corner from the origin. If at least one of them is omitted,
the inserted field is automatically scaled to match the maximum grid
extensions. This also works for irregular grid shapes.

.. code-block:: yaml

    volumes:
      <name-0>:
        index: <int>
        richards:
          type: <string>
          parameters:
            <param-name-0>: <float>
            # more parameters ...

        # if transport mode is enabled ...
        transport:
          type: <string>
          parameters:
            <param-name-0>: <float or sequence>
            # more parameters ...

      # more volumes ...

    scaling:
      type: <string>
      data:
        <scaling_section>:
          file: <string>
          dataset: <string>
          interpolation: <string>
          # optional:
          extensions: <sequence>
          offset: <sequence>

        # more scaling sections ...

You find a documentation of the parameterizations implemented in DORiE
along with the parameters they require below.
The parameterization ``type`` must match a static parameterization name or a
valid combination of them. Likewise, the parameters must match the names of
parameters these objects use.

.. _man-parameter_scaling:

Scaling Field Datasets
----------------------
One or more datasets in possibly multiple HDF5_ files are required for creating
a medium with small-scale heterogeneities. The datasets must consist of
floating-point values and must have the same dimensionality as the grid.
Other than that, there is no restriction on their shape because they are
inserted into :doc:`Interpolators <interpolators>`. The interpolators supply
scaling factor values ased on positions on the grid and thus create a
grid-independent representation. Scaling field input works the same for any
supported grid type.

During setup, the program reads the interpolator values at the **barycenter**
of every grid cell on the **coarsest** grid configuration. These values are
stored and referenced with the grid cell. If the cell is refined, the same
scaling factors apply in all its child cells.

Supported Parameter File Types
------------------------------
This section lists the available types for parameterizations, scalings, and
interpolators in a understandable format.

Richards Parameterizations
~~~~~~~~~~~~~~~~~~~~~~~~~~

As the *soil porosity* :math:`\phi \, [-]` and the *residual water content*
:math:`\theta_r \, [-]` vary between soil types, it is convenient to define the
*soil water saturation* :math:`\Theta \, [-]` by

.. math::

   \Theta (\theta_w) = \frac{\theta_w - \theta_r}{\phi - \theta_r},
   \quad \Theta \in \left[ 0, 1 \right],

where :math:`\theta_w \, [-]` is the volumetric soil water content.
One typically assumes that the entire pore space can be filled up with water,
hence we set the *saturated water content* :math:`\theta_s \, [-]` equal to the
porosity, :math:`\theta_s = \phi`.
This relation can be manipulated in certain :ref:`local scalings <man-scaling>`.

Mualem–van Genuchten
""""""""""""""""""""

    Implements the following parameterization functions:

    .. math::

        \Theta (h_m) &= \left[ 1 + \left[ \alpha h_m \right]^n \right]^{-1+1/n}
        \\
        K (\Theta) &= K_0 \Theta^\tau
                        \left[ 1 -
                            \left[ 1 - \Theta^{n / \left[ n-1 \right]}
                            \right]^{1-1/n}
                        \right]^2

    * ``type: MvG``

    Parameters:
        * ``theta_r``: Residual water content :math:`\theta_r`
        * ``theta_s``: Saturated water content / Porosity :math:`\theta_s`
        * ``k0``: Saturated conductivity :math:`K_0 \, [\text{ms}^{-1}]`
        * ``alpha``: Air-entry value :math:`\alpha \, [\text{m}^{-1}]`
        * ``n``: Retention curve shape factor :math:`n`
        * ``tau``: Skew factor :math:`\tau`

    YAML template:

    .. code-block:: yaml

      richards:
        type: MvG
        parameters:
          theta_r:
          theta_s:
          k0:
          alpha:
          n:
          tau:

Transport Parameterizations
~~~~~~~~~~~~~~~~~~~~~~~~~~~

Regardless of the parameterization, the transport model always computes
the microscopic péclet number, for which it requires the characteristic pore
length, the molecular diffusion, and the fluid velocity. The latter is directly
provided by the richards model while the other two have to be specified
for each volume:

Permanent parameters:
  * ``mol_diff``: Molecular diffusion
    :math:`D_m \, [\text{m}^2\text{s}^{-1}]`
  * ``char_length``: Characteristic pore length :math:`\ell \, [\text{m}]`

.. note::
    We support two options for specifying tensors through the YAML syntax.
    You may either specify every entry of the tensor with a dedicated key, like

    .. code-block:: yaml

        <param>_xx: <value_xx>
        <param>_xy: <value_xy>
        # ...

    or give an entire sequence that will be mapped to the respective entries,

    .. code-block:: yaml

        <param>: [<value_xx>, <value_xy> # , ...
                 ]

    The sequence is interpreted as a flattened tensor with row-major layout.

.. warning::
   You must omit any component containing the spatial direction ``z`` in a
   2D setup.

The hydrodynamic dispersion tensor :math:`\mathsf{D}_\text{hd} \,
[\text{m}^2\text{s}^{-1}]` is the main parameter to provide in the
transport model. Below you will find several parameterization for this.

Constant
""""""""

  In this case, the hydrodynamic dispersion tensor is inserted directly
  component-by-compoment.

  .. note::
      From a physical point of view, the hydrodynamic tensor must be
      symmetric, but the user input is not verified by DORiE in this regard.

  .. math::

      \mathsf{D}_\text{hd}  = \text{const}.

  * ``type: Dhd_const``

  Parameters:
      * ``hydrodynamic_disp_<ij>``: (i, j)-th component of the
        hydrodynamic dispersion tensor,
        :math:`\left( \mathsf{D}_\text{hd} \right)_{ij} \, [\text{m}^2\text{s}^{-1}]`,
        **or**
      * ``hydrodynamic_disp``: Flattened hydrodynamic dispersion tensor
        :math:`\mathsf{D}_\text{hd} \, [\text{m}^2\text{s}^{-1}]`.

  YAML template:

  .. code-block:: yaml

    transport:
      type: Dhd_const
      parameters:
        mol_diff:
        char_length:

        # sequence notation, or...
        hydrodynamic_disp: [ ]

        # component notation
        hydrodynamic_disp_xx:
        hydrodynamic_disp_xy:
        hydrodynamic_disp_xz:
        hydrodynamic_disp_yx:
        hydrodynamic_disp_yy:
        hydrodynamic_disp_yz:
        hydrodynamic_disp_zx:
        hydrodynamic_disp_zy:
        hydrodynamic_disp_zz:

Power Law
"""""""""
  Implements the following parameterization function:

  .. math::

      D_\text{hd}  = \gamma D_m \operatorname{pe}^\alpha.

  * ``type: Dhd_pl``

  Parameters:
      * ``gamma``: Scale the power law :math:`\gamma \, [-]`
      * ``alpha``: Exponent of the power law :math:`\alpha \, [-]`
      * ``mol_diff``: Molecular diffusion
        :math:`D_m \, [\text{m}^2\text{s}^{-1}]`

  The Peclét number :math:`\operatorname{pe}` is specified in the
  :doc:`config file <config-file>`.

  YAML template:

  .. code-block:: yaml

    transport:
      type: Dhd_pl
      parameters:
        mol_diff:
        char_length:
        alpha:
        gamma:

Superposition
"""""""""""""

  The hydrodynamic dispersion tensor is said to be the superposition of
  several diffusion/dispersion processes. In DORiE this possible by summing
  several valid parameterizations types. Currently we provide
  parameterizations for the *effective diffusion*
  :math:`D_\text{eff}` and for the *effective hydromechanic tensor*
  :math:`\mathsf{D}_\text{hm}` identified by the key prefixes ``Deff`` and
  ``Dhm`` respectively.

  .. math::

      \mathsf{D}_\text{hd}  = \mathsf{D}_\text{hm} + D_\text{eff}.

  * ``type: <Dhm_type> + <Deff_type>``

  Each of the types are further parameterized and can be found below.

Effective Diffusion
^^^^^^^^^^^^^^^^^^^

Constant Effective Diffusion
############################

  In this case, the effective diffusion is inserted directly,

  .. math::

      D_\text{eff}  = \text{const}.

  * ``Deff_type: Deff_const``

  Parameters:

      * ``eff_diff``: Effective diffusion
        :math:`D_\text{eff} \, [\text{m}^2\text{s}^{-1}]`

  YAML template:

  .. code-block:: yaml

    transport:
      type: <Dhm_type> + Deff_const
      parameters:
        mol_diff:
        char_length:
        eff_diff:
        # <Dhm_type> parameters ...

Milligton-Quirk I Effective Diffusion
#####################################

  Implements the following parameterization function:

  .. math::

      D_\text{eff} = D_m \frac{\theta_w^{7/3}}{\phi^{2/3}}.

  where the volumetric water content :math:`\theta_w \, [-]`
  is automatically obtained from the Richards model.

  * ``Deff_type: Deff_MQ1``

  Parameters:
      * ``mol_diff``: Molecular diffusion
        :math:`D_m \, [\text{m}^2\text{s}^{-1}]`
      * ``phi``: Soil porosity :math:`\phi \, [-]`

  YAML template:

  .. code-block:: yaml

    transport:
      type: <Dhm_type> + Deff_MQ1
      parameters:
        mol_diff:
        char_length:
        phi:
        # <Dhm_type> parameters ...

Milligton-Quirk II Effective Diffusion
######################################

  Implements the following parameterization function:

  .. math::

      D_\text{eff} = D_m \frac{\theta_w}{\phi^{2/3}}.

  where the volumetric water content :math:`\theta_w \, [-]`
  is automatically obtained from the Richards model.

  * ``Deff_type: Deff_MQ2``

  Parameters:
      * ``mol_diff``: Molecular diffusion
        :math:`D_m \, [\text{m}^2\text{s}^{-1}]`
      * ``phi``: Soil porosity :math:`\phi \, [-]`

  YAML template:

  .. code-block:: yaml

    transport:
      type: <Dhm_type> + Deff_MQ1
      parameters:
        mol_diff:
        char_length:
        phi:
        # <Dhm_type> parameters ...

Effective Hydromechanic Dispersion
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Constant Effective Hydromechanic Dispersion Tensor
##################################################

  In this case, the effective hydromechanic dispersion tensor is inserted
  directly.

  .. math::

      \mathsf{D}_\text{hm} = \text{const}.

  * ``Dhm_type: Dhm_const``

  Parameters:
      * ``eff_hydromechanic_disp_<ij>``: (i, j)-th component of the
        hydromechanic dispersion tensor,
        :math:`\left(\mathsf{D}_\text{hm}\right)_{ij} \, [\text{m}^2\text{s}^{-1}]`,
        **or**
      * ``eff_hydromechanic_disp``: Flattened hydromechanic dispersion tensor
        :math:`\mathsf{D}_\text{hm} \, [\text{m}^2\text{s}^{-1}]`.

  YAML template:

  .. code-block:: yaml

    transport:
      type: Dhm_const + <Deff_type>
      parameters:
        mol_diff:
        char_length:

        # sequence notation, or...
        eff_hydromechanic_disp: [ ]

        # component notation
        eff_hydromechanic_disp_xx:
        eff_hydromechanic_disp_xy:
        eff_hydromechanic_disp_xz:
        eff_hydromechanic_disp_yx:
        eff_hydromechanic_disp_yy:
        eff_hydromechanic_disp_yz:
        eff_hydromechanic_disp_zx:
        eff_hydromechanic_disp_zy:
        eff_hydromechanic_disp_zz:

        # <Deff_type> parameters ...

Effective Hydromechanic Dispersion Tensor for Isotropic Media
#############################################################

    Implements the following parameterization function:

    .. math::

        \left( \mathsf{D}_\text{hm} \right)_{ij}
          = (\lambda_l-\lambda_t)\frac{v_i v_j}{\lvert \mathbf{v} \rvert}
            + \delta_{ij}\lambda_t \lvert \mathbf{v} \rvert,

    where :math:`\mathbf{v} \, [\text{ms}^{-1}]` is the local fluid velocity
    and :math:`\delta_{ij}` is the Kronecker delta.

    * ``Dhm_type: Dhm_iso``

    Parameters:
        * ``lambda_l``: Longitudinal dispersivity :math:`\lambda_l \, [\text{m}^2\text{s}^{-1}]`
        * ``lambda_t``: Transverse dispersivity :math:`\lambda_t \, [\text{m}^2\text{s}^{-1}]`

    YAML template:

    .. code-block:: yaml

      transport:
        type: Dhm_iso + <Deff_type>
        parameters:
          mol_diff:
          char_length:
          lambda_l:
          lambda_t:
          # <Deff_type> parameters ...

.. _man-scaling:

Scalings
~~~~~~~~
Every ``scaling_section`` has the following layout:

.. code-block:: yaml

    <scaling_section>:
      file: <string>
      dataset: <string>
      interpolation: <string>
      # optional:
      extensions: <sequence>
      offset: <sequence>

The setting ``interpolation`` accepts any of the :doc:`implemented Interpolators <interpolators>`.
The values of ``extensions`` and ``offset`` are sequences containing the
coordinates in the respective spatial dimensions.

.. note::

    ``extensions`` and ``offset`` of the scaling field will be set to match
    the grid extensions automatically, if at least one of these *keys* is
    omitted. Only omitting the respective *values* of both keys will lead to
    a parser error.

.. _man-miller_scaling:

Miller Scaling
""""""""""""""

  Assumes geometric similarity between scaled regions. Applies the following
  scaling:

  .. math ::

      \Theta &= \Theta (h_m \cdot \xi_M)\\
      K &= K (\Theta) \cdot \xi_M^2

  * ``type: Miller``

  Scaling sections:
      * ``scale_miller``: Scaling factor :math:`\xi_M` to be applied onto
        matric head and conductivity simultaneously.

  YAML template:

  .. code-block:: yaml

      type: Miller
      data:
        scale_miller:
          # ...

Miller Porosity Scaling
"""""""""""""""""""""""

  Applies porosity scaling in addition to regular :ref:`Miller scaling <man-miller_scaling>`, violating its geometric similarity assumption.

  .. math::

      \Theta &= \Theta (h_m \cdot \xi_M) \\
      K &= K (\Theta) \cdot \xi_M^2 \\
      \phi &= \theta_s - \xi_\theta

  * ``type: MilPor``

  Scaling sections:
      * ``scale_miller``: Scaling factor :math:`\xi_M` applied onto matric head and conductivity simultaneously.
      * ``scale_porosity``: Scaling summand :math:`\xi_\theta` subtracted from the saturated conductivity.

  YAML template:

  .. code-block:: yaml

      type: MilPor
      data:
        scale_miller:
          # ...
        scale_porosity:
          # ...


.. _YAML: https://gettaurus.org/docs/YAMLTutorial/
.. _HDF5: https://www.h5py.org/
