**********************
Command Line Interface
**********************

.. contents::
    :depth: 4

DORiE consists of two main executables and multiple utility scripts.
This wrapper script supplies input files, and manages the execution of these
routines. The CLI is available from within the virtual environment
(``venv``).

.. _cli-venv:

The Virtual Environment
=======================

A `virtual environment <https://docs.python.org/3/tutorial/venv.html>`_ is
a specially configured shell session, where only a certain Python installation
with specific modules is available. It is installed separately from the
system's Python installation to avoid compatibility issues. DUNE and DORiE
automatically create such a virtual environment upon configuration and install
the necessary modules into it. You then just need to activate it.

Whenever using DORiE, you need to activate the virtual environment. You can do
so by running the Docker application, or by activating it from your local
installation. After following *either* of the subsequent instructions, you are
set up to execute DORiE.

Boot up the Docker application
------------------------------
The Docker application starts an interactive Docker container with its own file
system. To interchange files between the container and your local file system,
you need to mount a directory into it. Mounting a directory will include all its
subdirectories and preserve their relative file paths.

Start up the Docker application by calling
::

    docker run -it -v <dir>:/mnt <img>

Replace ``<img>`` with ``dorie/dorie[:<tag>]`` and optionally insert the ``tag``
of the version you want to run. Omitting tag information will default to the
``latest`` tag which points to the latest stable version of DORiE.

Replace ``<dir>`` with the directory you would like to mount into the container.
The mount endpoint is the directory ``/mnt`` inside the container. One easy way
of working with the directories is creating a new directory for input and output
data, navigating to it, and replacing ``<dir>`` with ``$PWD``, which mounts your
current working directory.

Executing the above command will start an interactive (``bash``) shell session
inside the container's file system. You will be placed inside the ``/mnt``
directory.

Activate the ``venv`` locally
-----------------------------
After installing DORiE manually, you can activate the virtual environment in
your local shell. Simply execute
::

    source <path/to/>dorie/build-cmake/activate

to activate it. The prefix ``(dune-env)`` will indicate that activation was
successful. You can deactivate the ``venv`` at any time by executing
::

    deactivate


You are now ready to use the DORiE Command Line Interface!

.. _man-cli-commands:

CLI Commands
============

The Command Line Interface has the following signature:
::

    dorie <cmd> [<opts>] [<args>]

The entire wrapper and every command of it support the ``-h`` or ``--help``
option for information about how to use the command, and on its possible
arguments.

Installation Support
--------------------

The command line interface supports execution of an installation directory tree
anywhere on the file system. If you installed DORiE into a specific directory,
export this directory as ``DORIE_INSTALL_DIR`` environment variable inside the
virtual environment. The command line interface will then use the install tree
instead of the default build tree:

.. code-block:: bash

    # Uses the default build tree
    dorie run config.ini

    # Will execute DORiE from installed location '/some/dir'
    export DORIE_INSTALL_DIR=/some/dir
    dorie run config.ini


Argument Parser Documentation
-----------------------------

The following is a documentation of the link
`ArgumentParser <https://docs.python.org/3.7/library/argparse.html>`_
instance used for the CLI.

.. argparse::
    :ref: dorie.cli.parser.create_cl_parser
    :prog: dorie
