Fluxes
======

Depending on the application for what you want to use DORiE, there might be the case that it is needed something more elaborated than the default fluxes that DORiE provides. Therefore, this section explains how to interpret the default fluxes and the flux reconstruction technique used to solve solute transport problems.

Understanding the water flux output
-----------------------------------

Firstly, we have to recall that DORiE solves a Discontinuous Galerking finite element problem with *matric head*/*solute concentration* as unknown. It means that the solution of the *matric head*/*solute concentration* (and therefore the *water flux*/*solute flux*) is continuous only element-wise, or in other words, it is discontinuous on the intersections between elements. On the other hand, the dG method solves *numerical fluxes* on the intersections between elements composed together with a penalty term that increases with respect to the discontinuity of the *matric head*/*solute concentration*. This ensures that the local solution is conservative while keeps the discontinuity as low as possible.

From the description above one can infer that one has to distinguish between * fluxes* at the interior of each element and at the intersections of all elements (we call these intersections skeleton of the grid). Unfortunately, there is no a standard form to write the skeleton fluxes on formats like VTK and that's the main reason why DORiE only provides the interior fluxes. However, assuming one can write both fluxes into some output format, they are still discontinuous (notice that direct use of discontinuous fluxes are useless for conservative computations since the transported quantities are very likely to get stagnated or over-transported in the nearby of intersections between elements). It means that it is needed some sort of post-processing that ensures that the *mass* is still locally and globally conserved.

When employing a finite volume solver, the regular flux output is omitted
because the basis function gradients are always zero. In this case, the only
option for evaluating flux data is flux reconstruction which has to be enabled
in the :doc:`config file <config-file>`.

.. _man-flux_reconstruction:

Flux reconstruction
-------------------
The flux reconstruction is a projection of the fluxes used in the Discontinuous Galerkin method into a vector field function. Using correct elements, this procedure can ensure that fluxes in normal direction to the element are *equivalent* to those computed by the Discontinuous Galerkin method, and most importantly, it can also ensure the continuity of them. Hence, the resulting vector field is useful to compute other problems that rely on the fluxes of the water (i.e. solute transport).

The flux reconstruction technique always use Raviar Thomas finite elements of one degree less than the one set for the Richards model. It can be identified in the vtk file by the name ``flux_RT{min(k-1,0)}``, where ``k`` is the finite element order set for the Richards model. Flux reconstruction is not available for non-conforming grids (i.e. Cube-adaptive grids).

+---------------------------+---+---+---+---+
| Richards FEorder          | 0 | 1 | 2 | 3 |
+============+====+=========+===+===+===+===+
|            | 2D | Simplex | ✗ | ✓ | ✓ | ✓ |
|            |    +---------+---+---+---+---+
|            |    | Cube    | ✓ | ✓ | ✓ | ✓ |
| Non-adapt. +----+---------+---+---+---+---+
|            | 3D | Simplex | ✗ | ✓ | ✓ | ✓ |
|            |    +---------+---+---+---+---+
|            |    | Cube    | ✓ | ✓ | ✓ |   |
+------------+----+---------+---+---+---+---+
|            | 2D | Simplex | ✗ | ✓ | ✓ | ✓ |
|            |    +---------+---+---+---+---+
|            |    | Cube    | ✗ |   |   |   |
| Adapt.     +----+---------+---+---+---+---+
|            | 3D | Simplex | ✗ | ✓ | ✓ | ✓ |
|            |    +---------+---+---+---+---+
|            |    | Cube    | ✗ |   |   |   |
+------------+----+---------+---+---+---+---+

Legend:

* ✓: Flux reconstruction available.
* ( ): Flux reconstruction unavailable.
* ✗: Invalid setting. Finite volume solvers only works on regular grids,
  see :ref:`Richards Solver Options <richards_solver_options>`.

Usage
-----
To activate/deactivate flux reconstruction use the keyword
``numeric.fluxReconstruction = true/false`` in the
:doc:`config file <config-file>`. Flux reconstruction of the water flow is
always enabled in a coupled Richards and transport simulation because it is
required by the discretization of the convection-diffusion equation.
