Generic VTK File Readers
************************

For an interactive inspection and analysis of the VTK files written by DORiE we
recommend using ParaView. For more information, learn
:doc:`how to use ParaView </cookbook/2-paraview/tutorial>`. More detailed analyses
typically require more complex operations on the data which is likely easier
in Python. The DORiE Python module provides facilities based on the
`VTK Python module <https://pypi.org/project/vtk/>`_.

.. contents::
   :depth: 2
   :local:


Prerequisites
=============

For using the modules and classes listed below, users need to install a set of
required software packages. If you followed the installation instructions in
the :doc:`ReadMe </markdown/README>`, you are already set and can start
using them inside the :ref:`virtual environment <cli-venv>`.

If you install the DORiE Python package separately, you may need to install
additional prerequisites for the VTK library, depending on your operating
system. **Linux** users need to install the OpenGL library. On Ubuntu and
similar systems, call

.. code-block:: console

    apt install libgl-dev


Example Code
============

This example code loads the resulting PVD file of a DORiE simulation, reports
the simulation times for which files are available and prints the water content
at the center of the domain for every file.

.. code-block:: python

    from dorie.utilities.vtktools.vtkreader import PVDReader

    # Load the PVD file written by a DORiE simulation run
    pvd_reader = PVDReader("result.pvd")

    # Report the times
    print(pvd_reader.times)

    # Iterate over the output files
    # NOTE: `dataset` is an instance of `VTKReader`
    for dataset in pvd_reader:
        # Retrieve time from file directly
        time = dataset.time

        # Fetch the water content data array
        # NOTE: `water_content` is an instance of `VTKDataArray`
        water_content = dataset["theta_w"]

        # Compute the center of the domain
        center = np.mean(water_content.bounds, axis=1)

        # Report the water content at this location
        print(water_content.evaluate(center))


Source Code Documentation
=========================

This is a flexible VTK XML file reader which can evaluate all data sets written
by DORiE. It is derived from the abstract base class ``Mapping`` and thus
supports accessing the data arrays stored inside like a Python ``dict``. Use
the data array names as keys.

.. note:: VTK files do not store time inherently. When loading a file directly
          using the ``VTKReader``, the ``time`` attribute will be set to
          ``None``. Use the
          :class:`dorie.utilities.vtktools.vtkreader.PVDReader` to load all VTK
          files of a simulation run and access their times.

.. autoclass:: dorie.utilities.vtktools.vtkreader.VTKReader
    :members:
    :inherited-members:

The ``VTKReader`` returns instances of ``VTKDataArray``, which is a wrapper
around the underlying VTK data array.

.. autoclass:: dorie.utilities.vtktools.vtkreader.VTKDataArray
   :members:

DORiE writes PVD files containing references to the VTK file(s) stored for
every time step, and the associated simulation time. The ``PVDReader`` stores
these references and orders them by time. If accessed, it returns instances of
:class:`dorie.utilities.vtktools.vtkreader.VTKReader` for each referenced file.

.. autoclass:: dorie.utilities.vtktools.vtkreader.PVDReader
   :members:
