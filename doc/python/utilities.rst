***************
dorie.utilities
***************

.. contents::

What is dorie.utilities?
========================

This package bundles some useful python modules that are being used by the other
DORiE python packages.


The dorie.utilities scripts
===========================

.. _plot_vtk:

.. object:: plot_vtk.py

  Plots the given, ASCII encoded, VTK file(s).

  This script accepts a VTK file or glob pattern, parses it,
  invokes :mod:`dorie.utilities.vtk_plotter` on each of them, and implements special
  handling of warnings and exceptions to keep the output as clean as possible.
  
  The variables to be plotted may be specified using the :option:`plot_vtk.py --var` command line option.

  **Command line options**:
  
  .. program:: plot_vtk.py
  
  .. option:: --vtk 

    Input VTK file or glob pattern

  .. option:: --var 
  
    The variables to plot, plots all if not given
    
  .. option:: --Wn

    Hide warnings (optional, default false)
  
Package reference
==================

.. automodule:: dorie.utilities
    :members:
    :undoc-members:

dorie.utilities.check_path
--------------------------

.. automodule:: dorie.utilities.check_path
    :members:
    :undoc-members:
    
dorie.utilities.grid
---------------------

.. automodule:: dorie.utilities.grid
    :members:
    :undoc-members:
    
dorie.utilities.fuzzy_compare_grid
----------------------------------

.. automodule:: dorie.utilities.fuzzy_compare_grid
    :members:
    :undoc-members:

dorie.utilities.vtktools.vtkfile
------------------------------------

.. automodule:: dorie.utilities.vtktools.vtkfile
    :members:
    :undoc-members:

dorie.utilities.vtktools.vtk_grabber
------------------------------------

.. automodule:: dorie.utilities.vtktools.vtk_grabber
    :members:
    :undoc-members:

dorie.utilities.vtktools.pvd_reader
-----------------------------------

.. automodule:: dorie.utilities.vtktools.pvd_reader
    :members:
    :undoc-members:

    
dorie.utilities._load_all_in_folder
-----------------------------------

.. automodule:: dorie.utilities._load_all_in_folder
    :members:
    :undoc-members:
    
dorie.utilities.warnings
------------------------

.. automodule:: dorie.utilities.warnings
    :members:
    :undoc-members:
