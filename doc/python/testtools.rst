dorie.testtools
****************

.. contents::

What is dorie.testtools?
========================

dorie.testtools provides the following components

- a wrapper script to run automated tests created with dune-testtools
- evaluation scripts to process the run data and determine pass or fail for each test

How to use dorie.testtools
++++++++++++++++++++++++++

The dorie.testtools wrappers are called via the CMake interface provided by dune-testtools
(CMake macro `add_dune_system_test`). The `SCRIPT` parameter allows to specify the script to be called.


The dorie.testtools scripts
===========================

``test_dorie.py``

  This script parses the given parameter file, executes DORiE, and passes the result
  to the corresponding evaluation module.
  The behavior is controlled by special parameter keys to be provided in the ini file.

  **Parameter keys**:

  * ``_evaluation``: Specifies the evaluation script that is to be called. Can be the name of any module in :mod:`dorie.testtools.evaluation`.
  * ``__inifile_optionkey``: The argument that is passed to the DORiE executable before the parameter file. This should be `run` in most situations.

  All command line arguments are passed through the dune-testtools CMake machinery and parsed by the
  :mod:`dune.testtools` python package.


The dorie.testtools core modules
================================

The module is intended to be called via the internal wrapper
:mod:`dorie.testtools.wrapper.test_dorie.py`.

.. automodule:: dorie.testtools.wrapper.test_dorie
    :members:
    :undoc-members:

dorie.testtools consists of multiple submodules:

- :mod:`dorie.testtools.utilities`, which contains convenience functions that are included by
   the evaluation modules and are too specific to be contained in :mod:`dorie.utilities`
- Evaluation modules for the system tests, where each module corresponds to one
    of the supported commands of the :mod:`dorie` wrapper script.


The ``dorie_create`` module
+++++++++++++++++++++++++++

.. automodule:: dorie.testtools.dorie_create.compare
    :members:
    :undoc-members:

The ``dorie_pfg`` module
++++++++++++++++++++++++

.. automodule:: dorie.testtools.dorie_pfg.reference
    :members:
    :undoc-members:

The ``dorie_plot`` module
+++++++++++++++++++++++++

Dummy module without actual evaluation scripts.

The ``dorie_run`` module
++++++++++++++++++++++++

.. automodule:: dorie.testtools.dorie_run.muphi
    :members:
    :undoc-members:

.. automodule:: dorie.testtools.dorie_run.ode
    :members:
    :undoc-members:

.. automodule:: dorie.testtools.dorie_run.reference
    :members:
    :undoc-members:

The utilities module
++++++++++++++++++++

.. automodule:: dorie.testtools.utilities
    :members:
    :undoc-members:

The MuPhi output file reader
----------------------------

.. automodule:: dorie.testtools.utilities.muphi_reader
    :members:
    :undoc-members:

Richards equation definitions
------------------------------

.. automodule:: dorie.testtools.utilities.richards_equation
    :members:
    :undoc-members:

Statistical tools
-----------------

.. automodule:: dorie.testtools.utilities.statistics
    :members:
    :undoc-members:
    
Decorators
----------

.. automodule:: dorie.testtools.utilities.decorators
    :members:
    :undoc-members:
