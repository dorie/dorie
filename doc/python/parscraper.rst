*****************
dorie.parscraper
*****************

.. contents::

What is dorie.parscraper?
==========================

dorie.parscraper is a python package with the purpose of automatic creation of
DORiE parameter files in various formats. The developers only need to maintain one single XML file that contains all the relevant information for each parameter.

It provides the following components:

- :mod:`Readers <dorie.parscraper.readers>` for parsing both the XML file containing the meta-information for each parameter and the DORiE C++ source files,

- a :mod:`matching function <dorie.parscraper.match_parameters>` that complements the information from the XML file with the location where each parameter is actually used in the code,

- :mod:`Writers <dorie.parscraper.writers>` for various output formats (e.g. to dynamically create the .ini files used in runs of the main program, or to create :doc:`a user cheat sheet </manual/config-file>` with further explanations),

- a :mod:`wrapper script <dorie.parscraper.wrapper>` controlling the scraping process.

Another purpose is to identify mismatches between XML meta-information and source
code and notify the developers. Therefore, a warning is issued if either a parameter
is only found in the XML file, or only found in the DORiE source code.


How to use dorie.parscraper
++++++++++++++++++++++++++++

The dorie.parscraper scripts are called via CMake while configuring DORiE. This is to make sure that the generated ini files are accessible as soon as the system tests are created.


The dorie.parscraper scripts
=============================

.. object:: scrape_folder.py

  This script basically just parses the command line arguments, implements special
  handling of warnings and exceptions to keep the output as clean as possible, and
  calls :mod:`dorie.parscraper.wrapper.scrape_folder`.

  **Command line options**:

  .. option:: --xml

    Path to the XML file holding parameter meta-information

  .. option:: --source

    The C++ source folder to scrape

  .. option:: --out

    Output file(s)

  .. option:: --css

    Path to a CSS file to include into HTML output (optional)

  .. option:: --debug

    Show warnings (optional, default off)

The dorie.parscraper core modules
==================================

.. automodule:: dorie.parscraper
    :members:
    :undoc-members:

Parameter class
+++++++++++++++

.. automodule:: dorie.parscraper.parameter
    :members:
    :private-members:
    :undoc-members:

Warnings
++++++++

.. automodule:: dorie.parscraper.warnings
    :members:
    :undoc-members:

Matching function
+++++++++++++++++

.. automodule:: dorie.parscraper.match_parameters
    :members:
    :undoc-members:

Wrapper
++++++++

.. automodule:: dorie.parscraper.wrapper

.. automodule:: dorie.parscraper.wrapper.scrape_folder
    :members:
    :undoc-members:

Readers
++++++++

.. automodule:: dorie.parscraper.readers

XML reader
----------

.. automodule:: dorie.parscraper.readers.xml
    :members:
    :undoc-members:

C++ source reader
-----------------

.. automodule:: dorie.parscraper.readers.source
    :members:
    :undoc-members:

Writers
++++++++

.. automodule:: dorie.parscraper.writers

.ini file writer
----------------

.. automodule:: dorie.parscraper.writers.ini
    :members:
    :undoc-members:

.rst file writer
----------------

.. automodule:: dorie.parscraper.writers.rst
    :members:
    :undoc-members:

.html file writer
-----------------

.. automodule:: dorie.parscraper.writers.html
    :members:
    :undoc-members:
