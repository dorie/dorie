****************
Feature Overview
****************

DORiE is a flexible partial differential equation (PDE) solver based on DUNE_
and the DUNE-PDELab_ module. DORiE runs in 2D and 3D and computes solutions on
structured and unstructured grids.

Richards Solver
===============

The Richards equation describes water flow in unsaturated porous media by two
state variables, the *matric head* :math:`h_m \, [\mathrm{m}]` and the
*volumetric water content* :math:`\theta_w`,

.. math::

   \frac{\partial}{\partial t} \theta_w
     - \nabla \left[ K (\theta_w)
                     \left[ \nabla h_m - \mathbf{\hat{g}} \right]
              \right]
   = 0,

where :math:`K \, [\mathrm{ms}^{-1}]` is the hydraulic conductivity
and :math:`\mathbf{\hat{g}}` is the unit vector in the direction of
gravitational force. To solve the equation, we need to convert the matric head
into water content and vice versa. The function :math:`\theta_w (h_m)` is
called *retention curve*. Both functions :math:`\theta_w (h_m)` and
:math:`K (\theta_w)` are supplied by a *parameterization* and represent the
hydraulic material properties.

DORiE solves the Richards equation for the matric head because it that a
reformulation of the *matric potential* :math:`\psi_m [\mathrm{Jm}^{-3}]`
which, by definition, is continuous and differentiable at any point in the
soil. Depending on the soil architecture, the hydraulic parameters may be
discontinuous which would lead to water content and conductivity exhibiting
singularities.

Use the value ``richards`` in the ``simulation.mode`` keyword to run the
standalone Richards solver.

.. _richards_solver_options:

Solver Options
--------------

DORiE includes a finite volume (FV) and a discontinuous Galerkin (DG) solver
for the Richards equation. The FV solver can be selected for structured
rectangular grids by setting ``richards.numerics.FEorder = 0`` when adaptivity
is disabled. The DG solver is used for orders :math:`k \geq 1` and is available
for all grid options.

Solute Transport Solver
=======================

The solute transport equation for unsaturated media describes movement of
solute by the *solute concentration* :math:`c_w \, [\mathrm{kg}/\mathrm{m}^3]`,

.. math::

   \frac{\partial}{\partial t} c_w \theta_w
     - \nabla \left[ \vec{j}_w c_w + D \theta_w \nabla c_w
              \right]
   = 0,

where :math:`D \, [\mathrm{m}^2/\mathrm{s}]` is the effective hydrodynamic
dispersion tensor, and :math:`\vec{j}_w` is the water flux. Currently,
:math:`D` can only be used as a constant parameter by the keyword
``effHydrDips`` in the configuration file. The solute concentration per total
volume :math:`c_t \, [\mathrm{kg}/\mathrm{m}^3]` is calculated by multiplying
the concentration in the water phase with the volumetric water content,

.. math::

   c_t = c_w \theta_w

.. TODO: Update effective hydrodynamic dispersion description once
         parameterization is merged in master.

The solute transport requires a water flux of the unsaturated media, hence,
the Richards solver must be utilized to solve them. Use the value
``richards+transport`` in the ``simulation.mode`` keyword to run the solute
transport solver.

Advanced Solver Features
========================

* **Unstructured Grids**: The DG scheme of DORiE can be applied onto any grid
  geometry. Users can load unstructured simplex grids into DORiE via GMSH
  ``.msh`` files. DORiE will then use an unstructured grid manager. See
  :doc:`/manual/grid` for details.

* **Adaptive Grid Refinement** (h-refinement): DORiE supports adaptive local
  grid refinement based on an a-posteriori flux error estimator. Enabling grid
  refinement requires using an unstructured grid manager. Adaptivity can be
  enabled by setting a refinement policy in the
  :doc:`Configuration File </manual/config-file>`.

* **Parallel Execution:** DUNE modules are generally set up to support parallel
  execution on distributed hardware via MPI. DORiE supports parallel execution
  on a best effort basis. This feature is currently only supported for
  structured rectangular grids. Parallel execution is controlled through the
  :ref:`Command Line Interface <man-cli-commands>`.

Available Models
================

Several settings like solver coupling, spatial dimension, or polynomial order
must be given at compile time. DORiE includes a discrete set of setting
combinations users may choose from via the
:doc:`configuration file </manual/config-file>`. If DORiE is run from its public
Docker image or with the default targets built locally on your machine, these
are the available combinations of options:

+----------------------+-------------------+-----------------------------+------------------------------+----------------+---------------------+
|``simulation.mode``   |``grid.dimensions``|``richards.numerics.FEorder``|``transport.numerics.FEorder``| GMSH Grid and  | Parallel            |
|                      |                   |                             |                              | grid refinement| execution           |
+======================+===================+=============================+==============================+================+=====================+
|``richards``          |  2                |  0                          |   ✗                          |    ✗           |  ✓                  |
|                      +-------------------+-----------------------------+------------------------------+----------------+---------------------+
|                      |  2                |  1..3                       |   ✗                          |    ✓           | on structured grids |
|                      +-------------------+-----------------------------+------------------------------+----------------+---------------------+
|                      |  3                |  0                          |   ✗                          |    ✗           |  ✓                  |
|                      +-------------------+-----------------------------+------------------------------+----------------+---------------------+
|                      |  3                |  1                          |   ✗                          |    ✓           | on structured grids |
+----------------------+-------------------+-----------------------------+------------------------------+----------------+---------------------+
|``richards+transport``|  2                |  0                          |   same as ``richards``       |    ✗           |  ✓                  |
|                      +-------------------+-----------------------------+------------------------------+----------------+---------------------+
|                      |  2                |  1..3                       |   same as ``richards``       |  ✓\ :sup:`†`   | on structured grids |
|                      +-------------------+-----------------------------+------------------------------+----------------+---------------------+
|                      |  3                |  0                          |   same as ``richards``       |    ✗           |  ✓                  |
|                      +-------------------+-----------------------------+------------------------------+----------------+---------------------+
|                      |  3                |  1                          |   same as ``richards``       |  ✓\ :sup:`†`   | on structured grids |
+----------------------+-------------------+-----------------------------+------------------------------+----------------+---------------------+

* :sup:`†`: Grid refinement on *rectangular* grids produces hanging nodes which
  are currently not considered in the
  :ref:`flux reconstruction <man-flux_reconstruction>`. This can result in
  erroneous results computed by the transport solver.

.. _DUNE: https://www.dune-project.org/
.. _DUNE-PDELab: https://www.dune-project.org/modules/dune-pdelab/
