*************************
Installation Preparations
*************************

.. contents::

You can build DORiE locally on your machine for maximum performance. This requires you to take care of all dependencies and to handle the installation process yourself. On the other hand, you can use a virtual environment to run DORiE. We support this feature by using `Docker <https://www.docker.com/>`_.
You can install your local DORiE source code into a Docker image or load a
prepared image from `Docker Hub <https://hub.docker.com/r/dorie/dorie/>`_.
You can then run the application as Docker container. Depending on your
operating system, this will lead to impared performance.

Refer to :doc:`the ReadMe file </markdown/README>` for installation
instructions.


Prerequisites
=============

To install and run DORiE, your computer needs to meet the following requirements:

- 64-bit version of Linux, macOS, or Windows

- Intel i3/5/7 2xxx processor with at least two cores, or equivalent

- at least 4GB of RAM (8GB or more recommended)

- about 10GB of free disk space


OS-Dependent Configuration
==========================

macOS
-----
The first thing you will have to do is to install Apple's Command Line Tools which
are bundled into the XCode software package. On more recent versions of OSX, this
can be done conveniently by executing

.. code-block:: shell

    xcode-select --install

in a terminal. If this does not work, you will have to install
`XCode <https://developer.apple.com/xcode/download/>`_ via the Mac App Store.

If you have successfully installed the developer tools, you still have to activate
them by accepting the software license. You will be prompted to do so upon starting
XCode for the first time, or you can simply execute

.. code-block:: shell

    sudo xcodebuild -license accept

in the terminal.

You are now ready to instal DORiE.


Linux
-----
Since there is a multitude of different Linux distributions, we cannot provide
installation instructions for all of them. DORiE is regularly tested on the latest stable version of Ubuntu, so if you are running this OS, you may proceed with the
installation right away.

On any other distribution, the procedure of installing dependencies might
differ from the step-by-step instructions supplied in
:doc:`the ReadMe </markdown/README>`.


Windows
-------
Local builds of DUNE and DORiE on Windows are not supported. Please use the Docker installation variant on this system. Docker is available for 64-bit versions of Windows 7 or later. We recommend using Windows 10 Professional or Enterprise. Please refer to the Docker Manual on Windows for further details.

If you run another version of Windows than the recommended ones, you have to install the `Docker Toolbox <https://docs.docker.com/toolbox/overview/>`_, to enable the virtualization features necessary for Docker.
