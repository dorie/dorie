Data I/O
********

Executing DORiE requires several input files, some of which are optional and
depending on the mode the program runs in. The configuration input files can
typically be created and manipulated by any text editor. For larger portions
of input data we use HDF5_, a hierachical binary data format. These files can
easily be written using the h5py_ Python module.

DORiE writes output files in the VTK_ format, a filetype suitable for storing
grid-based datasets. These files can be evaluated interactively using
Paraview_. The VTK software library can be included into C++ code and even
has `Python bindings <https://pypi.org/project/vtk/>`_.

Input Files
===========

Templates for the required files can conveniently be generated with the
:doc:`CLI </manual/cli>`.

+----------------------------------------+----------------+------------------------------------------+----------------------------------------------------------------------+
| Type                                   | Format         | Description                              | Templates                                                            |
+========================================+================+==========================================+======================================================================+
| :doc:`Configuration File               | :file:`.ini`   | The main input file for the solver.      | :download:`config.ini </default_files/config.ini>`                   |
| </manual/config-file>`                 |                |                                          |                                                                      |
+----------------------------------------+----------------+------------------------------------------+----------------------------------------------------------------------+
| :ref:`Parameterization Data File       | :file:`.yml`   | Specifies the soil and solute            | :download:`richards_param.yml </default_files/richards_param.yml>`,  |
| <man-parameter_file>`                  |                | parameterization type(s) and parameters. | :download:`transport_param.yml </default_files/transport_param.yml>` |
+----------------------------------------+----------------+------------------------------------------+----------------------------------------------------------------------+
| :doc:`Boundary Condition Data File     | :file:`.yml`   | Specifies the boundary segmentation and  | :download:`richards_bc.yml </default_files/richards_bc.yml>`,        |
| </manual/bcfile>`                      |                | the boundary conditions.                 | :download:`transport_bc.yml </default_files/transport_bc.yml>`       |
+----------------------------------------+----------------+------------------------------------------+----------------------------------------------------------------------+
| :ref:`GMSH Mesh File                   | :file:`.gmsh`  | Mesh with possibly complicated geometry  | ---                                                                  |
| <man-grid_gmsh>` (optional)            |                | to build an unstructured grid from.      |                                                                      |
+----------------------------------------+----------------+------------------------------------------+----------------------------------------------------------------------+
| :ref:`Grid Mapping H5 File             | :file:`.h5`    | Used to generate heterogeneous soil      | ---                                                                  |
| <man-grid_mapping-dataset>` (optional) |                | architectures and boundary segmentations |                                                                      |
|                                        |                | for rectangular grids.                   |                                                                      |
+----------------------------------------+----------------+------------------------------------------+----------------------------------------------------------------------+
| :ref:`Scaling Field H5 Files           | :file:`.h5`    | Datasets for interpolating local         | ---                                                                  |
| <man-parameter_scaling>` (optional)    |                | heterogeneities in the parameterization. |                                                                      |
|                                        |                |                                          |                                                                      |
+----------------------------------------+----------------+------------------------------------------+----------------------------------------------------------------------+

.. _intro-io-output:

Output Files
============

* Time Stamp File (``.pvd``): Lists the time step information and the
  respective VTK output files of a single program run. This file is always
  saved into the directory from which DORiE was executed.

* VTK Files (``.vtu``): The output data in unstructured grid format.
  It contains multiple multiple datasets depending on the type of model:

  .. object:: Richards

    - ``head``: Matric head :math:`h_m \, [\mathrm{m}]`
    - ``K0``: Saturated conductivity :math:`K_0 \, [\mathrm{ms}^{-1}]`
    - ``flux``: Water flux :math:`\mathbf{j}_w \, [\mathrm{ms}^{-1}]`
      (not reconstructed!)
    - ``flux_RT<k-1>``: Reconstructed water flux
      :math:`\mathbf{j}_{w, \mathrm{rc}} \, [\mathrm{ms}^{-1}]`
      (if enabled!)
    - ``theta_w``: Volumetric water content :math:`\theta \, [\mathrm{-}]`
    - ``Theta``: Water saturation :math:`\Theta \, [\mathrm{-}]`

  .. object:: Transport

    - ``solute``: Solute concentration in water phase
      :math:`c_w \, [\mathrm{kg}\mathrm{m}^{-3}]`
    - ``solute_total``: Total solute concentration
      :math:`c_t \, [\mathrm{kg}\mathrm{m}^{-3}]`
    - ``micro_peclet``: Microscopic peclet number
      :math:`\mathsf{pe_\mu} \, [-]`
    - ``flux_RT<k-1>``: Reconstructed solute flux
      :math:`\mathbf{j}_{s, \mathrm{rc}} \, [\mathrm{kg}\,\mathrm{m}^{-2}\mathrm{s}^{-1}]`
      (if enabled!)
    - ``eff_hd_dispersion``: Hydrodynamic dispersion tensor
      :math:`\mathsf{D}_\mathsf{hd} \, [\mathrm{m}^2\mathrm{s}^{-1}]`
      (if enabled!)

* VTK Parallel Collection Files (``.pvtu``): Merging multiple VTK files in case
  DORiE is run in parallel.

.. _HDF5: https://www.hdfgroup.org/solutions/hdf5/
.. _h5py: https://www.h5py.org/
.. _Paraview: https://www.paraview.org/
.. _VTK: https://vtk.org/
