import h5py
import numpy as np
import os
import argparse

def create_and_write_datasets(args):
    grid_test_2d = np.zeros((20, 20), dtype=np.int_)
    grid_test_2d[10:, :] = 1

    grid_test_2d_bc_left = np.full((20), 2, dtype=np.int_)
    grid_test_2d_bc_left[10:] = 3
    grid_test_2d_bc_right = np.full((20), 2, dtype=np.int_)
    grid_test_2d_bc_right[10:] = 3

    grid_test_3d = np.zeros((10, 10, 10), dtype=np.int_)
    grid_test_3d[5:, :, :] = 1
    grid_test_3d_bc_left = np.full((10, 10), 2, dtype=np.int_)
    grid_test_3d_bc_left[5:, ...] = 3
    grid_test_3d_bc_right = np.full((10, 10), 2, dtype=np.int_)
    grid_test_3d_bc_right[5:, ...] = 3
    grid_test_3d_bc_front = np.full((10, 10), 2, dtype=np.int_)
    grid_test_3d_bc_front[5:, ...] = 3
    grid_test_3d_bc_back = np.full((10, 10), 2, dtype=np.int_)
    grid_test_3d_bc_back[5:, ...] = 3

    ode_layered_320 = np.zeros((320, 1), dtype=np.int_)
    ode_layered_320[160:, ...] = 1

    ode_layered_160 = np.zeros((160, 1), dtype=np.int_)
    ode_layered_160[80:, ...] = 1

    ode_layered_40 = np.zeros((40, 1), dtype=np.int_)
    ode_layered_40[20:, ...] = 1

    ode_layered_20 = np.zeros((20, 1), dtype=np.int_)
    ode_layered_20[10:, ...] = 1

    param_test_2 = np.random.randint(2, size=(50, 50))
    param_test_5 = np.random.randint(2, size=(50, 50))

    # have a checkerboard!
    parallel_reference_2d = np.kron([[1, 0] * 2, [0, 1] * 2] * 2, np.ones((10, 10), dtype=np.int_))
    parallel_reference_3d = np.kron([[[1, 0],
                                      [0, 1]],
                                     [[0, 1],
                                      [1, 0]]], np.ones((5, 5, 5), dtype=np.int_))
    
    # boundary mappings for the solute BCs
    solute_2d_upper = np.zeros((40), dtype=np.int_)
    solute_2d_upper[10:31] = 1

    solute_3d_upper = np.zeros((15, 15), dtype=np.int_)
    solute_3d_upper[5:11, 5:11] = 1

    f = h5py.File(args.path, 'w')
    f.create_dataset("grid_test_2d/volume", data=grid_test_2d)
    f.create_dataset("grid_test_2d/boundary_left", data=grid_test_2d_bc_left)
    f.create_dataset("grid_test_2d/boundary_right", data=grid_test_2d_bc_right)
    f.create_dataset("grid_test_3d/volume", data=grid_test_3d)
    f.create_dataset("grid_test_3d/boundary_left", data=grid_test_3d_bc_left)
    f.create_dataset("grid_test_3d/boundary_right", data=grid_test_3d_bc_right)
    f.create_dataset("grid_test_3d/boundary_front", data=grid_test_3d_bc_front)
    f.create_dataset("grid_test_3d/boundary_back", data=grid_test_3d_bc_back)

    f.create_dataset("ode_layered_320", data=ode_layered_320)
    f.create_dataset("ode_layered_160", data=ode_layered_160)
    f.create_dataset("ode_layered_40", data=ode_layered_40)
    f.create_dataset("ode_layered_20", data=ode_layered_20)
    f.create_dataset("ut/param_test_2", data=param_test_2)
    f.create_dataset("ut/param_test_5", data=param_test_5)
    f.create_dataset("parallel_reference_2d", data=parallel_reference_2d)
    f.create_dataset("parallel_reference_3d", data=parallel_reference_3d)

    f.create_dataset("solute/2d/upper", data=solute_2d_upper)
    f.create_dataset("solute/3d/upper", data=solute_3d_upper)

    f.close()

def parse_cli():
    parser = argparse.ArgumentParser(description="Write parameter mapping "
        "datasets for all tests")
    parser.add_argument("path", type=os.path.realpath,
        help="File path to write the data")
    return parser.parse_args()

if __name__ == "__main__":
    args = parse_cli()
    create_and_write_datasets(args)
