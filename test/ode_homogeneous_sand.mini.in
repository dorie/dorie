include ${CMAKE_BINARY_DIR}/doc/default_files/config.ini

_test_command = run
_asset_path = "${CMAKE_CURRENT_LIST_DIR}"
_evaluation = ode

_order = 0, 1, 2, 3 | expand prec
_upwind = none, semiUpwind, fullUpwind | expand
_ic_mode = trans, stat | expand ic_mode
__name = ode_homogeneous_sand_k{_order}_{_upwind}_{_ic_mode}

{_order} > 0 and {_upwind} == fullUpwind | exclude

[grid]
gridType = rectangular
initialLevel = 0
cells = 1 320, 1 160, 1 40, 1 20 | expand prec

[richards]

output.fileName = {__name}
output.outputPath = {__name}
output.vertexData = false

initial.type = analytic, stationary | expand ic_mode
initial.equation = -y

boundary.file = "{_asset_path}/bcs/infiltration.yml"

parameters.file = "{_asset_path}/param/richards_param.yml"

time.end = 1E7, 0 | expand ic_mode
time.maxTimestep = 1E7
time.startTimestep = 1E4

NewtonParameters.AbsoluteLimit = 1E-10
NewtonParameters.Reduction = 1E-10

numerics.penaltyFactor = 10
numerics.FEorder = 0, 1, 2, 3 | expand prec
numerics.upwinding = {_upwind}

[_ode]

headLower = 0.
flux = -5.55e-6
head_abstol = 3E-4, 3E-6, 2E-8, 6E-8 | expand prec
flux_abstol = 1E100, 5E-11, 1E-9, 3E-12 | expand prec
flux_rt_abstol = 4E-14

# Error tolerance multipliers for stationary solution
head_eps_stat = 1.0, 1.0, 10, 3.2 | expand prec
flux_eps_stat = 1.0, 1.0, 1.0, 10 | expand prec
flux_rt_eps_stat = 1E3
