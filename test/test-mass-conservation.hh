#ifndef TEST_MASS_CONSERVATION_HH
#define TEST_MASS_CONSERVATION_HH

#include <algorithm>
#include <vector>
#include <memory>
#include <iostream>
#include <algorithm>

#include <dune/common/parallel/mpihelper.hh>
#include <dune/common/exceptions.hh>
#include <dune/common/parametertree.hh>
#include <dune/common/parallel/mpihelper.hh>

#include <dune/geometry/referenceelements.hh>

#include <dune/pdelab/common/quadraturerules.hh>
#include <dune/pdelab/backend/istl.hh>
#include <dune/pdelab/gridoperator/gridoperator.hh>
#include <dune/pdelab/constraints/common/constraintstransformation.hh>
#include <dune/pdelab/localoperator/pattern.hh>
#include <dune/pdelab/localoperator/flags.hh>
#include <dune/pdelab/localoperator/idefault.hh>
#include <dune/pdelab/localoperator/defaultimp.hh>
#include <dune/pdelab/finiteelement/localbasiscache.hh>

#include <dune/dorie/common/typedefs.hh>
#include <dune/dorie/common/logging.hh>
#include <dune/dorie/common/boundary_condition/neumann.hh>
#include <dune/dorie/model/richards/richards.cc>

namespace Dune {
namespace Dorie {

/// Local Operator evaluating the water content of a grid cell
template <typename Traits, typename Parameter>
class WaterContentOperator : public Dune::PDELab::FullVolumePattern,
                             public Dune::PDELab::LocalOperatorDefaultFlags
{
public:
  // residual assembly flags
  enum { doAlphaVolume = true };

private:
  static constexpr int dim = Traits::dim;
  using RF = typename Traits::RF;
  using Scalar = typename Traits::Scalar;

  const Parameter &param;      //!< class containing parameterization
  const int quadrature_factor; //!< factor to FEM integration order
  const int intorderadd;       //!< integration order addend

public:
  WaterContentOperator(
    const Parameter &_param,
    const int _quadrature_factor = 2,
    const int _intorderadd = 2)
    : param(_param),
      quadrature_factor(_quadrature_factor),
      intorderadd(_intorderadd)
  { }

  template <typename EG, typename LFSU, typename X, typename LFSV, typename R>
  void alpha_volume(
      const EG &eg,
      const LFSU &lfsu,
      const X &x,
      const LFSV &lfsv,
      R &r) const
  {
    const int order = lfsu.finiteElement().localBasis().order();
    const int intorder = intorderadd + quadrature_factor * order;

    // get element geomentry
    auto gt = eg.geometry();

    // bind parameterization and retrieve functions
    param.bind(eg.entity());
    const auto saturation_f = param.saturation_f();
    const auto water_content_f = param.water_content_f();

    // loop over quadrature points
    for (const auto &it : quadratureRule(gt, intorder))
    {
      // evaluate position in element local and global coordinates
      const auto p_local = it.position();

      // evaluate basis function
      std::vector<Scalar> phi(lfsu.size());
      lfsu.finiteElement().localBasis().evaluateFunction(p_local, phi);

      // evaluate u
      RF u = 0.;
      for (unsigned int i = 0; i < lfsu.size(); i++)
        u += x(lfsu, i) * phi[i];

      // calculate water content from matric head
      const RF water_content = water_content_f(saturation_f(u));

      // integration factor
      const RF factor = it.weight() * gt.integrationElement(p_local);

      // update residual
      r.accumulate(lfsv, 0, water_content * factor);
    } // quadrature rule
  }   // alpha_volume
};

/// Local Operator evaluating the boundary flux for each cell
/** This only includes fluxed through Neumann boundary conditions
 */
template <typename Traits, typename Parameter, typename Boundary>
class FluxOperator : public Dune::PDELab::FullVolumePattern,
                     public Dune::PDELab::LocalOperatorDefaultFlags,
                     public Dune::PDELab::InstationaryLocalOperatorDefaultMethods<typename Traits::RangeField>
{
public:
  // residual assembly flags
  enum { doLambdaBoundary = true };

private:
  static constexpr int dim = Traits::dim;
  using RF = typename Traits::RF;
  using Scalar = typename Traits::Scalar;

  const Parameter &param;      //!< class containing parameterization
  const Boundary &boundary;    //!< class containing BC information
  const int quadrature_factor; //!< factor to FEM integration order
  const int intorderadd;       //!< integration order addend

  RF time;

public:
  FluxOperator(
    const Parameter &_param,
    const Boundary &_boundary,
    const int _quadrature_factor = 2,
    const int _intorderadd = 2)
  : param(_param),
    boundary(_boundary),
    quadrature_factor(_quadrature_factor),
    intorderadd(_intorderadd)
  { }

  void setTime(RF t) { time = t; }

  template <typename IG, typename LFSV, typename R>
  void lambda_boundary(
      const IG &ig,
      const LFSV &lfsv_s,
      R &r_s) const
  {
    const int order = lfsv_s.finiteElement().localBasis().order();
    const int intorder = intorderadd + quadrature_factor * order;

    // get element geomentry
    auto gtface = ig.geometry();

    // check boundary condition type
    const auto bc = boundary.bc(ig.intersection());
    if (bc->type() != Operator::BCType::Neumann)
      return;

    // evaluate flux boundary condition
    RF normal_flux = bc->evaluate(time);

    // adjust flux to surface tilt
    const auto& bc_neumann
      = dynamic_cast<const NeumannBoundaryCondition<RF>&>(*bc);
    if (bc_neumann.horizontal_projection())
      normal_flux *= std::abs( ig.centerUnitOuterNormal() * param.gravity() );

    // loop over quadrature points
    for (const auto &it : quadratureRule(gtface, intorder))
    {
      // integration factor
      const RF factor = it.weight() * ig.geometry().integrationElement(it.position());

      // update residual
      r_s.accumulate(lfsv_s, 0, - normal_flux * factor);
    } // quadrature rule
  }   // lambda_boundary
};

/// Derived Model for testing mass conservation. Assumes
template <typename Traits>
class ModelTest : public ModelRichards<Traits>
{
protected:
  // fetch typedefs from base class
  using Sim = ModelRichards<Traits>;
  using GV = typename Traits::GV;
  using RF = typename Traits::RF;
  using GFS = typename Traits::GFS;
  using MBE = typename Traits::MBE;
  using Grid = typename Traits::Grid;
  using U = typename Traits::U;

  using GridCreator = typename Traits::GridCreator;

  using FlowParameters = typename Traits::FlowParameters;
  using FlowBoundary = typename Traits::FlowBoundary;

  /// Error Function Space Helper
  using ERRGFSHelper = Dune::Dorie::LinearSolverGridFunctionSpaceHelper<GV, RF, Traits::GridGeometryType>;
  /// Linear solver GFS
  using ERRGFS = typename ERRGFSHelper::Type;
  /// LSGFS Constraints Container
  using ERRCC = typename ERRGFSHelper::CC;
  /// Solution vector type
  using U0 = Dune::PDELab::Backend::Vector<ERRGFS, RF>;

  /// Empty constraints container
  using NoTrafo = Dune::PDELab::EmptyTransformation;
  /// Grid operator for Error LOP
  template <typename LOP>
  using ERRGO = Dune::PDELab::GridOperator<GFS, ERRGFS, LOP, MBE, RF, RF, RF, NoTrafo, NoTrafo>;

public:
  /// Construct this model instance.
  /** \todo Give error limits here
   */
  ModelTest(
    Dune::ParameterTree& _inifile,
		const GridCreator& _grid_creator,
		Dune::MPIHelper& _helper
  ) : ModelRichards<Traits>(_inifile, _grid_creator, _helper)
    , acc(0.)
    , acc_square(0.)
  { 
    // disable output
    // this->set_policy(OutputPolicy::None);
  }

  /// Compute the water flux into a certain domain
  double water_flux(U &solution, const RF time)
  {
    using FluxLOP = typename Dune::Dorie::FluxOperator<Traits,
                                                       FlowParameters, 
                                                       FlowBoundary>;
    using FluxGO = ERRGO<FluxLOP>;

    // create GFS and operators
    auto errgfs = ERRGFSHelper::create(this->gv);
    FluxLOP fluxlop(*this->fparam, *this->fboundary);
    fluxlop.setTime(time);
    MBE mbe(0);
    FluxGO fluxgo(*this->gfs, errgfs, fluxlop, mbe);
    U0 eta(errgfs, 0.0);

    // assemble residual and accumulate value
    fluxgo.residual(solution, eta);
    auto &eta_n = Dune::PDELab::Backend::native(eta);
    return std::accumulate(eta_n.begin(), eta_n.end(), 0.0);
  }

  /// Compute the total water content of a certain solution
  double water_content(U &solution)
  {
    using WCLOP = typename Dune::Dorie::WaterContentOperator<Traits, 
                                                             FlowParameters>;
    using WCGO = ERRGO<WCLOP>;

    // create GFS and operators
    auto errgfs = ERRGFSHelper::create(this->gv);
    WCLOP wclop(*this->fparam);
    MBE mbe(0);
    WCGO wcgo(*this->gfs, errgfs, wclop, mbe);
    U0 eta(errgfs, 0.0);

    // assemble residual and accumulate value
    wcgo.residual(solution, eta);
    auto &eta_n = Dune::PDELab::Backend::native(eta);
    return std::accumulate(eta_n.begin(), eta_n.end(), 0.0);
  }

  void step() override
  {
    const auto time_old = this->_time;
    const double wc_old = water_content(*this->u);
    // compute expected integrated flux (water content change)
    const double flux = water_flux(*this->u, time_old);

    ModelRichards<Traits>::step();

    const auto time_new = this->_time;
    const auto dt = time_new - time_old;
    const auto flux_int = flux * dt;

    // compute new water content
    const auto wc_new = water_content(*this->u);
    const auto deviation = (wc_new - wc_old) - flux_int;
    acc += deviation;
    acc_square += deviation * deviation;

    std::cout << "wc_old: " << wc_old << std::endl;
    std::cout << "wc_new: " << wc_new << std::endl;
    std::cout << "integrated flux: " << flux_int << std::endl;
    std::cout << "deviation: " << deviation << std::endl;
  }

  double run_test ()
  {
    acc = 0.0;        // accumulated deviation
    acc_square = 0.0; // accumulated squared deviation

    this->run();

    std::cout << "total deviation: " << acc << std::endl;
    std::cout << "total normalized deviation: " << std::sqrt(acc_square) << std::endl;

    return acc;
  }

private:
  double acc;        // accumulated deviation
  double acc_square; // accumulated squared deviation
};

} // namespace Dorie
} // namespace Dune

#endif // TEST_MASS_CONSERVATION_HH
