include ${CMAKE_BINARY_DIR}/doc/default_files/config.ini

_test_command = run
_asset_path = "${CMAKE_CURRENT_LIST_DIR}"
_evaluation = ode

_order = 0, 1, 2, 3 | expand prec
_upwind = none, semiUpwind, fullUpwind | expand
_ic_mode = trans, stat | expand ic_mode
__name = ode_layered_k{_order}_{_upwind}_{_ic_mode}

{_order} > 0 and {_upwind} != none | exclude

[grid]
gridType = rectangular
initialLevel = 0
cells = 1 320, 1 160, 1 40, 1 20 | expand prec

[grid.mapping]
file = "{_asset_path}/maps/cell_ids.h5"
volume = ode_layered_320, ode_layered_160, ode_layered_40, ode_layered_20 | expand prec

[richards]

output.fileName = {__name}
output.outputPath = {__name}
output.vertexData = false

initial.type = analytic, stationary | expand ic_mode
initial.equation = -y

boundary.file = "{_asset_path}/bcs/infiltration.yml"

parameters.file = "{_asset_path}/param/richards_param.yml"

time.end = 1E7
time.maxTimestep = 1E7
time.startTimestep = 1E4

NewtonParameters.AbsoluteLimit = 1E-10
NewtonParameters.Reduction = 1E-10

numerics.penaltyFactor = 10
numerics.FEorder = {_order}
numerics.upwinding = {_upwind}

[_ode]

headLower = 0.
flux = -5.55e-6
head_abstol = 1E-3, 3E-5, 4E-8, 4E-6 | expand prec
flux_abstol = 1E100, 5E-10, 4E-9, 5E-11 | expand prec
flux_rt_abstol = 4E-14

# Error tolerance multipliers for stationary solution
head_eps_stat = 2.0, 2.0, 10, 3.0 | expand prec
flux_eps_stat = 1.0, 1.0, 1.0, 10 | expand prec
flux_rt_eps_stat = 1E3
