// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
#ifdef HAVE_CONFIG_H
  #include "config.h"
#endif

#include <string>
#include <iostream>

// Filesystem access
#include <unistd.h>

#include "test-mass-conservation.hh"

#include <dune/common/exceptions.hh>
#include <dune/common/parametertree.hh>
#include <dune/common/parametertreeparser.hh>
#include <dune/common/parallel/mpihelper.hh>

#include <dune/grid/yaspgrid.hh>
#include <dune/grid/uggrid.hh>

#include <dune/dorie/common/grid_creator.hh>
#include <dune/dorie/common/setup.hh>

template<int dim, int order>
using Simplex = Dune::Dorie::ModelRichardsTraits<Dune::Dorie::BaseTraits<Dune::UGGrid<dim>,
  Dune::GeometryType::BasicType::simplex>,order>;

template<int dim, int order>
using Cube = Dune::Dorie::ModelRichardsTraits<Dune::Dorie::BaseTraits<Dune::YaspGrid<dim>,
  Dune::GeometryType::BasicType::cube>,order>;

template<int dim, int order>
using CubeAdaptive = Dune::Dorie::ModelRichardsTraits<Dune::Dorie::BaseTraits<Dune::UGGrid<dim>,
  Dune::GeometryType::BasicType::cube>,order>;


int main (int argc, char** argv)
{
  try{
    // Initialize ALL the things!
		auto [inifile, log, helper] = Dune::Dorie::Setup::init(argc, argv);

    // setup richards configuration
    Dune::ParameterTree richards_config = Dune::Dorie::Setup::prep_ini_for_richards(inifile);

    // Attempt to create output directory
    const std::string output_path = richards_config.get<std::string>("output.outputPath");
		log->info("Creating output directory: {}", output_path);
		mkdir(output_path.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
		if (access(output_path.c_str(), W_OK) != 0)
			DUNE_THROW(Dune::IOError,"Output folder " << output_path << " not writable");

    const double limit = inifile.get<double>("limit");

    // run test
    double result {0.0};
    const std::string adapt_policy_str = inifile.get<std::string>("adaptivity.policy");
    const auto grid_type = inifile.get<std::string>("grid.gridType");

    Dune::Dorie::AdaptivityPolicy adapt_policy = Dune::Dorie::AdaptivityPolicy::None;

    if (adapt_policy_str == "waterFlux")
      adapt_policy = Dune::Dorie::AdaptivityPolicy::WaterFlux;
    else if (adapt_policy_str != "none")
      DUNE_THROW(Dune::NotImplemented,"The adaptivity policy " << 
        adapt_policy_str << " is not implemented!");

    if (grid_type == "rectangular") {
      if (adapt_policy != Dune::Dorie::AdaptivityPolicy::None) {
        using ModelTest = Dune::Dorie::ModelTest<CubeAdaptive<2, 1>>;

        Dune::Dorie::GridCreator<Dune::UGGrid<2>> grid_creator(inifile, helper);
        ModelTest mod(richards_config, grid_creator, helper);

        mod.set_policy(adapt_policy);
        result = mod.run_test();
      }
      else {
        using ModelTest = Dune::Dorie::ModelTest<Cube<2, 1>>;
        Dune::Dorie::GridCreator<Dune::YaspGrid<2>> grid_creator(inifile, helper);

        ModelTest mod(richards_config, grid_creator, helper);
        result = mod.run_test();
      }
    }

    else if (grid_type == "gmsh") {
      using ModelTest = Dune::Dorie::ModelTest<Simplex<2, 1>>;
      Dune::Dorie::GridCreator<Dune::UGGrid<2>> grid_creator(inifile, helper);

      ModelTest mod(richards_config, grid_creator, helper);
      mod.set_policy(adapt_policy);
      result = mod.run_test();
    }

    if (std::abs(result) > limit) {
      std::cerr << "Mass conservation test exceeded limit!" << std::endl;
      std::cerr << "Limit: " << limit << std::endl;
      std::cerr << "Result: " << result << std::endl;
      return 2;
    }
  }

  catch (Dune::Exception& e) {
    std::cerr << "Exception occurred: " << e << std::endl;
    return 1;
  }

  catch(...) {
    std::cerr << "Exception occurred!" << std::endl;
    return 1;
  }

  return 0;
}
