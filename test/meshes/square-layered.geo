// Gmsh project created on Fri Nov  2 14:56:38 2018
//+
Point(1) = {0, 0, 0, 0.5};
//+
Point(2) = {2, 0, 0, 0.5};
//+
Point(3) = {2, 2, 0, 0.5};
//+
Point(4) = {0, 2, 0, 0.5};
//+
Point(5) = {0, 1, 0, 0.5};
//+
Point(6) = {2, 1, 0, 0.5};
//+
Line(1) = {1, 2};
//+
Line(2) = {2, 6};
//+
Line(3) = {6, 3};
//+
Line(4) = {3, 4};
//+
Line(5) = {4, 5};
//+
Line(6) = {5, 1};
//+
Line(7) = {5, 6};
//+
Line Loop(1) = {1, 2, -7, 6};
//+
Plane Surface(1) = {1};
//+
Line Loop(2) = {7, 3, 4, 5};
//+
Plane Surface(2) = {2};
//+
Physical Line(2) = {1, 2, 6};
//+
Physical Line(3) = {3, 4, 5};
//+
Physical Surface(0) = {1};
//+
Physical Surface(1) = {2};
