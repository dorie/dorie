// Gmsh project created on Sat Aug 25 12:29:12 2018
//+
Point(1) = {0, 0, 0, 1.0};
//+
Point(2) = {2, 0, 0, 1.0};
//+
Point(3) = {2, 2, 0, 1.0};
//+
Point(4) = {0, 2, 0, 1.0};
//+
Point(5) = {0, 0, 1, 1.0};
//+
Point(6) = {2, 0, 1, 1.0};
//+
Point(7) = {2, 2, 1, 1.0};
//+
Point(8) = {0, 2, 1, 1.0};
//+
Point(9) = {0, 0, 2, 1.0};
//+
Point(10) = {2, 0, 2, 1.0};
//+
Point(11) = {2, 2, 2, 1.0};
//+
Point(12) = {0, 2, 2, 1.0};
//+
Line(1) = {1, 2};
//+
Line(2) = {2, 3};
//+
Line(3) = {3, 4};
//+
Line(4) = {4, 1};
//+
Line(5) = {5, 6};
//+
Line(6) = {6, 7};
//+
Line(7) = {7, 8};
//+
Line(8) = {8, 5};
//+
Line(9) = {9, 10};
//+
Line(10) = {10, 11};
//+
Line(11) = {11, 12};
//+
Line(12) = {12, 9};
//+
Line(13) = {9, 5};
//+
Line(14) = {12, 8};
//+
Line(15) = {10, 6};
//+
Line(16) = {11, 7};
//+
Line(17) = {7, 3};
//+
Line(18) = {6, 2};
//+
Line(19) = {8, 4};
//+
Line(20) = {5, 1};
//+
Line Loop(1) = {1, 2, 3, 4};
//+
Plane Surface(1) = {1};
//+
Line Loop(2) = {5, 6, 7, 8};
//+
Plane Surface(2) = {2};
//+
Line Loop(3) = {9, 10, 11, 12};
//+
Plane Surface(3) = {3};
//+
Line Loop(4) = {15, -5, -13, 9};
//+
Plane Surface(4) = {4};
//+
Line Loop(5) = {8, -13, -12, 14};
//+
Plane Surface(5) = {5};
//+
Line Loop(6) = {14, -7, -16, 11};
//+
Plane Surface(6) = {6};
//+
Line Loop(7) = {6, -16, -10, 15};
//+
Plane Surface(7) = {7};
//+
Line Loop(8) = {2, -17, -6, 18};
//+
Plane Surface(8) = {8};
//+
Line Loop(9) = {3, -19, -7, 17};
//+
Plane Surface(9) = {9};
//+
Line Loop(10) = {8, 20, -4, -19};
//+
Plane Surface(10) = {10};
//+
Line Loop(11) = {1, -18, -5, 20};
//+
Plane Surface(11) = {11};
//+
Surface Loop(1) = {8, 1, 11, 10, 9, 2};
//+
Volume(1) = {1};
//+
Surface Loop(2) = {7, 6, 5, 4, 3, 2};
//+
Volume(2) = {2};
//+
Physical Surface(2) = {11, 10, 9, 8, 1};
//+
Physical Surface(3) = {4, 5, 3, 6, 7};
//+
Physical Volume(0) = {1};
//+
Physical Volume(1) = {2};
