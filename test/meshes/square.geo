// Gmsh project created on Fri May  3 15:09:55 2019
//+
Point(1) = {0, 0, 0, 0.1};
//+
Point(2) = {1, 0, 0, 0.1};
//+
Point(3) = {1, 1, 0, 0.1};
//+
Point(4) = {0, 1, 0, 0.1};
//+
Line(1) = {1, 4};
//+
Line(2) = {4, 3};
//+
Line(3) = {3, 2};
//+
Line(4) = {2, 1};
//+
Line Loop(1) = {1, 2, 3, 4};
//+
Plane Surface(1) = {1};
//+
Physical Line(10) = {4};
//+
Physical Line(11) = {2};
//+
Physical Line(12) = {1};
//+
Physical Line(13) = {3};
//+
Physical Surface(0) = {1};
