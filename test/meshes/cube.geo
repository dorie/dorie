// Gmsh project created on Sat Aug 25 12:29:12 2018
//+
Point(1) = {0, 0, 0, 0.25};
//+
Point(2) = {1, 0, 0, 0.25};
//+
Point(3) = {1, 1, 0, 0.25};
//+
Point(4) = {0, 1, 0, 0.25};
//+
Point(5) = {0, 0, 1, 0.25};
//+
Point(6) = {1, 0, 1, 0.25};
//+
Point(7) = {1, 1, 1, 0.25};
//+
Point(8) = {0, 1, 1, 0.25};
//+
Line(1) = {5, 6};
//+
Line(2) = {6, 2};
//+
Line(3) = {2, 1};
//+
Line(4) = {1, 5};
//+
Line(5) = {8, 7};
//+
Line(6) = {7, 3};
//+
Line(7) = {3, 4};
//+
Line(8) = {4, 8};
//+
Line(9) = {5, 8};
//+
Line(10) = {6, 7};
//+
Line(11) = {2, 3};
//+
Line(12) = {1, 4};
//+
Line Loop(1) = {3, 12, -7, -11};
//+
Plane Surface(1) = {1};
//+
Line Loop(2) = {1, 10, -5, -9};
//+
Plane Surface(2) = {2};
//+
Line Loop(3) = {12, 8, -9, -4};
//+
Plane Surface(3) = {3};
//+
Line Loop(4) = {2, 11, -6, -10};
//+
Plane Surface(4) = {4};
//+
Line Loop(5) = {3, 4, 1, 2};
//+
Plane Surface(5) = {5};
//+
Line Loop(6) = {7, 8, 5, 6};
//+
Plane Surface(6) = {6};
//+
Surface Loop(1) = {5, 1, 3, 6, 2, 4};
//+
Volume(1) = {1};
//+
Physical Surface(10) = {1};
//+
Physical Surface(11) = {2};
//+
Physical Surface(12) = {3};
//+
Physical Surface(13) = {4};
//+
Physical Surface(14) = {5};
//+
Physical Surface(15) = {6};
//+
Physical Volume(0) = {1};
