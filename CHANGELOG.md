# DORiE Changelog

## Unreleased

### Added

* Extended Miller scaling adapter with additional porosity scaling !122
* Coding style tools and instructions.
    [`clang-format-hook`](https://github.com/barisione/clang-format-hooks)
    as Git submodule !203
* Automatically trigger DUNE Docker image builds using `rules` !213
* JOSS paper and showcase source files, and `CITATION.cff` file !198

### Changed

* Linerar solver for finite volumes changed from `AMG_4_DG` to
    `BCGS_AMG_SSOR` !204
* Simplify installation instructions !205
* Update Docker base image to Ubuntu Focal Fossa 20.04 LTS !189
* Improve the pipeline by resolving several minor issues !214
* Increase performance of `PVDReader` when reading many VTK files !216

### Fixed

* Fix installation procedure !207
* Compiler warnings for unused variables in `dune/dorie/common/h5file.hh` !206
* Ensure UTF-8 locale definition in Docker images !210
* `grid.extensions` values were erroneously read when loading GMSH grids #193
* Compiling failed on macOS because C++11 was selected as standard #198
* Flux reconstruction now considers volume elements without DOFs !219
* Breaking changes in upstream Python modules !224

## 2.0.0 (2020-05-14)

### Added

* [`yaml-cpp`](https://github.com/jbeder/yaml-cpp/) v5.2.0 or greater as
    dependency !81
* Abstract base class for Models !87 !159
* Finite volume solver for passive transport model !93 !88 !179 !180 !181
* Grid function container to store transient grid functions !115
* Infrastructure for the input of Miller scaling fields !110 !124 !133
* Logging framework [`spdlog`](https://github.com/gabime/spdlog) as
    Git submodule for all terminal output !106 !192
* Support input of boundary segmentations !119
* Reconstruction of continuous fluxes using RT finite elements !105
* Custom DG finite element map for simplices based on Pk polynomials !125
* Initial conditions expressed as analytic functions using
    [muparser](http://beltoforion.de/article.php?a=muparser) !131
* Coupling between transient models for water flow and solute transport !96
* Initial conditions generated from H5 input data !130
* Linear interpolator for initial conditions and scaling fields !145, !156
* Parameterizations for hydrodynamic dispersion in solute transport !141
* Generic Python VTK file reader !143, !150, !178
* Define compile-time settings via CPP macros !144
* [Google Test](https://github.com/google/googletest) unit test framework
    as Git Submodule !159
* Finite volume solver for the Richards equation !132 !161
* Cookbook tutorial on infiltration into homogeneous sand !157
* GitLab Description Templates for Releases !166
* ParaView tutorial for analyzing DORiE output files !173
* Parameter XML files support tags for version changes !171
* Unit test for Parameter XML file parser !171
* CMake option to enable code coverage flags on all targets !172
* Steady state initial condition in Richards model !176
* Changes to config file parameters listed per version in user docs !175
* DG solver for solute transport model !112
* Cookbook tutorial on using the random field generator !184
* Outflow boundary condition for Richards model !191
* Specify water content as initial condition in Richards model !187
* Documentation about model solver loop and time step adaptation schemes !190
* Documentation about the parallel features of several classes and methods !197

### Changed

* Data structures for storing and accessing parameter information !55
* Split unit tests into separate CI job with coverage reports !98
* Split grid adaptivity process into marking and actual adaptation !91
* Rework VTK adapters and VTK output writer !64
* `RichardsSimulation` now implements the abstract simulation base class !89
* Switch to the stable release branch `releases/2.6` of `dune-testtools` !97
* Merge Python packages into single new package `dorie` !100
* Move `dorie` Command Line Interface script into Python package !102
* Parameterization data input via YAML and H5 files !82
* Simplify H5 reader to only read datasets !109
* Extend run config file to contain data on multiple models !103
* DORiE now writes vertex data by default. !128
* Switch license from MIT to GPLv3 !135
* Generalized initial condition specification in config file !129
* Structure and setup of Sphinx user docs !126
* Switch to stable `dune-randomfield` release branch !151, !153
* System tests for executing `dorie pfg` module !153
* Build independent library and executable for each compile-time setting !144
* Deploy online documentation for each branch to private server !163
* Use YAML (instead of muPhi `.bcdat`) files for specifying BCs !121
* Unit and system tests report code coverage separately !172
* Improve docs on config file parameters and make default ones executable !183
* Update the contribution guidelines and the code of conduct !194

### Fixed

* Allow meta-ini files for unit tests #105
* Solver in `RichardsSimulation` was using the wrong time variable !116
* `dune_add_system_test` requires target with location after bugfix #161
* `make all` would not build the solver application target `dorie` #162
* Allow Neumman BC to be applied on a different direction than gravity #136
* Use unsafe loader of PyYAML v5.2 for loading parameter scraper data !177
* Steps were not synchronized when timestep suggestions differed in parallel #187

### Deprecated

### Removed

* Boundary conditions `evaporation` and `limitedInflux` !120

## 1.1.1 (2018-08-21)

### Added

* Documentation of Docker images and their usage in the CI/CD pipeline.

### Changed

* Rebuilds of the DUNE environment Docker image are now triggered by setting the
    CI variable `REBUILD_BASE_IMAGE` to a non-empty value. Previously, the
    builds in stage `setup` where triggered by any manual pipeline run.
* The test system folder was renamed from `testing` to [`test`](test).
* File `operator_DG.hh` is renamed  
    [`richards_operator_DG.hh`](dune/dorie/solver/richards_operator_DG.hh)

### Fixed

* Removed VTK output from `dorie_mass_conservation` executable, which would
    cause the corresponding test to fail if the designated output directory did
    not exist.
* Removed CI artifacts of `test` stage from `deploy` jobs.
* Correctly estimate the number of row entries in the matrix backend based on
    the DG discretization and proper static blocking. This can lead to a
    performance improvement for large problems.
* VTK output is now written _before_ the grid is adapted. Previously, the output
    displayed the adapted grid and not the one the solution was computed on.
* Grid adaptation is skipped after computing the final solution.
* Path to Pipeline artifacts for `test` stage. The artifacts are now correctly
    captured and can be viewed and downloaded from the GitLab web interface.
* Fix a bug in the boundary condition evaluation where an `end()` iterator
    was dereferenced without error.
* Re-introduced deployment of the documentation which can now be found online
    at https://dorie-doc.netlify.com/.

## 1.1.0 (2018-07-27)

### Added

* DORiE now employs a `SubsamplingVTKWriter` for higher output precision.
    Subsampling levels are controlled via the new key `output.subsamplingLevel`.
* Automatic deployment of `devel` and stable DORiE Docker Images to Docker Hub.
* Automatic update of the DUNE environment Docker Image for `master` builds.
* CI pipeline now also compiles a Debug build with the LLVM Clang compiler.
* Add `.gitattributes` for `CHANGELOG.md` to reduce merge conflicts with
    `merge=union`.
* Public API definition in documentation.

### Changed

* Update code base to *DUNE v2.6*.
* Raise software requirements to versions available on Ubuntu 18.04 LTS
    (Bionic Beaver).
* Switch VTK output from conforming to non-conforming.
* Use `multicore` build tag only for `test:parallel` build because the current
    GitLab Runner cannot handle the load.
* Use the `OVLP_AMG_4_DG` linear solver for all computations, including
    sequential ones. This significantly improves performance for large problems.
* Change compilation order which reduces RAM usage when compiling in parallel.
* Decentralization of the header system so that in most of the header files are 
    independent of each other.

### Deprecated

* `output.subsamplingLevel` is optional and defaults to 0 if not given
    explicitly in the config file. The key will become mandatory in future
    versions.

### Removed

* Complete doxygen documentation from Sphinx docs.

### Fixed

* The source code is now compatible to the Clang LLVM compiler.
    DORiE can now be compiled with the onboard compiler on macOS.
* Fix finite element map used for simplex grids. The old one did not implement
    a discontinuous function space.
* Allow warnings on ParMETIS not being available for parallel computations.

## 1.0.0 (2018-03-28)

First stable version.
